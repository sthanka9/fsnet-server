
const models = require('./models/index');
const swapFundAmendmentsHelper = require('./helpers/swapFundAmendmentsHelper');
const _ = require('lodash');
const { Op } = require('sequelize');


models.Fund.findAll({
    where:{
        partnershipDocumentPageCount:null
    }
})
.then(async funds=>{

    funds.forEach(async fund => {
        
        if(fund.partnershipDocument.path){

            const partnershipDocumentPageCount = await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(fund.partnershipDocument.path);

            console.log("====",fund.id,partnershipDocumentPageCount)
            if(partnershipDocumentPageCount){

                await models.Fund.update({
                    partnershipDocumentPageCount:partnershipDocumentPageCount
                },{
                    where:{
                        id:fund.id
                    }
                });
            
                await models.DocumentsForSignature.update({
                    originalPartnershipDocumentPageCount:partnershipDocumentPageCount
                },{
                    where:{
                        fundId:fund.id,
                        docType:{
                            [Op.in]:['FUND_AGREEMENT','CONSOLIDATED_FUND_AGREEMENT']
                        }
                    },
                    paranoid:false
                })
            }
        }
      
    });
})
