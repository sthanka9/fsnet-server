const models = require('../models/index');
const pmodels = require('../production_models'); 

pmodels.prod_sequelize.query("SELECT * FROM Notifications ", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async users => { 
    let bulkArray=[];
      for (let i = 0; i < users.length ; i++) {

        let element = users[i]         

        let  tempobj = {
          id:element.id,
          fromUserId: element.fromUserId,
          toUserId: element.toUserId,
          accountId: element.accountId ? element.accountId : null,
          isRead:element.isRead,
          notification: element.notification, 
          createdAt: element.createdAt,
          updatedAt: element.updatedAt
        } 
        
        bulkArray.push(tempobj)
          /*

          let userinfo = await models.Notification.findOne({attributes:['id'],where:{
            
            id:element.id
          }});          

            if(!userinfo){
              console.log("insert ***********",element.id)    
              await models.Notification.create(tempobj);
            
            } else {
              console.log("update ***********",element.id) 
              await models.Notification.update(tempobj,{where:{id:element.id}});
            } */ 
 
    }
    
    models.Notification.bulkCreate(bulkArray)
    .then(res=>{
        console.log('*******bulk insert**success*****',res)
    })
    .catch(e=>{
        console.log('*******bulk insert****error***',e)
    })
     
})
