
const models = require('../models/index');
// const bcrypt = require('bcryptjs');


// const salt = bcrypt.genSaltSync(10);
// const passwordHash = bcrypt.hashSync('Fsnet@123', salt);

// const newUser = {
//     firstName: 'Admin',
//     lastName: 'Vanilla',
//     middleName: '',
//     accountType: 'FSNETAdministrator',
//     email: 'test@menlo-technologies.com',
//     password: passwordHash,
//     emailConfirmCode: null,
//     isEmailConfirmed: 1
// };

// models.User.create(newUser).then(user => {
//     models.UserRole.create({ roleId: 1, userId: user.id });
// })

models.User.findOne({
    attributes: ['id', 'email', 'accountType'],
    where: {accountType: 'LP' },
    include: [{
        attributes: ['fundId', 'lpId'],
        as: 'subscription',
        model: models.FundSubscription, where: { fundId: 83 },
        required: false
    }]
})