const models = require('../models/index');


  models.State.bulkCreate([{
    "id": "779",
    "name": "Bogota",
    "country_id": "47"
  },{
    "id": "780",
    "name": "Bolivar",
    "country_id": "47"
  },
  {
    "id": "781",
    "name": "Boyaca",
    "country_id": "47"
  },
  {
    "id": "782",
    "name": "Caldas",
    "country_id": "47"
  },
  {
    "id": "783",
    "name": "Caqueta",
    "country_id": "47"
  },
  {
    "id": "784",
    "name": "Casanare",
    "country_id": "47"
  },
  {
    "id": "785",
    "name": "Cauca",
    "country_id": "47"
  },
  {
    "id": "786",
    "name": "Cesar",
    "country_id": "47"
  },
  {
    "id": "787",
    "name": "Choco",
    "country_id": "47"
  },
  {
    "id": "788",
    "name": "Cordoba",
    "country_id": "47"
  },
  {
    "id": "789",
    "name": "Cundinamarca",
    "country_id": "47"
  },
  {
    "id": "790",
    "name": "Guainia",
    "country_id": "47"
  },
  {
    "id": "791",
    "name": "Guaviare",
    "country_id": "47"
  },
  {
    "id": "792",
    "name": "Huila",
    "country_id": "47"
  },
  {
    "id": "793",
    "name": "La Guajira",
    "country_id": "47"
  },
  {
    "id": "794",
    "name": "Magdalena",
    "country_id": "47"
  },
  {
    "id": "795",
    "name": "Meta",
    "country_id": "47"
  },
  {
    "id": "796",
    "name": "Narino",
    "country_id": "47"
  },
  {
    "id": "797",
    "name": "Norte de Santander",
    "country_id": "47"
  },
  {
    "id": "798",
    "name": "Putumayo",
    "country_id": "47"
  },
  {
    "id": "799",
    "name": "Quindio",
    "country_id": "47"
  },
  {
    "id": "800",
    "name": "Risaralda",
    "country_id": "47"
  },
  {
    "id": "801",
    "name": "San Andres y Providencia",
    "country_id": "47"
  },
  {
    "id": "802",
    "name": "Santander",
    "country_id": "47"
  },
  {
    "id": "803",
    "name": "Sucre",
    "country_id": "47"
  },
  {
    "id": "804",
    "name": "Tolima",
    "country_id": "47"
  },
  {
    "id": "805",
    "name": "Valle del Cauca",
    "country_id": "47"
  },
  {
    "id": "806",
    "name": "Vaupes",
    "country_id": "47"
  },
  {
    "id": "807",
    "name": "Vichada",
    "country_id": "47"
  },
  {
    "id": "808",
    "name": "Mwali",
    "country_id": "48"
  },
  {
    "id": "809",
    "name": "Njazidja",
    "country_id": "48"
  },
  {
    "id": "810",
    "name": "Nzwani",
    "country_id": "48"
  },
  {
    "id": "811",
    "name": "Bouenza",
    "country_id": "49"
  },
  {
    "id": "812",
    "name": "Brazzaville",
    "country_id": "49"
  },
  {
    "id": "813",
    "name": "Cuvette",
    "country_id": "49"
  },
  {
    "id": "814",
    "name": "Kouilou",
    "country_id": "49"
  },
  {
    "id": "815",
    "name": "Lekoumou",
    "country_id": "49"
  },
  {
    "id": "816",
    "name": "Likouala",
    "country_id": "49"
  },
  {
    "id": "817",
    "name": "Niari",
    "country_id": "49"
  },
  {
    "id": "818",
    "name": "Plateaux",
    "country_id": "49"
  },
  {
    "id": "819",
    "name": "Pool",
    "country_id": "49"
  },
  {
    "id": "820",
    "name": "Sangha",
    "country_id": "49"
  },
  {
    "id": "821",
    "name": "Bandundu",
    "country_id": "50"
  },
  {
    "id": "822",
    "name": "Bas-Congo",
    "country_id": "50"
  },
  {
    "id": "823",
    "name": "Equateur",
    "country_id": "50"
  },
  {
    "id": "824",
    "name": "Haut-Congo",
    "country_id": "50"
  },
  {
    "id": "825",
    "name": "Kasai-Occidental",
    "country_id": "50"
  },
  {
    "id": "826",
    "name": "Kasai-Oriental",
    "country_id": "50"
  },
  {
    "id": "827",
    "name": "Katanga",
    "country_id": "50"
  },
  {
    "id": "828",
    "name": "Kinshasa",
    "country_id": "50"
  },
  {
    "id": "829",
    "name": "Maniema",
    "country_id": "50"
  },
  {
    "id": "830",
    "name": "Nord-Kivu",
    "country_id": "50"
  },
  {
    "id": "831",
    "name": "Sud-Kivu",
    "country_id": "50"
  },
  {
    "id": "832",
    "name": "Aitutaki",
    "country_id": "51"
  },
  {
    "id": "833",
    "name": "Atiu",
    "country_id": "51"
  },
  {
    "id": "834",
    "name": "Mangaia",
    "country_id": "51"
  },
  {
    "id": "835",
    "name": "Manihiki",
    "country_id": "51"
  },
  {
    "id": "836",
    "name": "Mauke",
    "country_id": "51"
  },
  {
    "id": "837",
    "name": "Mitiaro",
    "country_id": "51"
  },
  {
    "id": "838",
    "name": "Nassau",
    "country_id": "51"
  },
  {
    "id": "839",
    "name": "Pukapuka",
    "country_id": "51"
  },
  {
    "id": "840",
    "name": "Rakahanga",
    "country_id": "51"
  },
  {
    "id": "841",
    "name": "Rarotonga",
    "country_id": "51"
  },
  {
    "id": "842",
    "name": "Tongareva",
    "country_id": "51"
  },
  {
    "id": "843",
    "name": "Alajuela",
    "country_id": "52"
  },
  {
    "id": "844",
    "name": "Cartago",
    "country_id": "52"
  },
  {
    "id": "845",
    "name": "Guanacaste",
    "country_id": "52"
  },
  {
    "id": "846",
    "name": "Heredia",
    "country_id": "52"
  },
  {
    "id": "847",
    "name": "Limon",
    "country_id": "52"
  },
  {
    "id": "848",
    "name": "Puntarenas",
    "country_id": "52"
  },
  {
    "id": "849",
    "name": "San Jose",
    "country_id": "52"
  },
  {
    "id": "850",
    "name": "Abidjan",
    "country_id": "53"
  },
  {
    "id": "851",
    "name": "Agneby",
    "country_id": "53"
  },
  {
    "id": "852",
    "name": "Bafing",
    "country_id": "53"
  },
  {
    "id": "853",
    "name": "Denguele",
    "country_id": "53"
  },
  {
    "id": "854",
    "name": "Dix-huit Montagnes",
    "country_id": "53"
  },
  {
    "id": "855",
    "name": "Fromager",
    "country_id": "53"
  },
  {
    "id": "856",
    "name": "Haut-Sassandra",
    "country_id": "53"
  },
  {
    "id": "857",
    "name": "Lacs",
    "country_id": "53"
  },
  {
    "id": "858",
    "name": "Lagunes",
    "country_id": "53"
  },
  {
    "id": "859",
    "name": "Marahoue",
    "country_id": "53"
  },
  {
    "id": "860",
    "name": "Moyen-Cavally",
    "country_id": "53"
  },
  {
    "id": "861",
    "name": "Moyen-Comoe",
    "country_id": "53"
  },
  {
    "id": "862",
    "name": "N\'\'zi-Comoe",
    "country_id": "53"
  },
  {
    "id": "863",
    "name": "Sassandra",
    "country_id": "53"
  },
  {
    "id": "864",
    "name": "Savanes",
    "country_id": "53"
  },
  {
    "id": "865",
    "name": "Sud-Bandama",
    "country_id": "53"
  },
  {
    "id": "866",
    "name": "Sud-Comoe",
    "country_id": "53"
  },
  {
    "id": "867",
    "name": "Vallee du Bandama",
    "country_id": "53"
  },
  {
    "id": "868",
    "name": "Worodougou",
    "country_id": "53"
  },
  {
    "id": "869",
    "name": "Zanzan",
    "country_id": "53"
  },
  {
    "id": "870",
    "name": "Bjelovar-Bilogora",
    "country_id": "54"
  },
  {
    "id": "871",
    "name": "Dubrovnik-Neretva",
    "country_id": "54"
  },
  {
    "id": "872",
    "name": "Grad Zagreb",
    "country_id": "54"
  },
  {
    "id": "873",
    "name": "Istra",
    "country_id": "54"
  },
  {
    "id": "874",
    "name": "Karlovac",
    "country_id": "54"
  },
  {
    "id": "875",
    "name": "Koprivnica-Krizhevci",
    "country_id": "54"
  },
  {
    "id": "876",
    "name": "Krapina-Zagorje",
    "country_id": "54"
  },
  {
    "id": "877",
    "name": "Lika-Senj",
    "country_id": "54"
  },
  {
    "id": "878",
    "name": "Medhimurje",
    "country_id": "54"
  },
  {
    "id": "879",
    "name": "Medimurska Zupanija",
    "country_id": "54"
  },
  {
    "id": "880",
    "name": "Osijek-Baranja",
    "country_id": "54"
  },
  {
    "id": "881",
    "name": "Osjecko-Baranjska Zupanija",
    "country_id": "54"
  },
  {
    "id": "882",
    "name": "Pozhega-Slavonija",
    "country_id": "54"
  },
  {
    "id": "883",
    "name": "Primorje-Gorski Kotar",
    "country_id": "54"
  },
  {
    "id": "884",
    "name": "Shibenik-Knin",
    "country_id": "54"
  },
  {
    "id": "885",
    "name": "Sisak-Moslavina",
    "country_id": "54"
  },
  {
    "id": "886",
    "name": "Slavonski Brod-Posavina",
    "country_id": "54"
  },
  {
    "id": "887",
    "name": "Split-Dalmacija",
    "country_id": "54"
  },
  {
    "id": "888",
    "name": "Varazhdin",
    "country_id": "54"
  },
  {
    "id": "889",
    "name": "Virovitica-Podravina",
    "country_id": "54"
  },
  {
    "id": "890",
    "name": "Vukovar-Srijem",
    "country_id": "54"
  },
  {
    "id": "891",
    "name": "Zadar",
    "country_id": "54"
  },
  {
    "id": "892",
    "name": "Zagreb",
    "country_id": "54"
  },
  {
    "id": "893",
    "name": "Camaguey",
    "country_id": "55"
  },
  {
    "id": "894",
    "name": "Ciego de Avila",
    "country_id": "55"
  },
  {
    "id": "895",
    "name": "Cienfuegos",
    "country_id": "55"
  },
  {
    "id": "896",
    "name": "Ciudad de la Habana",
    "country_id": "55"
  },
  {
    "id": "897",
    "name": "Granma",
    "country_id": "55"
  },
  {
    "id": "898",
    "name": "Guantanamo",
    "country_id": "55"
  },
  {
    "id": "899",
    "name": "Habana",
    "country_id": "55"
  },
  {
    "id": "900",
    "name": "Holguin",
    "country_id": "55"
  },
  {
    "id": "901",
    "name": "Isla de la Juventud",
    "country_id": "55"
  },
  {
    "id": "902",
    "name": "La Habana",
    "country_id": "55"
  },
  {
    "id": "903",
    "name": "Las Tunas",
    "country_id": "55"
  },
  {
    "id": "904",
    "name": "Matanzas",
    "country_id": "55"
  },
  {
    "id": "905",
    "name": "Pinar del Rio",
    "country_id": "55"
  },
  {
    "id": "906",
    "name": "Sancti Spiritus",
    "country_id": "55"
  },
  {
    "id": "907",
    "name": "Santiago de Cuba",
    "country_id": "55"
  },
  {
    "id": "908",
    "name": "Villa Clara",
    "country_id": "55"
  },
  {
    "id": "909",
    "name": "Government controlled area",
    "country_id": "56"
  },
  {
    "id": "910",
    "name": "Limassol",
    "country_id": "56"
  },
  {
    "id": "911",
    "name": "Nicosia District",
    "country_id": "56"
  },
  {
    "id": "912",
    "name": "Paphos",
    "country_id": "56"
  },
  {
    "id": "913",
    "name": "Turkish controlled area",
    "country_id": "56"
  },
  {
    "id": "914",
    "name": "Central Bohemian",
    "country_id": "57"
  },
  {
    "id": "915",
    "name": "Frycovice",
    "country_id": "57"
  },
  {
    "id": "916",
    "name": "Jihocesky Kraj",
    "country_id": "57"
  },
  {
    "id": "917",
    "name": "Jihochesky",
    "country_id": "57"
  },
  {
    "id": "918",
    "name": "Jihomoravsky",
    "country_id": "57"
  },
  {
    "id": "919",
    "name": "Karlovarsky",
    "country_id": "57"
  },
  {
    "id": "920",
    "name": "Klecany",
    "country_id": "57"
  },
  {
    "id": "921",
    "name": "Kralovehradecky",
    "country_id": "57"
  },
  {
    "id": "922",
    "name": "Liberecky",
    "country_id": "57"
  },
  {
    "id": "923",
    "name": "Lipov",
    "country_id": "57"
  },
  {
    "id": "924",
    "name": "Moravskoslezsky",
    "country_id": "57"
  },
  {
    "id": "925",
    "name": "Olomoucky",
    "country_id": "57"
  },
  {
    "id": "926",
    "name": "Olomoucky Kraj",
    "country_id": "57"
  },
  {
    "id": "927",
    "name": "Pardubicky",
    "country_id": "57"
  },
  {
    "id": "928",
    "name": "Plzensky",
    "country_id": "57"
  },
  {
    "id": "929",
    "name": "Praha",
    "country_id": "57"
  },
  {
    "id": "930",
    "name": "Rajhrad",
    "country_id": "57"
  },
  {
    "id": "931",
    "name": "Smirice",
    "country_id": "57"
  },
  {
    "id": "932",
    "name": "South Moravian",
    "country_id": "57"
  },
  {
    "id": "933",
    "name": "Straz nad Nisou",
    "country_id": "57"
  },
  {
    "id": "934",
    "name": "Stredochesky",
    "country_id": "57"
  },
  {
    "id": "935",
    "name": "Unicov",
    "country_id": "57"
  },
  {
    "id": "936",
    "name": "Ustecky",
    "country_id": "57"
  },
  {
    "id": "937",
    "name": "Valletta",
    "country_id": "57"
  },
  {
    "id": "938",
    "name": "Velesin",
    "country_id": "57"
  },
  {
    "id": "939",
    "name": "Vysochina",
    "country_id": "57"
  },
  {
    "id": "940",
    "name": "Zlinsky",
    "country_id": "57"
  },
  {
    "id": "941",
    "name": "Arhus",
    "country_id": "58"
  },
  {
    "id": "942",
    "name": "Bornholm",
    "country_id": "58"
  },
  {
    "id": "943",
    "name": "Frederiksborg",
    "country_id": "58"
  },
  {
    "id": "944",
    "name": "Fyn",
    "country_id": "58"
  },
  {
    "id": "945",
    "name": "Hovedstaden",
    "country_id": "58"
  },
  {
    "id": "946",
    "name": "Kobenhavn",
    "country_id": "58"
  },
  {
    "id": "947",
    "name": "Kobenhavns Amt",
    "country_id": "58"
  },
  {
    "id": "948",
    "name": "Kobenhavns Kommune",
    "country_id": "58"
  },
  {
    "id": "949",
    "name": "Nordjylland",
    "country_id": "58"
  },
  {
    "id": "950",
    "name": "Ribe",
    "country_id": "58"
  },
  {
    "id": "951",
    "name": "Ringkobing",
    "country_id": "58"
  },
  {
    "id": "952",
    "name": "Roervig",
    "country_id": "58"
  },
  {
    "id": "953",
    "name": "Roskilde",
    "country_id": "58"
  },
  {
    "id": "954",
    "name": "Roslev",
    "country_id": "58"
  },
  {
    "id": "955",
    "name": "Sjaelland",
    "country_id": "58"
  },
  {
    "id": "956",
    "name": "Soeborg",
    "country_id": "58"
  },
  {
    "id": "957",
    "name": "Sonderjylland",
    "country_id": "58"
  },
  {
    "id": "958",
    "name": "Storstrom",
    "country_id": "58"
  },
  {
    "id": "959",
    "name": "Syddanmark",
    "country_id": "58"
  },
  {
    "id": "960",
    "name": "Toelloese",
    "country_id": "58"
  },
  {
    "id": "961",
    "name": "Vejle",
    "country_id": "58"
  },
  {
    "id": "962",
    "name": "Vestsjalland",
    "country_id": "58"
  },
  {
    "id": "963",
    "name": "Viborg",
    "country_id": "58"
  },
  {
    "id": "964",
    "name": "Ali Sabih",
    "country_id": "59"
  },
  {
    "id": "965",
    "name": "Dikhil",
    "country_id": "59"
  },
  {
    "id": "966",
    "name": "Jibuti",
    "country_id": "59"
  },
  {
    "id": "967",
    "name": "Tajurah",
    "country_id": "59"
  },
  {
    "id": "968",
    "name": "Ubuk",
    "country_id": "59"
  },
  {
    "id": "969",
    "name": "Saint Andrew",
    "country_id": "60"
  },
  {
    "id": "970",
    "name": "Saint David",
    "country_id": "60"
  },
  {
    "id": "971",
    "name": "Saint George",
    "country_id": "60"
  },
  {
    "id": "972",
    "name": "Saint John",
    "country_id": "60"
  },
  {
    "id": "973",
    "name": "Saint Joseph",
    "country_id": "60"
  },
  {
    "id": "974",
    "name": "Saint Luke",
    "country_id": "60"
  },
  {
    "id": "975",
    "name": "Saint Mark",
    "country_id": "60"
  },
  {
    "id": "976",
    "name": "Saint Patrick",
    "country_id": "60"
  },
  {
    "id": "977",
    "name": "Saint Paul",
    "country_id": "60"
  },
  {
    "id": "978",
    "name": "Saint Peter",
    "country_id": "60"
  },
  {
    "id": "979",
    "name": "Azua",
    "country_id": "61"
  },
  {
    "id": "980",
    "name": "Bahoruco",
    "country_id": "61"
  },
  {
    "id": "981",
    "name": "Barahona",
    "country_id": "61"
  },
  {
    "id": "982",
    "name": "Dajabon",
    "country_id": "61"
  },
  {
    "id": "983",
    "name": "Distrito Nacional",
    "country_id": "61"
  },
  {
    "id": "984",
    "name": "Duarte",
    "country_id": "61"
  },
  {
    "id": "985",
    "name": "El Seybo",
    "country_id": "61"
  },
  {
    "id": "986",
    "name": "Elias Pina",
    "country_id": "61"
  },
  {
    "id": "987",
    "name": "Espaillat",
    "country_id": "61"
  },
  {
    "id": "988",
    "name": "Hato Mayor",
    "country_id": "61"
  },
  {
    "id": "989",
    "name": "Independencia",
    "country_id": "61"
  },
  {
    "id": "990",
    "name": "La Altagracia",
    "country_id": "61"
  },
  {
    "id": "991",
    "name": "La Romana",
    "country_id": "61"
  },
  {
    "id": "992",
    "name": "La Vega",
    "country_id": "61"
  },
  {
    "id": "993",
    "name": "Maria Trinidad Sanchez",
    "country_id": "61"
  },
  {
    "id": "994",
    "name": "Monsenor Nouel",
    "country_id": "61"
  },
  {
    "id": "995",
    "name": "Monte Cristi",
    "country_id": "61"
  },
  {
    "id": "996",
    "name": "Monte Plata",
    "country_id": "61"
  },
  {
    "id": "997",
    "name": "Pedernales",
    "country_id": "61"
  },
  {
    "id": "998",
    "name": "Peravia",
    "country_id": "61"
  },
  {
    "id": "999",
    "name": "Puerto Plata",
    "country_id": "61"
  },
  {
    "id": "1000",
    "name": "Salcedo",
    "country_id": "61"
  },
  {
    "id": "1001",
    "name": "Samana",
    "country_id": "61"
  },
  {
    "id": "1002",
    "name": "San Cristobal",
    "country_id": "61"
  },
  {
    "id": "1003",
    "name": "San Juan",
    "country_id": "61"
  },
  {
    "id": "1004",
    "name": "San Pedro de Macoris",
    "country_id": "61"
  },
  {
    "id": "1005",
    "name": "Sanchez Ramirez",
    "country_id": "61"
  },
  {
    "id": "1006",
    "name": "Santiago",
    "country_id": "61"
  },
  {
    "id": "1007",
    "name": "Santiago Rodriguez",
    "country_id": "61"
  },
  {
    "id": "1008",
    "name": "Valverde",
    "country_id": "61"
  },
  {
    "id": "1009",
    "name": "Aileu",
    "country_id": "62"
  },
  {
    "id": "1010",
    "name": "Ainaro",
    "country_id": "62"
  },
  {
    "id": "1011",
    "name": "Ambeno",
    "country_id": "62"
  },
  {
    "id": "1012",
    "name": "Baucau",
    "country_id": "62"
  },
  {
    "id": "1013",
    "name": "Bobonaro",
    "country_id": "62"
  },
  {
    "id": "1014",
    "name": "Cova Lima",
    "country_id": "62"
  },
  {
    "id": "1015",
    "name": "Dili",
    "country_id": "62"
  },
  {
    "id": "1016",
    "name": "Ermera",
    "country_id": "62"
  },
  {
    "id": "1017",
    "name": "Lautem",
    "country_id": "62"
  },
  {
    "id": "1018",
    "name": "Liquica",
    "country_id": "62"
  },
  {
    "id": "1019",
    "name": "Manatuto",
    "country_id": "62"
  },
  {
    "id": "1020",
    "name": "Manufahi",
    "country_id": "62"
  },
  {
    "id": "1021",
    "name": "Viqueque",
    "country_id": "62"
  },
  {
    "id": "1022",
    "name": "Azuay",
    "country_id": "63"
  },
  {
    "id": "1023",
    "name": "Bolivar",
    "country_id": "63"
  },
  {
    "id": "1024",
    "name": "Canar",
    "country_id": "63"
  },
  {
    "id": "1025",
    "name": "Carchi",
    "country_id": "63"
  },
  {
    "id": "1026",
    "name": "Chimborazo",
    "country_id": "63"
  },
  {
    "id": "1027",
    "name": "Cotopaxi",
    "country_id": "63"
  },
  {
    "id": "1028",
    "name": "El Oro",
    "country_id": "63"
  },
  {
    "id": "1029",
    "name": "Esmeraldas",
    "country_id": "63"
  },
  {
    "id": "1030",
    "name": "Galapagos",
    "country_id": "63"
  },
  {
    "id": "1031",
    "name": "Guayas",
    "country_id": "63"
  },
  {
    "id": "1032",
    "name": "Imbabura",
    "country_id": "63"
  },
  {
    "id": "1033",
    "name": "Loja",
    "country_id": "63"
  },
  {
    "id": "1034",
    "name": "Los Rios",
    "country_id": "63"
  },
  {
    "id": "1035",
    "name": "Manabi",
    "country_id": "63"
  },
  {
    "id": "1036",
    "name": "Morona Santiago",
    "country_id": "63"
  },
  {
    "id": "1037",
    "name": "Napo",
    "country_id": "63"
  },
  {
    "id": "1038",
    "name": "Orellana",
    "country_id": "63"
  },
  {
    "id": "1039",
    "name": "Pastaza",
    "country_id": "63"
  },
  {
    "id": "1040",
    "name": "Pichincha",
    "country_id": "63"
  },
  {
    "id": "1041",
    "name": "Sucumbios",
    "country_id": "63"
  },
  {
    "id": "1042",
    "name": "Tungurahua",
    "country_id": "63"
  },
  {
    "id": "1043",
    "name": "Zamora Chinchipe",
    "country_id": "63"
  },
  {
    "id": "1044",
    "name": "Aswan",
    "country_id": "64"
  },
  {
    "id": "1045",
    "name": "Asyut",
    "country_id": "64"
  },
  {
    "id": "1046",
    "name": "Bani Suwayf",
    "country_id": "64"
  },
  {
    "id": "1047",
    "name": "Bur Sa\'\'id",
    "country_id": "64"
  },
  {
    "id": "1048",
    "name": "Cairo",
    "country_id": "64"
  },
  {
    "id": "1049",
    "name": "Dumyat",
    "country_id": "64"
  },
  {
    "id": "1050",
    "name": "Kafr-ash-Shaykh",
    "country_id": "64"
  },
  {
    "id": "1051",
    "name": "Matruh",
    "country_id": "64"
  },
  {
    "id": "1052",
    "name": "Muhafazat ad Daqahliyah",
    "country_id": "64"
  },
  {
    "id": "1053",
    "name": "Muhafazat al Fayyum",
    "country_id": "64"
  },
  {
    "id": "1054",
    "name": "Muhafazat al Gharbiyah",
    "country_id": "64"
  },
  {
    "id": "1055",
    "name": "Muhafazat al Iskandariyah",
    "country_id": "64"
  },
  {
    "id": "1056",
    "name": "Muhafazat al Qahirah",
    "country_id": "64"
  },
  {
    "id": "1057",
    "name": "Qina",
    "country_id": "64"
  },
  {
    "id": "1058",
    "name": "Sawhaj",
    "country_id": "64"
  },
  {
    "id": "1059",
    "name": "Sina al-Janubiyah",
    "country_id": "64"
  },
  {
    "id": "1060",
    "name": "Sina ash-Shamaliyah",
    "country_id": "64"
  },
  {
    "id": "1061",
    "name": "ad-Daqahliyah",
    "country_id": "64"
  },
  {
    "id": "1062",
    "name": "al-Bahr-al-Ahmar",
    "country_id": "64"
  },
  {
    "id": "1063",
    "name": "al-Buhayrah",
    "country_id": "64"
  },
  {
    "id": "1064",
    "name": "al-Fayyum",
    "country_id": "64"
  },
  {
    "id": "1065",
    "name": "al-Gharbiyah",
    "country_id": "64"
  },
  {
    "id": "1066",
    "name": "al-Iskandariyah",
    "country_id": "64"
  },
  {
    "id": "1067",
    "name": "al-Ismailiyah",
    "country_id": "64"
  },
  {
    "id": "1068",
    "name": "al-Jizah",
    "country_id": "64"
  },
  {
    "id": "1069",
    "name": "al-Minufiyah",
    "country_id": "64"
  },
  {
    "id": "1070",
    "name": "al-Minya",
    "country_id": "64"
  },
  {
    "id": "1071",
    "name": "al-Qahira",
    "country_id": "64"
  },
  {
    "id": "1072",
    "name": "al-Qalyubiyah",
    "country_id": "64"
  },
  {
    "id": "1073",
    "name": "al-Uqsur",
    "country_id": "64"
  },
  {
    "id": "1074",
    "name": "al-Wadi al-Jadid",
    "country_id": "64"
  },
  {
    "id": "1075",
    "name": "as-Suways",
    "country_id": "64"
  },
  {
    "id": "1076",
    "name": "ash-Sharqiyah",
    "country_id": "64"
  },
  {
    "id": "1077",
    "name": "Ahuachapan",
    "country_id": "65"
  },
  {
    "id": "1078",
    "name": "Cabanas",
    "country_id": "65"
  },
  {
    "id": "1079",
    "name": "Chalatenango",
    "country_id": "65"
  },
  {
    "id": "1080",
    "name": "Cuscatlan",
    "country_id": "65"
  },
  {
    "id": "1081",
    "name": "La Libertad",
    "country_id": "65"
  },
  {
    "id": "1082",
    "name": "La Paz",
    "country_id": "65"
  },
  {
    "id": "1083",
    "name": "La Union",
    "country_id": "65"
  },
  {
    "id": "1084",
    "name": "Morazan",
    "country_id": "65"
  },
  {
    "id": "1085",
    "name": "San Miguel",
    "country_id": "65"
  },
  {
    "id": "1086",
    "name": "San Salvador",
    "country_id": "65"
  },
  {
    "id": "1087",
    "name": "San Vicente",
    "country_id": "65"
  },
  {
    "id": "1088",
    "name": "Santa Ana",
    "country_id": "65"
  },
  {
    "id": "1089",
    "name": "Sonsonate",
    "country_id": "65"
  },
  {
    "id": "1090",
    "name": "Usulutan",
    "country_id": "65"
  },
  {
    "id": "1091",
    "name": "Annobon",
    "country_id": "66"
  },
  {
    "id": "1092",
    "name": "Bioko Norte",
    "country_id": "66"
  },
  {
    "id": "1093",
    "name": "Bioko Sur",
    "country_id": "66"
  },
  {
    "id": "1094",
    "name": "Centro Sur",
    "country_id": "66"
  },
  {
    "id": "1095",
    "name": "Kie-Ntem",
    "country_id": "66"
  },
  {
    "id": "1096",
    "name": "Litoral",
    "country_id": "66"
  },
  {
    "id": "1097",
    "name": "Wele-Nzas",
    "country_id": "66"
  },
  {
    "id": "1098",
    "name": "Anseba",
    "country_id": "67"
  },
  {
    "id": "1099",
    "name": "Debub",
    "country_id": "67"
  },
  {
    "id": "1100",
    "name": "Debub-Keih-Bahri",
    "country_id": "67"
  },
  {
    "id": "1101",
    "name": "Gash-Barka",
    "country_id": "67"
  }]);