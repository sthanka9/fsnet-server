const models = require('../models/index');


  models.State.bulkCreate([{
    "id": "1842",
    "name": "Lecce",
    "country_id": "107"
  },
  {
    "id": "1843",
    "name": "Lecco",
    "country_id": "107"
  },
  {
    "id": "1844",
    "name": "Lecco Province",
    "country_id": "107"
  },
  {
    "id": "1845",
    "name": "Liguria",
    "country_id": "107"
  },
  {
    "id": "1846",
    "name": "Lodi",
    "country_id": "107"
  },
  {
    "id": "1847",
    "name": "Lombardia",
    "country_id": "107"
  },
  {
    "id": "1848",
    "name": "Lombardy",
    "country_id": "107"
  },
  {
    "id": "1849",
    "name": "Macerata",
    "country_id": "107"
  },
  {
    "id": "1850",
    "name": "Mantova",
    "country_id": "107"
  },
  {
    "id": "1851",
    "name": "Marche",
    "country_id": "107"
  },
  {
    "id": "1852",
    "name": "Messina",
    "country_id": "107"
  },
  {
    "id": "1853",
    "name": "Milan",
    "country_id": "107"
  },
  {
    "id": "1854",
    "name": "Modena",
    "country_id": "107"
  },
  {
    "id": "1855",
    "name": "Molise",
    "country_id": "107"
  },
  {
    "id": "1856",
    "name": "Molteno",
    "country_id": "107"
  },
  {
    "id": "1857",
    "name": "Montenegro",
    "country_id": "107"
  },
  {
    "id": "1858",
    "name": "Monza and Brianza",
    "country_id": "107"
  },
  {
    "id": "1859",
    "name": "Naples",
    "country_id": "107"
  },
  {
    "id": "1860",
    "name": "Novara",
    "country_id": "107"
  },
  {
    "id": "1861",
    "name": "Padova",
    "country_id": "107"
  },
  {
    "id": "1862",
    "name": "Parma",
    "country_id": "107"
  },
  {
    "id": "1863",
    "name": "Pavia",
    "country_id": "107"
  },
  {
    "id": "1864",
    "name": "Perugia",
    "country_id": "107"
  },
  {
    "id": "1865",
    "name": "Pesaro-Urbino",
    "country_id": "107"
  },
  {
    "id": "1866",
    "name": "Piacenza",
    "country_id": "107"
  },
  {
    "id": "1867",
    "name": "Piedmont",
    "country_id": "107"
  },
  {
    "id": "1868",
    "name": "Piemonte",
    "country_id": "107"
  },
  {
    "id": "1869",
    "name": "Pisa",
    "country_id": "107"
  },
  {
    "id": "1870",
    "name": "Pordenone",
    "country_id": "107"
  },
  {
    "id": "1871",
    "name": "Potenza",
    "country_id": "107"
  },
  {
    "id": "1872",
    "name": "Puglia",
    "country_id": "107"
  },
  {
    "id": "1873",
    "name": "Reggio Emilia",
    "country_id": "107"
  },
  {
    "id": "1874",
    "name": "Rimini",
    "country_id": "107"
  },
  {
    "id": "1875",
    "name": "Roma",
    "country_id": "107"
  },
  {
    "id": "1876",
    "name": "Salerno",
    "country_id": "107"
  },
  {
    "id": "1877",
    "name": "Sardegna",
    "country_id": "107"
  },
  {
    "id": "1878",
    "name": "Sassari",
    "country_id": "107"
  },
  {
    "id": "1879",
    "name": "Savona",
    "country_id": "107"
  },
  {
    "id": "1880",
    "name": "Sicilia",
    "country_id": "107"
  },
  {
    "id": "1881",
    "name": "Siena",
    "country_id": "107"
  },
  {
    "id": "1882",
    "name": "Sondrio",
    "country_id": "107"
  },
  {
    "id": "1883",
    "name": "South Tyrol",
    "country_id": "107"
  },
  {
    "id": "1884",
    "name": "Taranto",
    "country_id": "107"
  },
  {
    "id": "1885",
    "name": "Teramo",
    "country_id": "107"
  },
  {
    "id": "1886",
    "name": "Torino",
    "country_id": "107"
  },
  {
    "id": "1887",
    "name": "Toscana",
    "country_id": "107"
  },
  {
    "id": "1888",
    "name": "Trapani",
    "country_id": "107"
  },
  {
    "id": "1889",
    "name": "Trentino-Alto Adige",
    "country_id": "107"
  },
  {
    "id": "1890",
    "name": "Trento",
    "country_id": "107"
  },
  {
    "id": "1891",
    "name": "Treviso",
    "country_id": "107"
  },
  {
    "id": "1892",
    "name": "Udine",
    "country_id": "107"
  },
  {
    "id": "1893",
    "name": "Umbria",
    "country_id": "107"
  },
  {
    "id": "1894",
    "name": "Valle d\'\'Aosta",
    "country_id": "107"
  },
  {
    "id": "1895",
    "name": "Varese",
    "country_id": "107"
  },
  {
    "id": "1896",
    "name": "Veneto",
    "country_id": "107"
  },
  {
    "id": "1897",
    "name": "Venezia",
    "country_id": "107"
  },
  {
    "id": "1898",
    "name": "Verbano-Cusio-Ossola",
    "country_id": "107"
  },
  {
    "id": "1899",
    "name": "Vercelli",
    "country_id": "107"
  },
  {
    "id": "1900",
    "name": "Verona",
    "country_id": "107"
  },
  {
    "id": "1901",
    "name": "Vicenza",
    "country_id": "107"
  },
  {
    "id": "1902",
    "name": "Viterbo",
    "country_id": "107"
  },
  {
    "id": "1903",
    "name": "Buxoro Viloyati",
    "country_id": "108"
  },
  {
    "id": "1904",
    "name": "Clarendon",
    "country_id": "108"
  },
  {
    "id": "1905",
    "name": "Hanover",
    "country_id": "108"
  },
  {
    "id": "1906",
    "name": "Kingston",
    "country_id": "108"
  },
  {
    "id": "1907",
    "name": "Manchester",
    "country_id": "108"
  },
  {
    "id": "1908",
    "name": "Portland",
    "country_id": "108"
  },
  {
    "id": "1909",
    "name": "Saint Andrews",
    "country_id": "108"
  },
  {
    "id": "1910",
    "name": "Saint Ann",
    "country_id": "108"
  },
  {
    "id": "1911",
    "name": "Saint Catherine",
    "country_id": "108"
  },
  {
    "id": "1912",
    "name": "Saint Elizabeth",
    "country_id": "108"
  },
  {
    "id": "1913",
    "name": "Saint James",
    "country_id": "108"
  },
  {
    "id": "1914",
    "name": "Saint Mary",
    "country_id": "108"
  },
  {
    "id": "1915",
    "name": "Saint Thomas",
    "country_id": "108"
  },
  {
    "id": "1916",
    "name": "Trelawney",
    "country_id": "108"
  },
  {
    "id": "1917",
    "name": "Westmoreland",
    "country_id": "108"
  },
  {
    "id": "1918",
    "name": "Aichi",
    "country_id": "109"
  },
  {
    "id": "1919",
    "name": "Akita",
    "country_id": "109"
  },
  {
    "id": "1920",
    "name": "Aomori",
    "country_id": "109"
  },
  {
    "id": "1921",
    "name": "Chiba",
    "country_id": "109"
  },
  {
    "id": "1922",
    "name": "Ehime",
    "country_id": "109"
  },
  {
    "id": "1923",
    "name": "Fukui",
    "country_id": "109"
  },
  {
    "id": "1924",
    "name": "Fukuoka",
    "country_id": "109"
  },
  {
    "id": "1925",
    "name": "Fukushima",
    "country_id": "109"
  },
  {
    "id": "1926",
    "name": "Gifu",
    "country_id": "109"
  },
  {
    "id": "1927",
    "name": "Gumma",
    "country_id": "109"
  },
  {
    "id": "1928",
    "name": "Hiroshima",
    "country_id": "109"
  },
  {
    "id": "1929",
    "name": "Hokkaido",
    "country_id": "109"
  },
  {
    "id": "1930",
    "name": "Hyogo",
    "country_id": "109"
  },
  {
    "id": "1931",
    "name": "Ibaraki",
    "country_id": "109"
  },
  {
    "id": "1932",
    "name": "Ishikawa",
    "country_id": "109"
  },
  {
    "id": "1933",
    "name": "Iwate",
    "country_id": "109"
  },
  {
    "id": "1934",
    "name": "Kagawa",
    "country_id": "109"
  },
  {
    "id": "1935",
    "name": "Kagoshima",
    "country_id": "109"
  },
  {
    "id": "1936",
    "name": "Kanagawa",
    "country_id": "109"
  },
  {
    "id": "1937",
    "name": "Kanto",
    "country_id": "109"
  },
  {
    "id": "1938",
    "name": "Kochi",
    "country_id": "109"
  },
  {
    "id": "1939",
    "name": "Kumamoto",
    "country_id": "109"
  },
  {
    "id": "1940",
    "name": "Kyoto",
    "country_id": "109"
  },
  {
    "id": "1941",
    "name": "Mie",
    "country_id": "109"
  },
  {
    "id": "1942",
    "name": "Miyagi",
    "country_id": "109"
  },
  {
    "id": "1943",
    "name": "Miyazaki",
    "country_id": "109"
  },
  {
    "id": "1944",
    "name": "Nagano",
    "country_id": "109"
  },
  {
    "id": "1945",
    "name": "Nagasaki",
    "country_id": "109"
  },
  {
    "id": "1946",
    "name": "Nara",
    "country_id": "109"
  },
  {
    "id": "1947",
    "name": "Niigata",
    "country_id": "109"
  },
  {
    "id": "1948",
    "name": "Oita",
    "country_id": "109"
  },
  {
    "id": "1949",
    "name": "Okayama",
    "country_id": "109"
  },
  {
    "id": "1950",
    "name": "Okinawa",
    "country_id": "109"
  },
  {
    "id": "1951",
    "name": "Osaka",
    "country_id": "109"
  },
  {
    "id": "1952",
    "name": "Saga",
    "country_id": "109"
  },
  {
    "id": "1953",
    "name": "Saitama",
    "country_id": "109"
  },
  {
    "id": "1954",
    "name": "Shiga",
    "country_id": "109"
  },
  {
    "id": "1955",
    "name": "Shimane",
    "country_id": "109"
  },
  {
    "id": "1956",
    "name": "Shizuoka",
    "country_id": "109"
  },
  {
    "id": "1957",
    "name": "Tochigi",
    "country_id": "109"
  },
  {
    "id": "1958",
    "name": "Tokushima",
    "country_id": "109"
  },
  {
    "id": "1959",
    "name": "Tokyo",
    "country_id": "109"
  },
  {
    "id": "1960",
    "name": "Tottori",
    "country_id": "109"
  },
  {
    "id": "1961",
    "name": "Toyama",
    "country_id": "109"
  },
  {
    "id": "1962",
    "name": "Wakayama",
    "country_id": "109"
  },
  {
    "id": "1963",
    "name": "Yamagata",
    "country_id": "109"
  },
  {
    "id": "1964",
    "name": "Yamaguchi",
    "country_id": "109"
  },
  {
    "id": "1965",
    "name": "Yamanashi",
    "country_id": "109"
  },
  {
    "id": "1966",
    "name": "Grouville",
    "country_id": "110"
  },
  {
    "id": "1967",
    "name": "Saint Brelade",
    "country_id": "110"
  },
  {
    "id": "1968",
    "name": "Saint Clement",
    "country_id": "110"
  },
  {
    "id": "1969",
    "name": "Saint Helier",
    "country_id": "110"
  },
  {
    "id": "1970",
    "name": "Saint John",
    "country_id": "110"
  },
  {
    "id": "1971",
    "name": "Saint Lawrence",
    "country_id": "110"
  },
  {
    "id": "1972",
    "name": "Saint Martin",
    "country_id": "110"
  },
  {
    "id": "1973",
    "name": "Saint Mary",
    "country_id": "110"
  },
  {
    "id": "1974",
    "name": "Saint Peter",
    "country_id": "110"
  },
  {
    "id": "1975",
    "name": "Saint Saviour",
    "country_id": "110"
  },
  {
    "id": "1976",
    "name": "Trinity",
    "country_id": "110"
  },
  {
    "id": "1977",
    "name": "Ajlun",
    "country_id": "111"
  },
  {
    "id": "1978",
    "name": "Amman",
    "country_id": "111"
  },
  {
    "id": "1979",
    "name": "Irbid",
    "country_id": "111"
  },
  {
    "id": "1980",
    "name": "Jarash",
    "country_id": "111"
  },
  {
    "id": "1981",
    "name": "Ma\'\'an",
    "country_id": "111"
  },
  {
    "id": "1982",
    "name": "Madaba",
    "country_id": "111"
  },
  {
    "id": "1983",
    "name": "al-\'\'Aqabah",
    "country_id": "111"
  },
  {
    "id": "1984",
    "name": "al-Balqa",
    "country_id": "111"
  },
  {
    "id": "1985",
    "name": "al-Karak",
    "country_id": "111"
  },
  {
    "id": "1986",
    "name": "al-Mafraq",
    "country_id": "111"
  },
  {
    "id": "1987",
    "name": "at-Tafilah",
    "country_id": "111"
  },
  {
    "id": "1988",
    "name": "az-Zarqa",
    "country_id": "111"
  },
  {
    "id": "1989",
    "name": "Akmecet",
    "country_id": "112"
  },
  {
    "id": "1990",
    "name": "Akmola",
    "country_id": "112"
  },
  {
    "id": "1991",
    "name": "Aktobe",
    "country_id": "112"
  },
  {
    "id": "1992",
    "name": "Almati",
    "country_id": "112"
  },
  {
    "id": "1993",
    "name": "Atirau",
    "country_id": "112"
  },
  {
    "id": "1994",
    "name": "Batis Kazakstan",
    "country_id": "112"
  },
  {
    "id": "1995",
    "name": "Burlinsky Region",
    "country_id": "112"
  },
  {
    "id": "1996",
    "name": "Karagandi",
    "country_id": "112"
  },
  {
    "id": "1997",
    "name": "Kostanay",
    "country_id": "112"
  },
  {
    "id": "1998",
    "name": "Mankistau",
    "country_id": "112"
  },
  {
    "id": "1999",
    "name": "Ontustik Kazakstan",
    "country_id": "112"
  },
  {
    "id": "2000",
    "name": "Pavlodar",
    "country_id": "112"
  },
  {
    "id": "2001",
    "name": "Sigis Kazakstan",
    "country_id": "112"
  },
  {
    "id": "2002",
    "name": "Soltustik Kazakstan",
    "country_id": "112"
  },
  {
    "id": "2003",
    "name": "Taraz",
    "country_id": "112"
  },
  {
    "id": "2004",
    "name": "Central",
    "country_id": "113"
  }]);