const models = require('../models/index');


  models.State.bulkCreate([{
    "id": "1696",
    "name": "Sumatera Barat",
    "country_id": "102"
  },
  {
    "id": "1697",
    "name": "Sumatera Selatan",
    "country_id": "102"
  },
  {
    "id": "1698",
    "name": "Sumatera Utara",
    "country_id": "102"
  },
  {
    "id": "1699",
    "name": "Yogyakarta",
    "country_id": "102"
  },
  {
    "id": "1700",
    "name": "Ardabil",
    "country_id": "103"
  },
  {
    "id": "1701",
    "name": "Azarbayjan-e Bakhtari",
    "country_id": "103"
  },
  {
    "id": "1702",
    "name": "Azarbayjan-e Khavari",
    "country_id": "103"
  },
  {
    "id": "1703",
    "name": "Bushehr",
    "country_id": "103"
  },
  {
    "id": "1704",
    "name": "Chahar Mahal-e Bakhtiari",
    "country_id": "103"
  },
  {
    "id": "1705",
    "name": "Esfahan",
    "country_id": "103"
  },
  {
    "id": "1706",
    "name": "Fars",
    "country_id": "103"
  },
  {
    "id": "1707",
    "name": "Gilan",
    "country_id": "103"
  },
  {
    "id": "1708",
    "name": "Golestan",
    "country_id": "103"
  },
  {
    "id": "1709",
    "name": "Hamadan",
    "country_id": "103"
  },
  {
    "id": "1710",
    "name": "Hormozgan",
    "country_id": "103"
  },
  {
    "id": "1711",
    "name": "Ilam",
    "country_id": "103"
  },
  {
    "id": "1712",
    "name": "Kerman",
    "country_id": "103"
  },
  {
    "id": "1713",
    "name": "Kermanshah",
    "country_id": "103"
  },
  {
    "id": "1714",
    "name": "Khorasan",
    "country_id": "103"
  },
  {
    "id": "1715",
    "name": "Khuzestan",
    "country_id": "103"
  },
  {
    "id": "1716",
    "name": "Kohgiluyeh-e Boyerahmad",
    "country_id": "103"
  },
  {
    "id": "1717",
    "name": "Kordestan",
    "country_id": "103"
  },
  {
    "id": "1718",
    "name": "Lorestan",
    "country_id": "103"
  },
  {
    "id": "1719",
    "name": "Markazi",
    "country_id": "103"
  },
  {
    "id": "1720",
    "name": "Mazandaran",
    "country_id": "103"
  },
  {
    "id": "1721",
    "name": "Ostan-e Esfahan",
    "country_id": "103"
  },
  {
    "id": "1722",
    "name": "Qazvin",
    "country_id": "103"
  },
  {
    "id": "1723",
    "name": "Qom",
    "country_id": "103"
  },
  {
    "id": "1724",
    "name": "Semnan",
    "country_id": "103"
  },
  {
    "id": "1725",
    "name": "Sistan-e Baluchestan",
    "country_id": "103"
  },
  {
    "id": "1726",
    "name": "Tehran",
    "country_id": "103"
  },
  {
    "id": "1727",
    "name": "Yazd",
    "country_id": "103"
  },
  {
    "id": "1728",
    "name": "Zanjan",
    "country_id": "103"
  },
  {
    "id": "1729",
    "name": "Babil",
    "country_id": "104"
  },
  {
    "id": "1730",
    "name": "Baghdad",
    "country_id": "104"
  },
  {
    "id": "1731",
    "name": "Dahuk",
    "country_id": "104"
  },
  {
    "id": "1732",
    "name": "Dhi Qar",
    "country_id": "104"
  },
  {
    "id": "1733",
    "name": "Diyala",
    "country_id": "104"
  },
  {
    "id": "1734",
    "name": "Erbil",
    "country_id": "104"
  },
  {
    "id": "1735",
    "name": "Irbil",
    "country_id": "104"
  },
  {
    "id": "1736",
    "name": "Karbala",
    "country_id": "104"
  },
  {
    "id": "1737",
    "name": "Kurdistan",
    "country_id": "104"
  },
  {
    "id": "1738",
    "name": "Maysan",
    "country_id": "104"
  },
  {
    "id": "1739",
    "name": "Ninawa",
    "country_id": "104"
  },
  {
    "id": "1740",
    "name": "Salah-ad-Din",
    "country_id": "104"
  },
  {
    "id": "1741",
    "name": "Wasit",
    "country_id": "104"
  },
  {
    "id": "1742",
    "name": "al-Anbar",
    "country_id": "104"
  },
  {
    "id": "1743",
    "name": "al-Basrah",
    "country_id": "104"
  },
  {
    "id": "1744",
    "name": "al-Muthanna",
    "country_id": "104"
  },
  {
    "id": "1745",
    "name": "al-Qadisiyah",
    "country_id": "104"
  },
  {
    "id": "1746",
    "name": "an-Najaf",
    "country_id": "104"
  },
  {
    "id": "1747",
    "name": "as-Sulaymaniyah",
    "country_id": "104"
  },
  {
    "id": "1748",
    "name": "at-Ta\'\'mim",
    "country_id": "104"
  },
  {
    "id": "1749",
    "name": "Armagh",
    "country_id": "105"
  },
  {
    "id": "1750",
    "name": "Carlow",
    "country_id": "105"
  },
  {
    "id": "1751",
    "name": "Cavan",
    "country_id": "105"
  },
  {
    "id": "1752",
    "name": "Clare",
    "country_id": "105"
  },
  {
    "id": "1753",
    "name": "Cork",
    "country_id": "105"
  },
  {
    "id": "1754",
    "name": "Donegal",
    "country_id": "105"
  },
  {
    "id": "1755",
    "name": "Dublin",
    "country_id": "105"
  },
  {
    "id": "1756",
    "name": "Galway",
    "country_id": "105"
  },
  {
    "id": "1757",
    "name": "Kerry",
    "country_id": "105"
  },
  {
    "id": "1758",
    "name": "Kildare",
    "country_id": "105"
  },
  {
    "id": "1759",
    "name": "Kilkenny",
    "country_id": "105"
  },
  {
    "id": "1760",
    "name": "Laois",
    "country_id": "105"
  },
  {
    "id": "1761",
    "name": "Leinster",
    "country_id": "105"
  },
  {
    "id": "1762",
    "name": "Leitrim",
    "country_id": "105"
  },
  {
    "id": "1763",
    "name": "Limerick",
    "country_id": "105"
  },
  {
    "id": "1764",
    "name": "Loch Garman",
    "country_id": "105"
  },
  {
    "id": "1765",
    "name": "Longford",
    "country_id": "105"
  },
  {
    "id": "1766",
    "name": "Louth",
    "country_id": "105"
  },
  {
    "id": "1767",
    "name": "Mayo",
    "country_id": "105"
  },
  {
    "id": "1768",
    "name": "Meath",
    "country_id": "105"
  },
  {
    "id": "1769",
    "name": "Monaghan",
    "country_id": "105"
  },
  {
    "id": "1770",
    "name": "Offaly",
    "country_id": "105"
  },
  {
    "id": "1771",
    "name": "Roscommon",
    "country_id": "105"
  },
  {
    "id": "1772",
    "name": "Sligo",
    "country_id": "105"
  },
  {
    "id": "1773",
    "name": "Tipperary North Riding",
    "country_id": "105"
  },
  {
    "id": "1774",
    "name": "Tipperary South Riding",
    "country_id": "105"
  },
  {
    "id": "1775",
    "name": "Ulster",
    "country_id": "105"
  },
  {
    "id": "1776",
    "name": "Waterford",
    "country_id": "105"
  },
  {
    "id": "1777",
    "name": "Westmeath",
    "country_id": "105"
  },
  {
    "id": "1778",
    "name": "Wexford",
    "country_id": "105"
  },
  {
    "id": "1779",
    "name": "Wicklow",
    "country_id": "105"
  },
  {
    "id": "1780",
    "name": "Beit Hanania",
    "country_id": "106"
  },
  {
    "id": "1781",
    "name": "Ben Gurion Airport",
    "country_id": "106"
  },
  {
    "id": "1782",
    "name": "Bethlehem",
    "country_id": "106"
  },
  {
    "id": "1783",
    "name": "Caesarea",
    "country_id": "106"
  },
  {
    "id": "1784",
    "name": "Centre",
    "country_id": "106"
  },
  {
    "id": "1785",
    "name": "Gaza",
    "country_id": "106"
  },
  {
    "id": "1786",
    "name": "Hadaron",
    "country_id": "106"
  },
  {
    "id": "1787",
    "name": "Haifa District",
    "country_id": "106"
  },
  {
    "id": "1788",
    "name": "Hamerkaz",
    "country_id": "106"
  },
  {
    "id": "1789",
    "name": "Hazafon",
    "country_id": "106"
  },
  {
    "id": "1790",
    "name": "Hebron",
    "country_id": "106"
  },
  {
    "id": "1791",
    "name": "Jaffa",
    "country_id": "106"
  },
  {
    "id": "1792",
    "name": "Jerusalem",
    "country_id": "106"
  },
  {
    "id": "1793",
    "name": "Khefa",
    "country_id": "106"
  },
  {
    "id": "1794",
    "name": "Kiryat Yam",
    "country_id": "106"
  },
  {
    "id": "1795",
    "name": "Lower Galilee",
    "country_id": "106"
  },
  {
    "id": "1796",
    "name": "Qalqilya",
    "country_id": "106"
  },
  {
    "id": "1797",
    "name": "Talme Elazar",
    "country_id": "106"
  },
  {
    "id": "1798",
    "name": "Tel Aviv",
    "country_id": "106"
  },
  {
    "id": "1799",
    "name": "Tsafon",
    "country_id": "106"
  },
  {
    "id": "1800",
    "name": "Umm El Fahem",
    "country_id": "106"
  },
  {
    "id": "1801",
    "name": "Yerushalayim",
    "country_id": "106"
  },
  {
    "id": "1802",
    "name": "Abruzzi",
    "country_id": "107"
  },
  {
    "id": "1803",
    "name": "Abruzzo",
    "country_id": "107"
  },
  {
    "id": "1804",
    "name": "Agrigento",
    "country_id": "107"
  },
  {
    "id": "1805",
    "name": "Alessandria",
    "country_id": "107"
  },
  {
    "id": "1806",
    "name": "Ancona",
    "country_id": "107"
  },
  {
    "id": "1807",
    "name": "Arezzo",
    "country_id": "107"
  },
  {
    "id": "1808",
    "name": "Ascoli Piceno",
    "country_id": "107"
  },
  {
    "id": "1809",
    "name": "Asti",
    "country_id": "107"
  },
  {
    "id": "1810",
    "name": "Avellino",
    "country_id": "107"
  },
  {
    "id": "1811",
    "name": "Bari",
    "country_id": "107"
  },
  {
    "id": "1812",
    "name": "Basilicata",
    "country_id": "107"
  },
  {
    "id": "1813",
    "name": "Belluno",
    "country_id": "107"
  },
  {
    "id": "1814",
    "name": "Benevento",
    "country_id": "107"
  },
  {
    "id": "1815",
    "name": "Bergamo",
    "country_id": "107"
  },
  {
    "id": "1816",
    "name": "Biella",
    "country_id": "107"
  },
  {
    "id": "1817",
    "name": "Bologna",
    "country_id": "107"
  },
  {
    "id": "1818",
    "name": "Bolzano",
    "country_id": "107"
  },
  {
    "id": "1819",
    "name": "Brescia",
    "country_id": "107"
  },
  {
    "id": "1820",
    "name": "Brindisi",
    "country_id": "107"
  },
  {
    "id": "1821",
    "name": "Calabria",
    "country_id": "107"
  },
  {
    "id": "1822",
    "name": "Campania",
    "country_id": "107"
  },
  {
    "id": "1823",
    "name": "Cartoceto",
    "country_id": "107"
  },
  {
    "id": "1824",
    "name": "Caserta",
    "country_id": "107"
  },
  {
    "id": "1825",
    "name": "Catania",
    "country_id": "107"
  },
  {
    "id": "1826",
    "name": "Chieti",
    "country_id": "107"
  },
  {
    "id": "1827",
    "name": "Como",
    "country_id": "107"
  },
  {
    "id": "1828",
    "name": "Cosenza",
    "country_id": "107"
  },
  {
    "id": "1829",
    "name": "Cremona",
    "country_id": "107"
  },
  {
    "id": "1830",
    "name": "Cuneo",
    "country_id": "107"
  },
  {
    "id": "1831",
    "name": "Emilia-Romagna",
    "country_id": "107"
  },
  {
    "id": "1832",
    "name": "Ferrara",
    "country_id": "107"
  },
  {
    "id": "1833",
    "name": "Firenze",
    "country_id": "107"
  },
  {
    "id": "1834",
    "name": "Florence",
    "country_id": "107"
  },
  {
    "id": "1835",
    "name": "Forli-Cesena",
    "country_id": "107"
  },
  {
    "id": "1836",
    "name": "Friuli-Venezia Giulia",
    "country_id": "107"
  },
  {
    "id": "1837",
    "name": "Frosinone",
    "country_id": "107"
  },
  {
    "id": "1838",
    "name": "Genoa",
    "country_id": "107"
  },
  {
    "id": "1839",
    "name": "Gorizia",
    "country_id": "107"
  },
  {
    "id": "1840",
    "name": "L\'\'Aquila",
    "country_id": "107"
  },
  {
    "id": "1841",
    "name": "Lazio",
    "country_id": "107"
  },
  ]);