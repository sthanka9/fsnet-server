const models = require('../models/index');
const pmodels = require('../production_models'); 
const _ = require('lodash');

//Update only one existing record firstname='Clayton' , lpId=69
//update [ChangeCommitments] set isPriorAmount=0 where id=42
// insert into [ChangeCommitments] values(29,113,150000.00,'',0,null,'2019-05-01 18:34:09.0810000 +00:00','2019-05-01 18:34:09.0810000 +00:00',1)
//updating the prior commitments

pmodels.prod_sequelize.query("select min(id) as id FROM [dbo].[ChangeCommitments] group by fundId,subscriptionId", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async changeCommitments => { 
    
    changeCommitments.forEach(changeCommitment => {
        models.ChangeCommitment.update({
            isPriorAmount:1
        },{
            where:{
                id:changeCommitment.id
            }
        })
    });
  
})
