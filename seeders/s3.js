const models = require('../models/index');

models.State.bulkCreate([{
    "id": "2005",
    "name": "Coast",
    "country_id": "113"
  },
  {
    "id": "2006",
    "name": "Eastern",
    "country_id": "113"
  },
  {
    "id": "2007",
    "name": "Nairobi",
    "country_id": "113"
  },
  {
    "id": "2008",
    "name": "North Eastern",
    "country_id": "113"
  },
  {
    "id": "2009",
    "name": "Nyanza",
    "country_id": "113"
  },
  {
    "id": "2010",
    "name": "Rift Valley",
    "country_id": "113"
  },
  {
    "id": "2011",
    "name": "Western",
    "country_id": "113"
  },
  {
    "id": "2012",
    "name": "Abaiang",
    "country_id": "114"
  },
  {
    "id": "2013",
    "name": "Abemana",
    "country_id": "114"
  },
  {
    "id": "2014",
    "name": "Aranuka",
    "country_id": "114"
  },
  {
    "id": "2015",
    "name": "Arorae",
    "country_id": "114"
  },
  {
    "id": "2016",
    "name": "Banaba",
    "country_id": "114"
  },
  {
    "id": "2017",
    "name": "Beru",
    "country_id": "114"
  },
  {
    "id": "2018",
    "name": "Butaritari",
    "country_id": "114"
  },
  {
    "id": "2019",
    "name": "Kiritimati",
    "country_id": "114"
  },
  {
    "id": "2020",
    "name": "Kuria",
    "country_id": "114"
  },
  {
    "id": "2021",
    "name": "Maiana",
    "country_id": "114"
  },
  {
    "id": "2022",
    "name": "Makin",
    "country_id": "114"
  },
  {
    "id": "2023",
    "name": "Marakei",
    "country_id": "114"
  },
  {
    "id": "2024",
    "name": "Nikunau",
    "country_id": "114"
  },
  {
    "id": "2025",
    "name": "Nonouti",
    "country_id": "114"
  },
  {
    "id": "2026",
    "name": "Onotoa",
    "country_id": "114"
  },
  {
    "id": "2027",
    "name": "Phoenix Islands",
    "country_id": "114"
  },
  {
    "id": "2028",
    "name": "Tabiteuea North",
    "country_id": "114"
  },
  {
    "id": "2029",
    "name": "Tabiteuea South",
    "country_id": "114"
  },
  {
    "id": "2030",
    "name": "Tabuaeran",
    "country_id": "114"
  },
  {
    "id": "2031",
    "name": "Tamana",
    "country_id": "114"
  },
  {
    "id": "2032",
    "name": "Tarawa North",
    "country_id": "114"
  },
  {
    "id": "2033",
    "name": "Tarawa South",
    "country_id": "114"
  },
  {
    "id": "2034",
    "name": "Teraina",
    "country_id": "114"
  },
  {
    "id": "2035",
    "name": "Chagangdo",
    "country_id": "115"
  },
  {
    "id": "2036",
    "name": "Hamgyeongbukto",
    "country_id": "115"
  },
  {
    "id": "2037",
    "name": "Hamgyeongnamdo",
    "country_id": "115"
  },
  {
    "id": "2038",
    "name": "Hwanghaebukto",
    "country_id": "115"
  },
  {
    "id": "2039",
    "name": "Hwanghaenamdo",
    "country_id": "115"
  },
  {
    "id": "2040",
    "name": "Kaeseong",
    "country_id": "115"
  },
  {
    "id": "2041",
    "name": "Kangweon",
    "country_id": "115"
  },
  {
    "id": "2042",
    "name": "Nampo",
    "country_id": "115"
  },
  {
    "id": "2043",
    "name": "Pyeonganbukto",
    "country_id": "115"
  },
  {
    "id": "2044",
    "name": "Pyeongannamdo",
    "country_id": "115"
  },
  {
    "id": "2045",
    "name": "Pyeongyang",
    "country_id": "115"
  },
  {
    "id": "2046",
    "name": "Yanggang",
    "country_id": "115"
  },
  {
    "id": "2047",
    "name": "Busan",
    "country_id": "116"
  },
  {
    "id": "2048",
    "name": "Cheju",
    "country_id": "116"
  },
  {
    "id": "2049",
    "name": "Chollabuk",
    "country_id": "116"
  },
  {
    "id": "2050",
    "name": "Chollanam",
    "country_id": "116"
  },
  {
    "id": "2051",
    "name": "Chungbuk",
    "country_id": "116"
  },
  {
    "id": "2052",
    "name": "Chungcheongbuk",
    "country_id": "116"
  },
  {
    "id": "2053",
    "name": "Chungcheongnam",
    "country_id": "116"
  },
  {
    "id": "2054",
    "name": "Chungnam",
    "country_id": "116"
  },
  {
    "id": "2055",
    "name": "Daegu",
    "country_id": "116"
  },
  {
    "id": "2056",
    "name": "Gangwon-do",
    "country_id": "116"
  },
  {
    "id": "2057",
    "name": "Goyang-si",
    "country_id": "116"
  },
  {
    "id": "2058",
    "name": "Gyeonggi-do",
    "country_id": "116"
  },
  {
    "id": "2059",
    "name": "Gyeongsang",
    "country_id": "116"
  },
  {
    "id": "2060",
    "name": "Gyeongsangnam-do",
    "country_id": "116"
  },
  {
    "id": "2061",
    "name": "Incheon",
    "country_id": "116"
  },
  {
    "id": "2062",
    "name": "Jeju-Si",
    "country_id": "116"
  },
  {
    "id": "2063",
    "name": "Jeonbuk",
    "country_id": "116"
  },
  {
    "id": "2064",
    "name": "Kangweon",
    "country_id": "116"
  },
  {
    "id": "2065",
    "name": "Kwangju",
    "country_id": "116"
  },
  {
    "id": "2066",
    "name": "Kyeonggi",
    "country_id": "116"
  },
  {
    "id": "2067",
    "name": "Kyeongsangbuk",
    "country_id": "116"
  },
  {
    "id": "2068",
    "name": "Kyeongsangnam",
    "country_id": "116"
  },
  {
    "id": "2069",
    "name": "Kyonggi-do",
    "country_id": "116"
  },
  {
    "id": "2070",
    "name": "Kyungbuk-Do",
    "country_id": "116"
  },
  {
    "id": "2071",
    "name": "Kyunggi-Do",
    "country_id": "116"
  },
  {
    "id": "2072",
    "name": "Kyunggi-do",
    "country_id": "116"
  },
  {
    "id": "2073",
    "name": "Pusan",
    "country_id": "116"
  },
  {
    "id": "2074",
    "name": "Seoul",
    "country_id": "116"
  },
  {
    "id": "2075",
    "name": "Sudogwon",
    "country_id": "116"
  },
  {
    "id": "2076",
    "name": "Taegu",
    "country_id": "116"
  },
  {
    "id": "2077",
    "name": "Taejeon",
    "country_id": "116"
  },
  {
    "id": "2078",
    "name": "Taejon-gwangyoksi",
    "country_id": "116"
  },
  {
    "id": "2079",
    "name": "Ulsan",
    "country_id": "116"
  },
  {
    "id": "2080",
    "name": "Wonju",
    "country_id": "116"
  },
  {
    "id": "2081",
    "name": "gwangyoksi",
    "country_id": "116"
  },
  {
    "id": "2082",
    "name": "Al Asimah",
    "country_id": "117"
  },
  {
    "id": "2083",
    "name": "Hawalli",
    "country_id": "117"
  },
  {
    "id": "2084",
    "name": "Mishref",
    "country_id": "117"
  },
  {
    "id": "2085",
    "name": "Qadesiya",
    "country_id": "117"
  },
  {
    "id": "2086",
    "name": "Safat",
    "country_id": "117"
  },
  {
    "id": "2087",
    "name": "Salmiya",
    "country_id": "117"
  },
  {
    "id": "2088",
    "name": "al-Ahmadi",
    "country_id": "117"
  },
  {
    "id": "2089",
    "name": "al-Farwaniyah",
    "country_id": "117"
  },
  {
    "id": "2090",
    "name": "al-Jahra",
    "country_id": "117"
  },
  {
    "id": "2091",
    "name": "al-Kuwayt",
    "country_id": "117"
  },
  {
    "id": "2092",
    "name": "Batken",
    "country_id": "118"
  },
  {
    "id": "2093",
    "name": "Bishkek",
    "country_id": "118"
  },
  {
    "id": "2094",
    "name": "Chui",
    "country_id": "118"
  },
  {
    "id": "2095",
    "name": "Issyk-Kul",
    "country_id": "118"
  },
  {
    "id": "2096",
    "name": "Jalal-Abad",
    "country_id": "118"
  },
  {
    "id": "2097",
    "name": "Naryn",
    "country_id": "118"
  },
  {
    "id": "2098",
    "name": "Osh",
    "country_id": "118"
  },
  {
    "id": "2099",
    "name": "Talas",
    "country_id": "118"
  },
  {
    "id": "2100",
    "name": "Attopu",
    "country_id": "119"
  },
  {
    "id": "2101",
    "name": "Bokeo",
    "country_id": "119"
  },
  {
    "id": "2102",
    "name": "Bolikhamsay",
    "country_id": "119"
  },
  {
    "id": "2103",
    "name": "Champasak",
    "country_id": "119"
  },
  {
    "id": "2104",
    "name": "Houaphanh",
    "country_id": "119"
  },
  {
    "id": "2105",
    "name": "Khammouane",
    "country_id": "119"
  },
  {
    "id": "2106",
    "name": "Luang Nam Tha",
    "country_id": "119"
  },
  {
    "id": "2107",
    "name": "Luang Prabang",
    "country_id": "119"
  },
  {
    "id": "2108",
    "name": "Oudomxay",
    "country_id": "119"
  },
  {
    "id": "2109",
    "name": "Phongsaly",
    "country_id": "119"
  },
  {
    "id": "2110",
    "name": "Saravan",
    "country_id": "119"
  },
  {
    "id": "2111",
    "name": "Savannakhet",
    "country_id": "119"
  },
  {
    "id": "2112",
    "name": "Sekong",
    "country_id": "119"
  },
  {
    "id": "2113",
    "name": "Viangchan Prefecture",
    "country_id": "119"
  },
  {
    "id": "2114",
    "name": "Viangchan Province",
    "country_id": "119"
  },
  {
    "id": "2115",
    "name": "Xaignabury",
    "country_id": "119"
  },
  {
    "id": "2116",
    "name": "Xiang Khuang",
    "country_id": "119"
  },
  {
    "id": "2117",
    "name": "Aizkraukles",
    "country_id": "120"
  },
  {
    "id": "2118",
    "name": "Aluksnes",
    "country_id": "120"
  },
  {
    "id": "2119",
    "name": "Balvu",
    "country_id": "120"
  },
  {
    "id": "2120",
    "name": "Bauskas",
    "country_id": "120"
  },
  {
    "id": "2121",
    "name": "Cesu",
    "country_id": "120"
  },
  {
    "id": "2122",
    "name": "Daugavpils",
    "country_id": "120"
  },
  {
    "id": "2123",
    "name": "Daugavpils City",
    "country_id": "120"
  },
  {
    "id": "2124",
    "name": "Dobeles",
    "country_id": "120"
  },
  {
    "id": "2125",
    "name": "Gulbenes",
    "country_id": "120"
  },
  {
    "id": "2126",
    "name": "Jekabspils",
    "country_id": "120"
  },
  {
    "id": "2127",
    "name": "Jelgava",
    "country_id": "120"
  },
  {
    "id": "2128",
    "name": "Jelgavas",
    "country_id": "120"
  },
  {
    "id": "2129",
    "name": "Jurmala City",
    "country_id": "120"
  },
  {
    "id": "2130",
    "name": "Kraslavas",
    "country_id": "120"
  },
  {
    "id": "2131",
    "name": "Kuldigas",
    "country_id": "120"
  },
  {
    "id": "2132",
    "name": "Liepaja",
    "country_id": "120"
  },
  {
    "id": "2133",
    "name": "Liepajas",
    "country_id": "120"
  },
  {
    "id": "2134",
    "name": "Limbazhu",
    "country_id": "120"
  },
  {
    "id": "2135",
    "name": "Ludzas",
    "country_id": "120"
  },
  {
    "id": "2136",
    "name": "Madonas",
    "country_id": "120"
  },
  {
    "id": "2137",
    "name": "Ogres",
    "country_id": "120"
  },
  {
    "id": "2138",
    "name": "Preilu",
    "country_id": "120"
  },
  {
    "id": "2139",
    "name": "Rezekne",
    "country_id": "120"
  },
  {
    "id": "2140",
    "name": "Rezeknes",
    "country_id": "120"
  },
  {
    "id": "2141",
    "name": "Riga",
    "country_id": "120"
  },
  {
    "id": "2142",
    "name": "Rigas",
    "country_id": "120"
  },
  {
    "id": "2143",
    "name": "Saldus",
    "country_id": "120"
  },
  {
    "id": "2144",
    "name": "Talsu",
    "country_id": "120"
  },
  {
    "id": "2145",
    "name": "Tukuma",
    "country_id": "120"
  },
  {
    "id": "2146",
    "name": "Valkas",
    "country_id": "120"
  },
  {
    "id": "2147",
    "name": "Valmieras",
    "country_id": "120"
  },
  {
    "id": "2148",
    "name": "Ventspils",
    "country_id": "120"
  },
  {
    "id": "2149",
    "name": "Ventspils City",
    "country_id": "120"
  },
  {
    "id": "2150",
    "name": "Beirut",
    "country_id": "121"
  },
  {
    "id": "2151",
    "name": "Jabal Lubnan",
    "country_id": "121"
  },
  {
    "id": "2152",
    "name": "Mohafazat Liban-Nord",
    "country_id": "121"
  },
  {
    "id": "2153",
    "name": "Mohafazat Mont-Liban",
    "country_id": "121"
  },
  {
    "id": "2154",
    "name": "Sidon",
    "country_id": "121"
  },
  {
    "id": "2155",
    "name": "al-Biqa",
    "country_id": "121"
  },
  {
    "id": "2156",
    "name": "al-Janub",
    "country_id": "121"
  },
  {
    "id": "2157",
    "name": "an-Nabatiyah",
    "country_id": "121"
  },
  {
    "id": "2158",
    "name": "ash-Shamal",
    "country_id": "121"
  },
  {
    "id": "2159",
    "name": "Berea",
    "country_id": "122"
  },
  {
    "id": "2160",
    "name": "Butha-Buthe",
    "country_id": "122"
  },
  {
    "id": "2161",
    "name": "Leribe",
    "country_id": "122"
  },
  {
    "id": "2162",
    "name": "Mafeteng",
    "country_id": "122"
  },
  {
    "id": "2163",
    "name": "Maseru",
    "country_id": "122"
  },
  {
    "id": "2164",
    "name": "Mohale\'\'s Hoek",
    "country_id": "122"
  },
  {
    "id": "2165",
    "name": "Mokhotlong",
    "country_id": "122"
  },
  {
    "id": "2166",
    "name": "Qacha\'\'s Nek",
    "country_id": "122"
  },
  {
    "id": "2167",
    "name": "Quthing",
    "country_id": "122"
  },
  {
    "id": "2168",
    "name": "Thaba-Tseka",
    "country_id": "122"
  },
  {
    "id": "2169",
    "name": "Bomi",
    "country_id": "123"
  },
  {
    "id": "2170",
    "name": "Bong",
    "country_id": "123"
  },
  {
    "id": "2171",
    "name": "Grand Bassa",
    "country_id": "123"
  },
  {
    "id": "2172",
    "name": "Grand Cape Mount",
    "country_id": "123"
  },
  {
    "id": "2173",
    "name": "Grand Gedeh",
    "country_id": "123"
  },
  {
    "id": "2174",
    "name": "Loffa",
    "country_id": "123"
  },
  {
    "id": "2175",
    "name": "Margibi",
    "country_id": "123"
  },
  {
    "id": "2176",
    "name": "Maryland and Grand Kru",
    "country_id": "123"
  },
  {
    "id": "2177",
    "name": "Montserrado",
    "country_id": "123"
  },
  {
    "id": "2178",
    "name": "Nimba",
    "country_id": "123"
  },
  {
    "id": "2179",
    "name": "Rivercess",
    "country_id": "123"
  },
  {
    "id": "2180",
    "name": "Sinoe",
    "country_id": "123"
  },
  {
    "id": "2181",
    "name": "Ajdabiya",
    "country_id": "124"
  },
  {
    "id": "2182",
    "name": "Fezzan",
    "country_id": "124"
  },
  {
    "id": "2183",
    "name": "Banghazi",
    "country_id": "124"
  },
  {
    "id": "2184",
    "name": "Darnah",
    "country_id": "124"
  },
  {
    "id": "2185",
    "name": "Ghadamis",
    "country_id": "124"
  },
  {
    "id": "2186",
    "name": "Gharyan",
    "country_id": "124"
  },
  {
    "id": "2187",
    "name": "Misratah",
    "country_id": "124"
  },
  {
    "id": "2188",
    "name": "Murzuq",
    "country_id": "124"
  },
  {
    "id": "2189",
    "name": "Sabha",
    "country_id": "124"
  },
  {
    "id": "2190",
    "name": "Sawfajjin",
    "country_id": "124"
  },
  {
    "id": "2191",
    "name": "Surt",
    "country_id": "124"
  },
  {
    "id": "2192",
    "name": "Tarabulus",
    "country_id": "124"
  },
  {
    "id": "2193",
    "name": "Tarhunah",
    "country_id": "124"
  },
  {
    "id": "2194",
    "name": "Tripolitania",
    "country_id": "124"
  },
  {
    "id": "2195",
    "name": "Tubruq",
    "country_id": "124"
  },
  {
    "id": "2196",
    "name": "Yafran",
    "country_id": "124"
  },
  {
    "id": "2197",
    "name": "Zlitan",
    "country_id": "124"
  },
  {
    "id": "2198",
    "name": "al-\'\'Aziziyah",
    "country_id": "124"
  },
  {
    "id": "2199",
    "name": "al-Fatih",
    "country_id": "124"
  },
  {
    "id": "2200",
    "name": "al-Jabal al Akhdar",
    "country_id": "124"
  },
  {
    "id": "2201",
    "name": "al-Jufrah",
    "country_id": "124"
  },
  {
    "id": "2202",
    "name": "al-Khums",
    "country_id": "124"
  },
  {
    "id": "2203",
    "name": "al-Kufrah",
    "country_id": "124"
  },
  {
    "id": "2204",
    "name": "an-Nuqat al-Khams",
    "country_id": "124"
  },
  {
    "id": "2205",
    "name": "ash-Shati",
    "country_id": "124"
  },
  {
    "id": "2206",
    "name": "az-Zawiyah",
    "country_id": "124"
  },
  {
    "id": "2207",
    "name": "Balzers",
    "country_id": "125"
  },
  {
    "id": "2208",
    "name": "Eschen",
    "country_id": "125"
  },
  {
    "id": "2209",
    "name": "Gamprin",
    "country_id": "125"
  },
  {
    "id": "2210",
    "name": "Mauren",
    "country_id": "125"
  },
  {
    "id": "2211",
    "name": "Planken",
    "country_id": "125"
  },
  {
    "id": "2212",
    "name": "Ruggell",
    "country_id": "125"
  },
  {
    "id": "2213",
    "name": "Schaan",
    "country_id": "125"
  },
  {
    "id": "2214",
    "name": "Schellenberg",
    "country_id": "125"
  },
  {
    "id": "2215",
    "name": "Triesen",
    "country_id": "125"
  },
  {
    "id": "2216",
    "name": "Triesenberg",
    "country_id": "125"
  },
  {
    "id": "2217",
    "name": "Vaduz",
    "country_id": "125"
  },
  {
    "id": "2218",
    "name": "Alytaus",
    "country_id": "126"
  },
  {
    "id": "2219",
    "name": "Anyksciai",
    "country_id": "126"
  },
  {
    "id": "2220",
    "name": "Kauno",
    "country_id": "126"
  },
  {
    "id": "2221",
    "name": "Klaipedos",
    "country_id": "126"
  },
  {
    "id": "2222",
    "name": "Marijampoles",
    "country_id": "126"
  },
  {
    "id": "2223",
    "name": "Panevezhio",
    "country_id": "126"
  },
  {
    "id": "2224",
    "name": "Panevezys",
    "country_id": "126"
  },
  {
    "id": "2225",
    "name": "Shiauliu",
    "country_id": "126"
  },
  {
    "id": "2226",
    "name": "Taurages",
    "country_id": "126"
  },
  {
    "id": "2227",
    "name": "Telshiu",
    "country_id": "126"
  },
  {
    "id": "2228",
    "name": "Telsiai",
    "country_id": "126"
  },
  {
    "id": "2229",
    "name": "Utenos",
    "country_id": "126"
  },
  {
    "id": "2230",
    "name": "Vilniaus",
    "country_id": "126"
  },
  {
    "id": "2231",
    "name": "Capellen",
    "country_id": "127"
  },
  {
    "id": "2232",
    "name": "Clervaux",
    "country_id": "127"
  },
  {
    "id": "2233",
    "name": "Diekirch",
    "country_id": "127"
  },
  {
    "id": "2234",
    "name": "Echternach",
    "country_id": "127"
  },
  {
    "id": "2235",
    "name": "Esch-sur-Alzette",
    "country_id": "127"
  },
  {
    "id": "2236",
    "name": "Grevenmacher",
    "country_id": "127"
  },
  {
    "id": "2237",
    "name": "Luxembourg",
    "country_id": "127"
  },
  {
    "id": "2238",
    "name": "Mersch",
    "country_id": "127"
  },
  {
    "id": "2239",
    "name": "Redange",
    "country_id": "127"
  },
  {
    "id": "2240",
    "name": "Remich",
    "country_id": "127"
  },
  {
    "id": "2241",
    "name": "Vianden",
    "country_id": "127"
  },
  {
    "id": "2242",
    "name": "Wiltz",
    "country_id": "127"
  },
  {
    "id": "2243",
    "name": "Macau",
    "country_id": "128"
  },
  {
    "id": "2244",
    "name": "Berovo",
    "country_id": "129"
  },
  {
    "id": "2245",
    "name": "Bitola",
    "country_id": "129"
  },
  {
    "id": "2246",
    "name": "Brod",
    "country_id": "129"
  },
  {
    "id": "2247",
    "name": "Debar",
    "country_id": "129"
  },
  {
    "id": "2248",
    "name": "Delchevo",
    "country_id": "129"
  },
  {
    "id": "2249",
    "name": "Demir Hisar",
    "country_id": "129"
  },
  {
    "id": "2250",
    "name": "Gevgelija",
    "country_id": "129"
  },
  {
    "id": "2251",
    "name": "Gostivar",
    "country_id": "129"
  },
  {
    "id": "2252",
    "name": "Kavadarci",
    "country_id": "129"
  },
  {
    "id": "2253",
    "name": "Kichevo",
    "country_id": "129"
  },
  {
    "id": "2254",
    "name": "Kochani",
    "country_id": "129"
  },
  {
    "id": "2255",
    "name": "Kratovo",
    "country_id": "129"
  },
  {
    "id": "2256",
    "name": "Kriva Palanka",
    "country_id": "129"
  },
  {
    "id": "2257",
    "name": "Krushevo",
    "country_id": "129"
  },
  {
    "id": "2258",
    "name": "Kumanovo",
    "country_id": "129"
  },
  {
    "id": "2259",
    "name": "Negotino",
    "country_id": "129"
  },
  {
    "id": "2260",
    "name": "Ohrid",
    "country_id": "129"
  },
  {
    "id": "2261",
    "name": "Prilep",
    "country_id": "129"
  },
  {
    "id": "2262",
    "name": "Probishtip",
    "country_id": "129"
  },
  {
    "id": "2263",
    "name": "Radovish",
    "country_id": "129"
  },
  {
    "id": "2264",
    "name": "Resen",
    "country_id": "129"
  },
  {
    "id": "2265",
    "name": "Shtip",
    "country_id": "129"
  },
  {
    "id": "2266",
    "name": "Skopje",
    "country_id": "129"
  },
  {
    "id": "2267",
    "name": "Struga",
    "country_id": "129"
  },
  {
    "id": "2268",
    "name": "Strumica",
    "country_id": "129"
  },
  {
    "id": "2269",
    "name": "Sveti Nikole",
    "country_id": "129"
  },
  {
    "id": "2270",
    "name": "Tetovo",
    "country_id": "129"
  },
  {
    "id": "2271",
    "name": "Valandovo",
    "country_id": "129"
  },
  {
    "id": "2272",
    "name": "Veles",
    "country_id": "129"
  },
  {
    "id": "2273",
    "name": "Vinica",
    "country_id": "129"
  },
  {
    "id": "2274",
    "name": "Antananarivo",
    "country_id": "130"
  },
  {
    "id": "2275",
    "name": "Antsiranana",
    "country_id": "130"
  },
  {
    "id": "2276",
    "name": "Fianarantsoa",
    "country_id": "130"
  },
  {
    "id": "2277",
    "name": "Mahajanga",
    "country_id": "130"
  },
  {
    "id": "2278",
    "name": "Toamasina",
    "country_id": "130"
  },
  {
    "id": "2279",
    "name": "Toliary",
    "country_id": "130"
  },
  {
    "id": "2280",
    "name": "Balaka",
    "country_id": "131"
  },
  {
    "id": "2281",
    "name": "Blantyre City",
    "country_id": "131"
  },
  {
    "id": "2282",
    "name": "Chikwawa",
    "country_id": "131"
  },
  {
    "id": "2283",
    "name": "Chiradzulu",
    "country_id": "131"
  },
  {
    "id": "2284",
    "name": "Chitipa",
    "country_id": "131"
  },
  {
    "id": "2285",
    "name": "Dedza",
    "country_id": "131"
  },
  {
    "id": "2286",
    "name": "Dowa",
    "country_id": "131"
  },
  {
    "id": "2287",
    "name": "Karonga",
    "country_id": "131"
  },
  {
    "id": "2288",
    "name": "Kasungu",
    "country_id": "131"
  },
  {
    "id": "2289",
    "name": "Lilongwe City",
    "country_id": "131"
  },
  {
    "id": "2290",
    "name": "Machinga",
    "country_id": "131"
  },
  {
    "id": "2291",
    "name": "Mangochi",
    "country_id": "131"
  },
  {
    "id": "2292",
    "name": "Mchinji",
    "country_id": "131"
  },
  {
    "id": "2293",
    "name": "Mulanje",
    "country_id": "131"
  },
  {
    "id": "2294",
    "name": "Mwanza",
    "country_id": "131"
  },
  {
    "id": "2295",
    "name": "Mzimba",
    "country_id": "131"
  },
  {
    "id": "2296",
    "name": "Mzuzu City",
    "country_id": "131"
  },
  {
    "id": "2297",
    "name": "Nkhata Bay",
    "country_id": "131"
  },
  {
    "id": "2298",
    "name": "Nkhotakota",
    "country_id": "131"
  },
  {
    "id": "2299",
    "name": "Nsanje",
    "country_id": "131"
  },
  {
    "id": "2300",
    "name": "Ntcheu",
    "country_id": "131"
  }
  ]);