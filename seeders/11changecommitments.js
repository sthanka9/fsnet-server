const models = require('../models/index');
const pmodels = require('../production_models'); 
const _ = require('lodash');



pmodels.prod_sequelize.query("select * from ChangeCommitments", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async changeCommitments => { 
    
  changeCommitments.forEach(async changeCommitment => {

    let  tempobj = {
      id: changeCommitment.id,
      fundId: changeCommitment.fundId,
      // lpId: changeCommitment.lpId,
      subscriptionId: changeCommitment.subscriptionId,
      amount: changeCommitment.amount,
      isActive: changeCommitment.isActive,
      isPriorAmount: 0,
      comments: changeCommitment.comments,
      createdAt: changeCommitment.createdAt,
      updatedAt: changeCommitment.updatedAt,
      deletedAt: changeCommitment.deletedAt          
    } 
    
    await models.ChangeCommitment.create(tempobj)
  });
  
})
