const models = require('../models/index');

models.State.bulkCreate([{
    "id": "3160",
    "name": "al-Bahah",
    "country_id": "191"
  },
  {
    "id": "3161",
    "name": "al-Hudud-ash-Shamaliyah",
    "country_id": "191"
  },
  {
    "id": "3162",
    "name": "al-Madinah",
    "country_id": "191"
  },
  {
    "id": "3163",
    "name": "ar-Riyad",
    "country_id": "191"
  },
  {
    "id": "3164",
    "name": "Dakar",
    "country_id": "192"
  },
  {
    "id": "3165",
    "name": "Diourbel",
    "country_id": "192"
  },
  {
    "id": "3166",
    "name": "Fatick",
    "country_id": "192"
  },
  {
    "id": "3167",
    "name": "Kaolack",
    "country_id": "192"
  },
  {
    "id": "3168",
    "name": "Kolda",
    "country_id": "192"
  },
  {
    "id": "3169",
    "name": "Louga",
    "country_id": "192"
  },
  {
    "id": "3170",
    "name": "Saint-Louis",
    "country_id": "192"
  },
  {
    "id": "3171",
    "name": "Tambacounda",
    "country_id": "192"
  },
  {
    "id": "3172",
    "name": "Thies",
    "country_id": "192"
  },
  {
    "id": "3173",
    "name": "Ziguinchor",
    "country_id": "192"
  },
  {
    "id": "3174",
    "name": "Central Serbia",
    "country_id": "193"
  },
  {
    "id": "3175",
    "name": "Kosovo and Metohija",
    "country_id": "193"
  },
  {
    "id": "3176",
    "name": "Vojvodina",
    "country_id": "193"
  },
  {
    "id": "3177",
    "name": "Anse Boileau",
    "country_id": "194"
  },
  {
    "id": "3178",
    "name": "Anse Royale",
    "country_id": "194"
  },
  {
    "id": "3179",
    "name": "Cascade",
    "country_id": "194"
  },
  {
    "id": "3180",
    "name": "Takamaka",
    "country_id": "194"
  },
  {
    "id": "3181",
    "name": "Victoria",
    "country_id": "194"
  },
  {
    "id": "3182",
    "name": "Eastern",
    "country_id": "195"
  },
  {
    "id": "3183",
    "name": "Northern",
    "country_id": "195"
  },
  {
    "id": "3184",
    "name": "Southern",
    "country_id": "195"
  },
  {
    "id": "3185",
    "name": "Western",
    "country_id": "195"
  },
  {
    "id": "3186",
    "name": "Singapore",
    "country_id": "196"
  },
  {
    "id": "3187",
    "name": "Banskobystricky",
    "country_id": "197"
  },
  {
    "id": "3188",
    "name": "Bratislavsky",
    "country_id": "197"
  },
  {
    "id": "3189",
    "name": "Kosicky",
    "country_id": "197"
  },
  {
    "id": "3190",
    "name": "Nitriansky",
    "country_id": "197"
  },
  {
    "id": "3191",
    "name": "Presovsky",
    "country_id": "197"
  },
  {
    "id": "3192",
    "name": "Trenciansky",
    "country_id": "197"
  },
  {
    "id": "3193",
    "name": "Trnavsky",
    "country_id": "197"
  },
  {
    "id": "3194",
    "name": "Zilinsky",
    "country_id": "197"
  },
  {
    "id": "3195",
    "name": "Benedikt",
    "country_id": "198"
  },
  {
    "id": "3196",
    "name": "Gorenjska",
    "country_id": "198"
  },
  {
    "id": "3197",
    "name": "Gorishka",
    "country_id": "198"
  },
  {
    "id": "3198",
    "name": "Jugovzhodna Slovenija",
    "country_id": "198"
  },
  {
    "id": "3199",
    "name": "Koroshka",
    "country_id": "198"
  },
  {
    "id": "3200",
    "name": "Notranjsko-krashka",
    "country_id": "198"
  },
  {
    "id": "3201",
    "name": "Obalno-krashka",
    "country_id": "198"
  },
  {
    "id": "3202",
    "name": "Obcina Domzale",
    "country_id": "198"
  },
  {
    "id": "3203",
    "name": "Obcina Vitanje",
    "country_id": "198"
  },
  {
    "id": "3204",
    "name": "Osrednjeslovenska",
    "country_id": "198"
  },
  {
    "id": "3205",
    "name": "Podravska",
    "country_id": "198"
  },
  {
    "id": "3206",
    "name": "Pomurska",
    "country_id": "198"
  },
  {
    "id": "3207",
    "name": "Savinjska",
    "country_id": "198"
  },
  {
    "id": "3208",
    "name": "Slovenian Littoral",
    "country_id": "198"
  },
  {
    "id": "3209",
    "name": "Spodnjeposavska",
    "country_id": "198"
  },
  {
    "id": "3210",
    "name": "Zasavska",
    "country_id": "198"
  },
  {
    "id": "3211",
    "name": "Pitcairn",
    "country_id": "199"
  },
  {
    "id": "3212",
    "name": "Central",
    "country_id": "200"
  },
  {
    "id": "3213",
    "name": "Choiseul",
    "country_id": "200"
  },
  {
    "id": "3214",
    "name": "Guadalcanal",
    "country_id": "200"
  },
  {
    "id": "3215",
    "name": "Isabel",
    "country_id": "200"
  },
  {
    "id": "3216",
    "name": "Makira and Ulawa",
    "country_id": "200"
  },
  {
    "id": "3217",
    "name": "Malaita",
    "country_id": "200"
  },
  {
    "id": "3218",
    "name": "Rennell and Bellona",
    "country_id": "200"
  },
  {
    "id": "3219",
    "name": "Temotu",
    "country_id": "200"
  },
  {
    "id": "3220",
    "name": "Western",
    "country_id": "200"
  },
  {
    "id": "3221",
    "name": "Awdal",
    "country_id": "201"
  },
  {
    "id": "3222",
    "name": "Bakol",
    "country_id": "201"
  },
  {
    "id": "3223",
    "name": "Banadir",
    "country_id": "201"
  },
  {
    "id": "3224",
    "name": "Bari",
    "country_id": "201"
  },
  {
    "id": "3225",
    "name": "Bay",
    "country_id": "201"
  },
  {
    "id": "3226",
    "name": "Galgudug",
    "country_id": "201"
  },
  {
    "id": "3227",
    "name": "Gedo",
    "country_id": "201"
  },
  {
    "id": "3228",
    "name": "Hiran",
    "country_id": "201"
  },
  {
    "id": "3229",
    "name": "Jubbada Hose",
    "country_id": "201"
  },
  {
    "id": "3230",
    "name": "Jubbadha Dexe",
    "country_id": "201"
  },
  {
    "id": "3231",
    "name": "Mudug",
    "country_id": "201"
  },
  {
    "id": "3232",
    "name": "Nugal",
    "country_id": "201"
  },
  {
    "id": "3233",
    "name": "Sanag",
    "country_id": "201"
  },
  {
    "id": "3234",
    "name": "Shabellaha Dhexe",
    "country_id": "201"
  },
  {
    "id": "3235",
    "name": "Shabellaha Hose",
    "country_id": "201"
  },
  {
    "id": "3236",
    "name": "Togdher",
    "country_id": "201"
  },
  {
    "id": "3237",
    "name": "Woqoyi Galbed",
    "country_id": "201"
  },
  {
    "id": "3238",
    "name": "Eastern Cape",
    "country_id": "202"
  },
  {
    "id": "3239",
    "name": "Free State",
    "country_id": "202"
  },
  {
    "id": "3240",
    "name": "Gauteng",
    "country_id": "202"
  },
  {
    "id": "3241",
    "name": "Kempton Park",
    "country_id": "202"
  },
  {
    "id": "3242",
    "name": "Kramerville",
    "country_id": "202"
  },
  {
    "id": "3243",
    "name": "KwaZulu Natal",
    "country_id": "202"
  },
  {
    "id": "3244",
    "name": "Limpopo",
    "country_id": "202"
  },
  {
    "id": "3245",
    "name": "Mpumalanga",
    "country_id": "202"
  },
  {
    "id": "3246",
    "name": "North West",
    "country_id": "202"
  },
  {
    "id": "3247",
    "name": "Northern Cape",
    "country_id": "202"
  },
  {
    "id": "3248",
    "name": "Parow",
    "country_id": "202"
  },
  {
    "id": "3249",
    "name": "Table View",
    "country_id": "202"
  },
  {
    "id": "3250",
    "name": "Umtentweni",
    "country_id": "202"
  },
  {
    "id": "3251",
    "name": "Western Cape",
    "country_id": "202"
  },
  {
    "id": "3252",
    "name": "South Georgia",
    "country_id": "203"
  },
  {
    "id": "3253",
    "name": "Central Equatoria",
    "country_id": "204"
  },
  {
    "id": "3254",
    "name": "A Coruna",
    "country_id": "205"
  },
  {
    "id": "3255",
    "name": "Alacant",
    "country_id": "205"
  },
  {
    "id": "3256",
    "name": "Alava",
    "country_id": "205"
  },
  {
    "id": "3257",
    "name": "Albacete",
    "country_id": "205"
  },
  {
    "id": "3258",
    "name": "Almeria",
    "country_id": "205"
  },
  {
    "id": "3259",
    "name": "Andalucia",
    "country_id": "205"
  },
  {
    "id": "3260",
    "name": "Asturias",
    "country_id": "205"
  },
  {
    "id": "3261",
    "name": "Avila",
    "country_id": "205"
  },
  {
    "id": "3262",
    "name": "Badajoz",
    "country_id": "205"
  },
  {
    "id": "3263",
    "name": "Balears",
    "country_id": "205"
  },
  {
    "id": "3264",
    "name": "Barcelona",
    "country_id": "205"
  },
  {
    "id": "3265",
    "name": "Bertamirans",
    "country_id": "205"
  },
  {
    "id": "3266",
    "name": "Biscay",
    "country_id": "205"
  },
  {
    "id": "3267",
    "name": "Burgos",
    "country_id": "205"
  },
  {
    "id": "3268",
    "name": "Caceres",
    "country_id": "205"
  },
  {
    "id": "3269",
    "name": "Cadiz",
    "country_id": "205"
  },
  {
    "id": "3270",
    "name": "Cantabria",
    "country_id": "205"
  },
  {
    "id": "3271",
    "name": "Castello",
    "country_id": "205"
  },
  {
    "id": "3272",
    "name": "Catalunya",
    "country_id": "205"
  },
  {
    "id": "3273",
    "name": "Ceuta",
    "country_id": "205"
  },
  {
    "id": "3274",
    "name": "Ciudad Real",
    "country_id": "205"
  },
  {
    "id": "3275",
    "name": "Comunidad Autonoma de Canarias",
    "country_id": "205"
  },
  {
    "id": "3276",
    "name": "Comunidad Autonoma de Cataluna",
    "country_id": "205"
  },
  {
    "id": "3277",
    "name": "Comunidad Autonoma de Galicia",
    "country_id": "205"
  },
  {
    "id": "3278",
    "name": "Comunidad Autonoma de las Isla",
    "country_id": "205"
  },
  {
    "id": "3279",
    "name": "Comunidad Autonoma del Princip",
    "country_id": "205"
  },
  {
    "id": "3280",
    "name": "Comunidad Valenciana",
    "country_id": "205"
  },
  {
    "id": "3281",
    "name": "Cordoba",
    "country_id": "205"
  },
  {
    "id": "3282",
    "name": "Cuenca",
    "country_id": "205"
  },
  {
    "id": "3283",
    "name": "Gipuzkoa",
    "country_id": "205"
  },
  {
    "id": "3284",
    "name": "Girona",
    "country_id": "205"
  },
  {
    "id": "3285",
    "name": "Granada",
    "country_id": "205"
  },
  {
    "id": "3286",
    "name": "Guadalajara",
    "country_id": "205"
  },
  {
    "id": "3287",
    "name": "Guipuzcoa",
    "country_id": "205"
  },
  {
    "id": "3288",
    "name": "Huelva",
    "country_id": "205"
  },
  {
    "id": "3289",
    "name": "Huesca",
    "country_id": "205"
  },
  {
    "id": "3290",
    "name": "Jaen",
    "country_id": "205"
  },
  {
    "id": "3291",
    "name": "La Rioja",
    "country_id": "205"
  },
  {
    "id": "3292",
    "name": "Las Palmas",
    "country_id": "205"
  },
  {
    "id": "3293",
    "name": "Leon",
    "country_id": "205"
  },
  {
    "id": "3294",
    "name": "Lerida",
    "country_id": "205"
  },
  {
    "id": "3295",
    "name": "Lleida",
    "country_id": "205"
  },
  {
    "id": "3296",
    "name": "Lugo",
    "country_id": "205"
  },
  {
    "id": "3297",
    "name": "Madrid",
    "country_id": "205"
  },
  {
    "id": "3298",
    "name": "Malaga",
    "country_id": "205"
  },
  {
    "id": "3299",
    "name": "Melilla",
    "country_id": "205"
  },
  {
    "id": "3300",
    "name": "Murcia",
    "country_id": "205"
  },
  {
    "id": "3301",
    "name": "Navarra",
    "country_id": "205"
  },
  {
    "id": "3302",
    "name": "Ourense",
    "country_id": "205"
  },
  {
    "id": "3303",
    "name": "Pais Vasco",
    "country_id": "205"
  },
  {
    "id": "3304",
    "name": "Palencia",
    "country_id": "205"
  },
  {
    "id": "3305",
    "name": "Pontevedra",
    "country_id": "205"
  },
  {
    "id": "3306",
    "name": "Salamanca",
    "country_id": "205"
  },
  {
    "id": "3307",
    "name": "Santa Cruz de Tenerife",
    "country_id": "205"
  },
  {
    "id": "3308",
    "name": "Segovia",
    "country_id": "205"
  },
  {
    "id": "3309",
    "name": "Sevilla",
    "country_id": "205"
  },
  {
    "id": "3310",
    "name": "Soria",
    "country_id": "205"
  },
  {
    "id": "3311",
    "name": "Tarragona",
    "country_id": "205"
  },
  {
    "id": "3312",
    "name": "Tenerife",
    "country_id": "205"
  },
  {
    "id": "3313",
    "name": "Teruel",
    "country_id": "205"
  },
  {
    "id": "3314",
    "name": "Toledo",
    "country_id": "205"
  },
  {
    "id": "3315",
    "name": "Valencia",
    "country_id": "205"
  },
  {
    "id": "3316",
    "name": "Valladolid",
    "country_id": "205"
  },
  {
    "id": "3317",
    "name": "Vizcaya",
    "country_id": "205"
  },
  {
    "id": "3318",
    "name": "Zamora",
    "country_id": "205"
  },
  {
    "id": "3319",
    "name": "Zaragoza",
    "country_id": "205"
  },
  {
    "id": "3320",
    "name": "Amparai",
    "country_id": "206"
  },
  {
    "id": "3321",
    "name": "Anuradhapuraya",
    "country_id": "206"
  },
  {
    "id": "3322",
    "name": "Badulla",
    "country_id": "206"
  },
  {
    "id": "3323",
    "name": "Boralesgamuwa",
    "country_id": "206"
  },
  {
    "id": "3324",
    "name": "Colombo",
    "country_id": "206"
  },
  {
    "id": "3325",
    "name": "Galla",
    "country_id": "206"
  },
  {
    "id": "3326",
    "name": "Gampaha",
    "country_id": "206"
  },
  {
    "id": "3327",
    "name": "Hambantota",
    "country_id": "206"
  },
  {
    "id": "3328",
    "name": "Kalatura",
    "country_id": "206"
  },
  {
    "id": "3329",
    "name": "Kegalla",
    "country_id": "206"
  },
  {
    "id": "3330",
    "name": "Kilinochchi",
    "country_id": "206"
  },
  {
    "id": "3331",
    "name": "Kurunegala",
    "country_id": "206"
  },
  {
    "id": "3332",
    "name": "Madakalpuwa",
    "country_id": "206"
  },
  {
    "id": "3333",
    "name": "Maha Nuwara",
    "country_id": "206"
  },
  {
    "id": "3334",
    "name": "Malwana",
    "country_id": "206"
  },
  {
    "id": "3335",
    "name": "Mannarama",
    "country_id": "206"
  },
  {
    "id": "3336",
    "name": "Matale",
    "country_id": "206"
  },
  {
    "id": "3337",
    "name": "Matara",
    "country_id": "206"
  },
  {
    "id": "3338",
    "name": "Monaragala",
    "country_id": "206"
  },
  {
    "id": "3339",
    "name": "Mullaitivu",
    "country_id": "206"
  },
  {
    "id": "3340",
    "name": "North Eastern Province",
    "country_id": "206"
  },
  {
    "id": "3341",
    "name": "North Western Province",
    "country_id": "206"
  },
  {
    "id": "3342",
    "name": "Nuwara Eliya",
    "country_id": "206"
  },
  {
    "id": "3343",
    "name": "Polonnaruwa",
    "country_id": "206"
  },
  {
    "id": "3344",
    "name": "Puttalama",
    "country_id": "206"
  },
  {
    "id": "3345",
    "name": "Ratnapuraya",
    "country_id": "206"
  },
  {
    "id": "3346",
    "name": "Southern Province",
    "country_id": "206"
  },
  {
    "id": "3347",
    "name": "Tirikunamalaya",
    "country_id": "206"
  },
  {
    "id": "3348",
    "name": "Tuscany",
    "country_id": "206"
  },
  {
    "id": "3349",
    "name": "Vavuniyawa",
    "country_id": "206"
  },
  {
    "id": "3350",
    "name": "Western Province",
    "country_id": "206"
  },
  {
    "id": "3351",
    "name": "Yapanaya",
    "country_id": "206"
  },
  {
    "id": "3352",
    "name": "kadawatha",
    "country_id": "206"
  },
  {
    "id": "3353",
    "name": "A\'\'ali-an-Nil",
    "country_id": "207"
  },
  {
    "id": "3354",
    "name": "Bahr-al-Jabal",
    "country_id": "207"
  },
  {
    "id": "3355",
    "name": "Central Equatoria",
    "country_id": "207"
  },
  {
    "id": "3356",
    "name": "Gharb Bahr-al-Ghazal",
    "country_id": "207"
  },
  {
    "id": "3357",
    "name": "Gharb Darfur",
    "country_id": "207"
  },
  {
    "id": "3358",
    "name": "Gharb Kurdufan",
    "country_id": "207"
  },
  {
    "id": "3359",
    "name": "Gharb-al-Istiwa\'\'iyah",
    "country_id": "207"
  },
  {
    "id": "3360",
    "name": "Janub Darfur",
    "country_id": "207"
  },
  {
    "id": "3361",
    "name": "Janub Kurdufan",
    "country_id": "207"
  },
  {
    "id": "3362",
    "name": "Junqali",
    "country_id": "207"
  },
  {
    "id": "3363",
    "name": "Kassala",
    "country_id": "207"
  },
  {
    "id": "3364",
    "name": "Nahr-an-Nil",
    "country_id": "207"
  },
  {
    "id": "3365",
    "name": "Shamal Bahr-al-Ghazal",
    "country_id": "207"
  },
  {
    "id": "3366",
    "name": "Shamal Darfur",
    "country_id": "207"
  },
  {
    "id": "3367",
    "name": "Shamal Kurdufan",
    "country_id": "207"
  },
  {
    "id": "3368",
    "name": "Sharq-al-Istiwa\'\'iyah",
    "country_id": "207"
  },
  {
    "id": "3369",
    "name": "Sinnar",
    "country_id": "207"
  },
  {
    "id": "3370",
    "name": "Warab",
    "country_id": "207"
  },
  {
    "id": "3371",
    "name": "Wilayat al Khartum",
    "country_id": "207"
  },
  {
    "id": "3372",
    "name": "al-Bahr-al-Ahmar",
    "country_id": "207"
  },
  {
    "id": "3373",
    "name": "al-Buhayrat",
    "country_id": "207"
  },
  {
    "id": "3374",
    "name": "al-Jazirah",
    "country_id": "207"
  },
  {
    "id": "3375",
    "name": "al-Khartum",
    "country_id": "207"
  },
  {
    "id": "3376",
    "name": "al-Qadarif",
    "country_id": "207"
  },
  {
    "id": "3377",
    "name": "al-Wahdah",
    "country_id": "207"
  },
  {
    "id": "3378",
    "name": "an-Nil-al-Abyad",
    "country_id": "207"
  },
  {
    "id": "3379",
    "name": "an-Nil-al-Azraq",
    "country_id": "207"
  },
  {
    "id": "3380",
    "name": "ash-Shamaliyah",
    "country_id": "207"
  },
  {
    "id": "3381",
    "name": "Brokopondo",
    "country_id": "208"
  },
  {
    "id": "3382",
    "name": "Commewijne",
    "country_id": "208"
  },
  {
    "id": "3383",
    "name": "Coronie",
    "country_id": "208"
  },
  {
    "id": "3384",
    "name": "Marowijne",
    "country_id": "208"
  },
  {
    "id": "3385",
    "name": "Nickerie",
    "country_id": "208"
  },
  {
    "id": "3386",
    "name": "Para",
    "country_id": "208"
  },
  {
    "id": "3387",
    "name": "Paramaribo",
    "country_id": "208"
  },
  {
    "id": "3388",
    "name": "Saramacca",
    "country_id": "208"
  },
  {
    "id": "3389",
    "name": "Wanica",
    "country_id": "208"
  },
  {
    "id": "3390",
    "name": "Svalbard",
    "country_id": "209"
  },
  {
    "id": "3391",
    "name": "Hhohho",
    "country_id": "210"
  },
  {
    "id": "3392",
    "name": "Lubombo",
    "country_id": "210"
  },
  {
    "id": "3393",
    "name": "Manzini",
    "country_id": "210"
  },
  {
    "id": "3394",
    "name": "Shiselweni",
    "country_id": "210"
  },
  {
    "id": "3395",
    "name": "Alvsborgs Lan",
    "country_id": "211"
  },
  {
    "id": "3396",
    "name": "Angermanland",
    "country_id": "211"
  },
  {
    "id": "3397",
    "name": "Blekinge",
    "country_id": "211"
  },
  {
    "id": "3398",
    "name": "Bohuslan",
    "country_id": "211"
  },
  {
    "id": "3399",
    "name": "Dalarna",
    "country_id": "211"
  },
  {
    "id": "3400",
    "name": "Gavleborg",
    "country_id": "211"
  },
  {
    "id": "3401",
    "name": "Gaza",
    "country_id": "211"
  },
  {
    "id": "3402",
    "name": "Gotland",
    "country_id": "211"
  },
  {
    "id": "3403",
    "name": "Halland",
    "country_id": "211"
  },
  {
    "id": "3404",
    "name": "Jamtland",
    "country_id": "211"
  },
  {
    "id": "3405",
    "name": "Jonkoping",
    "country_id": "211"
  },
  {
    "id": "3406",
    "name": "Kalmar",
    "country_id": "211"
  },
  {
    "id": "3407",
    "name": "Kristianstads",
    "country_id": "211"
  },
  {
    "id": "3408",
    "name": "Kronoberg",
    "country_id": "211"
  },
  {
    "id": "3409",
    "name": "Norrbotten",
    "country_id": "211"
  },
  {
    "id": "3410",
    "name": "Orebro",
    "country_id": "211"
  },
  {
    "id": "3411",
    "name": "Ostergotland",
    "country_id": "211"
  },
  {
    "id": "3412",
    "name": "Saltsjo-Boo",
    "country_id": "211"
  },
  {
    "id": "3413",
    "name": "Skane",
    "country_id": "211"
  },
  {
    "id": "3414",
    "name": "Smaland",
    "country_id": "211"
  },
  {
    "id": "3415",
    "name": "Sodermanland",
    "country_id": "211"
  },
  {
    "id": "3416",
    "name": "Stockholm",
    "country_id": "211"
  },
  {
    "id": "3417",
    "name": "Uppsala",
    "country_id": "211"
  },
  {
    "id": "3418",
    "name": "Varmland",
    "country_id": "211"
  },
  {
    "id": "3419",
    "name": "Vasterbotten",
    "country_id": "211"
  },
  {
    "id": "3420",
    "name": "Vastergotland",
    "country_id": "211"
  },
  {
    "id": "3421",
    "name": "Vasternorrland",
    "country_id": "211"
  },
  {
    "id": "3422",
    "name": "Vastmanland",
    "country_id": "211"
  },
  {
    "id": "3423",
    "name": "Vastra Gotaland",
    "country_id": "211"
  },
  {
    "id": "3424",
    "name": "Aargau",
    "country_id": "212"
  },
  {
    "id": "3425",
    "name": "Appenzell Inner-Rhoden",
    "country_id": "212"
  },
  {
    "id": "3426",
    "name": "Appenzell-Ausser Rhoden",
    "country_id": "212"
  },
  {
    "id": "3427",
    "name": "Basel-Landschaft",
    "country_id": "212"
  },
  {
    "id": "3428",
    "name": "Basel-Stadt",
    "country_id": "212"
  },
  {
    "id": "3429",
    "name": "Bern",
    "country_id": "212"
  },
  {
    "id": "3430",
    "name": "Canton Ticino",
    "country_id": "212"
  },
  {
    "id": "3431",
    "name": "Fribourg",
    "country_id": "212"
  },
  {
    "id": "3432",
    "name": "Geneve",
    "country_id": "212"
  },
  {
    "id": "3433",
    "name": "Glarus",
    "country_id": "212"
  },
  {
    "id": "3434",
    "name": "Graubunden",
    "country_id": "212"
  },
  {
    "id": "3435",
    "name": "Heerbrugg",
    "country_id": "212"
  },
  {
    "id": "3436",
    "name": "Jura",
    "country_id": "212"
  },
  {
    "id": "3437",
    "name": "Kanton Aargau",
    "country_id": "212"
  },
  {
    "id": "3438",
    "name": "Luzern",
    "country_id": "212"
  },
  {
    "id": "3439",
    "name": "Morbio Inferiore",
    "country_id": "212"
  },
  {
    "id": "3440",
    "name": "Muhen",
    "country_id": "212"
  },
  {
    "id": "3441",
    "name": "Neuchatel",
    "country_id": "212"
  },
  {
    "id": "3442",
    "name": "Nidwalden",
    "country_id": "212"
  },
  {
    "id": "3443",
    "name": "Obwalden",
    "country_id": "212"
  },
  {
    "id": "3444",
    "name": "Sankt Gallen",
    "country_id": "212"
  },
  {
    "id": "3445",
    "name": "Schaffhausen",
    "country_id": "212"
  },
  {
    "id": "3446",
    "name": "Schwyz",
    "country_id": "212"
  },
  {
    "id": "3447",
    "name": "Solothurn",
    "country_id": "212"
  },
  {
    "id": "3448",
    "name": "Thurgau",
    "country_id": "212"
  },
  {
    "id": "3449",
    "name": "Ticino",
    "country_id": "212"
  },
  {
    "id": "3450",
    "name": "Uri",
    "country_id": "212"
  },
  {
    "id": "3451",
    "name": "Valais",
    "country_id": "212"
  },
  {
    "id": "3452",
    "name": "Vaud",
    "country_id": "212"
  },
  {
    "id": "3453",
    "name": "Vauffelin",
    "country_id": "212"
  },
  {
    "id": "3454",
    "name": "Zug",
    "country_id": "212"
  },
  {
    "id": "3455",
    "name": "Zurich",
    "country_id": "212"
  },
  {
    "id": "3456",
    "name": "Aleppo",
    "country_id": "213"
  },
  {
    "id": "3457",
    "name": "Dar\'\'a",
    "country_id": "213"
  },
  {
    "id": "3458",
    "name": "Dayr-az-Zawr",
    "country_id": "213"
  },
  {
    "id": "3459",
    "name": "Dimashq",
    "country_id": "213"
  },
  {
    "id": "3460",
    "name": "Halab",
    "country_id": "213"
  },
  {
    "id": "3461",
    "name": "Hamah",
    "country_id": "213"
  },
  {
    "id": "3462",
    "name": "Hims",
    "country_id": "213"
  },
  {
    "id": "3463",
    "name": "Idlib",
    "country_id": "213"
  },
  {
    "id": "3464",
    "name": "Madinat Dimashq",
    "country_id": "213"
  },
  {
    "id": "3465",
    "name": "Tartus",
    "country_id": "213"
  },
  {
    "id": "3466",
    "name": "al-Hasakah",
    "country_id": "213"
  },
  {
    "id": "3467",
    "name": "al-Ladhiqiyah",
    "country_id": "213"
  },
  {
    "id": "3468",
    "name": "al-Qunaytirah",
    "country_id": "213"
  },
  {
    "id": "3469",
    "name": "ar-Raqqah",
    "country_id": "213"
  },
  {
    "id": "3470",
    "name": "as-Suwayda",
    "country_id": "213"
  },
  {
    "id": "3471",
    "name": "Changhwa",
    "country_id": "214"
  },
  {
    "id": "3472",
    "name": "Chiayi Hsien",
    "country_id": "214"
  },
  {
    "id": "3473",
    "name": "Chiayi Shih",
    "country_id": "214"
  },
  {
    "id": "3474",
    "name": "Eastern Taipei",
    "country_id": "214"
  },
  {
    "id": "3475",
    "name": "Hsinchu Hsien",
    "country_id": "214"
  },
  {
    "id": "3476",
    "name": "Hsinchu Shih",
    "country_id": "214"
  },
  {
    "id": "3477",
    "name": "Hualien",
    "country_id": "214"
  },
  {
    "id": "3478",
    "name": "Ilan",
    "country_id": "214"
  },
  {
    "id": "3479",
    "name": "Kaohsiung Hsien",
    "country_id": "214"
  },
  {
    "id": "3480",
    "name": "Kaohsiung Shih",
    "country_id": "214"
  },
  {
    "id": "3481",
    "name": "Keelung Shih",
    "country_id": "214"
  },
  {
    "id": "3482",
    "name": "Kinmen",
    "country_id": "214"
  },
  {
    "id": "3483",
    "name": "Miaoli",
    "country_id": "214"
  },
  {
    "id": "3484",
    "name": "Nantou",
    "country_id": "214"
  },
  {
    "id": "3485",
    "name": "Northern Taiwan",
    "country_id": "214"
  },
  {
    "id": "3486",
    "name": "Penghu",
    "country_id": "214"
  },
  {
    "id": "3487",
    "name": "Pingtung",
    "country_id": "214"
  },
  {
    "id": "3488",
    "name": "Taichung",
    "country_id": "214"
  },
  {
    "id": "3489",
    "name": "Taichung Hsien",
    "country_id": "214"
  },
  {
    "id": "3490",
    "name": "Taichung Shih",
    "country_id": "214"
  },
  {
    "id": "3491",
    "name": "Tainan Hsien",
    "country_id": "214"
  },
  {
    "id": "3492",
    "name": "Tainan Shih",
    "country_id": "214"
  },
  {
    "id": "3493",
    "name": "Taipei Hsien",
    "country_id": "214"
  },
  {
    "id": "3494",
    "name": "Taipei Shih / Taipei Hsien",
    "country_id": "214"
  },
  {
    "id": "3495",
    "name": "Taitung",
    "country_id": "214"
  },
  {
    "id": "3496",
    "name": "Taoyuan",
    "country_id": "214"
  },
  {
    "id": "3497",
    "name": "Yilan",
    "country_id": "214"
  },
  {
    "id": "3498",
    "name": "Yun-Lin Hsien",
    "country_id": "214"
  },
  {
    "id": "3499",
    "name": "Yunlin",
    "country_id": "214"
  },
  {
    "id": "3500",
    "name": "Dushanbe",
    "country_id": "215"
  },
  {
    "id": "3501",
    "name": "Gorno-Badakhshan",
    "country_id": "215"
  },
  {
    "id": "3502",
    "name": "Karotegin",
    "country_id": "215"
  },
  {
    "id": "3503",
    "name": "Khatlon",
    "country_id": "215"
  },
  {
    "id": "3504",
    "name": "Sughd",
    "country_id": "215"
  },
  {
    "id": "3505",
    "name": "Arusha",
    "country_id": "216"
  },
  {
    "id": "3506",
    "name": "Dar es Salaam",
    "country_id": "216"
  },
  {
    "id": "3507",
    "name": "Dodoma",
    "country_id": "216"
  },
  {
    "id": "3508",
    "name": "Iringa",
    "country_id": "216"
  },
  {
    "id": "3509",
    "name": "Kagera",
    "country_id": "216"
  },
  {
    "id": "3510",
    "name": "Kigoma",
    "country_id": "216"
  },
  {
    "id": "3511",
    "name": "Kilimanjaro",
    "country_id": "216"
  },
  {
    "id": "3512",
    "name": "Lindi",
    "country_id": "216"
  },
  {
    "id": "3513",
    "name": "Mara",
    "country_id": "216"
  },
  {
    "id": "3514",
    "name": "Mbeya",
    "country_id": "216"
  },
  {
    "id": "3515",
    "name": "Morogoro",
    "country_id": "216"
  },
  {
    "id": "3516",
    "name": "Mtwara",
    "country_id": "216"
  },
  {
    "id": "3517",
    "name": "Mwanza",
    "country_id": "216"
  },
  {
    "id": "3518",
    "name": "Pwani",
    "country_id": "216"
  },
  {
    "id": "3519",
    "name": "Rukwa",
    "country_id": "216"
  },
  {
    "id": "3520",
    "name": "Ruvuma",
    "country_id": "216"
  },
  {
    "id": "3521",
    "name": "Shinyanga",
    "country_id": "216"
  },
  {
    "id": "3522",
    "name": "Singida",
    "country_id": "216"
  },
  {
    "id": "3523",
    "name": "Tabora",
    "country_id": "216"
  },
  {
    "id": "3524",
    "name": "Tanga",
    "country_id": "216"
  },
  {
    "id": "3525",
    "name": "Zanzibar and Pemba",
    "country_id": "216"
  },
  {
    "id": "3526",
    "name": "Amnat Charoen",
    "country_id": "217"
  },
  {
    "id": "3527",
    "name": "Ang Thong",
    "country_id": "217"
  },
  {
    "id": "3528",
    "name": "Bangkok",
    "country_id": "217"
  },
  {
    "id": "3529",
    "name": "Buri Ram",
    "country_id": "217"
  },
  {
    "id": "3530",
    "name": "Chachoengsao",
    "country_id": "217"
  },
  {
    "id": "3531",
    "name": "Chai Nat",
    "country_id": "217"
  },
  {
    "id": "3532",
    "name": "Chaiyaphum",
    "country_id": "217"
  },
  {
    "id": "3533",
    "name": "Changwat Chaiyaphum",
    "country_id": "217"
  },
  {
    "id": "3534",
    "name": "Chanthaburi",
    "country_id": "217"
  },
  {
    "id": "3535",
    "name": "Chiang Mai",
    "country_id": "217"
  },
  {
    "id": "3536",
    "name": "Chiang Rai",
    "country_id": "217"
  },
  {
    "id": "3537",
    "name": "Chon Buri",
    "country_id": "217"
  },
  {
    "id": "3538",
    "name": "Chumphon",
    "country_id": "217"
  },
  {
    "id": "3539",
    "name": "Kalasin",
    "country_id": "217"
  },
  {
    "id": "3540",
    "name": "Kamphaeng Phet",
    "country_id": "217"
  },
  {
    "id": "3541",
    "name": "Kanchanaburi",
    "country_id": "217"
  },
  {
    "id": "3542",
    "name": "Khon Kaen",
    "country_id": "217"
  },
  {
    "id": "3543",
    "name": "Krabi",
    "country_id": "217"
  },
  {
    "id": "3544",
    "name": "Krung Thep",
    "country_id": "217"
  },
  {
    "id": "3545",
    "name": "Lampang",
    "country_id": "217"
  },
  {
    "id": "3546",
    "name": "Lamphun",
    "country_id": "217"
  },
  {
    "id": "3547",
    "name": "Loei",
    "country_id": "217"
  },
  {
    "id": "3548",
    "name": "Lop Buri",
    "country_id": "217"
  },
  {
    "id": "3549",
    "name": "Mae Hong Son",
    "country_id": "217"
  },
  {
    "id": "3550",
    "name": "Maha Sarakham",
    "country_id": "217"
  },
  {
    "id": "3551",
    "name": "Mukdahan",
    "country_id": "217"
  },
  {
    "id": "3552",
    "name": "Nakhon Nayok",
    "country_id": "217"
  },
  {
    "id": "3553",
    "name": "Nakhon Pathom",
    "country_id": "217"
  },
  {
    "id": "3554",
    "name": "Nakhon Phanom",
    "country_id": "217"
  },
  {
    "id": "3555",
    "name": "Nakhon Ratchasima",
    "country_id": "217"
  },
  {
    "id": "3556",
    "name": "Nakhon Sawan",
    "country_id": "217"
  },
  {
    "id": "3557",
    "name": "Nakhon Si Thammarat",
    "country_id": "217"
  },
  {
    "id": "3558",
    "name": "Nan",
    "country_id": "217"
  },
  {
    "id": "3559",
    "name": "Narathiwat",
    "country_id": "217"
  },
  {
    "id": "3560",
    "name": "Nong Bua Lam Phu",
    "country_id": "217"
  },
  {
    "id": "3561",
    "name": "Nong Khai",
    "country_id": "217"
  },
  {
    "id": "3562",
    "name": "Nonthaburi",
    "country_id": "217"
  },
  {
    "id": "3563",
    "name": "Pathum Thani",
    "country_id": "217"
  },
  {
    "id": "3564",
    "name": "Pattani",
    "country_id": "217"
  },
  {
    "id": "3565",
    "name": "Phangnga",
    "country_id": "217"
  },
  {
    "id": "3566",
    "name": "Phatthalung",
    "country_id": "217"
  },
  {
    "id": "3567",
    "name": "Phayao",
    "country_id": "217"
  },
  {
    "id": "3568",
    "name": "Phetchabun",
    "country_id": "217"
  },
  {
    "id": "3569",
    "name": "Phetchaburi",
    "country_id": "217"
  },
  {
    "id": "3570",
    "name": "Phichit",
    "country_id": "217"
  },
  {
    "id": "3571",
    "name": "Phitsanulok",
    "country_id": "217"
  },
  {
    "id": "3572",
    "name": "Phra Nakhon Si Ayutthaya",
    "country_id": "217"
  },
  {
    "id": "3573",
    "name": "Phrae",
    "country_id": "217"
  },
  {
    "id": "3574",
    "name": "Phuket",
    "country_id": "217"
  },
  {
    "id": "3575",
    "name": "Prachin Buri",
    "country_id": "217"
  },
  {
    "id": "3576",
    "name": "Prachuap Khiri Khan",
    "country_id": "217"
  },
  {
    "id": "3577",
    "name": "Ranong",
    "country_id": "217"
  },
  {
    "id": "3578",
    "name": "Ratchaburi",
    "country_id": "217"
  },
  {
    "id": "3579",
    "name": "Rayong",
    "country_id": "217"
  },
  {
    "id": "3580",
    "name": "Roi Et",
    "country_id": "217"
  },
  {
    "id": "3581",
    "name": "Sa Kaeo",
    "country_id": "217"
  },
  {
    "id": "3582",
    "name": "Sakon Nakhon",
    "country_id": "217"
  },
  {
    "id": "3583",
    "name": "Samut Prakan",
    "country_id": "217"
  },
  {
    "id": "3584",
    "name": "Samut Sakhon",
    "country_id": "217"
  },
  {
    "id": "3585",
    "name": "Samut Songkhran",
    "country_id": "217"
  },
  {
    "id": "3586",
    "name": "Saraburi",
    "country_id": "217"
  },
  {
    "id": "3587",
    "name": "Satun",
    "country_id": "217"
  },
  {
    "id": "3588",
    "name": "Si Sa Ket",
    "country_id": "217"
  },
  {
    "id": "3589",
    "name": "Sing Buri",
    "country_id": "217"
  },
  {
    "id": "3590",
    "name": "Songkhla",
    "country_id": "217"
  },
  {
    "id": "3591",
    "name": "Sukhothai",
    "country_id": "217"
  },
  {
    "id": "3592",
    "name": "Suphan Buri",
    "country_id": "217"
  },
  {
    "id": "3593",
    "name": "Surat Thani",
    "country_id": "217"
  },
  {
    "id": "3594",
    "name": "Surin",
    "country_id": "217"
  },
  {
    "id": "3595",
    "name": "Tak",
    "country_id": "217"
  },
  {
    "id": "3596",
    "name": "Trang",
    "country_id": "217"
  },
  {
    "id": "3597",
    "name": "Trat",
    "country_id": "217"
  },
  {
    "id": "3598",
    "name": "Ubon Ratchathani",
    "country_id": "217"
  },
  {
    "id": "3599",
    "name": "Udon Thani",
    "country_id": "217"
  },
  {
    "id": "3600",
    "name": "Uthai Thani",
    "country_id": "217"
  },
  {
    "id": "3601",
    "name": "Uttaradit",
    "country_id": "217"
  },
  {
    "id": "3602",
    "name": "Yala",
    "country_id": "217"
  },
  {
    "id": "3603",
    "name": "Yasothon",
    "country_id": "217"
  },
  {
    "id": "3604",
    "name": "Centre",
    "country_id": "218"
  },
  {
    "id": "3605",
    "name": "Kara",
    "country_id": "218"
  },
  {
    "id": "3606",
    "name": "Maritime",
    "country_id": "218"
  },
  {
    "id": "3607",
    "name": "Plateaux",
    "country_id": "218"
  },
  {
    "id": "3608",
    "name": "Savanes",
    "country_id": "218"
  },
  {
    "id": "3609",
    "name": "Atafu",
    "country_id": "219"
  },
  {
    "id": "3610",
    "name": "Fakaofo",
    "country_id": "219"
  },
  {
    "id": "3611",
    "name": "Nukunonu",
    "country_id": "219"
  },
  {
    "id": "3612",
    "name": "Eua",
    "country_id": "220"
  },
  {
    "id": "3613",
    "name": "Ha\'\'apai",
    "country_id": "220"
  },
  {
    "id": "3614",
    "name": "Niuas",
    "country_id": "220"
  },
  {
    "id": "3615",
    "name": "Tongatapu",
    "country_id": "220"
  },
  {
    "id": "3616",
    "name": "Vava\'\'u",
    "country_id": "220"
  },
  {
    "id": "3617",
    "name": "Arima-Tunapuna-Piarco",
    "country_id": "221"
  },
  {
    "id": "3618",
    "name": "Caroni",
    "country_id": "221"
  },
  {
    "id": "3619",
    "name": "Chaguanas",
    "country_id": "221"
  },
  {
    "id": "3620",
    "name": "Couva-Tabaquite-Talparo",
    "country_id": "221"
  },
  {
    "id": "3621",
    "name": "Diego Martin",
    "country_id": "221"
  },
  {
    "id": "3622",
    "name": "Glencoe",
    "country_id": "221"
  },
  {
    "id": "3623",
    "name": "Penal Debe",
    "country_id": "221"
  },
  {
    "id": "3624",
    "name": "Point Fortin",
    "country_id": "221"
  },
  {
    "id": "3625",
    "name": "Port of Spain",
    "country_id": "221"
  },
  {
    "id": "3626",
    "name": "Princes Town",
    "country_id": "221"
  },
  {
    "id": "3627",
    "name": "Saint George",
    "country_id": "221"
  },
  {
    "id": "3628",
    "name": "San Fernando",
    "country_id": "221"
  },
  {
    "id": "3629",
    "name": "San Juan",
    "country_id": "221"
  },
  {
    "id": "3630",
    "name": "Sangre Grande",
    "country_id": "221"
  },
  {
    "id": "3631",
    "name": "Siparia",
    "country_id": "221"
  },
  {
    "id": "3632",
    "name": "Tobago",
    "country_id": "221"
  },
  {
    "id": "3633",
    "name": "Aryanah",
    "country_id": "222"
  },
  {
    "id": "3634",
    "name": "Bajah",
    "country_id": "222"
  },
  {
    "id": "3635",
    "name": "Bin \'\'Arus",
    "country_id": "222"
  },
  {
    "id": "3636",
    "name": "Binzart",
    "country_id": "222"
  },
  {
    "id": "3637",
    "name": "Gouvernorat de Ariana",
    "country_id": "222"
  },
  {
    "id": "3638",
    "name": "Gouvernorat de Nabeul",
    "country_id": "222"
  },
  {
    "id": "3639",
    "name": "Gouvernorat de Sousse",
    "country_id": "222"
  },
  {
    "id": "3640",
    "name": "Hammamet Yasmine",
    "country_id": "222"
  },
  {
    "id": "3641",
    "name": "Jundubah",
    "country_id": "222"
  },
  {
    "id": "3642",
    "name": "Madaniyin",
    "country_id": "222"
  },
  {
    "id": "3643",
    "name": "Manubah",
    "country_id": "222"
  },
  {
    "id": "3644",
    "name": "Monastir",
    "country_id": "222"
  },
  {
    "id": "3645",
    "name": "Nabul",
    "country_id": "222"
  },
  {
    "id": "3646",
    "name": "Qabis",
    "country_id": "222"
  },
  {
    "id": "3647",
    "name": "Qafsah",
    "country_id": "222"
  },
  {
    "id": "3648",
    "name": "Qibili",
    "country_id": "222"
  },
  {
    "id": "3649",
    "name": "Safaqis",
    "country_id": "222"
  },
  {
    "id": "3650",
    "name": "Sfax",
    "country_id": "222"
  },
  {
    "id": "3651",
    "name": "Sidi Bu Zayd",
    "country_id": "222"
  },
  {
    "id": "3652",
    "name": "Silyanah",
    "country_id": "222"
  },
  {
    "id": "3653",
    "name": "Susah",
    "country_id": "222"
  },
  {
    "id": "3654",
    "name": "Tatawin",
    "country_id": "222"
  },
  {
    "id": "3655",
    "name": "Tawzar",
    "country_id": "222"
  },
  {
    "id": "3656",
    "name": "Tunis",
    "country_id": "222"
  },
  {
    "id": "3657",
    "name": "Zaghwan",
    "country_id": "222"
  },
  {
    "id": "3658",
    "name": "al-Kaf",
    "country_id": "222"
  },
  {
    "id": "3659",
    "name": "al-Mahdiyah",
    "country_id": "222"
  },
  {
    "id": "3660",
    "name": "al-Munastir",
    "country_id": "222"
  },
  {
    "id": "3661",
    "name": "al-Qasrayn",
    "country_id": "222"
  },
  {
    "id": "3662",
    "name": "al-Qayrawan",
    "country_id": "222"
  },
  {
    "id": "3663",
    "name": "Adana",
    "country_id": "223"
  },
  {
    "id": "3664",
    "name": "Adiyaman",
    "country_id": "223"
  },
  {
    "id": "3665",
    "name": "Afyon",
    "country_id": "223"
  },
  {
    "id": "3666",
    "name": "Agri",
    "country_id": "223"
  },
  {
    "id": "3667",
    "name": "Aksaray",
    "country_id": "223"
  },
  {
    "id": "3668",
    "name": "Amasya",
    "country_id": "223"
  },
  {
    "id": "3669",
    "name": "Ankara",
    "country_id": "223"
  },
  {
    "id": "3670",
    "name": "Antalya",
    "country_id": "223"
  },
  {
    "id": "3671",
    "name": "Ardahan",
    "country_id": "223"
  },
  {
    "id": "3672",
    "name": "Artvin",
    "country_id": "223"
  },
  {
    "id": "3673",
    "name": "Aydin",
    "country_id": "223"
  },
  {
    "id": "3674",
    "name": "Balikesir",
    "country_id": "223"
  },
  {
    "id": "3675",
    "name": "Bartin",
    "country_id": "223"
  },
  {
    "id": "3676",
    "name": "Batman",
    "country_id": "223"
  },
  {
    "id": "3677",
    "name": "Bayburt",
    "country_id": "223"
  },
  {
    "id": "3678",
    "name": "Bilecik",
    "country_id": "223"
  },
  {
    "id": "3679",
    "name": "Bingol",
    "country_id": "223"
  },
  {
    "id": "3680",
    "name": "Bitlis",
    "country_id": "223"
  },
  {
    "id": "3681",
    "name": "Bolu",
    "country_id": "223"
  },
  {
    "id": "3682",
    "name": "Burdur",
    "country_id": "223"
  },
  {
    "id": "3683",
    "name": "Bursa",
    "country_id": "223"
  },
  {
    "id": "3684",
    "name": "Canakkale",
    "country_id": "223"
  },
  {
    "id": "3685",
    "name": "Cankiri",
    "country_id": "223"
  },
  {
    "id": "3686",
    "name": "Corum",
    "country_id": "223"
  },
  {
    "id": "3687",
    "name": "Denizli",
    "country_id": "223"
  },
  {
    "id": "3688",
    "name": "Diyarbakir",
    "country_id": "223"
  },
  {
    "id": "3689",
    "name": "Duzce",
    "country_id": "223"
  },
  {
    "id": "3690",
    "name": "Edirne",
    "country_id": "223"
  },
  {
    "id": "3691",
    "name": "Elazig",
    "country_id": "223"
  },
  {
    "id": "3692",
    "name": "Erzincan",
    "country_id": "223"
  },
  {
    "id": "3693",
    "name": "Erzurum",
    "country_id": "223"
  },
  {
    "id": "3694",
    "name": "Eskisehir",
    "country_id": "223"
  },
  {
    "id": "3695",
    "name": "Gaziantep",
    "country_id": "223"
  },
  {
    "id": "3696",
    "name": "Giresun",
    "country_id": "223"
  },
  {
    "id": "3697",
    "name": "Gumushane",
    "country_id": "223"
  },
  {
    "id": "3698",
    "name": "Hakkari",
    "country_id": "223"
  },
  {
    "id": "3699",
    "name": "Hatay",
    "country_id": "223"
  },
  {
    "id": "3700",
    "name": "Icel",
    "country_id": "223"
  },
  {
    "id": "3701",
    "name": "Igdir",
    "country_id": "223"
  },
  {
    "id": "3702",
    "name": "Isparta",
    "country_id": "223"
  },
  {
    "id": "3703",
    "name": "Istanbul",
    "country_id": "223"
  },
  {
    "id": "3704",
    "name": "Izmir",
    "country_id": "223"
  },
  {
    "id": "3705",
    "name": "Kahramanmaras",
    "country_id": "223"
  },
  {
    "id": "3706",
    "name": "Karabuk",
    "country_id": "223"
  },
  {
    "id": "3707",
    "name": "Karaman",
    "country_id": "223"
  },
  {
    "id": "3708",
    "name": "Kars",
    "country_id": "223"
  },
  {
    "id": "3709",
    "name": "Karsiyaka",
    "country_id": "223"
  },
  {
    "id": "3710",
    "name": "Kastamonu",
    "country_id": "223"
  },
  {
    "id": "3711",
    "name": "Kayseri",
    "country_id": "223"
  },
  {
    "id": "3712",
    "name": "Kilis",
    "country_id": "223"
  },
  {
    "id": "3713",
    "name": "Kirikkale",
    "country_id": "223"
  },
  {
    "id": "3714",
    "name": "Kirklareli",
    "country_id": "223"
  },
  {
    "id": "3715",
    "name": "Kirsehir",
    "country_id": "223"
  },
  {
    "id": "3716",
    "name": "Kocaeli",
    "country_id": "223"
  },
  {
    "id": "3717",
    "name": "Konya",
    "country_id": "223"
  },
  {
    "id": "3718",
    "name": "Kutahya",
    "country_id": "223"
  },
  {
    "id": "3719",
    "name": "Lefkosa",
    "country_id": "223"
  },
  {
    "id": "3720",
    "name": "Malatya",
    "country_id": "223"
  },
  {
    "id": "3721",
    "name": "Manisa",
    "country_id": "223"
  },
  {
    "id": "3722",
    "name": "Mardin",
    "country_id": "223"
  },
  {
    "id": "3723",
    "name": "Mugla",
    "country_id": "223"
  },
  {
    "id": "3724",
    "name": "Mus",
    "country_id": "223"
  },
  {
    "id": "3725",
    "name": "Nevsehir",
    "country_id": "223"
  },
  {
    "id": "3726",
    "name": "Nigde",
    "country_id": "223"
  },
]);