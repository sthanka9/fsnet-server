const models = require('../models/index');
const { Op} = require('sequelize')
const path = require('path');
const azureStorage = require('azure-storage');
const blobService = azureStorage.createBlobService('fsnetrgdiag919','kfcvxevowzcVGXNMhJmbcpWR86AzYbvhSoIPlHXUzIpmhqy4MmjtFEH4AmYol0aWqMqE+RfP+1sNKtK6OHpw7w==');
const commonHelper = require('../helpers/commonHelper');
const uuidv1 = require('uuid/v1');
const fs = require('fs')

// UAT to Cooley
models.DocumentsForSignature.findAll({ paranoid: false}).then(docs => {
    for(doc of docs) {
        let type = '';
        if(doc.filePath) {
            if(doc.docType == 'FUND_AGREEMENT') {
                type = 'FUND_AGREEMENT';
            } else  if(doc.docType == 'SUBSCRIPTION_AGREEMENT') {
                type = 'SUBSCRIPTION_AGREEMENT';
            } else  if(doc.docType == 'CONSOLIDATED_FUND_AGREEMENT') {
                type = 'CONSOLIDATED_FUND_AGREEMENT';
            } else if (doc.docType == 'CHANGE_COMMITMENT_AGREEMENT') {
                type = 'CHANGE_COMMITMENT_AGREEMENT'
            } else if (doc.docType == 'SIDE_LETTER_AGREEMENT') {
                type = 'SIDE_LETTER_AGREEMENT';
            }

            const filePath = doc.docType == 'CONSOLIDATED_FUND_AGREEMENT' ? `./assets/funds/${doc.fundId}/${type}_${uuidv1()}.pdf` : `./assets/funds/${doc.fundId}/${doc.subscriptionId}/${type}_${uuidv1()}.pdf`;
            commonHelper.ensureDirectoryExistence(filePath);

            if(doc.docType == 'SIDE_LETTER_AGREEMENT') {
                fs.copyFileSync('./'+doc.filePath, filePath);
            } else {
                _downloadBlob(doc.filePath,`${filePath}`);
            }
            
            models.DocumentsForSignature.update({ filePath : `${filePath}`  }, {
                where: {
                   id: doc.id
                }
            });

        }
    }
});

//update fund Images - UAT to Cooly
models.Fund.findAll({ paranoid: false}).then(funds => {
    for(fund of funds) {
        if(fund.fundImage) {
            const p = fund.fundImage;
            const filePath = `./assets/funds/images/${p.name}`
            commonHelper.ensureDirectoryExistence(filePath);
            _downloadBlob(p.path,`${filePath}`);
            p.path = filePath;
            p.uri =  `/assets/funds/images/${p.name}`;
            delete p.url;
            models.Fund.update({ fundImage: JSON.stringify(p) }, {
                where: {
                   id: fund.id
                }
            })
        }
    }
});




// //update profile pic path
models.User.findAll({ paranoid: false}).then(users => {
    for(user of users) {
        if(user.profilePic) {
            const p = user.profilePic;
            const filePath = `./assets/profile/pics/${p.name}`
            commonHelper.ensureDirectoryExistence(filePath);
            _downloadBlob(p.path,`${filePath}`);
            p.path = filePath;
            p.uri =  `/assets/profile/pics/${p.name}`;
            delete p.url;
            console.log(p);
            models.User.update({ profilePic: JSON.stringify(p) }, {
                where: {
                   id: user.id
                }
            })
        }
    }
});


// // update gp signature path
models.User.findAll({ paranoid: false}).then(users => {
    for(user of users) {
        if(user.signaturePic) {
            const fileName = path.basename(user.signaturePic);
            const signature = "./assets/signatures/"+fileName;
            commonHelper.ensureDirectoryExistence(signature);
            _downloadBlob(user.signaturePic,signature);
            models.User.update({ signaturePic: signature  }, {
                where: {
                   id: user.id
                }
            });
        }
    }
});


// update fund docs
models.Fund.findAll({ paranoid: false}).then(funds => {
    for(fund of funds) {
        if(fund.partnershipDocument) {
            const p = fund.partnershipDocument;
            const filePath = `./assets/funds/${fund.id}/${uuidv1()}.pdf`;
            commonHelper.ensureDirectoryExistence(filePath);
            _downloadBlob(p.blob,filePath);
            p.path = filePath;
            delete p.url;
            delete p.pageNumber;
            delete p.blob;
            models.Fund.update({partnershipDocument: JSON.stringify(p)}, {
                where: {
                   id: fund.id
                }
            })
        }
    }
})


// update gp SignatureTrack path
models.SignatureTrack.findAll({ paranoid: false}).then(signatures => {
    for(signature of signatures) {
        if(signature.signaturePath) {
            const name = path.basename(signature.signaturePath);
            const signaturePath = `./assets/signatures/${name}`;
            commonHelper.ensureDirectoryExistence(signaturePath);
            models.SignatureTrack.update({ signaturePath: signaturePath  }, {
                where: {
                   id: signature.id
                }
            })
        }
    }
});

const containerName = 'uat';
const _downloadBlob = async (blobNamePath, saveTo) => {
    return new Promise((resolve, reject) => {
        blobService.getBlobToLocalFile(containerName, blobNamePath.replace(`${containerName}/`, ''), saveTo, function (error, serverBlob) {
            if (error) {
                reject(error);
            } else {
                resolve(saveTo);
            }
        });
    });
};