const models = require('../models/index');

models.State.bulkCreate([{
    "id": "3727",
    "name": "Ordu",
    "country_id": "223"
  },
  {
    "id": "3728",
    "name": "Osmaniye",
    "country_id": "223"
  },
  {
    "id": "3729",
    "name": "Rize",
    "country_id": "223"
  },
  {
    "id": "3730",
    "name": "Sakarya",
    "country_id": "223"
  },
  {
    "id": "3731",
    "name": "Samsun",
    "country_id": "223"
  },
  {
    "id": "3732",
    "name": "Sanliurfa",
    "country_id": "223"
  },
  {
    "id": "3733",
    "name": "Siirt",
    "country_id": "223"
  },
  {
    "id": "3734",
    "name": "Sinop",
    "country_id": "223"
  },
  {
    "id": "3735",
    "name": "Sirnak",
    "country_id": "223"
  },
  {
    "id": "3736",
    "name": "Sivas",
    "country_id": "223"
  },
  {
    "id": "3737",
    "name": "Tekirdag",
    "country_id": "223"
  },
  {
    "id": "3738",
    "name": "Tokat",
    "country_id": "223"
  },
  {
    "id": "3739",
    "name": "Trabzon",
    "country_id": "223"
  },
  {
    "id": "3740",
    "name": "Tunceli",
    "country_id": "223"
  },
  {
    "id": "3741",
    "name": "Usak",
    "country_id": "223"
  },
  {
    "id": "3742",
    "name": "Van",
    "country_id": "223"
  },
  {
    "id": "3743",
    "name": "Yalova",
    "country_id": "223"
  },
  {
    "id": "3744",
    "name": "Yozgat",
    "country_id": "223"
  },
  {
    "id": "3745",
    "name": "Zonguldak",
    "country_id": "223"
  },
  {
    "id": "3746",
    "name": "Ahal",
    "country_id": "224"
  },
  {
    "id": "3747",
    "name": "Asgabat",
    "country_id": "224"
  },
  {
    "id": "3748",
    "name": "Balkan",
    "country_id": "224"
  },
  {
    "id": "3749",
    "name": "Dasoguz",
    "country_id": "224"
  },
  {
    "id": "3750",
    "name": "Lebap",
    "country_id": "224"
  },
  {
    "id": "3751",
    "name": "Mari",
    "country_id": "224"
  },
  {
    "id": "3752",
    "name": "Grand Turk",
    "country_id": "225"
  },
  {
    "id": "3753",
    "name": "South Caicos and East Caicos",
    "country_id": "225"
  },
  {
    "id": "3754",
    "name": "Funafuti",
    "country_id": "226"
  },
  {
    "id": "3755",
    "name": "Nanumanga",
    "country_id": "226"
  },
  {
    "id": "3756",
    "name": "Nanumea",
    "country_id": "226"
  },
  {
    "id": "3757",
    "name": "Niutao",
    "country_id": "226"
  },
  {
    "id": "3758",
    "name": "Nui",
    "country_id": "226"
  },
  {
    "id": "3759",
    "name": "Nukufetau",
    "country_id": "226"
  },
  {
    "id": "3760",
    "name": "Nukulaelae",
    "country_id": "226"
  },
  {
    "id": "3761",
    "name": "Vaitupu",
    "country_id": "226"
  },
  {
    "id": "3762",
    "name": "Central",
    "country_id": "227"
  },
  {
    "id": "3763",
    "name": "Eastern",
    "country_id": "227"
  },
  {
    "id": "3764",
    "name": "Northern",
    "country_id": "227"
  },
  {
    "id": "3765",
    "name": "Western",
    "country_id": "227"
  },
  {
    "id": "3766",
    "name": "Cherkas\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3767",
    "name": "Chernihivs\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3768",
    "name": "Chernivets\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3769",
    "name": "Crimea",
    "country_id": "228"
  },
  {
    "id": "3770",
    "name": "Dnipropetrovska",
    "country_id": "228"
  },
  {
    "id": "3771",
    "name": "Donets\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3772",
    "name": "Ivano-Frankivs\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3773",
    "name": "Kharkiv",
    "country_id": "228"
  },
  {
    "id": "3774",
    "name": "Kharkov",
    "country_id": "228"
  },
  {
    "id": "3775",
    "name": "Khersonska",
    "country_id": "228"
  },
  {
    "id": "3776",
    "name": "Khmel\'\'nyts\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3777",
    "name": "Kirovohrad",
    "country_id": "228"
  },
  {
    "id": "3778",
    "name": "Krym",
    "country_id": "228"
  },
  {
    "id": "3779",
    "name": "Kyyiv",
    "country_id": "228"
  },
  {
    "id": "3780",
    "name": "Kyyivs\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3781",
    "name": "L\'\'vivs\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3782",
    "name": "Luhans\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3783",
    "name": "Mykolayivs\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3784",
    "name": "Odes\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3785",
    "name": "Odessa",
    "country_id": "228"
  },
  {
    "id": "3786",
    "name": "Poltavs\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3787",
    "name": "Rivnens\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3788",
    "name": "Sevastopol",
    "country_id": "228"
  },
  {
    "id": "3789",
    "name": "Sums\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3790",
    "name": "Ternopil\'\'s\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3791",
    "name": "Volyns\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3792",
    "name": "Vynnyts\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3793",
    "name": "Zakarpats\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3794",
    "name": "Zaporizhia",
    "country_id": "228"
  },
  {
    "id": "3795",
    "name": "Zhytomyrs\'\'ka",
    "country_id": "228"
  },
  {
    "id": "3796",
    "name": "Abu Zabi",
    "country_id": "229"
  },
  {
    "id": "3797",
    "name": "Ajman",
    "country_id": "229"
  },
  {
    "id": "3798",
    "name": "Dubai",
    "country_id": "229"
  },
  {
    "id": "3799",
    "name": "Ras al-Khaymah",
    "country_id": "229"
  },
  {
    "id": "3800",
    "name": "Sharjah",
    "country_id": "229"
  },
  {
    "id": "3801",
    "name": "Sharjha",
    "country_id": "229"
  },
  {
    "id": "3802",
    "name": "Umm al Qaywayn",
    "country_id": "229"
  },
  {
    "id": "3803",
    "name": "al-Fujayrah",
    "country_id": "229"
  },
  {
    "id": "3804",
    "name": "ash-Shariqah",
    "country_id": "229"
  },
  {
    "id": "3805",
    "name": "Aberdeen",
    "country_id": "230"
  },
  {
    "id": "3806",
    "name": "Aberdeenshire",
    "country_id": "230"
  },
  {
    "id": "3807",
    "name": "Argyll",
    "country_id": "230"
  },
  {
    "id": "3808",
    "name": "Armagh",
    "country_id": "230"
  },
  {
    "id": "3809",
    "name": "Bedfordshire",
    "country_id": "230"
  },
  {
    "id": "3810",
    "name": "Belfast",
    "country_id": "230"
  },
  {
    "id": "3811",
    "name": "Berkshire",
    "country_id": "230"
  },
  {
    "id": "3812",
    "name": "Birmingham",
    "country_id": "230"
  },
  {
    "id": "3813",
    "name": "Brechin",
    "country_id": "230"
  },
  {
    "id": "3814",
    "name": "Bridgnorth",
    "country_id": "230"
  },
  {
    "id": "3815",
    "name": "Bristol",
    "country_id": "230"
  },
  {
    "id": "3816",
    "name": "Buckinghamshire",
    "country_id": "230"
  },
  {
    "id": "3817",
    "name": "Cambridge",
    "country_id": "230"
  },
  {
    "id": "3818",
    "name": "Cambridgeshire",
    "country_id": "230"
  },
  {
    "id": "3819",
    "name": "Channel Islands",
    "country_id": "230"
  },
  {
    "id": "3820",
    "name": "Cheshire",
    "country_id": "230"
  },
  {
    "id": "3821",
    "name": "Cleveland",
    "country_id": "230"
  },
  {
    "id": "3822",
    "name": "Co Fermanagh",
    "country_id": "230"
  },
  {
    "id": "3823",
    "name": "Conwy",
    "country_id": "230"
  },
  {
    "id": "3824",
    "name": "Cornwall",
    "country_id": "230"
  },
  {
    "id": "3825",
    "name": "Coventry",
    "country_id": "230"
  },
  {
    "id": "3826",
    "name": "Craven Arms",
    "country_id": "230"
  },
  {
    "id": "3827",
    "name": "Cumbria",
    "country_id": "230"
  },
  {
    "id": "3828",
    "name": "Denbighshire",
    "country_id": "230"
  },
  {
    "id": "3829",
    "name": "Derby",
    "country_id": "230"
  },
  {
    "id": "3830",
    "name": "Derbyshire",
    "country_id": "230"
  },
  {
    "id": "3831",
    "name": "Devon",
    "country_id": "230"
  },
  {
    "id": "3832",
    "name": "Dial Code Dungannon",
    "country_id": "230"
  },
  {
    "id": "3833",
    "name": "Didcot",
    "country_id": "230"
  },
  {
    "id": "3834",
    "name": "Dorset",
    "country_id": "230"
  },
  {
    "id": "3835",
    "name": "Dunbartonshire",
    "country_id": "230"
  },
  {
    "id": "3836",
    "name": "Durham",
    "country_id": "230"
  },
  {
    "id": "3837",
    "name": "East Dunbartonshire",
    "country_id": "230"
  },
  {
    "id": "3838",
    "name": "East Lothian",
    "country_id": "230"
  },
  {
    "id": "3839",
    "name": "East Midlands",
    "country_id": "230"
  },
  {
    "id": "3840",
    "name": "East Sussex",
    "country_id": "230"
  },
  {
    "id": "3841",
    "name": "East Yorkshire",
    "country_id": "230"
  },
  {
    "id": "3842",
    "name": "England",
    "country_id": "230"
  },
  {
    "id": "3843",
    "name": "Essex",
    "country_id": "230"
  },
  {
    "id": "3844",
    "name": "Fermanagh",
    "country_id": "230"
  },
  {
    "id": "3845",
    "name": "Fife",
    "country_id": "230"
  },
  {
    "id": "3846",
    "name": "Flintshire",
    "country_id": "230"
  },
  {
    "id": "3847",
    "name": "Fulham",
    "country_id": "230"
  },
  {
    "id": "3848",
    "name": "Gainsborough",
    "country_id": "230"
  },
  {
    "id": "3849",
    "name": "Glocestershire",
    "country_id": "230"
  },
  {
    "id": "3850",
    "name": "Gwent",
    "country_id": "230"
  },
  {
    "id": "3851",
    "name": "Hampshire",
    "country_id": "230"
  },
  {
    "id": "3852",
    "name": "Hants",
    "country_id": "230"
  },
  {
    "id": "3853",
    "name": "Herefordshire",
    "country_id": "230"
  },
  {
    "id": "3854",
    "name": "Hertfordshire",
    "country_id": "230"
  },
  {
    "id": "3855",
    "name": "Ireland",
    "country_id": "230"
  },
  {
    "id": "3856",
    "name": "Isle Of Man",
    "country_id": "230"
  },
  {
    "id": "3857",
    "name": "Isle of Wight",
    "country_id": "230"
  },
  {
    "id": "3858",
    "name": "Kenford",
    "country_id": "230"
  },
  {
    "id": "3859",
    "name": "Kent",
    "country_id": "230"
  },
  {
    "id": "3860",
    "name": "Kilmarnock",
    "country_id": "230"
  },
  {
    "id": "3861",
    "name": "Lanarkshire",
    "country_id": "230"
  },
  {
    "id": "3862",
    "name": "Lancashire",
    "country_id": "230"
  },
  {
    "id": "3863",
    "name": "Leicestershire",
    "country_id": "230"
  },
  {
    "id": "3864",
    "name": "Lincolnshire",
    "country_id": "230"
  },
  {
    "id": "3865",
    "name": "Llanymynech",
    "country_id": "230"
  },
  {
    "id": "3866",
    "name": "London",
    "country_id": "230"
  },
  {
    "id": "3867",
    "name": "Ludlow",
    "country_id": "230"
  },
  {
    "id": "3868",
    "name": "Manchester",
    "country_id": "230"
  },
  {
    "id": "3869",
    "name": "Mayfair",
    "country_id": "230"
  },
  {
    "id": "3870",
    "name": "Merseyside",
    "country_id": "230"
  },
  {
    "id": "3871",
    "name": "Mid Glamorgan",
    "country_id": "230"
  },
  {
    "id": "3872",
    "name": "Middlesex",
    "country_id": "230"
  },
  {
    "id": "3873",
    "name": "Mildenhall",
    "country_id": "230"
  },
  {
    "id": "3874",
    "name": "Monmouthshire",
    "country_id": "230"
  },
  {
    "id": "3875",
    "name": "Newton Stewart",
    "country_id": "230"
  },
  {
    "id": "3876",
    "name": "Norfolk",
    "country_id": "230"
  },
  {
    "id": "3877",
    "name": "North Humberside",
    "country_id": "230"
  },
  {
    "id": "3878",
    "name": "North Yorkshire",
    "country_id": "230"
  },
  {
    "id": "3879",
    "name": "Northamptonshire",
    "country_id": "230"
  },
  {
    "id": "3880",
    "name": "Northants",
    "country_id": "230"
  },
  {
    "id": "3881",
    "name": "Northern Ireland",
    "country_id": "230"
  },
  {
    "id": "3882",
    "name": "Northumberland",
    "country_id": "230"
  },
  {
    "id": "3883",
    "name": "Nottinghamshire",
    "country_id": "230"
  },
  {
    "id": "3884",
    "name": "Oxford",
    "country_id": "230"
  },
  {
    "id": "3885",
    "name": "Powys",
    "country_id": "230"
  },
  {
    "id": "3886",
    "name": "Roos-shire",
    "country_id": "230"
  },
  {
    "id": "3887",
    "name": "SUSSEX",
    "country_id": "230"
  },
  {
    "id": "3888",
    "name": "Sark",
    "country_id": "230"
  },
  {
    "id": "3889",
    "name": "Scotland",
    "country_id": "230"
  },
  {
    "id": "3890",
    "name": "Scottish Borders",
    "country_id": "230"
  },
  {
    "id": "3891",
    "name": "Shropshire",
    "country_id": "230"
  },
  {
    "id": "3892",
    "name": "Somerset",
    "country_id": "230"
  },
  {
    "id": "3893",
    "name": "South Glamorgan",
    "country_id": "230"
  },
  {
    "id": "3894",
    "name": "South Wales",
    "country_id": "230"
  },
  {
    "id": "3895",
    "name": "South Yorkshire",
    "country_id": "230"
  },
  {
    "id": "3896",
    "name": "Southwell",
    "country_id": "230"
  },
  {
    "id": "3897",
    "name": "Staffordshire",
    "country_id": "230"
  },
  {
    "id": "3898",
    "name": "Strabane",
    "country_id": "230"
  },
  {
    "id": "3899",
    "name": "Suffolk",
    "country_id": "230"
  },
  {
    "id": "3900",
    "name": "Surrey",
    "country_id": "230"
  },
  {
    "id": "3901",
    "name": "Sussex",
    "country_id": "230"
  },
  {
    "id": "3902",
    "name": "Twickenham",
    "country_id": "230"
  },
  {
    "id": "3903",
    "name": "Tyne and Wear",
    "country_id": "230"
  },
  {
    "id": "3904",
    "name": "Tyrone",
    "country_id": "230"
  },
  {
    "id": "3905",
    "name": "Utah",
    "country_id": "230"
  },
  {
    "id": "3906",
    "name": "Wales",
    "country_id": "230"
  },
  {
    "id": "3907",
    "name": "Warwickshire",
    "country_id": "230"
  },
  {
    "id": "3908",
    "name": "West Lothian",
    "country_id": "230"
  },
  {
    "id": "3909",
    "name": "West Midlands",
    "country_id": "230"
  },
  {
    "id": "3910",
    "name": "West Sussex",
    "country_id": "230"
  },
  {
    "id": "3911",
    "name": "West Yorkshire",
    "country_id": "230"
  },
  {
    "id": "3912",
    "name": "Whissendine",
    "country_id": "230"
  },
  {
    "id": "3913",
    "name": "Wiltshire",
    "country_id": "230"
  },
  {
    "id": "3914",
    "name": "Wokingham",
    "country_id": "230"
  },
  {
    "id": "3915",
    "name": "Worcestershire",
    "country_id": "230"
  },
  {
    "id": "3916",
    "name": "Wrexham",
    "country_id": "230"
  },
  {
    "id": "3917",
    "name": "Wurttemberg",
    "country_id": "230"
  },
  {
    "id": "3918",
    "name": "Yorkshire",
    "country_id": "230"
  },

  {
    "id": "3979",
    "name": "United States Minor Outlying I",
    "country_id": "232"
  },
  {
    "id": "3980",
    "name": "Artigas",
    "country_id": "233"
  },
  {
    "id": "3981",
    "name": "Canelones",
    "country_id": "233"
  },
  {
    "id": "3982",
    "name": "Cerro Largo",
    "country_id": "233"
  },
  {
    "id": "3983",
    "name": "Colonia",
    "country_id": "233"
  },
  {
    "id": "3984",
    "name": "Durazno",
    "country_id": "233"
  },
  {
    "id": "3985",
    "name": "FLorida",
    "country_id": "233"
  },
  {
    "id": "3986",
    "name": "Flores",
    "country_id": "233"
  },
  {
    "id": "3987",
    "name": "Lavalleja",
    "country_id": "233"
  },
  {
    "id": "3988",
    "name": "Maldonado",
    "country_id": "233"
  },
  {
    "id": "3989",
    "name": "Montevideo",
    "country_id": "233"
  },
  {
    "id": "3990",
    "name": "Paysandu",
    "country_id": "233"
  },
  {
    "id": "3991",
    "name": "Rio Negro",
    "country_id": "233"
  },
  {
    "id": "3992",
    "name": "Rivera",
    "country_id": "233"
  },
  {
    "id": "3993",
    "name": "Rocha",
    "country_id": "233"
  },
  {
    "id": "3994",
    "name": "Salto",
    "country_id": "233"
  },
  {
    "id": "3995",
    "name": "San Jose",
    "country_id": "233"
  },
  {
    "id": "3996",
    "name": "Soriano",
    "country_id": "233"
  },
  {
    "id": "3997",
    "name": "Tacuarembo",
    "country_id": "233"
  },
  {
    "id": "3998",
    "name": "Treinta y Tres",
    "country_id": "233"
  },
  {
    "id": "3999",
    "name": "Andijon",
    "country_id": "234"
  },
  {
    "id": "4000",
    "name": "Buhoro",
    "country_id": "234"
  },
  {
    "id": "4001",
    "name": "Buxoro Viloyati",
    "country_id": "234"
  },
  {
    "id": "4002",
    "name": "Cizah",
    "country_id": "234"
  },
  {
    "id": "4003",
    "name": "Fargona",
    "country_id": "234"
  },
  {
    "id": "4004",
    "name": "Horazm",
    "country_id": "234"
  },
  {
    "id": "4005",
    "name": "Kaskadar",
    "country_id": "234"
  },
  {
    "id": "4006",
    "name": "Korakalpogiston",
    "country_id": "234"
  },
  {
    "id": "4007",
    "name": "Namangan",
    "country_id": "234"
  },
  {
    "id": "4008",
    "name": "Navoi",
    "country_id": "234"
  },
  {
    "id": "4009",
    "name": "Samarkand",
    "country_id": "234"
  },
  {
    "id": "4010",
    "name": "Sirdare",
    "country_id": "234"
  },
  {
    "id": "4011",
    "name": "Surhondar",
    "country_id": "234"
  },
  {
    "id": "4012",
    "name": "Toskent",
    "country_id": "234"
  },
  {
    "id": "4013",
    "name": "Malampa",
    "country_id": "235"
  },
  {
    "id": "4014",
    "name": "Penama",
    "country_id": "235"
  },
  {
    "id": "4015",
    "name": "Sanma",
    "country_id": "235"
  },
  {
    "id": "4016",
    "name": "Shefa",
    "country_id": "235"
  },
  {
    "id": "4017",
    "name": "Tafea",
    "country_id": "235"
  },
  {
    "id": "4018",
    "name": "Torba",
    "country_id": "235"
  },
  {
    "id": "4019",
    "name": "Vatican City State (Holy See)",
    "country_id": "236"
  },
  {
    "id": "4020",
    "name": "Amazonas",
    "country_id": "237"
  },
  {
    "id": "4021",
    "name": "Anzoategui",
    "country_id": "237"
  },
  {
    "id": "4022",
    "name": "Apure",
    "country_id": "237"
  },
  {
    "id": "4023",
    "name": "Aragua",
    "country_id": "237"
  },
  {
    "id": "4024",
    "name": "Barinas",
    "country_id": "237"
  },
  {
    "id": "4025",
    "name": "Bolivar",
    "country_id": "237"
  },
  {
    "id": "4026",
    "name": "Carabobo",
    "country_id": "237"
  },
  {
    "id": "4027",
    "name": "Cojedes",
    "country_id": "237"
  },
  {
    "id": "4028",
    "name": "Delta Amacuro",
    "country_id": "237"
  },
  {
    "id": "4029",
    "name": "Distrito Federal",
    "country_id": "237"
  },
  {
    "id": "4030",
    "name": "Falcon",
    "country_id": "237"
  },
  {
    "id": "4031",
    "name": "Guarico",
    "country_id": "237"
  },
  {
    "id": "4032",
    "name": "Lara",
    "country_id": "237"
  },
  {
    "id": "4033",
    "name": "Merida",
    "country_id": "237"
  },
  {
    "id": "4034",
    "name": "Miranda",
    "country_id": "237"
  },
  {
    "id": "4035",
    "name": "Monagas",
    "country_id": "237"
  },
  {
    "id": "4036",
    "name": "Nueva Esparta",
    "country_id": "237"
  },
  {
    "id": "4037",
    "name": "Portuguesa",
    "country_id": "237"
  },
  {
    "id": "4038",
    "name": "Sucre",
    "country_id": "237"
  },
  {
    "id": "4039",
    "name": "Tachira",
    "country_id": "237"
  },
  {
    "id": "4040",
    "name": "Trujillo",
    "country_id": "237"
  },
  {
    "id": "4041",
    "name": "Vargas",
    "country_id": "237"
  },
  {
    "id": "4042",
    "name": "Yaracuy",
    "country_id": "237"
  },
  {
    "id": "4043",
    "name": "Zulia",
    "country_id": "237"
  },
  {
    "id": "4044",
    "name": "Bac Giang",
    "country_id": "238"
  },
  {
    "id": "4045",
    "name": "Binh Dinh",
    "country_id": "238"
  },
  {
    "id": "4046",
    "name": "Binh Duong",
    "country_id": "238"
  },
  {
    "id": "4047",
    "name": "Da Nang",
    "country_id": "238"
  },
  {
    "id": "4048",
    "name": "Dong Bang Song Cuu Long",
    "country_id": "238"
  },
  {
    "id": "4049",
    "name": "Dong Bang Song Hong",
    "country_id": "238"
  },
  {
    "id": "4050",
    "name": "Dong Nai",
    "country_id": "238"
  },
  {
    "id": "4051",
    "name": "Dong Nam Bo",
    "country_id": "238"
  },
  {
    "id": "4052",
    "name": "Duyen Hai Mien Trung",
    "country_id": "238"
  },
  {
    "id": "4053",
    "name": "Hanoi",
    "country_id": "238"
  },
  {
    "id": "4054",
    "name": "Hung Yen",
    "country_id": "238"
  },
  {
    "id": "4055",
    "name": "Khu Bon Cu",
    "country_id": "238"
  },
  {
    "id": "4056",
    "name": "Long An",
    "country_id": "238"
  },
  {
    "id": "4057",
    "name": "Mien Nui Va Trung Du",
    "country_id": "238"
  },
  {
    "id": "4058",
    "name": "Thai Nguyen",
    "country_id": "238"
  },
  {
    "id": "4059",
    "name": "Thanh Pho Ho Chi Minh",
    "country_id": "238"
  },
  {
    "id": "4060",
    "name": "Thu Do Ha Noi",
    "country_id": "238"
  },
  {
    "id": "4061",
    "name": "Tinh Can Tho",
    "country_id": "238"
  },
  {
    "id": "4062",
    "name": "Tinh Da Nang",
    "country_id": "238"
  },
  {
    "id": "4063",
    "name": "Tinh Gia Lai",
    "country_id": "238"
  },
  {
    "id": "4064",
    "name": "Anegada",
    "country_id": "239"
  },
  {
    "id": "4065",
    "name": "Jost van Dyke",
    "country_id": "239"
  },
  {
    "id": "4066",
    "name": "Tortola",
    "country_id": "239"
  },
  {
    "id": "4067",
    "name": "Saint Croix",
    "country_id": "240"
  },
  {
    "id": "4068",
    "name": "Saint John",
    "country_id": "240"
  },
  {
    "id": "4069",
    "name": "Saint Thomas",
    "country_id": "240"
  },
  {
    "id": "4070",
    "name": "Alo",
    "country_id": "241"
  },
  {
    "id": "4071",
    "name": "Singave",
    "country_id": "241"
  },
  {
    "id": "4072",
    "name": "Wallis",
    "country_id": "241"
  },
  {
    "id": "4073",
    "name": "Bu Jaydur",
    "country_id": "242"
  },
  {
    "id": "4074",
    "name": "Wad-adh-Dhahab",
    "country_id": "242"
  },
  {
    "id": "4075",
    "name": "al-\'\'Ayun",
    "country_id": "242"
  },
  {
    "id": "4076",
    "name": "as-Samarah",
    "country_id": "242"
  },
  {
    "id": "4077",
    "name": "Adan",
    "country_id": "243"
  },
  {
    "id": "4078",
    "name": "Abyan",
    "country_id": "243"
  },
  {
    "id": "4079",
    "name": "Dhamar",
    "country_id": "243"
  },
  {
    "id": "4080",
    "name": "Hadramaut",
    "country_id": "243"
  },
  {
    "id": "4081",
    "name": "Hajjah",
    "country_id": "243"
  },
  {
    "id": "4082",
    "name": "Hudaydah",
    "country_id": "243"
  },
  {
    "id": "4083",
    "name": "Ibb",
    "country_id": "243"
  },
  {
    "id": "4084",
    "name": "Lahij",
    "country_id": "243"
  },
  {
    "id": "4085",
    "name": "Ma\'\'rib",
    "country_id": "243"
  },
  {
    "id": "4086",
    "name": "Madinat San\'\'a",
    "country_id": "243"
  },
  {
    "id": "4087",
    "name": "Sa\'\'dah",
    "country_id": "243"
  },
  {
    "id": "4088",
    "name": "Sana",
    "country_id": "243"
  },
  {
    "id": "4089",
    "name": "Shabwah",
    "country_id": "243"
  },
  {
    "id": "4090",
    "name": "Ta\'\'izz",
    "country_id": "243"
  },
  {
    "id": "4091",
    "name": "al-Bayda",
    "country_id": "243"
  },
  {
    "id": "4092",
    "name": "al-Hudaydah",
    "country_id": "243"
  },
  {
    "id": "4093",
    "name": "al-Jawf",
    "country_id": "243"
  },
  {
    "id": "4094",
    "name": "al-Mahrah",
    "country_id": "243"
  },
  {
    "id": "4095",
    "name": "al-Mahwit",
    "country_id": "243"
  },
  {
    "id": "4096",
    "name": "Central Serbia",
    "country_id": "244"
  },
  {
    "id": "4097",
    "name": "Kosovo and Metohija",
    "country_id": "244"
  },
  {
    "id": "4098",
    "name": "Montenegro",
    "country_id": "244"
  },
  {
    "id": "4099",
    "name": "Republic of Serbia",
    "country_id": "244"
  },
  {
    "id": "4100",
    "name": "Serbia",
    "country_id": "244"
  },
  {
    "id": "4101",
    "name": "Vojvodina",
    "country_id": "244"
  },
  {
    "id": "4102",
    "name": "Central",
    "country_id": "245"
  },
  {
    "id": "4103",
    "name": "Copperbelt",
    "country_id": "245"
  },
  {
    "id": "4104",
    "name": "Eastern",
    "country_id": "245"
  },
  {
    "id": "4105",
    "name": "Luapala",
    "country_id": "245"
  },
  {
    "id": "4106",
    "name": "Lusaka",
    "country_id": "245"
  },
  {
    "id": "4107",
    "name": "North-Western",
    "country_id": "245"
  },
  {
    "id": "4108",
    "name": "Northern",
    "country_id": "245"
  },
  {
    "id": "4109",
    "name": "Southern",
    "country_id": "245"
  },
  {
    "id": "4110",
    "name": "Western",
    "country_id": "245"
  },
  {
    "id": "4111",
    "name": "Bulawayo",
    "country_id": "246"
  },
  {
    "id": "4112",
    "name": "Harare",
    "country_id": "246"
  },
  {
    "id": "4113",
    "name": "Manicaland",
    "country_id": "246"
  },
  {
    "id": "4114",
    "name": "Mashonaland Central",
    "country_id": "246"
  },
  {
    "id": "4115",
    "name": "Mashonaland East",
    "country_id": "246"
  },
  {
    "id": "4116",
    "name": "Mashonaland West",
    "country_id": "246"
  },
  {
    "id": "4117",
    "name": "Masvingo",
    "country_id": "246"
  },
  {
    "id": "4118",
    "name": "Matabeleland North",
    "country_id": "246"
  },
  {
    "id": "4119",
    "name": "Matabeleland South",
    "country_id": "246"
  },
  {
    "id": "4120",
    "name": "Midlands",
    "country_id": "246"
  }  ]);