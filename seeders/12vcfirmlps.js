const models = require('../models/index');
const pmodels = require('../production_models'); 

pmodels.prod_sequelize.query("SELECT * FROM VcFrimLps ", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async users => {
  
   
      for (let i = 0; i < users.length ; i++) {

        let element = users[i]   
 

        let  tempobj = {
          id:element.id,
          vcfirmId: element.vcfirmId,
          lpId: element.lpId,
          isOffline:0,
          isDeleted: element.isDeleted,
          createdBy: element.createdBy,
          updatedBy: element.updatedBy,
          createdAt: element.createdAt,
          updatedAt: element.updatedAt,          
          deletedAt: null
        }        
        
        let vcinfo = await models.VCFirm.findOne({attributes:['id'],where:{  
          id:element.vcfirmId 
        }})

        if(vcinfo){
          
              let userinfo = await models.VcFrimLp.findOne({attributes:['id'],where:{
                lpId:element.lpId,
                vcfirmId:element.vcfirmId
              }})

              if(!userinfo){
              console.log("insert ***********",element.id)    
              await models.VcFrimLp.create(tempobj);
            
            } else {
              console.log("update ***********",element.id)  

              await models.VcFrimLp.update(tempobj,{where:{lpId:element.lpId,
                vcfirmId:element.vcfirmId}});
            }

        }
 
    }       
     
})


 