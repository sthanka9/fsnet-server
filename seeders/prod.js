const models = require('../models/index');
const { Op} = require('sequelize')
const path = require('path');
const azureStorage = require('azure-storage');
const blobService = azureStorage.createBlobService('fsnetrgdiag919','kfcvxevowzcVGXNMhJmbcpWR86AzYbvhSoIPlHXUzIpmhqy4MmjtFEH4AmYol0aWqMqE+RfP+1sNKtK6OHpw7w==');

//update profile pic path
models.User.findAll().then(users => {
    for(user of users) {
        if(user.profilePic) {
            const p = user.profilePic;
            p.path = p.path.replace('prod/','./assets/');
            models.User.update({ profilePic: JSON.stringify(p) }, {
                where: {
                   id: user.id
                }
            })
        }
    }
});

//update fund Images
models.Fund.findAll().then(funds => {
    for(fund of funds) {
        if(fund.fundImage) {
            const p = fund.fundImage;
            p.path = p.path.replace('prod/','./assets/');
            models.Fund.update({ fundImage: JSON.stringify(p) }, {
                where: {
                   id: fund.id
                }
            })
        }
    }
});


// update gp signature path
models.User.findAll().then(users => {
    for(user of users) {
        console.log(user);
        if(user.signaturePic) {
            const p = user.signaturePic.replace('prod/','./assets/');
            models.User.update({ signaturePic: p  }, {
                where: {
                   id: user.id
                }
            })
        }
    }
});


// update fund additionalSignatoryPages
models.Fund.findAll().then(funds => {
    for(fund of funds) {
        if(fund.additionalSignatoryPages) {
            const p = fund.additionalSignatoryPages;
            const blobName = path.basename(p.blob);
            _downloadBlob(p.blob,'./assets/additionPages/');
            models.Fund.update({ additionalSignatoryPages: JSON.stringify({
                'originalname': p.originalname,
                'uri' : '/assets/'+blobName,
                'path' : './assets/'+blobName,
            }) }, {
                where: {
                   id: fund.id
                }
            })
        }
    }
})


// update gp DocumentsForSignatures path
models.DocumentsForSignature.findAll().then(docs => {
    for(doc of docs) {
        if(doc.lpSignaturePath) {
            const p = doc.lpSignaturePath.replace('prod/','./assets/');
            models.DocumentsForSignature.update({ lpSignaturePath: p  }, {
                where: {
                   id: doc.id
                }
            })
        }
    }
});

const containerName = 'prod';
const _downloadBlob = async (blobNamePath, saveTo) => {
    const blobName = path.basename(blobNamePath);
    const downloadFilePath = saveTo + blobName;
    return new Promise((resolve, reject) => {
        blobService.getBlobToLocalFile(containerName, blobNamePath.replace(`${containerName}/`, ''), downloadFilePath, function (error, serverBlob) {
            if (error) {
                reject(error);
            } else {
                resolve(downloadFilePath);
            }
        });
    });
};