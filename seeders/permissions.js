const models = require('../models/index');

// fsnet admin
models.RolePermission.bulkCreate([{
    roleId: 1,
    permissionId: 1 // create fund
}, {
    roleId: 1,
    permissionId: 2 // view fund
}, {
    roleId: 1,
    permissionId: 3 // list fund
}, {
    roleId: 1,
    permissionId: 6 // GP Document Locker
}, {
    roleId: 1,
    permissionId: 4 // Commitment Change Gp Sign
},
{
    roleId: 1,
    permissionId: 10 // lp Commitment Change Request
},
{
    roleId: 1,
    permissionId: 14 // lp Commitment Change Request
},
{
    roleId: 1,
    permissionId: 16 //  gp Delegate Invite
},
{
    roleId: 1,
    permissionId: 17 //  gp Delegate Invite
},
{
    roleId: 1,
    permissionId: 18 //  Close Fund
},
{
    roleId: 1,
    permissionId: 19 //  Deactivate Fund
},
{
    roleId: 1,
    permissionId: 20 //  Send Remainder to LP
},
{
    roleId: 1,
    permissionId: 24 //  Add Questions
},
{
    roleId: 1,
    permissionId: 25 //  fund Delete
},
{
    roleId: 1,
    permissionId: 26 //  impersonate user
},
]);
// fsnet administrator = 1
models.RolePermission.bulkCreate([{
    roleId: 1,
    permissionId: 23 // create firm
},
{
    roleId: 1,
    permissionId: 27 // create firm
},
{
    roleId: 1,
    permissionId: 28 // create firm
},
{
    roleId: 1,
    permissionId: 29 //  admin side check
},
{
    roleId: 1,
    permissionId: 30 //  gpSignatoryInvite
},
]);

// lp = 4
models.RolePermission.bulkCreate([{
    roleId: 4,
    permissionId: 5 // lp document locker
}, {
    roleId: 4,
    permissionId: 7 // create subscription
}, {
    roleId: 4,
    permissionId: 8 // view subscription
}, {
    roleId: 4,
    permissionId: 9 // list subscription
}, {
    roleId: 4,
    permissionId: 11 // lp New Subscription Action
}, {
    roleId: 4,
    permissionId: 12 // subscription Lp Delegate Manage
}, {
    roleId: 4,
    permissionId: 13 // subscription Pending Actions
}, {
    roleId: 4,
    permissionId: 15 // lp signature authentication
}, {
    roleId: 4,
    permissionId: 21 // View Subscription Pdf
}
]);

// lp Delegate = 5
models.RolePermission.bulkCreate([{
    roleId: 5,
    permissionId: 5 // lp document locker
}, {
    roleId: 5,
    permissionId: 7 // create subscription
}, {
    roleId: 5,
    permissionId: 8 // view subscription
}, {
    roleId: 5,
    permissionId: 9 // list subscription
}, {
    roleId: 5,
    permissionId: 11 // lp New Subscription Action
}, {
    roleId: 5,
    permissionId: 13 // Fund Pending Actions
}]);

// gp
models.RolePermission.bulkCreate([{
    roleId: 2,
    permissionId: 1 // create fund
}, {
    roleId: 2,
    permissionId: 2 // view fund
}, {
    roleId: 2,
    permissionId: 3 // list fund
}, {
    roleId: 2,
    permissionId: 6 // GP Document Locker
}, {
    roleId: 2,
    permissionId: 4 // Commitment Change Gp Sign
},
{
    roleId: 2,
    permissionId: 10 // lp Commitment Change Request
},
{
    roleId: 2,
    permissionId: 14 // lp Commitment Change Request
},
{
    roleId: 2,
    permissionId: 16 //  gp Delegate Invite
},
{
    roleId: 2,
    permissionId: 17 //  gp Delegate Invite
},
{
    roleId: 2,
    permissionId: 18 //  Close Fund
},
{
    roleId: 2,
    permissionId: 19 //  Deactivate Fund
},
{
    roleId: 2,
    permissionId: 20 //  Send Remainder to LP
},
{
    roleId: 2,
    permissionId: 24 //  Add Questions
},
{
    roleId: 2,
    permissionId: 30 // Able to add Secondary GP
},
{
    roleId: 2,
    permissionId: 7 // Able to fill subscription form
},
{
    roleId: 2,
    permissionId: 8 // Able to view subscription form
},
{
    roleId: 2,
    permissionId: 13 // Able to view pending actions
}
]);


// Gp Delegate
models.RolePermission.bulkCreate([{
    roleId: 3,
    permissionId: 1 // create fund
}, {
    roleId: 3,
    permissionId: 6 // GP Document Locker
}, {
    roleId: 3,
    permissionId: 2 // view fund
}, {
    roleId: 3,
    permissionId: 10 // lp Commitment Change Request
}, {
    roleId: 3,
    permissionId: 3 // list fund
}, {
    roleId: 3,
    permissionId: 13 // Fund Pending Actions
},
{
    roleId: 3,
    permissionId: 17 //  gp Delegate Invite
},
{
    roleId: 3,
    permissionId: 19 //  Deactivate Fund
},
{
    roleId: 3,
    permissionId: 20 //  Send Remainder to LP
},
{
    roleId: 3,
    permissionId: 22 //  Approval Required for Closing
},
{
    roleId: 3,
    permissionId: 24 // Add Questions
},
{
    roleId: 3,
    permissionId: 7 // Able to fill subscription form
},
{
    roleId: 3,
    permissionId:8 // Able to view subscription form
}
]);

// Gp Signatory
models.RolePermission.bulkCreate([{
    roleId: 6,
    permissionId: 1 // create fund
}, {
    roleId: 6,
    permissionId: 2 // view fund
}, {
    roleId: 6,
    permissionId: 3 // list fund
}, {
    roleId: 6,
    permissionId: 6 // GP Document Locker
}, {
    roleId: 6,
    permissionId: 4 // Commitment Change Gp Sign
},
{
    roleId: 6,
    permissionId: 10 // lp Commitment Change Request
},
{
    roleId: 6,
    permissionId: 14 // lp Commitment Change Request
},
{
    roleId: 6,
    permissionId: 16 //  gp Delegate Invite
},
{
    roleId: 6,
    permissionId: 17 //  gp Delegate Invite
},
{
    roleId: 6,
    permissionId: 18 //  Close Fund
},
{
    roleId: 6,
    permissionId: 19 //  Deactivate Fund
},
{
    roleId: 6,
    permissionId: 20 //  Send Remainder to LP
},
{
    roleId: 6,
    permissionId: 24 //  Add Questions
},
{
    roleId: 6,
    permissionId: 30 // Able to add Secondary GP
},
{
    roleId: 6,
    permissionId: 7 // Able to fill subscription form
},
{
    roleId: 6,
    permissionId: 8 // Able to view subscription form
},
{
    roleId: 6,
    permissionId: 13 // Able to view pending actions
}
]);


// LP Signatory
models.RolePermission.bulkCreate([{
    roleId: 7,
    permissionId: 5 // lp document locker
}, {
    roleId: 7,
    permissionId: 7 // create subscription
}, {
    roleId: 7,
    permissionId: 8 // view subscription
}, {
    roleId: 7,
    permissionId: 9 // list subscription
}, {
    roleId: 7,
    permissionId: 11 // lp New Subscription Action
}, {
    roleId: 7,
    permissionId: 12 // subscription Lp Delegate Manage
}, {
    roleId: 7,
    permissionId: 13 // subscription Pending Actions
}, {
    roleId: 7,
    permissionId: 15 // lp signature authentication
}, {
    roleId: 7,
    permissionId: 21 // View Subscription Pdf
}
]);






