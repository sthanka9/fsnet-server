const models = require('../models/index');
const pmodels = require('../production_models');
const fs = require('fs');
const uuidv1 = require('uuid/v1')
const commonHelper = require('../helpers/commonHelper');

pmodels.prod_sequelize.query("SELECT * FROM DocumentsForSignatures", { type: pmodels.prod_sequelize.QueryTypes.SELECT })
    .then(async docs => {

        for (doc of docs) {

            let docType = null
            if (doc.docType == 'subscriptionAgreement') {
                docType = 'SUBSCRIPTION_AGREEMENT';
            }
            if (doc.docType == 'partnershipAgreement') {
                docType = 'FUND_AGREEMENT';
            }
            if (doc.docType == 'sideLetterAgreement') {
                docType = 'SIDE_LETTER_AGREEMENT';
            }
            if (doc.docType == 'changeCommitment') {
                docType = 'CHANGE_COMMITMENT_AGREEMENT';
            }

            let tempPath = doc.envelopeRecipientViewUrl
            tempPath = tempPath.replace("/home/fsnetprod_un/server_release4/assets","./assets")
            tempPath = tempPath.replace("/home/fsnetprod_un/server_release3/assets","./assets")
            tempPath = tempPath.replace("/home/fsnetprod_un/server_release6/assets","./assets")

            const filePath = `./assets/funds/${doc.fundId}/${doc.subscriptionId}/${docType}_${uuidv1()}.pdf`;
            commonHelper.ensureDirectoryExistence(filePath);

            if (fs.existsSync(tempPath)) {
                fs.copyFileSync(tempPath, filePath);
            }
            

            let sideletterId = null;
            if (doc.docType == 'sideLetterAgreement') {
                const sideLetter = await models.SideLetter.create({
                    fundId: doc.fundId,
                    subscriptionId: doc.subscriptionId,
                    isActive: 1,
                    sideLetterDocumentPath: filePath
                });
                sideletterId = sideLetter.id
            }

           
            const newDoc = await models.DocumentsForSignature.create({
                fundId: doc.fundId,
                subscriptionId: doc.subscriptionId,
                changeCommitmentId: doc.changeCommitmentId,
                filePath: filePath,
                docType: docType,
                gpSignCount: doc.isGpSigned ? 1 : 0,
                lpSignCount: doc.isLpSigned ? 1 : 0,
                isPrimaryGpSigned: doc.isGpSigned,
                isPrimaryLpSigned: doc.isLpSigned,
                deletedAt: doc.deletedAt,
                createdAt: doc.createdAt,
                updatedAt: doc.updatedAt,
                createdBy: doc.createdBy,
                updatedBy: doc.updatedBy,
                isActive: doc.isActive,
                isAllGpSignatoriesSigned: doc.isGpSigned ? 1 : 0,
                isAllLpSignatoriesSigned: doc.isLpSigned ? 1 : 0,
                isAddedToFundAgreement: false,
                sideletterId: sideletterId,
            });

            let signature = [];
            
            const fund = await models.Fund.findOne({
                where: {
                    id: doc.fundId
                }
            });

        if(fund){

            const gp = await models.User.findOne({
                where: {
                    id: fund.gpId
                }
            });

            if (doc.isGpSigned && fund && gp) {

               
                let signaturePic = null;
                if (gp && gp.signaturePic) {
                    signaturePic = gp.signaturePic.replace('prod/', './assets/')
                }

                signature.push({
                    documentId: newDoc.id,
                    signaturePath: signaturePic,
                    ipAddress: doc.gpIpAddress,
                    signedDateTime: doc.gpSignedDateTime,
                    timezone: doc.gpTimezone,
                    signedDateInGMT: doc.gpGMTDate,
                    signedUserId: fund.gpId,
                    signedByType: "GP",
                    deletedAt: doc.deletedAt,
                    createdAt: doc.createdAt,
                    updatedAt: doc.updatedAt,
                    createdBy: doc.createdBy,
                    updatedBy: doc.updatedBy,
                    isActive: doc.isActive,
                    subscriptionId: doc.subscriptionId,
                    documentType: docType,
                    isAddedToConsolidatedFundAgreement: false,
                })
            } else {
                console.log('no gp');
            }

        }


            const subscription = await models.FundSubscription.findOne({
                where: {
                    id: doc.subscriptionId
                }
            });

            if (doc.isLpSigned && subscription) {
                signature.push({
                    documentId: newDoc.id,
                    signaturePath: doc.lpSignaturePath ? doc.lpSignaturePath.replace('prod/', './assets/') : doc.lpSignaturePath,
                    ipAddress: doc.lpIpAddress,
                    signedDateTime: doc.lpSignedDateTime,
                    timezone: doc.lpTimezone,
                    signedDateInGMT: doc.lpGMTDate,
                    signedUserId: subscription.lpId,
                    signedByType: 'LP',
                    deletedAt: doc.deletedAt,
                    createdAt: doc.createdAt,
                    updatedAt: doc.updatedAt,
                    createdBy: doc.createdBy,
                    updatedBy: doc.updatedBy,
                    isActive: doc.isActive,
                    subscriptionId: doc.subscriptionId,
                    documentType: docType,
                    isAddedToConsolidatedFundAgreement: false,
                });
            } else {
                console.log('NO LP');
            }

            if (signature.length > 0) {
              await models.SignatureTrack.bulkCreate(signature);
            }   
        
        } 
    })
