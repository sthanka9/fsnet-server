const models = require('../models/index');
const pmodels = require('../production_models'); 

pmodels.prod_sequelize.query("SELECT * FROM Users ", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async users => {
  
   
      for (let i = 0; i < users.length ; i++) {

        let element = users[i]
      
          let newpicobj = ''
          if(element.profilePic!='' && element.profilePic!=null)
          {
            let temppicobj = {}
            let profilePic = JSON.parse(element.profilePic)
            let profilepath = profilePic.path
            
            profilepath = profilepath.replace("prod/profile/","./assets/profile/pics/")

            let uripath = profilepath.replace("./assets/","/assets/")          
            temppicobj.originalname = profilePic.originalname
            temppicobj.name = profilePic.name
            temppicobj.size = profilePic.size
            temppicobj.path = profilepath          
            
            temppicobj.uri  = uripath
            newpicobj = JSON.stringify(temppicobj)
         } 

         let signaturePic = ''
         if(element.signaturePic!='' && element.signaturePic!=null)
         {
            signaturePic = element.signaturePic            
            signaturePic = signaturePic.replace("prod","./assets") 
        }       
 

        let  tempobj = {
          id:element.id,
          firstName:element.firstName,
          lastName:element.lastName,
          email:element.email,
          password:element.password,
          cellNumber:element.cellNumber,
          profilePic:newpicobj,
          isEmailConfirmed:element.isEmailConfirmed,
          emailConfirmCode:element.emailConfirmCode,
          lastCodeGeneratedAt:element.lastCodeGeneratedAt,
          city:element.city,
          isImpersonatingAllowed:element.isImpersonatingAllowed,
          accountType:element.accountType,
          loginFailAttempts:element.loginFailAttempts,
          loginFailAttemptsAt:element.loginFailAttemptsAt,
          lastLoginAt:element.lastLoginAt,
          organizationName:element.organizationName,
          createdAt:element.createdAt,
          updatedAt:element.updatedAt,
          streetAddress1:element.streetAddress1,
          streetAddress2:element.streetAddress2,
          zipcode:element.zipcode,
          primaryContact:element.primaryContact,
          country:element.country,
          state:element.state,
          signaturePic:signaturePic,
          isEmailNotification:element.isEmailNotification,
          deletedAt:element.deletedAt,
          middleName:element.middleName
        }        
 

      let userinfo = await models.User.findOne({attributes:['id'],where:{id:element.id}});

      if(!userinfo){
      console.log("insert ***********",element.id)    
      models.User.create(tempobj);
     
    } else {
      console.log("update ***********",element.id)  

      models.User.update(tempobj,{where:{id:element.id}});
    }
 
    }       
     
})


 