const models = require('../models/index');


models.Role.bulkCreate([{
    id: 9,
    role: 'Administrator',
    slug: 'Admin',
    isLocked: 1
}
]).then(function(role){
    console.log('Role created')
}).catch(function(err){
    console.log('Role already exists -- Adding permissions');
});

models.sequelize.query("SELECT * FROM Permissions", {type: models.sequelize.QueryTypes.SELECT})
    .then(async permission => {

        for (let i = 0; i < permission.length; i++) {
            let element = permission[i]

            let tempobj = {
                roleId: 9,
                permissionId: element.id
            }

            let userRoleExists = await models.RolePermission.findOne({where: tempobj});

            if (!userRoleExists) {
                console.log("insert " + element.permission + " permission role --admin ",)
                await models.RolePermission.create(tempobj).then(function(rolePermission) {
                        console.log('success', rolePermission.toJSON());
                    })
                    .catch(function(err) {
                        console.log('Something went wrong :: ', err);
                    });
            } else {
                console.log(element.permission + " permission already exists for role --admin ",)
            }
        }
    });

