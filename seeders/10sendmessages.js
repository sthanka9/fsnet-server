const models = require('../models/index');
const pmodels = require('../production_models'); 

pmodels.prod_sequelize.query("SELECT * FROM SendMessages ", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async users => { 
   
      for (let i = 0; i < users.length ; i++) {

        let element = users[i]         

        let  tempobj = {
          id:element.id,
          fromUserId: element.fromUserId,
          toUserId: element.toUserId,           
          message: element.message, 
          createdAt: element.createdAt,
          updatedAt: element.updatedAt,
          createdBy: element.createdBy,
          updatedBy: element.updatedBy          
        }        
          

          let userinfo = await models.SendMessage.findOne({attributes:['id'],where:{
            
            id:element.id
          }});          

            if(!userinfo){
              console.log("insert ***********",element.id)    
              await models.SendMessage.create(tempobj);
            
            } else {
              console.log("update ***********",element.id) 
              await models.SendMessage.update(tempobj,{where:{id:element.id}});
            }  
 
    }       
     
})
