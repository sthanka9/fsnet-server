const models = require('../models/index');


  models.State.bulkCreate([{
    "id": "1102",
    "name": "Maekel",
    "country_id": "67"
  },
  {
    "id": "1103",
    "name": "Semien-Keih-Bahri",
    "country_id": "67"
  },
  {
    "id": "1104",
    "name": "Harju",
    "country_id": "68"
  },
  {
    "id": "1105",
    "name": "Hiiu",
    "country_id": "68"
  },
  {
    "id": "1106",
    "name": "Ida-Viru",
    "country_id": "68"
  },
  {
    "id": "1107",
    "name": "Jarva",
    "country_id": "68"
  },
  {
    "id": "1108",
    "name": "Jogeva",
    "country_id": "68"
  },
  {
    "id": "1109",
    "name": "Laane",
    "country_id": "68"
  },
  {
    "id": "1110",
    "name": "Laane-Viru",
    "country_id": "68"
  },
  {
    "id": "1111",
    "name": "Parnu",
    "country_id": "68"
  },
  {
    "id": "1112",
    "name": "Polva",
    "country_id": "68"
  },
  {
    "id": "1113",
    "name": "Rapla",
    "country_id": "68"
  },
  {
    "id": "1114",
    "name": "Saare",
    "country_id": "68"
  },
  {
    "id": "1115",
    "name": "Tartu",
    "country_id": "68"
  },
  {
    "id": "1116",
    "name": "Valga",
    "country_id": "68"
  },
  {
    "id": "1117",
    "name": "Viljandi",
    "country_id": "68"
  },
  {
    "id": "1118",
    "name": "Voru",
    "country_id": "68"
  },
  {
    "id": "1119",
    "name": "Addis Abeba",
    "country_id": "69"
  },
  {
    "id": "1120",
    "name": "Afar",
    "country_id": "69"
  },
  {
    "id": "1121",
    "name": "Amhara",
    "country_id": "69"
  },
  {
    "id": "1122",
    "name": "Benishangul",
    "country_id": "69"
  },
  {
    "id": "1123",
    "name": "Diredawa",
    "country_id": "69"
  },
  {
    "id": "1124",
    "name": "Gambella",
    "country_id": "69"
  },
  {
    "id": "1125",
    "name": "Harar",
    "country_id": "69"
  },
  {
    "id": "1126",
    "name": "Jigjiga",
    "country_id": "69"
  },
  {
    "id": "1127",
    "name": "Mekele",
    "country_id": "69"
  },
  {
    "id": "1128",
    "name": "Oromia",
    "country_id": "69"
  },
  {
    "id": "1129",
    "name": "Somali",
    "country_id": "69"
  },
  {
    "id": "1130",
    "name": "Southern",
    "country_id": "69"
  },
  {
    "id": "1131",
    "name": "Tigray",
    "country_id": "69"
  },
  {
    "id": "1132",
    "name": "Christmas Island",
    "country_id": "70"
  },
  {
    "id": "1133",
    "name": "Cocos Islands",
    "country_id": "70"
  },
  {
    "id": "1134",
    "name": "Coral Sea Islands",
    "country_id": "70"
  },
  {
    "id": "1135",
    "name": "Falkland Islands",
    "country_id": "71"
  },
  {
    "id": "1136",
    "name": "South Georgia",
    "country_id": "71"
  },
  {
    "id": "1137",
    "name": "Klaksvik",
    "country_id": "72"
  },
  {
    "id": "1138",
    "name": "Nor ara Eysturoy",
    "country_id": "72"
  },
  {
    "id": "1139",
    "name": "Nor oy",
    "country_id": "72"
  },
  {
    "id": "1140",
    "name": "Sandoy",
    "country_id": "72"
  },
  {
    "id": "1141",
    "name": "Streymoy",
    "country_id": "72"
  },
  {
    "id": "1142",
    "name": "Su uroy",
    "country_id": "72"
  },
  {
    "id": "1143",
    "name": "Sy ra Eysturoy",
    "country_id": "72"
  },
  {
    "id": "1144",
    "name": "Torshavn",
    "country_id": "72"
  },
  {
    "id": "1145",
    "name": "Vaga",
    "country_id": "72"
  },
  {
    "id": "1146",
    "name": "Central",
    "country_id": "73"
  },
  {
    "id": "1147",
    "name": "Eastern",
    "country_id": "73"
  },
  {
    "id": "1148",
    "name": "Northern",
    "country_id": "73"
  },
  {
    "id": "1149",
    "name": "South Pacific",
    "country_id": "73"
  },
  {
    "id": "1150",
    "name": "Western",
    "country_id": "73"
  },
  {
    "id": "1151",
    "name": "Ahvenanmaa",
    "country_id": "74"
  },
  {
    "id": "1152",
    "name": "Etela-Karjala",
    "country_id": "74"
  },
  {
    "id": "1153",
    "name": "Etela-Pohjanmaa",
    "country_id": "74"
  },
  {
    "id": "1154",
    "name": "Etela-Savo",
    "country_id": "74"
  },
  {
    "id": "1155",
    "name": "Etela-Suomen Laani",
    "country_id": "74"
  },
  {
    "id": "1156",
    "name": "Ita-Suomen Laani",
    "country_id": "74"
  },
  {
    "id": "1157",
    "name": "Ita-Uusimaa",
    "country_id": "74"
  },
  {
    "id": "1158",
    "name": "Kainuu",
    "country_id": "74"
  },
  {
    "id": "1159",
    "name": "Kanta-Hame",
    "country_id": "74"
  },
  {
    "id": "1160",
    "name": "Keski-Pohjanmaa",
    "country_id": "74"
  },
  {
    "id": "1161",
    "name": "Keski-Suomi",
    "country_id": "74"
  },
  {
    "id": "1162",
    "name": "Kymenlaakso",
    "country_id": "74"
  },
  {
    "id": "1163",
    "name": "Lansi-Suomen Laani",
    "country_id": "74"
  },
  {
    "id": "1164",
    "name": "Lappi",
    "country_id": "74"
  },
  {
    "id": "1165",
    "name": "Northern Savonia",
    "country_id": "74"
  },
  {
    "id": "1166",
    "name": "Ostrobothnia",
    "country_id": "74"
  },
  {
    "id": "1167",
    "name": "Oulun Laani",
    "country_id": "74"
  },
  {
    "id": "1168",
    "name": "Paijat-Hame",
    "country_id": "74"
  },
  {
    "id": "1169",
    "name": "Pirkanmaa",
    "country_id": "74"
  },
  {
    "id": "1170",
    "name": "Pohjanmaa",
    "country_id": "74"
  },
  {
    "id": "1171",
    "name": "Pohjois-Karjala",
    "country_id": "74"
  },
  {
    "id": "1172",
    "name": "Pohjois-Pohjanmaa",
    "country_id": "74"
  },
  {
    "id": "1173",
    "name": "Pohjois-Savo",
    "country_id": "74"
  },
  {
    "id": "1174",
    "name": "Saarijarvi",
    "country_id": "74"
  },
  {
    "id": "1175",
    "name": "Satakunta",
    "country_id": "74"
  },
  {
    "id": "1176",
    "name": "Southern Savonia",
    "country_id": "74"
  },
  {
    "id": "1177",
    "name": "Tavastia Proper",
    "country_id": "74"
  },
  {
    "id": "1178",
    "name": "Uleaborgs Lan",
    "country_id": "74"
  },
  {
    "id": "1179",
    "name": "Uusimaa",
    "country_id": "74"
  },
  {
    "id": "1180",
    "name": "Varsinais-Suomi",
    "country_id": "74"
  },
  {
    "id": "1181",
    "name": "Ain",
    "country_id": "75"
  },
  {
    "id": "1182",
    "name": "Aisne",
    "country_id": "75"
  },
  {
    "id": "1183",
    "name": "Albi Le Sequestre",
    "country_id": "75"
  },
  {
    "id": "1184",
    "name": "Allier",
    "country_id": "75"
  },
  {
    "id": "1185",
    "name": "Alpes-Cote dAzur",
    "country_id": "75"
  },
  {
    "id": "1186",
    "name": "Alpes-Maritimes",
    "country_id": "75"
  },
  {
    "id": "1187",
    "name": "Alpes-de-Haute-Provence",
    "country_id": "75"
  },
  {
    "id": "1188",
    "name": "Alsace",
    "country_id": "75"
  },
  {
    "id": "1189",
    "name": "Aquitaine",
    "country_id": "75"
  },
  {
    "id": "1190",
    "name": "Ardeche",
    "country_id": "75"
  },
  {
    "id": "1191",
    "name": "Ardennes",
    "country_id": "75"
  },
  {
    "id": "1192",
    "name": "Ariege",
    "country_id": "75"
  },
  {
    "id": "1193",
    "name": "Aube",
    "country_id": "75"
  },
  {
    "id": "1194",
    "name": "Aude",
    "country_id": "75"
  },
  {
    "id": "1195",
    "name": "Auvergne",
    "country_id": "75"
  },
  {
    "id": "1196",
    "name": "Aveyron",
    "country_id": "75"
  },
  {
    "id": "1197",
    "name": "Bas-Rhin",
    "country_id": "75"
  },
  {
    "id": "1198",
    "name": "Basse-Normandie",
    "country_id": "75"
  },
  {
    "id": "1199",
    "name": "Bouches-du-Rhone",
    "country_id": "75"
  },
  {
    "id": "1200",
    "name": "Bourgogne",
    "country_id": "75"
  },
  {
    "id": "1201",
    "name": "Bretagne",
    "country_id": "75"
  },
  {
    "id": "1202",
    "name": "Brittany",
    "country_id": "75"
  },
  {
    "id": "1203",
    "name": "Burgundy",
    "country_id": "75"
  },
  {
    "id": "1204",
    "name": "Calvados",
    "country_id": "75"
  },
  {
    "id": "1205",
    "name": "Cantal",
    "country_id": "75"
  },
  {
    "id": "1206",
    "name": "Cedex",
    "country_id": "75"
  },
  {
    "id": "1207",
    "name": "Centre",
    "country_id": "75"
  },
  {
    "id": "1208",
    "name": "Charente",
    "country_id": "75"
  },
  {
    "id": "1209",
    "name": "Charente-Maritime",
    "country_id": "75"
  },
  {
    "id": "1210",
    "name": "Cher",
    "country_id": "75"
  },
  {
    "id": "1211",
    "name": "Correze",
    "country_id": "75"
  },
  {
    "id": "1212",
    "name": "Corse-du-Sud",
    "country_id": "75"
  },
  {
    "id": "1213",
    "name": "Cote-d\'\'Or",
    "country_id": "75"
  },
  {
    "id": "1214",
    "name": "Cotes-d\'\'Armor",
    "country_id": "75"
  },
  {
    "id": "1215",
    "name": "Creuse",
    "country_id": "75"
  },
  {
    "id": "1216",
    "name": "Crolles",
    "country_id": "75"
  },
  {
    "id": "1217",
    "name": "Deux-Sevres",
    "country_id": "75"
  },
  {
    "id": "1218",
    "name": "Dordogne",
    "country_id": "75"
  },
  {
    "id": "1219",
    "name": "Doubs",
    "country_id": "75"
  },
  {
    "id": "1220",
    "name": "Drome",
    "country_id": "75"
  },
  {
    "id": "1221",
    "name": "Essonne",
    "country_id": "75"
  },
  {
    "id": "1222",
    "name": "Eure",
    "country_id": "75"
  },
  {
    "id": "1223",
    "name": "Eure-et-Loir",
    "country_id": "75"
  },
  {
    "id": "1224",
    "name": "Feucherolles",
    "country_id": "75"
  },
  {
    "id": "1225",
    "name": "Finistere",
    "country_id": "75"
  },
  {
    "id": "1226",
    "name": "Franche-Comte",
    "country_id": "75"
  },
  {
    "id": "1227",
    "name": "Gard",
    "country_id": "75"
  },
  {
    "id": "1228",
    "name": "Gers",
    "country_id": "75"
  },
  {
    "id": "1229",
    "name": "Gironde",
    "country_id": "75"
  },
  {
    "id": "1230",
    "name": "Haut-Rhin",
    "country_id": "75"
  },
  {
    "id": "1231",
    "name": "Haute-Corse",
    "country_id": "75"
  },
  {
    "id": "1232",
    "name": "Haute-Garonne",
    "country_id": "75"
  },
  {
    "id": "1233",
    "name": "Haute-Loire",
    "country_id": "75"
  },
  {
    "id": "1234",
    "name": "Haute-Marne",
    "country_id": "75"
  },
  {
    "id": "1235",
    "name": "Haute-Saone",
    "country_id": "75"
  },
  {
    "id": "1236",
    "name": "Haute-Savoie",
    "country_id": "75"
  },
  {
    "id": "1237",
    "name": "Haute-Vienne",
    "country_id": "75"
  },
  {
    "id": "1238",
    "name": "Hautes-Alpes",
    "country_id": "75"
  },
  {
    "id": "1239",
    "name": "Hautes-Pyrenees",
    "country_id": "75"
  },
  {
    "id": "1240",
    "name": "Hauts-de-Seine",
    "country_id": "75"
  },
  {
    "id": "1241",
    "name": "Herault",
    "country_id": "75"
  },
  {
    "id": "1242",
    "name": "Ile-de-France",
    "country_id": "75"
  },
  {
    "id": "1243",
    "name": "Ille-et-Vilaine",
    "country_id": "75"
  },
  {
    "id": "1244",
    "name": "Indre",
    "country_id": "75"
  },
  {
    "id": "1245",
    "name": "Indre-et-Loire",
    "country_id": "75"
  },
  {
    "id": "1246",
    "name": "Isere",
    "country_id": "75"
  },
  {
    "id": "1247",
    "name": "Jura",
    "country_id": "75"
  },
  {
    "id": "1248",
    "name": "Klagenfurt",
    "country_id": "75"
  },
  {
    "id": "1249",
    "name": "Landes",
    "country_id": "75"
  },
  {
    "id": "1250",
    "name": "Languedoc-Roussillon",
    "country_id": "75"
  },
  {
    "id": "1251",
    "name": "Larcay",
    "country_id": "75"
  },
  {
    "id": "1252",
    "name": "Le Castellet",
    "country_id": "75"
  },
  {
    "id": "1253",
    "name": "Le Creusot",
    "country_id": "75"
  },
  {
    "id": "1254",
    "name": "Limousin",
    "country_id": "75"
  },
  {
    "id": "1255",
    "name": "Loir-et-Cher",
    "country_id": "75"
  },
  {
    "id": "1256",
    "name": "Loire",
    "country_id": "75"
  },
  {
    "id": "1257",
    "name": "Loire-Atlantique",
    "country_id": "75"
  },
  {
    "id": "1258",
    "name": "Loiret",
    "country_id": "75"
  },
  {
    "id": "1259",
    "name": "Lorraine",
    "country_id": "75"
  },
  {
    "id": "1260",
    "name": "Lot",
    "country_id": "75"
  },
  {
    "id": "1261",
    "name": "Lot-et-Garonne",
    "country_id": "75"
  },
  {
    "id": "1262",
    "name": "Lower Normandy",
    "country_id": "75"
  },
  {
    "id": "1263",
    "name": "Lozere",
    "country_id": "75"
  },
  {
    "id": "1264",
    "name": "Maine-et-Loire",
    "country_id": "75"
  },
  {
    "id": "1265",
    "name": "Manche",
    "country_id": "75"
  },
  {
    "id": "1266",
    "name": "Marne",
    "country_id": "75"
  },
  {
    "id": "1267",
    "name": "Mayenne",
    "country_id": "75"
  },
  {
    "id": "1268",
    "name": "Meurthe-et-Moselle",
    "country_id": "75"
  },
  {
    "id": "1269",
    "name": "Meuse",
    "country_id": "75"
  },
  {
    "id": "1270",
    "name": "Midi-Pyrenees",
    "country_id": "75"
  },
  {
    "id": "1271",
    "name": "Morbihan",
    "country_id": "75"
  },
  {
    "id": "1272",
    "name": "Moselle",
    "country_id": "75"
  },
  {
    "id": "1273",
    "name": "Nievre",
    "country_id": "75"
  },
  {
    "id": "1274",
    "name": "Nord",
    "country_id": "75"
  },
  {
    "id": "1275",
    "name": "Nord-Pas-de-Calais",
    "country_id": "75"
  },
  {
    "id": "1276",
    "name": "Oise",
    "country_id": "75"
  },
  {
    "id": "1277",
    "name": "Orne",
    "country_id": "75"
  },
  {
    "id": "1278",
    "name": "Paris",
    "country_id": "75"
  },
  {
    "id": "1279",
    "name": "Pas-de-Calais",
    "country_id": "75"
  },
  {
    "id": "1280",
    "name": "Pays de la Loire",
    "country_id": "75"
  },
  {
    "id": "1281",
    "name": "Pays-de-la-Loire",
    "country_id": "75"
  },
  {
    "id": "1282",
    "name": "Picardy",
    "country_id": "75"
  },
  {
    "id": "1283",
    "name": "Puy-de-Dome",
    "country_id": "75"
  },
  {
    "id": "1284",
    "name": "Pyrenees-Atlantiques",
    "country_id": "75"
  },
  {
    "id": "1285",
    "name": "Pyrenees-Orientales",
    "country_id": "75"
  },
  {
    "id": "1286",
    "name": "Quelmes",
    "country_id": "75"
  },
  {
    "id": "1287",
    "name": "Rhone",
    "country_id": "75"
  },
  {
    "id": "1288",
    "name": "Rhone-Alpes",
    "country_id": "75"
  },
  {
    "id": "1289",
    "name": "Saint Ouen",
    "country_id": "75"
  },
  {
    "id": "1290",
    "name": "Saint Viatre",
    "country_id": "75"
  },
  {
    "id": "1291",
    "name": "Saone-et-Loire",
    "country_id": "75"
  },
  {
    "id": "1292",
    "name": "Sarthe",
    "country_id": "75"
  },
  {
    "id": "1293",
    "name": "Savoie",
    "country_id": "75"
  },
  {
    "id": "1294",
    "name": "Seine-Maritime",
    "country_id": "75"
  },
  {
    "id": "1295",
    "name": "Seine-Saint-Denis",
    "country_id": "75"
  },
  {
    "id": "1296",
    "name": "Seine-et-Marne",
    "country_id": "75"
  },
  {
    "id": "1297",
    "name": "Somme",
    "country_id": "75"
  },
  {
    "id": "1298",
    "name": "Sophia Antipolis",
    "country_id": "75"
  },
  {
    "id": "1299",
    "name": "Souvans",
    "country_id": "75"
  },
  {
    "id": "1300",
    "name": "Tarn",
    "country_id": "75"
  },
  {
    "id": "1301",
    "name": "Tarn-et-Garonne",
    "country_id": "75"
  },
  {
    "id": "1302",
    "name": "Territoire de Belfort",
    "country_id": "75"
  },
  {
    "id": "1303",
    "name": "Treignac",
    "country_id": "75"
  },
  {
    "id": "1304",
    "name": "Upper Normandy",
    "country_id": "75"
  },
  {
    "id": "1305",
    "name": "Val-d\'\'Oise",
    "country_id": "75"
  },
  {
    "id": "1306",
    "name": "Val-de-Marne",
    "country_id": "75"
  },
  {
    "id": "1307",
    "name": "Var",
    "country_id": "75"
  },
  {
    "id": "1308",
    "name": "Vaucluse",
    "country_id": "75"
  },
  {
    "id": "1309",
    "name": "Vellise",
    "country_id": "75"
  },
  {
    "id": "1310",
    "name": "Vendee",
    "country_id": "75"
  },
  {
    "id": "1311",
    "name": "Vienne",
    "country_id": "75"
  },
  {
    "id": "1312",
    "name": "Vosges",
    "country_id": "75"
  },
  {
    "id": "1313",
    "name": "Yonne",
    "country_id": "75"
  },
  {
    "id": "1314",
    "name": "Yvelines",
    "country_id": "75"
  },
  {
    "id": "1315",
    "name": "Cayenne",
    "country_id": "76"
  },
  {
    "id": "1316",
    "name": "Saint-Laurent-du-Maroni",
    "country_id": "76"
  },
  {
    "id": "1317",
    "name": "Iles du Vent",
    "country_id": "77"
  },
  {
    "id": "1318",
    "name": "Iles sous le Vent",
    "country_id": "77"
  },
  {
    "id": "1319",
    "name": "Marquesas",
    "country_id": "77"
  },
  {
    "id": "1320",
    "name": "Tuamotu",
    "country_id": "77"
  },
  {
    "id": "1321",
    "name": "Tubuai",
    "country_id": "77"
  },
  {
    "id": "1322",
    "name": "Amsterdam",
    "country_id": "78"
  },
  {
    "id": "1323",
    "name": "Crozet Islands",
    "country_id": "78"
  },
  {
    "id": "1324",
    "name": "Kerguelen",
    "country_id": "78"
  },
  {
    "id": "1325",
    "name": "Estuaire",
    "country_id": "79"
  },
  {
    "id": "1326",
    "name": "Haut-Ogooue",
    "country_id": "79"
  },
  {
    "id": "1327",
    "name": "Moyen-Ogooue",
    "country_id": "79"
  },
  {
    "id": "1328",
    "name": "Ngounie",
    "country_id": "79"
  },
  {
    "id": "1329",
    "name": "Nyanga",
    "country_id": "79"
  },
  {
    "id": "1330",
    "name": "Ogooue-Ivindo",
    "country_id": "79"
  },
  {
    "id": "1331",
    "name": "Ogooue-Lolo",
    "country_id": "79"
  },
  {
    "id": "1332",
    "name": "Ogooue-Maritime",
    "country_id": "79"
  },
  {
    "id": "1333",
    "name": "Woleu-Ntem",
    "country_id": "79"
  },
  {
    "id": "1334",
    "name": "Banjul",
    "country_id": "80"
  },
  {
    "id": "1335",
    "name": "Basse",
    "country_id": "80"
  },
  {
    "id": "1336",
    "name": "Brikama",
    "country_id": "80"
  },
  {
    "id": "1337",
    "name": "Janjanbureh",
    "country_id": "80"
  },
  {
    "id": "1338",
    "name": "Kanifing",
    "country_id": "80"
  },
  {
    "id": "1339",
    "name": "Kerewan",
    "country_id": "80"
  },
  {
    "id": "1340",
    "name": "Kuntaur",
    "country_id": "80"
  },
  {
    "id": "1341",
    "name": "Mansakonko",
    "country_id": "80"
  },
  {
    "id": "1342",
    "name": "Abhasia",
    "country_id": "81"
  },
  {
    "id": "1343",
    "name": "Ajaria",
    "country_id": "81"
  },
  {
    "id": "1344",
    "name": "Guria",
    "country_id": "81"
  },
  {
    "id": "1345",
    "name": "Imereti",
    "country_id": "81"
  },
  {
    "id": "1346",
    "name": "Kaheti",
    "country_id": "81"
  },
  {
    "id": "1347",
    "name": "Kvemo Kartli",
    "country_id": "81"
  },
  {
    "id": "1348",
    "name": "Mcheta-Mtianeti",
    "country_id": "81"
  },
  {
    "id": "1349",
    "name": "Racha",
    "country_id": "81"
  },
  {
    "id": "1350",
    "name": "Samagrelo-Zemo Svaneti",
    "country_id": "81"
  },
  {
    "id": "1351",
    "name": "Samche-Zhavaheti",
    "country_id": "81"
  },
  {
    "id": "1352",
    "name": "Shida Kartli",
    "country_id": "81"
  },
  {
    "id": "1353",
    "name": "Tbilisi",
    "country_id": "81"
  },
  {
    "id": "1354",
    "name": "Auvergne",
    "country_id": "82"
  },
  {
    "id": "1355",
    "name": "Baden-Wurttemberg",
    "country_id": "82"
  },
  {
    "id": "1356",
    "name": "Bavaria",
    "country_id": "82"
  },
  {
    "id": "1357",
    "name": "Bayern",
    "country_id": "82"
  },
  {
    "id": "1358",
    "name": "Beilstein Wurtt",
    "country_id": "82"
  },
  {
    "id": "1359",
    "name": "Berlin",
    "country_id": "82"
  },
  {
    "id": "1360",
    "name": "Brandenburg",
    "country_id": "82"
  },
  {
    "id": "1361",
    "name": "Bremen",
    "country_id": "82"
  },
  {
    "id": "1362",
    "name": "Dreisbach",
    "country_id": "82"
  },
  {
    "id": "1363",
    "name": "Freistaat Bayern",
    "country_id": "82"
  },
  {
    "id": "1364",
    "name": "Hamburg",
    "country_id": "82"
  },
  {
    "id": "1365",
    "name": "Hannover",
    "country_id": "82"
  },
  {
    "id": "1366",
    "name": "Heroldstatt",
    "country_id": "82"
  },
  {
    "id": "1367",
    "name": "Hessen",
    "country_id": "82"
  },
  {
    "id": "1368",
    "name": "Kortenberg",
    "country_id": "82"
  },
  {
    "id": "1369",
    "name": "Laasdorf",
    "country_id": "82"
  },
  {
    "id": "1370",
    "name": "Land Baden-Wurttemberg",
    "country_id": "82"
  },
  {
    "id": "1371",
    "name": "Land Bayern",
    "country_id": "82"
  },
  {
    "id": "1372",
    "name": "Land Brandenburg",
    "country_id": "82"
  },
  {
    "id": "1373",
    "name": "Land Hessen",
    "country_id": "82"
  },
  {
    "id": "1374",
    "name": "Land Mecklenburg-Vorpommern",
    "country_id": "82"
  },
  {
    "id": "1375",
    "name": "Land Nordrhein-Westfalen",
    "country_id": "82"
  },
  {
    "id": "1376",
    "name": "Land Rheinland-Pfalz",
    "country_id": "82"
  },
  {
    "id": "1377",
    "name": "Land Sachsen",
    "country_id": "82"
  },
  {
    "id": "1378",
    "name": "Land Sachsen-Anhalt",
    "country_id": "82"
  },
  {
    "id": "1379",
    "name": "Land Thuringen",
    "country_id": "82"
  },
  {
    "id": "1380",
    "name": "Lower Saxony",
    "country_id": "82"
  },
  {
    "id": "1381",
    "name": "Mecklenburg-Vorpommern",
    "country_id": "82"
  },
  {
    "id": "1382",
    "name": "Mulfingen",
    "country_id": "82"
  },
  {
    "id": "1383",
    "name": "Munich",
    "country_id": "82"
  },
  {
    "id": "1384",
    "name": "Neubeuern",
    "country_id": "82"
  },
  {
    "id": "1385",
    "name": "Niedersachsen",
    "country_id": "82"
  },
  {
    "id": "1386",
    "name": "Noord-Holland",
    "country_id": "82"
  },
  {
    "id": "1387",
    "name": "Nordrhein-Westfalen",
    "country_id": "82"
  },
  {
    "id": "1388",
    "name": "North Rhine-Westphalia",
    "country_id": "82"
  },
  {
    "id": "1389",
    "name": "Osterode",
    "country_id": "82"
  },
  {
    "id": "1390",
    "name": "Rheinland-Pfalz",
    "country_id": "82"
  },
  {
    "id": "1391",
    "name": "Rhineland-Palatinate",
    "country_id": "82"
  },
  {
    "id": "1392",
    "name": "Saarland",
    "country_id": "82"
  },
  {
    "id": "1393",
    "name": "Sachsen",
    "country_id": "82"
  },
  {
    "id": "1394",
    "name": "Sachsen-Anhalt",
    "country_id": "82"
  },
  {
    "id": "1395",
    "name": "Saxony",
    "country_id": "82"
  },
  {
    "id": "1396",
    "name": "Schleswig-Holstein",
    "country_id": "82"
  },
  {
    "id": "1397",
    "name": "Thuringia",
    "country_id": "82"
  },
  {
    "id": "1398",
    "name": "Webling",
    "country_id": "82"
  },
  {
    "id": "1399",
    "name": "Weinstrabe",
    "country_id": "82"
  },
  {
    "id": "1400",
    "name": "schlobborn",
    "country_id": "82"
  },
  {
    "id": "1401",
    "name": "Ashanti",
    "country_id": "83"
  },
  {
    "id": "1402",
    "name": "Brong-Ahafo",
    "country_id": "83"
  },
  {
    "id": "1403",
    "name": "Central",
    "country_id": "83"
  },
  {
    "id": "1404",
    "name": "Eastern",
    "country_id": "83"
  },
  {
    "id": "1405",
    "name": "Greater Accra",
    "country_id": "83"
  },
  {
    "id": "1406",
    "name": "Northern",
    "country_id": "83"
  },
  {
    "id": "1407",
    "name": "Upper East",
    "country_id": "83"
  },
  {
    "id": "1408",
    "name": "Upper West",
    "country_id": "83"
  },
  {
    "id": "1409",
    "name": "Volta",
    "country_id": "83"
  },
  {
    "id": "1410",
    "name": "Western",
    "country_id": "83"
  },
  {
    "id": "1411",
    "name": "Gibraltar",
    "country_id": "84"
  },
  {
    "id": "1412",
    "name": "Acharnes",
    "country_id": "85"
  },
  {
    "id": "1413",
    "name": "Ahaia",
    "country_id": "85"
  },
  {
    "id": "1414",
    "name": "Aitolia kai Akarnania",
    "country_id": "85"
  },
  {
    "id": "1415",
    "name": "Argolis",
    "country_id": "85"
  },
  {
    "id": "1416",
    "name": "Arkadia",
    "country_id": "85"
  },
  {
    "id": "1417",
    "name": "Arta",
    "country_id": "85"
  },
  {
    "id": "1418",
    "name": "Attica",
    "country_id": "85"
  },
  {
    "id": "1419",
    "name": "Attiki",
    "country_id": "85"
  },
  {
    "id": "1420",
    "name": "Ayion Oros",
    "country_id": "85"
  },
  {
    "id": "1421",
    "name": "Crete",
    "country_id": "85"
  },
  {
    "id": "1422",
    "name": "Dodekanisos",
    "country_id": "85"
  },
  {
    "id": "1423",
    "name": "Drama",
    "country_id": "85"
  },
  {
    "id": "1424",
    "name": "Evia",
    "country_id": "85"
  },
  {
    "id": "1425",
    "name": "Evritania",
    "country_id": "85"
  },
  {
    "id": "1426",
    "name": "Evros",
    "country_id": "85"
  },
  {
    "id": "1427",
    "name": "Evvoia",
    "country_id": "85"
  },
  {
    "id": "1428",
    "name": "Florina",
    "country_id": "85"
  },
  {
    "id": "1429",
    "name": "Fokis",
    "country_id": "85"
  },
  {
    "id": "1430",
    "name": "Fthiotis",
    "country_id": "85"
  },
  {
    "id": "1431",
    "name": "Grevena",
    "country_id": "85"
  },
  {
    "id": "1432",
    "name": "Halandri",
    "country_id": "85"
  },
  {
    "id": "1433",
    "name": "Halkidiki",
    "country_id": "85"
  },
  {
    "id": "1434",
    "name": "Hania",
    "country_id": "85"
  },
  {
    "id": "1435",
    "name": "Heraklion",
    "country_id": "85"
  },
  {
    "id": "1436",
    "name": "Hios",
    "country_id": "85"
  },
  {
    "id": "1437",
    "name": "Ilia",
    "country_id": "85"
  },
  {
    "id": "1438",
    "name": "Imathia",
    "country_id": "85"
  },
  {
    "id": "1439",
    "name": "Ioannina",
    "country_id": "85"
  },
  {
    "id": "1440",
    "name": "Iraklion",
    "country_id": "85"
  },
  {
    "id": "1441",
    "name": "Karditsa",
    "country_id": "85"
  },
  {
    "id": "1442",
    "name": "Kastoria",
    "country_id": "85"
  },
  {
    "id": "1443",
    "name": "Kavala",
    "country_id": "85"
  },
  {
    "id": "1444",
    "name": "Kefallinia",
    "country_id": "85"
  },
  {
    "id": "1445",
    "name": "Kerkira",
    "country_id": "85"
  },
  {
    "id": "1446",
    "name": "Kiklades",
    "country_id": "85"
  },
  {
    "id": "1447",
    "name": "Kilkis",
    "country_id": "85"
  },
  {
    "id": "1448",
    "name": "Korinthia",
    "country_id": "85"
  },
  {
    "id": "1449",
    "name": "Kozani",
    "country_id": "85"
  },
  {
    "id": "1450",
    "name": "Lakonia",
    "country_id": "85"
  },
  {
    "id": "1451",
    "name": "Larisa",
    "country_id": "85"
  },
  {
    "id": "1452",
    "name": "Lasithi",
    "country_id": "85"
  },
  {
    "id": "1453",
    "name": "Lesvos",
    "country_id": "85"
  },
  {
    "id": "1454",
    "name": "Levkas",
    "country_id": "85"
  },
  {
    "id": "1455",
    "name": "Magnisia",
    "country_id": "85"
  },
  {
    "id": "1456",
    "name": "Messinia",
    "country_id": "85"
  },
  {
    "id": "1457",
    "name": "Nomos Attikis",
    "country_id": "85"
  },
  {
    "id": "1458",
    "name": "Nomos Zakynthou",
    "country_id": "85"
  },
  {
    "id": "1459",
    "name": "Pella",
    "country_id": "85"
  },
  {
    "id": "1460",
    "name": "Pieria",
    "country_id": "85"
  },
  {
    "id": "1461",
    "name": "Piraios",
    "country_id": "85"
  },
  {
    "id": "1462",
    "name": "Preveza",
    "country_id": "85"
  },
  {
    "id": "1463",
    "name": "Rethimni",
    "country_id": "85"
  },
  {
    "id": "1464",
    "name": "Rodopi",
    "country_id": "85"
  },
  {
    "id": "1465",
    "name": "Samos",
    "country_id": "85"
  },
  {
    "id": "1466",
    "name": "Serrai",
    "country_id": "85"
  },
  {
    "id": "1467",
    "name": "Thesprotia",
    "country_id": "85"
  },
  {
    "id": "1468",
    "name": "Thessaloniki",
    "country_id": "85"
  },
  {
    "id": "1469",
    "name": "Trikala",
    "country_id": "85"
  },
  {
    "id": "1470",
    "name": "Voiotia",
    "country_id": "85"
  },
  {
    "id": "1471",
    "name": "West Greece",
    "country_id": "85"
  },
  {
    "id": "1472",
    "name": "Xanthi",
    "country_id": "85"
  },
  {
    "id": "1473",
    "name": "Zakinthos",
    "country_id": "85"
  },
  {
    "id": "1474",
    "name": "Aasiaat",
    "country_id": "86"
  },
  {
    "id": "1475",
    "name": "Ammassalik",
    "country_id": "86"
  },
  {
    "id": "1476",
    "name": "Illoqqortoormiut",
    "country_id": "86"
  },
  {
    "id": "1477",
    "name": "Ilulissat",
    "country_id": "86"
  },
  {
    "id": "1478",
    "name": "Ivittuut",
    "country_id": "86"
  },
  {
    "id": "1479",
    "name": "Kangaatsiaq",
    "country_id": "86"
  },
  {
    "id": "1480",
    "name": "Maniitsoq",
    "country_id": "86"
  },
  {
    "id": "1481",
    "name": "Nanortalik",
    "country_id": "86"
  },
  {
    "id": "1482",
    "name": "Narsaq",
    "country_id": "86"
  },
  {
    "id": "1483",
    "name": "Nuuk",
    "country_id": "86"
  },
  {
    "id": "1484",
    "name": "Paamiut",
    "country_id": "86"
  },
  {
    "id": "1485",
    "name": "Qaanaaq",
    "country_id": "86"
  },
  {
    "id": "1486",
    "name": "Qaqortoq",
    "country_id": "86"
  },
  {
    "id": "1487",
    "name": "Qasigiannguit",
    "country_id": "86"
  },
  {
    "id": "1488",
    "name": "Qeqertarsuaq",
    "country_id": "86"
  },
  {
    "id": "1489",
    "name": "Sisimiut",
    "country_id": "86"
  },
  {
    "id": "1490",
    "name": "Udenfor kommunal inddeling",
    "country_id": "86"
  },
  {
    "id": "1491",
    "name": "Upernavik",
    "country_id": "86"
  },
  {
    "id": "1492",
    "name": "Uummannaq",
    "country_id": "86"
  },
  {
    "id": "1493",
    "name": "Carriacou-Petite Martinique",
    "country_id": "87"
  },
  {
    "id": "1494",
    "name": "Saint Andrew",
    "country_id": "87"
  },
  {
    "id": "1495",
    "name": "Saint Davids",
    "country_id": "87"
  },
  {
    "id": "1496",
    "name": "Saint George\'\'s",
    "country_id": "87"
  },
  {
    "id": "1497",
    "name": "Saint John",
    "country_id": "87"
  },
  {
    "id": "1498",
    "name": "Saint Mark",
    "country_id": "87"
  },
  {
    "id": "1499",
    "name": "Saint Patrick",
    "country_id": "87"
  },
  {
    "id": "1500",
    "name": "Basse-Terre",
    "country_id": "88"
  },
  {
    "id": "1501",
    "name": "Grande-Terre",
    "country_id": "88"
  },
  {
    "id": "1502",
    "name": "Iles des Saintes",
    "country_id": "88"
  },
  {
    "id": "1503",
    "name": "La Desirade",
    "country_id": "88"
  },
  {
    "id": "1504",
    "name": "Marie-Galante",
    "country_id": "88"
  },
  {
    "id": "1505",
    "name": "Saint Barthelemy",
    "country_id": "88"
  },
  {
    "id": "1506",
    "name": "Saint Martin",
    "country_id": "88"
  },
  {
    "id": "1507",
    "name": "Agana Heights",
    "country_id": "89"
  },
  {
    "id": "1508",
    "name": "Agat",
    "country_id": "89"
  },
  {
    "id": "1509",
    "name": "Barrigada",
    "country_id": "89"
  },
  {
    "id": "1510",
    "name": "Chalan-Pago-Ordot",
    "country_id": "89"
  },
  {
    "id": "1511",
    "name": "Dededo",
    "country_id": "89"
  },
  {
    "id": "1512",
    "name": "Hagatna",
    "country_id": "89"
  },
  {
    "id": "1513",
    "name": "Inarajan",
    "country_id": "89"
  },
  {
    "id": "1514",
    "name": "Mangilao",
    "country_id": "89"
  },
  {
    "id": "1515",
    "name": "Merizo",
    "country_id": "89"
  },
  {
    "id": "1516",
    "name": "Mongmong-Toto-Maite",
    "country_id": "89"
  },
  {
    "id": "1517",
    "name": "Santa Rita",
    "country_id": "89"
  },
  {
    "id": "1518",
    "name": "Sinajana",
    "country_id": "89"
  },
  {
    "id": "1519",
    "name": "Talofofo",
    "country_id": "89"
  },
  {
    "id": "1520",
    "name": "Tamuning",
    "country_id": "89"
  },
  {
    "id": "1521",
    "name": "Yigo",
    "country_id": "89"
  },
  {
    "id": "1522",
    "name": "Yona",
    "country_id": "89"
  },
  {
    "id": "1523",
    "name": "Alta Verapaz",
    "country_id": "90"
  },
  {
    "id": "1524",
    "name": "Baja Verapaz",
    "country_id": "90"
  },
  {
    "id": "1525",
    "name": "Chimaltenango",
    "country_id": "90"
  },
  {
    "id": "1526",
    "name": "Chiquimula",
    "country_id": "90"
  },
  {
    "id": "1527",
    "name": "El Progreso",
    "country_id": "90"
  },
  {
    "id": "1528",
    "name": "Escuintla",
    "country_id": "90"
  },
  {
    "id": "1529",
    "name": "Guatemala",
    "country_id": "90"
  },
  {
    "id": "1530",
    "name": "Huehuetenango",
    "country_id": "90"
  },
  {
    "id": "1531",
    "name": "Izabal",
    "country_id": "90"
  },
  {
    "id": "1532",
    "name": "Jalapa",
    "country_id": "90"
  },
  {
    "id": "1533",
    "name": "Jutiapa",
    "country_id": "90"
  },
  {
    "id": "1534",
    "name": "Peten",
    "country_id": "90"
  },
  {
    "id": "1535",
    "name": "Quezaltenango",
    "country_id": "90"
  },
  {
    "id": "1536",
    "name": "Quiche",
    "country_id": "90"
  },
  {
    "id": "1537",
    "name": "Retalhuleu",
    "country_id": "90"
  },
  {
    "id": "1538",
    "name": "Sacatepequez",
    "country_id": "90"
  },
  {
    "id": "1539",
    "name": "San Marcos",
    "country_id": "90"
  },
  {
    "id": "1540",
    "name": "Santa Rosa",
    "country_id": "90"
  },
  {
    "id": "1541",
    "name": "Solola",
    "country_id": "90"
  },
  {
    "id": "1542",
    "name": "Suchitepequez",
    "country_id": "90"
  },
  {
    "id": "1543",
    "name": "Totonicapan",
    "country_id": "90"
  },
  {
    "id": "1544",
    "name": "Zacapa",
    "country_id": "90"
  },
  {
    "id": "1545",
    "name": "Alderney",
    "country_id": "91"
  },
  {
    "id": "1546",
    "name": "Castel",
    "country_id": "91"
  },
  {
    "id": "1547",
    "name": "Forest",
    "country_id": "91"
  },
  {
    "id": "1548",
    "name": "Saint Andrew",
    "country_id": "91"
  },
  {
    "id": "1549",
    "name": "Saint Martin",
    "country_id": "91"
  },
  {
    "id": "1550",
    "name": "Saint Peter Port",
    "country_id": "91"
  },
  {
    "id": "1551",
    "name": "Saint Pierre du Bois",
    "country_id": "91"
  },
  {
    "id": "1552",
    "name": "Saint Sampson",
    "country_id": "91"
  },
  {
    "id": "1553",
    "name": "Saint Saviour",
    "country_id": "91"
  },
  {
    "id": "1554",
    "name": "Sark",
    "country_id": "91"
  },
  {
    "id": "1555",
    "name": "Torteval",
    "country_id": "91"
  },
  {
    "id": "1556",
    "name": "Vale",
    "country_id": "91"
  },
  {
    "id": "1557",
    "name": "Beyla",
    "country_id": "92"
  },
  {
    "id": "1558",
    "name": "Boffa",
    "country_id": "92"
  },
  {
    "id": "1559",
    "name": "Boke",
    "country_id": "92"
  },
  {
    "id": "1560",
    "name": "Conakry",
    "country_id": "92"
  },
  {
    "id": "1561",
    "name": "Coyah",
    "country_id": "92"
  },
  {
    "id": "1562",
    "name": "Dabola",
    "country_id": "92"
  },
  {
    "id": "1563",
    "name": "Dalaba",
    "country_id": "92"
  },
  {
    "id": "1564",
    "name": "Dinguiraye",
    "country_id": "92"
  },
  {
    "id": "1565",
    "name": "Faranah",
    "country_id": "92"
  },
  {
    "id": "1566",
    "name": "Forecariah",
    "country_id": "92"
  },
  {
    "id": "1567",
    "name": "Fria",
    "country_id": "92"
  },
  {
    "id": "1568",
    "name": "Gaoual",
    "country_id": "92"
  },
  {
    "id": "1569",
    "name": "Gueckedou",
    "country_id": "92"
  },
  {
    "id": "1570",
    "name": "Kankan",
    "country_id": "92"
  },
  {
    "id": "1571",
    "name": "Kerouane",
    "country_id": "92"
  },
  {
    "id": "1572",
    "name": "Kindia",
    "country_id": "92"
  },
  {
    "id": "1573",
    "name": "Kissidougou",
    "country_id": "92"
  },
  {
    "id": "1574",
    "name": "Koubia",
    "country_id": "92"
  },
  {
    "id": "1575",
    "name": "Koundara",
    "country_id": "92"
  },
  {
    "id": "1576",
    "name": "Kouroussa",
    "country_id": "92"
  },
  {
    "id": "1577",
    "name": "Labe",
    "country_id": "92"
  },
  {
    "id": "1578",
    "name": "Lola",
    "country_id": "92"
  },
  {
    "id": "1579",
    "name": "Macenta",
    "country_id": "92"
  },
  {
    "id": "1580",
    "name": "Mali",
    "country_id": "92"
  },
  {
    "id": "1581",
    "name": "Mamou",
    "country_id": "92"
  },
  {
    "id": "1582",
    "name": "Mandiana",
    "country_id": "92"
  },
  {
    "id": "1583",
    "name": "Nzerekore",
    "country_id": "92"
  },
  {
    "id": "1584",
    "name": "Pita",
    "country_id": "92"
  },
  {
    "id": "1585",
    "name": "Siguiri",
    "country_id": "92"
  },
  {
    "id": "1586",
    "name": "Telimele",
    "country_id": "92"
  },
  {
    "id": "1587",
    "name": "Tougue",
    "country_id": "92"
  },
  {
    "id": "1588",
    "name": "Yomou",
    "country_id": "92"
  },
  {
    "id": "1589",
    "name": "Bafata",
    "country_id": "93"
  },
  {
    "id": "1590",
    "name": "Bissau",
    "country_id": "93"
  },
  {
    "id": "1591",
    "name": "Bolama",
    "country_id": "93"
  },
  {
    "id": "1592",
    "name": "Cacheu",
    "country_id": "93"
  },
  {
    "id": "1593",
    "name": "Gabu",
    "country_id": "93"
  },
  {
    "id": "1594",
    "name": "Oio",
    "country_id": "93"
  },
  {
    "id": "1595",
    "name": "Quinara",
    "country_id": "93"
  },
  {
    "id": "1596",
    "name": "Tombali",
    "country_id": "93"
  },
  {
    "id": "1597",
    "name": "Barima-Waini",
    "country_id": "94"
  },
  {
    "id": "1598",
    "name": "Cuyuni-Mazaruni",
    "country_id": "94"
  },
  {
    "id": "1599",
    "name": "Demerara-Mahaica",
    "country_id": "94"
  },
  {
    "id": "1600",
    "name": "East Berbice-Corentyne",
    "country_id": "94"
  },
  {
    "id": "1601",
    "name": "Essequibo Islands-West Demerar",
    "country_id": "94"
  },
  {
    "id": "1602",
    "name": "Mahaica-Berbice",
    "country_id": "94"
  },
  {
    "id": "1603",
    "name": "Pomeroon-Supenaam",
    "country_id": "94"
  },
  {
    "id": "1604",
    "name": "Potaro-Siparuni",
    "country_id": "94"
  },
  {
    "id": "1605",
    "name": "Upper Demerara-Berbice",
    "country_id": "94"
  },
  {
    "id": "1606",
    "name": "Upper Takutu-Upper Essequibo",
    "country_id": "94"
  },
  {
    "id": "1607",
    "name": "Artibonite",
    "country_id": "95"
  },
  {
    "id": "1608",
    "name": "Centre",
    "country_id": "95"
  },
  {
    "id": "1609",
    "name": "Grand\'\'Anse",
    "country_id": "95"
  },
  {
    "id": "1610",
    "name": "Nord",
    "country_id": "95"
  },
  {
    "id": "1611",
    "name": "Nord-Est",
    "country_id": "95"
  },
  {
    "id": "1612",
    "name": "Nord-Ouest",
    "country_id": "95"
  },
  {
    "id": "1613",
    "name": "Ouest",
    "country_id": "95"
  },
  {
    "id": "1614",
    "name": "Sud",
    "country_id": "95"
  },
  {
    "id": "1615",
    "name": "Sud-Est",
    "country_id": "95"
  },
  {
    "id": "1616",
    "name": "Heard and McDonald Islands",
    "country_id": "96"
  },
  {
    "id": "1617",
    "name": "Atlantida",
    "country_id": "97"
  },
  {
    "id": "1618",
    "name": "Choluteca",
    "country_id": "97"
  },
  {
    "id": "1619",
    "name": "Colon",
    "country_id": "97"
  },
  {
    "id": "1620",
    "name": "Comayagua",
    "country_id": "97"
  },
  {
    "id": "1621",
    "name": "Copan",
    "country_id": "97"
  },
  {
    "id": "1622",
    "name": "Cortes",
    "country_id": "97"
  },
  {
    "id": "1623",
    "name": "Distrito Central",
    "country_id": "97"
  },
  {
    "id": "1624",
    "name": "El Paraiso",
    "country_id": "97"
  },
  {
    "id": "1625",
    "name": "Francisco Morazan",
    "country_id": "97"
  },
  {
    "id": "1626",
    "name": "Gracias a Dios",
    "country_id": "97"
  },
  {
    "id": "1627",
    "name": "Intibuca",
    "country_id": "97"
  },
  {
    "id": "1628",
    "name": "Islas de la Bahia",
    "country_id": "97"
  },
  {
    "id": "1629",
    "name": "La Paz",
    "country_id": "97"
  },
  {
    "id": "1630",
    "name": "Lempira",
    "country_id": "97"
  },
  {
    "id": "1631",
    "name": "Ocotepeque",
    "country_id": "97"
  },
  {
    "id": "1632",
    "name": "Olancho",
    "country_id": "97"
  },
  {
    "id": "1633",
    "name": "Santa Barbara",
    "country_id": "97"
  },
  {
    "id": "1634",
    "name": "Valle",
    "country_id": "97"
  },
  {
    "id": "1635",
    "name": "Yoro",
    "country_id": "97"
  },
  {
    "id": "1636",
    "name": "Hong Kong",
    "country_id": "98"
  },
  {
    "id": "1637",
    "name": "Bacs-Kiskun",
    "country_id": "99"
  },
  {
    "id": "1638",
    "name": "Baranya",
    "country_id": "99"
  },
  {
    "id": "1639",
    "name": "Bekes",
    "country_id": "99"
  },
  {
    "id": "1640",
    "name": "Borsod-Abauj-Zemplen",
    "country_id": "99"
  },
  {
    "id": "1641",
    "name": "Budapest",
    "country_id": "99"
  },
  {
    "id": "1642",
    "name": "Csongrad",
    "country_id": "99"
  },
  {
    "id": "1643",
    "name": "Fejer",
    "country_id": "99"
  },
  {
    "id": "1644",
    "name": "Gyor-Moson-Sopron",
    "country_id": "99"
  },
  {
    "id": "1645",
    "name": "Hajdu-Bihar",
    "country_id": "99"
  },
  {
    "id": "1646",
    "name": "Heves",
    "country_id": "99"
  },
  {
    "id": "1647",
    "name": "Jasz-Nagykun-Szolnok",
    "country_id": "99"
  },
  {
    "id": "1648",
    "name": "Komarom-Esztergom",
    "country_id": "99"
  },
  {
    "id": "1649",
    "name": "Nograd",
    "country_id": "99"
  },
  {
    "id": "1650",
    "name": "Pest",
    "country_id": "99"
  },
  {
    "id": "1651",
    "name": "Somogy",
    "country_id": "99"
  },
  {
    "id": "1652",
    "name": "Szabolcs-Szatmar-Bereg",
    "country_id": "99"
  },
  {
    "id": "1653",
    "name": "Tolna",
    "country_id": "99"
  },
  {
    "id": "1654",
    "name": "Vas",
    "country_id": "99"
  },
  {
    "id": "1655",
    "name": "Veszprem",
    "country_id": "99"
  },
  {
    "id": "1656",
    "name": "Zala",
    "country_id": "99"
  },
  {
    "id": "1657",
    "name": "Austurland",
    "country_id": "100"
  },
  {
    "id": "1658",
    "name": "Gullbringusysla",
    "country_id": "100"
  },
  {
    "id": "1659",
    "name": "Hofu borgarsva i",
    "country_id": "100"
  },
  {
    "id": "1660",
    "name": "Nor urland eystra",
    "country_id": "100"
  },
  {
    "id": "1661",
    "name": "Nor urland vestra",
    "country_id": "100"
  },
  {
    "id": "1662",
    "name": "Su urland",
    "country_id": "100"
  },
  {
    "id": "1663",
    "name": "Su urnes",
    "country_id": "100"
  },
  {
    "id": "1664",
    "name": "Vestfir ir",
    "country_id": "100"
  },
  {
    "id": "1665",
    "name": "Vesturland",
    "country_id": "100"
  },
  {
    "id": "1666",
    "name": "Aceh",
    "country_id": "102"
  },
  {
    "id": "1667",
    "name": "Bali",
    "country_id": "102"
  },
  {
    "id": "1668",
    "name": "Bangka-Belitung",
    "country_id": "102"
  },
  {
    "id": "1669",
    "name": "Banten",
    "country_id": "102"
  },
  {
    "id": "1670",
    "name": "Bengkulu",
    "country_id": "102"
  },
  {
    "id": "1671",
    "name": "Gandaria",
    "country_id": "102"
  },
  {
    "id": "1672",
    "name": "Gorontalo",
    "country_id": "102"
  },
  {
    "id": "1673",
    "name": "Jakarta",
    "country_id": "102"
  },
  {
    "id": "1674",
    "name": "Jambi",
    "country_id": "102"
  },
  {
    "id": "1675",
    "name": "Jawa Barat",
    "country_id": "102"
  },
  {
    "id": "1676",
    "name": "Jawa Tengah",
    "country_id": "102"
  },
  {
    "id": "1677",
    "name": "Jawa Timur",
    "country_id": "102"
  },
  {
    "id": "1678",
    "name": "Kalimantan Barat",
    "country_id": "102"
  },
  {
    "id": "1679",
    "name": "Kalimantan Selatan",
    "country_id": "102"
  },
  {
    "id": "1680",
    "name": "Kalimantan Tengah",
    "country_id": "102"
  },
  {
    "id": "1681",
    "name": "Kalimantan Timur",
    "country_id": "102"
  },
  {
    "id": "1682",
    "name": "Kendal",
    "country_id": "102"
  },
  {
    "id": "1683",
    "name": "Lampung",
    "country_id": "102"
  },
  {
    "id": "1684",
    "name": "Maluku",
    "country_id": "102"
  },
  {
    "id": "1685",
    "name": "Maluku Utara",
    "country_id": "102"
  },
  {
    "id": "1686",
    "name": "Nusa Tenggara Barat",
    "country_id": "102"
  },
  {
    "id": "1687",
    "name": "Nusa Tenggara Timur",
    "country_id": "102"
  },
  {
    "id": "1688",
    "name": "Papua",
    "country_id": "102"
  },
  {
    "id": "1689",
    "name": "Riau",
    "country_id": "102"
  },
  {
    "id": "1690",
    "name": "Riau Kepulauan",
    "country_id": "102"
  },
  {
    "id": "1691",
    "name": "Solo",
    "country_id": "102"
  },
  {
    "id": "1692",
    "name": "Sulawesi Selatan",
    "country_id": "102"
  },
  {
    "id": "1693",
    "name": "Sulawesi Tengah",
    "country_id": "102"
  },
  {
    "id": "1694",
    "name": "Sulawesi Tenggara",
    "country_id": "102"
  },
  {
    "id": "1695",
    "name": "Sulawesi Utara",
    "country_id": "102"
  }
]);