const models = require('../models/index');
const pmodels = require('../production_models'); 

pmodels.prod_sequelize.query("SELECT * FROM VcFrimDelegates ", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async users => {
  
   
      for (let i = 0; i < users.length ; i++) {

        let element = users[i]   
 

        let  tempobj = {
          id:element.id,
          vcfirmId: element.vcfirmId,
          delegateId: element.delegateId,
          type: element.type,
          createdBy: element.createdBy,
          updatedBy: element.updatedBy,
          deletedAt: element.deletedAt,
        }        
        
        let vcinfo = await models.VCFirm.findOne({attributes:['id'],where:{  
          id:element.vcfirmId 
        }})

        if(vcinfo){
          
              let userinfo = await models.VcFrimDelegate.findOne({attributes:['id'],where:{
                vcfirmId:element.vcfirmId,
                delegateId:element.delegateId,
                type:element.type
              }});

              if(!userinfo){
              console.log("insert ***********",element.id)    
              await models.VcFrimDelegate.create(tempobj);
            
            } else {
              console.log("update ***********",element.id)  

              await models.VcFrimDelegate.update(tempobj,{where:{id:element.id}});
            }

        }
 
    }       
     
})


 