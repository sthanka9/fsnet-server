const models = require('../models/index');
const pmodels = require('../production_models'); 
const fs = require('fs');
const uuidv1 = require('uuid/v1')
const commonHelper = require('../helpers/commonHelper');

pmodels.prod_sequelize.query("SELECT * FROM Funds   ", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async users => {
  
   
      for (let i = 0; i < users.length ; i++) {

      let element = users[i]
      let filePath = `./assets/funds/${element.id}/CONSOLIDATED_FUND_AGREEMENT.pdf`;       

      commonHelper.ensureDirectoryExistence(filePath);

      if(element.consolidateFundAgreement!='' && fs.existsSync(element.consolidateFundAgreement)){
        fs.copyFileSync(element.consolidateFundAgreement, filePath);  
      }
        
        
        let  tempobj = {
          fundId: element.id,
          filePath: filePath,
          createdBy:element.createdBy,
          updatedBy:element.updatedBy,
          isPrimaryGpSigned:1,
          isPrimaryLpSigned:1,
          gpSignCount:0,
          lpSignCount:0,
          isPrimaryGpSigned:1,
          isPrimaryLpSigned:1,
          docType: 'CONSOLIDATED_FUND_AGREEMENT',
          isActive:1          
        }


        let info = await models.DocumentsForSignature.findOne({attributes:['id'],where:{  
          fundId:element.id,
          docType:'CONSOLIDATED_FUND_AGREEMENT' 
        }})

        if(info){
          //update
            await models.DocumentsForSignature.update(tempobj,{where:{fundId:element.id,docType:'CONSOLIDATED_FUND_AGREEMENT'}});

        } else {
          //insert
            await models.DocumentsForSignature.create(tempobj)
        }

    
 
    }       
     
})


 