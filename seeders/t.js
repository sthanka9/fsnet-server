const { series } = require('async');
const { exec } = require('child_process');


var childProcess = require('child_process');

function runScript(scriptPath, callback) {

    // keep track of whether callback has been invoked to prevent multiple invocations
    var invoked = false;

    var process = childProcess.fork(scriptPath);

    // listen for errors as they may prevent the exit event from firing
    process.on('error', function (err) {
        if (invoked) return;
        invoked = true;
        callback(err);
    });

    // execute the callback once the process has finished running
    process.on('exit', function (code) {
        if (invoked) return;
        invoked = true;
        var err = code === 0 ? null : new Error('exit code ' + code);
        callback(err);
    });

}


series([
    function (callback) {
        runScript('./seeders/rolesAndPermissions.js', callback);
    },
    function (callback) {
        runScript('./seeders/Permissions.js', callback);
    },
    function (callback) {
        runScript('./seeders/lookupData.js', callback);
    },
    function (callback) {
        runScript('./seeders/1users.js', callback);
    },
    function (callback) {
        runScript('./seeders/2userroles.js', callback);
    },
    function (callback) {
        runScript('./seeders/23createadmin.js', callback);
    },
    function (callback) {
        runScript('./seeders/3converttoaccounts.js', callback);
    },
    function (callback) {
        runScript('./seeders/importVcFirms.js', callback);
    },
    function (callback) {
        runScript('./seeders/4vcfirmdelegates.js', callback);
    },
    function (callback) {
        runScript('./seeders/importFunds.js', callback);
    },
    function (callback) {
        runScript('./seeders/importFundSubscriptions.js', callback);
    },
    function (callback) {
        runScript('./seeders/importFundClosings.js', callback);
    },
    function (callback) {
        runScript('./seeders/5gpdelegates.js', callback);
    },
    function (callback) {
        runScript('./seeders/6gpdelegateapprovals.js', callback);
    },
    function (callback) {
        runScript('./seeders/7lpdelegates.js', callback);
    },
    function (callback) {
        runScript('./seeders/8notifications.js', callback);
    },
    function (callback) {
        runScript('./seeders/9updateaccountidfornotifications.js', callback);
    },
    function (callback) {
        runScript('./seeders/10sendmessages.js', callback);
    },
    function (callback) {
        runScript('./seeders/11changecommitments.js', callback);
    },
    function (callback) {
        runScript('./seeders/12vcfirmlps.js', callback);
    },
    function (callback) {
        runScript('./seeders/10Docs.js', callback);
    },
    function (callback) {
        runScript('./seeders/13consolidatedfund.js', callback);
    }
], function (err, results) {
    console.log(results);
    // results is now equal to: {one: 1, two: 2}
});

//4>seeders/countries.txt
//5>seeders/states.txt

