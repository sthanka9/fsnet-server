const models = require('../models/index');


  models.State.bulkCreate([{
    "id": "200",
    "name": "Unclaimed Sector",
    "country_id": "8"
  },
  {
    "id": "201",
    "name": "Barbuda",
    "country_id": "9"
  },
  {
    "id": "202",
    "name": "Saint George",
    "country_id": "9"
  },
  {
    "id": "203",
    "name": "Saint John",
    "country_id": "9"
  },
  {
    "id": "204",
    "name": "Saint Mary",
    "country_id": "9"
  },
  {
    "id": "205",
    "name": "Saint Paul",
    "country_id": "9"
  },
  {
    "id": "206",
    "name": "Saint Peter",
    "country_id": "9"
  },
  {
    "id": "207",
    "name": "Saint Philip",
    "country_id": "9"
  },
  {
    "id": "208",
    "name": "Buenos Aires",
    "country_id": "10"
  },
  {
    "id": "209",
    "name": "Catamarca",
    "country_id": "10"
  },
  {
    "id": "210",
    "name": "Chaco",
    "country_id": "10"
  },
  {
    "id": "211",
    "name": "Chubut",
    "country_id": "10"
  },
  {
    "id": "212",
    "name": "Cordoba",
    "country_id": "10"
  },
  {
    "id": "213",
    "name": "Corrientes",
    "country_id": "10"
  },
  {
    "id": "214",
    "name": "Distrito Federal",
    "country_id": "10"
  },
  {
    "id": "215",
    "name": "Entre Rios",
    "country_id": "10"
  },
  {
    "id": "216",
    "name": "Formosa",
    "country_id": "10"
  },
  {
    "id": "217",
    "name": "Jujuy",
    "country_id": "10"
  },
  {
    "id": "218",
    "name": "La Pampa",
    "country_id": "10"
  },
  {
    "id": "219",
    "name": "La Rioja",
    "country_id": "10"
  },
  {
    "id": "220",
    "name": "Mendoza",
    "country_id": "10"
  },
  {
    "id": "221",
    "name": "Misiones",
    "country_id": "10"
  },
  {
    "id": "222",
    "name": "Neuquen",
    "country_id": "10"
  },
  {
    "id": "223",
    "name": "Rio Negro",
    "country_id": "10"
  },
  {
    "id": "224",
    "name": "Salta",
    "country_id": "10"
  },
  {
    "id": "225",
    "name": "San Juan",
    "country_id": "10"
  },
  {
    "id": "226",
    "name": "San Luis",
    "country_id": "10"
  },
  {
    "id": "227",
    "name": "Santa Cruz",
    "country_id": "10"
  },
  {
    "id": "228",
    "name": "Santa Fe",
    "country_id": "10"
  },
  {
    "id": "229",
    "name": "Santiago del Estero",
    "country_id": "10"
  },
  {
    "id": "230",
    "name": "Tierra del Fuego",
    "country_id": "10"
  },
  {
    "id": "231",
    "name": "Tucuman",
    "country_id": "10"
  },
  {
    "id": "232",
    "name": "Aragatsotn",
    "country_id": "11"
  },
  {
    "id": "233",
    "name": "Ararat",
    "country_id": "11"
  },
  {
    "id": "234",
    "name": "Armavir",
    "country_id": "11"
  },
  {
    "id": "235",
    "name": "Gegharkunik",
    "country_id": "11"
  },
  {
    "id": "236",
    "name": "Kotaik",
    "country_id": "11"
  },
  {
    "id": "237",
    "name": "Lori",
    "country_id": "11"
  },
  {
    "id": "238",
    "name": "Shirak",
    "country_id": "11"
  },
  {
    "id": "239",
    "name": "Stepanakert",
    "country_id": "11"
  },
  {
    "id": "240",
    "name": "Syunik",
    "country_id": "11"
  },
  {
    "id": "241",
    "name": "Tavush",
    "country_id": "11"
  },
  {
    "id": "242",
    "name": "Vayots Dzor",
    "country_id": "11"
  },
  {
    "id": "243",
    "name": "Yerevan",
    "country_id": "11"
  },
  {
    "id": "244",
    "name": "Aruba",
    "country_id": "12"
  },
  {
    "id": "245",
    "name": "Auckland",
    "country_id": "13"
  },
  {
    "id": "246",
    "name": "Australian Capital Territory",
    "country_id": "13"
  },
  {
    "id": "247",
    "name": "Balgowlah",
    "country_id": "13"
  },
  {
    "id": "248",
    "name": "Balmain",
    "country_id": "13"
  },
  {
    "id": "249",
    "name": "Bankstown",
    "country_id": "13"
  },
  {
    "id": "250",
    "name": "Baulkham Hills",
    "country_id": "13"
  },
  {
    "id": "251",
    "name": "Bonnet Bay",
    "country_id": "13"
  },
  {
    "id": "252",
    "name": "Camberwell",
    "country_id": "13"
  },
  {
    "id": "253",
    "name": "Carole Park",
    "country_id": "13"
  },
  {
    "id": "254",
    "name": "Castle Hill",
    "country_id": "13"
  },
  {
    "id": "255",
    "name": "Caulfield",
    "country_id": "13"
  },
  {
    "id": "256",
    "name": "Chatswood",
    "country_id": "13"
  },
  {
    "id": "257",
    "name": "Cheltenham",
    "country_id": "13"
  },
  {
    "id": "258",
    "name": "Cherrybrook",
    "country_id": "13"
  },
  {
    "id": "259",
    "name": "Clayton",
    "country_id": "13"
  },
  {
    "id": "260",
    "name": "Collingwood",
    "country_id": "13"
  },
  {
    "id": "261",
    "name": "Frenchs Forest",
    "country_id": "13"
  },
  {
    "id": "262",
    "name": "Hawthorn",
    "country_id": "13"
  },
  {
    "id": "263",
    "name": "Jannnali",
    "country_id": "13"
  },
  {
    "id": "264",
    "name": "Knoxfield",
    "country_id": "13"
  },
  {
    "id": "265",
    "name": "Melbourne",
    "country_id": "13"
  },
  {
    "id": "266",
    "name": "New South Wales",
    "country_id": "13"
  },
  {
    "id": "267",
    "name": "Northern Territory",
    "country_id": "13"
  },
  {
    "id": "268",
    "name": "Perth",
    "country_id": "13"
  },
  {
    "id": "269",
    "name": "Queensland",
    "country_id": "13"
  },
  {
    "id": "270",
    "name": "South Australia",
    "country_id": "13"
  },
  {
    "id": "271",
    "name": "Tasmania",
    "country_id": "13"
  },
  {
    "id": "272",
    "name": "Templestowe",
    "country_id": "13"
  },
  {
    "id": "273",
    "name": "Victoria",
    "country_id": "13"
  },
  {
    "id": "274",
    "name": "Werribee south",
    "country_id": "13"
  },
  {
    "id": "275",
    "name": "Western Australia",
    "country_id": "13"
  },
  {
    "id": "276",
    "name": "Wheeler",
    "country_id": "13"
  },
  {
    "id": "277",
    "name": "Bundesland Salzburg",
    "country_id": "14"
  },
  {
    "id": "278",
    "name": "Bundesland Steiermark",
    "country_id": "14"
  },
  {
    "id": "279",
    "name": "Bundesland Tirol",
    "country_id": "14"
  },
  {
    "id": "280",
    "name": "Burgenland",
    "country_id": "14"
  },
  {
    "id": "281",
    "name": "Carinthia",
    "country_id": "14"
  },
  {
    "id": "282",
    "name": "Karnten",
    "country_id": "14"
  },
  {
    "id": "283",
    "name": "Liezen",
    "country_id": "14"
  },
  {
    "id": "284",
    "name": "Lower Austria",
    "country_id": "14"
  },
  {
    "id": "285",
    "name": "Niederosterreich",
    "country_id": "14"
  },
  {
    "id": "286",
    "name": "Oberosterreich",
    "country_id": "14"
  },
  {
    "id": "287",
    "name": "Salzburg",
    "country_id": "14"
  },
  {
    "id": "288",
    "name": "Schleswig-Holstein",
    "country_id": "14"
  },
  {
    "id": "289",
    "name": "Steiermark",
    "country_id": "14"
  },
  {
    "id": "290",
    "name": "Styria",
    "country_id": "14"
  },
  {
    "id": "291",
    "name": "Tirol",
    "country_id": "14"
  },
  {
    "id": "292",
    "name": "Upper Austria",
    "country_id": "14"
  },
  {
    "id": "293",
    "name": "Vorarlberg",
    "country_id": "14"
  },
  {
    "id": "294",
    "name": "Wien",
    "country_id": "14"
  },
  {
    "id": "295",
    "name": "Abseron",
    "country_id": "15"
  },
  {
    "id": "296",
    "name": "Baki Sahari",
    "country_id": "15"
  },
  {
    "id": "297",
    "name": "Ganca",
    "country_id": "15"
  },
  {
    "id": "298",
    "name": "Ganja",
    "country_id": "15"
  },
  {
    "id": "299",
    "name": "Kalbacar",
    "country_id": "15"
  },
  {
    "id": "300",
    "name": "Lankaran",
    "country_id": "15"
  },
  {
    "id": "301",
    "name": "Mil-Qarabax",
    "country_id": "15"
  },
  {
    "id": "302",
    "name": "Mugan-Salyan",
    "country_id": "15"
  },
  {
    "id": "303",
    "name": "Nagorni-Qarabax",
    "country_id": "15"
  },
  {
    "id": "304",
    "name": "Naxcivan",
    "country_id": "15"
  },
  {
    "id": "305",
    "name": "Priaraks",
    "country_id": "15"
  },
  {
    "id": "306",
    "name": "Qazax",
    "country_id": "15"
  },
  {
    "id": "307",
    "name": "Saki",
    "country_id": "15"
  },
  {
    "id": "308",
    "name": "Sirvan",
    "country_id": "15"
  },
  {
    "id": "309",
    "name": "Xacmaz",
    "country_id": "15"
  },
  {
    "id": "310",
    "name": "Abaco",
    "country_id": "16"
  },
  {
    "id": "311",
    "name": "Acklins Island",
    "country_id": "16"
  },
  {
    "id": "312",
    "name": "Andros",
    "country_id": "16"
  },
  {
    "id": "313",
    "name": "Berry Islands",
    "country_id": "16"
  },
  {
    "id": "314",
    "name": "Biminis",
    "country_id": "16"
  },
  {
    "id": "315",
    "name": "Cat Island",
    "country_id": "16"
  },
  {
    "id": "316",
    "name": "Crooked Island",
    "country_id": "16"
  },
  {
    "id": "317",
    "name": "Eleuthera",
    "country_id": "16"
  },
  {
    "id": "318",
    "name": "Exuma and Cays",
    "country_id": "16"
  },
  {
    "id": "319",
    "name": "Grand Bahama",
    "country_id": "16"
  },
  {
    "id": "320",
    "name": "Inagua Islands",
    "country_id": "16"
  },
  {
    "id": "321",
    "name": "Long Island",
    "country_id": "16"
  },
  {
    "id": "322",
    "name": "Mayaguana",
    "country_id": "16"
  },
  {
    "id": "323",
    "name": "New Providence",
    "country_id": "16"
  },
  {
    "id": "324",
    "name": "Ragged Island",
    "country_id": "16"
  },
  {
    "id": "325",
    "name": "Rum Cay",
    "country_id": "16"
  },
  {
    "id": "326",
    "name": "San Salvador",
    "country_id": "16"
  },
  {
    "id": "327",
    "name": "Isa",
    "country_id": "17"
  },
  {
    "id": "328",
    "name": "Badiyah",
    "country_id": "17"
  },
  {
    "id": "329",
    "name": "Hidd",
    "country_id": "17"
  },
  {
    "id": "330",
    "name": "Jidd Hafs",
    "country_id": "17"
  },
  {
    "id": "331",
    "name": "Mahama",
    "country_id": "17"
  },
  {
    "id": "332",
    "name": "Manama",
    "country_id": "17"
  },
  {
    "id": "333",
    "name": "Sitrah",
    "country_id": "17"
  },
  {
    "id": "334",
    "name": "al-Manamah",
    "country_id": "17"
  },
  {
    "id": "335",
    "name": "al-Muharraq",
    "country_id": "17"
  },
  {
    "id": "336",
    "name": "ar-Rifa\'\'a",
    "country_id": "17"
  },
  {
    "id": "337",
    "name": "Bagar Hat",
    "country_id": "18"
  },
  {
    "id": "338",
    "name": "Bandarban",
    "country_id": "18"
  },
  {
    "id": "339",
    "name": "Barguna",
    "country_id": "18"
  },
  {
    "id": "340",
    "name": "Barisal",
    "country_id": "18"
  },
  {
    "id": "341",
    "name": "Bhola",
    "country_id": "18"
  },
  {
    "id": "342",
    "name": "Bogora",
    "country_id": "18"
  },
  {
    "id": "343",
    "name": "Brahman Bariya",
    "country_id": "18"
  },
  {
    "id": "344",
    "name": "Chandpur",
    "country_id": "18"
  },
  {
    "id": "345",
    "name": "Chattagam",
    "country_id": "18"
  },
  {
    "id": "346",
    "name": "Chittagong Division",
    "country_id": "18"
  },
  {
    "id": "347",
    "name": "Chuadanga",
    "country_id": "18"
  },
  {
    "id": "348",
    "name": "Dhaka",
    "country_id": "18"
  },
  {
    "id": "349",
    "name": "Dinajpur",
    "country_id": "18"
  },
  {
    "id": "350",
    "name": "Faridpur",
    "country_id": "18"
  },
  {
    "id": "351",
    "name": "Feni",
    "country_id": "18"
  },
  {
    "id": "352",
    "name": "Gaybanda",
    "country_id": "18"
  },
  {
    "id": "353",
    "name": "Gazipur",
    "country_id": "18"
  },
  {
    "id": "354",
    "name": "Gopalganj",
    "country_id": "18"
  },
  {
    "id": "355",
    "name": "Habiganj",
    "country_id": "18"
  },
  {
    "id": "356",
    "name": "Jaipur Hat",
    "country_id": "18"
  },
  {
    "id": "357",
    "name": "Jamalpur",
    "country_id": "18"
  },
  {
    "id": "358",
    "name": "Jessor",
    "country_id": "18"
  },
  {
    "id": "359",
    "name": "Jhalakati",
    "country_id": "18"
  },
  {
    "id": "360",
    "name": "Jhanaydah",
    "country_id": "18"
  },
  {
    "id": "361",
    "name": "Khagrachhari",
    "country_id": "18"
  },
  {
    "id": "362",
    "name": "Khulna",
    "country_id": "18"
  },
  {
    "id": "363",
    "name": "Kishorganj",
    "country_id": "18"
  },
  {
    "id": "364",
    "name": "Koks Bazar",
    "country_id": "18"
  },
  {
    "id": "365",
    "name": "Komilla",
    "country_id": "18"
  },
  {
    "id": "366",
    "name": "Kurigram",
    "country_id": "18"
  },
  {
    "id": "367",
    "name": "Kushtiya",
    "country_id": "18"
  },
  {
    "id": "368",
    "name": "Lakshmipur",
    "country_id": "18"
  },
  {
    "id": "369",
    "name": "Lalmanir Hat",
    "country_id": "18"
  },
  {
    "id": "370",
    "name": "Madaripur",
    "country_id": "18"
  },
  {
    "id": "371",
    "name": "Magura",
    "country_id": "18"
  },
  {
    "id": "372",
    "name": "Maimansingh",
    "country_id": "18"
  },
  {
    "id": "373",
    "name": "Manikganj",
    "country_id": "18"
  },
  {
    "id": "374",
    "name": "Maulvi Bazar",
    "country_id": "18"
  },
  {
    "id": "375",
    "name": "Meherpur",
    "country_id": "18"
  },
  {
    "id": "376",
    "name": "Munshiganj",
    "country_id": "18"
  },
  {
    "id": "377",
    "name": "Naral",
    "country_id": "18"
  },
  {
    "id": "378",
    "name": "Narayanganj",
    "country_id": "18"
  },
  {
    "id": "379",
    "name": "Narsingdi",
    "country_id": "18"
  },
  {
    "id": "380",
    "name": "Nator",
    "country_id": "18"
  },
  {
    "id": "381",
    "name": "Naugaon",
    "country_id": "18"
  },
  {
    "id": "382",
    "name": "Nawabganj",
    "country_id": "18"
  },
  {
    "id": "383",
    "name": "Netrakona",
    "country_id": "18"
  },
  {
    "id": "384",
    "name": "Nilphamari",
    "country_id": "18"
  },
  {
    "id": "385",
    "name": "Noakhali",
    "country_id": "18"
  },
  {
    "id": "386",
    "name": "Pabna",
    "country_id": "18"
  },
  {
    "id": "387",
    "name": "Panchagarh",
    "country_id": "18"
  },
  {
    "id": "388",
    "name": "Patuakhali",
    "country_id": "18"
  },
  {
    "id": "389",
    "name": "Pirojpur",
    "country_id": "18"
  },
  {
    "id": "390",
    "name": "Rajbari",
    "country_id": "18"
  },
  {
    "id": "391",
    "name": "Rajshahi",
    "country_id": "18"
  },
  {
    "id": "392",
    "name": "Rangamati",
    "country_id": "18"
  },
  {
    "id": "393",
    "name": "Rangpur",
    "country_id": "18"
  },
  {
    "id": "394",
    "name": "Satkhira",
    "country_id": "18"
  },
  {
    "id": "395",
    "name": "Shariatpur",
    "country_id": "18"
  },
  {
    "id": "396",
    "name": "Sherpur",
    "country_id": "18"
  },
  {
    "id": "397",
    "name": "Silhat",
    "country_id": "18"
  },
  {
    "id": "398",
    "name": "Sirajganj",
    "country_id": "18"
  },
  {
    "id": "399",
    "name": "Sunamganj",
    "country_id": "18"
  },
  {
    "id": "400",
    "name": "Tangayal",
    "country_id": "18"
  },
  {
    "id": "401",
    "name": "Thakurgaon",
    "country_id": "18"
  },
  {
    "id": "402",
    "name": "Christ Church",
    "country_id": "19"
  },
  {
    "id": "403",
    "name": "Saint Andrew",
    "country_id": "19"
  },
  {
    "id": "404",
    "name": "Saint George",
    "country_id": "19"
  },
  {
    "id": "405",
    "name": "Saint James",
    "country_id": "19"
  },
  {
    "id": "406",
    "name": "Saint John",
    "country_id": "19"
  },
  {
    "id": "407",
    "name": "Saint Joseph",
    "country_id": "19"
  },
  {
    "id": "408",
    "name": "Saint Lucy",
    "country_id": "19"
  },
  {
    "id": "409",
    "name": "Saint Michael",
    "country_id": "19"
  },
  {
    "id": "410",
    "name": "Saint Peter",
    "country_id": "19"
  },
  {
    "id": "411",
    "name": "Saint Philip",
    "country_id": "19"
  },
  {
    "id": "412",
    "name": "Saint Thomas",
    "country_id": "19"
  },
  {
    "id": "413",
    "name": "Brest",
    "country_id": "20"
  },
  {
    "id": "414",
    "name": "Homjel",
    "country_id": "20"
  },
  {
    "id": "415",
    "name": "Hrodna",
    "country_id": "20"
  },
  {
    "id": "416",
    "name": "Mahiljow",
    "country_id": "20"
  },
  {
    "id": "417",
    "name": "Mahilyowskaya Voblasts",
    "country_id": "20"
  },
  {
    "id": "418",
    "name": "Minsk",
    "country_id": "20"
  },
  {
    "id": "419",
    "name": "Minskaja Voblasts",
    "country_id": "20"
  },
  {
    "id": "420",
    "name": "Petrik",
    "country_id": "20"
  },
  {
    "id": "421",
    "name": "Vicebsk",
    "country_id": "20"
  },
  {
    "id": "422",
    "name": "Antwerpen",
    "country_id": "21"
  },
  {
    "id": "423",
    "name": "Berchem",
    "country_id": "21"
  },
  {
    "id": "424",
    "name": "Brabant",
    "country_id": "21"
  },
  {
    "id": "425",
    "name": "Brabant Wallon",
    "country_id": "21"
  },
  {
    "id": "426",
    "name": "Brussel",
    "country_id": "21"
  },
  {
    "id": "427",
    "name": "East Flanders",
    "country_id": "21"
  },
  {
    "id": "428",
    "name": "Hainaut",
    "country_id": "21"
  },
  {
    "id": "429",
    "name": "Liege",
    "country_id": "21"
  },
  {
    "id": "430",
    "name": "Limburg",
    "country_id": "21"
  },
  {
    "id": "431",
    "name": "Luxembourg",
    "country_id": "21"
  },
  {
    "id": "432",
    "name": "Namur",
    "country_id": "21"
  },
  {
    "id": "433",
    "name": "Ontario",
    "country_id": "21"
  },
  {
    "id": "434",
    "name": "Oost-Vlaanderen",
    "country_id": "21"
  },
  {
    "id": "435",
    "name": "Provincie Brabant",
    "country_id": "21"
  },
  {
    "id": "436",
    "name": "Vlaams-Brabant",
    "country_id": "21"
  },
  {
    "id": "437",
    "name": "Wallonne",
    "country_id": "21"
  },
  {
    "id": "438",
    "name": "West-Vlaanderen",
    "country_id": "21"
  },
  {
    "id": "439",
    "name": "Belize",
    "country_id": "22"
  },
  {
    "id": "440",
    "name": "Cayo",
    "country_id": "22"
  },
  {
    "id": "441",
    "name": "Corozal",
    "country_id": "22"
  },
  {
    "id": "442",
    "name": "Orange Walk",
    "country_id": "22"
  },
  {
    "id": "443",
    "name": "Stann Creek",
    "country_id": "22"
  },
  {
    "id": "444",
    "name": "Toledo",
    "country_id": "22"
  },
  {
    "id": "445",
    "name": "Alibori",
    "country_id": "23"
  },
  {
    "id": "446",
    "name": "Atacora",
    "country_id": "23"
  },
  {
    "id": "447",
    "name": "Atlantique",
    "country_id": "23"
  },
  {
    "id": "448",
    "name": "Borgou",
    "country_id": "23"
  },
  {
    "id": "449",
    "name": "Collines",
    "country_id": "23"
  },
  {
    "id": "450",
    "name": "Couffo",
    "country_id": "23"
  },
  {
    "id": "451",
    "name": "Donga",
    "country_id": "23"
  },
  {
    "id": "452",
    "name": "Littoral",
    "country_id": "23"
  },
  {
    "id": "453",
    "name": "Mono",
    "country_id": "23"
  },
  {
    "id": "454",
    "name": "Oueme",
    "country_id": "23"
  },
  {
    "id": "455",
    "name": "Plateau",
    "country_id": "23"
  },
  {
    "id": "456",
    "name": "Zou",
    "country_id": "23"
  },
  {
    "id": "457",
    "name": "Hamilton",
    "country_id": "24"
  },
  {
    "id": "458",
    "name": "Saint George",
    "country_id": "24"
  },
  {
    "id": "459",
    "name": "Bumthang",
    "country_id": "25"
  },
  {
    "id": "460",
    "name": "Chhukha",
    "country_id": "25"
  },
  {
    "id": "461",
    "name": "Chirang",
    "country_id": "25"
  },
  {
    "id": "462",
    "name": "Daga",
    "country_id": "25"
  },
  {
    "id": "463",
    "name": "Geylegphug",
    "country_id": "25"
  },
  {
    "id": "464",
    "name": "Ha",
    "country_id": "25"
  },
  {
    "id": "465",
    "name": "Lhuntshi",
    "country_id": "25"
  },
  {
    "id": "466",
    "name": "Mongar",
    "country_id": "25"
  },
  {
    "id": "467",
    "name": "Pemagatsel",
    "country_id": "25"
  },
  {
    "id": "468",
    "name": "Punakha",
    "country_id": "25"
  },
  {
    "id": "469",
    "name": "Rinpung",
    "country_id": "25"
  },
  {
    "id": "470",
    "name": "Samchi",
    "country_id": "25"
  },
  {
    "id": "471",
    "name": "Samdrup Jongkhar",
    "country_id": "25"
  },
  {
    "id": "472",
    "name": "Shemgang",
    "country_id": "25"
  },
  {
    "id": "473",
    "name": "Tashigang",
    "country_id": "25"
  },
  {
    "id": "474",
    "name": "Timphu",
    "country_id": "25"
  },
  {
    "id": "475",
    "name": "Tongsa",
    "country_id": "25"
  },
  {
    "id": "476",
    "name": "Wangdiphodrang",
    "country_id": "25"
  },
  {
    "id": "477",
    "name": "Beni",
    "country_id": "26"
  },
  {
    "id": "478",
    "name": "Chuquisaca",
    "country_id": "26"
  },
  {
    "id": "479",
    "name": "Cochabamba",
    "country_id": "26"
  },
  {
    "id": "480",
    "name": "La Paz",
    "country_id": "26"
  },
  {
    "id": "481",
    "name": "Oruro",
    "country_id": "26"
  },
  {
    "id": "482",
    "name": "Pando",
    "country_id": "26"
  },
  {
    "id": "483",
    "name": "Potosi",
    "country_id": "26"
  },
  {
    "id": "484",
    "name": "Santa Cruz",
    "country_id": "26"
  },
  {
    "id": "485",
    "name": "Tarija",
    "country_id": "26"
  },
  {
    "id": "486",
    "name": "Federacija Bosna i Hercegovina",
    "country_id": "27"
  },
  {
    "id": "487",
    "name": "Republika Srpska",
    "country_id": "27"
  },
  {
    "id": "488",
    "name": "Central Bobonong",
    "country_id": "28"
  },
  {
    "id": "489",
    "name": "Central Boteti",
    "country_id": "28"
  },
  {
    "id": "490",
    "name": "Central Mahalapye",
    "country_id": "28"
  },
  {
    "id": "491",
    "name": "Central Serowe-Palapye",
    "country_id": "28"
  },
  {
    "id": "492",
    "name": "Central Tutume",
    "country_id": "28"
  },
  {
    "id": "493",
    "name": "Chobe",
    "country_id": "28"
  },
  {
    "id": "494",
    "name": "Francistown",
    "country_id": "28"
  },
  {
    "id": "495",
    "name": "Gaborone",
    "country_id": "28"
  },
  {
    "id": "496",
    "name": "Ghanzi",
    "country_id": "28"
  },
  {
    "id": "497",
    "name": "Jwaneng",
    "country_id": "28"
  },
  {
    "id": "498",
    "name": "Kgalagadi North",
    "country_id": "28"
  },
  {
    "id": "499",
    "name": "Kgalagadi South",
    "country_id": "28"
  },
  {
    "id": "500",
    "name": "Kgatleng",
    "country_id": "28"
  },
  {
    "id": "501",
    "name": "Kweneng",
    "country_id": "28"
  },
  {
    "id": "502",
    "name": "Lobatse",
    "country_id": "28"
  },
  {
    "id": "503",
    "name": "Ngamiland",
    "country_id": "28"
  },
  {
    "id": "504",
    "name": "Ngwaketse",
    "country_id": "28"
  },
  {
    "id": "505",
    "name": "North East",
    "country_id": "28"
  },
  {
    "id": "506",
    "name": "Okavango",
    "country_id": "28"
  },
  {
    "id": "507",
    "name": "Orapa",
    "country_id": "28"
  },
  {
    "id": "508",
    "name": "Selibe Phikwe",
    "country_id": "28"
  },
  {
    "id": "509",
    "name": "South East",
    "country_id": "28"
  },
  {
    "id": "510",
    "name": "Sowa",
    "country_id": "28"
  },
  {
    "id": "511",
    "name": "Bouvet Island",
    "country_id": "29"
  },
  {
    "id": "512",
    "name": "Acre",
    "country_id": "30"
  },
  {
    "id": "513",
    "name": "Alagoas",
    "country_id": "30"
  },
  {
    "id": "514",
    "name": "Amapa",
    "country_id": "30"
  },
  {
    "id": "515",
    "name": "Amazonas",
    "country_id": "30"
  },
  {
    "id": "516",
    "name": "Bahia",
    "country_id": "30"
  },
  {
    "id": "517",
    "name": "Ceara",
    "country_id": "30"
  },
  {
    "id": "518",
    "name": "Distrito Federal",
    "country_id": "30"
  },
  {
    "id": "519",
    "name": "Espirito Santo",
    "country_id": "30"
  },
  {
    "id": "520",
    "name": "Estado de Sao Paulo",
    "country_id": "30"
  },
  {
    "id": "521",
    "name": "Goias",
    "country_id": "30"
  },
  {
    "id": "522",
    "name": "Maranhao",
    "country_id": "30"
  },
  {
    "id": "523",
    "name": "Mato Grosso",
    "country_id": "30"
  },
  {
    "id": "524",
    "name": "Mato Grosso do Sul",
    "country_id": "30"
  },
  {
    "id": "525",
    "name": "Minas Gerais",
    "country_id": "30"
  },
  {
    "id": "526",
    "name": "Para",
    "country_id": "30"
  },
  {
    "id": "527",
    "name": "Paraiba",
    "country_id": "30"
  },
  {
    "id": "528",
    "name": "Parana",
    "country_id": "30"
  },
  {
    "id": "529",
    "name": "Pernambuco",
    "country_id": "30"
  },
  {
    "id": "530",
    "name": "Piaui",
    "country_id": "30"
  },
  {
    "id": "531",
    "name": "Rio Grande do Norte",
    "country_id": "30"
  },
  {
    "id": "532",
    "name": "Rio Grande do Sul",
    "country_id": "30"
  },
  {
    "id": "533",
    "name": "Rio de Janeiro",
    "country_id": "30"
  },
  {
    "id": "534",
    "name": "Rondonia",
    "country_id": "30"
  },
  {
    "id": "535",
    "name": "Roraima",
    "country_id": "30"
  },
  {
    "id": "536",
    "name": "Santa Catarina",
    "country_id": "30"
  },
  {
    "id": "537",
    "name": "Sao Paulo",
    "country_id": "30"
  },
  {
    "id": "538",
    "name": "Sergipe",
    "country_id": "30"
  },
  {
    "id": "539",
    "name": "Tocantins",
    "country_id": "30"
  },
  {
    "id": "540",
    "name": "British Indian Ocean Territory",
    "country_id": "31"
  },
  {
    "id": "541",
    "name": "Belait",
    "country_id": "32"
  },
  {
    "id": "542",
    "name": "Brunei-Muara",
    "country_id": "32"
  },
  {
    "id": "543",
    "name": "Temburong",
    "country_id": "32"
  },
  {
    "id": "544",
    "name": "Tutong",
    "country_id": "32"
  },
  {
    "id": "545",
    "name": "Blagoevgrad",
    "country_id": "33"
  },
  {
    "id": "546",
    "name": "Burgas",
    "country_id": "33"
  },
  {
    "id": "547",
    "name": "Dobrich",
    "country_id": "33"
  },
  {
    "id": "548",
    "name": "Gabrovo",
    "country_id": "33"
  },
  {
    "id": "549",
    "name": "Haskovo",
    "country_id": "33"
  },
  {
    "id": "550",
    "name": "Jambol",
    "country_id": "33"
  },
  {
    "id": "551",
    "name": "Kardzhali",
    "country_id": "33"
  },
  {
    "id": "552",
    "name": "Kjustendil",
    "country_id": "33"
  },
  {
    "id": "553",
    "name": "Lovech",
    "country_id": "33"
  },
  {
    "id": "554",
    "name": "Montana",
    "country_id": "33"
  },
  {
    "id": "555",
    "name": "Oblast Sofiya-Grad",
    "country_id": "33"
  },
  {
    "id": "556",
    "name": "Pazardzhik",
    "country_id": "33"
  },
  {
    "id": "557",
    "name": "Pernik",
    "country_id": "33"
  },
  {
    "id": "558",
    "name": "Pleven",
    "country_id": "33"
  },
  {
    "id": "559",
    "name": "Plovdiv",
    "country_id": "33"
  },
  {
    "id": "560",
    "name": "Razgrad",
    "country_id": "33"
  },
  {
    "id": "561",
    "name": "Ruse",
    "country_id": "33"
  },
  {
    "id": "562",
    "name": "Shumen",
    "country_id": "33"
  },
  {
    "id": "563",
    "name": "Silistra",
    "country_id": "33"
  },
  {
    "id": "564",
    "name": "Sliven",
    "country_id": "33"
  },
  {
    "id": "565",
    "name": "Smoljan",
    "country_id": "33"
  },
  {
    "id": "566",
    "name": "Sofija grad",
    "country_id": "33"
  },
  {
    "id": "567",
    "name": "Sofijska oblast",
    "country_id": "33"
  },
  {
    "id": "568",
    "name": "Stara Zagora",
    "country_id": "33"
  },
  {
    "id": "569",
    "name": "Targovishte",
    "country_id": "33"
  },
  {
    "id": "570",
    "name": "Varna",
    "country_id": "33"
  },
  {
    "id": "571",
    "name": "Veliko Tarnovo",
    "country_id": "33"
  },
  {
    "id": "572",
    "name": "Vidin",
    "country_id": "33"
  },
  {
    "id": "573",
    "name": "Vraca",
    "country_id": "33"
  },
  {
    "id": "574",
    "name": "Yablaniza",
    "country_id": "33"
  },
  {
    "id": "575",
    "name": "Bale",
    "country_id": "34"
  },
  {
    "id": "576",
    "name": "Bam",
    "country_id": "34"
  },
  {
    "id": "577",
    "name": "Bazega",
    "country_id": "34"
  },
  {
    "id": "578",
    "name": "Bougouriba",
    "country_id": "34"
  },
  {
    "id": "579",
    "name": "Boulgou",
    "country_id": "34"
  },
  {
    "id": "580",
    "name": "Boulkiemde",
    "country_id": "34"
  },
  {
    "id": "581",
    "name": "Comoe",
    "country_id": "34"
  },
  {
    "id": "582",
    "name": "Ganzourgou",
    "country_id": "34"
  },
  {
    "id": "583",
    "name": "Gnagna",
    "country_id": "34"
  },
  {
    "id": "584",
    "name": "Gourma",
    "country_id": "34"
  },
  {
    "id": "585",
    "name": "Houet",
    "country_id": "34"
  },
  {
    "id": "586",
    "name": "Ioba",
    "country_id": "34"
  },
  {
    "id": "587",
    "name": "Kadiogo",
    "country_id": "34"
  },
  {
    "id": "588",
    "name": "Kenedougou",
    "country_id": "34"
  },
  {
    "id": "589",
    "name": "Komandjari",
    "country_id": "34"
  },
  {
    "id": "590",
    "name": "Kompienga",
    "country_id": "34"
  },
  {
    "id": "591",
    "name": "Kossi",
    "country_id": "34"
  },
  {
    "id": "592",
    "name": "Kouritenga",
    "country_id": "34"
  },
  {
    "id": "593",
    "name": "Kourweogo",
    "country_id": "34"
  },
  {
    "id": "594",
    "name": "Leraba",
    "country_id": "34"
  },
  {
    "id": "595",
    "name": "Mouhoun",
    "country_id": "34"
  },
  {
    "id": "596",
    "name": "Nahouri",
    "country_id": "34"
  },
  {
    "id": "597",
    "name": "Namentenga",
    "country_id": "34"
  },
  {
    "id": "598",
    "name": "Noumbiel",
    "country_id": "34"
  },
  {
    "id": "599",
    "name": "Oubritenga",
    "country_id": "34"
  },
  {
    "id": "600",
    "name": "Oudalan",
    "country_id": "34"
  },
  {
    "id": "601",
    "name": "Passore",
    "country_id": "34"
  },
  {
    "id": "602",
    "name": "Poni",
    "country_id": "34"
  },
  {
    "id": "603",
    "name": "Sanguie",
    "country_id": "34"
  },
  {
    "id": "604",
    "name": "Sanmatenga",
    "country_id": "34"
  },
  {
    "id": "605",
    "name": "Seno",
    "country_id": "34"
  },
  {
    "id": "606",
    "name": "Sissili",
    "country_id": "34"
  },
  {
    "id": "607",
    "name": "Soum",
    "country_id": "34"
  },
  {
    "id": "608",
    "name": "Sourou",
    "country_id": "34"
  },
  {
    "id": "609",
    "name": "Tapoa",
    "country_id": "34"
  },
  {
    "id": "610",
    "name": "Tuy",
    "country_id": "34"
  },
  {
    "id": "611",
    "name": "Yatenga",
    "country_id": "34"
  },
  {
    "id": "612",
    "name": "Zondoma",
    "country_id": "34"
  },
  {
    "id": "613",
    "name": "Zoundweogo",
    "country_id": "34"
  },
  {
    "id": "614",
    "name": "Bubanza",
    "country_id": "35"
  },
  {
    "id": "615",
    "name": "Bujumbura",
    "country_id": "35"
  },
  {
    "id": "616",
    "name": "Bururi",
    "country_id": "35"
  },
  {
    "id": "617",
    "name": "Cankuzo",
    "country_id": "35"
  },
  {
    "id": "618",
    "name": "Cibitoke",
    "country_id": "35"
  },
  {
    "id": "619",
    "name": "Gitega",
    "country_id": "35"
  },
  {
    "id": "620",
    "name": "Karuzi",
    "country_id": "35"
  },
  {
    "id": "621",
    "name": "Kayanza",
    "country_id": "35"
  },
  {
    "id": "622",
    "name": "Kirundo",
    "country_id": "35"
  },
  {
    "id": "623",
    "name": "Makamba",
    "country_id": "35"
  },
  {
    "id": "624",
    "name": "Muramvya",
    "country_id": "35"
  },
  {
    "id": "625",
    "name": "Muyinga",
    "country_id": "35"
  },
  {
    "id": "626",
    "name": "Ngozi",
    "country_id": "35"
  },
  {
    "id": "627",
    "name": "Rutana",
    "country_id": "35"
  },
  {
    "id": "628",
    "name": "Ruyigi",
    "country_id": "35"
  },
  {
    "id": "629",
    "name": "Banteay Mean Chey",
    "country_id": "36"
  },
  {
    "id": "630",
    "name": "Bat Dambang",
    "country_id": "36"
  },
  {
    "id": "631",
    "name": "Kampong Cham",
    "country_id": "36"
  },
  {
    "id": "632",
    "name": "Kampong Chhnang",
    "country_id": "36"
  },
  {
    "id": "633",
    "name": "Kampong Spoeu",
    "country_id": "36"
  },
  {
    "id": "634",
    "name": "Kampong Thum",
    "country_id": "36"
  },
  {
    "id": "635",
    "name": "Kampot",
    "country_id": "36"
  },
  {
    "id": "636",
    "name": "Kandal",
    "country_id": "36"
  },
  {
    "id": "637",
    "name": "Kaoh Kong",
    "country_id": "36"
  },
  {
    "id": "638",
    "name": "Kracheh",
    "country_id": "36"
  },
  {
    "id": "639",
    "name": "Krong Kaeb",
    "country_id": "36"
  },
  {
    "id": "640",
    "name": "Krong Pailin",
    "country_id": "36"
  },
  {
    "id": "641",
    "name": "Krong Preah Sihanouk",
    "country_id": "36"
  },
  {
    "id": "642",
    "name": "Mondol Kiri",
    "country_id": "36"
  },
  {
    "id": "643",
    "name": "Otdar Mean Chey",
    "country_id": "36"
  },
  {
    "id": "644",
    "name": "Phnum Penh",
    "country_id": "36"
  },
  {
    "id": "645",
    "name": "Pousat",
    "country_id": "36"
  },
  {
    "id": "646",
    "name": "Preah Vihear",
    "country_id": "36"
  },
  {
    "id": "647",
    "name": "Prey Veaeng",
    "country_id": "36"
  },
  {
    "id": "648",
    "name": "Rotanak Kiri",
    "country_id": "36"
  },
  {
    "id": "649",
    "name": "Siem Reab",
    "country_id": "36"
  },
  {
    "id": "650",
    "name": "Stueng Traeng",
    "country_id": "36"
  },
  {
    "id": "651",
    "name": "Svay Rieng",
    "country_id": "36"
  },
  {
    "id": "652",
    "name": "Takaev",
    "country_id": "36"
  },
  {
    "id": "653",
    "name": "Adamaoua",
    "country_id": "37"
  },
  {
    "id": "654",
    "name": "Centre",
    "country_id": "37"
  },
  {
    "id": "655",
    "name": "Est",
    "country_id": "37"
  },
  {
    "id": "656",
    "name": "Littoral",
    "country_id": "37"
  },
  {
    "id": "657",
    "name": "Nord",
    "country_id": "37"
  },
  {
    "id": "658",
    "name": "Nord Extreme",
    "country_id": "37"
  },
  {
    "id": "659",
    "name": "Nordouest",
    "country_id": "37"
  },
  {
    "id": "660",
    "name": "Ouest",
    "country_id": "37"
  },
  {
    "id": "661",
    "name": "Sud",
    "country_id": "37"
  },
  {
    "id": "662",
    "name": "Sudouest",
    "country_id": "37"
  },
  {
    "id": "663",
    "name": "Alberta",
    "country_id": "38"
  },
  {
    "id": "664",
    "name": "British Columbia",
    "country_id": "38"
  },
  {
    "id": "665",
    "name": "Manitoba",
    "country_id": "38"
  },
  {
    "id": "666",
    "name": "New Brunswick",
    "country_id": "38"
  },
  {
    "id": "667",
    "name": "Newfoundland and Labrador",
    "country_id": "38"
  },
  {
    "id": "668",
    "name": "Northwest Territories",
    "country_id": "38"
  },
  {
    "id": "669",
    "name": "Nova Scotia",
    "country_id": "38"
  },
  {
    "id": "670",
    "name": "Nunavut",
    "country_id": "38"
  },
  {
    "id": "671",
    "name": "Ontario",
    "country_id": "38"
  },
  {
    "id": "672",
    "name": "Prince Edward Island",
    "country_id": "38"
  },
  {
    "id": "673",
    "name": "Quebec",
    "country_id": "38"
  },
  {
    "id": "674",
    "name": "Saskatchewan",
    "country_id": "38"
  },
  {
    "id": "675",
    "name": "Yukon",
    "country_id": "38"
  },
  {
    "id": "676",
    "name": "Boavista",
    "country_id": "39"
  },
  {
    "id": "677",
    "name": "Brava",
    "country_id": "39"
  },
  {
    "id": "678",
    "name": "Fogo",
    "country_id": "39"
  },
  {
    "id": "679",
    "name": "Maio",
    "country_id": "39"
  },
  {
    "id": "680",
    "name": "Sal",
    "country_id": "39"
  },
  {
    "id": "681",
    "name": "Santo Antao",
    "country_id": "39"
  },
  {
    "id": "682",
    "name": "Sao Nicolau",
    "country_id": "39"
  },
  {
    "id": "683",
    "name": "Sao Tiago",
    "country_id": "39"
  },
  {
    "id": "684",
    "name": "Sao Vicente",
    "country_id": "39"
  },
  {
    "id": "685",
    "name": "Grand Cayman",
    "country_id": "40"
  },
  {
    "id": "686",
    "name": "Bamingui-Bangoran",
    "country_id": "41"
  },
  {
    "id": "687",
    "name": "Bangui",
    "country_id": "41"
  },
  {
    "id": "688",
    "name": "Basse-Kotto",
    "country_id": "41"
  },
  {
    "id": "689",
    "name": "Haut-Mbomou",
    "country_id": "41"
  },
  {
    "id": "690",
    "name": "Haute-Kotto",
    "country_id": "41"
  },
  {
    "id": "691",
    "name": "Kemo",
    "country_id": "41"
  },
  {
    "id": "692",
    "name": "Lobaye",
    "country_id": "41"
  },
  {
    "id": "693",
    "name": "Mambere-Kadei",
    "country_id": "41"
  },
  {
    "id": "694",
    "name": "Mbomou",
    "country_id": "41"
  },
  {
    "id": "695",
    "name": "Nana-Gribizi",
    "country_id": "41"
  },
  {
    "id": "696",
    "name": "Nana-Mambere",
    "country_id": "41"
  },
  {
    "id": "697",
    "name": "Ombella Mpoko",
    "country_id": "41"
  },
  {
    "id": "698",
    "name": "Ouaka",
    "country_id": "41"
  },
  {
    "id": "699",
    "name": "Ouham",
    "country_id": "41"
  },
  {
    "id": "700",
    "name": "Ouham-Pende",
    "country_id": "41"
  },
  {
    "id": "701",
    "name": "Sangha-Mbaere",
    "country_id": "41"
  },
  {
    "id": "702",
    "name": "Vakaga",
    "country_id": "41"
  },
  {
    "id": "703",
    "name": "Batha",
    "country_id": "42"
  },
  {
    "id": "704",
    "name": "Biltine",
    "country_id": "42"
  },
  {
    "id": "705",
    "name": "Bourkou-Ennedi-Tibesti",
    "country_id": "42"
  },
  {
    "id": "706",
    "name": "Chari-Baguirmi",
    "country_id": "42"
  },
  {
    "id": "707",
    "name": "Guera",
    "country_id": "42"
  },
  {
    "id": "708",
    "name": "Kanem",
    "country_id": "42"
  },
  {
    "id": "709",
    "name": "Lac",
    "country_id": "42"
  },
  {
    "id": "710",
    "name": "Logone Occidental",
    "country_id": "42"
  },
  {
    "id": "711",
    "name": "Logone Oriental",
    "country_id": "42"
  },
  {
    "id": "712",
    "name": "Mayo-Kebbi",
    "country_id": "42"
  },
  {
    "id": "713",
    "name": "Moyen-Chari",
    "country_id": "42"
  },
  {
    "id": "714",
    "name": "Ouaddai",
    "country_id": "42"
  },
  {
    "id": "715",
    "name": "Salamat",
    "country_id": "42"
  },
  {
    "id": "716",
    "name": "Tandjile",
    "country_id": "42"
  },
  {
    "id": "717",
    "name": "Aisen",
    "country_id": "43"
  },
  {
    "id": "718",
    "name": "Antofagasta",
    "country_id": "43"
  },
  {
    "id": "719",
    "name": "Araucania",
    "country_id": "43"
  },
  {
    "id": "720",
    "name": "Atacama",
    "country_id": "43"
  },
  {
    "id": "721",
    "name": "Bio Bio",
    "country_id": "43"
  },
  {
    "id": "722",
    "name": "Coquimbo",
    "country_id": "43"
  },
  {
    "id": "723",
    "name": "Libertador General Bernardo O",
    "country_id": "43"
  },
  {
    "id": "724",
    "name": "Los Lagos",
    "country_id": "43"
  },
  {
    "id": "725",
    "name": "Magellanes",
    "country_id": "43"
  },
  {
    "id": "726",
    "name": "Maule",
    "country_id": "43"
  },
  {
    "id": "727",
    "name": "Metropolitana",
    "country_id": "43"
  },
  {
    "id": "728",
    "name": "Metropolitana de Santiago",
    "country_id": "43"
  },
  {
    "id": "729",
    "name": "Tarapaca",
    "country_id": "43"
  },
  {
    "id": "730",
    "name": "Valparaiso",
    "country_id": "43"
  },
  {
    "id": "731",
    "name": "Anhui",
    "country_id": "44"
  },
  {
    "id": "732",
    "name": "Anhui Province",
    "country_id": "44"
  },
  {
    "id": "733",
    "name": "Anhui Sheng",
    "country_id": "44"
  },
  {
    "id": "734",
    "name": "Aomen",
    "country_id": "44"
  },
  {
    "id": "735",
    "name": "Beijing",
    "country_id": "44"
  },
  {
    "id": "736",
    "name": "Beijing Shi",
    "country_id": "44"
  },
  {
    "id": "737",
    "name": "Chongqing",
    "country_id": "44"
  },
  {
    "id": "738",
    "name": "Fujian",
    "country_id": "44"
  },
  {
    "id": "739",
    "name": "Fujian Sheng",
    "country_id": "44"
  },
  {
    "id": "740",
    "name": "Gansu",
    "country_id": "44"
  },
  {
    "id": "741",
    "name": "Guangdong",
    "country_id": "44"
  },
  {
    "id": "742",
    "name": "Guangdong Sheng",
    "country_id": "44"
  },
  {
    "id": "743",
    "name": "Guangxi",
    "country_id": "44"
  },
  {
    "id": "744",
    "name": "Guizhou",
    "country_id": "44"
  },
  {
    "id": "745",
    "name": "Hainan",
    "country_id": "44"
  },
  {
    "id": "746",
    "name": "Hebei",
    "country_id": "44"
  },
  {
    "id": "747",
    "name": "Heilongjiang",
    "country_id": "44"
  },
  {
    "id": "748",
    "name": "Henan",
    "country_id": "44"
  },
  {
    "id": "749",
    "name": "Hubei",
    "country_id": "44"
  },
  {
    "id": "750",
    "name": "Hunan",
    "country_id": "44"
  },
  {
    "id": "751",
    "name": "Jiangsu",
    "country_id": "44"
  },
  {
    "id": "752",
    "name": "Jiangsu Sheng",
    "country_id": "44"
  },
  {
    "id": "753",
    "name": "Jiangxi",
    "country_id": "44"
  },
  {
    "id": "754",
    "name": "Jilin",
    "country_id": "44"
  },
  {
    "id": "755",
    "name": "Liaoning",
    "country_id": "44"
  },
  {
    "id": "756",
    "name": "Liaoning Sheng",
    "country_id": "44"
  },
  {
    "id": "757",
    "name": "Nei Monggol",
    "country_id": "44"
  },
  {
    "id": "758",
    "name": "Ningxia Hui",
    "country_id": "44"
  },
  {
    "id": "759",
    "name": "Qinghai",
    "country_id": "44"
  },
  {
    "id": "760",
    "name": "Shaanxi",
    "country_id": "44"
  },
  {
    "id": "761",
    "name": "Shandong",
    "country_id": "44"
  },
  {
    "id": "762",
    "name": "Shandong Sheng",
    "country_id": "44"
  },
  {
    "id": "763",
    "name": "Shanghai",
    "country_id": "44"
  },
  {
    "id": "764",
    "name": "Shanxi",
    "country_id": "44"
  },
  {
    "id": "765",
    "name": "Sichuan",
    "country_id": "44"
  },
  {
    "id": "766",
    "name": "Tianjin",
    "country_id": "44"
  },
  {
    "id": "767",
    "name": "Xianggang",
    "country_id": "44"
  },
  {
    "id": "768",
    "name": "Xinjiang",
    "country_id": "44"
  },
  {
    "id": "769",
    "name": "Xizang",
    "country_id": "44"
  },
  {
    "id": "770",
    "name": "Yunnan",
    "country_id": "44"
  },
  {
    "id": "771",
    "name": "Zhejiang",
    "country_id": "44"
  },
  {
    "id": "772",
    "name": "Zhejiang Sheng",
    "country_id": "44"
  },
  {
    "id": "773",
    "name": "Christmas Island",
    "country_id": "45"
  },
  {
    "id": "774",
    "name": "Cocos (Keeling) Islands",
    "country_id": "46"
  },
  {
    "id": "775",
    "name": "Amazonas",
    "country_id": "47"
  },
  {
    "id": "776",
    "name": "Antioquia",
    "country_id": "47"
  },
  {
    "id": "777",
    "name": "Arauca",
    "country_id": "47"
  },
  {
    "id": "778",
    "name": "Atlantico",
    "country_id": "47"
  },
  
]);