const models = require('../models/index');
const pmodels = require('../production_models'); 

pmodels.prod_sequelize.query("SELECT * FROM GpDelegateApprovals ", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async users => {
  
   
      for (let i = 0; i < users.length ; i++) {

        let element = users[i]  
        
       // console.log("eeeee",element)

        let  tempobj = {
          id:element.id,
          fundId: element.fundId,
          delegateId: element.delegateId,
          subscriptionId: element.subscriptionId,
          lpId:element.lpId,
          approvedByDelegate:element.approvedByDelegate,
          createdAt: element.createdAt,
          updatedAt: element.updatedAt 
        }        
         
        let info = await models.FundSubscription.findOne({attributes:['fundId'],where:{  
          id:element.subscriptionId 
        }})

        //console.log("****info*******",info)

        if(info){
          
              let userinfo = await models.GpDelegateApprovals.findOne({attributes:['id'],where:{
                fundId:element.fundId,
                delegateId:element.delegateId,
                lpId:element.lpId
              }});

              if(!userinfo){
              console.log("insert ***********",element.id)    
              await models.GpDelegateApprovals.create(tempobj);
            
            } else {
              console.log("update ***********",element.id)  

              await models.GpDelegateApprovals.update(tempobj,{where:{id:element.id}});
            }

          }
 
    }       
     
})


 