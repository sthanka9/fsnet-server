const models = require('../models/index');
const pmodels = require('../production_models'); 

pmodels.prod_sequelize.query("SELECT * FROM userroles ", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async users => {
  
   
      for (let i = 0; i < users.length ; i++) {

        let element = users[i]   
 

        let  tempobj = {
          
          userId:element.userId,
          roleId:element.roleId 
        }        
        
        let isuser = await models.User.findOne({attributes:['id'],where:{id:element.userId}});

        if(isuser){

              let userinfo = await models.UserRole.findOne({attributes:['id'],where:{userId:element.userId}});

              if(!userinfo){
              console.log("insert ***********",element.userId)    
              await models.UserRole.create(tempobj);
            
            } else {
              console.log("update ***********",element.userId)  

              await models.UserRole.update(tempobj,{where:{userId:element.userId}});
            }

        }
 
    }       
     
})


 