const models = require('../models/index');

// remove all 
models.Role.destroy({
    truncate: true
});

models.Permission.destroy({
    truncate: true
});

models.RolePermission.destroy({
    truncate: true
});

models.Role.bulkCreate([{
    id: 1,
    role: 'FSNET Administrator',
    slug: 'fsnetadministrator',
    isLocked: 1
}, {
    id: 2,
    role: 'GP',
    slug: 'gp',
    isLocked: 1
}, {
    id: 3,
    role: 'GP Delegate',
    slug: 'gpdelegate',
    isLocked: 1
}, {
    id: 4,
    role: 'LP',
    slug: 'lp',
    isLocked: 1
}, {
    id: 5,
    role: 'LP Delegate',
    slug: 'lpdelegate',
    isLocked: 1
}, {
    id: 6,
    role: 'Secondary GP',
    slug: 'secondaryGP',
    isLocked: 1
}, {
    id: 7,
    role: 'Secondary LP',
    slug: 'secondaryLP',
    isLocked: 1
}, {
    id: 8,
    role: 'Offline LP',
    slug: 'offlineLP',
    isLocked: 1
}
]);

models.Permission.bulkCreate([{
    id: 1,
    permission: 'fund create',
    slug: 'fundCreate'
}, {
    id: 2,
    permission: 'View Fund',
    slug: 'viewFund'
}, {
    id: 3,
    permission: 'List Fund',
    slug: 'listFund'
}, {
    id: 4,
    permission: 'Commitment Change Gp Sign',
    slug: 'commitmentChangeGpSign'
},
{
    id: 5,
    permission: 'LP Document Locker',
    slug: 'lpDocumentLocker'
}, {
    id: 6,
    permission: 'GP Document Locker',
    slug: 'gpDocumentLocker'
}, {
    id: 7,
    permission: 'Subscription create',
    slug: 'subscriptionCreate'
}, {
    id: 8,
    permission: 'View Subscription',
    slug: 'viewSubscription'
}, {
    id: 9,
    permission: 'List Subscriptions',
    slug: 'listSubscriptions'
}, {
    id: 10,
    permission: 'lp Commitment Change Request',
    slug: 'lpCommitmentChangeRequest'
}, {
    id: 11,
    permission: 'lp New Subscription Action',
    slug: 'lpNewSubscriptionAction'
}, {
    id: 12,
    permission: 'subscription Lp Delegate Manage',
    slug: 'subscriptionLpDelegateManage'
},
{
    id: 13,
    permission: 'subscription Pending Actions',
    slug: 'lpPendingActions'
},
{
    id: 14,
    permission: 'Fund Pending Actions',
    slug: 'gpPendingActions'
},
{
    id: 15,
    permission: 'LP Signature Upload',
    slug: 'lpSignature'
},
{
    id: 16,
    permission: 'Gp Delegate Invite',
    slug: 'gpDelegateInvite'
},
{
    id: 17,
    permission: 'Gp Delegate Invite',
    slug: 'gpSendMessageToLp'
},
{
    id: 18,
    permission: 'Close Fund',
    slug: 'closeFund'
},
{
    id: 19,
    permission: 'Deactivate Fund',
    slug: 'deactivateFund'
},
{
    id: 20,
    permission: 'Send Remainder to LP',
    slug: 'gpSendRemainderToLp'
},

{
    id: 21,
    permission: 'View Subscription Pdf',
    slug: 'viewSubscriptionPdf'
},
{
    id: 22,
    permission: 'Approval Required for Closing',
    slug: 'approvalForClose'
},
{
    id: 23,
    permission: 'Create Firm',
    slug: 'createFirm'
},
{
    id: 24,
    permission: 'Add Questions',
    slug: 'addQuestions'
}, {
    id: 25,
    permission: 'Fund Delete',
    slug: 'fundDelete'
}, {
    id: 26,
    permission: 'Impersonate User',
    slug: 'impersonateUser'
}, {
    id: 27,
    permission: 'Firm List',
    slug: 'firmList'
}, 
{
    id: 28,
    permission: 'Firm Fund List',
    slug: 'firmFundList'
}, 
{
    id: 29,
    permission: 'Admin Side Check',
    slug: 'adminCheck'
},
{
    id: 30,
    permission: 'Secondary GP Invite',
    slug: 'gpSignatoryInvite'
},
]);

