const models = require('../production_models');

const qa_models = require('../models/index');
const _ = require('lodash');

//update [FundSubscriptions] set invitedDate=createdAt , isInvestorInvited = 2  where fundId not in (select id from [dbo].[Funds] where statusId = 12)

//While running this file comment out hooks in fund subscription model
models.FundSubscription.findAll({})
.then(fundSData=> {

    const fundSArray=[];

    fundSData.forEach(async data=>{

            let fundSObj={};
            fundSObj.id = data.id,
            fundSObj.fundId = data.fundId,
            fundSObj.lpId = data.lpId,
            fundSObj.investorType = data.investorType,
            fundSObj.whatSTATEIsTheEntityRegistered = data.whatSTATEIsTheEntityRegistered,
            fundSObj.name = data.name,
            fundSObj.areYouSubscribingAsJointIndividual = data.areYouSubscribingAsJointIndividual,
            fundSObj.areYouAccreditedInvestor = data.areYouAccreditedInvestor,
            fundSObj.indirectBeneficialOwnersSubjectFOIAStatutes = data.indirectBeneficialOwnersSubjectFOIAStatutes,
            fundSObj.areYouQualifiedPurchaser = data.areYouQualifiedPurchaser,
            fundSObj.areYouQualifiedClient = data.areYouQualifiedClient,
            fundSObj.typeOfLegalOwnership = data.typeOfLegalOwnership,
            fundSObj.mailingAddressStreet = data.mailingAddressStreet,
            fundSObj.mailingAddressCity = data.mailingAddressCity,
            fundSObj.mailingAddressState = data.mailingAddressState,
            fundSObj.mailingAddressZip = data.mailingAddressZip,
            fundSObj.mailingAddressPhoneNumber = data.mailingAddressPhoneNumber,
            fundSObj.otherInvestorAttributes = data.otherInvestorAttributes,
            fundSObj.investorSubType = data.investorSubType,
            fundSObj.otherInvestorSubType = data.otherInvestorSubType,
            fundSObj.entityName = data.entityName,
            fundSObj.jurisdictionEntityLegallyRegistered = data.jurisdictionEntityLegallyRegistered,
            fundSObj.isEntityTaxExemptForUSFederalIncomeTax = data.isEntityTaxExemptForUSFederalIncomeTax,
            fundSObj.isEntityUS501c3 = data.isEntityUS501c3,
            fundSObj.istheEntityFundOfFundsOrSimilarTypeVehicle = data.istheEntityFundOfFundsOrSimilarTypeVehicle,
            fundSObj.releaseInvestmentEntityRequired = data.releaseInvestmentEntityRequired,
            fundSObj.companiesAct = data.companiesAct,
            fundSObj.numberOfDirectEquityOwners = data.numberOfDirectEquityOwners? data.numberOfDirectEquityOwners:1,
            fundSObj.existingOrProspectiveInvestorsOfTheFund = data.existingOrProspectiveInvestorsOfTheFund,
            fundSObj.numberOfexistingOrProspectives = data.numberOfexistingOrProspectives,
            fundSObj.entityProposingAcquiringInvestment = data.entityProposingAcquiringInvestment ,
            fundSObj.anyOtherInvestorInTheFund = data.anyOtherInvestorInTheFund,
            fundSObj.entityHasMadeInvestmentsPriorToThedate = data.entityHasMadeInvestmentsPriorToThedate ,
            fundSObj.partnershipWillNotConstituteMoreThanFortyPercent = data.partnershipWillNotConstituteMoreThanFortyPercent ,
            fundSObj.beneficialInvestmentMadeByTheEntity = data.beneficialInvestmentMadeByTheEntity ,
            fundSObj.employeeBenefitPlan = data.employeeBenefitPlan,
            fundSObj.planAsDefinedInSection4975e1 = data.planAsDefinedInSection4975e1,
            fundSObj.benefitPlanInvestor = data.benefitPlanInvestor,
            fundSObj.totalValueOfEquityInterests = data.totalValueOfEquityInterests,
            fundSObj.fiduciaryEntityIvestment = data.fiduciaryEntityIvestment,
            fundSObj.entityDecisionToInvestInFund = data.entityDecisionToInvestInFund,
            fundSObj.aggrement1 = data.aggrement1,
            fundSObj.aggrement2 = data.aggrement2,
            fundSObj.status = data.status,
            fundSObj.createdBy = data.createdBy,
            fundSObj.updatedBy = data.updatedBy,
            fundSObj.createdAt = data.createdAt,
            fundSObj.updatedAt = data.updatedAt,
            fundSObj.numberOfGrantorsOfTheTrust = data.numberOfGrantorsOfTheTrust,
            fundSObj.trustLegallyDomiciled = data.trustLegallyDomiciled,
            fundSObj.selectState = data.selectState,
            fundSObj.trustName = data.trustName,
            fundSObj.isTrust501c3 = data.isTrust501c3,
            fundSObj.legalTitleDesignation = data.legalTitleDesignation,
            fundSObj.mailingAddressCountry = data.mailingAddressCountry,
            fundSObj.lpCapitalCommitment = 0.0,
            fundSObj.isSubjectToDisqualifyingEvent = data.isSubjectToDisqualifyingEvent,
            fundSObj.fundManagerInfo = data.fundManagerInfo,
            fundSObj.entityTitle = data.entityTitle,
            fundSObj.acceptedByGp = data.acceptedByGp,
            fundSObj.investorCount = data.investorCount? data.investorCount:1,
            fundSObj.subscriptionHtml = data.subscriptionHtml? data.subscriptionHtml :null,
            fundSObj.subscriptionHtmlLastUpdated = data.subscriptionHtmlLastUpdated,
            fundSObj.deletedAt = data.deletedAt,
            fundSObj.previousStatus = data.previousStatus,
            fundSObj.subscriptionAgreementPath = data.subscriptionAgreementPath? data.subscriptionAgreementPath : null,
            fundSObj.legalTitle = data.legalTitle,
            fundSObj.trustTitle = data.trustTitle
            fundSObj.isPrimaryInvestment = 0,
            fundSObj.isLpSignatoriesNotified = 0,

            qa_models.FundSubscription.create(fundSObj)
            .then(res=>{
                if(res){

                    qa_models.sequelize.query(`update FundSubscriptions set lpCapitalCommitment = ${data.lpCapitalCommitment} where id = ${data.id}`)
                }
            })
           
        
    });

    //     qa_models.FundSubscription.bulkCreate(fundSArray)
    //     .then(res=>{
    //         console.log('===res',res)
    //     })
    //     .catch(e=>{
    //         console.log("==e",e)
    //     });
    // }

});

