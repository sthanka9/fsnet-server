const models = require('../production_models');

const qa_models = require('../models/index');

const { Op} = require('sequelize');
const path = require('path');
const azureStorage = require('azure-storage');
const blobService = azureStorage.createBlobService('fsnetrgdiag919','kfcvxevowzcVGXNMhJmbcpWR86AzYbvhSoIPlHXUzIpmhqy4MmjtFEH4AmYol0aWqMqE+RfP+1sNKtK6OHpw7w==');
const commonHelper = require('../helpers/commonHelper');
const uuidv1 = require('uuid/v1');
const fs = require('fs');


const containerName = 'prod';
const _downloadBlob = async (blobNamePath, saveTo) => {
    return new Promise((resolve, reject) => {
        blobService.getBlobToLocalFile(containerName, blobNamePath.replace(`${containerName}/`, ''), saveTo, function (error, serverBlob) {
            if (error) {
                reject(error);
            } else {
                resolve(saveTo);
            }
        });
    });
};


models.Fund.findAll({})
.then(async fundData=> {
    const fundArray=[];
    
    fundData.forEach(async data=>{
        let fundObj={};
        let p =null;
        let p1 =null;
        let additionalObj;
        if(data.fundImage) {
            p = data.fundImage;
            const filePath = `./assets/funds/images/${p.name}`
            commonHelper.ensureDirectoryExistence(filePath);
            _downloadBlob(p.path,`${filePath}`);
            p.path = filePath;
            p.uri =  `/assets/funds/images/${p.name}`;
            delete p.url;

            p = JSON.stringify(p)
        }
        if(data.partnershipDocument) {
            p1 = data.partnershipDocument;
            const filePath = `./assets/funds/${data.id}/${uuidv1()}.pdf`;
            commonHelper.ensureDirectoryExistence(filePath);
            _downloadBlob(p1.blob,filePath);
            p1.path = filePath;
            delete p1.url;
            delete p1.pageNumber;
            delete p1.blob;

            p1 =JSON.stringify(p1)
        }
        if(data.additionalSignatoryPages) {
            let p2 = data.additionalSignatoryPages;
            const filePath = `./assets/additionPages/${uuidv1()}.pdf`;
            commonHelper.ensureDirectoryExistence(filePath);
            _downloadBlob(p2.blob,filePath);
            p2.path=filePath
            //delete p2.originalname
            //delete p2.blob

            //additionalObj.path = filePath
            /*{"originalname":"CapitalCom.pdf","blob":"prod/funds/44/additionalSignatoryPages-3851188706181592-CapitalCom.pdf","path":"./assets/additionPages/4e87f940-db88-11e9-a2ee-25a1cf0009d0.pdf"}*/
            let tempobj = {}
            tempobj.path = filePath
            tempobj.originalname = p2.originalname
            additionalObj= JSON.stringify(tempobj)
            

        }else{
            additionalObj=null
        }
        
        let newitem = {}
        if(data.subscriptionAgreementPath){
            let tempSubscriptionAgPath = data.subscriptionAgreementPath
 
            Object.keys(tempSubscriptionAgPath).forEach(function(key) {
            
                let tempValue = tempSubscriptionAgPath[key]
            
                if(tempValue){
                    tempValue = tempValue.replace('prod/funds/','./assets/funds/')
                    tempValue = tempValue.replace('prod/default','default')
                    tempValue = tempValue.replace('DefaultSection1.docx','DefaultSection1.pdf')
                    tempValue = tempValue.replace('DefaultSection2.docx','DefaultSection2.pdf')
                    tempValue = tempValue.replace('DefaultSection3.docx','DefaultSection3.pdf')
                    tempValue = tempValue.replace('DefaultSection4.docx','DefaultSection4.pdf')
                    tempValue = tempValue.replace('DefaultSection5.docx','DefaultSection5.pdf')
                    tempValue = tempValue.replace('137.116.56.150','localhost')
                }
                newitem[key] = tempValue
            
            
            })
        }

        fundObj.id = data.id,
        fundObj.vcfirmId = data.vcfirmId,
        fundObj.gpId = data.gpId,
        fundObj.legalEntity = data.legalEntity,
        fundObj.fundHardCap = data.fundHardCap,
        fundObj.fundManagerLegalEntityName = data.fundManagerLegalEntityName,
        fundObj.fundTargetCommitment = data.fundTargetCommitment,
        fundObj.capitalCommitmentByFundManager = data.capitalCommitmentByFundManager,
        fundObj.partnershipDocument = p1,
        fundObj.fundImage = p,
        fundObj.percentageOfLPCommitment = data.percentageOfLPCommitment,
        fundObj.percentageOfLPAndGPAggregateCommitment = data.percentageOfLPAndGPAggregateCommitment,
        fundObj.deactivateReason = data.deactivateReason,
        fundObj.createdBy = data.createdBy,
        fundObj.updatedBy = data.updatedBy,
        fundObj.createdAt = data.createdAt,
        fundObj.updatedAt = data.updatedAt,
        fundObj.generalPartnersCapitalCommitmentindicated = data.generalPartnersCapitalCommitmentindicated,
        fundObj.fundType = data.fundType,
        fundObj.fundManagerTitle = data.fundManagerTitle,
        fundObj.fundCommonName = data.fundCommonName,
        fundObj.statusId = data.statusId,
        fundObj.fundManagerCommonName = data.fundManagerCommonName,
        fundObj.hardCapApplicationRule = data.hardCapApplicationRule,
        fundObj.subscriptionHtml = data.subscriptionHtml ?data.subscriptionHtml: null,
        fundObj.subscriptionHtmlLastUpdated = data.subscriptionHtmlLastUpdated,
        fundObj.subscriptionAgreementPath = data.subscriptionAgreementPath ? newitem : null,
        fundObj.additionalSignatoryPages = additionalObj,
        fundObj.additionalSignatoryPagesUpdatedDate = data.additionalSignatoryPagesUpdatedDate,
        fundObj.isAdditionalSignatoryPagesAddedToConslidatedPDF = data.isAdditionalSignatoryPagesAddedToConslidatedPDF

        let res = await qa_models.Fund.create(fundObj)
        // .then(res=>{
            if(res){
                console.log("*******************",additionalObj)
                //qa_models.sequelize.query(`update Funds set additionalSignatoryPages = ${additionalObj} where id = ${data.id}`)
            }
        // })
        // .catch(e=>{
        //     console.log("===e",e)
        // })
    });
});


