const models = require('../models/index');


  models.State.bulkCreate([{
    "id": "2301",
    "name": "Ntchisi",
    "country_id": "131"
  },
  {
    "id": "2302",
    "name": "Phalombe",
    "country_id": "131"
  },
  {
    "id": "2303",
    "name": "Rumphi",
    "country_id": "131"
  },
  {
    "id": "2304",
    "name": "Salima",
    "country_id": "131"
  },
  {
    "id": "2305",
    "name": "Thyolo",
    "country_id": "131"
  },
  {
    "id": "2306",
    "name": "Zomba Municipality",
    "country_id": "131"
  },
  {
    "id": "2307",
    "name": "Johor",
    "country_id": "132"
  },
  {
    "id": "2308",
    "name": "Kedah",
    "country_id": "132"
  },
  {
    "id": "2309",
    "name": "Kelantan",
    "country_id": "132"
  },
  {
    "id": "2310",
    "name": "Kuala Lumpur",
    "country_id": "132"
  },
  {
    "id": "2311",
    "name": "Labuan",
    "country_id": "132"
  },
  {
    "id": "2312",
    "name": "Melaka",
    "country_id": "132"
  },
  {
    "id": "2313",
    "name": "Negeri Johor",
    "country_id": "132"
  },
  {
    "id": "2314",
    "name": "Negeri Sembilan",
    "country_id": "132"
  },
  {
    "id": "2315",
    "name": "Pahang",
    "country_id": "132"
  },
  {
    "id": "2316",
    "name": "Penang",
    "country_id": "132"
  },
  {
    "id": "2317",
    "name": "Perak",
    "country_id": "132"
  },
  {
    "id": "2318",
    "name": "Perlis",
    "country_id": "132"
  },
  {
    "id": "2319",
    "name": "Pulau Pinang",
    "country_id": "132"
  },
  {
    "id": "2320",
    "name": "Sabah",
    "country_id": "132"
  },
  {
    "id": "2321",
    "name": "Sarawak",
    "country_id": "132"
  },
  {
    "id": "2322",
    "name": "Selangor",
    "country_id": "132"
  },
  {
    "id": "2323",
    "name": "Sembilan",
    "country_id": "132"
  },
  {
    "id": "2324",
    "name": "Terengganu",
    "country_id": "132"
  },
  {
    "id": "2325",
    "name": "Alif Alif",
    "country_id": "133"
  },
  {
    "id": "2326",
    "name": "Alif Dhaal",
    "country_id": "133"
  },
  {
    "id": "2327",
    "name": "Baa",
    "country_id": "133"
  },
  {
    "id": "2328",
    "name": "Dhaal",
    "country_id": "133"
  },
  {
    "id": "2329",
    "name": "Faaf",
    "country_id": "133"
  },
  {
    "id": "2330",
    "name": "Gaaf Alif",
    "country_id": "133"
  },
  {
    "id": "2331",
    "name": "Gaaf Dhaal",
    "country_id": "133"
  },
  {
    "id": "2332",
    "name": "Ghaviyani",
    "country_id": "133"
  },
  {
    "id": "2333",
    "name": "Haa Alif",
    "country_id": "133"
  },
  {
    "id": "2334",
    "name": "Haa Dhaal",
    "country_id": "133"
  },
  {
    "id": "2335",
    "name": "Kaaf",
    "country_id": "133"
  },
  {
    "id": "2336",
    "name": "Laam",
    "country_id": "133"
  },
  {
    "id": "2337",
    "name": "Lhaviyani",
    "country_id": "133"
  },
  {
    "id": "2338",
    "name": "Male",
    "country_id": "133"
  },
  {
    "id": "2339",
    "name": "Miim",
    "country_id": "133"
  },
  {
    "id": "2340",
    "name": "Nuun",
    "country_id": "133"
  },
  {
    "id": "2341",
    "name": "Raa",
    "country_id": "133"
  },
  {
    "id": "2342",
    "name": "Shaviyani",
    "country_id": "133"
  },
  {
    "id": "2343",
    "name": "Siin",
    "country_id": "133"
  },
  {
    "id": "2344",
    "name": "Thaa",
    "country_id": "133"
  },
  {
    "id": "2345",
    "name": "Vaav",
    "country_id": "133"
  },
  {
    "id": "2346",
    "name": "Bamako",
    "country_id": "134"
  },
  {
    "id": "2347",
    "name": "Gao",
    "country_id": "134"
  },
  {
    "id": "2348",
    "name": "Kayes",
    "country_id": "134"
  },
  {
    "id": "2349",
    "name": "Kidal",
    "country_id": "134"
  },
  {
    "id": "2350",
    "name": "Koulikoro",
    "country_id": "134"
  },
  {
    "id": "2351",
    "name": "Mopti",
    "country_id": "134"
  },
  {
    "id": "2352",
    "name": "Segou",
    "country_id": "134"
  },
  {
    "id": "2353",
    "name": "Sikasso",
    "country_id": "134"
  },
  {
    "id": "2354",
    "name": "Tombouctou",
    "country_id": "134"
  },
  {
    "id": "2355",
    "name": "Gozo and Comino",
    "country_id": "135"
  },
  {
    "id": "2356",
    "name": "Inner Harbour",
    "country_id": "135"
  },
  {
    "id": "2357",
    "name": "Northern",
    "country_id": "135"
  },
  {
    "id": "2358",
    "name": "Outer Harbour",
    "country_id": "135"
  },
  {
    "id": "2359",
    "name": "South Eastern",
    "country_id": "135"
  },
  {
    "id": "2360",
    "name": "Valletta",
    "country_id": "135"
  },
  {
    "id": "2361",
    "name": "Western",
    "country_id": "135"
  },
  {
    "id": "2362",
    "name": "Castletown",
    "country_id": "136"
  },
  {
    "id": "2363",
    "name": "Douglas",
    "country_id": "136"
  },
  {
    "id": "2364",
    "name": "Laxey",
    "country_id": "136"
  },
  {
    "id": "2365",
    "name": "Onchan",
    "country_id": "136"
  },
  {
    "id": "2366",
    "name": "Peel",
    "country_id": "136"
  },
  {
    "id": "2367",
    "name": "Port Erin",
    "country_id": "136"
  },
  {
    "id": "2368",
    "name": "Port Saint Mary",
    "country_id": "136"
  },
  {
    "id": "2369",
    "name": "Ramsey",
    "country_id": "136"
  },
  {
    "id": "2370",
    "name": "Ailinlaplap",
    "country_id": "137"
  },
  {
    "id": "2371",
    "name": "Ailuk",
    "country_id": "137"
  },
  {
    "id": "2372",
    "name": "Arno",
    "country_id": "137"
  },
  {
    "id": "2373",
    "name": "Aur",
    "country_id": "137"
  },
  {
    "id": "2374",
    "name": "Bikini",
    "country_id": "137"
  },
  {
    "id": "2375",
    "name": "Ebon",
    "country_id": "137"
  },
  {
    "id": "2376",
    "name": "Enewetak",
    "country_id": "137"
  },
  {
    "id": "2377",
    "name": "Jabat",
    "country_id": "137"
  },
  {
    "id": "2378",
    "name": "Jaluit",
    "country_id": "137"
  },
  {
    "id": "2379",
    "name": "Kili",
    "country_id": "137"
  },
  {
    "id": "2380",
    "name": "Kwajalein",
    "country_id": "137"
  },
  {
    "id": "2381",
    "name": "Lae",
    "country_id": "137"
  },
  {
    "id": "2382",
    "name": "Lib",
    "country_id": "137"
  },
  {
    "id": "2383",
    "name": "Likiep",
    "country_id": "137"
  },
  {
    "id": "2384",
    "name": "Majuro",
    "country_id": "137"
  },
  {
    "id": "2385",
    "name": "Maloelap",
    "country_id": "137"
  },
  {
    "id": "2386",
    "name": "Mejit",
    "country_id": "137"
  },
  {
    "id": "2387",
    "name": "Mili",
    "country_id": "137"
  },
  {
    "id": "2388",
    "name": "Namorik",
    "country_id": "137"
  },
  {
    "id": "2389",
    "name": "Namu",
    "country_id": "137"
  },
  {
    "id": "2390",
    "name": "Rongelap",
    "country_id": "137"
  },
  {
    "id": "2391",
    "name": "Ujae",
    "country_id": "137"
  },
  {
    "id": "2392",
    "name": "Utrik",
    "country_id": "137"
  },
  {
    "id": "2393",
    "name": "Wotho",
    "country_id": "137"
  },
  {
    "id": "2394",
    "name": "Wotje",
    "country_id": "137"
  },
  {
    "id": "2395",
    "name": "Fort-de-France",
    "country_id": "138"
  },
  {
    "id": "2396",
    "name": "La Trinite",
    "country_id": "138"
  },
  {
    "id": "2397",
    "name": "Le Marin",
    "country_id": "138"
  },
  {
    "id": "2398",
    "name": "Saint-Pierre",
    "country_id": "138"
  },
  {
    "id": "2399",
    "name": "Adrar",
    "country_id": "139"
  },
  {
    "id": "2400",
    "name": "Assaba",
    "country_id": "139"
  },
  {
    "id": "2401",
    "name": "Brakna",
    "country_id": "139"
  },
  {
    "id": "2402",
    "name": "Dhakhlat Nawadibu",
    "country_id": "139"
  },
  {
    "id": "2403",
    "name": "Hudh-al-Gharbi",
    "country_id": "139"
  },
  {
    "id": "2404",
    "name": "Hudh-ash-Sharqi",
    "country_id": "139"
  },
  {
    "id": "2405",
    "name": "Inshiri",
    "country_id": "139"
  },
  {
    "id": "2406",
    "name": "Nawakshut",
    "country_id": "139"
  },
  {
    "id": "2407",
    "name": "Qidimagha",
    "country_id": "139"
  },
  {
    "id": "2408",
    "name": "Qurqul",
    "country_id": "139"
  },
  {
    "id": "2409",
    "name": "Taqant",
    "country_id": "139"
  },
  {
    "id": "2410",
    "name": "Tiris Zammur",
    "country_id": "139"
  },
  {
    "id": "2411",
    "name": "Trarza",
    "country_id": "139"
  },
  {
    "id": "2412",
    "name": "Black River",
    "country_id": "140"
  },
  {
    "id": "2413",
    "name": "Eau Coulee",
    "country_id": "140"
  },
  {
    "id": "2414",
    "name": "Flacq",
    "country_id": "140"
  },
  {
    "id": "2415",
    "name": "Floreal",
    "country_id": "140"
  },
  {
    "id": "2416",
    "name": "Grand Port",
    "country_id": "140"
  },
  {
    "id": "2417",
    "name": "Moka",
    "country_id": "140"
  },
  {
    "id": "2418",
    "name": "Pamplempousses",
    "country_id": "140"
  },
  {
    "id": "2419",
    "name": "Plaines Wilhelm",
    "country_id": "140"
  },
  {
    "id": "2420",
    "name": "Port Louis",
    "country_id": "140"
  },
  {
    "id": "2421",
    "name": "Riviere du Rempart",
    "country_id": "140"
  },
  {
    "id": "2422",
    "name": "Rodrigues",
    "country_id": "140"
  },
  {
    "id": "2423",
    "name": "Rose Hill",
    "country_id": "140"
  },
  {
    "id": "2424",
    "name": "Savanne",
    "country_id": "140"
  },
  {
    "id": "2425",
    "name": "Mayotte",
    "country_id": "141"
  },
  {
    "id": "2426",
    "name": "Pamanzi",
    "country_id": "141"
  },
  {
    "id": "2427",
    "name": "Aguascalientes",
    "country_id": "142"
  },
  {
    "id": "2428",
    "name": "Baja California",
    "country_id": "142"
  },
  {
    "id": "2429",
    "name": "Baja California Sur",
    "country_id": "142"
  },
  {
    "id": "2430",
    "name": "Campeche",
    "country_id": "142"
  },
  {
    "id": "2431",
    "name": "Chiapas",
    "country_id": "142"
  },
  {
    "id": "2432",
    "name": "Chihuahua",
    "country_id": "142"
  },
  {
    "id": "2433",
    "name": "Coahuila",
    "country_id": "142"
  },
  {
    "id": "2434",
    "name": "Colima",
    "country_id": "142"
  },
  {
    "id": "2435",
    "name": "Distrito Federal",
    "country_id": "142"
  },
  {
    "id": "2436",
    "name": "Durango",
    "country_id": "142"
  },
  {
    "id": "2437",
    "name": "Estado de Mexico",
    "country_id": "142"
  },
  {
    "id": "2438",
    "name": "Guanajuato",
    "country_id": "142"
  },
  {
    "id": "2439",
    "name": "Guerrero",
    "country_id": "142"
  },
  {
    "id": "2440",
    "name": "Hidalgo",
    "country_id": "142"
  },
  {
    "id": "2441",
    "name": "Jalisco",
    "country_id": "142"
  },
  {
    "id": "2442",
    "name": "Mexico",
    "country_id": "142"
  },
  {
    "id": "2443",
    "name": "Michoacan",
    "country_id": "142"
  },
  {
    "id": "2444",
    "name": "Morelos",
    "country_id": "142"
  },
  {
    "id": "2445",
    "name": "Nayarit",
    "country_id": "142"
  },
  {
    "id": "2446",
    "name": "Nuevo Leon",
    "country_id": "142"
  },
  {
    "id": "2447",
    "name": "Oaxaca",
    "country_id": "142"
  },
  {
    "id": "2448",
    "name": "Puebla",
    "country_id": "142"
  },
  {
    "id": "2449",
    "name": "Queretaro",
    "country_id": "142"
  },
  {
    "id": "2450",
    "name": "Quintana Roo",
    "country_id": "142"
  },
  {
    "id": "2451",
    "name": "San Luis Potosi",
    "country_id": "142"
  },
  {
    "id": "2452",
    "name": "Sinaloa",
    "country_id": "142"
  },
  {
    "id": "2453",
    "name": "Sonora",
    "country_id": "142"
  },
  {
    "id": "2454",
    "name": "Tabasco",
    "country_id": "142"
  },
  {
    "id": "2455",
    "name": "Tamaulipas",
    "country_id": "142"
  },
  {
    "id": "2456",
    "name": "Tlaxcala",
    "country_id": "142"
  },
  {
    "id": "2457",
    "name": "Veracruz",
    "country_id": "142"
  },
  {
    "id": "2458",
    "name": "Yucatan",
    "country_id": "142"
  },
  {
    "id": "2459",
    "name": "Zacatecas",
    "country_id": "142"
  },
  {
    "id": "2460",
    "name": "Chuuk",
    "country_id": "143"
  },
  {
    "id": "2461",
    "name": "Kusaie",
    "country_id": "143"
  },
  {
    "id": "2462",
    "name": "Pohnpei",
    "country_id": "143"
  },
  {
    "id": "2463",
    "name": "Yap",
    "country_id": "143"
  },
  {
    "id": "2464",
    "name": "Balti",
    "country_id": "144"
  },
  {
    "id": "2465",
    "name": "Cahul",
    "country_id": "144"
  },
  {
    "id": "2466",
    "name": "Chisinau",
    "country_id": "144"
  },
  {
    "id": "2467",
    "name": "Chisinau Oras",
    "country_id": "144"
  },
  {
    "id": "2468",
    "name": "Edinet",
    "country_id": "144"
  },
  {
    "id": "2469",
    "name": "Gagauzia",
    "country_id": "144"
  },
  {
    "id": "2470",
    "name": "Lapusna",
    "country_id": "144"
  },
  {
    "id": "2471",
    "name": "Orhei",
    "country_id": "144"
  },
  {
    "id": "2472",
    "name": "Soroca",
    "country_id": "144"
  },
  {
    "id": "2473",
    "name": "Taraclia",
    "country_id": "144"
  },
  {
    "id": "2474",
    "name": "Tighina",
    "country_id": "144"
  },
  {
    "id": "2475",
    "name": "Transnistria",
    "country_id": "144"
  },
  {
    "id": "2476",
    "name": "Ungheni",
    "country_id": "144"
  },
  {
    "id": "2477",
    "name": "Fontvieille",
    "country_id": "145"
  },
  {
    "id": "2478",
    "name": "La Condamine",
    "country_id": "145"
  },
  {
    "id": "2479",
    "name": "Monaco-Ville",
    "country_id": "145"
  },
  {
    "id": "2480",
    "name": "Monte Carlo",
    "country_id": "145"
  },
  {
    "id": "2481",
    "name": "Arhangaj",
    "country_id": "146"
  },
  {
    "id": "2482",
    "name": "Bajan-Olgij",
    "country_id": "146"
  },
  {
    "id": "2483",
    "name": "Bajanhongor",
    "country_id": "146"
  },
  {
    "id": "2484",
    "name": "Bulgan",
    "country_id": "146"
  },
  {
    "id": "2485",
    "name": "Darhan-Uul",
    "country_id": "146"
  },
  {
    "id": "2486",
    "name": "Dornod",
    "country_id": "146"
  },
  {
    "id": "2487",
    "name": "Dornogovi",
    "country_id": "146"
  },
  {
    "id": "2488",
    "name": "Dundgovi",
    "country_id": "146"
  },
  {
    "id": "2489",
    "name": "Govi-Altaj",
    "country_id": "146"
  },
  {
    "id": "2490",
    "name": "Govisumber",
    "country_id": "146"
  },
  {
    "id": "2491",
    "name": "Hentij",
    "country_id": "146"
  },
  {
    "id": "2492",
    "name": "Hovd",
    "country_id": "146"
  },
  {
    "id": "2493",
    "name": "Hovsgol",
    "country_id": "146"
  },
  {
    "id": "2494",
    "name": "Omnogovi",
    "country_id": "146"
  },
  {
    "id": "2495",
    "name": "Orhon",
    "country_id": "146"
  },
  {
    "id": "2496",
    "name": "Ovorhangaj",
    "country_id": "146"
  },
  {
    "id": "2497",
    "name": "Selenge",
    "country_id": "146"
  },
  {
    "id": "2498",
    "name": "Suhbaatar",
    "country_id": "146"
  },
  {
    "id": "2499",
    "name": "Tov",
    "country_id": "146"
  },
  {
    "id": "2500",
    "name": "Ulaanbaatar",
    "country_id": "146"
  },
  {
    "id": "2501",
    "name": "Uvs",
    "country_id": "146"
  },
  {
    "id": "2502",
    "name": "Zavhan",
    "country_id": "146"
  },
  {
    "id": "2503",
    "name": "Montserrat",
    "country_id": "147"
  },
  {
    "id": "2504",
    "name": "Agadir",
    "country_id": "148"
  },
  {
    "id": "2505",
    "name": "Casablanca",
    "country_id": "148"
  },
  {
    "id": "2506",
    "name": "Chaouia-Ouardigha",
    "country_id": "148"
  },
  {
    "id": "2507",
    "name": "Doukkala-Abda",
    "country_id": "148"
  },
  {
    "id": "2508",
    "name": "Fes-Boulemane",
    "country_id": "148"
  },
  {
    "id": "2509",
    "name": "Gharb-Chrarda-Beni Hssen",
    "country_id": "148"
  },
  {
    "id": "2510",
    "name": "Guelmim",
    "country_id": "148"
  },
  {
    "id": "2511",
    "name": "Kenitra",
    "country_id": "148"
  },
  {
    "id": "2512",
    "name": "Marrakech-Tensift-Al Haouz",
    "country_id": "148"
  },
  {
    "id": "2513",
    "name": "Meknes-Tafilalet",
    "country_id": "148"
  },
  {
    "id": "2514",
    "name": "Oriental",
    "country_id": "148"
  },
  {
    "id": "2515",
    "name": "Oujda",
    "country_id": "148"
  },
  {
    "id": "2516",
    "name": "Province de Tanger",
    "country_id": "148"
  },
  {
    "id": "2517",
    "name": "Rabat-Sale-Zammour-Zaer",
    "country_id": "148"
  },
  {
    "id": "2518",
    "name": "Sala Al Jadida",
    "country_id": "148"
  },
  {
    "id": "2519",
    "name": "Settat",
    "country_id": "148"
  },
  {
    "id": "2520",
    "name": "Souss Massa-Draa",
    "country_id": "148"
  },
  {
    "id": "2521",
    "name": "Tadla-Azilal",
    "country_id": "148"
  },
  {
    "id": "2522",
    "name": "Tangier-Tetouan",
    "country_id": "148"
  },
  {
    "id": "2523",
    "name": "Taza-Al Hoceima-Taounate",
    "country_id": "148"
  },
  {
    "id": "2524",
    "name": "Wilaya de Casablanca",
    "country_id": "148"
  },
  {
    "id": "2525",
    "name": "Wilaya de Rabat-Sale",
    "country_id": "148"
  },
  {
    "id": "2526",
    "name": "Cabo Delgado",
    "country_id": "149"
  },
  {
    "id": "2527",
    "name": "Gaza",
    "country_id": "149"
  },
  {
    "id": "2528",
    "name": "Inhambane",
    "country_id": "149"
  },
  {
    "id": "2529",
    "name": "Manica",
    "country_id": "149"
  },
  {
    "id": "2530",
    "name": "Maputo",
    "country_id": "149"
  },
  {
    "id": "2531",
    "name": "Maputo Provincia",
    "country_id": "149"
  },
  {
    "id": "2532",
    "name": "Nampula",
    "country_id": "149"
  },
  {
    "id": "2533",
    "name": "Niassa",
    "country_id": "149"
  },
  {
    "id": "2534",
    "name": "Sofala",
    "country_id": "149"
  },
  {
    "id": "2535",
    "name": "Tete",
    "country_id": "149"
  },
  {
    "id": "2536",
    "name": "Zambezia",
    "country_id": "149"
  },
  {
    "id": "2537",
    "name": "Ayeyarwady",
    "country_id": "150"
  },
  {
    "id": "2538",
    "name": "Bago",
    "country_id": "150"
  },
  {
    "id": "2539",
    "name": "Chin",
    "country_id": "150"
  },
  {
    "id": "2540",
    "name": "Kachin",
    "country_id": "150"
  },
  {
    "id": "2541",
    "name": "Kayah",
    "country_id": "150"
  },
  {
    "id": "2542",
    "name": "Kayin",
    "country_id": "150"
  },
  {
    "id": "2543",
    "name": "Magway",
    "country_id": "150"
  },
  {
    "id": "2544",
    "name": "Mandalay",
    "country_id": "150"
  },
  {
    "id": "2545",
    "name": "Mon",
    "country_id": "150"
  },
  {
    "id": "2546",
    "name": "Nay Pyi Taw",
    "country_id": "150"
  },
  {
    "id": "2547",
    "name": "Rakhine",
    "country_id": "150"
  },
  {
    "id": "2548",
    "name": "Sagaing",
    "country_id": "150"
  },
  {
    "id": "2549",
    "name": "Shan",
    "country_id": "150"
  },
  {
    "id": "2550",
    "name": "Tanintharyi",
    "country_id": "150"
  },
  {
    "id": "2551",
    "name": "Yangon",
    "country_id": "150"
  },
  {
    "id": "2552",
    "name": "Caprivi",
    "country_id": "151"
  },
  {
    "id": "2553",
    "name": "Erongo",
    "country_id": "151"
  },
  {
    "id": "2554",
    "name": "Hardap",
    "country_id": "151"
  },
  {
    "id": "2555",
    "name": "Karas",
    "country_id": "151"
  },
  {
    "id": "2556",
    "name": "Kavango",
    "country_id": "151"
  },
  {
    "id": "2557",
    "name": "Khomas",
    "country_id": "151"
  },
  {
    "id": "2558",
    "name": "Kunene",
    "country_id": "151"
  },
  {
    "id": "2559",
    "name": "Ohangwena",
    "country_id": "151"
  },
  {
    "id": "2560",
    "name": "Omaheke",
    "country_id": "151"
  },
  {
    "id": "2561",
    "name": "Omusati",
    "country_id": "151"
  },
  {
    "id": "2562",
    "name": "Oshana",
    "country_id": "151"
  },
  {
    "id": "2563",
    "name": "Oshikoto",
    "country_id": "151"
  },
  {
    "id": "2564",
    "name": "Otjozondjupa",
    "country_id": "151"
  },
  {
    "id": "2565",
    "name": "Yaren",
    "country_id": "152"
  },
  {
    "id": "2566",
    "name": "Bagmati",
    "country_id": "153"
  },
  {
    "id": "2567",
    "name": "Bheri",
    "country_id": "153"
  },
  {
    "id": "2568",
    "name": "Dhawalagiri",
    "country_id": "153"
  },
  {
    "id": "2569",
    "name": "Gandaki",
    "country_id": "153"
  },
  {
    "id": "2570",
    "name": "Janakpur",
    "country_id": "153"
  },
  {
    "id": "2571",
    "name": "Karnali",
    "country_id": "153"
  },
  {
    "id": "2572",
    "name": "Koshi",
    "country_id": "153"
  },
  {
    "id": "2573",
    "name": "Lumbini",
    "country_id": "153"
  },
  {
    "id": "2574",
    "name": "Mahakali",
    "country_id": "153"
  },
  {
    "id": "2575",
    "name": "Mechi",
    "country_id": "153"
  },
  {
    "id": "2576",
    "name": "Narayani",
    "country_id": "153"
  },
  {
    "id": "2577",
    "name": "Rapti",
    "country_id": "153"
  },
  {
    "id": "2578",
    "name": "Sagarmatha",
    "country_id": "153"
  },
  {
    "id": "2579",
    "name": "Seti",
    "country_id": "153"
  },
  {
    "id": "2580",
    "name": "Bonaire",
    "country_id": "154"
  },
  {
    "id": "2581",
    "name": "Curacao",
    "country_id": "154"
  },
  {
    "id": "2582",
    "name": "Saba",
    "country_id": "154"
  },
  {
    "id": "2583",
    "name": "Sint Eustatius",
    "country_id": "154"
  },]);