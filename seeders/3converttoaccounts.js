const models = require('../models/index');
const pmodels = require('../production_models'); 



models.User.findAll({
    order: [['id', 'ASC']]
    }).then(async users =>{
 
        

    for (let i = 0; i < users.length ; i++) {
        let user = users[i]
    console.log("*************userid***************",user.id)
    let accountId = 0
    let account = {}
    let olduserdetails = {}
    account = await models.Account.findOne({
        where: { email: user.email }
    })        
    //if account exists with same email id
    if(account){
        accountId = account.id
        await models.User.update({accountId}, { where: { id: user.id } });

        //if there is no default userid
        if(!account.defaultUserId){

            olduserdetails =  await models.User.findOne({
                where: { email: user.email },
                order: [['createdAt', 'ASC']]
            })

            await models.Account.update({defaultUserId:olduserdetails.id}, { where: { id: accountId } });
        }

    } else { // new account
        
        olduserdetails =  await models.User.findOne({
            where: { email: user.email },
            order: [['createdAt', 'ASC']]
        })
        
        let accountdata = {
            firstName : olduserdetails.firstName,
            lastName : olduserdetails.lastName,
            email : olduserdetails.email,
            middleName : olduserdetails.middleName,
            defaultUserId : olduserdetails.id,
            password : olduserdetails.password,
            cellNumber : olduserdetails.cellNumber, 
            isEmailConfirmed : olduserdetails.isEmailConfirmed,
            city : olduserdetails.city,
            isImpersonatingAllowed : olduserdetails.isImpersonatingAllowed,
            streetAddress1 : olduserdetails.streetAddress1,
            streetAddress2 : olduserdetails.streetAddress2,
            zipcode : olduserdetails.zipcode,
            isEmailNotification : olduserdetails.isEmailNotification,
            country : olduserdetails.country,
            state : olduserdetails.state,                
            organizationName : olduserdetails.organizationName,
            deletedAt : olduserdetails.deletedAt
        }            
        account = await models.Account.create(accountdata)
        accountId = account.id
        
        let signaturePic = olduserdetails.signaturePic
        await models.Account.update({signaturePic}, { where: { id: accountId } });

        let profilePic = JSON.stringify(olduserdetails.profilePic)
        await models.Account.update({profilePic}, { where: { id: accountId } });
   
        let primaryContact = JSON.stringify(olduserdetails.primaryContact)
        await models.Account.update({primaryContact}, { where: { id: accountId } });

        await models.User.update({accountId}, { where: { id: user.id } });
        
    }     
}
     
    
  })
  

 