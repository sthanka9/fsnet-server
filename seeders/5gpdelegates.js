const models = require('../models/index');
const pmodels = require('../production_models'); 

pmodels.prod_sequelize.query("SELECT * FROM GpDelegates ", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async users => {
  
   
      for (let i = 0; i < users.length ; i++) {

        let element = users[i]         

        let  tempobj = {
          id:element.id,
          fundId: element.fundId,
          delegateId: element.delegateId,
          gpId: element.gpId,
          gPDelegateRequiredDocuSignBehalfGP:element.gPDelegateRequiredDocuSignBehalfGP,
          gPDelegateRequiredConsentHoldClosing:element.gPDelegateRequiredConsentHoldClosing,
          createdBy: element.createdBy,
          updatedBy: element.updatedBy,
          createdAt: element.createdAt,
          updatedAt: element.updatedAt,          
          deletedAt: element.deletedAt,
        }        
         
          
              let userinfo = await models.GpDelegates.findOne({attributes:['id'],where:{
                fundId:element.fundId,
                delegateId:element.delegateId,
                gpId:element.gpId
              }});

              if(!userinfo){
              console.log("insert ***********",element.id)    
              await models.GpDelegates.create(tempobj);
            
            } else {
              console.log("update ***********",element.id)  

              await models.GpDelegates.update(tempobj,{where:{id:element.id}});
            }

         
 
    }       
     
})


 