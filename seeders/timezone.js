
const models = require('../models/index'); 

models.timezone.bulkCreate([
    {
        "id": "1",
        "displayName": "GMT",
        "code": "GMT",
        "status": "1",
        "isDefault": "0"
        
    },
    {
        "id": "2",
        "displayName": "UTC",
        "code": "GMT",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "3",
        "displayName": "ECT",
        "code": "GMT+1:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "4",
        "displayName": "EET",
        "code": "GMT+2:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "5",
        "displayName": "ART",
        "code": "GMT+2:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "6",
        "displayName": "EAT",
        "code": "GMT+3:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "7",
        "displayName": "MET",
        "code": "GMT+3:30",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "8",
        "displayName": "NET",
        "code": "GMT+4:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "9",
        "displayName": "PLT",
        "code": "GMT+5:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "10",
        "displayName": "IST",
        "code": "GMT+5:30",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "11",
        "displayName": "BST",
        "code": "GMT+6:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "12",
        "displayName": "VST",
        "code": "GMT+7:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "13",
        "displayName": "CTT",
        "code": "GMT+8:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "14",
        "displayName": "JST",
        "code": "GMT+9:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "15",
        "displayName": "ACT",
        "code": "GMT+9:30",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "16",
        "displayName": "AET",
        "code": "GMT+10:00",
        "status": "1"
    },
    {
        "id": "17",
        "displayName": "SST",
        "code": "GMT+11:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "18",
        "displayName": "NST",
        "code": "GMT+12:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "19",
        "displayName": "MIT",
        "code": "GMT-11:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "20",
        "displayName": "HST",
        "code": "GMT-10:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "21",
        "displayName": "AST",
        "code": "GMT-9:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "22",
        "displayName": "PST",
        "code": "GMT-8:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "23",
        "displayName": "PNT",
        "code": "GMT-7:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "24",
        "displayName": "MST",
        "code": "GMT-7:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "25",
        "displayName": "CST",
        "code": "GMT-6:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "26",
        "displayName": "EST",
        "code": "GMT-5:00",
        "status": "1",
        "isDefault": "1"
    },
    {
        "id": "27",
        "displayName": "IET",
        "code": "GMT-5:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "28",
        "displayName": "PRT",
        "code": "GMT-4:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "29",
        "displayName": "CNT",
        "code": "GMT-3:30",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "30",
        "displayName": "AGT",
        "code": "GMT-3:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "31",
        "displayName": "BET",
        "code": "GMT-3:00",
        "status": "1",
        "isDefault": "0"
    },
    {
        "id": "32",
        "displayName": "CAT",
        "code": "GMT-1:00",
        "status": "1",
        "isDefault": "0"
    }
]);
