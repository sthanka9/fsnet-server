const models = require('../models/index');
const pmodels = require('../production_models'); 

pmodels.prod_sequelize.query("SELECT * FROM LpDelegates ", { type: pmodels.prod_sequelize.QueryTypes.SELECT})
  .then(async users => {
  
   
      for (let i = 0; i < users.length ; i++) {

        let element = users[i]         

        let  tempobj = {
          id:element.id,
          fundId: element.fundId,
          delegateId: element.delegateId,
          lpId: element.lpId,
          createdBy: element.createdBy,
          updatedBy: element.updatedBy,
          createdAt: element.createdAt,
          updatedAt: element.updatedAt,
          subscriptionId: element.subscriptionId,
          deletedAt: element.deletedAt ? element.deletedAt : null,
          invitedDate: element.invitedDate ? element.invitedDate : null,
          isInvestorInvited: element.isInvestorInvited ? element.isInvestorInvited : null,
        }        
         
          
        let info = await models.FundSubscription.findOne({attributes:['fundId'],where:{  
          id:element.subscriptionId 
        }})

      //  console.log("****info*******",info)

        if(info){

          let userinfo = await models.LpDelegate.findOne({attributes:['id'],where:{
            fundId:element.fundId,
            delegateId:element.delegateId,
            lpId:element.lpId
          }});          

              if(!userinfo){
              console.log("insert ***********",element.id)    
              await models.LpDelegate.create(tempobj);
            
            } else {
              console.log("update ***********",element.id)  

              await models.LpDelegate.update(tempobj,{where:{id:element.id}});
            }
          }
         
 
    }       
     
})


 