const models = require('../production_models');

const qa_models = require('../models/index');


models.prod_sequelize.query('select * from VCFirms',{ type: models.prod_sequelize.QueryTypes.SELECT})
.then(fundData=> {
    const firmArray=[];

    fundData.forEach(data=>{
        let fundObj={};
        fundObj.id = data.id,
        fundObj.firmName = data.firmName,
        fundObj.firstName = data.firstName,
        fundObj.lastName = data.lastName,
        fundObj.middleName = null,
        fundObj.cellNumber = data.cellNumber,
        fundObj.email = data.email,
        fundObj.gpId = data.gpId,
        fundObj.subscriptonType = data.subscriptonType,
        fundObj.isImporsonatingAllowed = data.isImporsonatingAllowed,
        fundObj.allowGPdelegatesToSign = data.allowGPdelegatesToSign,
        fundObj.VCFirmLogo = data.VCFirmLogo,
        fundObj.isPublished = data.isPublished,
        fundObj.isGPNotified = 0,
        fundObj.createdBy = data.createdBy,
        fundObj.updatedBy = data.updatedBy
        fundObj.createdAt = data.createdAt
        fundObj.updatedAt = data.updatedAt
   
        firmArray.push(fundObj)
    });

    qa_models.VCFirm.bulkCreate(firmArray)
    .then(res=>{
        console.log('===res',res)
    })
    .catch(e=>{
        console.log("==e",e)
    });

});