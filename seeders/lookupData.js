
const models = require('../models/index');

models.FundStatus.bulkCreate([{ name: 'Open', lpSideName: "Open" },
    { name:'Open-Ready-Draft', lpSideName: 'Subscribe to Fund'}, 
    { name:'Not Interested', lpSideName: 'Not Interested'}, 
    { name:'Rejected/Cutout', lpSideName: 'Rejected/Cutout'}, 
    { name:'Removed', lpSideName: 'Removed'}, 
    { name:'Rescind', lpSideName: 'Rescind'}, 
    { name:'Close-Ready', lpSideName : 'Subscription Docs Received'}, 
    { name:'Close-pending-document', lpSideName: 'Close-pending-document'}, 
    { name:'Close-Signed', lpSideName: 'Close-Signed'}, 
    { name:'Closed', lpSideName :'Subscribed'}, 
    { name: 'Declined', lpSideName: 'Declined'},
    { name:'New-Draft', lpSideName :'New-Draft'}, 
    { name:'Open-Ready', lpSideName :'Open-Ready'}, 
    { name:'Deactivated', lpSideName :'Deactivated'}, 
    { name: 'Admin-Draft', lpSideName: "Admin-Draft" },
    { name: 'Waiting For Signatory Signs', lpSideName: "Waiting For Signatory Signs" },
    { name: 'Invitation-Pending', lpSideName: "Investor - Pending Invitation" }
]);


models.InvestorSubType.bulkCreate([
    { name:'U.S. C Corporation', group: 'LLC', isUS: 1 },
    { name:'U.S. S Corporation', group: 'LLC', isUS: 1 },
    { name:'U.S. Limited Liability Company', group: 'LLC', isUS: 1 },
    { name:'U.S. Limited Partnership', group: 'LLC', isUS: 1 },
    { name:'U.S. General Partnership', group: 'LLC', isUS: 1 },
    { name:'Non-U.S. Corporation', group: 'LLC', isUS: 0 },
    { name:'Non-U.S. LLC or Similar Private Company', group: 'LLC', isUS: 0 },
    { name:'Non-U.S. Limited Partnership or Similar', group: 'LLC', isUS: 0 },
    { name:'Revocable Trust', group: 'trust'  },
    { name:'Irrevocable Trust', group: 'trust'}
]);


models.FundType.bulkCreate(
      [{
          'fundType': 'U.S. Fund',
          'createdAt': new Date(),
          'updatedAt': new Date()
        },
        {
          'fundType': 'Non-U.S. Fund',
          'createdAt': new Date(),
          'updatedAt': new Date()
        },
        {
          'fundType': 'Venture Capital',
          'createdAt': new Date(),
          'updatedAt': new Date()
        },
        {
          'fundType': 'Buyout',
          'createdAt': new Date(),
          'updatedAt': new Date()
        },
        {
          'fundType': 'Real Estate Credit',
          'createdAt': new Date(),
          'updatedAt': new Date()
        },
        {
          'fundType': 'Infrastructure',
          'createdAt': new Date(),
          'updatedAt': new Date()
        }
      ]);