const models = require('../models/index');
const pmodels = require('../production_models'); 

//admin user

models.User.findOne({attributes:['id'],where:{email:'vanillavc@cooley.com'}}).then(async users =>{
  if(users){
    console.log("already created")
  } else {
    console.log("need to create")

    const bcrypt = require('bcryptjs');

    const salt = bcrypt.genSaltSync(10);
    const passwordHash = bcrypt.hashSync('Fsnet@123', salt);
   
    const newUser = {
        firstName: 'Admin',
        lastName: 'Vanilla',
        middleName: '',
        accountType: 'FSNETAdministrator',
        email: 'vanillavc@cooley.com',
        password: passwordHash,
        emailConfirmCode: null,
        isEmailConfirmed: 1
    }
   
    await models.User.create(newUser).then(user => {
        models.UserRole.create({ roleId: 1, userId: user.id });
    })
         
  }
   
  
})


 