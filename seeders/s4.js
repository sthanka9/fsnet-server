const models = require('../models/index');

models.State.bulkCreate([{
    "id": "2584",
    "name": "Sint Maarten",
    "country_id": "154"
  },
  {
    "id": "2585",
    "name": "Amsterdam",
    "country_id": "155"
  },
  {
    "id": "2586",
    "name": "Benelux",
    "country_id": "155"
  },
  {
    "id": "2587",
    "name": "Drenthe",
    "country_id": "155"
  },
  {
    "id": "2588",
    "name": "Flevoland",
    "country_id": "155"
  },
  {
    "id": "2589",
    "name": "Friesland",
    "country_id": "155"
  },
  {
    "id": "2590",
    "name": "Gelderland",
    "country_id": "155"
  },
  {
    "id": "2591",
    "name": "Groningen",
    "country_id": "155"
  },
  {
    "id": "2592",
    "name": "Limburg",
    "country_id": "155"
  },
  {
    "id": "2593",
    "name": "Noord-Brabant",
    "country_id": "155"
  },
  {
    "id": "2594",
    "name": "Noord-Holland",
    "country_id": "155"
  },
  {
    "id": "2595",
    "name": "Overijssel",
    "country_id": "155"
  },
  {
    "id": "2596",
    "name": "South Holland",
    "country_id": "155"
  },
  {
    "id": "2597",
    "name": "Utrecht",
    "country_id": "155"
  },
  {
    "id": "2598",
    "name": "Zeeland",
    "country_id": "155"
  },
  {
    "id": "2599",
    "name": "Zuid-Holland",
    "country_id": "155"
  },
  {
    "id": "2600",
    "name": "Iles",
    "country_id": "156"
  },
  {
    "id": "2601",
    "name": "Nord",
    "country_id": "156"
  },
  {
    "id": "2602",
    "name": "Sud",
    "country_id": "156"
  },
  {
    "id": "2603",
    "name": "Area Outside Region",
    "country_id": "157"
  },
  {
    "id": "2604",
    "name": "Auckland",
    "country_id": "157"
  },
  {
    "id": "2605",
    "name": "Bay of Plenty",
    "country_id": "157"
  },
  {
    "id": "2606",
    "name": "Canterbury",
    "country_id": "157"
  },
  {
    "id": "2607",
    "name": "Christchurch",
    "country_id": "157"
  },
  {
    "id": "2608",
    "name": "Gisborne",
    "country_id": "157"
  },
  {
    "id": "2609",
    "name": "Hawke\'\'s Bay",
    "country_id": "157"
  },
  {
    "id": "2610",
    "name": "Manawatu-Wanganui",
    "country_id": "157"
  },
  {
    "id": "2611",
    "name": "Marlborough",
    "country_id": "157"
  },
  {
    "id": "2612",
    "name": "Nelson",
    "country_id": "157"
  },
  {
    "id": "2613",
    "name": "Northland",
    "country_id": "157"
  },
  {
    "id": "2614",
    "name": "Otago",
    "country_id": "157"
  },
  {
    "id": "2615",
    "name": "Rodney",
    "country_id": "157"
  },
  {
    "id": "2616",
    "name": "Southland",
    "country_id": "157"
  },
  {
    "id": "2617",
    "name": "Taranaki",
    "country_id": "157"
  },
  {
    "id": "2618",
    "name": "Tasman",
    "country_id": "157"
  },
  {
    "id": "2619",
    "name": "Waikato",
    "country_id": "157"
  },
  {
    "id": "2620",
    "name": "Wellington",
    "country_id": "157"
  },
  {
    "id": "2621",
    "name": "West Coast",
    "country_id": "157"
  },
  {
    "id": "2622",
    "name": "Atlantico Norte",
    "country_id": "158"
  },
  {
    "id": "2623",
    "name": "Atlantico Sur",
    "country_id": "158"
  },
  {
    "id": "2624",
    "name": "Boaco",
    "country_id": "158"
  },
  {
    "id": "2625",
    "name": "Carazo",
    "country_id": "158"
  },
  {
    "id": "2626",
    "name": "Chinandega",
    "country_id": "158"
  },
  {
    "id": "2627",
    "name": "Chontales",
    "country_id": "158"
  },
  {
    "id": "2628",
    "name": "Esteli",
    "country_id": "158"
  },
  {
    "id": "2629",
    "name": "Granada",
    "country_id": "158"
  },
  {
    "id": "2630",
    "name": "Jinotega",
    "country_id": "158"
  },
  {
    "id": "2631",
    "name": "Leon",
    "country_id": "158"
  },
  {
    "id": "2632",
    "name": "Madriz",
    "country_id": "158"
  },
  {
    "id": "2633",
    "name": "Managua",
    "country_id": "158"
  },
  {
    "id": "2634",
    "name": "Masaya",
    "country_id": "158"
  },
  {
    "id": "2635",
    "name": "Matagalpa",
    "country_id": "158"
  },
  {
    "id": "2636",
    "name": "Nueva Segovia",
    "country_id": "158"
  },
  {
    "id": "2637",
    "name": "Rio San Juan",
    "country_id": "158"
  },
  {
    "id": "2638",
    "name": "Rivas",
    "country_id": "158"
  },
  {
    "id": "2639",
    "name": "Agadez",
    "country_id": "159"
  },
  {
    "id": "2640",
    "name": "Diffa",
    "country_id": "159"
  },
  {
    "id": "2641",
    "name": "Dosso",
    "country_id": "159"
  },
  {
    "id": "2642",
    "name": "Maradi",
    "country_id": "159"
  },
  {
    "id": "2643",
    "name": "Niamey",
    "country_id": "159"
  },
  {
    "id": "2644",
    "name": "Tahoua",
    "country_id": "159"
  },
  {
    "id": "2645",
    "name": "Tillabery",
    "country_id": "159"
  },
  {
    "id": "2646",
    "name": "Zinder",
    "country_id": "159"
  },
  {
    "id": "2647",
    "name": "Abia",
    "country_id": "160"
  },
  {
    "id": "2648",
    "name": "Abuja Federal Capital Territor",
    "country_id": "160"
  },
  {
    "id": "2649",
    "name": "Adamawa",
    "country_id": "160"
  },
  {
    "id": "2650",
    "name": "Akwa Ibom",
    "country_id": "160"
  },
  {
    "id": "2651",
    "name": "Anambra",
    "country_id": "160"
  },
  {
    "id": "2652",
    "name": "Bauchi",
    "country_id": "160"
  },
  {
    "id": "2653",
    "name": "Bayelsa",
    "country_id": "160"
  },
  {
    "id": "2654",
    "name": "Benue",
    "country_id": "160"
  },
  {
    "id": "2655",
    "name": "Borno",
    "country_id": "160"
  },
  {
    "id": "2656",
    "name": "Cross River",
    "country_id": "160"
  },
  {
    "id": "2657",
    "name": "Delta",
    "country_id": "160"
  },
  {
    "id": "2658",
    "name": "Ebonyi",
    "country_id": "160"
  },
  {
    "id": "2659",
    "name": "Edo",
    "country_id": "160"
  },
  {
    "id": "2660",
    "name": "Ekiti",
    "country_id": "160"
  },
  {
    "id": "2661",
    "name": "Enugu",
    "country_id": "160"
  },
  {
    "id": "2662",
    "name": "Gombe",
    "country_id": "160"
  },
  {
    "id": "2663",
    "name": "Imo",
    "country_id": "160"
  },
  {
    "id": "2664",
    "name": "Jigawa",
    "country_id": "160"
  },
  {
    "id": "2665",
    "name": "Kaduna",
    "country_id": "160"
  },
  {
    "id": "2666",
    "name": "Kano",
    "country_id": "160"
  },
  {
    "id": "2667",
    "name": "Katsina",
    "country_id": "160"
  },
  {
    "id": "2668",
    "name": "Kebbi",
    "country_id": "160"
  },
  {
    "id": "2669",
    "name": "Kogi",
    "country_id": "160"
  },
  {
    "id": "2670",
    "name": "Kwara",
    "country_id": "160"
  },
  {
    "id": "2671",
    "name": "Lagos",
    "country_id": "160"
  },
  {
    "id": "2672",
    "name": "Nassarawa",
    "country_id": "160"
  },
  {
    "id": "2673",
    "name": "Niger",
    "country_id": "160"
  },
  {
    "id": "2674",
    "name": "Ogun",
    "country_id": "160"
  },
  {
    "id": "2675",
    "name": "Ondo",
    "country_id": "160"
  },
  {
    "id": "2676",
    "name": "Osun",
    "country_id": "160"
  },
  {
    "id": "2677",
    "name": "Oyo",
    "country_id": "160"
  },
  {
    "id": "2678",
    "name": "Plateau",
    "country_id": "160"
  },
  {
    "id": "2679",
    "name": "Rivers",
    "country_id": "160"
  },
  {
    "id": "2680",
    "name": "Sokoto",
    "country_id": "160"
  },
  {
    "id": "2681",
    "name": "Taraba",
    "country_id": "160"
  },
  {
    "id": "2682",
    "name": "Yobe",
    "country_id": "160"
  },
  {
    "id": "2683",
    "name": "Zamfara",
    "country_id": "160"
  },
  {
    "id": "2684",
    "name": "Niue",
    "country_id": "161"
  },
  {
    "id": "2685",
    "name": "Norfolk Island",
    "country_id": "162"
  },
  {
    "id": "2686",
    "name": "Northern Islands",
    "country_id": "163"
  },
  {
    "id": "2687",
    "name": "Rota",
    "country_id": "163"
  },
  {
    "id": "2688",
    "name": "Saipan",
    "country_id": "163"
  },
  {
    "id": "2689",
    "name": "Tinian",
    "country_id": "163"
  },
  {
    "id": "2690",
    "name": "Akershus",
    "country_id": "164"
  },
  {
    "id": "2691",
    "name": "Aust Agder",
    "country_id": "164"
  },
  {
    "id": "2692",
    "name": "Bergen",
    "country_id": "164"
  },
  {
    "id": "2693",
    "name": "Buskerud",
    "country_id": "164"
  },
  {
    "id": "2694",
    "name": "Finnmark",
    "country_id": "164"
  },
  {
    "id": "2695",
    "name": "Hedmark",
    "country_id": "164"
  },
  {
    "id": "2696",
    "name": "Hordaland",
    "country_id": "164"
  },
  {
    "id": "2697",
    "name": "Moere og Romsdal",
    "country_id": "164"
  },
  {
    "id": "2698",
    "name": "Nord Trondelag",
    "country_id": "164"
  },
  {
    "id": "2699",
    "name": "Nordland",
    "country_id": "164"
  },
  {
    "id": "2700",
    "name": "Oestfold",
    "country_id": "164"
  },
  {
    "id": "2701",
    "name": "Oppland",
    "country_id": "164"
  },
  {
    "id": "2702",
    "name": "Oslo",
    "country_id": "164"
  },
  {
    "id": "2703",
    "name": "Rogaland",
    "country_id": "164"
  },
  {
    "id": "2704",
    "name": "Soer Troendelag",
    "country_id": "164"
  },
  {
    "id": "2705",
    "name": "Sogn og Fjordane",
    "country_id": "164"
  },
  {
    "id": "2706",
    "name": "Stavern",
    "country_id": "164"
  },
  {
    "id": "2707",
    "name": "Sykkylven",
    "country_id": "164"
  },
  {
    "id": "2708",
    "name": "Telemark",
    "country_id": "164"
  },
  {
    "id": "2709",
    "name": "Troms",
    "country_id": "164"
  },
  {
    "id": "2710",
    "name": "Vest Agder",
    "country_id": "164"
  },
  {
    "id": "2711",
    "name": "Vestfold",
    "country_id": "164"
  },
  {
    "id": "2712",
    "name": "ÃƒÂ˜stfold",
    "country_id": "164"
  },
  {
    "id": "2713",
    "name": "Al Buraimi",
    "country_id": "165"
  },
  {
    "id": "2714",
    "name": "Dhufar",
    "country_id": "165"
  },
  {
    "id": "2715",
    "name": "Masqat",
    "country_id": "165"
  },
  {
    "id": "2716",
    "name": "Musandam",
    "country_id": "165"
  },
  {
    "id": "2717",
    "name": "Rusayl",
    "country_id": "165"
  },
  {
    "id": "2718",
    "name": "Wadi Kabir",
    "country_id": "165"
  },
  {
    "id": "2719",
    "name": "ad-Dakhiliyah",
    "country_id": "165"
  },
  {
    "id": "2720",
    "name": "adh-Dhahirah",
    "country_id": "165"
  },
  {
    "id": "2721",
    "name": "al-Batinah",
    "country_id": "165"
  },
  {
    "id": "2722",
    "name": "ash-Sharqiyah",
    "country_id": "165"
  },
  {
    "id": "2723",
    "name": "Baluchistan",
    "country_id": "166"
  },
  {
    "id": "2724",
    "name": "Federal Capital Area",
    "country_id": "166"
  },
  {
    "id": "2725",
    "name": "Federally administered Tribal",
    "country_id": "166"
  },
  {
    "id": "2726",
    "name": "North-West Frontier",
    "country_id": "166"
  },
  {
    "id": "2727",
    "name": "Northern Areas",
    "country_id": "166"
  },
  {
    "id": "2728",
    "name": "Punjab",
    "country_id": "166"
  },
  {
    "id": "2729",
    "name": "Sind",
    "country_id": "166"
  },
  {
    "id": "2730",
    "name": "Aimeliik",
    "country_id": "167"
  },
  {
    "id": "2731",
    "name": "Airai",
    "country_id": "167"
  },
  {
    "id": "2732",
    "name": "Angaur",
    "country_id": "167"
  },
  {
    "id": "2733",
    "name": "Hatobohei",
    "country_id": "167"
  },
  {
    "id": "2734",
    "name": "Kayangel",
    "country_id": "167"
  },
  {
    "id": "2735",
    "name": "Koror",
    "country_id": "167"
  },
  {
    "id": "2736",
    "name": "Melekeok",
    "country_id": "167"
  },
  {
    "id": "2737",
    "name": "Ngaraard",
    "country_id": "167"
  },
  {
    "id": "2738",
    "name": "Ngardmau",
    "country_id": "167"
  },
  {
    "id": "2739",
    "name": "Ngaremlengui",
    "country_id": "167"
  },
  {
    "id": "2740",
    "name": "Ngatpang",
    "country_id": "167"
  },
  {
    "id": "2741",
    "name": "Ngchesar",
    "country_id": "167"
  },
  {
    "id": "2742",
    "name": "Ngerchelong",
    "country_id": "167"
  },
  {
    "id": "2743",
    "name": "Ngiwal",
    "country_id": "167"
  },
  {
    "id": "2744",
    "name": "Peleliu",
    "country_id": "167"
  },
  {
    "id": "2745",
    "name": "Sonsorol",
    "country_id": "167"
  },
  {
    "id": "2746",
    "name": "Ariha",
    "country_id": "168"
  },
  {
    "id": "2747",
    "name": "Bayt Lahm",
    "country_id": "168"
  },
  {
    "id": "2748",
    "name": "Bethlehem",
    "country_id": "168"
  },
  {
    "id": "2749",
    "name": "Dayr-al-Balah",
    "country_id": "168"
  },
  {
    "id": "2750",
    "name": "Ghazzah",
    "country_id": "168"
  },
  {
    "id": "2751",
    "name": "Ghazzah ash-Shamaliyah",
    "country_id": "168"
  },
  {
    "id": "2752",
    "name": "Janin",
    "country_id": "168"
  },
  {
    "id": "2753",
    "name": "Khan Yunis",
    "country_id": "168"
  },
  {
    "id": "2754",
    "name": "Nabulus",
    "country_id": "168"
  },
  {
    "id": "2755",
    "name": "Qalqilyah",
    "country_id": "168"
  },
  {
    "id": "2756",
    "name": "Rafah",
    "country_id": "168"
  },
  {
    "id": "2757",
    "name": "Ram Allah wal-Birah",
    "country_id": "168"
  },
  {
    "id": "2758",
    "name": "Salfit",
    "country_id": "168"
  },
  {
    "id": "2759",
    "name": "Tubas",
    "country_id": "168"
  },
  {
    "id": "2760",
    "name": "Tulkarm",
    "country_id": "168"
  },
  {
    "id": "2761",
    "name": "al-Khalil",
    "country_id": "168"
  },
  {
    "id": "2762",
    "name": "al-Quds",
    "country_id": "168"
  },
  {
    "id": "2763",
    "name": "Bocas del Toro",
    "country_id": "169"
  },
  {
    "id": "2764",
    "name": "Chiriqui",
    "country_id": "169"
  },
  {
    "id": "2765",
    "name": "Cocle",
    "country_id": "169"
  },
  {
    "id": "2766",
    "name": "Colon",
    "country_id": "169"
  },
  {
    "id": "2767",
    "name": "Darien",
    "country_id": "169"
  },
  {
    "id": "2768",
    "name": "Embera",
    "country_id": "169"
  },
  {
    "id": "2769",
    "name": "Herrera",
    "country_id": "169"
  },
  {
    "id": "2770",
    "name": "Kuna Yala",
    "country_id": "169"
  },
  {
    "id": "2771",
    "name": "Los Santos",
    "country_id": "169"
  },
  {
    "id": "2772",
    "name": "Ngobe Bugle",
    "country_id": "169"
  },
  {
    "id": "2773",
    "name": "Panama",
    "country_id": "169"
  },
  {
    "id": "2774",
    "name": "Veraguas",
    "country_id": "169"
  },
  {
    "id": "2775",
    "name": "East New Britain",
    "country_id": "170"
  },
  {
    "id": "2776",
    "name": "East Sepik",
    "country_id": "170"
  },
  {
    "id": "2777",
    "name": "Eastern Highlands",
    "country_id": "170"
  },
  {
    "id": "2778",
    "name": "Enga",
    "country_id": "170"
  },
  {
    "id": "2779",
    "name": "Fly River",
    "country_id": "170"
  },
  {
    "id": "2780",
    "name": "Gulf",
    "country_id": "170"
  },
  {
    "id": "2781",
    "name": "Madang",
    "country_id": "170"
  },
  {
    "id": "2782",
    "name": "Manus",
    "country_id": "170"
  },
  {
    "id": "2783",
    "name": "Milne Bay",
    "country_id": "170"
  },
  {
    "id": "2784",
    "name": "Morobe",
    "country_id": "170"
  },
  {
    "id": "2785",
    "name": "National Capital District",
    "country_id": "170"
  },
  {
    "id": "2786",
    "name": "New Ireland",
    "country_id": "170"
  },
  {
    "id": "2787",
    "name": "North Solomons",
    "country_id": "170"
  },
  {
    "id": "2788",
    "name": "Oro",
    "country_id": "170"
  },
  {
    "id": "2789",
    "name": "Sandaun",
    "country_id": "170"
  },
  {
    "id": "2790",
    "name": "Simbu",
    "country_id": "170"
  },
  {
    "id": "2791",
    "name": "Southern Highlands",
    "country_id": "170"
  },
  {
    "id": "2792",
    "name": "West New Britain",
    "country_id": "170"
  },
  {
    "id": "2793",
    "name": "Western Highlands",
    "country_id": "170"
  },
  {
    "id": "2794",
    "name": "Alto Paraguay",
    "country_id": "171"
  },
  {
    "id": "2795",
    "name": "Alto Parana",
    "country_id": "171"
  },
  {
    "id": "2796",
    "name": "Amambay",
    "country_id": "171"
  },
  {
    "id": "2797",
    "name": "Asuncion",
    "country_id": "171"
  },
  {
    "id": "2798",
    "name": "Boqueron",
    "country_id": "171"
  },
  {
    "id": "2799",
    "name": "Caaguazu",
    "country_id": "171"
  },
  {
    "id": "2800",
    "name": "Caazapa",
    "country_id": "171"
  },
  {
    "id": "2801",
    "name": "Canendiyu",
    "country_id": "171"
  },
  {
    "id": "2802",
    "name": "Central",
    "country_id": "171"
  },
  {
    "id": "2803",
    "name": "Concepcion",
    "country_id": "171"
  },
  {
    "id": "2804",
    "name": "Cordillera",
    "country_id": "171"
  },
  {
    "id": "2805",
    "name": "Guaira",
    "country_id": "171"
  },
  {
    "id": "2806",
    "name": "Itapua",
    "country_id": "171"
  },
  {
    "id": "2807",
    "name": "Misiones",
    "country_id": "171"
  },
  {
    "id": "2808",
    "name": "Neembucu",
    "country_id": "171"
  },
  {
    "id": "2809",
    "name": "Paraguari",
    "country_id": "171"
  },
  {
    "id": "2810",
    "name": "Presidente Hayes",
    "country_id": "171"
  },
  {
    "id": "2811",
    "name": "San Pedro",
    "country_id": "171"
  },
  {
    "id": "2812",
    "name": "Amazonas",
    "country_id": "172"
  },
  {
    "id": "2813",
    "name": "Ancash",
    "country_id": "172"
  },
  {
    "id": "2814",
    "name": "Apurimac",
    "country_id": "172"
  },
  {
    "id": "2815",
    "name": "Arequipa",
    "country_id": "172"
  },
  {
    "id": "2816",
    "name": "Ayacucho",
    "country_id": "172"
  },
  {
    "id": "2817",
    "name": "Cajamarca",
    "country_id": "172"
  },
  {
    "id": "2818",
    "name": "Cusco",
    "country_id": "172"
  },
  {
    "id": "2819",
    "name": "Huancavelica",
    "country_id": "172"
  },
  {
    "id": "2820",
    "name": "Huanuco",
    "country_id": "172"
  },
  {
    "id": "2821",
    "name": "Ica",
    "country_id": "172"
  },
  {
    "id": "2822",
    "name": "Junin",
    "country_id": "172"
  },
  {
    "id": "2823",
    "name": "La Libertad",
    "country_id": "172"
  },
  {
    "id": "2824",
    "name": "Lambayeque",
    "country_id": "172"
  },
  {
    "id": "2825",
    "name": "Lima y Callao",
    "country_id": "172"
  },
  {
    "id": "2826",
    "name": "Loreto",
    "country_id": "172"
  },
  {
    "id": "2827",
    "name": "Madre de Dios",
    "country_id": "172"
  },
  {
    "id": "2828",
    "name": "Moquegua",
    "country_id": "172"
  },
  {
    "id": "2829",
    "name": "Pasco",
    "country_id": "172"
  },
  {
    "id": "2830",
    "name": "Piura",
    "country_id": "172"
  },
  {
    "id": "2831",
    "name": "Puno",
    "country_id": "172"
  },
  {
    "id": "2832",
    "name": "San Martin",
    "country_id": "172"
  },
  {
    "id": "2833",
    "name": "Tacna",
    "country_id": "172"
  },
  {
    "id": "2834",
    "name": "Tumbes",
    "country_id": "172"
  },
  {
    "id": "2835",
    "name": "Ucayali",
    "country_id": "172"
  },
  {
    "id": "2836",
    "name": "Batangas",
    "country_id": "173"
  },
  {
    "id": "2837",
    "name": "Bicol",
    "country_id": "173"
  },
  {
    "id": "2838",
    "name": "Bulacan",
    "country_id": "173"
  },
  {
    "id": "2839",
    "name": "Cagayan",
    "country_id": "173"
  },
  {
    "id": "2840",
    "name": "Caraga",
    "country_id": "173"
  },
  {
    "id": "2841",
    "name": "Central Luzon",
    "country_id": "173"
  },
  {
    "id": "2842",
    "name": "Central Mindanao",
    "country_id": "173"
  },
  {
    "id": "2843",
    "name": "Central Visayas",
    "country_id": "173"
  },
  {
    "id": "2844",
    "name": "Cordillera",
    "country_id": "173"
  },
  {
    "id": "2845",
    "name": "Davao",
    "country_id": "173"
  },
  {
    "id": "2846",
    "name": "Eastern Visayas",
    "country_id": "173"
  },
  {
    "id": "2847",
    "name": "Greater Metropolitan Area",
    "country_id": "173"
  },
  {
    "id": "2848",
    "name": "Ilocos",
    "country_id": "173"
  },
  {
    "id": "2849",
    "name": "Laguna",
    "country_id": "173"
  },
  {
    "id": "2850",
    "name": "Luzon",
    "country_id": "173"
  },
  {
    "id": "2851",
    "name": "Mactan",
    "country_id": "173"
  },
  {
    "id": "2852",
    "name": "Metropolitan Manila Area",
    "country_id": "173"
  },
  {
    "id": "2853",
    "name": "Muslim Mindanao",
    "country_id": "173"
  },
  {
    "id": "2854",
    "name": "Northern Mindanao",
    "country_id": "173"
  },
  {
    "id": "2855",
    "name": "Southern Mindanao",
    "country_id": "173"
  },
  {
    "id": "2856",
    "name": "Southern Tagalog",
    "country_id": "173"
  },
  {
    "id": "2857",
    "name": "Western Mindanao",
    "country_id": "173"
  },
  {
    "id": "2858",
    "name": "Western Visayas",
    "country_id": "173"
  },
  {
    "id": "2859",
    "name": "Pitcairn Island",
    "country_id": "174"
  },
  {
    "id": "2860",
    "name": "Biale Blota",
    "country_id": "175"
  },
  {
    "id": "2861",
    "name": "Dobroszyce",
    "country_id": "175"
  },
  {
    "id": "2862",
    "name": "Dolnoslaskie",
    "country_id": "175"
  },
  {
    "id": "2863",
    "name": "Dziekanow Lesny",
    "country_id": "175"
  },
  {
    "id": "2864",
    "name": "Hopowo",
    "country_id": "175"
  },
  {
    "id": "2865",
    "name": "Kartuzy",
    "country_id": "175"
  },
  {
    "id": "2866",
    "name": "Koscian",
    "country_id": "175"
  },
  {
    "id": "2867",
    "name": "Krakow",
    "country_id": "175"
  },
  {
    "id": "2868",
    "name": "Kujawsko-Pomorskie",
    "country_id": "175"
  },
  {
    "id": "2869",
    "name": "Lodzkie",
    "country_id": "175"
  },
  {
    "id": "2870",
    "name": "Lubelskie",
    "country_id": "175"
  },
  {
    "id": "2871",
    "name": "Lubuskie",
    "country_id": "175"
  },
  {
    "id": "2872",
    "name": "Malomice",
    "country_id": "175"
  },
  {
    "id": "2873",
    "name": "Malopolskie",
    "country_id": "175"
  },
  {
    "id": "2874",
    "name": "Mazowieckie",
    "country_id": "175"
  },
  {
    "id": "2875",
    "name": "Mirkow",
    "country_id": "175"
  },
  {
    "id": "2876",
    "name": "Opolskie",
    "country_id": "175"
  },
  {
    "id": "2877",
    "name": "Ostrowiec",
    "country_id": "175"
  },
  {
    "id": "2878",
    "name": "Podkarpackie",
    "country_id": "175"
  },
  {
    "id": "2879",
    "name": "Podlaskie",
    "country_id": "175"
  },
  {
    "id": "2880",
    "name": "Polska",
    "country_id": "175"
  },
  {
    "id": "2881",
    "name": "Pomorskie",
    "country_id": "175"
  },
  {
    "id": "2882",
    "name": "Poznan",
    "country_id": "175"
  },
  {
    "id": "2883",
    "name": "Pruszkow",
    "country_id": "175"
  },
  {
    "id": "2884",
    "name": "Rymanowska",
    "country_id": "175"
  },
  {
    "id": "2885",
    "name": "Rzeszow",
    "country_id": "175"
  },
  {
    "id": "2886",
    "name": "Slaskie",
    "country_id": "175"
  },
  {
    "id": "2887",
    "name": "Stare Pole",
    "country_id": "175"
  },
  {
    "id": "2888",
    "name": "Swietokrzyskie",
    "country_id": "175"
  },
  {
    "id": "2889",
    "name": "Warminsko-Mazurskie",
    "country_id": "175"
  },
  {
    "id": "2890",
    "name": "Warsaw",
    "country_id": "175"
  },
  {
    "id": "2891",
    "name": "Wejherowo",
    "country_id": "175"
  },
  {
    "id": "2892",
    "name": "Wielkopolskie",
    "country_id": "175"
  },
  {
    "id": "2893",
    "name": "Wroclaw",
    "country_id": "175"
  },
  {
    "id": "2894",
    "name": "Zachodnio-Pomorskie",
    "country_id": "175"
  },
  {
    "id": "2895",
    "name": "Zukowo",
    "country_id": "175"
  },
  {
    "id": "2896",
    "name": "Abrantes",
    "country_id": "176"
  },
  {
    "id": "2897",
    "name": "Acores",
    "country_id": "176"
  },
  {
    "id": "2898",
    "name": "Alentejo",
    "country_id": "176"
  },
  {
    "id": "2899",
    "name": "Algarve",
    "country_id": "176"
  },
  {
    "id": "2900",
    "name": "Braga",
    "country_id": "176"
  },
  {
    "id": "2901",
    "name": "Centro",
    "country_id": "176"
  },
  {
    "id": "2902",
    "name": "Distrito de Leiria",
    "country_id": "176"
  },
  {
    "id": "2903",
    "name": "Distrito de Viana do Castelo",
    "country_id": "176"
  },
  {
    "id": "2904",
    "name": "Distrito de Vila Real",
    "country_id": "176"
  },
  {
    "id": "2905",
    "name": "Distrito do Porto",
    "country_id": "176"
  },
  {
    "id": "2906",
    "name": "Lisboa e Vale do Tejo",
    "country_id": "176"
  },
  {
    "id": "2907",
    "name": "Madeira",
    "country_id": "176"
  },
  {
    "id": "2908",
    "name": "Norte",
    "country_id": "176"
  },
  {
    "id": "2909",
    "name": "Paivas",
    "country_id": "176"
  },
  {
    "id": "2910",
    "name": "Arecibo",
    "country_id": "177"
  },
  {
    "id": "2911",
    "name": "Bayamon",
    "country_id": "177"
  },
  {
    "id": "2912",
    "name": "Carolina",
    "country_id": "177"
  },
  {
    "id": "2913",
    "name": "Florida",
    "country_id": "177"
  },
  {
    "id": "2914",
    "name": "Guayama",
    "country_id": "177"
  },
  {
    "id": "2915",
    "name": "Humacao",
    "country_id": "177"
  },
  {
    "id": "2916",
    "name": "Mayaguez-Aguadilla",
    "country_id": "177"
  },
  {
    "id": "2917",
    "name": "Ponce",
    "country_id": "177"
  },
  {
    "id": "2918",
    "name": "Salinas",
    "country_id": "177"
  },
  {
    "id": "2919",
    "name": "San Juan",
    "country_id": "177"
  },
  {
    "id": "2920",
    "name": "Doha",
    "country_id": "178"
  },
  {
    "id": "2921",
    "name": "Jarian-al-Batnah",
    "country_id": "178"
  },
  {
    "id": "2922",
    "name": "Umm Salal",
    "country_id": "178"
  },
  {
    "id": "2923",
    "name": "ad-Dawhah",
    "country_id": "178"
  },
  {
    "id": "2924",
    "name": "al-Ghuwayriyah",
    "country_id": "178"
  },
  {
    "id": "2925",
    "name": "al-Jumayliyah",
    "country_id": "178"
  },
  {
    "id": "2926",
    "name": "al-Khawr",
    "country_id": "178"
  },
  {
    "id": "2927",
    "name": "al-Wakrah",
    "country_id": "178"
  },
  {
    "id": "2928",
    "name": "ar-Rayyan",
    "country_id": "178"
  },
  {
    "id": "2929",
    "name": "ash-Shamal",
    "country_id": "178"
  },
  {
    "id": "2930",
    "name": "Saint-Benoit",
    "country_id": "179"
  },
  {
    "id": "2931",
    "name": "Saint-Denis",
    "country_id": "179"
  },
  {
    "id": "2932",
    "name": "Saint-Paul",
    "country_id": "179"
  },
  {
    "id": "2933",
    "name": "Saint-Pierre",
    "country_id": "179"
  },
  {
    "id": "2934",
    "name": "Alba",
    "country_id": "180"
  },
  {
    "id": "2935",
    "name": "Arad",
    "country_id": "180"
  },
  {
    "id": "2936",
    "name": "Arges",
    "country_id": "180"
  },
  {
    "id": "2937",
    "name": "Bacau",
    "country_id": "180"
  },
  {
    "id": "2938",
    "name": "Bihor",
    "country_id": "180"
  },
  {
    "id": "2939",
    "name": "Bistrita-Nasaud",
    "country_id": "180"
  },
  {
    "id": "2940",
    "name": "Botosani",
    "country_id": "180"
  },
  {
    "id": "2941",
    "name": "Braila",
    "country_id": "180"
  },
  {
    "id": "2942",
    "name": "Brasov",
    "country_id": "180"
  },
  {
    "id": "2943",
    "name": "Bucuresti",
    "country_id": "180"
  },
  {
    "id": "2944",
    "name": "Buzau",
    "country_id": "180"
  },
  {
    "id": "2945",
    "name": "Calarasi",
    "country_id": "180"
  },
  {
    "id": "2946",
    "name": "Caras-Severin",
    "country_id": "180"
  },
  {
    "id": "2947",
    "name": "Cluj",
    "country_id": "180"
  },
  {
    "id": "2948",
    "name": "Constanta",
    "country_id": "180"
  },
  {
    "id": "2949",
    "name": "Covasna",
    "country_id": "180"
  },
  {
    "id": "2950",
    "name": "Dambovita",
    "country_id": "180"
  },
  {
    "id": "2951",
    "name": "Dolj",
    "country_id": "180"
  },
  {
    "id": "2952",
    "name": "Galati",
    "country_id": "180"
  },
  {
    "id": "2953",
    "name": "Giurgiu",
    "country_id": "180"
  },
  {
    "id": "2954",
    "name": "Gorj",
    "country_id": "180"
  },
  {
    "id": "2955",
    "name": "Harghita",
    "country_id": "180"
  },
  {
    "id": "2956",
    "name": "Hunedoara",
    "country_id": "180"
  },
  {
    "id": "2957",
    "name": "Ialomita",
    "country_id": "180"
  },
  {
    "id": "2958",
    "name": "Iasi",
    "country_id": "180"
  },
  {
    "id": "2959",
    "name": "Ilfov",
    "country_id": "180"
  },
  {
    "id": "2960",
    "name": "Maramures",
    "country_id": "180"
  },
  {
    "id": "2961",
    "name": "Mehedinti",
    "country_id": "180"
  },
  {
    "id": "2962",
    "name": "Mures",
    "country_id": "180"
  },
  {
    "id": "2963",
    "name": "Neamt",
    "country_id": "180"
  },
  {
    "id": "2964",
    "name": "Olt",
    "country_id": "180"
  },
  {
    "id": "2965",
    "name": "Prahova",
    "country_id": "180"
  },
  {
    "id": "2966",
    "name": "Salaj",
    "country_id": "180"
  },
  {
    "id": "2967",
    "name": "Satu Mare",
    "country_id": "180"
  },
  {
    "id": "2968",
    "name": "Sibiu",
    "country_id": "180"
  },
  {
    "id": "2969",
    "name": "Sondelor",
    "country_id": "180"
  },
  {
    "id": "2970",
    "name": "Suceava",
    "country_id": "180"
  },
  {
    "id": "2971",
    "name": "Teleorman",
    "country_id": "180"
  },
  {
    "id": "2972",
    "name": "Timis",
    "country_id": "180"
  },
  {
    "id": "2973",
    "name": "Tulcea",
    "country_id": "180"
  },
  {
    "id": "2974",
    "name": "Valcea",
    "country_id": "180"
  },
  {
    "id": "2975",
    "name": "Vaslui",
    "country_id": "180"
  },
  {
    "id": "2976",
    "name": "Vrancea",
    "country_id": "180"
  },
  {
    "id": "2977",
    "name": "Adygeja",
    "country_id": "181"
  },
  {
    "id": "2978",
    "name": "Aga",
    "country_id": "181"
  },
  {
    "id": "2979",
    "name": "Alanija",
    "country_id": "181"
  },
  {
    "id": "2980",
    "name": "Altaj",
    "country_id": "181"
  },
  {
    "id": "2981",
    "name": "Amur",
    "country_id": "181"
  },
  {
    "id": "2982",
    "name": "Arhangelsk",
    "country_id": "181"
  },
  {
    "id": "2983",
    "name": "Astrahan",
    "country_id": "181"
  },
  {
    "id": "2984",
    "name": "Bashkortostan",
    "country_id": "181"
  },
  {
    "id": "2985",
    "name": "Belgorod",
    "country_id": "181"
  },
  {
    "id": "2986",
    "name": "Brjansk",
    "country_id": "181"
  },
  {
    "id": "2987",
    "name": "Burjatija",
    "country_id": "181"
  },
  {
    "id": "2988",
    "name": "Chechenija",
    "country_id": "181"
  },
  {
    "id": "2989",
    "name": "Cheljabinsk",
    "country_id": "181"
  },
  {
    "id": "2990",
    "name": "Chita",
    "country_id": "181"
  },
  {
    "id": "2991",
    "name": "Chukotka",
    "country_id": "181"
  },
  {
    "id": "2992",
    "name": "Chuvashija",
    "country_id": "181"
  },
  {
    "id": "2993",
    "name": "Dagestan",
    "country_id": "181"
  },
  {
    "id": "2994",
    "name": "Evenkija",
    "country_id": "181"
  },
  {
    "id": "2995",
    "name": "Gorno-Altaj",
    "country_id": "181"
  },
  {
    "id": "2996",
    "name": "Habarovsk",
    "country_id": "181"
  },
  {
    "id": "2997",
    "name": "Hakasija",
    "country_id": "181"
  },
  {
    "id": "2998",
    "name": "Hanty-Mansija",
    "country_id": "181"
  },
  {
    "id": "2999",
    "name": "Ingusetija",
    "country_id": "181"
  },
  {
    "id": "3000",
    "name": "Irkutsk",
    "country_id": "181"
  },
  {
    "id": "3001",
    "name": "Ivanovo",
    "country_id": "181"
  },
  {
    "id": "3002",
    "name": "Jamalo-Nenets",
    "country_id": "181"
  },
  {
    "id": "3003",
    "name": "Jaroslavl",
    "country_id": "181"
  },
  {
    "id": "3004",
    "name": "Jevrej",
    "country_id": "181"
  },
  {
    "id": "3005",
    "name": "Kabardino-Balkarija",
    "country_id": "181"
  },
  {
    "id": "3006",
    "name": "Kaliningrad",
    "country_id": "181"
  },
  {
    "id": "3007",
    "name": "Kalmykija",
    "country_id": "181"
  },
  {
    "id": "3008",
    "name": "Kaluga",
    "country_id": "181"
  },
  {
    "id": "3009",
    "name": "Kamchatka",
    "country_id": "181"
  },
  {
    "id": "3010",
    "name": "Karachaj-Cherkessija",
    "country_id": "181"
  },
  {
    "id": "3011",
    "name": "Karelija",
    "country_id": "181"
  },
  {
    "id": "3012",
    "name": "Kemerovo",
    "country_id": "181"
  },
  {
    "id": "3013",
    "name": "Khabarovskiy Kray",
    "country_id": "181"
  },
  {
    "id": "3014",
    "name": "Kirov",
    "country_id": "181"
  },
  {
    "id": "3015",
    "name": "Komi",
    "country_id": "181"
  },
  {
    "id": "3016",
    "name": "Komi-Permjakija",
    "country_id": "181"
  },
  {
    "id": "3017",
    "name": "Korjakija",
    "country_id": "181"
  },
  {
    "id": "3018",
    "name": "Kostroma",
    "country_id": "181"
  },
  {
    "id": "3019",
    "name": "Krasnodar",
    "country_id": "181"
  },
  {
    "id": "3020",
    "name": "Krasnojarsk",
    "country_id": "181"
  },
  {
    "id": "3021",
    "name": "Krasnoyarskiy Kray",
    "country_id": "181"
  },
  {
    "id": "3022",
    "name": "Kurgan",
    "country_id": "181"
  },
  {
    "id": "3023",
    "name": "Kursk",
    "country_id": "181"
  },
  {
    "id": "3024",
    "name": "Leningrad",
    "country_id": "181"
  },
  {
    "id": "3025",
    "name": "Lipeck",
    "country_id": "181"
  },
  {
    "id": "3026",
    "name": "Magadan",
    "country_id": "181"
  },
  {
    "id": "3027",
    "name": "Marij El",
    "country_id": "181"
  },
  {
    "id": "3028",
    "name": "Mordovija",
    "country_id": "181"
  },
  {
    "id": "3029",
    "name": "Moscow",
    "country_id": "181"
  },
  {
    "id": "3030",
    "name": "Moskovskaja Oblast",
    "country_id": "181"
  },
  {
    "id": "3031",
    "name": "Moskovskaya Oblast",
    "country_id": "181"
  },
  {
    "id": "3032",
    "name": "Moskva",
    "country_id": "181"
  },
  {
    "id": "3033",
    "name": "Murmansk",
    "country_id": "181"
  },
  {
    "id": "3034",
    "name": "Nenets",
    "country_id": "181"
  },
  {
    "id": "3035",
    "name": "Nizhnij Novgorod",
    "country_id": "181"
  },
  {
    "id": "3036",
    "name": "Novgorod",
    "country_id": "181"
  },
  {
    "id": "3037",
    "name": "Novokusnezk",
    "country_id": "181"
  },
  {
    "id": "3038",
    "name": "Novosibirsk",
    "country_id": "181"
  },
  {
    "id": "3039",
    "name": "Omsk",
    "country_id": "181"
  },
  {
    "id": "3040",
    "name": "Orenburg",
    "country_id": "181"
  },
  {
    "id": "3041",
    "name": "Orjol",
    "country_id": "181"
  },
  {
    "id": "3042",
    "name": "Penza",
    "country_id": "181"
  },
  {
    "id": "3043",
    "name": "Perm",
    "country_id": "181"
  },
  {
    "id": "3044",
    "name": "Primorje",
    "country_id": "181"
  },
  {
    "id": "3045",
    "name": "Pskov",
    "country_id": "181"
  },
  {
    "id": "3046",
    "name": "Pskovskaya Oblast",
    "country_id": "181"
  },
  {
    "id": "3047",
    "name": "Rjazan",
    "country_id": "181"
  },
  {
    "id": "3048",
    "name": "Rostov",
    "country_id": "181"
  },
  {
    "id": "3049",
    "name": "Saha",
    "country_id": "181"
  },
  {
    "id": "3050",
    "name": "Sahalin",
    "country_id": "181"
  },
  {
    "id": "3051",
    "name": "Samara",
    "country_id": "181"
  },
  {
    "id": "3052",
    "name": "Samarskaya",
    "country_id": "181"
  },
  {
    "id": "3053",
    "name": "Sankt-Peterburg",
    "country_id": "181"
  },
  {
    "id": "3054",
    "name": "Saratov",
    "country_id": "181"
  },
  {
    "id": "3055",
    "name": "Smolensk",
    "country_id": "181"
  },
  {
    "id": "3056",
    "name": "Stavropol",
    "country_id": "181"
  },
  {
    "id": "3057",
    "name": "Sverdlovsk",
    "country_id": "181"
  },
  {
    "id": "3058",
    "name": "Tajmyrija",
    "country_id": "181"
  },
  {
    "id": "3059",
    "name": "Tambov",
    "country_id": "181"
  },
  {
    "id": "3060",
    "name": "Tatarstan",
    "country_id": "181"
  },
  {
    "id": "3061",
    "name": "Tjumen",
    "country_id": "181"
  },
  {
    "id": "3062",
    "name": "Tomsk",
    "country_id": "181"
  },
  {
    "id": "3063",
    "name": "Tula",
    "country_id": "181"
  },
  {
    "id": "3064",
    "name": "Tver",
    "country_id": "181"
  },
  {
    "id": "3065",
    "name": "Tyva",
    "country_id": "181"
  },
  {
    "id": "3066",
    "name": "Udmurtija",
    "country_id": "181"
  },
  {
    "id": "3067",
    "name": "Uljanovsk",
    "country_id": "181"
  },
  {
    "id": "3068",
    "name": "Ulyanovskaya Oblast",
    "country_id": "181"
  },
  {
    "id": "3069",
    "name": "Ust-Orda",
    "country_id": "181"
  },
  {
    "id": "3070",
    "name": "Vladimir",
    "country_id": "181"
  },
  {
    "id": "3071",
    "name": "Volgograd",
    "country_id": "181"
  },
  {
    "id": "3072",
    "name": "Vologda",
    "country_id": "181"
  },
  {
    "id": "3073",
    "name": "Voronezh",
    "country_id": "181"
  },
  {
    "id": "3074",
    "name": "Butare",
    "country_id": "182"
  },
  {
    "id": "3075",
    "name": "Byumba",
    "country_id": "182"
  },
  {
    "id": "3076",
    "name": "Cyangugu",
    "country_id": "182"
  },
  {
    "id": "3077",
    "name": "Gikongoro",
    "country_id": "182"
  },
  {
    "id": "3078",
    "name": "Gisenyi",
    "country_id": "182"
  },
  {
    "id": "3079",
    "name": "Gitarama",
    "country_id": "182"
  },
  {
    "id": "3080",
    "name": "Kibungo",
    "country_id": "182"
  },
  {
    "id": "3081",
    "name": "Kibuye",
    "country_id": "182"
  },
  {
    "id": "3082",
    "name": "Kigali-ngali",
    "country_id": "182"
  },
  {
    "id": "3083",
    "name": "Ruhengeri",
    "country_id": "182"
  },
  {
    "id": "3084",
    "name": "Ascension",
    "country_id": "183"
  },
  {
    "id": "3085",
    "name": "Gough Island",
    "country_id": "183"
  },
  {
    "id": "3086",
    "name": "Saint Helena",
    "country_id": "183"
  },
  {
    "id": "3087",
    "name": "Tristan da Cunha",
    "country_id": "183"
  },
  {
    "id": "3088",
    "name": "Christ Church Nichola Town",
    "country_id": "184"
  },
  {
    "id": "3089",
    "name": "Saint Anne Sandy Point",
    "country_id": "184"
  },
  {
    "id": "3090",
    "name": "Saint George Basseterre",
    "country_id": "184"
  },
  {
    "id": "3091",
    "name": "Saint George Gingerland",
    "country_id": "184"
  },
  {
    "id": "3092",
    "name": "Saint James Windward",
    "country_id": "184"
  },
  {
    "id": "3093",
    "name": "Saint John Capesterre",
    "country_id": "184"
  },
  {
    "id": "3094",
    "name": "Saint John Figtree",
    "country_id": "184"
  },
  {
    "id": "3095",
    "name": "Saint Mary Cayon",
    "country_id": "184"
  },
  {
    "id": "3096",
    "name": "Saint Paul Capesterre",
    "country_id": "184"
  },
  {
    "id": "3097",
    "name": "Saint Paul Charlestown",
    "country_id": "184"
  },
  {
    "id": "3098",
    "name": "Saint Peter Basseterre",
    "country_id": "184"
  },
  {
    "id": "3099",
    "name": "Saint Thomas Lowland",
    "country_id": "184"
  },
  {
    "id": "3100",
    "name": "Saint Thomas Middle Island",
    "country_id": "184"
  },
  {
    "id": "3101",
    "name": "Trinity Palmetto Point",
    "country_id": "184"
  },
  {
    "id": "3102",
    "name": "Anse-la-Raye",
    "country_id": "185"
  },
  {
    "id": "3103",
    "name": "Canaries",
    "country_id": "185"
  },
  {
    "id": "3104",
    "name": "Castries",
    "country_id": "185"
  },
  {
    "id": "3105",
    "name": "Choiseul",
    "country_id": "185"
  },
  {
    "id": "3106",
    "name": "Dennery",
    "country_id": "185"
  },
  {
    "id": "3107",
    "name": "Gros Inlet",
    "country_id": "185"
  },
  {
    "id": "3108",
    "name": "Laborie",
    "country_id": "185"
  },
  {
    "id": "3109",
    "name": "Micoud",
    "country_id": "185"
  },
  {
    "id": "3110",
    "name": "Soufriere",
    "country_id": "185"
  },
  {
    "id": "3111",
    "name": "Vieux Fort",
    "country_id": "185"
  },
  {
    "id": "3112",
    "name": "Miquelon-Langlade",
    "country_id": "186"
  },
  {
    "id": "3113",
    "name": "Saint-Pierre",
    "country_id": "186"
  },
  {
    "id": "3114",
    "name": "Charlotte",
    "country_id": "187"
  },
  {
    "id": "3115",
    "name": "Grenadines",
    "country_id": "187"
  },
  {
    "id": "3116",
    "name": "Saint Andrew",
    "country_id": "187"
  },
  {
    "id": "3117",
    "name": "Saint David",
    "country_id": "187"
  },
  {
    "id": "3118",
    "name": "Saint George",
    "country_id": "187"
  },
  {
    "id": "3119",
    "name": "Saint Patrick",
    "country_id": "187"
  },
  {
    "id": "3120",
    "name": "A\'\'ana",
    "country_id": "188"
  },
  {
    "id": "3121",
    "name": "Aiga-i-le-Tai",
    "country_id": "188"
  },
  {
    "id": "3122",
    "name": "Atua",
    "country_id": "188"
  },
  {
    "id": "3123",
    "name": "Fa\'\'asaleleaga",
    "country_id": "188"
  },
  {
    "id": "3124",
    "name": "Gaga\'\'emauga",
    "country_id": "188"
  },
  {
    "id": "3125",
    "name": "Gagaifomauga",
    "country_id": "188"
  },
  {
    "id": "3126",
    "name": "Palauli",
    "country_id": "188"
  },
  {
    "id": "3127",
    "name": "Satupa\'\'itea",
    "country_id": "188"
  },
  {
    "id": "3128",
    "name": "Tuamasaga",
    "country_id": "188"
  },
  {
    "id": "3129",
    "name": "Va\'\'a-o-Fonoti",
    "country_id": "188"
  },
  {
    "id": "3130",
    "name": "Vaisigano",
    "country_id": "188"
  },
  {
    "id": "3131",
    "name": "Acquaviva",
    "country_id": "189"
  },
  {
    "id": "3132",
    "name": "Borgo Maggiore",
    "country_id": "189"
  },
  {
    "id": "3133",
    "name": "Chiesanuova",
    "country_id": "189"
  },
  {
    "id": "3134",
    "name": "Domagnano",
    "country_id": "189"
  },
  {
    "id": "3135",
    "name": "Faetano",
    "country_id": "189"
  },
  {
    "id": "3136",
    "name": "Fiorentino",
    "country_id": "189"
  },
  {
    "id": "3137",
    "name": "Montegiardino",
    "country_id": "189"
  },
  {
    "id": "3138",
    "name": "San Marino",
    "country_id": "189"
  },
  {
    "id": "3139",
    "name": "Serravalle",
    "country_id": "189"
  },
  {
    "id": "3140",
    "name": "Agua Grande",
    "country_id": "190"
  },
  {
    "id": "3141",
    "name": "Cantagalo",
    "country_id": "190"
  },
  {
    "id": "3142",
    "name": "Lemba",
    "country_id": "190"
  },
  {
    "id": "3143",
    "name": "Lobata",
    "country_id": "190"
  },
  {
    "id": "3144",
    "name": "Me-Zochi",
    "country_id": "190"
  },
  {
    "id": "3145",
    "name": "Pague",
    "country_id": "190"
  },
  {
    "id": "3146",
    "name": "Al Khobar",
    "country_id": "191"
  },
  {
    "id": "3147",
    "name": "Aseer",
    "country_id": "191"
  },
  {
    "id": "3148",
    "name": "Ash Sharqiyah",
    "country_id": "191"
  },
  {
    "id": "3149",
    "name": "Asir",
    "country_id": "191"
  },
  {
    "id": "3150",
    "name": "Central Province",
    "country_id": "191"
  },
  {
    "id": "3151",
    "name": "Eastern Province",
    "country_id": "191"
  },
  {
    "id": "3152",
    "name": "Ha\'\'il",
    "country_id": "191"
  },
  {
    "id": "3153",
    "name": "Jawf",
    "country_id": "191"
  },
  {
    "id": "3154",
    "name": "Jizan",
    "country_id": "191"
  },
  {
    "id": "3155",
    "name": "Makkah",
    "country_id": "191"
  },
  {
    "id": "3156",
    "name": "Najran",
    "country_id": "191"
  },
  {
    "id": "3157",
    "name": "Qasim",
    "country_id": "191"
  },
  {
    "id": "3158",
    "name": "Tabuk",
    "country_id": "191"
  },
  {
    "id": "3159",
    "name": "Western Province",
    "country_id": "191"
  },

]);