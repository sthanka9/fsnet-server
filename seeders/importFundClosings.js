const models = require('../production_models');

const qa_models = require('../models/index');


models.prod_sequelize.query('select * from FundClosings',{ type: models.prod_sequelize.QueryTypes.SELECT})
.then(fundData=> {
    const fundClosingArray=[];

    fundData.forEach(data=>{
        let fundObj={};
        fundObj.id = data.id,
        fundObj.subscriptionId = data.subscriptionId,
        fundObj.fundId = data.fundId,
        fundObj.lpCapitalCommitment = data.lpCapitalCommitment,
        fundObj.gpConsumed = data.gpConsumed,
        fundObj.isActive = data.isActive,
        fundObj.createdBy = data.createdBy,
        fundObj.updatedBy = data.updatedBy,
        fundObj.createdAt = data.createdAt,
        fundObj.updatedAt = data.updatedAt,
        fundObj.deletedAt = data.deletedAt
   
        fundClosingArray.push(fundObj)
    });

    if(fundClosingArray.length>0){

        qa_models.FundClosings.bulkCreate(fundClosingArray)
        .then(res=>{
            console.log('===res',res)
        })
        .catch(e=>{
            console.log("==e",e)
        });
    }

});