
const models = require('../models/index');

models.Country.bulkCreate([
    {
      "id": "1",
      "sortname": "AF",
      "name": "Afghanistan",
      "isEEACountry": 0
    },
    {
      "id": "2",
      "sortname": "AL",
      "name": "Albania",
      "isEEACountry": 0
    },
    {
      "id": "3",
      "sortname": "DZ",
      "name": "Algeria",
      "isEEACountry": 0
    },
    {
      "id": "4",
      "sortname": "AS",
      "name": "American Samoa",
      "isEEACountry": 0
    },
    {
      "id": "5",
      "sortname": "AD",
      "name": "Andorra",
      "isEEACountry": 0
    },
    {
      "id": "6",
      "sortname": "AO",
      "name": "Angola",
      "isEEACountry": 0
    },
    {
      "id": "7",
      "sortname": "AI",
      "name": "Anguilla",
      "isEEACountry": 0
    },
    {
      "id": "8",
      "sortname": "AQ",
      "name": "Antarctica",
      "isEEACountry": 0
    },
    {
      "id": "9",
      "sortname": "AG",
      "name": "Antigua And Barbuda",
      "isEEACountry": 0
    },
    {
      "id": "10",
      "sortname": "AR",
      "name": "Argentina",
      "isEEACountry": 0
    },
    {
      "id": "11",
      "sortname": "AM",
      "name": "Armenia",
      "isEEACountry": 0
    },
    {
      "id": "12",
      "sortname": "AW",
      "name": "Aruba",
      "isEEACountry": 0
    },
    {
      "id": "13",
      "sortname": "AU",
      "name": "Australia",
      "isEEACountry": 0
    },
    {
      "id": "14",
      "sortname": "AT",
      "name": "Austria",
      "isEEACountry": 1
    },
    {
      "id": "15",
      "sortname": "AZ",
      "name": "Azerbaijan",
      "isEEACountry": 0
    },
    {
      "id": "16",
      "sortname": "BS",
      "name": "Bahamas The",
      "isEEACountry": 0
    },
    {
      "id": "17",
      "sortname": "BH",
      "name": "Bahrain",
      "isEEACountry": 0
    },
    {
      "id": "18",
      "sortname": "BD",
      "name": "Bangladesh",
      "isEEACountry": 0
    },
    {
      "id": "19",
      "sortname": "BB",
      "name": "Barbados",
      "isEEACountry": 0
    },
    {
      "id": "20",
      "sortname": "BY",
      "name": "Belarus",
      "isEEACountry": 0
    },
    {
      "id": "21",
      "sortname": "BE",
      "name": "Belgium",
      "isEEACountry": 1
    },
    {
      "id": "22",
      "sortname": "BZ",
      "name": "Belize",
      "isEEACountry": 0
    },
    {
      "id": "23",
      "sortname": "BJ",
      "name": "Benin",
      "isEEACountry": 0
    },
    {
      "id": "24",
      "sortname": "BM",
      "name": "Bermuda",
      "isEEACountry": 0
    },
    {
      "id": "25",
      "sortname": "BT",
      "name": "Bhutan",
      "isEEACountry": 0
    },
    {
      "id": "26",
      "sortname": "BO",
      "name": "Bolivia",
      "isEEACountry": 0
    },
    {
      "id": "27",
      "sortname": "BA",
      "name": "Bosnia and Herzegovina",
      "isEEACountry": 0
    },
    {
      "id": "28",
      "sortname": "BW",
      "name": "Botswana",
      "isEEACountry": 0
    },
    {
      "id": "29",
      "sortname": "BV",
      "name": "Bouvet Island",
      "isEEACountry": 0
    },
    {
      "id": "30",
      "sortname": "BR",
      "name": "Brazil",
      "isEEACountry": 0
    },
    {
      "id": "31",
      "sortname": "IO",
      "name": "British Indian Ocean Territory",
      "isEEACountry": 0
    },
    {
      "id": "32",
      "sortname": "BN",
      "name": "Brunei",
      "isEEACountry": 0
    },
    {
      "id": "33",
      "sortname": "BG",
      "name": "Bulgaria",
      "isEEACountry": 1
    },
    {
      "id": "34",
      "sortname": "BF",
      "name": "Burkina Faso",
      "isEEACountry": 0
    },
    {
      "id": "35",
      "sortname": "BI",
      "name": "Burundi",
      "isEEACountry": 0
    },
    {
      "id": "36",
      "sortname": "KH",
      "name": "Cambodia",
      "isEEACountry": 0
    },
    {
      "id": "37",
      "sortname": "CM",
      "name": "Cameroon",
      "isEEACountry": 0
    },
    {
      "id": "38",
      "sortname": "CA",
      "name": "Canada",
      "isEEACountry": 0
    },
    {
      "id": "39",
      "sortname": "CV",
      "name": "Cape Verde",
      "isEEACountry": 0
    },
    {
      "id": "40",
      "sortname": "KY",
      "name": "Cayman Islands",
      "isEEACountry": 0
    },
    {
      "id": "41",
      "sortname": "CF",
      "name": "Central African Republic",
      "isEEACountry": 0
    },
    {
      "id": "42",
      "sortname": "TD",
      "name": "Chad",
      "isEEACountry": 0
    },
    {
      "id": "43",
      "sortname": "CL",
      "name": "Chile",
      "isEEACountry": 0
    },
    {
      "id": "44",
      "sortname": "CN",
      "name": "China",
      "isEEACountry": 0
    },
    {
      "id": "45",
      "sortname": "CX",
      "name": "Christmas Island",
      "isEEACountry": 0
    },
    {
      "id": "46",
      "sortname": "CC",
      "name": "Cocos (Keeling) Islands",
      "isEEACountry": 0
    },
    {
      "id": "47",
      "sortname": "CO",
      "name": "Colombia",
      "isEEACountry": 0
    },
    {
      "id": "48",
      "sortname": "KM",
      "name": "Comoros",
      "isEEACountry": 0
    },
    {
      "id": "49",
      "sortname": "CG",
      "name": "Congo",
      "isEEACountry": 0
    },
    {
      "id": "50",
      "sortname": "CD",
      "name": "Congo The Democratic Republic Of The",
      "isEEACountry": 0
    },
    {
      "id": "51",
      "sortname": "CK",
      "name": "Cook Islands",
      "isEEACountry": 0
    },
    {
      "id": "52",
      "sortname": "CR",
      "name": "Costa Rica",
      "isEEACountry": 0
    },
    {
      "id": "53",
      "sortname": "CI",
      "name": "Cote D\'\'Ivoire (Ivory Coast)",
      "isEEACountry": 0
    },
    {
      "id": "54",
      "sortname": "HR",
      "name": "Croatia (Hrvatska)",
      "isEEACountry": 1
    },
    {
      "id": "55",
      "sortname": "CU",
      "name": "Cuba",
      "isEEACountry": 0
    },
    {
      "id": "56",
      "sortname": "CY",
      "name": "Cyprus",
      "isEEACountry": 1
    },
    {
      "id": "57",
      "sortname": "CZ",
      "name": "Czech Republic",
      "isEEACountry": 1
    },
    {
      "id": "58",
      "sortname": "DK",
      "name": "Denmark",
      "isEEACountry": 1
    },
    {
      "id": "59",
      "sortname": "DJ",
      "name": "Djibouti",
      "isEEACountry": 0
    },
    {
      "id": "60",
      "sortname": "DM",
      "name": "Dominica",
      "isEEACountry": 0
    },
    {
      "id": "61",
      "sortname": "DO",
      "name": "Dominican Republic",
      "isEEACountry": 0
    },
    {
      "id": "62",
      "sortname": "TP",
      "name": "East Timor",
      "isEEACountry": 0
    },
    {
      "id": "63",
      "sortname": "EC",
      "name": "Ecuador",
      "isEEACountry": 0
    },
    {
      "id": "64",
      "sortname": "EG",
      "name": "Egypt",
      "isEEACountry": 0
    },
    {
      "id": "65",
      "sortname": "SV",
      "name": "El Salvador",
      "isEEACountry": 0
    },
    {
      "id": "66",
      "sortname": "GQ",
      "name": "Equatorial Guinea",
      "isEEACountry": 0
    },
    {
      "id": "67",
      "sortname": "ER",
      "name": "Eritrea",
      "isEEACountry": 0
    },
    {
      "id": "68",
      "sortname": "EE",
      "name": "Estonia",
      "isEEACountry": 1
    },
    {
      "id": "69",
      "sortname": "ET",
      "name": "Ethiopia",
      "isEEACountry": 0
    },
    {
      "id": "70",
      "sortname": "XA",
      "name": "External Territories of Australia",
      "isEEACountry": 0
    },
    {
      "id": "71",
      "sortname": "FK",
      "name": "Falkland Islands",
      "isEEACountry": 0
    },
    {
      "id": "72",
      "sortname": "FO",
      "name": "Faroe Islands",
      "isEEACountry": 0
    },
    {
      "id": "73",
      "sortname": "FJ",
      "name": "Fiji Islands",
      "isEEACountry": 0
    },
    {
      "id": "74",
      "sortname": "FI",
      "name": "Finland",
      "isEEACountry": 1
    },
    {
      "id": "75",
      "sortname": "FR",
      "name": "France",
      "isEEACountry": 1
    },
    {
      "id": "76",
      "sortname": "GF",
      "name": "French Guiana",
      "isEEACountry": 0
    },
    {
      "id": "77",
      "sortname": "PF",
      "name": "French Polynesia",
      "isEEACountry": 0
    },
    {
      "id": "78",
      "sortname": "TF",
      "name": "French Southern Territories",
      "isEEACountry": 0
    },
    {
      "id": "79",
      "sortname": "GA",
      "name": "Gabon",
      "isEEACountry": 0
    },
    {
      "id": "80",
      "sortname": "GM",
      "name": "Gambia The",
      "isEEACountry": 0
    },
    {
      "id": "81",
      "sortname": "GE",
      "name": "Georgia",
      "isEEACountry": 0
    },
    {
      "id": "82",
      "sortname": "DE",
      "name": "Germany",
      "isEEACountry": 1
    },
    {
      "id": "83",
      "sortname": "GH",
      "name": "Ghana",
      "isEEACountry": 0
    },
    {
      "id": "84",
      "sortname": "GI",
      "name": "Gibraltar",
      "isEEACountry": 0
    },
    {
      "id": "85",
      "sortname": "GR",
      "name": "Greece",
      "isEEACountry": 1
    },
    {
      "id": "86",
      "sortname": "GL",
      "name": "Greenland",
      "isEEACountry": 0
    },
    {
      "id": "87",
      "sortname": "GD",
      "name": "Grenada",
      "isEEACountry": 0
    },
    {
      "id": "88",
      "sortname": "GP",
      "name": "Guadeloupe",
      "isEEACountry": 0
    },
    {
      "id": "89",
      "sortname": "GU",
      "name": "Guam",
      "isEEACountry": 0
    },
    {
      "id": "90",
      "sortname": "GT",
      "name": "Guatemala",
      "isEEACountry": 0
    },
    {
      "id": "91",
      "sortname": "XU",
      "name": "Guernsey and Alderney",
      "isEEACountry": 0
    },
    {
      "id": "92",
      "sortname": "GN",
      "name": "Guinea",
      "isEEACountry": 0
    },
    {
      "id": "93",
      "sortname": "GW",
      "name": "Guinea-Bissau",
      "isEEACountry": 0
    },
    {
      "id": "94",
      "sortname": "GY",
      "name": "Guyana",
      "isEEACountry": 0
    },
    {
      "id": "95",
      "sortname": "HT",
      "name": "Haiti",
      "isEEACountry": 0
    },
    {
      "id": "96",
      "sortname": "HM",
      "name": "Heard and McDonald Islands",
      "isEEACountry": 0
    },
    {
      "id": "97",
      "sortname": "HN",
      "name": "Honduras",
      "isEEACountry": 0
    },
    {
      "id": "98",
      "sortname": "HK",
      "name": "Hong Kong S.A.R.",
      "isEEACountry": 0
    },
    {
      "id": "99",
      "sortname": "HU",
      "name": "Hungary",
      "isEEACountry": 1
    },
    {
      "id": "100",
      "sortname": "IS",
      "name": "Iceland",
      "isEEACountry": 1
    },
    {
      "id": "101",
      "sortname": "IN",
      "name": "India",
      "isEEACountry": 0
    },
    {
      "id": "102",
      "sortname": "ID",
      "name": "Indonesia",
      "isEEACountry": 0
    },
    {
      "id": "103",
      "sortname": "IR",
      "name": "Iran",
      "isEEACountry": 0
    },
    {
      "id": "104",
      "sortname": "IQ",
      "name": "Iraq",
      "isEEACountry": 0
    },
    {
      "id": "105",
      "sortname": "IE",
      "name": "Ireland",
      "isEEACountry": 1
    },
    {
      "id": "106",
      "sortname": "IL",
      "name": "Israel",
      "isEEACountry": 0
    },
    {
      "id": "107",
      "sortname": "IT",
      "name": "Italy",
      "isEEACountry": 1
    },
    {
      "id": "108",
      "sortname": "JM",
      "name": "Jamaica",
      "isEEACountry": 0
    },
    {
      "id": "109",
      "sortname": "JP",
      "name": "Japan",
      "isEEACountry": 0
    },
    {
      "id": "110",
      "sortname": "XJ",
      "name": "Jersey",
      "isEEACountry": 0
    },
    {
      "id": "111",
      "sortname": "JO",
      "name": "Jordan",
      "isEEACountry": 0
    },
    {
      "id": "112",
      "sortname": "KZ",
      "name": "Kazakhstan",
      "isEEACountry": 0
    },
    {
      "id": "113",
      "sortname": "KE",
      "name": "Kenya",
      "isEEACountry": 0
    },
    {
      "id": "114",
      "sortname": "KI",
      "name": "Kiribati",
      "isEEACountry": 0
    },
    {
      "id": "115",
      "sortname": "KP",
      "name": "Korea North",
      "isEEACountry": 0
    },
    {
      "id": "116",
      "sortname": "KR",
      "name": "Korea South",
      "isEEACountry": 0
    },
    {
      "id": "117",
      "sortname": "KW",
      "name": "Kuwait",
      "isEEACountry": 0
    },
    {
      "id": "118",
      "sortname": "KG",
      "name": "Kyrgyzstan",
      "isEEACountry": 0
    },
    {
      "id": "119",
      "sortname": "LA",
      "name": "Laos",
      "isEEACountry": 0
    },
    {
      "id": "120",
      "sortname": "LV",
      "name": "Latvia",
      "isEEACountry": 1
    },
    {
      "id": "121",
      "sortname": "LB",
      "name": "Lebanon",
      "isEEACountry": 0
    },
    {
      "id": "122",
      "sortname": "LS",
      "name": "Lesotho",
      "isEEACountry": 0
    },
    {
      "id": "123",
      "sortname": "LR",
      "name": "Liberia",
      "isEEACountry": 0
    },
    {
      "id": "124",
      "sortname": "LY",
      "name": "Libya",
      "isEEACountry": 0
    },
    {
      "id": "125",
      "sortname": "LI",
      "name": "Liechtenstein",
      "isEEACountry": 1
    },
    {
      "id": "126",
      "sortname": "LT",
      "name": "Lithuania",
      "isEEACountry": 1
    },
    {
      "id": "127",
      "sortname": "LU",
      "name": "Luxembourg",
      "isEEACountry": 1
    },
    {
      "id": "128",
      "sortname": "MO",
      "name": "Macau S.A.R.",
      "isEEACountry": 0
    },
    {
      "id": "129",
      "sortname": "MK",
      "name": "Macedonia",
      "isEEACountry": 0
    },
    {
      "id": "130",
      "sortname": "MG",
      "name": "Madagascar",
      "isEEACountry": 0
    },
    {
      "id": "131",
      "sortname": "MW",
      "name": "Malawi",
      "isEEACountry": 0
    },
    {
      "id": "132",
      "sortname": "MY",
      "name": "Malaysia",
      "isEEACountry": 0
    },
    {
      "id": "133",
      "sortname": "MV",
      "name": "Maldives",
      "isEEACountry": 0
    },
    {
      "id": "134",
      "sortname": "ML",
      "name": "Mali",
      "isEEACountry": 0
    },
    {
      "id": "135",
      "sortname": "MT",
      "name": "Malta",
      "isEEACountry": 1,
      "isEEACountry": 0
    },
    {
      "id": "136",
      "sortname": "XM",
      "name": "Man (Isle of)",
      "isEEACountry": 0
    },
    {
      "id": "137",
      "sortname": "MH",
      "name": "Marshall Islands",
      "isEEACountry": 0
    },
    {
      "id": "138",
      "sortname": "MQ",
      "name": "Martinique",
      "isEEACountry": 0
    },
    {
      "id": "139",
      "sortname": "MR",
      "name": "Mauritania",
      "isEEACountry": 0
    },
    {
      "id": "140",
      "sortname": "MU",
      "name": "Mauritius",
      "isEEACountry": 0
    },
    {
      "id": "141",
      "sortname": "YT",
      "name": "Mayotte",
      "isEEACountry": 0
    },
    {
      "id": "142",
      "sortname": "MX",
      "name": "Mexico",
      "isEEACountry": 0
    },
    {
      "id": "143",
      "sortname": "FM",
      "name": "Micronesia",
      "isEEACountry": 0
    },
    {
      "id": "144",
      "sortname": "MD",
      "name": "Moldova",
      "isEEACountry": 0
    },
    {
      "id": "145",
      "sortname": "MC",
      "name": "Monaco",
      "isEEACountry": 0
    },
    {
      "id": "146",
      "sortname": "MN",
      "name": "Mongolia",
      "isEEACountry": 0
    },
    {
      "id": "147",
      "sortname": "MS",
      "name": "Montserrat",
      "isEEACountry": 0
    },
    {
      "id": "148",
      "sortname": "MA",
      "name": "Morocco",
      "isEEACountry": 0
    },
    {
      "id": "149",
      "sortname": "MZ",
      "name": "Mozambique",
      "isEEACountry": 0
    },
    {
      "id": "150",
      "sortname": "MM",
      "name": "Myanmar",
      "isEEACountry": 0
    },
    {
      "id": "151",
      "sortname": "NA",
      "name": "Namibia",
      "isEEACountry": 0
    },
    {
      "id": "152",
      "sortname": "NR",
      "name": "Nauru",
      "isEEACountry": 0
    },
    {
      "id": "153",
      "sortname": "NP",
      "name": "Nepal",
      "isEEACountry": 0
    },
    {
      "id": "154",
      "sortname": "AN",
      "name": "Netherlands Antilles",
      "isEEACountry": 0
    },
    {
      "id": "155",
      "sortname": "NL",
      "name": "Netherlands The",
      "isEEACountry": 1
    },
    {
      "id": "156",
      "sortname": "NC",
      "name": "New Caledonia",
      "isEEACountry": 0
    },
    {
      "id": "157",
      "sortname": "NZ",
      "name": "New Zealand",
      "isEEACountry": 0
    },
    {
      "id": "158",
      "sortname": "NI",
      "name": "Nicaragua",
      "isEEACountry": 0
    },
    {
      "id": "159",
      "sortname": "NE",
      "name": "Niger",
      "isEEACountry": 0
    },
    {
      "id": "160",
      "sortname": "NG",
      "name": "Nigeria",
      "isEEACountry": 0
    },
    {
      "id": "161",
      "sortname": "NU",
      "name": "Niue",
      "isEEACountry": 0
    },
    {
      "id": "162",
      "sortname": "NF",
      "name": "Norfolk Island",
      "isEEACountry": 0
    },
    {
      "id": "163",
      "sortname": "MP",
      "name": "Northern Mariana Islands",
      "isEEACountry": 0
    },
    {
      "id": "164",
      "sortname": "NO",
      "name": "Norway",
      "isEEACountry": 1
    },
    {
      "id": "165",
      "sortname": "OM",
      "name": "Oman",
      "isEEACountry": 0
    },
    {
      "id": "166",
      "sortname": "PK",
      "name": "Pakistan",
      "isEEACountry": 0
    },
    {
      "id": "167",
      "sortname": "PW",
      "name": "Palau",
      "isEEACountry": 0
    },
    {
      "id": "168",
      "sortname": "PS",
      "name": "Palestinian Territory Occupied",
      "isEEACountry": 0
    },
    {
      "id": "169",
      "sortname": "PA",
      "name": "Panama",
      "isEEACountry": 0
    },
    {
      "id": "170",
      "sortname": "PG",
      "name": "Papua new Guinea",
      "isEEACountry": 0
    },
    {
      "id": "171",
      "sortname": "PY",
      "name": "Paraguay",
      "isEEACountry": 0
    },
    {
      "id": "172",
      "sortname": "PE",
      "name": "Peru",
      "isEEACountry": 0
    },
    {
      "id": "173",
      "sortname": "PH",
      "name": "Philippines",
      "isEEACountry": 0
    },
    {
      "id": "174",
      "sortname": "PN",
      "name": "Pitcairn Island",
      "isEEACountry": 0
    },
    {
      "id": "175",
      "sortname": "PL",
      "name": "Poland",
      "isEEACountry": 1
    },
    {
      "id": "176",
      "sortname": "PT",
      "name": "Portugal",
      "isEEACountry": 1
    },
    {
      "id": "177",
      "sortname": "PR",
      "name": "Puerto Rico",
      "isEEACountry": 0
    },
    {
      "id": "178",
      "sortname": "QA",
      "name": "Qatar",
      "isEEACountry": 0
    },
    {
      "id": "179",
      "sortname": "RE",
      "name": "Reunion",
      "isEEACountry": 0
    },
    {
      "id": "180",
      "sortname": "RO",
      "name": "Romania",
      "isEEACountry": 1
    },
    {
      "id": "181",
      "sortname": "RU",
      "name": "Russia",
      "isEEACountry": 0
    },
    {
      "id": "182",
      "sortname": "RW",
      "name": "Rwanda",
      "isEEACountry": 0
    },
    {
      "id": "183",
      "sortname": "SH",
      "name": "Saint Helena",
      "isEEACountry": 0
    },
    {
      "id": "184",
      "sortname": "KN",
      "name": "Saint Kitts And Nevis",
      "isEEACountry": 0
    },
    {
      "id": "185",
      "sortname": "LC",
      "name": "Saint Lucia",
      "isEEACountry": 0
    },
    {
      "id": "186",
      "sortname": "PM",
      "name": "Saint Pierre and Miquelon",
      "isEEACountry": 0
    },
    {
      "id": "187",
      "sortname": "VC",
      "name": "Saint Vincent And The Grenadines",
      "isEEACountry": 0
    },
    {
      "id": "188",
      "sortname": "WS",
      "name": "Samoa",
      "isEEACountry": 0
    },
    {
      "id": "189",
      "sortname": "SM",
      "name": "San Marino",
      "isEEACountry": 0
    },
    {
      "id": "190",
      "sortname": "ST",
      "name": "Sao Tome and Principe",
      "isEEACountry": 0
    },
    {
      "id": "191",
      "sortname": "SA",
      "name": "Saudi Arabia",
      "isEEACountry": 0
    },
    {
      "id": "192",
      "sortname": "SN",
      "name": "Senegal",
      "isEEACountry": 0
    },
    {
      "id": "193",
      "sortname": "RS",
      "name": "Serbia",
      "isEEACountry": 0
    },
    {
      "id": "194",
      "sortname": "SC",
      "name": "Seychelles",
      "isEEACountry": 0
    },
    {
      "id": "195",
      "sortname": "SL",
      "name": "Sierra Leone",
      "isEEACountry": 0
    },
    {
      "id": "196",
      "sortname": "SG",
      "name": "Singapore",
      "isEEACountry": 0
    },
    {
      "id": "197",
      "sortname": "SK",
      "name": "Slovakia",
      "isEEACountry": 1
    },
    {
      "id": "198",
      "sortname": "SI",
      "name": "Slovenia",
      "isEEACountry": 1
    },
    {
      "id": "199",
      "sortname": "XG",
      "name": "Smaller Territories of the UK",
      "isEEACountry": 0
    },
    {
      "id": "200",
      "sortname": "SB",
      "name": "Solomon Islands",
      "isEEACountry": 0
    },
    {
      "id": "201",
      "sortname": "SO",
      "name": "Somalia",
      "isEEACountry": 0
    },
    {
      "id": "202",
      "sortname": "ZA",
      "name": "South Africa",
      "isEEACountry": 0
    },
    {
      "id": "203",
      "sortname": "GS",
      "name": "South Georgia",
      "isEEACountry": 0
    },
    {
      "id": "204",
      "sortname": "SS",
      "name": "South Sudan",
      "isEEACountry": 0
    },
    {
      "id": "205",
      "sortname": "ES",
      "name": "Spain",
      "isEEACountry": 1
    },
    {
      "id": "206",
      "sortname": "LK",
      "name": "Sri Lanka",
      "isEEACountry": 0
    },
    {
      "id": "207",
      "sortname": "SD",
      "name": "Sudan",
      "isEEACountry": 0
    },
    {
      "id": "208",
      "sortname": "SR",
      "name": "Suriname",
      "isEEACountry": 0
    },
    {
      "id": "209",
      "sortname": "SJ",
      "name": "Svalbard And Jan Mayen Islands",
      "isEEACountry": 0
    },
    {
      "id": "210",
      "sortname": "SZ",
      "name": "Swaziland",
      "isEEACountry": 0
    },
    {
      "id": "211",
      "sortname": "SE",
      "name": "Sweden",
      "isEEACountry": 1
    },
    {
      "id": "212",
      "sortname": "CH",
      "name": "Switzerland",
      "isEEACountry": 1
    },
    {
      "id": "213",
      "sortname": "SY",
      "name": "Syria",
      "isEEACountry": 0
    },
    {
      "id": "214",
      "sortname": "TW",
      "name": "Taiwan",
      "isEEACountry": 0
    },
    {
      "id": "215",
      "sortname": "TJ",
      "name": "Tajikistan",
      "isEEACountry": 0
    },
    {
      "id": "216",
      "sortname": "TZ",
      "name": "Tanzania",
      "isEEACountry": 0
    },
    {
      "id": "217",
      "sortname": "TH",
      "name": "Thailand",
      "isEEACountry": 0
    },
    {
      "id": "218",
      "sortname": "TG",
      "name": "Togo",
      "isEEACountry": 0
    },
    {
      "id": "219",
      "sortname": "TK",
      "name": "Tokelau",
      "isEEACountry": 0
    },
    {
      "id": "220",
      "sortname": "TO",
      "name": "Tonga",
      "isEEACountry": 0
    },
    {
      "id": "221",
      "sortname": "TT",
      "name": "Trinidad And Tobago",
      "isEEACountry": 0
    },
    {
      "id": "222",
      "sortname": "TN",
      "name": "Tunisia",
      "isEEACountry": 0
    },
    {
      "id": "223",
      "sortname": "TR",
      "name": "Turkey",
      "isEEACountry": 0
    },
    {
      "id": "224",
      "sortname": "TM",
      "name": "Turkmenistan",
      "isEEACountry": 0
    },
    {
      "id": "225",
      "sortname": "TC",
      "name": "Turks And Caicos Islands",
      "isEEACountry": 0
    },
    {
      "id": "226",
      "sortname": "TV",
      "name": "Tuvalu",
      "isEEACountry": 0
    },
    {
      "id": "227",
      "sortname": "UG",
      "name": "Uganda",
      "isEEACountry": 0
    },
    {
      "id": "228",
      "sortname": "UA",
      "name": "Ukraine",
      "isEEACountry": 0
    },
    {
      "id": "229",
      "sortname": "AE",
      "name": "United Arab Emirates",
      "isEEACountry": 0
    },
    {
      "id": "230",
      "sortname": "GB",
      "name": "United Kingdom",
      "isEEACountry": 1
    },
    {
      "id": "231",
      "sortname": "US",
      "name": "United States",
      "order": 1,
      "isEEACountry": 0
    },
    {
      "id": "233",
      "sortname": "UY",
      "name": "Uruguay",
      "isEEACountry": 0
    },
    {
      "id": "234",
      "sortname": "UZ",
      "name": "Uzbekistan",
      "isEEACountry": 0
    },
    {
      "id": "235",
      "sortname": "VU",
      "name": "Vanuatu",
      "isEEACountry": 0
    },
    {
      "id": "236",
      "sortname": "VA",
      "name": "Vatican City State (Holy See)",
      "isEEACountry": 0
    },
    {
      "id": "237",
      "sortname": "VE",
      "name": "Venezuela",
      "isEEACountry": 0
    },
    {
      "id": "238",
      "sortname": "VN",
      "name": "Vietnam",
      "isEEACountry": 0
    },
    {
      "id": "239",
      "sortname": "VG",
      "name": "Virgin Islands (British)",
      "isEEACountry": 0
    },
    {
      "id": "240",
      "sortname": "VI",
      "name": "Virgin Islands (US)",
      "isEEACountry": 0
    },
    {
      "id": "241",
      "sortname": "WF",
      "name": "Wallis And Futuna Islands",
      "isEEACountry": 0
    },
    {
      "id": "242",
      "sortname": "EH",
      "name": "Western Sahara",
      "isEEACountry": 0
    },
    {
      "id": "243",
      "sortname": "YE",
      "name": "Yemen",
      "isEEACountry": 0
    },
    {
      "id": "244",
      "sortname": "YU",
      "name": "Yugoslavia",
      "isEEACountry": 0
    },
    {
      "id": "245",
      "sortname": "ZM",
      "name": "Zambia",
      "isEEACountry": 0
    },
    {
      "id": "246",
      "sortname": "ZW",
      "name": "Zimbabwe",
      "isEEACountry": 0
    }
  ]);