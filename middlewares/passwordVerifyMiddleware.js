
const models =  require('../models');
const messageHelper = require('../helpers/messageHelper');

const verifySignaturePassword = async (req, res, next) => {

    try {
        const userId = req.userId;
        const { signaturePassword } = req.body;

        const user = await models.User.findOne({
            where: {
                id: userId
            }
        });

        // user not valid
        if (!user) {
            return errorHelper.error_400(res, 'user', messageHelper.userNotFound);
        }

        let accountId = user.accountId
        
        const account = await models.Account.findOne({
            where: {
                id: accountId
            }
        });
                
        // user not valid
        if (!account) {
            return errorHelper.error_400(res, 'user', messageHelper.userNotFound);
        }        

        // password is invalid. Check signature password with user password
        if (!bcrypt.compareSync(signaturePassword, account.password)) {

            return errorHelper.error_400(res, 'signaturePassword', messageHelper.old_password_not_match);

        } else {
            return res.json({
                error: false,
                message: messageHelper.signaturePassword_verified
            });
        }

    } catch (e) {
        next(e);
    }
}