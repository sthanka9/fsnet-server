const jwt = require('jsonwebtoken');
const config = require('../config');
const models = require('../models/index');
const redis = require('redis')
const messageHelper = require('../helpers/messageHelper');
const authHelper = require('../helpers/authHelper');
const moment = require('moment')

module.exports.verifyAuthToken = function (req, res, next) {

	// check header, url parameters, and post parameters for token
	var authToken = req.headers['x-auth-token'] || req.body.authToken || req.query.token; 

	// decode token
	if (!authToken) {
		return res.status(401).json({
			"errors": [
				{
					"msg": messageHelper.authTokenMissing
				}
			]
		});
	}

	// verifies secret and checks exp
	jwt.verify(authToken, config.get('jwt').privatekey, {
		algorithms: ['HS256']
	}, async function (err, decoded) {

		if (err) {
			return res.status(401).json({
				"errors": [
					{
						"msg": messageHelper.invalidAuthToken
					}
				]
			});
		}



		if (!decoded.user) {
			return res.status(401).json({
				"errors": [
					{
						"msg": messageHelper.invalidAuthToken
					}
				]
			});
		}

		let uuid = decoded.user.uuid  
	

		let client = redis.createClient(config.get('redis').port,config.get('redis').host)		
		 
		await client.get(uuid, async (err, result) => { 
			//console.log(uuid+"***************************",decoded.user.email)
			//console.log("****************uuid************",uuid)
			//console.log("****************err************",err)
			//console.log("****************result************",result)

			if (result==null) {
				return res.status(401).json({
					"errors": [
						{
							"msg": messageHelper.invalidAuthToken
						}
					]
				});
			}else { 			

				let currentTimeStamp = moment().format("DD/MM/YYYY HH:mm:ss")
				let duration = moment(currentTimeStamp, "DD/MM/YYYY HH:mm:ss").diff(moment(result, "DD/MM/YYYY HH:mm:ss"), 'minutes')

				//console.log("duration***********",duration)
				//console.log("config set time *************",config.get('session_time'))

				if (duration >= config.get('session_time')) {
					console.log("i am in if session time***********************")
					//remove token related data from cache
					await client.del(uuid, function (err, response) {
						if (response == 1) {
							console.log("Deleted Successfully!")
						} else {
							console.log("Cannot delete")
						}
					})
					return res.status(401).json({
						"errors": [
							{
								"msg": 'Session Expired'
							}
						]
					})

				} else {      
					//console.log("i am in else ***********************")
					client.setex(uuid, config.get('session_login_uuid'), currentTimeStamp)
					const user = await authHelper.validate(decoded.user);
		
					if (!user) {
						return res.status(401).json({
							"errors": [
								{
									"msg": messageHelper.invalidAuthToken
								}
							]
						});
					}
			
					const vcfirm = await models.VCFirm.findOne({
						where: {
							gpId: user.id
						}
					})
			
					req.user = user;
					req.userId = user.id;
					req.accountId = user.accountid
					req.vcfirmId = vcfirm ? vcfirm.id : null
			
					next();										 
				}
			}
		})	 





	});
}