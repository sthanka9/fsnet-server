const { check } = require('express-validator/check')
const db = require('../models/index')
const _ = require('lodash')
const messageHelper = require('../helpers/messageHelper');

const rules = [

    check('fundId')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.fundIdValidate),

        check('vcfirmId')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.vcfirmIdValidate),
        
    check('firstName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.firstNameValidate),

    check('lastName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.lastNameValidate),

    check('email')
        .trim()
        .not().isEmpty()
        .escape().isEmail().withMessage(messageHelper.emailValidate)

        .custom((value, { req }) => {
            //same user email can not add as secondary signatory
            if(req.user.email==value){
                return Promise.reject(messageHelper.secondaySignatoryValidation)
            } else {             
                return db.User.findOne({
                    attributes: ['id', 'email', 'accountType'],
                    where: { email: value, accountType: 'SecondaryGP' },
                    include: [{
                        attributes: ['fundId', 'signatoryId'],
                        model: db.GpSignatories, where: { fundId: req.body.fundId },
                        required: false
                    }, {
                        model: db.VcFirmSignatories,
                        as: 'vcFirmSignatories',
                        where: {
                            type: 'SecondaryGP',
                            vcfirmId: req.body.vcfirmId
                        },
                        required: false
                    }]
                }).then(user => {
                    if (user && user.GpSignatories) {
                        return Promise.reject(messageHelper.secondaryGpAccountAlreadyExists);
                    } else if(user &&  user.vcFirmSignatories) {
                        return Promise.reject(messageHelper.selectGpSignatoryFromTheListBelow)
                    }
                });
            }
        }),

];

module.exports = {
    rules
}