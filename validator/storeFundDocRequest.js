const { check } = require('express-validator/check')
const _ = require('lodash')


const rules = [
    check('fundId').not().isEmpty()
];

module.exports = {
    rules
}

