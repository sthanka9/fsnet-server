const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const forgotPasswordRules = [

    check('email').isEmail().withMessage(messageHelper.emailValidate),

];

const forgotPasswordGetUsernameRules = [

    check('email').optional()
        .isEmail().withMessage(messageHelper.emailValidate),
    check('cellNumber').escape().optional(),

];


module.exports = {
    forgotPasswordRules,
    forgotPasswordGetUsernameRules
}
