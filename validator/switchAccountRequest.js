const { check } = require('express-validator/check');
const rules = [ 
        check('userId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
];
module.exports = {
    rules
}