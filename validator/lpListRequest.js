const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const rules = [

    check('order').trim().optional().isIn(['asc','desc']).withMessage(messageHelper.orderValidate)
    .escape(),
    
    check('orderBy').trim().optional().isIn(['firstName','organizationName']).withMessage(messageHelper.lpListSortValidate)
    .escape(),

];

module.exports = {
    rules
}