const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const addOrUpdate = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdValidate),
    check('question')
        .not()
        .isEmpty()
        .trim(),
    check('questionTitle')
        .not()
        .isEmpty()
        .trim(),
    check('typeOfQuestion')
        .not()
        .isEmpty()
        .trim(),
    check('isRequired')
        .not()
        .isEmpty()
        .trim()
   
];

module.exports={
    addOrUpdate
}