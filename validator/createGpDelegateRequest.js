const { check } = require('express-validator/check')
const db = require('../models/index')
const _ = require('lodash')
const messageHelper = require('../helpers/messageHelper');

const rules = [

    check('fundId')
    .trim()
    .not().isEmpty()
    .escape().withMessage(messageHelper.fundIdValidate),


    check('firstName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.firstNameValidate),

    check('lastName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.lastNameValidate),

    check('email')
        .trim()
        .not().isEmpty()
        .escape().isEmail().withMessage(messageHelper.emailValidate)

        .custom((value , {req}) => {
           return db.User.findOne({
               attributes: ['id','email','accountType'],
                where: {   email: value, accountType: 'GPDelegate'},
                include: [{
                    attributes: ['fundId','delegateId'],
                    model: db.GpDelegates, where: { fundId: req.body.fundId},
                    required: true
                }]
            }).then(user => {
                if (user) {
                    return Promise.reject(messageHelper.gpAccountAlreadyExists)
                }
            });
        }),

];

module.exports = {
    rules
}