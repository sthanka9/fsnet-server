const { check } = require('express-validator/check');

const _ = require('lodash');
const db = require('../models/index');
const messageHelper = require('../helpers/messageHelper');

const isGpDelegateNew = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),
    check('email')
        .not()
        .isEmpty()
        .trim()
        .escape().withMessage(messageHelper.emailValidate)

];

const fundGpDelegateInvite = [
    check('fundId')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('firstName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.firstNameValidate),

    check('lastName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.lastNameValidate),

    check('email')
        .trim()
        .not().isEmpty()
        .escape().isEmail().withMessage(messageHelper.emailValidate)

        .custom((value, { req }) => {
            return db.User.findOne({
                attributes: ['id', 'email', 'accountType'],
                where: { email: value, accountType: 'GPDelegate' },
                include: [{
                    attributes: ['fundId', 'delegateId'],
                    model: db.GpDelegates, where: { fundId: req.body.fundId },
                    required: true
                }]
            }).then(user => {
                if (user) {
                    return Promise.reject(messageHelper.gpAccountAlreadyExists)
                }
            });
        })
]

const isLpDelegateNew = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),
    check('email')
        .not()
        .isEmpty()
        .trim()
        .escape().withMessage(messageHelper.emailValidate),
    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
]

const fundLpDelegateInvite = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),
    check('firstName')
        .not()
        .isEmpty()
        .trim(),
    check('middleName')
        .trim(),
    check('lastName')
        .not()
        .isEmpty()
        .trim(),
    check('cellNumber')
        .trim(),
    check('email')
        .not()
        .isEmpty()
        .trim()
        .escape().withMessage(messageHelper.emailValidate),
    check('organizationName')
        .trim(),
    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()

]

module.exports = {
    isGpDelegateNew,
    isLpDelegateNew,
    fundLpDelegateInvite,
    fundGpDelegateInvite
}