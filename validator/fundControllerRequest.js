const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const removeFundLpDelegate = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('delegateId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.delegateIdValidate)
];

const fundIdValidator =[
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate)
]

const getFoundList = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('vcfirmId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
];

const assignLpsRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('notAssignedLps')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('assignedLps')
        .not()
        .isEmpty()
        .trim()
        .escape()
];

const offlineSubscriptionUploadRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdReq),

    check('date')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('timeZone')
        .not()
        .isEmpty()
        .trim()
        .escape()
];

const offlineLpCapitalCommitmentRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdReq),

    check('amount')
        .not()
        .isEmpty()
        .trim()
        .escape()
];

const offlinefundAmendmentSignaturesRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdReq),

    check('documents')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('date')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('docType')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('timeZone')
        .not()
        .isEmpty()
        .trim()
        .escape()
];

const offlineSideletterUploadRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
];

const getOfflinePendingDocumentsRules = [
    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
];



module.exports = {
    removeFundLpDelegate,
    fundIdValidator,
    getFoundList,
    assignLpsRules,
    offlineSubscriptionUploadRules,
    offlineLpCapitalCommitmentRules,
    offlinefundAmendmentSignaturesRules,
    offlineSideletterUploadRules,
    getOfflinePendingDocumentsRules
}