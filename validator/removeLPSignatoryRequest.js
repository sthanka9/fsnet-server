const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const rules = [

    check('signatoryId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.signatoryIdValidate),

    check('').not().isEmpty(),

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdValidate),

];

module.exports = {
    rules
}
