const { check } = require('express-validator/check');
const db = require('../models/index')
const _ = require('lodash')
const messageHelper = require('../helpers/messageHelper');

const addLpRules = [

    check('fundId')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.fundIdValidate),
        
    check('vcfirmId')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.vcfirmIdValidate),

    check('firstName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.firstNameValidate),

    check('lastName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.lastNameValidate),


    check('email')
        .trim()
        .not().isEmpty()
        .isEmail().withMessage(messageHelper.emailValidate)
        .custom((value, { req }) => {
            return db.User.findOne({
                attributes: ['id', 'email', 'accountType'],
                where: { email: value, accountType: 'LP' },
                include: [{
                    attributes: ['fundId', 'lpId'],
                    as: 'subscription',
                    model: db.FundSubscription, where: { fundId: req.body.fundId },
                    required: false
                }, {
                    as: 'lps',
                    model: db.VcFrimLp,
                    where: { vcfirmId: req.body.vcfirmId },
                    required: false
                }]
            }).then(user => {
                if (user && user.subscription) {
                    return Promise.reject(messageHelper.lpAccountAlreadyExists)
                } else if (user && user.lps) {
                    return Promise.reject(messageHelper.selectLpFromTheListBelow)
                }
            });
        }),

    // check('middleName')
    //     .not()
    //     .isEmpty()
    //     .trim()
    //     .escape(),

    check('organizationName')
        .trim()
        .escape(),


];

const updateLpRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('lpId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),

    check('firstName')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('lastName')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('organizationName')
        .trim()
        .escape(),


    check('email')
        .not()
        .isEmpty()
        .trim()
        .escape(),


];


const updateLpDelegateRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('userId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),

    check('firstName')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('lastName')
        .not()
        .isEmpty()
        .trim()
        .escape(), 

    check('email')
        .not()
        .isEmpty()
        .trim()
        .escape(),

];

const deleteLpRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('lpId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),
];


const addOfflineLpRules = [
    check('fundId')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('firstName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.firstNameValidate),

    check('lastName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.lastNameValidate),

    check('middleName')
        .trim()
        .escape(),
    check('email')
        .trim()
        .not().isEmpty()
        .custom((value, { req }) => {
            let splittedArray = value.split('@');

            if (_.includes(splittedArray, 'temporary.com')) {
                return true
            }
            return Promise.reject(messageHelper.temporaryEmailValidate);
        }),

    check('organizationName')
        .trim()
        .escape()
]

const updateOfflineLpRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('lpId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),

    check('firstName')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('lastName')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('middleName')
        .trim()
        .escape(),

    check('organizationName')
        .trim()
        .escape(),

    check('email')
        .trim()
        .not().isEmpty()
];

const deleteOfflineLpRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('lpId')
        .not()
        .isEmpty()
        .trim()
        .escape(),
];


module.exports = {
    addLpRules,
    updateLpRules,
    updateLpDelegateRules,
    deleteLpRules,
    addOfflineLpRules,
    updateOfflineLpRules,
    deleteOfflineLpRules
}