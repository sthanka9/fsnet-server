const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const resendRules = [
  check('id')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

  check('fundId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

  check('accountType')
    .not()
    .isEmpty()
    .trim()
    .escape()
];

const copyRules = [
  check('id')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

  check('fundId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

  check('accountType')
    .not()
    .isEmpty()
    .trim()
    .escape()
];

const actionHistroyRules = [
  check('userId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

  check('fundId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

  check('accountType')
    .not()
    .isEmpty()
    .trim()
    .escape()
];

const editRules = [
  check('userId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

  check('fundId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

  check('accountType')
    .not()
    .isEmpty()
    .trim()
    .escape(),

  check('firstName')
    .not()
    .isEmpty()
    .trim()
    .escape(),

  check('lastName')
    .not()
    .isEmpty()
    .trim()
    .escape(),

  check('organizationName')
    .trim()
    .escape(),

  check('middleName')
    .trim()
    .escape(),

  check('accountType')
    .not()
    .isEmpty()
    .trim()
    .escape(),

  check('email')
    .trim()
    .not().isEmpty()
    .escape().isEmail().withMessage(messageHelper.emailValidate)
];


module.exports = {
  resendRules,
  copyRules,
  editRules,
  actionHistroyRules
}
