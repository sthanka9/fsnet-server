const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const getInvitationDataRules = [
    check('id')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]

const setPasswordRules = [
    check('id')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),

    check('code')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('password')
        .not()
        .isEmpty()
        .trim()
        .escape()
]

const changeGPToFundManagerRules = [
    check('gprequestid')
        .not()
        .isEmpty()
        .trim()
        // .isNumeric()
        .escape(),

    check('code')
        .not()
        .isEmpty()
        .trim()
        .escape()
]



const dismissNotificationRules = [
    check('notificationId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]

const userIdRules = [
    check('userId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]

const fundIdRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate)
]

const filenameRules = [
    check('filename')
        .not()
        .isEmpty()
        .trim()
        .escape()
]

const fundIdLpIdRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('lpId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]

const fundIdLpIdsRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('lpIds')
        .not()
        .isEmpty()
]

const subscriptionIdRules = [
    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdReq)
]

const idRules = [
    check('id')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]

const getAmendmentFundAgreementRules = [
    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdReq),

    check('documentId')
        .not()
        .isEmpty()
        .trim()
        .escape()
]

const documentIdRules = [
    check('documentId')
        .not()
        .isEmpty()
        .trim()
        .escape()
]

const getDFSAmmendmentPathRules = [
    check('documentId')
        .not()
        .isEmpty()
        .trim()
        //  .isNumeric()
        .escape()
]

const getAmmendmentPathRules = [
    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdReq),

    check('amendmentId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]

const amendmentIdRules = [
    check('amendmentId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]

const delegateIdRules = [
    check('delegateId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]

const sectionViewRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('section')
        .not()
        .isEmpty()
        .trim()
        .escape()
]

const fundIdSubscriptionIdRules = [
    /*
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate)
    */
]

const vcfirmIdRules = [
    check('vcfirmId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),
]

const changeFundGPRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('userId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),

    check('vcfirmIdOld')
        .not()
        .isEmpty()
        .trim()
        .escape()
]

const gprequestidRules = [
    check('gprequestid')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),
]

const vcFirmIdfundIdRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('vcfirmId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]

const fundIddelegateIdRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('userId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]

const emailFundidRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('email')
        .trim()
        .not().isEmpty()
        .escape().isEmail().withMessage(messageHelper.emailValidate)
]

const emailRules = [
    check('email')
        .trim()
        .not().isEmpty()
        .escape().isEmail().withMessage(messageHelper.emailValidate)
]

const messageRules = [
    check('toUserID')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),

    check('message')
        .not()
        .isEmpty()
]

const softDeleteRules = [
    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdReq),

    check('lpId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate)
]

const cloneSubscriptionRules = [
    check('selectedSubscriptionId')
        .not()
        .isEmpty()
        .trim()
        //.isNumeric()
        .escape(),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdReq),

    check('lpId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),
]

const countryIdRules = [
    check('countryId')
        .not()
        .isEmpty()
        .trim()
        // .isNumeric()
        .escape()
]

const groupRules = [
    check('group')
        .not()
        .isEmpty()
        .trim()
        // .isNumeric()
        .escape()
]

const editProfileRules = [
    check('userId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),

    check('email')
        .trim()
        .not().isEmpty()
        .escape().isEmail().withMessage(messageHelper.emailValidate),

    check('firstName')
        .not()
        .isEmpty()
        .trim()
        // .isNumeric()
        .escape(),

    check('lastName')
        .not()
        .isEmpty()
        .trim()
        // .isNumeric()
        .escape()
]

const changePasswordRules = [

    check('oldPassword')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('newPassword')
        .not()
        .isEmpty()
        .trim()
        .escape()
]

const fileIdsRules = [
    check('fileIds')
        .not()
        .isEmpty()
        .escape()
]

const fundIdSubscriptionIdSectionIdRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdReq),

    check('sectionId')
        .not()
        .isEmpty()
        .trim()
        // .isNumeric()
        .escape()

]

const restoreDeclinedInvestmentRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.subscriptionIdReq),

    check('lpId')
        .not()
        .isEmpty()
        .trim()
        // .isNumeric()
        .escape(),

    check('title')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('investorType')
        .not()
        .isEmpty()
        .trim()
        .escape()
]

const questionIdRules = [
    check('questionId')
        .not()
        .isEmpty()
        .isNumeric()
        .escape()
]

const fundIdAmendmentIdRules = [
    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('amendmentId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),
]

const signPendingAmmendmentAgreementRules = [
    check('subscriptionId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape().withMessage(messageHelper.subscriptionIdReq),

    check('documentId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),
    /*
    check('timeZone')
    .not()
    .isEmpty()
    .isNumeric()
    .escape()*/
]

const downloadRules =[
    check('subscriptionId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape().withMessage(messageHelper.subscriptionIdReq),

    check('fundId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape().withMessage(messageHelper.fundIdValidate),

    check('section')
    .not()
    .isEmpty()
    .escape()
]

module.exports = {
    getInvitationDataRules,
    setPasswordRules,
    changeGPToFundManagerRules,
    idRules,
    dismissNotificationRules,
    userIdRules,
    fundIdRules,
    filenameRules,
    fundIdLpIdRules,
    subscriptionIdRules,
    getAmendmentFundAgreementRules,
    documentIdRules,
    getDFSAmmendmentPathRules,
    getAmmendmentPathRules,
    amendmentIdRules,
    delegateIdRules,
    sectionViewRules,
    vcfirmIdRules,
    fundIdSubscriptionIdRules,
    changeFundGPRules,
    gprequestidRules,
    vcFirmIdfundIdRules,
    fundIddelegateIdRules,
    emailFundidRules,
    emailRules,
    messageRules,
    softDeleteRules,
    cloneSubscriptionRules,
    countryIdRules,
    groupRules,
    editProfileRules,
    changePasswordRules,
    fileIdsRules,
    fundIdSubscriptionIdSectionIdRules,
    restoreDeclinedInvestmentRules,
    questionIdRules,
    fundIdAmendmentIdRules,
    signPendingAmmendmentAgreementRules,
    downloadRules,
    fundIdLpIdsRules
}
