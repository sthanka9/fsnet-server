const { check } = require('express-validator/check')
const messageHelper = require('../helpers/messageHelper');

const rules = [
    check('fundId').optional().withMessage(messageHelper.fundIdValidate),
    check('vcfirmId').not().isEmpty().withMessage(messageHelper.vcFirmIdValidate),
    check('legalEntity').withMessage(messageHelper.legalEntityValidate),
    check('fundManagerLegalEntityName').withMessage(messageHelper.fundManagerLegalEntityNameValidate),
    check('fundCommonName').withMessage(messageHelper.fundCommonName),
    check('fundManagerTitle').withMessage(messageHelper.fundManagerTitle),
    check('fundManagerCommonName').withMessage(messageHelper.fundManagerCommonName),
    check('status').optional().withMessage(messageHelper.status),
    check('fundImageUrl').optional().withMessage(messageHelper.fundImageUrlValidate),
];

module.exports = {
    rules
}