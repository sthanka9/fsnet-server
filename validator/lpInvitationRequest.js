const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const rules = [

    check('subscriptionId').trim().not().isEmpty().escape().withMessage(messageHelper.subscriptionIdValidate),
    check('fundId').trim().not().isEmpty().escape().withMessage(messageHelper.fundIdValidate)
]

module.exports = {rules};