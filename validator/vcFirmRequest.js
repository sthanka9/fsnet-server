const { check, validationResult } = require('express-validator/check')
const { matchedData, sanitize } = require('express-validator/filter')
const db  = require('../models/index')
const messageHelper = require('../helpers/messageHelper');

const rules = [

    check('firmName')
        .trim()
        .not().isEmpty()
        .escape(),

    check('firstName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.firstNameValidate),

    check('lastName')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.lastNameValidate),

    check('email')
        .isEmail().withMessage(messageHelper.emailValidate)
        .trim()
        .not().isEmpty()
        .escape()
        .custom(value => {
            return db.Account.findOne({ where: { email: value} }).then(account => {
                 if (account) {
                    return Promise.reject(messageHelper.gpAccountAlreadyExists)
                }
            });
    }),
        
    
    check('subscriptonType')
        .trim()
        .not().isEmpty()
        .escape(),    

    check('VCFirmLogo')
        .trim()
        .escape(),
            
];

module.exports = {
    rules
}