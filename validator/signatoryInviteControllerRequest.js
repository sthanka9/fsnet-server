const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const assignGpSignatoryToFundRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),
];

const removeFundGPSignatoryRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('userId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.signatoryIdValidate),


];
const removeFundGpDelegateRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('userId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.signatoryIdValidate),


];

const signatureSettingsRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('noOfSignaturesRequiredForClosing')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),

    check('noOfSignaturesRequiredForCapitalCommitment')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),


];

const fundLpSignatoryInviteRules = [


    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('firstName')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('lastName')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('email')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),

];

const assignLpSignatoryToSubscriptionRules = [

    check('fundId')
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('subscriptionId')
        .trim()
        .isNumeric()
        .escape(),

    check('lpSignatories')
        .trim(),

];

const removeFundLpSignatoryRules = [

    check('fundId')
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('signatoryId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.signatoryIdValidate),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),
]

const isLpSignatoryNewRules = [

    check('email')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape(),
]
const isGpSignatoryNewRules = [

    check('email')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape()
]


module.exports = {
    assignGpSignatoryToFundRules,
    removeFundGPSignatoryRules,
    signatureSettingsRules,
    fundLpSignatoryInviteRules,
    assignLpSignatoryToSubscriptionRules,
    removeFundLpSignatoryRules,
    removeFundGpDelegateRules,
    isLpSignatoryNewRules,
    isGpSignatoryNewRules
}