const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const rules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),

    check('lps')
        .not()
        .isEmpty()
        .trim()
        .escape(),
];

module.exports = {
    rules
}