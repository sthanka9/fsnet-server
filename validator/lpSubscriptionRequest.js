const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const rules = [

    check('fundId').trim().escape(),

    check('step').trim().escape().not().isEmpty(),

    check('subscriptionId').trim().escape().not().isEmpty(),

    check('investorType').trim().escape().isIn(['Individual', 'LLC', 'Trust']).custom((value, { req }) => (req.body.step == 1 ? value : true)),

    check('areYouSubscribingAsJointIndividual').escape().custom((value, { req }) => (req.body.step == 1 && req.body.investorType == "Individual" ? String(value) : true)), //.isIn(['Yes','No'])

    check('typeOfLegalOwnership').trim().escape().custom((value, { req }) => {
        if (req.body.step == 1 && req.body.investorType == "Individual" && req.body.areYouSubscribingAsJointIndividual) {
            return String(value)
        } else {
          //  req.body.areYouSubscribingAsJointIndividual = null;
            return true
        }
    }

    ), // .isIn(['communityProperty','tenantsInCommon','jointTenants'])


    //step 3 for ind, llc
    check('areYouAccreditedInvestor').trim().escape().custom((value, { req }) => ((req.body.step == 3 && (req.body.investorType == "Trust" || req.body.investorType == "Individual" || req.body.investorType == "LLC")) ? String(value) : true)), //yes or no

    check('areYouQualifiedPurchaser').trim().escape().custom((value, { req }) => ((req.body.step == 4 && (req.body.investorType == "Trust" || req.body.investorType == "Individual" || req.body.investorType == "LLC")) ? String(value) : true)), // yes or no

    check('areYouQualifiedClient').trim().escape().custom((value, { req }) => {

        if ((req.body.step == 4 && (req.body.investorType == "Trust"  || req.body.investorType == "Individual" || req.body.investorType == "LLC"))) {

            if (req.body.areYouQualifiedPurchaser === false) {

                return Promise.resolve(true)
            } else {
                //req.body.areYouQualifiedClient = !req.body.areYouQualifiedPurchaser ? req.body.areYouQualifiedClient : null;
                return Promise.resolve(true)
            }
        } else {
            return Promise.resolve(true)
        }
    }),


    // for all step 2
    check('mailingAddressStreet').trim().custom((value, { req }) => (req.body.step == 2 ? value : true)),
    check('mailingAddressCity').trim().custom((value, { req }) => (req.body.step == 2 ? value : true)),
    check('mailingAddressCountry').trim().escape().custom((value, { req }) => (req.body.step == 2 ? value : true)),
    check('mailingAddressState').trim().escape().custom((value, { req }) => (req.body.step == 2 ? value : true)),
    check('mailingAddressZip').trim().escape().custom((value, { req }) => (req.body.step == 2 ? value : true)),
    check('mailingAddressPhoneNumber').trim().escape(),


    // llc step 1
    check('otherInvestorSubType').trim().escape()
        .custom((value, { req }) => (req.body.step == 1 && req.body.investorType == "LLC" && req.body.investorSubType == 'otherEntity') ? value : true)
        .custom((value, { req }) => {
            if (req.body.investorSubType != 'otherEntity') {
              //  req.body.otherInvestorSubType = null
                return true;
            }
            return true;
        }),

    // check('investorSubType').trim().escape().custom((value, { req }) => (req.body.step == 1 && req.body.investorType == "LLC") ? value : true).custom((value, { req }) => {
    //     if (req.body.investorSubType == 'otherEntity') {
    //         //req.body.investorSubType = null
    //         return true;
    //     }
    //     return true;
    // }),



    check('jurisdictionEntityLegallyRegistered').trim().escape().custom((value, { req }) => (req.body.step == 1 && req.body.investorType == "LLC") ? String(value) : true),

    check('whatSTATEIsTheEntityRegistered').trim().escape().custom((value, { req }) => {
        if(req.body.investorType == "LLC") {
            if (req.body.step == 1 && req.body.jurisdictionEntityLegallyRegistered == 231) {
                return String(value);
            } else {
              //  req.body.whatSTATEIsTheEntityRegistered = null;
                return true;
            }
        } else {
            return true;
        }

    }),





    // check('istheEntityFundOfFundsOrSimilarTypeVehicle').trim().escape().custom((value, { req }) => {

    //     // if (!value && req.body.investorType == "LLC") {
    //     //     req.body.isEntityTaxExemptForUSFederalIncomeTax = null;
    //     //     req.body.isEntityUS501c3 = null;
    //     // }

    //     if (req.body.step == 1 && req.body.investorType == "LLC") {
    //         return String(value);
    //     } else {
    //         return true;
    //     }
    // }
    // ),

    // check('isEntityTaxExemptForUSFederalIncomeTax').trim().escape().custom((value, { req }) => {
    //     // if (!value && req.body.investorType == "LLC" ) {
    //     //     req.body.isEntityUS501c3 = null;
    //     // }
    //     if (req.body.step == 1 && req.body.investorType == "LLC" && req.body.istheEntityFundOfFundsOrSimilarTypeVehicle) { return String(value); } else { return true; }
    // }),


    check('isEntityUS501c3').trim().escape().custom((value, { req }) => {
        if (req.body.step == 1 && req.body.investorType == "LLC" && req.body.isEntityTaxExemptForUSFederalIncomeTax) { return String(value); } else { return true; }
    }),

    check('releaseInvestmentEntityRequired').trim().escape().custom((value, { req }) => (req.body.step == 1 && req.body.investorType == "LLC") ? String(value) : true),


    check('indirectBeneficialOwnersSubjectFOIAStatutes').trim().escape().custom((value, { req }) => {
        if (req.body.step == 1 && req.body.investorType == "LLC" && req.body.releaseInvestmentEntityRequired) {
            return String(value)
        } else {
            return true
        }
    }



    ),

    // llc step5
    check('companiesAct').trim().escape().custom((value, { req }) => (req.body.step == 5 && (req.body.investorType == "LLC" || req.body.investorType == "Trust")) ? String(value) : true),

    check('numberOfDirectEquityOwners').trim().escape().custom((value, { req }) => (req.body.step == 5 &&  req.body.investorType == "Trust" && [2,3].includes(req.body.companiesAct)) ? String(value) : true),
    check('existingOrProspectiveInvestorsOfTheFund').trim().escape().custom((value, { req }) => (req.body.step == 5 && req.body.investorType == "Trust" && [2,3].includes(req.body.companiesAct)) ? String(value) : true),
    check('numberOfexistingOrProspectives').trim().custom((value, { req }) => (req.body.step == 5 && req.body.investorType == "Trust" && [2,3].includes(req.body.companiesAct)) ? String(value) : true),


    // step 6
    check('numberOfDirectEquityOwners').trim().escape().custom((value, { req }) => (req.body.step == 6 && req.body.investorType == "LLC") ? String(value) : true),
    check('existingOrProspectiveInvestorsOfTheFund').trim().escape().custom((value, { req }) => (req.body.step == 6 && req.body.investorType == "LLC") ? String(value) : true),
    check('numberOfexistingOrProspectives').trim().custom((value, { req }) => (req.body.step == 6 && req.body.investorType == "LLC") ? String(value) : true).custom((value, { req }) => {
        if (req.body.step == 6 && req.body.investorType == "LLC") {
            if (req.body.existingOrProspectiveInvestorsOfTheFund && !value) {
                return Promise.reject(messageHelper.numberOfexistingOrProspectivesReq)
            } else {
               // req.body.numberOfexistingOrProspectives = req.body.existingOrProspectiveInvestorsOfTheFund ? req.body.numberOfexistingOrProspectives : null;
                return Promise.resolve(true)
            }
        } else {
            return Promise.resolve(true)
        }
    }),

    // llc step 7

    check('entityProposingAcquiringInvestment').trim().escape().custom((value, { req }) => (req.body.step == 7 && req.body.investorType == "LLC") ? String(value) : true),
    check('anyOtherInvestorInTheFund').trim().escape().custom((value, { req }) => (req.body.step == 7 && req.body.investorType == "LLC") ? String(value) : true),
    check('entityHasMadeInvestmentsPriorToThedate').trim().escape().custom((value, { req }) => (req.body.step == 7 && req.body.investorType == "LLC") ? String(value) : true),
    check('partnershipWillNotConstituteMoreThanFortyPercent').trim().escape().custom((value, { req }) => (req.body.step == 7 && req.body.investorType == "LLC") ? String(value) : true),
    check('beneficialInvestmentMadeByTheEntity').trim().escape().custom((value, { req }) => (req.body.step == 7 && req.body.investorType == "LLC") ? String(value) : true),


    // llc step 8
    check('employeeBenefitPlan').trim().escape().custom((value, { req }) => (req.body.step == 8 && req.body.investorType == "LLC") ? String(value) : true),
    check('planAsDefinedInSection4975e1').trim().escape().custom((value, { req }) => (req.body.step == 8 && req.body.investorType == "LLC") ? String(value) : true),
    check('benefitPlanInvestor').trim().escape().custom((value, { req }) => (req.body.step == 8 && req.body.investorType == "LLC") ? String(value) : true),
    check('fiduciaryEntityIvestment').trim().escape().custom((value, { req }) => (req.body.step == 8 && req.body.investorType == "LLC") ? String(value) : true),
    check('entityDecisionToInvestInFund').trim().escape().custom((value, { req }) => (req.body.step == 8 && req.body.investorType == "LLC") ? String(value) : true),
    check('totalValueOfEquityInterests').trim().escape().custom((value, { req }) => (req.body.step == 8 && req.body.investorType == "LLC") ? String(value) : true),
    check('aggrement1').trim().escape().custom((value, { req }) => (req.body.step == 8 && req.body.investorType == "LLC") ? String(value) : true),
    check('aggrement2').trim().escape().custom((value, { req }) => (req.body.step == 8 && req.body.investorType == "LLC") ? String(value) : true),


    // trust 

    check('investorSubType').trim().escape().custom((value, { req }) => (req.body.step == 1 && req.body.investorType == "Trust") ? value : true),

    check('numberOfGrantorsOfTheTrust').trim().escape(),

    //check('trustLegallyDomiciled').trim().escape().custom((value, { req }) => (req.body.step == 1 && req.body.investorType == "Trust") ? value : true),
    // check('selectState').trim().escape().custom((value, { req }) => (req.body.step == 1 && req.body.investorType == "Trust" && req.body.trustLegallyDomiciled == 231) ? value : true),
    
    check('trustName').trim().escape().custom((value, { req }) => (req.body.step == 1 && req.body.investorType == "Trust") ? value : true),
   
    check('isEntityTaxExemptForUSFederalIncomeTax').trim().escape().custom((value, { req }) => {
        if (req.body.step == 1 && req.body.investorType == "Trust") { return String(value); } else { return true; }
    }),


    check('legalTitleDesignation').trim().escape().custom((value, { req }) => {
        if (req.body.step == 2 && req.body.investorType == "Trust" && req.body.investorSubType == 10) { return String(value); } else { return true; }
    }),


    check('isTrust501c3').trim().escape().custom((value, { req }) => {
        if (req.body.step == 1 && req.body.investorType == "Trust" && req.body.isEntityTaxExemptForUSFederalIncomeTax) { return String(value); } else { return true; }
    }),


    check('releaseInvestmentEntityRequired').trim().escape().custom((value, { req }) => (req.body.step == 1 && req.body.investorType == "Trust") ? String(value) : true),
];


module.exports = {
    rules
}


