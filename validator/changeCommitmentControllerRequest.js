const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const saveLpCommitmentsRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate),
    check('subscriptionId')
        .not()
        .isEmpty()
        .trim(),
    check('lpId')
        .not()
        .isEmpty()
        .trim(),
    check('amount')
        .not()
        .isEmpty()
        .trim(),
    check('comments')
        .not()
        .isEmpty()
        .trim()
];

const cancelCommitmentChange = [
 
    check('subscriptionId')
        .not()
        .isEmpty()
        .trim()
];


const gpSign = [

    check('documentId')
        .not()
        .isEmpty()
        .trim()
        .escape().withMessage(messageHelper.envelopeValidate),
    check('date')
        .not()
        .isEmpty()
        .trim()
        .withMessage(messageHelper.envelopeValidate),
    check('timeZone')
        .not()
        .isEmpty()
        .trim()
        .withMessage(messageHelper.envelopeValidate)
];

module.exports = {
    saveLpCommitmentsRules,
    cancelCommitmentChange,
    gpSign
}