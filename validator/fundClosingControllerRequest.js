const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');
const db = require('../models/index')
const _ = require('lodash')

const closeFundRules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .isNumeric()
        .escape().withMessage(messageHelper.fundIdValidate)
];

module.exports = {
    closeFundRules
}