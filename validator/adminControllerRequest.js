const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const listGpLpRules = [
  check('fundId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape()
]

const addGpLpRules = [
  check('fundId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

  check('firstName')
    .not()
    .isEmpty()
    .trim()
    .escape(),

  check('lastName')
    .not()
    .isEmpty()
    .trim()
    .escape(),

  check('email')
    .trim()
    .not().isEmpty()
    .escape().isEmail().withMessage(messageHelper.emailValidate)

];

const addLpDelegateSignatoryRules = [
  check('fundId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

    check('lpId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),
    
  check('firstName')
    .not()
    .isEmpty()
    .trim()
    .escape(),

  check('lastName')
    .not()
    .isEmpty()
    .trim()
    .escape(),

  check('email')
    .trim()
    .not().isEmpty()
    .escape().isEmail().withMessage(messageHelper.emailValidate)

];

const deleteUserRules = [
  check('id')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape()
];


module.exports = {
  listGpLpRules,
  addGpLpRules,
  addLpDelegateSignatoryRules,
  deleteUserRules
}
