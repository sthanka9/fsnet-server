const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');

const getRules = [
    check('subscriptionId').trim().not().isEmpty().escape().withMessage(messageHelper.subscriptionIdValidate),
]

module.exports = { getRules };