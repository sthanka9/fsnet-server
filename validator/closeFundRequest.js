const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper')

const rules = [

    check('fundId')
        .not()
        .isEmpty()
        .trim()
        .escape(),

    check('fundClosingData').not().isEmpty()

];

module.exports = {
    rules
}
