const { check } = require('express-validator/check')
const db  = require('../models/index')
const _ = require('lodash')
const messageHelper = require('../helpers/messageHelper');

const rules = [
 

    check('email').isEmail().withMessage(messageHelper.emailValidate),
    check('cellNumber').escape().optional(),
    check('code').not().isEmpty().trim().isLength(6).custom((value, {req}) => {
    const whereFilter = { email: req.body.email, emailConfirmCode : value};
        return db.Account.findOne({ where: whereFilter }).then(account => {
            if (!account) {
                return Promise.reject('invalid code.')
            }
        });
    }),

        // check('code').not().isEmpty().trim().isLength(6).custom((value, {req}) => {
        //     const whereFilter = _.pickBy({ email: req.body.email, cellNumber: req.body.cellNumber}, _.identity) // remove empty keys
        //     whereFilter.accountType = req.body.accountType;
        //     whereFilter.emailConfirmCode = value;
        //     return db.User.findOne({ where: whereFilter }).then(user => {
        //         if (!user) {
        //             return Promise.reject('invalid code')
        //         }
        //     });
        // }),

        check('password').not().isEmpty().matches(/^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[?\{\}\|\(\)\`~!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/, 'g').withMessage(messageHelper.password),

    check('passwordConfirmation', 'passwordConfirmation field must have the same value as the password field')
    .exists()
    .custom((value, { req }) => value === req.body.password)

];

module.exports = {
    rules
}

