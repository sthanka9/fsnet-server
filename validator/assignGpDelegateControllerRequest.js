const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');
const rules = [

    check('fundId')
        .trim()
        .not().isEmpty()
        .escape().withMessage(messageHelper.fundIdValidate),

    // check('gpDelegates')
    //     .trim()
    //     .not().isEmpty(),

];



module.exports = {
    rules
}