const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');
const rules = [
    check('email').isEmail().withMessage(messageHelper.emailValidate),
    check('password').not().isEmpty()
];

module.exports = {
    rules
}