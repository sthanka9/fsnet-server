const { check } = require('express-validator/check')
const db = require('../models/index')
const messageHelper = require('../helpers/messageHelper');

const rules = [


    check('email')
        .isEmail().withMessage(messageHelper.emailValidate),

    check('password').not().isEmpty().matches(/^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[?\{\}\|\(\)\`~!@#\$%\^&\*\[\]"\';:_\-<>\., =\+\/\\]).{8,}$/, 'g').withMessage(messageHelper.password),
 

    check('cellNumber') .not()
    .isEmpty()
    .trim()
    .escape(),

    check('emailConfirmCode').trim()
        .not().isEmpty().custom(value => {
            return db.Account.findOne({ where: { emailConfirmCode: value } }).then(account => {
                if (!account) {
                    return Promise.reject(messageHelper.inValidConfirmCode)
                } else if (account.isEmailConfirmed) {
                    return Promise.reject(messageHelper.confirmCodeAlreadyUsed)
                }
            });
        })
];

module.exports = {
    rules
}