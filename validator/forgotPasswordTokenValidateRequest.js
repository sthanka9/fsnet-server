const { check, validationResult } = require('express-validator/check');
const db  = require('../models/index')
const _ = require('lodash')
const messageHelper = require('../helpers/messageHelper');
const rules = [

    check('email').optional()
    .isEmail().withMessage(messageHelper.emailValidate),

    check('cellNumber').escape().optional(),

    check('code').not().isEmpty().trim().isLength(6).custom((value, {req}) => {
        const whereFilter = _.pickBy({ email: req.body.email, cellNumber: req.body.cellNumber}, _.identity) // remove empty keys
        whereFilter.emailConfirmCode = value;
        return db.Account.findOne({ where: whereFilter }).then(account => {
            if (!account) {
                return Promise.reject(messageHelper.confirmCodeExpired)
            }
        });
    })

];

module.exports = {
    rules
}
