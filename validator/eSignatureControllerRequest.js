const { check } = require('express-validator/check');
const messageHelper = require('../helpers/messageHelper');


const addeSignatureRules = [
  check('subscriptionId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape(),

  check('firstName')
    .not()
    .isEmpty()
    .trim()
    .escape(),

  check('lastName')
    .not()
    .isEmpty()
    .trim()
    .escape(),

  check('email')
    .trim()
    .not().isEmpty()
    .escape().isEmail().withMessage(messageHelper.emailValidate)

];

const   getDocsForSignatureRules = [
  check('tokenId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape()
];

const createSubscriptionRules = [
  check('subscriptionId')
    .not()
    .isEmpty()
    .trim()
    .isNumeric()
    .escape()
];


module.exports = {
  addeSignatureRules,
  getDocsForSignatureRules,
  createSubscriptionRules
  
}
