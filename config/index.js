const fs = require('fs');
const config = JSON.parse(fs.readFileSync('./config/config.json', 'utf8'));
const env = 'development'; // development, production,test

config.get = function (key) {
   return config[env][key];
}

module.exports = config;
module.exports.env = env;