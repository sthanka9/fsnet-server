'use strict';

module.exports=()=>{

  up: (queryInterface, Sequelize) => {
    
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addConstraint('AddendumLps', ['addendumId'], {
          type: 'foreign key',
          name: 'Fk_AddendumLps_Addendum_Id',
          references: {
            table: 'Addendums',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('AddendumLps', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_AddendumLps_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('AddendumLps', ['lpId'], {
          type: 'foreign key',
          name: 'FK_AddendumLps_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        //Change commitments
        queryInterface.addConstraint('ChangeCommitments', ['fundId'], {
          type: 'foreign key',
          name: 'FK_ChangeCommitments_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('ChangeCommitments', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_ChangeCommitments_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('ChangeCommitments', ['lpId'], {
          type: 'foreign key',
          name: 'FK_ChangeCommitments_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),

        //Addendums
        queryInterface.addConstraint('Addendums', ['fundId'], {
          type: 'foreign key',
          name: 'FK_Addendums_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),

        //DocumentsForSignatures
        queryInterface.addConstraint('DocumentsForSignatures', ['changeCommitmentId'], {
          type: 'foreign key',
          name: 'FK_DocumentsForSignatures_ChangeCommitments_Id',
          references: {
            table: 'ChangeCommitments',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('DocumentsForSignatures', ['fundId'], {
          type: 'foreign key',
          name: 'FK_DocumentsForSignatures_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('DocumentsForSignatures', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_DocumentsForSignatures_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),

        //Events
        queryInterface.addConstraint('Events', ['fundId'], {
          type: 'foreign key',
          name: 'FK_Events_Funds_id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('Events', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_Events_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('Events', ['userId'], {
          type: 'foreign key',
          name: 'FK_Events_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),

        //FundClosings
        queryInterface.addConstraint('FundClosings', ['fundId'], {
          type: 'foreign key',
          name: 'FK_FundClosings_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('FundClosings', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_FundClosings_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),

        //VcFrimLps
        queryInterface.addConstraint('VcFrimLps', ['lpId'], {
          type: 'foreign key',
          name: 'FK_VcFrimLps_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('VcFrimLps', ['vcfirmId'], {
          type: 'foreign key',
          name: 'FK_VcFrimLps_VCFirms_Id',
          references: {
            table: 'VCFirms',
            field: 'id'
          },
        }, { transaction: t }),

        //FundDocuments
        queryInterface.addConstraint('FundDocuments', ['fundId'], {
          type: 'foreign key',
          name: 'FK_FundDocuments_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),

        //FundLps
        queryInterface.addConstraint('FundLps', ['fundId'], {
          type: 'foreign key',
          name: 'FK_FundLps_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('FundLps', ['lpId'], {
          type: 'foreign key',
          name: 'FK_FundLps_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),

        //Funds
        queryInterface.addConstraint('Funds', ['statusId'], {
          type: 'foreign key',
          name: 'FK_Funds_FundStatuses_Id',
          references: {
            table: 'FundStatuses',
            field: 'id'
          },
        }),
        queryInterface.addConstraint('Funds', ['fundType'], {
          type: 'foreign key',
          name: 'FK_Funds_FundTypes_Id',
          references: {
            table: 'FundTypes',
            field: 'id'
          },
        }),
        queryInterface.addConstraint('Funds', ['vcfirmId'], {
          type: 'foreign key',
          name: 'FK_Funds_VCFirms_Id',
          references: {
            table: 'VCFirms',
            field: 'id'
          },
        }),

        //VcFrimDelegates
        queryInterface.addConstraint('VcFrimDelegates', ['delegateId'], {
          type: 'foreign key',
          name: 'FK_VcFrimDelegates_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('VcFrimDelegates', ['vcfirmId'], {
          type: 'foreign key',
          name: 'FK_VcFrimDelegates_VCFirms_Id',
          references: {
            table: 'VCFirms',
            field: 'id'
          },
        }, { transaction: t }),

        //GpDelegateApprovals
        queryInterface.addConstraint('GpDelegateApprovals', ['delegateId'], {
          type: 'foreign key',
          name: 'FK_GpDelegateApprovals_Deleg_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpDelegateApprovals', ['lpId'], {
          type: 'foreign key',
          name: 'FK_GpDelegateApprovals_Lp_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpDelegateApprovals', ['fundId'], {
          type: 'foreign key',
          name: 'FK_GpDelegateApprovals_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpDelegateApprovals', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_GpDelegateApprovals_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),

        //GpDelegates
        queryInterface.addConstraint('GpDelegates', ['delegateId'], {
          type: 'foreign key',
          name: 'FK_GpDelegates_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpDelegates', ['gpId'], {
          type: 'foreign key',
          name: 'FK_GpDelegates_Gp_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpDelegates', ['fundId'], {
          type: 'foreign key',
          name: 'FK_GpDelegates_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),

        //GpSignatories
        queryInterface.addConstraint('GpSignatories', ['gpId'], {
          type: 'foreign key',
          name: 'FK_GpSignatories_Gp_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpSignatories', ['signatoryId'], {
          type: 'foreign key',
          name: 'FK_GpSignatories_Sig_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpSignatories', ['fundId'], {
          type: 'foreign key',
          name: 'FK_GpSignatories_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),

        //GpSignatoryApprovals
        queryInterface.addConstraint('GpSignatoryApprovals', ['gpId'], {
          type: 'foreign key',
          name: 'FK_GpSignatoryApprovals_Gp_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpSignatoryApprovals', ['lpId'], {
          type: 'foreign key',
          name: 'FK_GpSignatoryApprovals_Lp_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpSignatoryApprovals', ['signatoryId'], {
          type: 'foreign key',
          name: 'FK_GpSignatoryApprovals_Sig_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpSignatoryApprovals', ['fundId'], {
          type: 'foreign key',
          name: 'FK_GpSignatoryApprovals_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('GpSignatoryApprovals', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_GpSignatoryApprovals_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),

        //InvestorAnswers
        queryInterface.addConstraint('InvestorAnswers', ['questionId'], {
          type: 'foreign key',
          name: 'FK_InvestorAnswers_InvestorQuestions_Id',
          references: {
            table: 'InvestorQuestions',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('InvestorAnswers', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_InvestorAnswers_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),

        //InvestorQuestions
        queryInterface.addConstraint('InvestorQuestions', ['fundId'], {
          type: 'foreign key',
          name: 'FK_InvestorQuestions_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('InvestorQuestions', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_InvestorQuestions_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),

        //LpDelegates
        queryInterface.addConstraint('LpDelegates', ['delegateId'], {
          type: 'foreign key',
          name: 'FK_LpDelegates_Deleg_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('LpDelegates', ['fundId'], {
          type: 'foreign key',
          name: 'FK_LpDelegates_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),

        //LpRequiredFundDocuments
        queryInterface.addConstraint('LpRequiredFundDocuments', ['lpId'], {
          type: 'foreign key',
          name: 'FK_LpRequiredFundDocuments_Lp_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('LpRequiredFundDocuments', ['fundId'], {
          type: 'foreign key',
          name: 'FK_LpRequiredFundDocuments_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),

        //LpSignatories
        queryInterface.addConstraint('LpSignatories', ['lpId'], {
          type: 'foreign key',
          name: 'FK_LpSignatories_Lp_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('LpSignatories', ['signatoryId'], {
          type: 'foreign key',
          name: 'FK_LpSignatories_Signat_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('LpSignatories', ['fundId'], {
          type: 'foreign key',
          name: 'FK_LpSignatories_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('LpSignatories', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_LpSignatories_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),

        //partnershipAgreementDocHistories
        queryInterface.addConstraint('partnershipAgreementDocHistories', ['fundId'], {
          type: 'foreign key',
          name: 'FK_partnershipAgreementDocHistories_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),

        //RolePermissions
        queryInterface.addConstraint('RolePermissions', ['permissionId'], {
          type: 'foreign key',
          name: 'FK_RolePermissions_Permissions_Id',
          references: {
            table: 'Permissions',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('RolePermissions', ['roleId'], {
          type: 'foreign key',
          name: 'FK_RolePermissions_Roles_Id',
          references: {
            table: 'Roles',
            field: 'id'
          },
        }, { transaction: t }),

        //SideletterLps
        queryInterface.addConstraint('SideletterLps', ['sideletterId'], {
          type: 'foreign key',
          name: 'FK_SideletterLps_Sideletters_Id',
          references: {
            table: 'Sideletters',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('SideletterLps', ['lpId'], {
          type: 'foreign key',
          name: 'FK_SideletterLps_Lp_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),

        //Sideletters
        queryInterface.addConstraint('Sideletters', ['fundId'], {
          type: 'foreign key',
          name: 'FK_Sideletters_Funds_Id',
          references: {
            table: 'Funds',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('Sideletters', ['lpId'], {
          type: 'foreign key',
          name: 'FK_Sideletters_Lp_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('Sideletters', ['subscriptionId'], {
          type: 'foreign key',
          name: 'FK_Sideletters_FundSubscriptions_Id',
          references: {
            table: 'FundSubscriptions',
            field: 'id'
          },
        }, { transaction: t }),

        //SignatureTracks
        queryInterface.addConstraint('SignatureTracks', ['signedUserId'], {
          type: 'foreign key',
          name: 'FK_SignatureTracks_Sign_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),

        //UserRoles
        queryInterface.addConstraint('UserRoles', ['roleId'], {
          type: 'foreign key',
          name: 'FK_UserRoles_Roles_Id',
          references: {
            table: 'Roles',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('UserRoles', ['userId'], {
          type: 'foreign key',
          name: 'FK_UserRoles_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t }),

        //VcFirmSignatories
        queryInterface.addConstraint('VcFirmSignatories', ['vcfirmId'], {
          type: 'foreign key',
          name: 'FK_VcFirmSignatories_VCFirms_Id',
          references: {
            table: 'VCFirms',
            field: 'id'
          },
        }, { transaction: t }),
        queryInterface.addConstraint('VcFirmSignatories', ['signatoryId'], {
          type: 'foreign key',
          name: 'FK_VcFirmSignatories_Signat_Users_Id',
          references: {
            table: 'Users',
            field: 'id'
          },
        }, { transaction: t })
      ])
    })
  },

  down: (queryInterface, Sequelize) => {
    
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeConstraint('AddendumLps', 'Fk_AddendumLps_Addendum_Id', { transaction: t }),
        queryInterface.removeConstraint('AddendumLps', 'FK_AddendumLps_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('AddendumLps', 'FK_AddendumLps_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('Addendums', 'FK_Addendums_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('ChangeCommitments', 'FK_ChangeCommitments_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('ChangeCommitments', 'FK_ChangeCommitments_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('ChangeCommitments', 'FK_ChangeCommitments_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('DocumentsForSignatures', 'FK_DocumentsForSignatures_ChangeCommitments_Id', { transaction: t }),
        queryInterface.removeConstraint('DocumentsForSignatures', 'FK_DocumentsForSignatures_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('DocumentsForSignatures', 'FK_DocumentsForSignatures_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('Events', 'FK_Events_Funds_id', { transaction: t }),
        queryInterface.removeConstraint('Events', 'FK_Events_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('Events', 'FK_Events_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('FundClosings', 'FK_FundClosings_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('FundClosings', 'FK_FundClosings_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('VcFrimLps', 'FK_VcFrimLps_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('VcFrimLps', 'FK_VcFrimLps_VCFirms_Id', { transaction: t }),
        queryInterface.removeConstraint('FundDocuments', 'FK_FundDocuments_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('FundLps', 'FK_FundLps_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('FundLps', 'FK_FundLps_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('Funds', 'FK_Funds_FundStatuses_Id'),
        queryInterface.removeConstraint('Funds', 'FK_Funds_FundTypes_Id'),
        queryInterface.removeConstraint('Funds', 'FK_Funds_VCFirms_Id'),
        queryInterface.removeConstraint('VcFrimDelegates', 'FK_VcFrimDelegates_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('VcFrimDelegates', 'FK_VcFrimDelegates_VCFirms_Id', { transaction: t }),
        queryInterface.removeConstraint('GpDelegateApprovals', 'FK_GpDelegateApprovals_Deleg_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('GpDelegateApprovals', 'FK_GpDelegateApprovals_Lp_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('GpDelegateApprovals', 'FK_GpDelegateApprovals_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('GpDelegateApprovals', 'FK_GpDelegateApprovals_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('GpDelegates', 'FK_GpDelegates_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('GpDelegates', 'FK_GpDelegates_Gp_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('GpDelegates', 'FK_GpDelegates_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('GpSignatories', 'FK_GpSignatories_Gp_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('GpSignatories', 'FK_GpSignatories_Sig_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('GpSignatories', 'FK_GpSignatories_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('GpSignatoryApprovals', 'FK_GpSignatoryApprovals_Gp_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('GpSignatoryApprovals', 'FK_GpSignatoryApprovals_Lp_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('GpSignatoryApprovals', 'FK_GpSignatoryApprovals_Sig_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('GpSignatoryApprovals', 'FK_GpSignatoryApprovals_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('GpSignatoryApprovals', 'FK_GpSignatoryApprovals_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('InvestorAnswers', 'FK_InvestorAnswers_InvestorQuestions_Id', { transaction: t }),
        queryInterface.removeConstraint('InvestorAnswers', 'FK_InvestorAnswers_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('InvestorQuestions', 'FK_InvestorQuestions_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('InvestorQuestions', 'FK_InvestorQuestions_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('LpDelegates', 'FK_LpDelegates_Deleg_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('LpDelegates', 'FK_LpDelegates_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('LpRequiredFundDocuments', 'FK_LpRequiredFundDocuments_Lp_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('LpRequiredFundDocuments', 'FK_LpRequiredFundDocuments_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('LpSignatories', 'FK_LpSignatories_Lp_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('LpSignatories', 'FK_LpSignatories_Signat_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('LpSignatories', 'FK_LpSignatories_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('LpSignatories', 'FK_LpSignatories_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('partnershipAgreementDocHistories', 'FK_partnershipAgreementDocHistories_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('RolePermissions', 'FK_RolePermissions_Roles_Id', { transaction: t }),
        queryInterface.removeConstraint('RolePermissions', 'FK_RolePermissions_Permissions_Id', { transaction: t }),
        queryInterface.removeConstraint('SideletterLps', 'FK_SideletterLps_Sideletters_Id', { transaction: t }),
        queryInterface.removeConstraint('SideletterLps', 'FK_SideletterLps_Lp_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('Sideletters', 'FK_Sideletters_Funds_Id', { transaction: t }),
        queryInterface.removeConstraint('Sideletters', 'FK_Sideletters_Lp_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('Sideletters', 'FK_Sideletters_FundSubscriptions_Id', { transaction: t }),
        queryInterface.removeConstraint('SignatureTracks', 'FK_SignatureTracks_Sign_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('UserRoles', 'FK_UserRoles_Roles_Id', { transaction: t }),
        queryInterface.removeConstraint('UserRoles', 'FK_UserRoles_Users_Id', { transaction: t }),
        queryInterface.removeConstraint('VcFirmSignatories', 'FK_VcFirmSignatories_VCFirms_Id', { transaction: t }),
        queryInterface.removeConstraint('VcFirmSignatories', 'FK_VcFirmSignatories_Signat_Users_Id', { transaction: t })
      ])
    })
  }
}

