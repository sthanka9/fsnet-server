const router = require('express').Router()
const guard = require('express-jwt-permissions')()
const multer = require('multer');
const { sanitizeParam } = require('express-validator/filter');
const path = require('path');
const uuidv1 = require('uuid/v1');
const commonHelper = require('../helpers/commonHelper');
const validatorHelper = require('../helpers/validatorHelper');
const permissionCheckHelper = require('../helpers/permissionCheckHelper');
const messageHelper = require('../helpers/messageHelper');
const requestValidation = require('../validator/actionRequest');

const fundImageUpload = multer({
        storage: multer.diskStorage({
                destination: function (req, file, callback) {
                        callback(null, "./assets/funds/images/")
                },
                filename: function (req, file, callback) {
                        const ext = path.extname(file.originalname)
                        callback(null, `${uuidv1()}${ext}`)
                }
        })
});

const profilePicUpload = multer({
        storage: multer.diskStorage({
                destination: function (req, file, callback) {
                        let dir = `./assets/profile/pics/`
                        commonHelper.ensureDirectoryExistence(dir)
                        callback(null, dir)
                },
                filename: function (req, file, callback) {
                        const ext = path.extname(file.originalname)
                        callback(null, `${uuidv1()}${ext}`)
                }
        })
});

const faNonClosedInvestors = multer({
        storage: multer.diskStorage({
                destination: function (req, file, callback) {                   
                        let fundId = req.params.fundId;
                        let dir = `./assets/funds/${fundId}/`
                        commonHelper.ensureDirectoryExistence(dir)
                        callback(null, dir)
                },
                filename: function (req, file, callback) {
                        let filetype = file.fieldname
                        let filename = ''
                        if(filetype=='fundagreement')
                            filename = 'FUND_AGREEMENT_'
                        else
                            filename = 'CHANGED_PAGES_'

                        const ext = path.extname(file.originalname)
                        callback(null, `${filename}${uuidv1()}${ext}`)
                }
        })
});


const customSubscriptionAgreementSections = multer({
        storage: multer.diskStorage({
                destination: function (req, file, callback) {
                        let fundId = req.body.fundId;
                        commonHelper.ensureDirectoryExistence(`./assets/funds/${fundId}`)
                        let dir = `./assets/funds/${fundId}/customSubscriptionAgreementSections/`
                        commonHelper.ensureDirectoryExistence(dir)
                        callback(null, dir)
                },
                filename: function (req, file, callback) {
                        const ext = path.extname(file.originalname)
                        callback(null, `${uuidv1()}_section_${req.body.section}${ext}`)
                }
        }),
        fileFilter: function (req, file, callback) {
                var ext = path.extname(file.originalname);
                if (ext && ext.toLowerCase() !== '.pdf') {
                        return callback(new Error(messageHelper.defaultSectionValidate));
                }
                callback(null, true);
        }
});


const vcfirmLogoUpload = multer({
        storage: multer.diskStorage({
                destination: function (req, file, callback) {
                        callback(null, "./assets/vcfirm/logos")
                },
                filename: function (req, file, callback) {
                        const ext = path.extname(file.originalname)
                        callback(null, `${uuidv1()}${ext}`)
                }
        }),
        fileFilter: function (req, file, callback) {
                var ext = path.extname(file.originalname)
                if (ext !== '.jpg') {
                        return callback(new Error('Only images are allowed.'))
                }

                callback(null, true)
        }
}).single('vcfirmLogo');


// sideLetter upload
const sideLetterFileUpload = multer({
        storage: multer.diskStorage({
                destination: function (req, file, cb) {
                        let dir = `./assets/funds/${req.body.fundId}/${req.body.subscriptionId}`;
                        commonHelper.ensureDirectoryExistence(dir);
                        cb(null, dir)
                },
                filename: function (req, file, callback) {
                        var ext = path.extname(file.originalname)
                        callback(null, `${uuidv1()}${ext}`)
                }
        })
}).single('file');


const signatureUpload = multer({
        storage: multer.diskStorage({
                destination: function (req, file, callback) {
                        callback(null, "./assets/signatures/")
                },
                filename: function (req, file, callback) {
                        constext = path.extname(file.originalname)
                        callback(null, Date.now() + "_" + Math.random().toString(26).slice(2) + ext)
                }
        })
});
 


const closedFundDocsUpload = multer({ 

        storage: multer.diskStorage({
                destination: function (req, file, callback) {
                        let fundId = req.params.fundId || req.body.fundId;
                        let uploadType = req.params.uploadType;
                        let dir =  uploadType && uploadType == 'Amendment' ? `./assets/funds/${fundId}/ammendments/` :`./assets/funds/${fundId}/`;
                        commonHelper.ensureDirectoryExistence(dir);
                        callback(null, dir)
                },
                filename: function (req, file, callback) {
                        let filetype = file.fieldname
                        const ext = path.extname(file.originalname)
                        let uploadType = req.params.uploadType; 
                        let fileName = uploadType && uploadType == 'Amendment' ? `AMENDMENT_AGREEMENT_${uuidv1()}${ext}` :  filetype=='fundDoc'  ?`FUND_AGREEMENT_${uuidv1()}${ext}` : `CHANGED_PAGES_${uuidv1()}${ext}`;    
                        callback(null, fileName)  
                }
        }) 
})


const funDocUpload = multer({
        storage: multer.diskStorage({
                destination: function (req, file, callback) {
                        let fundId = req.params.fundId || req.body.fundId;
                        let uploadType = req.params.uploadType;
                        let dir =  uploadType && uploadType == 'Amendment' ? `./assets/funds/${fundId}/ammendments/` :`./assets/funds/${fundId}/`;
                        commonHelper.ensureDirectoryExistence(dir);
                        callback(null, dir)
                },
                filename: function (req, file, callback) {
                        const ext = path.extname(file.originalname)
                        let uploadType = req.params.uploadType; 
                        let fileName = uploadType && uploadType == 'Amendment' ? `AMENDMENT_AGREEMENT_${uuidv1()}${ext}` :`FUND_AGREEMENT_${uuidv1()}${ext}`;callback(null, fileName)
                }
        }),
        fileFilter: function (req, file, callback) {
                var ext = path.extname(file.originalname)
                if (ext && ext.toLowerCase() !== '.pdf') {
                        return callback(new Error(messageHelper.fundDocumentUploadValidate))
                }

                callback(null, true)
        }
}).single('fundDoc');

// fund doc upload 
const fundDocFileUpload = multer({
        storage: multer.diskStorage({
                destination: function (req, file, cb) {
                        cb(null, 'assets/temp')
                },
                filename: function (req, file, callback) {
                        var ext = path.extname(file.originalname)
                        callback(null, `${uuidv1()}${ext}`)
                }
        })
}).single('fundDoc');

// offline user uploads
let offlineUserUploads = multer.diskStorage({
        destination: function (req, file, cb) {

                let dir = file.fieldname == 'fundAmendmentOfflineSignatures' ? `./assets/funds/${req.body.fundId}/amedments/` : `./assets/funds/${req.body.fundId}/${req.body.subscriptionId}`;
                commonHelper.ensureDirectoryExistence(dir);
                cb(null, dir);
        },
        filename: function (req, file, callback) {
                const ext = path.extname(file.originalname);
                if (ext && ext.toLowerCase() !== '.pdf') {
                        return callback(new Error(messageHelper.fundDocumentUploadValidate));
                }
                let typeOfTheDoc = '';
                if (file.fieldname == 'subscription') { typeOfTheDoc = 'SUBSCRIPTION_AGREEMENT'; }
                if (file.fieldname == 'signatures') { typeOfTheDoc = 'OFFLINE_SIGNATURE'; }
                if (file.fieldname == 'capital') { typeOfTheDoc = 'CHANGE_COMMITMENT_AGREEMENT'; }
                if (file.fieldname == 'fundAmendmentOfflineSignatures') { typeOfTheDoc = 'FUND_AMENDMENT_OFFLINE_SIGNATURE'; }
                callback(null, `${typeOfTheDoc}_${uuidv1()}${ext}`);
        }
})
const offlineUserUpload = multer({ storage: offlineUserUploads });


const authTokenMiddleware = require('../middlewares/verifyAuthToken-middleware');

const loginRequest = require('../validator/LoginRequest');
const switchAccountRequest = require('../validator/switchAccountRequest');
const vcFirmRequest = require('../validator/vcFirmRequest');
const registerRequest = require('../validator/registerRequest');
const forgotPasswordControllerRequest = require('../validator/forgotPasswordControllerRequest');
const forgotPasswordTokenValidateRequest = require('../validator/forgotPasswordTokenValidateRequest');
const resetPasswordRequest = require('../validator/resetPasswordRequest');
const storeFundRequest = require('../validator/storeFundRequest');
const lpListRequest = require('../validator/lpListRequest');
const lpSubscriptionRequest = require('../validator/lpSubscriptionRequest');
const createGpSignatoryRequest = require('../validator/createGpSignatoryRequest');
const lpInvitationRequest = require('../validator/lpInvitationRequest');
const removeLPSignatoryRequest = require('../validator/removeLPSignatoryRequest');
const signatoryInviteControllerRequest = require('../validator/signatoryInviteControllerRequest');
const addLpControllerRequest = require('../validator/addLpControllerRequest');
const assignGpDelegateControllerRequest = require('../validator/assignGpDelegateControllerRequest');
const changeCommitmentControllerRequest = require('../validator/changeCommitmentControllerRequest');
const SideletterControllerRequest = require('../validator/SideletterControllerRequest');
const delegateInviteControllerRequest = require('../validator/delegateInviteControllerRequest');
const fundControllerRequest = require('../validator/fundControllerRequest');
const questionControllerRequest = require('../validator/questionControllerRequest');
const subscriptionControllerRequest = require('../validator/subscriptionControllerRequest');
const actionsControllerRequest = require('../validator/actionRequest');
const adminControllerRequest = require('../validator/adminControllerRequest');
const eSignatureControllerRequest = require('../validator/eSignatureControllerRequest');
const requestController = require('../validator/requestController');


const vcFirmController = require('../controllers/vcFirmController');
const fundController = require('../controllers/fundController');
const fundTrackerController = require('../controllers/fundTrackerController');
const assignGpDelegateController = require('../controllers/assignGpDelegateController');
const addLpController = require('../controllers/addLpController');
const changePasswordController = require('../controllers/auth/changePasswordController');
const forgotPasswordController = require('../controllers/auth/forgotPasswordController');
const loginController = require('../controllers/auth/loginController');
const switchAccountController = require('../controllers/auth/switchAccountController');
const registerController = require('../controllers/auth/registerController');
const resetPasswordController = require('../controllers/auth/resetPasswordController');
const profileController = require('../controllers/user/profileController');
const settingsController = require('../controllers/user/settingsController');
const userController = require('../controllers/user/userController');
const notificationsController = require('../controllers/notifications/index');
const gpDelegateListController = require('../controllers/gpDelegateListController');
const lpDelegateListController = require('../controllers/lpDelegateListController');
const lpsListController = require('../controllers/lpsListController');
const lpDashboardController = require('../controllers/lpDashboardController');
const fundClosingController = require('../controllers/fundClosingController');
const sendMessageController = require('../controllers/sendMessageController');
const changeCommitmentController = require('../controllers/changeCommitmentController');
const documentLockerController = require('../controllers/documentLockerController');
const delegateInviteController = require('../controllers/delegateInviteController');
const pendingActionListController = require('../controllers/pendingActionListController');
const downloadController = require('../controllers/downloadController');
const gpDelegateApprovalController = require('../controllers/gpDelegateApprovalController');
const SubscriptionFormController = require('../controllers/SubscriptionFormController');
const questionController = require('../controllers/questionController');
const fundAgreementController = require('../controllers/fundAgreementController');
const subscriptionAgreementManage = require('../controllers/subscriptionAgreementManage');
const sampleSubscriptionAgreementPDF = require('../controllers/subscription/sampleSubscriptionAgreementPDF');
const sideLetterManageController = require('../controllers/sideLetterManageController');
const signatoryInviteController = require('../controllers/signatoryInviteController');
const eSignatureController = require('../controllers/eSignatureController');
const adminController = require('../controllers/admin/adminController');
const eventController = require('../controllers/admin/eventController');
const lpProceedToSignController = require('../controllers/subscription/lpProceedToSignController');
const lpInvitationController = require('../controllers/lpInvitationController');
const gpSignatoryApprovalController = require('../controllers/gpSignatoryApprovalController');
const subscriptionController = require('../controllers/subscriptionController');
const gpDashboardController = require('../controllers/gpDashboardController');
const fundClosingControllerRequest = require('../validator/fundClosingControllerRequest');
const documentViewerController = require('../controllers/documentViewerController');
const lpController = require('../controllers/lpController');
const offlineFileUploadController = require('../controllers/offlineFileUploadController');
//convert previous accounts
const conversionController = require('../controllers/conversionController');
const cronController = require('../controllers/cronController');

const actionsController = require('../controllers/actionsController');

//convert/migrate users to accoutnt
router.get('/convertUsersToAccounts', conversionController.convertUsers) //??
//convert user notifications to accounts
router.get('/updateUserNotificationToAccounts', conversionController.updateUserNotificationToAccounts)//??

router.post('/register', profilePicUpload.single('profilePic'), registerRequest.rules, validatorHelper.validationResult, registerController);
router.get('/getInvitationData/:id', sanitizeParam('id').toInt(), requestController.getInvitationDataRules, validatorHelper.validationResult, userController.getInvitationData);
router.post('/login', loginRequest.rules, validatorHelper.validationResult, loginController);
router.post('/forgotPassword', forgotPasswordControllerRequest.forgotPasswordRules, validatorHelper.validationResult, forgotPasswordController.forgotPassword);
router.post('/forgotPasswordTokenValidate', forgotPasswordTokenValidateRequest.rules, validatorHelper.validationResult, forgotPasswordController.forgotPasswordTokenValidate);
router.post('/resetPassword', resetPasswordRequest.rules, validatorHelper.validationResult, resetPasswordController);
router.post('/user/setPassword/:code/:id', requestController.setPasswordRules, validatorHelper.validationResult, userController.setPassword);

//when use clicks acceptance in email
router.post('/fund/changegp/accepted',requestController.changeGPToFundManagerRules,validatorHelper.validationResult,  fundController.changeGPToFundManager);

// cron job to unlock user
router.get('/unlockUsers', userController.unlockUsers);


//Cron 1 
router.get('/mapAllSubscriptionsRelatedToFund',cronController.mapAllSubscriptionsRelatedToFund);

//Cron 2
router.get('/swapAllDocuments',cronController.swapAllDocuments);

router.use(authTokenMiddleware.verifyAuthToken);


/*********************COMMON API'S********************************/

// alerts
router.get('/user/notifications/:id',requestController.idRules,validatorHelper.validationResult,permissionCheckHelper.isUserAllowedToViewNotifications, notificationsController.getAccountNotifications);
router.post('/user/notifications/dismiss', requestController.dismissNotificationRules,validatorHelper.validationResult,permissionCheckHelper.isUserAllowedToDismissNotifications, notificationsController.dismissNotification);
router.post('/user/notifications/dismissAll',notificationsController.dismissAllAccountNotifications); // need to check

router.get('/getTimezones', lpDashboardController.getTimezones)//??

router.get('/logout', loginController.logout);//??

/*************************GP API'S  START***********************************/
router.get('/funds/:search?/:limit?',
        sanitizeParam('search').toString(),
        sanitizeParam('limit').toInt(),
        guard.check('listFund'),
        gpDashboardController.fundsList);//??

router.get('/fund/view/info/:fundId?',
        guard.check('viewFund'),
        permissionCheckHelper.isUserAllowedToViewThisFund,
        fundTrackerController.getFundById);

router.get('/fund/downloadTracker/:fundId',
requestController.fundIdRules, validatorHelper.validationResult,
        guard.check('viewFund'),
        permissionCheckHelper.isUserAllowedToViewThisFund,
        fundTrackerController.fundSummary);

router.post('/closeFund',
        guard.check('closeFund'),
        funDocUpload,
        fundClosingControllerRequest.closeFundRules,
        validatorHelper.validationResult,
        permissionCheckHelper.isUserAllowedToViewThisFund,
        fundClosingController.closeFund);

router.post('/closeFundDocUpload',fundDocFileUpload, fundClosingController.closeFundDocUpload);

router.post('/fund/deactivate',
        fundControllerRequest.fundIdValidator,
        validatorHelper.validationResult,
        guard.check('deactivateFund'),
        fundController.deactivate);


router.post('/fund/sendRemainder',
requestController.fundIdLpIdRules, validatorHelper.validationResult,
        guard.check('gpSendRemainderToLp'),
        fundController.sendRemainderToLp);


router.get('/fund/view/:fundId',
requestController.fundIdRules, validatorHelper.validationResult,
        sanitizeParam('fundId').toString(),
        permissionCheckHelper.isUserAllowedToViewThisFund,
        fundTrackerController.viewFund);

router.get('/fund/view/:fundId/total',
requestController.fundIdRules, validatorHelper.validationResult,
        sanitizeParam('fundId').toString(),
        permissionCheckHelper.isUserAllowedToViewThisFund,
        fundTrackerController.fundTotals)

router.get('/fund/view/:fundId/rightSideSummary',
requestController.fundIdRules,validatorHelper.validationResult,
        sanitizeParam('fundId').toString(),
        permissionCheckHelper.isUserAllowedToViewThisFund,
        fundTrackerController.rightSideSummary);

router.get('/fund/view/:fundId/leftSideSummary',
requestController.fundIdRules,validatorHelper.validationResult,
        sanitizeParam('fundId').toString(),
        permissionCheckHelper.isUserAllowedToViewThisFund,
        fundTrackerController.leftSideSummary);
 
router.get('/gpDocumentLocker/:fundId',
requestController.fundIdRules, validatorHelper.validationResult,
        sanitizeParam('fundId').toInt(),
        guard.check('gpDocumentLocker'),
        permissionCheckHelper.isUserAllowedToViewThisFund,
        documentLockerController.getGPDocuments);

router.get('/gpSideLetter/:fundId', 
requestController.fundIdRules, validatorHelper.validationResult,
        permissionCheckHelper.isUserAllowedToViewThisFund,
        sideLetterManageController.getGPSideLetters);

router.get('/getGPAmendments/:fundId',
requestController.fundIdRules, validatorHelper.validationResult,
        sanitizeParam('fundId').toInt(),
        permissionCheckHelper.isUserAllowedToViewThisFund,
        fundAgreementController.getGPAmendments);

router.post('/subscription/agreement', 
        customSubscriptionAgreementSections.single('file'),
        requestController.fundIdRules, validatorHelper.validationResult,        
        permissionCheckHelper.isUserAllowedToViewThisFund,
        subscriptionAgreementManage.upload);   
        
router.get('/fund/sendAllLpInvitations/:fundId', requestController.fundIdRules,   validatorHelper.validationResult, 
permissionCheckHelper.isUserAllowedToViewThisFund,
addLpController.sendAllLpInvitations);

router.post('/fund/assign/investors',
requestController.fundIdLpIdsRules,validatorHelper.validationResult,
permissionCheckHelper.isUserAllowedToViewThisFund,
addLpController.assignInvestors);

/*************************GP API'S END***********************************/



//switch user accounts
router.post('/switchaccount', switchAccountRequest.rules, validatorHelper.validationResult, switchAccountController);

//get list of roles for user
router.get('/listUsersForAccount', switchAccountController.listUsersForAccount); 

//get changespages for closed investors
router.get('/closedlp/getRevisedFundAgreement/:subscriptionId/:documentId', 
sanitizeParam('documentId').toInt(),
permissionCheckHelper.isUserAllowedToViewThisDoc, lpProceedToSignController.closedLPRevisedFundAgreement);

//get changespages for closed investors
router.get('/closedlp/getRevisedFundAgreement/:subscriptionId/:documentId', 
sanitizeParam('documentId').toInt(),
permissionCheckHelper.isUserAllowedToViewThisDoc, lpProceedToSignController.closedLPRevisedFundAgreement);

//need to remove on monday
router.get('/document/view/cfa/:fundId',permissionCheckHelper.isGPUserAllowedToViewThisDoc, documentViewerController.getConsolidatedFundAgreement);
router.get('/document/view/sa/:subscriptionId',permissionCheckHelper.isUserAllowedToViewThisDoc, documentViewerController.getSubscriptionAgreement);
router.get('/document/view/cc/:subscriptionId', permissionCheckHelper.isUserAllowedToViewThisDoc,documentViewerController.getChangeCommitmentAgreement);
router.get('/document/view/ccfile/:subscriptionId',permissionCheckHelper.isUserAllowedToViewThisDoc,documentViewerController.getChangeCommitmentAgreementFile);
router.get('/document/view/ccgpsign/:subscriptionId', permissionCheckHelper.isUserAllowedToViewThisDoc,documentViewerController.getGPChangeCommitmentAgreement);
router.get('/document/view/viewcc/:subscriptionId', permissionCheckHelper.isUserAllowedToViewThisDoc,documentViewerController.viewChangeCommitmentAgreement);
router.get('/document/view/cp/:fundId', documentViewerController.getChangedPagesAgreement);
router.get('/document/view/viewcp/:id', documentViewerController.viewChangedPagesAgreement);
router.get('/document/view/sia/:subscriptionId',permissionCheckHelper.isUserAllowedToViewThisDoc, documentViewerController.getSideLetterAgreement); 
router.get('/document/view/viewsia/:subscriptionId', permissionCheckHelper.isUserAllowedToViewThisDoc,documentViewerController.viewSideLetterAgreement);
router.get('/document/view/viewiasia/:id', documentViewerController.viewInActiveSideLetterAgreement);
router.get('/document/view/fa/:subscriptionId',permissionCheckHelper.isUserAllowedToViewThisDoc, documentViewerController.getFundAgreement);
router.get('/document/view/afa/:subscriptionId/:documentId', 
requestController.getAmendmentFundAgreementRules,validatorHelper.validationResult,
sanitizeParam('documentId').toInt(),
permissionCheckHelper.isUserAllowedToViewThisDoc,documentViewerController.getAmendmentFundAgreement);
router.get('/document/view/docs/:documentId', requestController.documentIdRules, validatorHelper.validationResult, documentViewerController.getDocumentPath); 
router.get('/document/view/dfsammendment/:subscriptionId/:documentId', requestController.getDFSAmmendmentPathRules,validatorHelper.validationResult,permissionCheckHelper.isUserAllowedToViewThisDoc,documentViewerController.getDFSAmmendmentPath);
router.get('/document/view/ammendment/:subscriptionId/:amendmentId', requestController.getAmmendmentPathRules, validatorHelper.validationResult, permissionCheckHelper.isUserAllowedToViewThisDoc,documentViewerController.getAmmendmentPath);
router.get('/document/view/displayamendment/:amendmentId',requestController.amendmentIdRules, validatorHelper.validationResult, permissionCheckHelper.isUserAllowedToViewThisDoc,documentViewerController.displayAmendment);
//To display uploded FA
router.get('/document/view/displayFA/:fundId',  requestController.fundIdRules,validatorHelper.validationResult,permissionCheckHelper.isGPUserAllowedToViewThisDoc,documentViewerController.displayFA);

router.get('/getVCfirms/:delegateId',requestController.delegateIdRules,validatorHelper.validationResult, delegateInviteController.getVCFirmsOfDelegate);

//get subscription docs
router.get('/document/view/:section/:fundId', requestController.sectionViewRules,validatorHelper.validationResult,permissionCheckHelper.isGPUserAllowedToViewThisDoc,
documentViewerController.sectionView);

// vcFirm 
router.post('/createVCFirm', vcfirmLogoUpload, vcFirmRequest.rules, validatorHelper.validationResult, vcFirmController.createVCFirm)



router.get('/firm/list', guard.check('firmList'), vcFirmController.get);//??
router.get('/firm/fund/list/:vcfirmId',requestController.vcfirmIdRules, validatorHelper.validationResult, guard.check('firmFundList'), vcFirmController.fundList);

//funds



router.get('/fund/getSubscriptionDetails/:fundId/:subscriptionId',  
        requestController.fundIdSubscriptionIdRules, validatorHelper.validationResult,
        permissionCheckHelper.isGPUserAllowedtoSignThisDoc,          
        fundController.getSubscriptionDetails);

router.get('/fund/:fundId',
        sanitizeParam('fundId').toInt(),
        guard.check('viewFund'),
        permissionCheckHelper.isUserAllowedToViewThisFund,
        lpListRequest.rules,
        validatorHelper.validationResult,
        fundController.getFundById);

router.get('/fund/getfirmlps/:fundId',
        sanitizeParam('fundId').toInt(),
        guard.check('viewFund'),
        permissionCheckHelper.isUserAllowedToViewThisFund,
        lpListRequest.rules,
        validatorHelper.validationResult,
        fundController.getFirmLPs);        

router.get('/subscription/form/html/status/:fundId/:subscriptionId',
        sanitizeParam('fundId').toInt(),
        sanitizeParam('subscriptionId').toInt(),
        requestController.fundIdSubscriptionIdRules, validatorHelper.validationResult,
        fundController.getStatusOfSubscriptionForm);

router.post('/fund/change/gp', requestController.changeFundGPRules,validatorHelper.validationResult, fundController.changeFundGP);
router.post('/fund/gp/convert/to/secondary',requestController.gprequestidRules, validatorHelper.validationResult, fundController.convertGPToSignatory);

router.post('/fund/delete', requestController.fundIdRules, validatorHelper.validationResult, fundController.deleteFund);

router.post('/fund/store',
        sanitizeParam('vcfirmId').toInt(),
        fundImageUpload.single('fundImage'),
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        storeFundRequest.rules,
        validatorHelper.validationResult,
        fundController.fundStore);

router.post('/fund/gpDelegate/invite',
        guard.check('gpDelegateInvite'),
        delegateInviteControllerRequest.fundGpDelegateInvite,
        validatorHelper.validationResult,
        delegateInviteController.fundGpDelegateInvite);


router.post('/fund/assignGpDelegate',
        assignGpDelegateControllerRequest.rules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        assignGpDelegateController);


router.get('/fund/gpDelegateList/:vcfirmId/:fundId/:orderBy?/:order?',
        sanitizeParam('vcfirmId').toInt(),
        sanitizeParam('fundId').toInt(),
        sanitizeParam('orderBy').toString(),
        sanitizeParam('order').toString(),
        guard.check('fundCreate'),
        requestController.vcFirmIdfundIdRules, validatorHelper.validationResult,
        permissionCheckHelper.isUserAllowedToUpdateFund,
        gpDelegateListController);

/*
router.post('/fund/removeFundGPDelegate',
requestController.fundIddelegateIdRules, validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        fundController.removeFundGPDelegate);
*/

router.post('/fund/removeFundGpDelegate',
        signatoryInviteControllerRequest.removeFundGpDelegateRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        signatoryInviteController.removeFundGpDelegate);
                

router.post('/fund/remove/file',
        fundControllerRequest.fundIdValidator,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        fundController.removeFundFile);


router.post('/delegate/gp/check',
        delegateInviteControllerRequest.isGpDelegateNew,
        guard.check('fundCreate'),
        delegateInviteController.isGpDelegateNew);


router.post('/fund/document/add/:fundId',
        sanitizeParam('fundId').toInt(),
        guard.check('fundCreate'),
        funDocUpload,
        permissionCheckHelper.isUserAllowedToViewThisFund,
        fundController.fundDocumentUpload);


router.post('/fund/removeLp',
        guard.check('fundCreate'),
        requestController.fundIdLpIdRules,validatorHelper.validationResult,
        permissionCheckHelper.isUserAllowedToUpdateFund,
        fundController.removeLp);

router.post('/fund/assign/lp',
requestController.fundIdLpIdRules,validatorHelper.validationResult,
        addLpController.assignLp);

router.post('/fund/add/lp',
        addLpControllerRequest.addLpRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        addLpController.addLp);

router.post('/fund/add/offlineLp',
        addLpControllerRequest.addOfflineLpRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        addLpController.addOfflineLp);

router.post('/fund/update/lp',
        addLpControllerRequest.updateLpRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        addLpController.updateLp);

router.post('/fund/update/lpdelegate',
        addLpControllerRequest.updateLpDelegateRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        addLpController.updateLpDelegate);        

router.post('/fund/update/offlineLp',
        addLpControllerRequest.updateOfflineLpRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        addLpController.updateOfflineLp);

router.post('/fund/delete/lp',
        addLpControllerRequest.deleteLpRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        addLpController.deleteLp);

router.post('/fund/delete/offlineLp',
        addLpControllerRequest.deleteOfflineLpRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        addLpController.deleteOfflineLp);


router.post('/fund/sendLpInvitations', requestController.fundIdRules,validatorHelper.validationResult, addLpController.sendLpInvitations);

router.post('/fund/delete/removeClosedLp',
        guard.check('fundCreate'),
        requestController.fundIdLpIdRules, validatorHelper.validationResult,
        permissionCheckHelper.isUserAllowedToUpdateFund,
        fundController.removeClosedLp);

router.get('/fund/lpList/:vcfirmId/:fundId/:orderBy?/:order?',
        sanitizeParam('vcfirmId').toInt(),
        sanitizeParam('fundId').toInt(),
        sanitizeParam('orderBy').toString(),
        sanitizeParam('order').toString(),
        guard.check([['createFund'], ['gpDocumentLocker']]),
        requestController.vcFirmIdfundIdRules, validatorHelper.validationResult,
        lpsListController);


router.post('/fund/publish',
        fundControllerRequest.fundIdValidator,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        fundController.publishFund);


router.post('/lp/check',
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        requestController.emailRules, validatorHelper.validationResult,
        addLpController.isNewLP);

router.post('/lp/check/isNewOfflineLP',
        guard.check('fundCreate'),
        requestController.emailRules, validatorHelper.validationResult,
        permissionCheckHelper.isUserAllowedToUpdateFund,
        addLpController.isNewOfflineLP);


router.get('/viewFundType', fundTrackerController.viewFundType)//??

// fundClosingController
router.get('/fund/getClosedReadyLps/:fundId',
        requestController.fundIdRules,validatorHelper.validationResult,
        permissionCheckHelper.isUserAllowedToViewThisFund,
        lpDashboardController.getClosedReadyLps);


router.post('/sendMessage',
        guard.check('gpSendMessageToLp'),
        requestController.messageRules, validatorHelper.validationResult,
        sendMessageController.sendMessage);

router.post('/titleUniqueCheck',requestController.fundIdSubscriptionIdRules, validatorHelper.validationResult, lpDashboardController.titleUniqueCheck);


// subscription
router.post('/lp/delete', requestController.subscriptionIdRules, validatorHelper.validationResult,
        subscriptionController.deleteSubscription);

router.post('/lp/delete/soft', requestController.softDeleteRules, validatorHelper.validationResult,
        subscriptionController.softDeleteSubscription);
/**********************************LP API'S Start******************************************/

router.get('/lp/fund/list',
        guard.check('listSubscriptions'),
        lpDashboardController.fundsList); //??


// LP pending actions
router.get('/lp/pendingActions/:subscriptionId',
        sanitizeParam('subscriptionId').toInt(),
        guard.check('lpPendingActions'),
        pendingActionListController.lpPendingActions)
router.get('/gp/pendingActions/:fundId', sanitizeParam('fundId').toInt(), guard.check('gpPendingActions'), pendingActionListController.gpPendingActions)


router.get('/lp/signatureView/:subscriptionId', lpProceedToSignController.getCurrentLinks);
router.get('/lp/currentSignatureView/:subscriptionId',permissionCheckHelper.isLPUserAllowedtoSignThisDoc,
permissionCheckHelper.checkLpSignatoryAccess,
permissionCheckHelper.isUserAllowedToViewThisDoc, lpProceedToSignController.getCurrentLinks);

router.post('/lp/submit/docsForSignatories', lpProceedToSignController.generateSubscriptionAndFundAgreement);
router.post('/lp/signature', lpProceedToSignController.verifySignaturePassword, lpProceedToSignController.signSubscriptionAndPartnershipAgreement);


/**********************************End LP API'S ******************************************/

router.post('/fund/lpDelegate/invite',
        delegateInviteControllerRequest.fundLpDelegateInvite,
        guard.check([['subscriptionCreate'], ['subscriptionLpDelegateManage']]),
        delegateInviteController.fundLpDelegateInvite);


router.get('/fund/lpDelegateList/:fundId/:subscriptionId/:orderBy?/:order?',
        requestController.fundIdSubscriptionIdRules, validatorHelper.validationResult,
        lpDelegateListController);

router.post('/fund/removeFundLpDelegate',
        fundControllerRequest.removeFundLpDelegate,
        guard.check([['subscriptionCreate'], ['subscriptionLpDelegateManage']]),
        fundController.removeFundLpDelegate);

router.post('/delegate/lp/check',
        delegateInviteControllerRequest.isLpDelegateNew,
        guard.check([['subscriptionCreate'], ['subscriptionLpDelegateManage']]),
        delegateInviteController.isLpDelegateNew);

router.get('/lp/subscription/:subscriptionId',
        sanitizeParam('subscriptionId').toInt(),
        subscriptionControllerRequest.getRules,
        validatorHelper.validationResult,
        guard.check([['subscriptionCreate'], ['viewSubscription']]),
        permissionCheckHelper.isUserAllowedToViewThisSubscription,
        subscriptionController.get);

router.post('/lp/subscription',
        guard.check('subscriptionCreate'),
        lpSubscriptionRequest.rules,
        validatorHelper.validationResult,
        lpDashboardController.store);

router.post('/lp/fund/accept',
        guard.check('lpNewSubscriptionAction'),
        requestController.subscriptionIdRules, validatorHelper.validationResult,
        lpInvitationController.acceptFundSubscription);

router.post('/lp/fund/reject',
        guard.check('lpNewSubscriptionAction'),
        requestController.subscriptionIdRules, validatorHelper.validationResult,
        lpInvitationController.rejectFundSubscription);

router.post('/lp/fund/reinvite',
        guard.check('viewFund'),
        lpInvitationRequest.rules,
        validatorHelper.validationResult,
        lpInvitationController.reinviteLp);

router.get('/getDocumentTypes',
        guard.check([['gpDocumentLocker'], ['lpDocumentLocker']]),
        documentLockerController.getDocumentTypes); //??

router.get('/lp/prePopulate/subscription/:subscriptionId/:search?',
        sanitizeParam('subscriptionId').toInt(),
        sanitizeParam('search').toString(),
        requestController.subscriptionIdRules, validatorHelper.validationResult,
        lpDashboardController.getPreviousFunds);

router.post('/lp/subscription/clone',
        guard.check('subscriptionCreate'),
        requestController.cloneSubscriptionRules, validatorHelper.validationResult,
        lpDashboardController.cloneSubscription);

// for all
router.get('/lpDocumentLocker/:subscriptionId?',
        sanitizeParam('subscriptionId').toInt(),
        guard.check('lpDocumentLocker'),
        requestController.subscriptionIdRules, validatorHelper.validationResult,
        documentLockerController.getLPDocuments);


router.post('/sideletter/upload',sideLetterFileUpload,  requestController.fundIdRules, validatorHelper.validationResult, sideLetterManageController.upload);

router.post('/lp/changeSideletter/signature',
requestController.documentIdRules, validatorHelper.validationResult,
permissionCheckHelper.isLPUserAllowedtoSignThisDoc, 
permissionCheckHelper.isUserAllowedToViewThisDoc,
lpProceedToSignController.verifySignaturePassword, 
lpProceedToSignController.signChangeSideletter);

// just login user
router.get('/getUSStates', lpDashboardController.getUSStates) //??
router.get('/getStates/:countryId', requestController.countryIdRules, validatorHelper.validationResult, lpDashboardController.getStates)
router.get('/getAllCountries/:exceptUS?', sanitizeParam('exceptUS').toString(), lpDashboardController.getAllCountries) //??
router.get('/getAllCountries', sanitizeParam('exceptUS').toString(), lpDashboardController.getAllCountries) //??
router.get('/getInvestorSubType/:group', guard.check('subscriptionCreate'), sanitizeParam('group').toString(),
requestController.groupRules, validatorHelper.validationResult, lpDashboardController.getInvestorSubType)


// just login user - edit profile
router.post('/profile', profilePicUpload.single('profilePic'), requestController.editProfileRules, validatorHelper.validationResult, profileController.profileEdit)
router.get('/profile/:userId', requestController.userIdRules, validatorHelper.validationResult,profileController.profileDetails)
router.post('/userSettings', requestController.userIdRules,validatorHelper.validationResult, settingsController)
router.post('/changePassword', requestController.changePasswordRules,validatorHelper.validationResult, changePasswordController)
router.post('/signaturePic', signatureUpload.single('signaturePic'), userController.signaturePic)
router.post('/impersonateUser',requestController.userIdRules, validatorHelper.validationResult, guard.check('impersonateUser'), userController.impersonateUser);
router.post('/verifySignaturePassword',userController.verifySignaturePassword)
router.get('/user/public/profile/:id', requestController.idRules, validatorHelper.validationResult, profileController.getPublicProfile)



router.get('/getCapitalCommitmentChangeAgreement/:id', requestController.idRules, validatorHelper.validationResult ,changeCommitmentController.getCapitalCommitmentChangeAgreement)

router.get('/getSideLetterAgreement/:id',  requestController.idRules, validatorHelper.validationResult , sideLetterManageController.getSideLetterAgreement)


//cancel commitment change
router.get('/gpCancelCommitmentChange/:subscriptionId', changeCommitmentControllerRequest.cancelCommitmentChange,
        validatorHelper.validationResult, changeCommitmentController.CancelCommitmentChange)

router.post('/lpCommitmentChange',
        changeCommitmentControllerRequest.saveLpCommitmentsRules,
        guard.check('lpCommitmentChangeRequest'),
        changeCommitmentController.saveLpCommitments);

router.post('/lpCommitmentChange/gp/sign',
        changeCommitmentControllerRequest.gpSign,
        permissionCheckHelper.isGPUserAllowedtoSignThisDoc,
        permissionCheckHelper.isUserAllowedToViewThisDoc,
        guard.check('commitmentChangeGpSign'),
        changeCommitmentController.gpSign);

router.post('/lpSideletter/gp/sign',
        SideletterControllerRequest.gpSign,
        permissionCheckHelper.isGPUserAllowedtoSignThisDoc,
        permissionCheckHelper.isUserAllowedToViewThisDoc,
        sideLetterManageController.gpSign);

router.post('/sendForSignatureByDelegate', requestController.fundIdSubscriptionIdRules, validatorHelper.validationResult,
        lpDashboardController.sendForSignatureByDelegate);


// LP pending actions
router.get('/lp/pendingActions/:subscriptionId',
        sanitizeParam('subscriptionId').toInt(),
        guard.check('lpPendingActions'),
        requestController.subscriptionIdRules, validatorHelper.validationResult,
        pendingActionListController.lpPendingActions)
router.get('/gp/pendingActions/:fundId', sanitizeParam('fundId').toInt(), guard.check('gpPendingActions'), 
requestController.fundIdRules,validatorHelper.validationResult,pendingActionListController.gpPendingActions)

router.get('/lp/signatureView/:subscriptionId',requestController.subscriptionIdRules,validatorHelper.validationResult, lpProceedToSignController.getCurrentLinks);
router.get('/lp/currentSignatureView/:subscriptionId', requestController.subscriptionIdRules,validatorHelper.validationResult, 
//permissionCheckHelper.isUserAllowedToSeePendingLinks,
permissionCheckHelper.isUserAllowedToViewThisDoc, 
lpProceedToSignController.getCurrentLinks);

router.post('/lp/submit/docsForSignatories', requestController.subscriptionIdRules,validatorHelper.validationResult,  lpProceedToSignController.generateSubscriptionAndFundAgreement);
router.post('/lp/signature', requestController.subscriptionIdRules,validatorHelper.validationResult, lpProceedToSignController.verifySignaturePassword, lpProceedToSignController.signSubscriptionAndPartnershipAgreement);

router.post('/gp/signatory/signature', requestController.fundIdRules,validatorHelper.validationResult, 
        lpProceedToSignController.verifySignaturePassword, 
        gpSignatoryApprovalController.signatoryApprovalForDocs);

router.post('/lp/changeCommitment/signature', requestController.documentIdRules, validatorHelper.validationResult,
permissionCheckHelper.isLPUserAllowedtoSignThisDoc,
permissionCheckHelper.isUserAllowedToViewThisDoc,
lpProceedToSignController.verifySignaturePassword, lpProceedToSignController.signChangeCommitment);

router.post('/download/files', requestController.fundIdSubscriptionIdRules, validatorHelper.validationResult, downloadController.download);
router.post('/download/amendmentfiles',requestController.fileIdsRules,validatorHelper.validationResult, downloadController.amendmentdownload);

// gp delegate approval process
router.get('/delegate/checkConsentForClose/fund/:fundId',requestController.fundIdRules,validatorHelper.validationResult, gpDelegateApprovalController.checkConsentForClose)
router.post('/delegate/lp/approvalForClose', requestController.fundIdRules,validatorHelper.validationResult,
        //guard.check('approvalForClose'), 
        gpDelegateApprovalController.approvalForClose);

router.get('/subscription/form/restoreToDefault/:fundId/:subscriptionId/:sectionId',requestController.fundIdSubscriptionIdSectionIdRules,validatorHelper.validationResult, SubscriptionFormController.restoreToDefault);

router.post('/subscription/form/edit/:fundId/:subscriptionId/:sectionId', requestController.fundIdSubscriptionIdSectionIdRules,validatorHelper.validationResult, SubscriptionFormController.update);
router.get('/subscription/form/get/:fundId/:subscriptionId/:sectionId',requestController.fundIdSubscriptionIdSectionIdRules,validatorHelper.validationResult, SubscriptionFormController.get);
router.get('/subscription/sample/generate/:fundId/:subscriptionId',requestController.fundIdSubscriptionIdRules,validatorHelper.validationResult, sampleSubscriptionAgreementPDF.generate);
router.get('/rescindSubscription/:subscriptionId', requestController.subscriptionIdRules,validatorHelper.validationResult, lpDashboardController.rescindSubscription);
router.post('/restoreDeclinedInvestment', requestController.restoreDeclinedInvestmentRules, validatorHelper.validationResult, lpDashboardController.restoreDeclinedInvestment);
router.get('/reIssueSubscriptionAgreement/:fundId/:subscriptionId',requestController.fundIdSubscriptionIdRules, validatorHelper.validationResult, SubscriptionFormController.reIssueSubscriptionAgreement);

router.post('/question/add', questionControllerRequest.addOrUpdate, guard.check('addQuestions'), questionController.add);
router.get('/question/list/:fundId/:subscriptionId', requestController.fundIdSubscriptionIdRules, validatorHelper.validationResult,  guard.check('addQuestions'), questionController.list);
router.post('/question/edit/:questionId', questionControllerRequest.addOrUpdate, guard.check('addQuestions'), questionController.update);
router.post('/question/delete/:questionId', requestController.questionIdRules, validatorHelper.validationResult, guard.check('addQuestions'), questionController.questionDelete);

// Fund Agreement Change
router.get('/checkFundStatus/:fundId',
        sanitizeParam('fundId').toInt(),
        requestController.fundIdRules, validatorHelper.validationResult,
        fundAgreementController.checkFundStatus);

//FA //Amendment
router.post('/uploadWithoutInvestorConsent/:fundId/:uploadType',
        sanitizeParam('fundId').toInt(),
        requestController.fundIdRules, validatorHelper.validationResult,
        funDocUpload,
        fundAgreementController.withoutInvestorConsentUpload);

router.get('/existingFAandAmmendments/:fundId',
        sanitizeParam('fundId').toInt(),
        requestController.fundIdRules, validatorHelper.validationResult,
        fundAgreementController.existingFAandAmmendments);

router.post('/uploadDocsToClosedInvestors/:fundId/:uploadType',
        sanitizeParam('fundId').toInt(),
        closedFundDocsUpload.any(),
        fundAgreementController.uploadDocsToClosedInvestors);
 
router.get('/displayAmmendmentTracker/:fundId',
        sanitizeParam('fundId').toInt(),
        requestController.fundIdRules, validatorHelper.validationResult,
        fundAgreementController.ammendmentSummary);


router.get('/investorsDetails/:fundId/:amendmentId',
        sanitizeParam('fundId').toInt(),
        sanitizeParam('amendmentId').toInt(),
        requestController.fundIdAmendmentIdRules, validatorHelper.validationResult,
        fundAgreementController.investorsDetails);

router.get('/sendRemainderToClosedInvestors/:fundId/:amendmentId',
        sanitizeParam('fundId').toInt(),
        sanitizeParam('amendmentId').toInt(),
        requestController.fundIdAmendmentIdRules, validatorHelper.validationResult,
        fundAgreementController.sendRemainderToClosedInvestors);

router.post('/fund/uploadFANonClosedInvestors/:fundId', 
        faNonClosedInvestors.any(), 
        requestController.fundIdRules, validatorHelper.validationResult,
        fundAgreementController.uploadFANonClosedInvestors);

router.post('/effectuateAmendment',
        lpProceedToSignController.verifySignaturePassword,
        requestController.fundIdAmendmentIdRules, validatorHelper.validationResult,
        fundAgreementController.effectuateAmendment);

router.post('/terminateAmendment',
        lpProceedToSignController.verifySignaturePassword,
        requestController.fundIdAmendmentIdRules, validatorHelper.validationResult,
        fundAgreementController.terminateAmendment);

router.post('/lp/signature/ammendment', 
        lpProceedToSignController.verifySignaturePassword,
         requestController.signPendingAmmendmentAgreementRules, validatorHelper.validationResult, lpProceedToSignController.signPendingAmmendmentAgreement);

router.post('/lp/signature/fundagreement', 
        requestController.signPendingAmmendmentAgreementRules, validatorHelper.validationResult,
        lpProceedToSignController.verifySignaturePassword,
        lpProceedToSignController.signPendingFundAgreement);

router.get('/subscription/:section/download/:fundId/:subscriptionId',
        requestController.downloadRules, validatorHelper.validationResult,
        subscriptionAgreementManage.download);

router.post('/fund/additionalSignatoryPages/add/:fundId',
        sanitizeParam('fundId').toInt(),
        guard.check('fundCreate'),
        requestController.fundIdRules, validatorHelper.validationResult,
        permissionCheckHelper.isUserAllowedToViewThisFund,
        fundController.additionalSignatoryPages);

router.post('/fund/additionalSignatoryPages/remove',
        fundControllerRequest.fundIdValidator,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        fundController.removeAdditionalSignatoryPages);


router.post('/sendGPAccountDetails',
        requestController.vcfirmIdRules, validatorHelper.validationResult,
        vcFirmController.triggerGPAccountDetails);

router.post('/triggerAccountAndFundDetailsEmail',
        requestController.fundIdRules, validatorHelper.validationResult,
        vcFirmController.triggerAccountAndFundDetailsEmail);

// fund manager (GP) - add multiple signatories
router.post('/signatory/gp/check',
signatoryInviteControllerRequest.isGpSignatoryNewRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        signatoryInviteController.isGpSignatoryNew);

router.post('/fund/gpSignatory/invite',
        guard.check('gpSignatoryInvite'),
        createGpSignatoryRequest.rules,
        validatorHelper.validationResult,
        signatoryInviteController.fundGpSignatoryInvite);

router.post('/fund/assignGpSignatory',
        signatoryInviteControllerRequest.assignGpSignatoryToFundRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        signatoryInviteController.assignGpSignatoryToFund);

router.post('/fund/removeFundGPSignatory',
        signatoryInviteControllerRequest.removeFundGPSignatoryRules,
        validatorHelper.validationResult,
        guard.check('fundCreate'),
        permissionCheckHelper.isUserAllowedToUpdateFund,
        signatoryInviteController.removeFundGPSignatory);

router.post('/fund/signatory/settings',
        signatoryInviteControllerRequest.signatureSettingsRules,
        validatorHelper.validationResult,
        signatoryInviteController.signatureSettings);

// LP - manage multiple signatories
router.post('/signatory/lp/check', signatoryInviteControllerRequest.isLpSignatoryNewRules,validatorHelper.validationResult,
        guard.check('subscriptionCreate'),
        signatoryInviteController.isLpSignatoryNew);

router.post('/fund/lpSignatory/invite',
        signatoryInviteControllerRequest.fundLpSignatoryInviteRules,
        validatorHelper.validationResult,
        guard.check('subscriptionCreate'),
        signatoryInviteController.fundLpSignatoryInvite);

router.post('/fund/assignLpSignatory',
        signatoryInviteControllerRequest.assignLpSignatoryToSubscriptionRules,
        validatorHelper.validationResult,
        guard.check('subscriptionCreate'),
        signatoryInviteController.assignLpSignatoryToSubscription);

router.post('/fund/removeFundLpSignatory',
        removeLPSignatoryRequest.rules,
        validatorHelper.validationResult,
        guard.check('subscriptionCreate'),
        signatoryInviteController.removeFundLpSignatory);



/*
------------------------
 admin related routes
 ------------------------
 */
router.get('/admin/gp/gpDelegates/list', adminController.gpList); //??
router.get('/admin/lp/lpDelegates/list', adminController.lpList); //??
router.post('/admin/setNotificationSetting/user/:userId', adminController.setNotificationSetting);
router.get('/listFunds', adminController.listFunds); //??
router.get('/listLps/:fundId',  adminControllerRequest.listGpLpRules, validatorHelper.validationResult, adminController.listLpsByFundId);
router.post('/admin/add/GpDelegate',  adminControllerRequest.addGpLpRules, validatorHelper.validationResult,  adminController.addGpDelegate);
router.post('/admin/add/Lp',adminControllerRequest.addGpLpRules, validatorHelper.validationResult, adminController.addLp);

router.post('/admin/add/LpDelegate',adminControllerRequest.addLpDelegateSignatoryRules, validatorHelper.validationResult, adminController.addLpDelegate);
router.get('/listLps', adminController.listLps);// ??
router.post('/admin/add/GpSignatory', adminControllerRequest.addGpLpRules, validatorHelper.validationResult, guard.check('adminCheck'), adminController.addGpSignatory);
router.post('/admin/add/LpSignatory',adminControllerRequest.addLpDelegateSignatoryRules, validatorHelper.validationResult, adminController.addLpSignatory);
router.post('/admin/user/delete',adminControllerRequest.deleteUserRules, validatorHelper.validationResult, adminController.deleteUser);
router.get('/admin/events/list', eventController.list); //??
router.get('/listUsers', eventController.listUsers); // ??
router.get('/eventTypes', eventController.eventTypes);// ??

router.get('/get/user/accounts', userController.getAccounts);//??

router.post('/signatory/eSignature/request',eSignatureControllerRequest.addeSignatureRules, validatorHelper.validationResult, eSignatureController.sendESignatureLink);
router.get('/signatory/eSignature/validate/:tokenId',eSignatureControllerRequest.getDocsForSignatureRules, validatorHelper.validationResult, eSignatureController.validateAndGetDocsForSignature);
router.post('/create/subscription',eSignatureControllerRequest.createSubscriptionRules, validatorHelper.validationResult, subscriptionController.createFormOtherSubscription);


//Investors CRUD 
router.get('/fund/lpsList/:vcfirmId/:fundId', fundControllerRequest.getFoundList,validatorHelper.validationResult,  lpController.getAllLpsList);
router.post('/fund/assignLps', fundControllerRequest.assignLpsRules, validatorHelper.validationResult, lpController.assignLps);

//Offline uploads
router.post('/fund/offlineSA',offlineUserUpload.single('subscription'), fundControllerRequest.offlineSubscriptionUploadRules, validatorHelper.validationResult,offlineFileUploadController.offlineSubscriptionUploadController);
router.post('/fund/offlineLpSignature', offlineUserUpload.single('signatures'),fundControllerRequest.offlineSubscriptionUploadRules, validatorHelper.validationResult,  offlineFileUploadController.offlineLpSignaturesUploadController);
router.post('/fund/offlineCapitalCommitment',offlineUserUpload.single('capital'), fundControllerRequest.offlineLpCapitalCommitmentRules, validatorHelper.validationResult,  offlineFileUploadController.offlineLpCapitalCommitment);
router.post('/fund/offlinefundAmendmentSignatures', 
offlineUserUpload.single('fundAmendmentOfflineSignatures'), 
fundControllerRequest.offlinefundAmendmentSignaturesRules, validatorHelper.validationResult, offlineFileUploadController.offlinefundAmendmentSignatures);
router.post('/fund/offlineSideLetter',sideLetterFileUpload, fundControllerRequest.offlineSideletterUploadRules,
validatorHelper.validationResult, 
offlineFileUploadController.offlineSideletterUploadController);
router.get('/getOfflinePendingDocuments/:subscriptionId', fundControllerRequest.getOfflinePendingDocumentsRules
, validatorHelper.validationResult, offlineFileUploadController.getOfflinePendingDocuments)

//Actions
router.post('/edit/GpDelegateOrSignatory', actionsControllerRequest.editRules, validatorHelper.validationResult, actionsController.editGPDelOrSign);
router.post('/resend/resendEmail', actionsControllerRequest.resendRules, validatorHelper.validationResult, actionsController.resendEmail);
router.post('/copy/copyLink', actionsControllerRequest.copyRules, validatorHelper.validationResult, actionsController.copyLink);
router.post('/actionsHistory', actionsControllerRequest.actionHistroyRules, validatorHelper.validationResult, actionsController.getActionsHistory);
//router.delete('/delete/deleteDelegate/:id', actionsController.deleteDelegate);

router.get('/session/user/info', (req, res) => {
        const user = req.user;
        return res.json({
                firstName: user.firstName,
                lastName: user.lastName,
                middleName: user.middleName,
                accountType: user.accountType,
                id: user.id
        });
});

module.exports = router;

