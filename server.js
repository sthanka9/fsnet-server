const express = require('express')
const app = express()
const http = require('http').Server(app)
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path')
const config = require('./config/index')
const logger = require('./helpers/logger');
const morganBody = require('morgan-body');
const fs = require('fs');
const Puppeteer = require('puppeteer');
global._ = require('lodash');

const getIpInfoMiddleware = function (req, res, next) {
  const xForwardedFor = (req.headers['x-forwarded-for'] || '').replace(/:\d+$/, '');
  const ip = xForwardedFor || req.connection.remoteAddress;
  req.ipInfo = ip;
  next();
}

app.use(getIpInfoMiddleware);
// parse application/json
app.use(bodyParser.json({ limit: '50mb', extended: true }));
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
// only for dev, will remove this in prod.
app.use(cors());

app.set('view engine', 'html');
app.set('views', path.join(__dirname, "views"));


//set assets paths
app.use('/assets', express.static('assets'));
app.use('/default', express.static('default'));
app.use('/public', express.static('public'));
app.use('/subscription/view', express.static('assets/subscription'));
app.use('/assets/sideLetters', express.static('assets/sideLetters'));
app.use('/assets/temp',  express.static('assets/temp'));
app.use('/assets/offlineUserUploads/fund', express.static('assets/offlineUserUploads/fund'));
app.use('/assets/offlineUserUploads/subscription', express.static('assets/offlineUserUploads/subscription'));
app.use('/assets/offlineUserUploads/signatures', express.static('assets/offlineUserUploads/signatures'));

// set locales app level variables
app.locals.clientURL = config.get('clientURL');

const logSkip = [
  '/api/v1/login',
  '/api/v1/changePassword'
];
morganBody(app, {
  prettify: true,
  skip: (req, res) => logSkip.includes(req.originalUrl),
  stream: {
    write: message => logger.log('verbose', message.trim()),
  },
});


// import all routes
const routes = require('./routes/index');
app.get('/lang/:lang/:file', (req, res, next) => {
  let contents = fs.readFileSync(`./lang/${req.params.lang}/${req.params.file}`, 'utf8');
  contents = contents ? JSON.parse(contents) : []
  return res.json(contents);
});
app.use('/api/v1/', routes);


// app.use(function (err, req, res, next) {
//   if (err.code === 'permission_denied') {
//     return res.status(403).json({ message: 'Forbidden' });
//   } else {
//     logger.error(err.stack)
//     //return res.status(500).send({ error: 'Something failed!' }) // prod 
//     return res.status(500).json({ message: err.message });
//   }
// });

http.listen(config.get('serverPort'), "0.0.0.0", async () => {
  const browser = await Puppeteer.launch({
    headless: true,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox'
    ]
  });
  process.browser = browser;
  console.log('Server up.');
});