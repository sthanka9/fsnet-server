
const config = require('../config/index')
const _ = require('lodash');
const fs = require('fs')
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const path = require('path');
const Handlebars = require('handlebars')
const moment = require('moment-timezone');
const merge = require('easy-pdf-merge');
const models = require('../models')
const os = require('os');

module.exports.getFileInfo = async (fileInfo, stringify = true) => {

    if (!fileInfo) return '';

    const uri = fileInfo.destination.replace('./', '/') + fileInfo.filename;
    const filePath = fileInfo.destination + fileInfo.filename;

    const fileExt = path.extname(fileInfo.filename);

    const data = {
        originalname: fileInfo.originalname,
        name: fileInfo.filename,
        size: fileInfo.size,
        uri: uri,
        path: filePath
    };

    return stringify ? JSON.stringify(data) : data;
}

Handlebars.registerHelper('ifCond', function (v1, v2, options) {
    if (v1 == v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('includes', function (elem, list, options) {
    if (typeof (list) == 'object' && list.includes(elem)) {
        return options.fn(this);
    }
    return options.inverse(this);
});


Handlebars.registerHelper("increment", function (positionCounter, options) {
    return positionCounter + 13;
});


Handlebars.registerHelper('compare', function (lvalue, rvalue, options) {

    if (arguments.length < 3)
        throw new Error("Handlerbars Helper 'compare' needs 2 parameters");

    var operator = options.hash.operator || "==";

    var operators = {
        '==': function (l, r) { return l == r; },
        '===': function (l, r) { return l === r; },
        '!=': function (l, r) { return l != r; },
        '<': function (l, r) { return l < r; },
        '>': function (l, r) { return l > r; },
        '<=': function (l, r) { return l <= r; },
        '>=': function (l, r) { return l >= r; },
        'typeof': function (l, r) { return typeof l == r; }
    }

    if (!operators[operator])
        throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);

    var result = rvalue != '' ? operators[operator](lvalue, rvalue) : (lvalue === rvalue) ;

    if (result) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }

});

module.exports.makePDF = async (content, data, filePath, parentTemplate = false) => {

    let template = Handlebars.compile(content)
    let html = template(data);

    if (parentTemplate) {
        content = fs.readFileSync(parentTemplate);
        content = content.toString();
        template = Handlebars.compile(content);
        html = template({ html: html });
    }

    const DELTA = 0.27; // closest additional mm to not break layout

    const page = await process.browser.newPage();

    await page.setContent(html);

    return page.pdf({
        path: filePath,
        format: 'A4',
        displayHeaderFooter: false,
        printBackground: false,
        margin: {
            top: 100,
            right: 140,
            bottom: 100,
            left: 140,
        },
    })
}



module.exports.ensureDirectoryExistence = ensureDirectoryExistenceFnc;

function ensureDirectoryExistenceFnc(filePath) {
    let dirname;
    if (path.extname(filePath)) {
        dirname = path.dirname(filePath);
    } else {
        dirname = filePath;
    }
    if (fs.existsSync(dirname)) {
        return true;
    }
    fs.mkdirSync(dirname,{ recursive: true});
}


module.exports.generateToken = (email) => {
    return Math.floor(100000 + Math.random() * 900000);
}

module.exports.errorHandle = (error) => {
    console.log(error)
}


module.exports.base64Encode = (data) => {
    if (data) {
        let buff = Buffer.from(data);
        return buff.toString('base64');
    }
}

module.exports.base64Decode = (data) => {
    if (data) {
        let buff = Buffer.from(data, 'base64');
        return buff.toString('ascii');
    }
}

let mergePdfs = (pdfFiles, outputFile) => {
    return new Promise(
        (resolve, reject) => {
            merge(pdfFiles, outputFile, function (error) {
                if (error) reject(error);
                resolve(true);
            });
        }
    );
};

module.exports.pdfMerge = async (pdfFiles, outputFile) => {
    try {
        if (os.platform() == "win32") {
            return await mergePdfs(pdfFiles, outputFile);
        } else {
            pdfFiles = pdfFiles.join(' ')
            await exec(`gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=${outputFile} -dBATCH ${pdfFiles}`)
        }
    } catch (error) {
        console.log(error)
    }
}

module.exports.setDateByTimezone = async (date, timezone) => {
    if (date && timezone) {
        return moment(date).tz(timezone).format('YYYY-MM-DD HH:mm:ss');
    } else {
        // current date & time
        return moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }
}


module.exports.randomString = (len, charSet) => {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@!$';
    let randomString = '';
    for (let i = 0; i < len; i++) {
        let randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString + '@';
}

module.exports.getFullName = (user) => {
    return (user && user.middleName) ? `${user.firstName} ${user.middleName} ${user.lastName}`
        : `${user.firstName} ${user.lastName}`
}

module.exports.getDateAndTimeFormat = (date) => {
    return moment(date).format('MM/DD/YYYY h:mm a ')
}

//get local url
module.exports.getLocalUrl = (url, fromTemp = false) => {
    if (!url) return null;
    if(fromTemp) {
        url = `assets/temp/${path.basename(url)}`
    }
    url = url.replace('./','');
    const assetsUrl = config.get('assetsURL');
    return assetsUrl + "/" + url;
}

//get local server url for signature pic
module.exports.getSignaturePicUrl = (url) => {
    if (!url) return null;    
    url = url.replace('./','');
    const assetsUrl = config.get('localServerURL');
    return assetsUrl + "/" + url;
}


module.exports.getLookThroughValues = async(subscription,getQualifiedPurchaseArray,percentageOfInvestment) => {
   
    // let percentageOfInvestment = (subscription.lpCapitalCommitment / totalCapitalClosedOrHypotheticalCommitment) * 100

    //If percentage is greater than 10%
    if(Math.round(percentageOfInvestment) > 10){
        //If companites act is true, then check qualified purchase condition to show '3c(1)'( Any one purchaser is false)
        return ([2,3].includes(Number(subscription.companiesAct)) && getQualifiedPurchaseArray.includes(false)) ? 'Yes' : 'No';
    }else{
        return 'No';
    }
}
module.exports.getInvestorName = (subscription)=>{
    return ((subscription.investorType == 'LLC' ? subscription.entityName :
    (subscription.investorType == 'Trust' ? subscription.trustName : 
    (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null)))  
    || (subscription.organizationName ? subscription.organizationName : subscription.lp.organizationName) ||
    this.getFullName(subscription.lp))
}



module.exports.checkIfGPRegistered= async (email)=>{

    const userAccountGP= await models.User.findOne({
      where:{
        email,
        accountType:'GP'
      }
    });
    if(userAccountGP){
      const isEmailRegistered = await models.Account.findOne({
        where:{
          email,
          isEmailConfirmed:1
        }
      });
  
      return isEmailRegistered ? true: false
    }else{
      return true;
    }
  
  
  }

module.exports.setDateUtcToTimezone = async (date, timezone) => {
    if(date && timezone) {
        return moment.utc(date).tz(timezone).format('MM-DD-YYYY HH:mm:ss');
    } else {
        return date;
    }
}

module.exports.setDateUtcToOffset = async (date, offset) => {
    if(date && offset) {
        //return moment(date).utcOffset(offset).format('MM-DD-YYYY HH:mm:ss');
        let gmtOffset = offset.replace('GMT-','-0');
        gmtOffset = gmtOffset.replace('GMT+','+0');
        return moment(date).utcOffset(gmtOffset).format('MM-DD-YYYY HH:mm:ss');
    } else {
        return date;
    }
}

module.exports.setOnlyDateUtcToOffset = async (date, offset) => {
    if(date && offset) {
        let gmtOffset = offset.replace('GMT-','-0');
        gmtOffset = gmtOffset.replace('GMT+','+0');
        return moment(date).utcOffset(gmtOffset).format('MM-DD-YYYY');
    } else {
        return date;
    }
};

module.exports.getDateAndTime = (date) => {
    return moment(date).format('MM-DD-YYYY HH:mm:ss ');
};

module.exports.getDate = () => {
    return moment.utc().format('MM-DD-YYYY');
};
module.exports.getDateTime = () => {
    return moment.utc().format('MM-DD-YYYY HH:mm:ss');
};

module.exports.getDateTimeWithTimezone = (timeZone, date, is_datetime, with_timezone = 1, ampm=0, dateFormat = 1) => {
    let date_time;
    if(timeZone && timeZone.code && date) {

        let gmtOffset = timeZone.code;
        gmtOffset = gmtOffset.replace('GMT-','-0');
        gmtOffset = gmtOffset.replace('GMT+','+0');

        if(gmtOffset == 'GMT'){
            gmtOffset = '+00:00';
        }

        if(!dateFormat){
            return date_time = moment(date).utcOffset(gmtOffset).format();
        }
        else if(is_datetime) {
            if(ampm){
                date_time = moment(date).utcOffset(gmtOffset).format('MM/DD/YYYY hh:mm A');
            } else {
                date_time = moment(date).utcOffset(gmtOffset).format('MM-DD-YYYY HH:mm:ss');
            }
        } else {
            date_time = moment(date).utcOffset(gmtOffset).format('MM/D/YYYY');
        }

        if(with_timezone) {
            return date_time + ` (` + timeZone.displayName + `)`;
        } else {
            return date_time;
        }
    } else {
        if(!dateFormat){
            return moment(date).format();
        }
        else if(is_datetime) {
            return moment(date).format('MM-DD-YYYY HH:mm:ss');
        } else {
            return moment(date).format('MM/D/YYYY');
        }
    }
};


module.exports.getInvestorName = (subscription)=>{
    return ((subscription.investorType == 'LLC' ? subscription.entityName :
    (subscription.investorType == 'Trust' ? subscription.trustName : 
    (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null)))
    || (subscription.organizationName ? subscription.organizationName : subscription.lp.organizationName)
    || this.getFullName(subscription.lp))

};

module.exports.getIndividualContent = (subscription,fundCommonName)=>{

    return (subscription.areYouSubscribingAsJointIndividual || subscription.investorType == 'LLC' || subscription.investorType == 'Trust' ) ? `In respect of the investment in ${fundCommonName} by ${this.getInvestorName(subscription) }` : `In respect of your investment in ${fundCommonName}`

};

module.exports.getMomentUTCDate = () => {
    return moment.utc().format();
};

//check notifications enabled for gp
module.exports.isNotificationsEnabledForFundGP= async (fundId)=>{

    let fundInfo =  await models.Fund.findOne({
    attributes:['isNotificationsEnabled'],
      where:{
        id:fundId
      }
    })
    
    return  (fundInfo && fundInfo.isNotificationsEnabled) ? true : false  
}
