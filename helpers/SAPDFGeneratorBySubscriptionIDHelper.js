


const _ = require('lodash');
const SAPDFGeneratorHelper = require('../helpers/SAPDFGeneratorHelper');
const models = require('../models/index');

module.exports = async (subscriptionIdOrSubscription, subscriptionAgreementPath = false) => {

    let subscription = Number(subscriptionIdOrSubscription) ? await getSubscription(subscriptionIdOrSubscription) : subscriptionIdOrSubscription;

    subscription = subscription?subscription.toJSON():subscription

    let subscriptionStepsData
        = (subscription && subscription.subscriptionAgreementPath) ? 
        subscription.subscriptionAgreementPath : 
        (subscription.fund && subscription.fund.subscriptionAgreementPath ? subscription.fund.subscriptionAgreementPath :null);
    

    const investorQuestions = subscription.fund.investorQuestions;
    let questionResult=[];
    if(investorQuestions){

        questionResult = investorQuestions.map(function (element) {
            let questionResponse = (element.answers) ? element.answers.questionResponse : null;
            return {
                id: element.id,
                fundId: element.fundId,
                questionTitle: element.questionTitle,
                question: element.question,
                typeOfQuestion: element.typeOfQuestion,
                isRequired: element.isRequired,
                deletedAt: element.deletedAt,
                createdAt: element.createdAt,
                updatedAt: element.updatedAt,
                questionResponse: questionResponse
            }
        });
    }

    const data = {
        fundManagerTitle: subscription.fund.fundManagerTitle,
        gpName: `${subscription.fund.gp.firstName} ${subscription.fund.gp.lastName}`,
        fundManagerLegalEntityName: subscription.fund.fundManagerLegalEntityName,
        fundLegalEntityName: subscription.fund.legalEntity,
        legalTitle: subscription.legalTitle,
        fundLegalEntity: subscription.fund.legalEntity,
        areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
        fundTopLegalName: _.toUpper(subscription.fund.legalEntity),
        investorType: subscription.investorType,
        investorSubType: subscription.investorSubType,
        noOfGrantors: subscription.numberOfGrantorsPrettyForPDF,
        entityInvestor: subscription.titlePrettyForPDF,
        lpName: `${subscription.lp.firstName} ${subscription.lp.lastName}`,
        entityTitle: subscription.entityTitle,
        legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
        fundManagerCommonName: subscription.fund.fundManagerCommonName,
        fundManagerInfo: subscription.fundManagerInfo,
        otherInvestorSubType: subscription.otherInvestorSubType,
        otherInvestorAttributes: subscription.otherInvestorAttributes,
        lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
        isSubjectToDisqualifyingEvent: subscription.isSubjectToDisqualifyingEvent,
        releaseInvestmentEntityRequired: subscription.releaseInvestmentEntityRequired,
        benefitPlanInvestor: subscription.benefitPlanInvestor,
        planAsDefinedInSection4975e1: subscription.planAsDefinedInSection4975e1,
        employeeBenefitPlan: subscription.employeeBenefitPlan,
        numberOfDirectEquityOwners: subscription.numberOfDirectEquityOwners,
        beneficialInvestmentMadeByTheEntity: subscription.beneficialInvestmentMadeByTheEntity,
        partnershipWillNotConstituteMoreThanFortyPercent: subscription.partnershipWillNotConstituteMoreThanFortyPercent,
        anyOtherInvestorInTheFund: subscription.anyOtherInvestorInTheFund,
        entityProposingAcquiringInvestment: subscription.entityProposingAcquiringInvestment,
        entityHasMadeInvestmentsPriorToThedate: subscription.entityHasMadeInvestmentsPriorToThedate,
        companiesAct: subscription.companiesAct,
        areYouQualifiedClient: subscription.areYouQualifiedClient,
        areYouQualifiedPurchaser: subscription.areYouQualifiedPurchaser,
        areYouAccreditedInvestor: subscription.areYouAccreditedInvestor,
        totalValueOfEquityInterests: subscription.totalValueOfEquityInterests,
        investorQuestions: questionResult,
        typeOfLegalOwnership: subscription.areYouSubscribingAsJointIndividual?subscription.typeOfLegalOwnership:'',
        indirectBeneficialOwnersSubjectFOIAStatutes: subscription.indirectBeneficialOwnersSubjectFOIAStatutes?subscription.indirectBeneficialOwnersSubjectFOIAStatutes:''

    };

    return await SAPDFGeneratorHelper(subscriptionStepsData,data, subscriptionAgreementPath);

    

}


const getSubscription = async(subscriptionId) => {
    return await models.FundSubscription.getSubscription(subscriptionId, models);
}