const config = require('../config/index')
const _ = require('lodash')  
const models = require('../models')
const { Op } = require('sequelize');
const commonHelper = require('../helpers/commonHelper');

//lp pending actions
module.exports.getLPPendingActions = async(req,res,fundSubscription)=>{

    let subscriptionId = fundSubscription.id

    let whereFilter = {
        subscriptionId: subscriptionId,
        isActive: 1,
        isArchived:0,
        isPrimaryLpSigned: false
    }



    console.log("fundSubscription***********changedpagesid*******",fundSubscription.changedPagesId)

    // if lp signatories available then true otherwise false
    const signatoriesCheck = (fundSubscription.noOfSignaturesRequired > 0) ? true : false;

    let accountType = req.user.accountType;

    if (accountType == 'LP') {
        whereFilter.isAllLpSignatoriesSigned = signatoriesCheck;
    }

    if (accountType == 'SecondaryLP') {             
        const signatureTrack = await models.SignatureTrack.findAll({
            attributes: ['documentId'],
            where: {
                signedUserId: req.userId
            }
        });
        const documentIds = _.map(signatureTrack, 'documentId');
        whereFilter.Id = {
            [Op.notIn]: documentIds
        }
    } 


    const documentsForSignature = await models.DocumentsForSignature.findAll({
        where: whereFilter,
        include: [{
            model: models.FundSubscription,
            as: 'subscription',
            required: true,
            include: [{
                attributes: ['timezone'],
                model: models.Fund,
                as: 'fund',
                required: false,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }]
            }]
        }]
    }) 

    let docs = []
    let changedPages = false
    let changedPagesUrl = ''

    console.log("***8documentsForSignature******",documentsForSignature.length)

    
    for (let documentForSignature of documentsForSignature) {
        const documentPath = commonHelper.getLocalUrl(documentForSignature.filePath)  
            
            if(documentForSignature.changedPagesId && documentForSignature.changedPagesId!=null && documentForSignature.changedPagesId>0){
                changedPages = true
                changedPagesUrl = `${config.get('serverURL')}/api/v1/document/view/viewcp/${documentForSignature.changedPagesId}`
            }

            if(fundSubscription.changedPagesId && fundSubscription.changedPagesId!=null && fundSubscription.changedPagesId>0){
                changedPages = true
                changedPagesUrl = `${config.get('serverURL')}/api/v1/document/view/viewcp/${fundSubscription.changedPagesId}`
            }                

            if(documentForSignature.docType=='AMENDMENT_AGREEMENT' || documentForSignature.docType=='SIDE_LETTER_AGREEMENT' || documentForSignature.docType=='CHANGE_COMMITMENT_AGREEMENT')
                changedPages = false


    
            //date conversion based on fund timezone
            let timezone_date = ''; 
          
            if(documentForSignature.subscription && documentForSignature.subscription.fund.timeZone && documentForSignature.subscription.fund.timeZone.code) {
                timezone_date = await commonHelper.setDateUtcToOffset(documentForSignature.createdAt, documentForSignature.subscription.fund.timeZone.code).then(function (value) {
                    return value;
                });
                timezone = documentForSignature.subscription.fund.timeZone.displayName;
            } else {
                timezone_date = await commonHelper.getDateAndTime(documentForSignature.createdAt);
            }     

            docs.push({
                documentType: documentForSignature.docType,
                createdAt: timezone_date,
                documentUrl: documentPath,
                documentId: documentForSignature.id,
                isAdditionalQuestionAdded: documentForSignature.subscription.isAdditionalQuestionAdded ? true : false,
                isSubscriptionContentUpdated: documentForSignature.subscription.isSubscriptionContentUpdated ? true : false,
                status:fundSubscription.status,
                changedPages:changedPages,
                changedPagesUrl:changedPagesUrl,      
                fundId:fundSubscription.fundId
            })            
        
    }

    console.log("docs before******************",docs)

    let totalDocs = docs.length

    if (accountType == 'LP' || accountType == 'SecondaryLP' ) {            
        let saDocument = _.find(docs,{documentType:'SUBSCRIPTION_AGREEMENT'})
        let faDocument = _.find(docs,{documentType:'FUND_AGREEMENT'})
        let slDocument = _.find(docs,{documentType:'SIDE_LETTER_AGREEMENT'})                    
        let atDocument = _.find(docs,{documentType:'AMENDMENT_AGREEMENT'})  

        //if lp inprogress remove pending item of side letter
        if(fundSubscription.status==2 || fundSubscription.status==16){                 
            let atDocumentAll = _.findLast(docs,{documentType: 'AMENDMENT_AGREEMENT'})
            //check user previously closed ready and status to inprogress from closed ready
            let preSignedDocs = await models.SignatureTrack.findAll({
                attributes:['id'],
                where:{
                    subscriptionId:subscriptionId,
                    documentType:'FUND_AGREEMENT'
                }                         
            })                
            if(preSignedDocs.length==0){ //direct in progress user
                _.remove(docs,{documentType:'SIDE_LETTER_AGREEMENT'})
                _.remove(docs,{documentType:'AMENDMENT_AGREEMENT'})
                _.remove(docs,{documentType:'FUND_AGREEMENT'})                    
            } else if(preSignedDocs.length>0){  //closed-ready to in-progress user

                if(totalDocs == 1 && (faDocument || slDocument  || atDocumentAll ) ){
                } else if(totalDocs== 2 && saDocument && faDocument){
                    _.remove(docs,{documentType:'FUND_AGREEMENT'})                             
                } else if(totalDocs== 2 && slDocument && atDocumentAll){
                    docs = []
                    docs.push({
                        documentType: 'COMMON_AGREEMENT',
                        name : 'Proceed to sign Side Letter and Amendment to the Fund Agreement',
                        createdAt : slDocument.createdAt,
                        status:slDocument.status
                    })                        

                } else if(totalDocs== 2 && faDocument && atDocumentAll){
                    docs = []
                    docs.push({
                        documentType: 'COMMON_AGREEMENT',
                        name : 'Proceed to sign Fund Agreement and Amendment to the Fund Agreement',
                        createdAt : faDocument.createdAt,
                        status:faDocument.status
                    })                        

                } else if(totalDocs== 2 && faDocument && slDocument){
                    docs = []
                    docs.push({
                        documentType: 'COMMON_AGREEMENT',
                        name : 'Proceed to sign Fund Agreement and Side Letter',
                        createdAt : faDocument.createdAt,
                        status:faDocument.status
                    })                        

                } else if(totalDocs== 3 && faDocument && slDocument && saDocument){
                    docs = [] 
                    docs.push({
                        documentType: 'COMMON_AGREEMENT',
                        name : 'Proceed to sign',
                        createdAt : faDocument.createdAt,
                        status:faDocument.status
                    })                        

                } else if(totalDocs>=3 && faDocument && atDocument && saDocument){
                    docs = [] 
                    docs.push(saDocument)                  

                }else if(totalDocs>=3 && faDocument && atDocument && !saDocument){
                    docs = [] 
                    docs.push({
                    documentType: 'COMMON_AGREEMENT',
                    name : 'Proceed to sign Fund Agreement and Amendment to the Fund Agreement',
                    createdAt : faDocument.createdAt,
                    status:faDocument.status   
                    })            

                }else if(totalDocs> 1  && atDocumentAll && !faDocument && !saDocument && !slDocument){
                    docs = []
                    docs.push({
                        documentType: 'COMMON_AGREEMENT',
                        name : 'Proceed to sign Amendment to the Fund Agreement',
                        createdAt : atDocumentAll.createdAt,
                        status:atDocumentAll.status
                    })                       
                }
                 else{

                }
            }
        } 
        
        console.log("docs after conditions******************",docs)

        //check all signatories signed, then only display the pending docs
        if(accountType == 'LP'){

            let isLPSignatoriesSigned = await models.DocumentsForSignature.count({
                where: {                        
                    subscriptionId: subscriptionId,  
                    isActive: 1,    
                    deletedAt:null,
                    isAllLpSignatoriesSigned:0,
                    docType: {
                        [Op.ne] : 'SUBSCRIPTION_AGREEMENT'            
                    }
                }
            })
            
            if(isLPSignatoriesSigned>0 && signatoriesCheck && fundSubscription.status!=10){
                docs = [] 
            }
        }

    }
     
    console.log("docs last******************",docs) 

    //if user in invited status , then pending action count is 0
    docs =  fundSubscription.status==1 ? [] : docs

    return docs
}