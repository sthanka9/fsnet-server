const models = require("../models");
const config = require("../config");
const jwt = require("jsonwebtoken");
const _ = require('lodash');
const redis = require('redis')
const uuidv1 = require('uuid/v1') 
const moment = require('moment');


module.exports.validate = async function (input) {

  const user = await models.User.findOne({
    where: {
      id: input.id,
      deletedAt: null
    },
    include: [{
      model: models.Role,
      as: 'roles',
      required: true,
      include: [{
        model: models.Permission,
        as: 'permissions',
        required: true
      }]
    }]
  });

  let permissions = _.map(user.roles, 'permissions');
  permissions = _.flattenDeep(permissions);
  permissions = _.map(permissions, 'slug');

  return {
        firstName: user.firstName,
        lastName: user.lastName,
        accountType: user.accountType,
        id: user.id,
        accountid:input.accountid,
        email: user.email,
        permissions: permissions,
        middleName: user.middleName
      }
    


};

module.exports.createAuthToken = function (user) {  
  let permissions = _.map(user.roles, 'permissions');
  permissions = _.flattenDeep(permissions);
  permissions = _.map(permissions, 'slug');
  
  let client = redis.createClient(config.get('redis').port,config.get('redis').host)
  let uuid = uuidv1() 
  let timeStamp = moment().format("DD/MM/YYYY HH:mm:ss") 
  client.setex(uuid, config.get('session_login_uuid'),timeStamp) 

  return jwt.sign(
    {
      user: {
        firstName: user.firstName,
        lastName: user.lastName,
        accountType: user.accountType,
        id: user.id,
        uuid:uuid,
        email: user.email,
        permissions: permissions,
        middleName: user.middleName,
        accountid:user.accountId
      }
    },
    config.get("jwt").privatekey,
    {
      algorithm: "HS256"
    });
}