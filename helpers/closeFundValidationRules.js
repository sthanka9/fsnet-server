

const models = require('../models/index');
const { Op } = require('sequelize');
const emailHelper = require("../helpers/emailHelper");
const commonHelper = require("../helpers/commonHelper");
const config = require('../config/index');
const _ = require('lodash');

async function triggerEmailToGpAndDelegates(emailsContent, fund, investorCount) {
    try {
        let gpDelegatesIDs = await models.GpDelegates.findAll({
            attributes: ['delegateId','gpId'],
            where: {
                fundId: fund.id,
                gpId: fund.gpId
            }
        })

        
        gpDelegatesIDs = _.map(gpDelegatesIDs,'delegateId')
        //gpDelegatesIDs.push(_.map(gpDelegatesIDs,'gpId'))

        let whereCondition = {}

        if(fund.isNotificationsEnabled){
            whereCondition = {
                id: {
                    [Op.or]: {
                        [Op.in]: gpDelegatesIDs,
                        [Op.eq]: fund.gpId
                    }

                },
                isEmailConfirmed:1
            }
        } else {
            whereCondition = {
                id: {
                    [Op.or]: {
                        [Op.in]: gpDelegatesIDs 
                    }

                },
                isEmailConfirmed:1
            }
        }

        const gpDelegates = await models.User.findAll({
            where: whereCondition
        })
        
        let html = ''
        if (emailsContent.INVESTORS_COUNT_EXCESS_99) {
            html += `<p>Please note that the documents in hand for <a href="${config.get('clientURL')}/fund/view/${fund.id}">${fund.legalEntity}</a> in status close-ready and closed would create a situation if closed upon, in that the Fund would be limited to 99 investor slots under Section 3(c)(1) of the Companies Act, and the current investor count if all investors are closed upon would be ${investorCount}.  Please consult your legal adviser.</p>`
        } 
        if (emailsContent.INVESTORS_COUNT_EXCESS_499) {
            html += `<p>Please note that the documents in hand for <a href="${config.get('clientURL')}/fund/view/${fund.id}">${fund.legalEntity}</a>  in status close-ready and closed would create a situation if closed upon, in that the Fund would be limited to 499 investor slots under Section 3(c)(7) of the Companies Act, and the current investor count if all investors are closed upon would be ${investorCount}.</p>`
        } 
        if (emailsContent.ERISA_WARNINGS) {
            html += `<p>Please note that the documents in hand for <a href="${config.get('clientURL')}/fund/view/${fund.id}">${fund.legalEntity}</a>  in status close-ready and closed would create a situation if closed upon, in that the Fund would have over 25% ERISA Benefit Plan money and would become subject to regulation as a Fund holding Plan Assets unless the Fund operates as a venture capital operating company.  Please consult your legal adviser.</p>`
        } 
        if (emailsContent.DISQUALIFYING_EVENT) {
            html += `<p>Please note that the documents in hand for <a href="${config.get('clientURL')}/fund/view/${fund.id}">${fund.legalEntity}</a>  in status close-ready and closed indicate that one or more investors has had a Disqualifying Event.  Please consult your legal adviser.</p>`
        } 
        if (emailsContent.ACCREDITED_EVENT) {
            html += `<p>Please note that the documents in hand for <a href="${config.get('clientURL')}/fund/view/${fund.id}">${fund.legalEntity}</a> in status close-ready and closed would create a situation if closed upon, in that the Fund would have a <b>non-accredited investor</b>. <u><b>This is a serious situation which is almost never advisable.</b></u> Please consult your legal adviser.</p>`
        }
        if(emailsContent.EQUITY_OWNERS_EVENT){
            html += `<p>${emailsContent.fundInvestorNames} has indicated that they are under common control with another Investor which they designated as ${emailsContent.numberOfexistingOrProspectives}.  Please note that if these investors add up collectively to 10% or more of total Investor interests at the applicable closing, you should carefully review with your counsel the 1940 Act Investor Count as Vanilla will not automatically count them as related and therefore the 1940 Investor Act Count may be higher than indicated by Vanilla.  Please discuss this issue with your legal counsel if applicable.</p>`

        }


        if(html !== '') {

            for (let gpDelegate of gpDelegates) {
                emailHelper.sendEmail({
                    toAddress: gpDelegate.email,
                    subject: 'Fund Closing Validation Alert',
                    data: { 
                        fund: fund, 
                        fundManagerName: commonHelper.getFullName(gpDelegate),  
                        user: gpDelegate, 
                        html: html
                    },
                    htmlPath: "fundClosingWarnnings.html"
                })
            }

        }
    } catch(e) {
        console.log('suspected issue....');
        console.log(e);
    }

}


module.exports = async(fund) => {



    let emailsContent = {}

    //get close ready or closed status investors that are NOT qualified purchasers.
    const fundInvestorsToValidate = await models.FundSubscription.findAll({
        where: {
            fundId: fund.id,
            status: {
                [Op.in]: [10, 7] // closed and close ready.
            }
        },
        include:[{
            model:models.User,
            as:'lp',
            required:true
        },{
            attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
            model: models.FundClosings,
            required: false,
            as: 'fundClosingInfo',
            where: {
                isActive: 1
            }
        }]
    });

    //If atleast one fund has 'not' QualifiedPurchaser then return notQualified purchase
    //default true
    const getQualifiedPurchaseArray = _.map(fundInvestorsToValidate, 'areYouQualifiedPurchaser');

    let totalCapitalClosedOrHypotheticalCommitment = 0;
    fundInvestorsToValidate.map(closedInvetsor=>{
        //If fund is closed then take gpconsumed amount
        if(closedInvetsor.status == 10){
            totalCapitalClosedOrHypotheticalCommitment = totalCapitalClosedOrHypotheticalCommitment + _.last(closedInvetsor.fundClosingInfo).gpConsumed;
        }else{
            totalCapitalClosedOrHypotheticalCommitment = totalCapitalClosedOrHypotheticalCommitment + closedInvetsor.lpCapitalCommitment
        }
    });


    let investorCountArray = [];
    // let investorCount = _.sumBy(closeAndCloseReadySubscriptions, 'investorCount');
    for(let subscription of fundInvestorsToValidate){

        let gpConsumed=0;
        if (_.last(subscription.fundClosingInfo)) {
            gpConsumed = _.last(subscription.fundClosingInfo).gpConsumed
        }

        //Fund percentage
        let fundPercentage = totalCapitalClosedOrHypotheticalCommitment > 0 ?
            parseFloat(((gpConsumed ? gpConsumed: subscription.lpCapitalCommitment) / totalCapitalClosedOrHypotheticalCommitment) * 100).toFixed(2) : 0;
       

        //Look through issues
        if(subscription.lookThrough){
            lookThroughIssuesFlag = await commonHelper.getLookThroughValues(subscription,getQualifiedPurchaseArray,fundPercentage);

            //Default investorCount is 1
            const investorCountValue = (lookThroughIssuesFlag == 'Yes') ? subscription.numberOfDirectEquityOwners : (subscription.investorCount ?subscription.investorCount:1)
        
            investorCountArray.push( investorCountValue );
        }else{
            investorCountArray.push(subscription.numberOfDirectEquityOwners);
        }
    }

    let investorCount = _.sum(investorCountArray);

    // let investorCount = _.sumBy(fundInvestorsToValidate, 'investorCount');

    const fundInvestorsToValidate99 = _.filter(fundInvestorsToValidate, {
        areYouQualifiedPurchaser: false,
        areYouAccreditedInvestor: true
    })


    const fundInvestorsToValidate499 = _.filter(fundInvestorsToValidate, {
        areYouQualifiedPurchaser: true
    })

    const equityOwnerQuestions = _.filter(fundInvestorsToValidate,{
        existingOrProspectiveInvestorsOfTheFund:true
    })


    if (fundInvestorsToValidate499 && fundInvestorsToValidate499.length > 0 &&  fundInvestorsToValidate.length == fundInvestorsToValidate499.length && investorCount > 499) {
        emailsContent.INVESTORS_COUNT_EXCESS_499 = true
    }

    if (fundInvestorsToValidate99 && fundInvestorsToValidate99.length > 0 && investorCount > 99) {
        emailsContent.INVESTORS_COUNT_EXCESS_99 = true
    }

    //ERISA – TESTED ONGOING AND AT CLOSING
    const erisaFundSubscriptionsToValidate = _.filter(fundInvestorsToValidate, function (data) {
        return (!data.employeeBenefitPlan && !data.planAsDefinedInSection4975e1 && !data.benefitPlanInvestor ? '0' : (!data.employeeBenefitPlan && !data.planAsDefinedInSection4975e1 ? `${data.totalValueOfEquityInterests ? data.totalValueOfEquityInterests : '0'}` : '100')) > 20
    });

    if (erisaFundSubscriptionsToValidate && erisaFundSubscriptionsToValidate.length > 0) {
        emailsContent.ERISA_WARNINGS = true
    }

    //Disqualifying Event – TESTED ONGOING AND AT CLOSING
    const disqualifyingEvent = _.filter(fundInvestorsToValidate, {
        isSubjectToDisqualifyingEvent: true
    })


    if (disqualifyingEvent && disqualifyingEvent.length > 0) {
        emailsContent.DISQUALIFYING_EVENT = true
    }

    // Accredited Investor {data.areYouAccreditedInvestor ? 'Yes' : 'No'} 
    const accreditedInvestorToValidate = _.filter(fundInvestorsToValidate, {
        areYouAccreditedInvestor: false
    })

    if (accreditedInvestorToValidate && accreditedInvestorToValidate.length > 0) {
        emailsContent.ACCREDITED_EVENT = true
    }


    if(equityOwnerQuestions && equityOwnerQuestions.length){
        const investorNameArray=[]
        emailsContent.EQUITY_OWNERS_EVENT = true
        emailsContent.numberOfexistingOrProspectives = _.filter(_.uniq(_.map(fundInvestorsToValidate,'numberOfexistingOrProspectives'))).join(', ')
        _.filter(fundInvestorsToValidate,investor=>{
            if(investor.existingOrProspectiveInvestorsOfTheFund ){
                investorNameArray.push(investor.investorType  == 'LLC' ? investor.entityName : 
                (investor.investorType == 'Trust' ? investor.trustName : null ))
                return investorNameArray
            }
        })

        emailsContent.fundInvestorNames = _.filter(investorNameArray).join(', ')

    }

    triggerEmailToGpAndDelegates(emailsContent, fund, investorCount)
}