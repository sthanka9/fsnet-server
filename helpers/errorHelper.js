module.exports.error_400 = (res, field, message, data={}) => {
    return res.status(400).json({
        "errors": [
            {
                "param": field,
                "msg": message,
                ...data
            }
        ]
    });
}

module.exports.error_403 = (res, field, message, data={}) => {
    return res.status(403).json({
        "errors": [
            {
                "param": field,
                "msg": message,
                ...data
            }
        ]
    });
}