module.exports = class CustomUnauthorizedError extends Error { 

    constructor(code, error) {

        super(error.message);

        this.name = this.constructor.name;

        Error.captureStackTrace(this, this.constructor);

        this.message = error.message
      
        this.code = error.code
        this.status = 406
        this.inner = error
    }
}
