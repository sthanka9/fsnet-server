const models = require('../models/index');
const { Op } = require('sequelize');
const config = require('../config/index');
const _ = require('lodash');
const moment = require('moment');
const fs = require('fs');
const uuidv1 = require('uuid/v1');
const path = require('path');
const util = require('util');
const commonHelper = require('../helpers/commonHelper');


const consolidatedFullRestatedFundAgreement = async (amendmentId, ammendmentInfo, user,partnershipDocumentPageCount) => {

    //create the new document with signed investors
    const fund = await models.Fund.findOne({
        include: [{
            model: models.User,
            as: 'gp',
            required: true
        }],
        where: { id: ammendmentInfo.fundId }
    })

    //get gp signature path
    let gpSignaturePic = commonHelper.getSignaturePicUrl(fund.gp.signaturePic);

    const gpSignatories = await models.GpSignatoryApproval.findAll({
        where: {
            amendmentId,
            fundId: ammendmentInfo.fundId
        },
        include: [{
            model: models.User,
            as: 'gpSignatory',
            required: false
        }]
    });


    //get all lp signatures
    const documentsForSignature = await models.DocumentsForSignature.findAll({
        where: {
            fundId: ammendmentInfo.fundId,
            docType: 'FUND_AGREEMENT',
            isPrimaryLpSigned: true,
            amendmentId
        },
        include: [{
            model: models.SignatureTrack,
            as: 'signatureTracks',
            include: [{
                model: models.User,
                as: 'signeduser',
                required: false
            }, {
                model: models.OfflineUsers,
                as: 'signedOfflineUser',
                required: false
            }],
            where: {
                signedByType: {
                    [Op.in]: ['LP', 'SecondaryLP', 'SecondaryGP', 'OfflineLP']
                },
                isActive: 1,
                documentType: 'FUND_AGREEMENT',
            },
            required: true
        }]
    });


    const subscriptionIds = _.map(documentsForSignature, 'subscriptionId');
    const documentIds = _.map(documentsForSignature, 'id');

    //Consider Gp signatories from approvals table not from tracks
    // const user = await models.User.findOne({ where: { id: req.userId } });
    // let gpSignaturePath = user.signaturePic;

     //UPdate isDocumentEffectuated flag when no closed investor has signed effectuated FA
     await models.DocumentsForSignature.update({
        isDocumentEffectuated: true
    }, {
        where: {
            fundId: ammendmentInfo.fundId,
            docType: 'CONSOLIDATED_FUND_AGREEMENT',
            isActive: 1,
            isArchived: 0,
            deletedAt: null
        }
    });

    if (subscriptionIds.length == 0) {

        return;
    }


    const subscriptions = await models.FundSubscription.findAll({
        include: [{
            model: models.User,
            as: 'lp',
            required: false
        }, {
            model: models.OfflineUsers,
            as: 'offlineLp',
            required: false
        }],
        where: {
            id: {
                [Op.in]: subscriptionIds
            }
        }
    });

    let lpSignedPages = [];
    let signedTrackIds = [];
    let signedUsers = [];
    let offlineSignedUsers = [];
    let gpSignatorySignedIds = [];

    const consolidateAmmendmentAgreement = await models.DocumentsForSignature.findOne({
        where: {
            fundId: ammendmentInfo.fundId,
            docType: 'CONSOLIDATED_FUND_AGREEMENT',
            isActive: 1,
            deletedAt: null,
            isArchived: 0,
            isDocumentEffectuated: false
            // amendmentId
        }
    });


    for (let gpSignatoryObj of gpSignatories) {

        if (gpSignatoryObj.isAddedToConsolidatedAmendmentAgreement !== 1) {

            let gpSignatory = _.find(gpSignatoryObj.gpSignatory, { id: gpSignatoryObj.signatoryId })
            const agreementSignData = {
                fundManagerTitle: fund.fundManagerTitle,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                fundLegalEntity: fund.legalEntity,
                fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                gpName: commonHelper.getFullName(gpSignatory),
                gpSignaturePic: commonHelper.getSignaturePicUrl(gpSignatory.signaturePic.path ? gpSignatory.signaturePic.path : gpSignatory.signaturePic),
                date: moment().format('M/D/YYYY')
            };

            const htmlFilePath = "./views/fundAgreementConsolidatedGpSign.html";

            const signedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);
            lpSignedPages.push(signedLastPage.path);
            gpSignatorySignedIds.push(gpSignatoryObj.id);
        }
    }

    for (let documentForSignature of documentsForSignature) {

        const subscription = _.find(subscriptions, { id: documentForSignature.subscriptionId })

        for (let signature of documentForSignature.signatureTracks) {
            if (!signedUsers.includes(signature.signedUserId) && (!signature.isAddedToConsolidatedFundAgreement) && signature.signedByType !== 'OfflineLP') {

                if (signature.signedByType !== 'SecondaryGP') {

                    const htmlFilePath = "./views/fundAgreementConsolidatedLpSign.html";

                    const agreementSignData = {
                        name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName,
                        jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                        investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                        legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                        areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                        lpName: (signature && signature.signeduser) ? commonHelper.getFullName(signature.signeduser) : '',
                        date: moment().format('M/D/YYYY'),
                        lpSignaturePic: commonHelper.getSignaturePicUrl(signature.signaturePath),
                        lpCapitalCommitment: subscription.lpCapitalCommitmentPretty
                    }

                    const signedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);
                    lpSignedPages.push(signedLastPage.path);
                    signedTrackIds.push(signature.id);

                }

                if (signature.signedByType === 'SecondaryGP') {
                    const agreementSignData = {

                        gpName: commonHelper.getFullName(subscription.signature.signeduser),
                        fundManagerTitle: fund.fundManagerTitle,
                        fundManagerCommonName: fund.fundManagerCommonName,
                        fundCommonName: fund.fundCommonName,
                        fundLegalEntity: fund.legalEntity,
                        fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                        gpSignaturePic: commonHelper.getSignaturePicUrl(subscription.signature.signaturePath),
                        date: moment().format('M/D/YYYY')
                    };

                    const htmlFilePath = "./views/fundAgreementConsolidatedGpSign.html";

                    const signedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);
                    lpSignedPages.push(signedLastPage.path);
                    signedTrackIds.push(signature.id);
                }

            }

            if (!offlineSignedUsers.includes(signature.signedUserId) && (!signature.isAddedToConsolidatedFundAgreement)) {
                if (signature.signedByType == 'OfflineLP' && signature.signedOfflineUser) {
                    lpSignedPages.push(signature.signaturePath)
                    signedTrackIds.push(signature.id);
                }
            }



            signature.signedByType == 'OfflineLP' ?
                offlineSignedUsers.push(signature.signedUserId)
                : signedUsers.push(signature.signedUserId);
        }
    }

    if (lpSignedPages.length != 0) {

        let data = false;

        if (!consolidateAmmendmentAgreement) {

            const consolidateAmmendmentAgreementTempPath = ammendmentInfo.document.path;

            const agreementSignData = {
                gpName: commonHelper.getFullName(user),
                fundManagerTitle: fund.fundManagerTitle,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                fundLegalEntity: fund.legalEntity,
                fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                gpSignaturePic: gpSignaturePic,
                date: moment().format('M/D/YYYY')
            };

            const htmlFilePath = "./views/fundAgreementConsolidatedGpSign.html";

            const gpSignedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);

            data = [consolidateAmmendmentAgreementTempPath, gpSignedLastPage.path].concat(lpSignedPages);

        } else {
            const consolidateAmmendmentAgreementTempPath = consolidateAmmendmentAgreement.filePath;
            data = [consolidateAmmendmentAgreementTempPath].concat(lpSignedPages);
        }

        let filePathToSave = `./assets/funds/${ammendmentInfo.fundId}/CONSOLIDATED_FUND_AGREEMENT_${uuidv1()}.pdf`
        const fileName = `CONSOLIDATED_FUND_AGREEMENT_${uuidv1()}.pdf`;
        const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
        await commonHelper.pdfMerge(data, outputFile);
        fs.copyFileSync(outputFile, filePathToSave);

        if (consolidateAmmendmentAgreement) {

            //Update with new filepath with added signatures
            await models.DocumentsForSignature.update({
                filePath: filePathToSave,
            }, {
                where: {
                    fundId: ammendmentInfo.fundId,
                    docType: 'CONSOLIDATED_FUND_AGREEMENT',
                    isActive: 1,
                    isArchived: 0,
                    deletedAt: null
                }
            });

        } else {
            //Update existing consolidated FA to archive
            await models.DocumentsForSignature.update({
                isArchived:1,
                isActive:0
            }, {
                where: {
                    fundId: ammendmentInfo.fundId,
                    docType: 'CONSOLIDATED_FUND_AGREEMENT',
                    isActive: 1,
                    isArchived: 0,
                    deletedAt: null,
                    amendmentId: null,
                    isDocumentEffectuated: true
                }
            });
        }

        await models.DocumentsForSignature.findOrCreate({
            where: {
                fundId: ammendmentInfo.fundId,
                docType: 'CONSOLIDATED_FUND_AGREEMENT',
                isActive: 1,
                deletedAt: null,
                isArchived: 0,
                originalPartnershipDocumentPageCount:partnershipDocumentPageCount,
                isDocumentEffectuated:0
            },
            defaults: {
                fundId: ammendmentInfo.fundId,
                docType: 'CONSOLIDATED_FUND_AGREEMENT',
                filePath: filePathToSave,
                isPrimaryGpSigned: true,
                isPrimaryLpSigned: true,
                createdBy: user.id,
                updatedBy: user.id,
                isActive: 1,
                isArchived: 0,
                isDocumentEffectuated:0
            }
        });


        await models.DocumentsForSignature.update({
            isAddedToFundAgreement: 1,
            originalPartnershipDocumentPageCount:partnershipDocumentPageCount
        }, {
            where: {
                id: {
                    [Op.in]: documentIds
                }
            }
        });

        //Update previous consolidated FA to archived document
        await models.DocumentsForSignature.update({
            isActive: 0,
            isArchived:1

        },{
            where: {
                fundId: ammendmentInfo.fundId,
                docType: 'CONSOLIDATED_FUND_AGREEMENT',
                isDocumentEffectuated:{
                    [Op.in]:[1,null]
                }               
            }
        });

        if (signedTrackIds.length > 0) {
            await models.SignatureTrack.update({
                isAddedToConsolidatedFundAgreement: 1
            }, {
                where: {
                    id: {
                        [Op.in]: signedTrackIds
                    }
                }
            });
        }

        if (gpSignatorySignedIds.length > 0) {
            await models.GpSignatoryApproval.update({
                isAddedToConsolidatedAmendmentAgreement: 1
            }, {
                where: {
                    id: {
                        [Op.in]: gpSignatorySignedIds
                    }
                }
            })
        }

        return filePathToSave;

    } else {
        console.log('No pages to add.');
    }
}

//Get Ammendment
//Closed Investors signatures
//Closed GP Signature

//Here user is current user logged in details
const consolidatedAmmendment = async (amendmentId, ammendmentInfo, user) => {

    const fund = await models.Fund.findOne({
        include: [{
            model: models.User,
            as: 'gp',
            required: true
        }],
        where: { id: ammendmentInfo.fundId }
    });

    let gpSignaturePic = commonHelper.getSignaturePicUrl(fund.gp.signaturePic.path ? fund.gp.signaturePic.path : fund.gp.signaturePic)

    const gpSignatories = await models.GpSignatoryApproval.findAll({
        where: {
            amendmentId,
            fundId: ammendmentInfo.fundId
        },
        include: [{
            model: models.User,
            as: 'gpSignatory',
            required: false
        }]
    });


    const documentsForSignature = await models.DocumentsForSignature.findAll({
        where: {
            fundId: ammendmentInfo.fundId,
            docType: 'AMENDMENT_AGREEMENT',
            //isActive: 1,
            isPrimaryLpSigned: true,
            isArchived: 0,
            amendmentId
        },
        include: [{
            model: models.SignatureTrack,
            as: 'signatureTracks',
            include: [{
                model: models.User,
                as: 'signeduser',
                required: false
            }, {
                model: models.OfflineUsers,
                as: 'signedOfflineUser',
                required: false
            }],
            where: {
                signedByType: {
                    [Op.in]: ['LP', 'SecondaryLP', 'SecondaryGP', 'OfflineLP']
                },
                isActive: 1,
                documentType: 'AMENDMENT_AGREEMENT',
            },
            required: true
        }]
    });


    const subscriptionIds = _.map(documentsForSignature, 'subscriptionId');
    const documentIds = _.map(documentsForSignature, 'id');


    if (subscriptionIds.length == 0) {

        return;
    }

    const subscriptions = await models.FundSubscription.findAll({
        include: [{
            model: models.User,
            as: 'lp',
            required: false
        }, {
            model: models.OfflineUsers,
            as: 'offlineLp',
            required: false
        }],
        where: {
            id: {
                [Op.in]: subscriptionIds
            }
        }
    });

    let lpSignedPages = [];
    let signedTrackIds = [];
    let signedUsers = [];
    let offlineSignedUsers = [];
    let gpSignatorySignedIds = [];


    const consolidateAmmendmentAgreement = await models.DocumentsForSignature.findOne({
        where: {
            fundId: ammendmentInfo.fundId,
            docType: 'CONSOLIDATED_AMENDMENT_AGREEMENT',
            isActive: 1,
            deletedAt: null,
            isArchived: 0,
            amendmentId
        }
    });


    for (let gpSignatoryObj of gpSignatories) {

        if (gpSignatoryObj.isAddedToConsolidatedAmendmentAgreement !== 1) {


            let gpSignatory = _.find(gpSignatoryObj.gpSignatory, { id: gpSignatoryObj.signatoryId })
            const agreementSignData = {
                fundManagerTitle: fund.fundManagerTitle,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                fundLegalEntity: fund.legalEntity,
                fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                gpName: commonHelper.getFullName(gpSignatory),
                gpSignaturePic: commonHelper.getSignaturePicUrl(gpSignatory.signaturePic.path ? gpSignatory.signaturePic.path : gpSignatory.signaturePic),
                date: moment().format('M/D/YYYY')
            };

            const htmlFilePath = "./views/fundAgreementConsolidatedGpSign.html";

            const signedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);
            lpSignedPages.push(signedLastPage.path);
            gpSignatorySignedIds.push(gpSignatoryObj.id);
        }
    }

    for (let documentForSignature of documentsForSignature) {

        const subscription = _.find(subscriptions, { id: documentForSignature.subscriptionId })

        for (let signature of documentForSignature.signatureTracks) {

            if (!signedUsers.includes(signature.signedUserId) && (!signature.isAddedToConsolidatedFundAgreement) && signature.signedByType !== 'OfflineLP') {

                if (signature.signedByType !== 'SecondaryGP') {

                    const htmlFilePath = "./views/fundAgreementConsolidatedLpSign.html";

                    const agreementSignData = {
                        name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName,
                        jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                        investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                        legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                        areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                        lpName: (signature && signature.signeduser) ? commonHelper.getFullName(signature.signeduser) : '',
                        date: moment().format('M/D/YYYY'),
                        lpSignaturePic: commonHelper.getSignaturePicUrl(signature.signaturePath),
                        lpCapitalCommitment: subscription.lpCapitalCommitmentPretty
                    }

                    const signedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);
                    lpSignedPages.push(signedLastPage.path);
                    signedTrackIds.push(signature.id);

                }

            }

            if (!offlineSignedUsers.includes(signature.signedUserId) && (!signature.isAddedToConsolidatedFundAgreement)) {
                if (signature.signedByType == 'OfflineLP' && signature.signedOfflineUser) {
                    lpSignedPages.push(signature.signaturePath)
                    signedTrackIds.push(signature.id);
                }
            }

            signature.signedByType == 'OfflineLP' ?
                offlineSignedUsers.push(signature.signedUserId)
                : signedUsers.push(signature.signedUserId);
        }
    }


    if (lpSignedPages.length != 0) {

        let data = false;

        if (!consolidateAmmendmentAgreement) {

            const consolidateAmmendmentAgreementTempPath = ammendmentInfo.document ? ammendmentInfo.document.path : ammendmentInfo.filePath;

            const agreementSignData = {
                gpName: commonHelper.getFullName(user),
                fundManagerTitle: fund.fundManagerTitle,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                fundLegalEntity: fund.legalEntity,
                fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                gpSignaturePic: gpSignaturePic,
                date: moment().format('M/D/YYYY')
            };

            const htmlFilePath = "./views/fundAgreementConsolidatedGpSign.html";

            const gpSignedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);

            data = [consolidateAmmendmentAgreementTempPath, gpSignedLastPage.path].concat(lpSignedPages);

        } else {
            const consolidateAmmendmentAgreementTempPath = consolidateAmmendmentAgreement.filePath;
            data = [consolidateAmmendmentAgreementTempPath].concat(lpSignedPages);
        }

        let filePathToSave = `./assets/funds/${ammendmentInfo.fundId}/ammendments/CONSOLIDATED_AMENDMENT_AGREEMENT_${uuidv1()}.pdf`

        const fileName = `CONSOLIDATED_AMENDMENT_AGREEMENT_${uuidv1()}.pdf`;
        const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
        await commonHelper.pdfMerge(data, outputFile);
        fs.copyFileSync(outputFile, filePathToSave);

        if (consolidateAmmendmentAgreement) {
            //Update with new filepath with added signatures
            await models.DocumentsForSignature.update({
                filePath: filePathToSave
            }, {
                where: {
                    fundId: ammendmentInfo.fundId,
                    docType: 'CONSOLIDATED_AMENDMENT_AGREEMENT',
                    isActive: 1,
                    deletedAt: null,
                    amendmentId
                }
            });


        }

        await models.DocumentsForSignature.findOrCreate({
            where: {
                fundId: ammendmentInfo.fundId,
                docType: 'CONSOLIDATED_AMENDMENT_AGREEMENT',
                isActive: 1,
                deletedAt: null,
                isArchived: 0,
                amendmentId
            },
            defaults: {
                fundId: ammendmentInfo.fundId,
                docType: 'CONSOLIDATED_AMENDMENT_AGREEMENT',
                filePath: filePathToSave,
                isPrimaryGpSigned: true,
                isPrimaryLpSigned: true,
                createdBy: user.id,
                updatedBy: user.id,
                isActive: 1,
                isArchived: 0,
                amendmentId
            }
        });

        await models.DocumentsForSignature.update({
            isAddedToFundAgreement: 1
        }, {
            where: {
                id: {
                    [Op.in]: documentIds
                }
            }
        });

        

        if (signedTrackIds.length > 0) {
            await models.SignatureTrack.update({
                isAddedToConsolidatedFundAgreement: 1
            }, {
                where: {
                    id: {
                        [Op.in]: signedTrackIds
                    }
                }
            });
        }

        if (gpSignatorySignedIds.length > 0) {
            await models.GpSignatoryApproval.update({
                isAddedToConsolidatedAmendmentAgreement: 1
            }, {
                where: {
                    id: {
                        [Op.in]: gpSignatorySignedIds
                    }
                }
            })
        }
        return filePathToSave;
    } else {
        console.log('No pages to add.');
    }

}


async function generateSignedPage(data, htmlFilePath) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);

    let html = fs.readFileSync(htmlFilePath);

    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}

//add gp and gp signatories sign
const addGpSignatureAmmendment = async (req, amendmentId, ammendmentInfo, docType, user) => {
    console.log("********i am in addGpSignatureAmmendment *********")
    const fund = await models.Fund.findOne({
        include: [{
            model: models.User,
            as: 'gp',
            required: true
        }],
        where: { id: ammendmentInfo.fundId }
    })

    let gpSignaturePic = commonHelper.getSignaturePicUrl(fund.gp.signaturePic.path ? fund.gp.signaturePic.path : fund.gp.signaturePic)

    //get gp signatories 
    const gpSignatoryList = await models.GpSignatoryApproval.findAll({
        where: {
            fundId: ammendmentInfo.fundId
        },
        include: [{
            model: models.User,
            as: 'gpSignatory',
            required: false
        }]
    });

    
    //get all lp signatures
    const documentsForSignature = await models.DocumentsForSignature.findAll({
        where: {
            fundId: ammendmentInfo.fundId,
            isActive: 1,
            docType: docType,
            isPrimaryLpSigned: 1,
            isPrimaryGpSigned: 0,
            amendmentId,
            isArchived:0
        }
    }) 

    for (let documentForSignature of documentsForSignature) {

        let lpSignedPages = []

        //add gp signature first
        if (gpSignatoryList) {
            let tempObj = []
            for (let gpSignatoryObj of gpSignatoryList) {
                if(tempObj.indexOf(gpSignatoryObj.signatoryId)==-1){
                    tempObj.push(gpSignatoryObj.signatoryId)
                    let gpSignatory = _.find(gpSignatoryObj.gpSignatory,{id: gpSignatoryObj.signatoryId})
                    let agreementSignData = {
                        fundManagerTitle: fund.fundManagerTitle,
                        fundManagerCommonName: fund.fundManagerCommonName,
                        fundCommonName: fund.fundCommonName,
                        fundLegalEntity: fund.legalEntity,
                        fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                        gpName: commonHelper.getFullName(gpSignatory),
                        gpSignaturePic: commonHelper.getSignaturePicUrl(gpSignatory.signaturePic.path ? gpSignatory.signaturePic.path : gpSignatory.signaturePic),
                        date: moment().format('M/D/YYYY')
                    }
                    let htmlFilePath = "./views/fundAgreementConsolidatedGpSign.html";
                    let signedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);
                    lpSignedPages.push(signedLastPage.path)
                }
            }
        } 

        //add gp sign
        let gpAgreementSignData = {
            gpName: commonHelper.getFullName(user),
            fundManagerTitle: fund.fundManagerTitle,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            fundLegalEntity: fund.legalEntity,
            fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
            gpSignaturePic: gpSignaturePic,
            date: moment().format('M/D/YYYY')
        }

        let htmlFilePath = "./views/fundAgreementConsolidatedGpSign.html";

        let gpSignedLastPage = await generateSignedPage(gpAgreementSignData, htmlFilePath);

        let tempPath = documentForSignature.filePath
        let allLPSignedFiles = lpSignedPages.join(" ")
        console.log("allLPSignedFiles************",allLPSignedFiles)
        let data = false
        data = [tempPath,allLPSignedFiles].concat(gpSignedLastPage.path)      
        let fileName = `${docType}_SIGNED_${uuidv1()}_${documentForSignature.id}.pdf`
        let outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`)

        console.log("outputFile***************",outputFile)

        await commonHelper.pdfMerge(data, outputFile)
        fs.copyFileSync(outputFile, tempPath)

        //update the DFS for lp not signed 
        await models.DocumentsForSignature.update({
            isPrimaryGpSigned: 1
        }, {
                where: {
                    id:documentForSignature.id                         
                }
        }) 
  
    }

}

module.exports = {
    consolidatedAmmendment,
    consolidatedFullRestatedFundAgreement,
    addGpSignatureAmmendment
};

