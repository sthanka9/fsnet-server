const Handlebars = require('handlebars');
const config = require("../config");
const fs = require("fs");
const logger = require('../helpers/logger');
const nodemailer = require("nodemailer");
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(config.get('sendGridKey'));
const models = require("../models");
const commonHelper = require("../helpers/commonHelper");




module.exports.sendEmail = async function (mailOptionsObject) {

  logger.info('******** Email Helper Object***************'+mailOptionsObject)
  let GPRegistered = mailOptionsObject.data.user && mailOptionsObject.data.user.accountType == 'GP' ? await commonHelper.checkIfGPRegistered(mailOptionsObject.toAddress) :true;

  // //To check if the user is GP, If GP then verify whether he has registered into the sys or not
  if(GPRegistered){

    logger.info('******** Email Helper Start***************')
    let content = fs.readFileSync(__dirname + "/../views/email/" + mailOptionsObject.htmlPath);
    content = content.toString();
    let template = Handlebars.compile(content);
    let html = template(mailOptionsObject.data);
    const msg = {
      to: mailOptionsObject.toAddress,
      from: config.get('emailFrom'),
      subject: mailOptionsObject.subject,
      html: html
    }  
    const status = await sgMail.send(msg)
    logger.info('******** Email Helper to email Address***************'+mailOptionsObject.toAddress)
    logger.info('******** Email Helper to View Name***************'+mailOptionsObject.htmlPath)
    logger.info('******** Email Helper End***************')
    return status;
  }

};

module.exports.triggerAlertEmailNotification = (toAddress, subject, emailBodyContent, emailContentHtml) => {

  this.sendEmail({
    toAddress: toAddress,
    subject: subject,
    data: {
      firstName: emailBodyContent.firstName,
      lastName: emailBodyContent.lastName,
      investorName:emailBodyContent.investorName,
      individualContent: emailBodyContent.individualContent,
      name: emailBodyContent.name,
      fundManagerName: emailBodyContent.fundManagerName,
      fundCommonName: emailBodyContent.fundCommonName,
      capitalCommitmentOfferedAmount: emailBodyContent ? emailBodyContent.capitalCommitmentOfferedAmount : null,
      capitalCommitmentAccepted: emailBodyContent ? emailBodyContent.capitalCommitmentAccepted : null,
      clientURL: config.get('clientURL'),
      user:emailBodyContent.user
    },
    htmlPath: 'alerts/' + emailContentHtml
  })
}
