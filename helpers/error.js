module.exports = class UnauthorizedError extends Error { 

    constructor(code, error) {

        super(error.message);

        this.name = this.constructor.name;

        Error.captureStackTrace(this, this.constructor);

        this.message = error.message
      
        this.code = code
        this.status = 403
        this.inner = error
    }
}
