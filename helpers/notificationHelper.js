const models = require("../models");
const messageHelper = require("../helpers/messageHelper");
const commonHelper = require("../helpers/commonHelper");
const notificationHelper = require("../helpers/notificationHelper");
const _ = require("lodash");

module.exports.triggerNotification = async (req, fromUserId, toUserId,ToDelegateUsers, notificationData,accountId=null,gpData=null) => {

    let data = [];

    let GPRegistered = gpData && gpData.accountType == 'GP' ? await commonHelper.checkIfGPRegistered(gpData.email) :true;

    if(toUserId && GPRegistered) {
        data.push({
            toUserId: toUserId,
            fromUserId: fromUserId,
            accountId:accountId,//added accountid
            notification: JSON.stringify(notificationData)
        });
    }
    
    if(ToDelegateUsers.length>0) {
        for(let ToDelegateUser of ToDelegateUsers) {
            data.push({
                toUserId: ToDelegateUser.delegateId ? ToDelegateUser.delegateId : (ToDelegateUser.signatoryId) ? ToDelegateUser.signatoryId :  ToDelegateUser.id,
                fromUserId: fromUserId,
                accountId:ToDelegateUser.accountId, //added accountid
                notification: JSON.stringify(notificationData)
            });
        }
    }

    if(data) {
        models.Notification.bulkCreate(data);
    }
}

module.exports.eeaCountryAlert  = async (countryDomicileOrResidenceId,fund,user,gpDelegates,investorName,accountId)=>{

    // get all EEA countries
    let eEACountries = await models.Country.findAll({
        where: {
            isEEACountry: 1
        },
        attributes:['id']
    })

    eEACountries = _.map(eEACountries,'id')


    if(_.includes(eEACountries,Number(countryDomicileOrResidenceId))){

        //Send EEA Alert
        const alertData = {
            htmlMessage: messageHelper.investorCountryAlertMessage,
            message: 'Investor Country',
            lpName: investorName,
            gpName: commonHelper.getFullName(fund.gp),
            fundName: fund.legalEntity,
            sentBy: user.id
        };

        notificationHelper.triggerNotification(user, user.id, fund.gp.id, gpDelegates, alertData,accountId,fund.gp);
    }
  
}

module.exports.equityOwnersAlert = (req,fund,gpDelegates,investorName,accountId)=>{

    const alertData = {
        htmlMessage: messageHelper.equityOwnersMessage,
        message: 'Equity Owners Questions',
        lpName: investorName,
        numberOfexistingOrProspectives: fund.numberOfexistingOrProspectives,
        sentBy: req.userId
    };

    notificationHelper.triggerNotification(req, req.userId, fund.gp.id, gpDelegates, alertData,accountId,fund.gp);
}

