const models = require('../models/index');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const messageHelper = require('../helpers/messageHelper');
const config = require('../config/index');
const { Op } = require('sequelize')

module.exports.triggerEmailNotificationsToSignatories = async(newIds, fund, accountType, htmlPath, lpName = null) => {

    models.User.findAll({
        where: {
            id: {
                [Op.in]: newIds
            }
        },
        include:[{
            model: models.Account,
            as:'accountDetails',
            required:true
        }]        
    }).then(users => {

        if (!users) return; 

        users.forEach(function (user) {

            const loginOrRegisterLink = user.accountDetails.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${user.accountDetails.emailConfirmCode}`;
            
            //if (user.isEmailNotification || accountType != 'FSNETAdministrator') { // if setting is enabled
                emailHelper.sendEmail({
                    toAddress: user.email,
                    subject: messageHelper.assignGpDelegateEmailSubject,
                    data: {
                        name: commonHelper.getFullName(user),
                        fundName: fund.legalEntity,
                        fundCommonName: fund.fundCommonName,
                        gpName: commonHelper.getFullName(fund.gp),
                        lpName: lpName ? lpName : null,
                        loginOrRegisterLink: loginOrRegisterLink,
                        user
                    },
                    htmlPath: htmlPath
                }).catch(error => {
                    commonHelper.errorHandle(error)
                })
            //}


        });
    }).catch(error => {
        commonHelper.errorHandle(error)
    })
}