const models = require('../models/index');
const _ = require('lodash');
const messageHelper = require('../helpers/messageHelper');
const errorHelper = require('../helpers/errorHelper');
const emailHelper = require('../helpers/emailHelper');
const commonHelper = require('../helpers/commonHelper');
const logger = require('../helpers/logger');
const { Op } = require('sequelize');
const path = require('path');
const uuidv1 = require('uuid/v1');
const moment = require('moment');
const util = require('util');
const scissors = require('scissors');
const exec = util.promisify(require('child_process').exec);


//Replace existing FA with uploaded FA
module.exports.swapAgreements = async (parameters)=>{

    let tempPath = `../assets/temp/${uuidv1()}.pdf`;
    tempPath = path.join(__dirname,tempPath);
    let message={};

    let pdfFiles=[];

    try{
        await exec(`pdftk  ${parameters.existingAgreement} cat  ${parameters.existingAgreementPageCount+1}-end  output ${tempPath}`)
        pdfFiles.push(parameters.uploadedFilePath,tempPath);
    }catch(e){

        // message.status='Error'
        // message.logs =parameters.logs
        logger.log('error','Error while swapping the documents.',e)
        await models.LogsTable.update({
            status:2
        },{
            where:{
                fundId: parameters.fundId,
                id: parameters.logs.id
            }
        });
        return message


    }
   
    if(pdfFiles.length ==2){
        
        await commonHelper.pdfMerge(pdfFiles, parameters.outputFilePath);

        if(parameters.docType !== 'AMENDMENT_AGREEMENT'&& parameters.docType !== 'CONSOLIDATED_AMENDMENT_AGREEMENT'){

            //Replica of existing record with deleted date
            models.DocumentsForSignature.findOne({
                where:{
                    fundId: parameters.fundId,
                    subscriptionId: parameters.subscriptionId,
                    docType: parameters.docType,
                    isActive:1,
                    isArchived :0
                },
                raw:true
            }).then(async data=>{
    
                //Archive existing doc
                data.previousDocumentId = data.id
                delete data.id
                // data.deletedAt = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
                data.isActive =0
                data.createdBy = parameters.user.userId
                data.updatedBy = parameters.user.userId
                data.isArchived = 1
                
                await models.DocumentsForSignature.create(data);

                //Update with the new file path 
                await models.DocumentsForSignature.update({
                    filePath: parameters.faFilePathToSave
                }, {
                    where: {
                        fundId:parameters.fundId,
                        subscriptionId: parameters.subscriptionId,
                        docType: parameters.docType,
                        isActive:1,
                        deletedAt:null,
                        isArchived :0
                    }
                });

                //Replica of existing record(Archival)
                models.FundAmmendment.findOne({
                    where:{
                        fundId: parameters.fundId,
                        id: data.amendmentId,
                        isAmmendment:false,
                        isAffectuated:true,
                        isActive:1,
                        isArchived:0
                    },
                    raw:true
                }).then(async data=>{

                    if(data){
                        //Archive existing doc
                        delete data.id
                        data.isActive =0
                        data.createdBy = parameters.user.userId
                        data.updatedBy = parameters.user.userId
                        data.isArchived = 1
                        
                        await models.FundAmmendment.create(data);
    
                        //Update with the new file path 
                        await models.FundAmmendment.update({
                            document: await commonHelper.getFileInfo(parameters.uploadedFile.file,true)
                        }, {
                            where: {
                                fundId: parameters.fundId,
                                id: data.amendmentId,
                                isAmmendment:false,
                                isAffectuated:true,
                                isActive:1,
                                isArchived:0
                            }
                        });
                    }

                });
            });

            //status for logs 0-start 1-success 2-fail
            await models.LogsTable.update({
                status:1,
                endTime: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
            },{
                where:{
                    fundId: parameters.fundId,
                    id: parameters.logs.id
                }
            });

            console.log("====subscriptionIdsLength",parameters.count,parameters.length)
            if(parameters.length == parameters.count){
                // message.status='Success'
                // return message
                await models.Fund.update({ 
                    // isFundLocked: false,
                    partnershipDocument: await commonHelper.getFileInfo(parameters.uploadedFile,true), 
                    updatedBy: parameters.user.id 
                }, {
                    where: {
                        id: parameters.fundId
                    }
                });

                emailHelper.sendEmail({
                    toAddress: parameters.user.email,
                    subject: 'Upload without investor consent - Vanilla',
                    data:{
                        name: commonHelper.getFullName(parameters.user),
                        docType:'Fund Agreement'
                    },
                    htmlPath: "alerts/withoutInvestorConsent.html"
                });
            }

        }else{

            //Replica of existing record(Archieved)
            models.DocumentsForSignature.findOne({
                where:{
                    fundId: parameters.fundId,
                    subscriptionId: parameters.subscriptionId,
                    docType: parameters.docType,
                    amendmentId: parameters.amendmentId,
                    isActive:1,
                    isArchived :0
                },
                raw:true
            }).then(async data=>{
                
                if(data){

                    //Archive existing doc
                    data.previousDocumentId = data.id
                    delete data.id
                    // data.deletedAt = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
                    data.isActive =0
                    data.createdBy = parameters.user.userId
                    data.updatedBy = parameters.user.userId
                    data.isArchived = 1
                    
                    await models.DocumentsForSignature.create(data);
                    
                    //Update with the new file path 
                    await models.DocumentsForSignature.update({
                        filePath: parameters.faFilePathToSave
                    }, {
                        where: {
                            fundId:parameters.fundId,
                            subscriptionId: parameters.subscriptionId,
                            docType: parameters.docType,
                            isActive:1,
                            deletedAt:null,
                            isArchived :0,
                            amendmentId: parameters.amendmentId,
                        }
                    });
                }
            });
    
            
            //status for logs 0-start 1-success 2-fail
            await models.LogsTable.update({
                status:1,
                endTime: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
            },{
                where:{
                    fundId: parameters.fundId,
                    id: parameters.logs.id
                }
            });

            console.log("====subscriptionIdsLength22",parameters.count,parameters.length)


            if(parameters.length == parameters.count){
               
                // message.status='Success'
                // return message
                emailHelper.sendEmail({
                    toAddress: parameters.user.email,
                    subject: 'Upload without investor consent - Vanilla',
                    data:{
                        name: commonHelper.getFullName(parameters.user),
                        docType:'Amendment to Fund Agreement'
                    },
                    htmlPath: "alerts/withoutInvestorConsent.html"
                });
            }
        }
    }
        
}


module.exports.getPartnershipDocumentPageCount = async (filepath)=>{

    let partnershipDocumentPath = path.join(__dirname.replace('helpers',''),filepath);
    return await scissors(partnershipDocumentPath).getNumPages(); 

}