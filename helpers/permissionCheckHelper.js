const UnauthorizedError = require('./error');
const CustomUnauthorizedError = require('./customerror');
const models = require('../models')
const path  = require('path')
const { Op } = require('sequelize');
const config = require('../config/index');
const messageHelper = require('../helpers/messageHelper');
const errorHelper = require('../helpers/errorHelper');
const logger = require('../helpers/logger');

//check signatory signed 
const checkLpSignatoryAccess = async(req,res,next) => {
    let isAllowed = true
    let accountType = req.user.accountType
    let subscriptionId = req.params.subscriptionId 
    let subscriptionInfo = await models.FundSubscription.findOne({
        attributes:['noOfSignaturesRequired'],
        where:{
            id:subscriptionId
        }
    })

    if(accountType=='GPDelegate' || accountType=='LPDelegate'){
        isAllowed = false
    }

    if(accountType=='LP'){
    // if lp has signatories , then signatories first need to sign
    if(subscriptionInfo.noOfSignaturesRequired>=1){

        let isLPSignatoriesSigned = await models.DocumentsForSignature.count({
            where: {                        
                subscriptionId: subscriptionId,  
                isActive: 1,    
                deletedAt:null,
                isAllLpSignatoriesSigned:0
            }
        })        

        if(isLPSignatoriesSigned>0){
            isAllowed = false
        }

        //check already signed
        let isSigned = await models.DocumentsForSignature.count({                
            where:{
                subscriptionId:subscriptionId,
                isActive:1
            },
            include: [{
                model: models.SignatureTrack,
                as: 'signatureTracks',
                required: true,
                where:{
                    signedByType:'LP',
                    signedUserId:req.userId,
                    isActive:1
                }
            }]                
        })

        if(isSigned>0){
            isAllowed = false
        }        
    }

    }

    if(accountType=='SecondaryLP'){

        if(subscriptionInfo.noOfSignaturesRequired==0 || subscriptionInfo.noOfSignaturesRequired==null){

            isAllowed = false

        } else {
            //check already signed
            let isSigned = await models.DocumentsForSignature.count({                
                where:{
                    subscriptionId:subscriptionId,
                    isActive:1
                },
                include: [{
                    model: models.SignatureTrack,
                    as: 'signatureTracks',
                    required: true,
                    where:{
                        signedByType:'SecondaryLP',
                        signedUserId:req.userId,
                        isActive:1
                    }
                }]                
            })

            if(isSigned>0){
                isAllowed = false
            }
        }
    }

    return isAllowed ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    ))     
}

//check user already signed the sideletter
const isUserSignedSideLetter = async (req,res,next) => {

    let isAllowed = true
    let accountType = req.user.accountType
    let subscriptionId = req.params.subscriptionId

    if(accountType=='GPDelegate' || accountType=='LPDelegate'){
        isAllowed = false
    }    

    let subscriptionInfo = await models.FundSubscription.findOne({
        attributes:['noOfSignaturesRequired'],
        where:{
            id:subscriptionId
        }
    })    


    let documentsForSignature = await models.DocumentsForSignature.findOne({
        where: {
            subscriptionId: subscriptionId,       
            isActive: 1,
            docType:'SIDE_LETTER_AGREEMENT'
        }
    })

    let fundId = documentsForSignature.fundId
    let documentId = documentsForSignature.id
    let isPrimaryGpSigned = documentsForSignature.isPrimaryGpSigned

    let fundInfo = await models.Fund.findOne({
        attributes:['noOfSignaturesRequiredForSideLetter'],
        where:{
            id:fundId
        }
    })
    
    let noOfSignaturesRequiredForSideLetter = fundInfo.noOfSignaturesRequiredForSideLetter

    if(accountType=='GP'){        
        //only GP
        if(noOfSignaturesRequiredForSideLetter==1){

            let isCurrentUserSigned = await checkCurrentUserSignedTheDocument(documentsForSignature.id,req.userId)

            if(isCurrentUserSigned)
                isAllowed = false

        } else { // gp + gp signatory
            let signatoriesSignCount = await checkSignatoriesSignCount(documentId) 

            if( ! (noOfSignaturesRequiredForSideLetter-1 == signatoriesSignCount ))
                isAllowed = false
        } 
    }

    if(accountType=='SecondaryGP'){        
        //only GP
        if(noOfSignaturesRequiredForSideLetter==1){
            isAllowed = false
        } else {

            let isCurrentUserSigned = await checkCurrentUserSignedTheDocument(documentsForSignature.id,req.userId)

            if(isCurrentUserSigned || isPrimaryGpSigned)
                isAllowed = false              
        } 
    }
    
    if(accountType=='LP'){
        // if lp has signatories , then signatories first need to sign
        if(subscriptionInfo.noOfSignaturesRequired>=1){
    
            let isLPSignatoriesSigned = await models.DocumentsForSignature.count({
                where: {                        
                    subscriptionId: subscriptionId,  
                    isActive: 1,    
                    deletedAt:null,
                    isAllLpSignatoriesSigned:0
                }
            })        
    
            if(isLPSignatoriesSigned>0){
                isAllowed = false
            }
    
            //check already signed
            let isSigned = await models.DocumentsForSignature.count({                
                where:{
                    id:documentId,
                    isActive:1
                },
                include: [{
                    model: models.SignatureTrack,
                    as: 'signatureTracks',
                    required: true,
                    where:{
                        signedByType:'LP',
                        signedUserId:req.userId,
                        isActive:1
                    }
                }]                
            })
    
            if(isSigned>0){
                isAllowed = false
            }        
        }
    
        }
    
        if(accountType=='SecondaryLP'){
    
            if(subscriptionInfo.noOfSignaturesRequired==0 || subscriptionInfo.noOfSignaturesRequired==null){
    
                isAllowed = false
    
            } else {
                //check already signed
                let isSigned = await models.DocumentsForSignature.count({                
                    where:{
                        id:documentId,
                        isActive:1
                    },
                    include: [{
                        model: models.SignatureTrack,
                        as: 'signatureTracks',
                        required: true,
                        where:{
                            signedByType:'SecondaryLP',
                            signedUserId:req.userId,
                            isActive:1
                        }
                    }]                
                })
    
                if(isSigned>0){
                    isAllowed = false
                }
            }
        } 

    return isAllowed ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    )) 

}


//check user already signed the sideletter
const isUserSignedCCLetter = async (req,res,next) => {

    let isAllowed = true
    let accountType = req.user.accountType
    let subscriptionId = req.params.subscriptionId

    if(accountType=='GPDelegate' || accountType=='LPDelegate'){
        isAllowed = false
    }    

    let subscriptionInfo = await models.FundSubscription.findOne({
        attributes:['noOfSignaturesRequired'],
        where:{
            id:subscriptionId
        }
    })    


    let documentsForSignature = await models.DocumentsForSignature.findOne({
        where: {
            subscriptionId: subscriptionId,       
            isActive: 1,
            docType:'CHANGE_COMMITMENT_AGREEMENT'
        }
    })

    let fundId = documentsForSignature.fundId
    let documentId = documentsForSignature.id
    let isPrimaryGpSigned = documentsForSignature.isPrimaryGpSigned

    let fundInfo = await models.Fund.findOne({
        attributes:['noOfSignaturesRequiredForCapitalCommitment'],
        where:{
            id:fundId
        }
    })
    
    let noOfSignaturesRequiredForCapitalCommitment = fundInfo.noOfSignaturesRequiredForCapitalCommitment

    if(accountType=='GP'){        
        //only GP
        if(noOfSignaturesRequiredForCapitalCommitment==1){

            let isCurrentUserSigned = await checkCurrentUserSignedTheDocument(documentsForSignature.id,req.userId)

            if(isCurrentUserSigned)
                isAllowed = false

        } else { // gp + gp signatory
            let signatoriesSignCount = await checkSignatoriesSignCount(documentId) 

            if( ! (noOfSignaturesRequiredForCapitalCommitment-1 == signatoriesSignCount ))
                isAllowed = false
        } 
    }

    if(accountType=='SecondaryGP'){        
        //only GP
        if(noOfSignaturesRequiredForCapitalCommitment==1){
            isAllowed = false
        } else {

            let isCurrentUserSigned = await checkCurrentUserSignedTheDocument(documentsForSignature.id,req.userId)

            if(isCurrentUserSigned || isPrimaryGpSigned)
                isAllowed = false              
        } 
    }    


    if(accountType=='LP'){
        // if lp has signatories , then signatories first need to sign
        if(subscriptionInfo.noOfSignaturesRequired>=1){
    
            let isLPSignatoriesSigned = await models.DocumentsForSignature.count({
                where: {                        
                    subscriptionId: subscriptionId,  
                    isActive: 1,    
                    deletedAt:null,
                    isAllLpSignatoriesSigned:0
                }
            })        
    
            if(isLPSignatoriesSigned>0){
                isAllowed = false
            }
    
            //check already signed
            let isSigned = await models.DocumentsForSignature.count({                
                where:{
                    id:documentId,
                    isActive:1
                },
                include: [{
                    model: models.SignatureTrack,
                    as: 'signatureTracks',
                    required: true,
                    where:{
                        signedByType:'LP',
                        signedUserId:req.userId,
                        isActive:1
                    }
                }]                
            })
    
            if(isSigned>0){
                isAllowed = false
            }        
        }
    
        }
    
        if(accountType=='SecondaryLP'){
    
            if(subscriptionInfo.noOfSignaturesRequired==0 || subscriptionInfo.noOfSignaturesRequired==null){
    
                isAllowed = false
    
            } else {
                //check already signed
                let isSigned = await models.DocumentsForSignature.count({                
                    where:{
                        id:documentId,
                        isActive:1
                    },
                    include: [{
                        model: models.SignatureTrack,
                        as: 'signatureTracks',
                        required: true,
                        where:{
                            signedByType:'SecondaryLP',
                            signedUserId:req.userId,
                            isActive:1
                        }
                    }]                
                })
    
                if(isSigned>0){
                    isAllowed = false
                }
            }
    }     

    return isAllowed ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    )) 

}

//check signatories sign count
const checkSignatoriesSignCount = async(documentId) => {

    let count = await models.SignatureTrack.count({
        where: {
            documentId:documentId,
            signedByType:'SecondaryGP',
            isActive:1
        }
    })

    return count
}
 

const checkCurrentUserSignedTheDocument = async (documentId,userId) => {    
    let isSigned = await models.SignatureTrack.count({
        where: {
            documentId:documentId,
            signedUserId:userId,
            isActive:1
        }
    })

    return (isSigned==0) ? false : true
}

const isUserAllowedToDismissNotifications = async (req, res, next) => {    
    let { notificationId } = req.body
    let notification = await models.Notification.findOne({
        attributes:['accountId'],
        where: {
            id: notificationId
        }
    })      
    let isAllowed = (notification && notification.accountId == req.accountId) ? true : false
    return isAllowed ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    )) 
}

const isUserAllowedToViewNotifications = async (req, res, next) => {
    let userId = req.params.id
    let fromAccount = await models.User.findOne({ attributes:['accountId'],where: { id: userId } }) 
    let isAllowed = (fromAccount.accountId == req.accountId) ? true : false
    return isAllowed ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    )) 
}
 
const isLPUserAllowedtoSignThisDoc = async (req, res, next) => {   
   
    const accountType = req.user.accountType; // current user account type
    let isAllowed = false

    if(accountType == 'LP' || accountType == 'LPDelegate' || accountType == 'SecondaryLP' || accountType == 'FSNETAdministrator'){
        isAllowed = true          
    }

    return isAllowed ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    ))
 
}

const isGPUserAllowedtoSignThisDoc = async (req, res, next) => {   
   
    const accountType = req.user.accountType; // current user account type
    let isAllowed = false

    if(accountType == 'GP' ||  accountType == 'GPDelegate' || accountType == 'SecondaryGP' || accountType == 'FSNETAdministrator'){
        isAllowed = true          
    }

    return isAllowed ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    ))
 
}
 


const isUserAllowedToViewThisDoc = async (req, res, next) => {
    let url = config.get('clientURL')
    let subscriptionId = req.params.subscriptionId  ||  req.body.subscriptionId; 
    console.log("*******subscriptionId**********",subscriptionId)
    let documentId = req.params.documentId  ||  req.body.documentId ;
    let amendmentId =  req.params.amendmentId;

 
    if (!subscriptionId) { 
        
        //get subscription id
        if(documentId){ 
            let subscriptionInfo = await models.DocumentsForSignature.findOne({
                where: {
                    id: documentId                    
                }
            })            
            subscriptionId = subscriptionInfo ? subscriptionInfo.subscriptionId : null
        } 

        if(amendmentId){
            let subscriptionInfo = await models.DocumentsForSignature.findOne({
                where: {
                    amendmentId                    
                }
            })            

            subscriptionId = subscriptionInfo ? subscriptionInfo.subscriptionId:null
        }
    }

    if (!subscriptionId || subscriptionId =='undefined' || !Number.isInteger(parseInt(subscriptionId))) {             
        return errorHelper.error_403(res, 'documentId', messageHelper.noDataFound);         
    }    

    const currentUserId = req.user.id; // login/current user id
    const accountType = req.user.accountType; // current user account type

    let fundInfo = await models.FundSubscription.findOne({
        attributes: ['fundId'],
        where: {
            id: subscriptionId,
        }
    })

    if(!fundInfo){
        return res.status(403).send('Permission denied');   
    }

    const fundId = fundInfo.fundId

    let isAllowed = true

    isAllowed =  accountType == 'LP' ? await isLpAllowedToViewSubscription(subscriptionId, currentUserId) : accountType == 'SecondaryLP' ? await isSecondaryLpAllowedToViewSubscription(subscriptionId, currentUserId) : accountType == 'LPDelegate' ? await isDelegateLpAllowedToViewSubscription(subscriptionId, currentUserId)   : accountType == 'GP' ? await isGpAllowedToViewFund(fundId, currentUserId) : accountType == 'GPDelegate' ? await isGpDelegateAllowedToViewFund(fundId, currentUserId) :   accountType == 'SecondaryGP' ? await isGpSignatoryAllowedToViewFund(fundId, currentUserId) : false;    

    return isAllowed ? next() : res.status(403).send('Permission denied');   
 
}

const isGPUserAllowedToViewThisDoc = async (req,res,next) => {

    const fundId = req.params.fundId;
    const currentUserId = req.user.id; // logged in user id
    const accountType = req.user.accountType; // current user account type

    let isAllowed = true;

    isAllowed =  accountType == 'GP' ? await isGpAllowedToViewFund(fundId, currentUserId) : accountType == 'GPDelegate' ? await isGpDelegateAllowedToViewFund(fundId, currentUserId) :   accountType == 'SecondaryGP' ? await isGpSignatoryAllowedToViewFund(fundId, currentUserId) : false;

    return isAllowed ? next() : res.status(403).send('Permission denied');
}


async function isDelegateLpAllowedToViewSubscription(subscriptionId, lpId) {
    return await models.LpDelegates.findOne({
        attributes: ['id'],
        where: {
            subscriptionId: subscriptionId,
            delegateId: lpId
        }
    })
}


async function isSecondaryLpAllowedToViewSubscription(subscriptionId, lpId) {
    return await models.LpSignatories.findOne({
        attributes: ['id'],
        where: {
            subscriptionId: subscriptionId,
            signatoryId: lpId
        }
    })
}

const isUserAllowedToViewSubscriptionPdf  = async (req, res, next) => {

    const subscriptionId = req.params.subscriptionId; // subscription ID, which user is trying to view
    const currentUserId = req.user.id; // login/current user id
    const accountType = req.user.accountType; // current user account type

    if(accountType == 'FSNETAdministrator') {
        return next();
    }

    const isAllowed = accountType == 'LP' ? await isLpAllowedToViewSubscription(subscriptionId, currentUserId) : false;

    return isAllowed ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    ));

}

const isUserAllowedToUpdateFund = async (req, res, next) => {

    const currentUserId = req.user.id; // login/current user id
    const accountType = req.user.accountType; // current user account type
    const fundId = req.body.fundId;

    if(accountType == 'FSNETAdministrator') {
        return next();
    }

    if(!fundId) {
        return next(); // new fund
    }

    const isAllowed = accountType == 'GP' ? await isGpAllowedToViewFund(fundId, currentUserId) :
        (accountType == 'GPDelegate' ? await isGpDelegateAllowedToViewFund(fundId, currentUserId) : 
        (accountType == 'SecondaryGP' ? await isGpSignatoryAllowedToViewFund(fundId, currentUserId) : false));

    return isAllowed ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    ));

}


const isUserAllowedToViewThisFundPdf = async (req, res, next) => {

    const fundId = req.params.fundId; // fund ID, which user is trying to view
    const currentUserId = req.user.id; // login/current user id
    const accountType = req.user.accountType; // current user account type

    if(accountType == 'FSNETAdministrator') {
        return next();
    }

    const isAllowed = accountType == 'GP' ? await isGpAllowedToViewFund(fundId, currentUserId) :
        (accountType == 'GPDelegate' ? await isGpDelegateAllowedToViewFund(fundId, currentUserId) : 
        (accountType == 'LP' ? await isLpAllowedToViewFundPartnershipAgreement(fundId, currentUserId)  :  
        (accountType == 'LPDelegate' ? await isLpDelegateAllowedToFundPartnershipAgreement(fundId, currentUserId): false)));

    return isAllowed ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    ));
}


const isUserAllowedToViewThisFund = async (req, res, next) => {

    const fundId = req.params.fundId ? req.params.fundId : req.body.fundId ? req.body.fundId : null  ; // fund ID, which user is trying to view
    const currentUserId = req.user.id; // login/current user id
    const accountType = req.user.accountType; // current user account type

    if(accountType == 'FSNETAdministrator') {
        return next();
    }    

    let isAllowed = accountType == 'GP' ? await isGpAllowedToViewFund(fundId, currentUserId) :
        (accountType == 'GPDelegate' ? await isGpDelegateAllowedToViewFund(fundId, currentUserId) : 
        (accountType == 'SecondaryGP' ? await isGpSignatoryAllowedToViewFund(fundId, currentUserId) : false)); 

    //check gp requested to change fund manager and signatory accepted the fund
    const isFundManagerChanged = (accountType == 'GP' && !isAllowed ) ? await isGpRequestToAcceptFund(fundId, currentUserId) : false 

    //check fund manager has changed due to role switching
    if(isFundManagerChanged){ 
        return next(new CustomUnauthorizedError(
            'permission_denied', { code:'406',message: 'Fund has been assigned to new Fund Maanger'}
        ))
    }
    else{    
        return isAllowed ? next() : next(new UnauthorizedError(
            'permission_denied', { message: 'Permission denied' }
        ))
    }
}

//check gprequested to change fund manager and change was accepted
async function isGpRequestToAcceptFund(fundId, gpId) {
    
    return await models.gpChangeRequest.findOne({
        attributes: ['id'],
        where: {
            randomToken:0,
            fundId: fundId,
            userId: gpId,
            status:1,
            deletedAt: {
                [Op.ne]: null
            }
        }
    })
}

const isUserAllowedToViewThisSubscription = async (req, res, next) => {

    const subscriptionId = req.params.subscriptionId; // subscription ID, which user is trying to view
    const currentUserId = req.user.id; // login/current user id
    const accountType = req.user.accountType; // current user account type

    if(accountType == 'FSNETAdministrator') {
        return next();
    }
    let isAllowed = accountType == 'LP' ? await isLpAllowedToViewSubscription(subscriptionId, currentUserId) :
        (accountType == 'LPDelegate' ? await isLpDelegateAllowedToViewSubscription(subscriptionId, currentUserId) : 
        (accountType == 'SecondaryLP' ? await isLpSignatoryAllowedToViewSubscription(subscriptionId, currentUserId) : 
        ((accountType == 'GP' || 'GPDelegate' || 'SecondaryGP') ? await isGpAllowedToViewSubscription(subscriptionId, currentUserId) :
        false))); 

    return (isAllowed || (isAllowed && isAllowed.length >0) ? true :false) ? next() : next(new UnauthorizedError(
        'permission_denied', { message: 'Permission denied' }
    ));
}



async function isLpDelegateAllowedToViewSubscription(subscriptionId, delegateId) {
    return await models.LpDelegate.findOne({
        where: {
            subscriptionId: subscriptionId,
            delegateId: delegateId
        }
    })
}

async function isLpSignatoryAllowedToViewSubscription(subscriptionId, signatoryId) {
    return await models.LpSignatories.findOne({
        where: {
            subscriptionId: subscriptionId,
            signatoryId: signatoryId
        }
    })
}

async function isLpDelegateAllowedToFundPartnershipAgreement(fundId, delegateId) {
    return await models.LpDelegate.findOne({
        where: {
            fundId: fundId,
            delegateId: delegateId
        }
    })
}

async function isLpAllowedToViewFundPartnershipAgreement(fundId, lpId) {
    return await models.FundSubscription.findOne({
        attributes: ['id'],
        where: {
            fundId: fundId,
            lpId: lpId
        }
    })
}


async function isLpAllowedToViewSubscription(subscriptionId, lpId) {
    return await models.FundSubscription.findOne({
        attributes: ['id'],
        where: {
            id: subscriptionId,
            lpId: lpId
        }
    })
}

async function isGpDelegateAllowedToViewFund(fundId, delegateId) {
    return await models.GpDelegates.findOne({
        where: {
            fundId: fundId,
            delegateId: delegateId
        }
    })
}

async function isGpSignatoryAllowedToViewFund(fundId, signatoryId) {
    return await models.GpSignatories.findOne({
        where: {
            fundId: fundId,
            signatoryId: signatoryId
        }
    })
}

async function isGpAllowedToViewFund(fundId, gpId) {
    return await models.Fund.findOne({
        attributes: ['id'],
        where: {
            id: fundId,
            gpId: gpId
        }
    })
}

async function isGpAllowedToViewSubscription(subscriptionId, gpId){
    return await models.Fund.findAll({
        attributes: ['id'],
        where: {
            gpId
        },
        include:[{
            model:models.FundSubscription,
            where:{
                isOfflineInvestor:1
            },
            as:'fundInvestor'
        }]
    })
}

module.exports = {
    isUserAllowedToViewThisFund,
    isUserAllowedToViewThisSubscription,
    isUserAllowedToUpdateFund,
    isUserAllowedToViewSubscriptionPdf,
    isUserAllowedToViewThisFundPdf,
    isUserAllowedToViewThisDoc,
    isLPUserAllowedtoSignThisDoc,
    isGPUserAllowedtoSignThisDoc,
    isGPUserAllowedToViewThisDoc,
    isUserAllowedToViewNotifications,
    isUserAllowedToDismissNotifications,
    isUserSignedSideLetter,
    isUserSignedCCLetter,
    checkLpSignatoryAccess
}