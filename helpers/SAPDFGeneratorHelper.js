const _ = require('lodash');
const commonHelper = require("../helpers/commonHelper");
const path = require('path');
const fs = require('fs');
const uuidv1 = require('uuid/v1');
const config = require('../config/index');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function makePDFWithDoc(docFilePath,pdfPath){
    // const blobPath = await commonHelper.downloadBlob(docFilePath, {
    //     blobName: uuidv1()
    // });
    
    // const blobPath = `${config.get('serverURL')}${docFilePath.replace('./', '/')}`;
   
    // let blobPath = path.join(__dirname.replace('./','/'),docFilePath)
    //     blobPath = blobPath.replace('/helpers','')
    const ext = path.extname(docFilePath);
    if(ext == '.docx' || ext == '.doc'){

        const commandConvertToPDF = `curl --form file=@${docFilePath} ${config.get('pdfConvertService')} > ${pdfPath}`
        return await exec(commandConvertToPDF);
    }else{

        fs.copyFileSync(docFilePath, pdfPath);
    }
    

}

module.exports = async (subscriptionStepsData, data, subscriptionAgreementPath = false) => {

    let subscriptionHtml = fs.readFileSync('./views/subscription/form.html');

    subscriptionHtml = subscriptionHtml.toString();

    let firstPdfPath = `./assets/temp/${uuidv1()}_1.pdf`;
    let secondPdfPath = `./assets/temp/${uuidv1()}_2.pdf`;
    /*
    let thirdPdfPath = `./assets/temp/${uuidv1()}_3.pdf`;
    let fourthPdfPath = `./assets/temp/${uuidv1()}_4.pdf`;
    let fivePdfPath = `./assets/temp/${uuidv1()}_5.pdf`;*/

    const subscriptionFormPDF = path.resolve(__dirname, `../assets/temp/${uuidv1()}.pdf`)
    const htmlTemp = './views/subscription/subscriptionFormTemp.html';
    await commonHelper.makePDF(subscriptionHtml, data, subscriptionFormPDF, htmlTemp);



    if(subscriptionStepsData && subscriptionStepsData.one) {
        await makePDFWithDoc(subscriptionStepsData.one,firstPdfPath);
    } else {

        let secOneHtmlPath = path.resolve(__dirname, `../views/subscription/subscriptionOneSection.html`);
        secOneHtmlPath = fs.readFileSync(secOneHtmlPath);
        secOneHtmlPath = secOneHtmlPath.toString();

        await commonHelper.makePDF(secOneHtmlPath, data, firstPdfPath, htmlTemp);
    }

    if(subscriptionStepsData && subscriptionStepsData.two){
        await makePDFWithDoc(subscriptionStepsData.two,secondPdfPath);
    }else{

        let secTwoHtmlPath = path.resolve(__dirname, `../views/subscription/subscriptionTwoSection.html`);
        secTwoHtmlPath = fs.readFileSync(secTwoHtmlPath);
        secTwoHtmlPath = secTwoHtmlPath.toString();
        await commonHelper.makePDF(secTwoHtmlPath, data, secondPdfPath, htmlTemp);
    }

    /*

    if(subscriptionStepsData && subscriptionStepsData.three){
        await makePDFWithDoc(subscriptionStepsData.three,thirdPdfPath);
    }else{

        let secThreeHtmlPath = path.resolve(__dirname, `../views/subscription/subscriptionThreeSection.html`);
        secThreeHtmlPath = fs.readFileSync(secThreeHtmlPath);
        secThreeHtmlPath = secThreeHtmlPath.toString();
        await commonHelper.makePDF(secThreeHtmlPath, data, thirdPdfPath, htmlTemp);
    }

    if(subscriptionStepsData && subscriptionStepsData.four){
        await makePDFWithDoc(subscriptionStepsData.four,fourthPdfPath);
    }else{

        let secFourHtmlPath = path.resolve(__dirname, `../views/subscription/subscriptionFourSection.html`);
        secFourHtmlPath = fs.readFileSync(secFourHtmlPath);
        secFourHtmlPath = secFourHtmlPath.toString();
        await commonHelper.makePDF(secFourHtmlPath, data, fourthPdfPath, htmlTemp);
    }


    if(subscriptionStepsData && subscriptionStepsData.five){
        await makePDFWithDoc(subscriptionStepsData.five,fivePdfPath);
    }else{

        let secFiveHtmlPath = path.resolve(__dirname, `../views/subscription/subscriptionFiveSection.html`);
        secFiveHtmlPath = fs.readFileSync(secFiveHtmlPath);
        secFiveHtmlPath = secFiveHtmlPath.toString();
        await commonHelper.makePDF(secFiveHtmlPath, data, fivePdfPath, htmlTemp);
    }
    */
    

    const subscriptionFileName = `SUBSCRIPTION_AGREEMENT_${uuidv1()}.pdf`;

    const finalSubscriptionPDF = subscriptionAgreementPath || path.resolve(__dirname, `../assets/temp/${subscriptionFileName}`);
   commonHelper.ensureDirectoryExistence(finalSubscriptionPDF);
    await commonHelper.pdfMerge([firstPdfPath, subscriptionFormPDF, secondPdfPath], finalSubscriptionPDF);
    return { path: finalSubscriptionPDF, name: subscriptionFileName, url: `${config.get('serverURL')}/assets/temp/${subscriptionFileName}` };

}