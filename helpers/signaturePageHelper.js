const commonHelper = require('./commonHelper');
const uuidv1 = require('uuid/v1');
const path = require('path');
const fs = require('fs');

module.exports.agreementSign = async(fileType, document, data,htmlFilePath)  => {

    switch (fileType) {
        case 'FA':
          fileName = `FUND_AGREEMENT_${uuidv1()}.pdf`
          break;
        case 'AA':
          fileName = `AMENDMENT_AGREEMENT_${uuidv1()}.pdf`
          break;          
        case 'SA':
          fileName = `SUBSCRIPTION_AGREEMENT_${uuidv1()}.pdf`
          break;
        case 'CC':
          fileName = `CHANGE_COMMITMENT_AGREEMENT_${uuidv1()}.pdf`
          break;
        case 'SL':
          fileName = `SIDE_LETTER_AGREEMENT_${uuidv1()}.pdf`
          break;
        default:
          Error('file type no in range.')
    }

    const signaturePageTempFileName = `${uuidv1()}.pdf`;
    const signaturePageTempFilePath = path.resolve(__dirname, `../assets/temp/${signaturePageTempFileName}`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, signaturePageTempFilePath);

    const finalOutput = path.resolve(__dirname, `../assets/temp/${fileName}`);
    await commonHelper.pdfMerge([document, signaturePageTempFilePath], finalOutput);

    return { urlpath: `./assets/temp/${fileName}`, path: finalOutput, name: fileName, url: commonHelper.getLocalUrl(`assets/temp/${fileName}`) };
  
}