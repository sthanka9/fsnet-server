const models = require('../models/index');
const moment = require('moment');

module.exports.saveEventLogInformation = (eventType, fundId, subscriptionId, req,lpId=null) => {
    return models.Events.create({
        eventType: eventType,
        userId: req.userId,
        fundId: fundId ? fundId : '',
        subscriptionId: subscriptionId ? subscriptionId : '',
        eventTimestamp: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z'),
        ipAddress: req.ipInfo,
        createdBy: req.userId,
        updatedBy: req.userId,
        lpId:lpId
    });
}