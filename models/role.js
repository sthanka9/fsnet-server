'use strict';
module.exports = (sequelize, DataTypes) => {
  var Role = sequelize.define('Role', {
    role: DataTypes.STRING,
    slug: DataTypes.STRING,
    isLocked: DataTypes.INTEGER
  }, {});
  Role.associate = function (models) {
    models.Role.belongsToMany(models.Permission, { as: 'permissions', through: models.RolePermission });
  };
  return Role;
};