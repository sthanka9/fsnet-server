'use strict';
module.exports = (sequelize, DataTypes) => {
  var VCFirm = sequelize.define('VCFirm', {
    firmName: DataTypes.STRING,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    middleName: DataTypes.STRING,
    cellNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    gpId: DataTypes.INTEGER,
    subscriptonType: DataTypes.STRING,
    isImporsonatingAllowed: DataTypes.BOOLEAN,
    allowGPdelegatesToSign: DataTypes.BOOLEAN,
    VCFirmLogo: DataTypes.TEXT,
    isPublished: DataTypes.BOOLEAN,
    isGPNotified: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {});
  VCFirm.associate = function(models) {
    models.VCFirm.belongsTo(models.User, { as: 'gp', foreignKey: 'gpId' });
    models.VCFirm.hasMany(models.VcFrimDelegate, { as: 'VcFrimDelegate', foreignKey: 'vcfirmId' });
  };
  return VCFirm;
};