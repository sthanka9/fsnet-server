'use strict';
module.exports = (sequelize, DataTypes) => {
  var LpSignatories = sequelize.define('LpSignatories', {
    fundId: DataTypes.INTEGER,
    subscriptionId: DataTypes.INTEGER,
    signatoryId: DataTypes.INTEGER,
    lpId: DataTypes.INTEGER,
    subscriptionId: DataTypes.INTEGER,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    deletedAt: DataTypes.DATE,
  }, {paranoid: true});
  LpSignatories.associate = function(models) {
    models.LpSignatories.belongsTo(models.User,{as: 'signatoryDetails', foreignKey: 'signatoryId'});
  };
  return LpSignatories;
};