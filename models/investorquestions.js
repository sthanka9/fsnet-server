'use strict';
module.exports = (sequelize, DataTypes) => {
  var InvestorQuestions = sequelize.define('InvestorQuestions', {
    fundId: DataTypes.INTEGER,
    subscriptionId: DataTypes.INTEGER,    
    questionTitle: DataTypes.STRING,
    question: DataTypes.TEXT,
    typeOfQuestion: DataTypes.INTEGER,
    isRequired: DataTypes.BOOLEAN,
    deletedAt: DataTypes.DATE
    
  }, {});
  InvestorQuestions.associate = function(models) {
    models.InvestorQuestions.hasOne(models.InvestorAnswers, { as: 'answers', foreignKey: 'questionId' });
  };
  return InvestorQuestions;
};