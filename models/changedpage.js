'use strict';
module.exports = (sequelize, DataTypes) => {
  const ChangedPage = sequelize.define('ChangedPage', {
    fundId: DataTypes.INTEGER,
    isActive: DataTypes.BOOLEAN,
    documentPath: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    },      

  }, {});
  ChangedPage.associate = function(models) {
    // associations can be defined here
    
  };
  return ChangedPage;
};