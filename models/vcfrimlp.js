'use strict';
module.exports = (sequelize, DataTypes) => {
  var VcFrimLp = sequelize.define('VcFrimLp', {
    vcfirmId: DataTypes.INTEGER,
    lpId: DataTypes.INTEGER,
    isDeleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isOffline:{
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    deletedAt: DataTypes.DATE,
  }, { paranoid: true });
  VcFrimLp.associate = function(models) {
    models.VcFrimLp.belongsTo(models.User,{as: 'details', foreignKey: 'lpId'});

  };
  return VcFrimLp;
};