'use strict';
module.exports = (sequelize, DataTypes) => {
  var delegateApproval = sequelize.define('GpDelegateApprovals', {
    fundId: DataTypes.INTEGER,
    subscriptionId: DataTypes.INTEGER,
    delegateId: DataTypes.INTEGER,
    lpId: DataTypes.INTEGER,
    approvedByDelegate: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {});
  delegateApproval.associate = function(models) {
    //models.GpDelegateApprovals.belongsTo(models.User,{as: 'approvedDetails', foreignKey: 'lpId'});
    //models.GpDelegateApprovals.belongsTo(models.GpDelegates,{as: 'delegateSelected', targetKey: 'delegateId'});
  };
  return delegateApproval;
};