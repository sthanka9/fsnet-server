'use strict';
module.exports = (sequelize, DataTypes) => {
  const LogsTable = sequelize.define('LogsTable', {
    fundId: {
      type: DataTypes.INTEGER
    },
    subscriptionId: {
      type: DataTypes.INTEGER
    },
    status:{
      type: DataTypes.ENUM,
      values: [0,1,2],
      allowNull:false

    },
    docType:{
      type: DataTypes.STRING,
    },
    createdBy:{
      type: DataTypes.INTEGER
    },
    startTime:{
      type: DataTypes.DATE
    },
    endTime:{
      type: DataTypes.DATE
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }

  }, {});
  LogsTable.associate = function(models) {
    // associations can be defined here
  };
  return LogsTable;
};