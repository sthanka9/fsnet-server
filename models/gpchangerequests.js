'use strict';
module.exports = (sequelize, DataTypes) => {
  const gpChangeRequest = sequelize.define('gpChangeRequest', {
    fundId: DataTypes.INTEGER,
    currentGpId: DataTypes.INTEGER,    
    newGpId: DataTypes.INTEGER,
    vcfirmIdOld: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
    randomToken: DataTypes.STRING,
    convertCurrentGpToSignatory: DataTypes.BOOLEAN,
    deletedAt:DataTypes.DATE,
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    status: DataTypes.BOOLEAN,      

  }, {});
  gpChangeRequest.associate = function(models) {
    // associations can be defined here
    
  };
  return gpChangeRequest;
};

