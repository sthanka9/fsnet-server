'use strict';
module.exports = (sequelize, DataTypes) => {
  var ChangeCommitment = sequelize.define('ChangeCommitment', {
    fundId: DataTypes.INTEGER,
    subscriptionId: DataTypes.INTEGER,
    amount: DataTypes.FLOAT,
    amountPretty: {
      type: DataTypes.VIRTUAL,
      get() {
        const amount = this.getDataValue('amount')
        return amount ? amount.toLocaleString('en-US', {
          style: 'currency',
          currency: 'USD',
        }): amount;
      }
    },
    isPriorAmount:{type:DataTypes.BOOLEAN,default:false},
    comments: DataTypes.TEXT,
    deletedAt: {
      type: DataTypes.DATE,
    },
    isActive: DataTypes.BOOLEAN
  }, { paranoid: true });
  ChangeCommitment.associate = function(models) {
    models.ChangeCommitment.hasOne(models.DocumentsForSignature,{as: 'document',sourceKey:'id', foreignKey: 'changeCommitmentId'});
  };
  return ChangeCommitment;
};