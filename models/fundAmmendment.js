'use strict';
const commonHelper = require('../helpers/commonHelper');


module.exports = (sequelize, DataTypes) => {
  const FundAmmendment = sequelize.define('FundAmmendment', {
    fundId: {
      type: DataTypes.INTEGER
    },
    document: {
      type: DataTypes.TEXT,
      get() {
        let document = this.getDataValue('document');
        document = document ? JSON.parse(document) : null;
        if (document) {
          document['url'] = commonHelper.getLocalUrl(document.path);
          return document;
        } else { return null; }
      }
    },
    targetPercentage:{
      type: DataTypes.FLOAT
    },
    approvedPercentage:{
      type: DataTypes.FLOAT
    },
    isAmmendment:{
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isAffectuated:{
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    affectuatedDate:{
      type: DataTypes.DATE
    },
    isTerminated:{
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    isArchived: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }, 
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }, 
    considerOnlyLpOfferedAmount: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    noOfSignaturesRequiredForFundAmmendments: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    changedPagesId: {
      type: DataTypes.INTEGER
    },    
    deletedAt: {
      type: DataTypes.DATE
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  FundAmmendment.associate = function(models) {
    // associations can be defined here
    models.FundAmmendment.hasMany(models.GpSignatoryApproval, { as: 'gpSignatoryAprovedFundAmmendments',  sourceKey: 'id', foreignKey: 'amendmentId'});
    models.FundAmmendment.belongsTo(models.Fund, { as: 'fund', foreignKey: 'fundId' });
  };
  return FundAmmendment;
};
