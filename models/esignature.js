'use strict';
module.exports = (sequelize, DataTypes) => {
  const Esignature = sequelize.define('Esignature', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    middleName: DataTypes.STRING,
    email: DataTypes.STRING,
    documentIds: DataTypes.TEXT,
    subscriptionId: DataTypes.INTEGER,
    tokenId: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV1,
    },
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {});
  Esignature.associate = function (models) {
    // associations can be defined here
  };
  return Esignature;
};