'use strict';
module.exports = (sequelize, DataTypes) => {
  const Events = sequelize.define('Events', {
    eventType: {
      type: DataTypes.STRING
    },
    userId: DataTypes.INTEGER,
    fundId: DataTypes.INTEGER,
    subscriptionId: DataTypes.INTEGER,
    eventTimestamp: {
      type: DataTypes.DATE
    },
    ipAddress: {
      type: DataTypes.STRING
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    lpId:{
      type: DataTypes.INTEGER
    }
  }, {});
  Events.associate = function(models) {
    // associations can be defined here
    models.Events.belongsTo(models.User, { as: 'eventUser', targetKey: 'id', foreignKey: 'userId' });
    models.Events.belongsTo(models.Fund, { as: 'eventFund', targetKey: 'id', foreignKey: 'fundId' });
    models.Events.belongsTo(models.FundSubscription, { as: 'FundSubscription', targetKey: 'id', foreignKey: 'subscriptionId' });
  };
  return Events;
};