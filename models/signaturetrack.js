'use strict';

module.exports = (sequelize, DataTypes) => {
  const SignatureTrack = sequelize.define('SignatureTrack', {
    documentId: {
      type: DataTypes.STRING
    },
    signaturePath: {
      type: DataTypes.STRING
    },
    ipAddress: {
      type: DataTypes.STRING
    },
    signedDateTime: {
      type: DataTypes.DATE
    },
    timezone: {
      type: DataTypes.STRING
    },
    signedDateInGMT: {
      type: DataTypes.DATE
    },
    signedUserId: {
      type: DataTypes.INTEGER
    },
    signedByType: {
      type: DataTypes.STRING
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      default: true
    },
    deletedAt: {
      type: DataTypes.DATE,
    },
    subscriptionId: {
      type: DataTypes.INTEGER,
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    documentType:{
      type: DataTypes.STRING
    },
    isAddedToConsolidatedFundAgreement:{
      type: DataTypes.BOOLEAN,
      default:false
    }
  }, {});
  SignatureTrack.associate = function(models) {
    models.SignatureTrack.belongsTo(models.User, { as: 'signeduser', foreignKey: 'signedUserId' });
    models.SignatureTrack.belongsTo(models.OfflineUsers, { as: 'signedOfflineUser', foreignKey: 'signedUserId' });
  };
  return SignatureTrack;
};