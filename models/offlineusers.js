'use strict';
module.exports = (sequelize, DataTypes) => {
  const OfflineUsers = sequelize.define('OfflineUsers', {
    firstName: {
      type: DataTypes.STRING
    },
    lastName: {
      type: DataTypes.STRING
    },
    middleName: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    email: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    fundId:{
      type: DataTypes.INTEGER
    },
    organizationName: {
      type: DataTypes.STRING,
      defaultValue: null

    },
    address:{
      type: DataTypes.TEXT,
      defaultValue: null
    },
    cellNumber:{
      type: DataTypes.STRING,
      defaultValue: null
    },
    accountType: {
      type: DataTypes.ENUM,
      values: ['OfflineLP'],
      allowNull: false
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    deletedAt: DataTypes.DATE
  }, { paranoid: true });
  OfflineUsers.associate = function(models) {
    // associations can be defined here
  };
  return OfflineUsers;
};