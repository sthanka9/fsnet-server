'use strict';
module.exports = (sequelize, DataTypes) => {
  var LpRequiredFundDocument = sequelize.define('LpRequiredFundDocument', {
    lpId: DataTypes.INTEGER,
    fundId: DataTypes.INTEGER,
    partnershipDoc: DataTypes.BOOLEAN
  }, {});
  LpRequiredFundDocument.associate = function(models) {
    // associations can be defined here
  };
  return LpRequiredFundDocument;
};