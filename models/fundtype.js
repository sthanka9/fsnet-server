'use strict';
module.exports = (sequelize, DataTypes) => {
  const FundType = sequelize.define('FundType', {
    fundType: {
      type: DataTypes.STRING
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  FundType.associate = function (models) {
    // associations can be defined here
  };
  return FundType;
};