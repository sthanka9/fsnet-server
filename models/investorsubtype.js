'use strict';
module.exports = (sequelize, DataTypes) => {
  var InvestorSubType = sequelize.define('InvestorSubType', {
    name: DataTypes.STRING,
    group: DataTypes.STRING,
    isUS: {
      type: DataTypes.BOOLEAN,
      get() {
        const isUS = this.getDataValue('isUS')
        return isUS ? 1 : 0;
     }
    } 
  }, {});
  InvestorSubType.associate = function(models) {
    // associations can be defined here
  };
  return InvestorSubType;
};