'use strict';
const commonHelper = require('../helpers/commonHelper')
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

module.exports = (sequelize, DataTypes) => {
  var Account = sequelize.define('Account', {
    defaultUserId:{
      type: DataTypes.INTEGER
    },
    firstName: {
      type: DataTypes.STRING,
      get() {
        let firstName = this.getDataValue('firstName');
        return firstName ? entities.decode(firstName) : firstName;
      }
    },
    lastName: {
      type: DataTypes.STRING,
      get() {
        let lastName = this.getDataValue('lastName');
        return lastName ? entities.decode(lastName) : lastName;
      }
    },
    middleName: {
      type: DataTypes.STRING,
      get() {
        let middleName = this.getDataValue('middleName');
        return middleName ? entities.decode(middleName) : middleName;
      },
      defaultValue: null
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING
    },
    organizationName: {
      type: DataTypes.STRING
    },
    city: {
      type: DataTypes.STRING
    },
    state: {
      type: DataTypes.STRING
    },
    country: {
      type: DataTypes.STRING
    },
    cellNumber: {
      type: DataTypes.STRING
    },
    profilePic: {
      type: DataTypes.TEXT,
      get() {
        let profilePic = this.getDataValue('profilePic')
        profilePic = profilePic ? JSON.parse(profilePic) : null
        if (profilePic) {
          profilePic['url'] = commonHelper.getLocalUrl(profilePic['path']);
          return profilePic
        } else { return null }
      }
    },
    isImpersonatingAllowed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    isEmailConfirmed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    emailConfirmCode: {
      type: DataTypes.STRING
    },
    lastCodeGeneratedAt: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.DATE
    },
    loginFailAttempts: {
      allowNull: true,
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    loginFailAttemptsAt: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.DATE
    },
    lastLoginAt: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.DATE
    },
    streetAddress1: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.STRING
    },
    streetAddress2: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.STRING
    },
    zipcode: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.STRING
    },
    signaturePic: {
      type: DataTypes.TEXT
    },
    primaryContact: {
      type: DataTypes.TEXT,
      get() {
        const primaryContact = this.getDataValue('primaryContact')
        return primaryContact ? JSON.parse(primaryContact) : null
      },
    },
    isEmailNotification: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    },    
    deletedAt: DataTypes.DATE,
  }, { paranoid: true });
  Account.associate = function (models) {

      
 
  
    
  };
  return Account;
};