'use strict';
const path = require('path');
const entDecode = require('ent/decode');
const moment = require('moment');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

const decode = (text) => {
  return entDecode(entities.decode(text));
}


module.exports = (sequelize, DataTypes) => {
  var Notification = sequelize.define('Notification', {
    fromUserId: DataTypes.INTEGER,
    toUserId: DataTypes.INTEGER,
    accountId: {
      type:DataTypes.INTEGER,
      allowNull: true,
    },
    isRead: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    notification: {
      type: DataTypes.TEXT,
      get() {
        let notification = this.getDataValue('notification')
        notification = notification ? JSON.parse(notification) : null;
        if(notification && notification.htmlMessage){
          notification.htmlMessage = decode(notification.htmlMessage)
        }
        if(notification && notification.numberOfexistingOrProspectives){
          notification.numberOfexistingOrProspectives = decode(notification.numberOfexistingOrProspectives)
        }

        return notification
      }
    }
  }, {});
  Notification.associate = function(models) {
    
  };
  return Notification;
};