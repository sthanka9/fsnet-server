'use strict';
module.exports = (sequelize, DataTypes) => {
  var VcFirmSignatories = sequelize.define('VcFirmSignatories', {
    vcfirmId: DataTypes.INTEGER,
    signatoryId: DataTypes.INTEGER,
    type: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    deletedAt: DataTypes.DATE,
  }, { paranoid: true });
  VcFirmSignatories.associate = function(models) {
    models.VcFirmSignatories.belongsTo(models.User,{as: 'signatoryDetails', foreignKey: 'signatoryId'});
    models.VcFirmSignatories.belongsTo(models.LpSignatories,{as: 'lpSignatoriesSelected',targetKey: 'signatoryId', foreignKey: 'signatoryId'});
    //models.VcFirmSignatories.belongsTo(models.GpDelegates,{as: 'fund', targetKey: 'signatoryId', foreignKey: 'signatoryId'});
  };
  return VcFirmSignatories;
};