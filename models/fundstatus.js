'use strict';
module.exports = (sequelize, DataTypes) => {
  var FundStatus = sequelize.define('FundStatus', {
    name:{
      type:DataTypes.STRING
    },
    lpSideName: {
      type:DataTypes.STRING,
      get() {
        var name = this.getDataValue('name')
        var lpSideName = this.getDataValue('lpSideName')
        if(name == 'Open-Ready-Draft'){
          return name='In Progress'
        }else if(name == 'Open'){
          return name='Invited'
        }else if(name == 'Not Interested'){
          return name='Declined'
        }else if(name == 'Rescind'){
          return name='Declined'
        }else if (name == 'Invitation-Pending'){
          return lpSideName
        }else{
          return name
        }
      }
    }
  }, {});
  FundStatus.associate = function(models) {
    // associations can be defined here
  };
  return FundStatus;
};