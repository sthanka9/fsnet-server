'use strict';
module.exports = (sequelize, DataTypes) => {
  var VcFrimDelegate = sequelize.define('VcFrimDelegate', {
    vcfirmId: DataTypes.INTEGER,
    delegateId: DataTypes.INTEGER,
    type: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    deletedAt: DataTypes.DATE,
  }, { paranoid: true });
  VcFrimDelegate.associate = function(models) {
    models.VcFrimDelegate.belongsTo(models.User,{as: 'details', foreignKey: 'delegateId'});
    models.VcFrimDelegate.belongsTo(models.GpDelegates,{as: 'fund', targetKey: 'delegateId', foreignKey: 'delegateId'});
    models.VcFrimDelegate.hasMany(models.VCFirm,{ as: 'vcFirms', sourceKey: 'vcfirmId', foreignKey: 'id'  });
  };
  return VcFrimDelegate;
};