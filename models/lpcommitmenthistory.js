'use strict';
module.exports = (sequelize, DataTypes) => {
  var LpCommitmentHistory = sequelize.define('LpCommitmentHistory', {
    fundId: DataTypes.INTEGER,
    subscriptionId: DataTypes.INTEGER,
    lpId: DataTypes.INTEGER,
    amount: DataTypes.INTEGER,
    comments: DataTypes.TEXT,
    isActive: DataTypes.BOOLEAN,
    envelopeId: {
      type: DataTypes.STRING
    },
    isGpSigned: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
      
    },
    isLpSigned: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
  }, {});
  LpCommitmentHistory.associate = function(models) {
    // associations can be defined here
  };
  return LpCommitmentHistory;
};