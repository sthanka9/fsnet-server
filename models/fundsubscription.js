'use strict';

const entDecode = require('ent/decode');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

const decode = (text) => {
  return entDecode(entities.decode(text));
}
module.exports = (sequelize, DataTypes) => {
  var FundSubscription = sequelize.define('FundSubscription', {
    fundId: {
      type: DataTypes.INTEGER
    },
    lpId: {
      type: DataTypes.INTEGER
    },
    changedPagesId: {
      type: DataTypes.INTEGER
    },       
    offlineUserId:{
      type: DataTypes.INTEGER,
      defaultValue:null   
    },
    isOfflineInvestor:{
      type: DataTypes.BOOLEAN,
      defaultValue:false
    },
    investorType: {
      type: DataTypes.ENUM,
      values: ['Individual', 'LLC', 'Trust'],
      allowNull: true
    },
    trustTitle: DataTypes.STRING,
    isSubjectToDisqualifyingEvent: DataTypes.BOOLEAN,
    fundManagerInfo: DataTypes.TEXT('long'),
    name: {
      type: DataTypes.STRING
    },
    otherInvestorAttributes: {
      type: DataTypes.STRING,
      get() {
        const otherInvestorAttributes = this.getDataValue('otherInvestorAttributes')
        return otherInvestorAttributes ? otherInvestorAttributes.split(',') : []
      },

      set(val) {
        this.setDataValue('otherInvestorAttributes', ((val && typeof (val) == 'object') ? val.join(',') : val));
      }
    },
    areYouSubscribingAsJointIndividual: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    legalTitle: {
      type: DataTypes.STRING,
      allowNull: true
    },
    areYouAccreditedInvestor: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    areYouQualifiedPurchaser: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    isPrimaryInvestment: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false  
    },
    areYouQualifiedClient: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    typeOfLegalOwnership: {
      type: DataTypes.STRING,
      // values: ['jointTenants', 'jointTenantsRights', 'tenantsInCommon', 'tenantsEntirety', 'communityProperty'],
      allowNull: true
    },
    whatSTATEIsTheEntityRegistered: {
      type: DataTypes.INTEGER
    },
    mailingAddressStreet: {
      type: DataTypes.STRING
    },
    mailingAddressCity: {
      type: DataTypes.STRING
    },
    mailingAddressState: {
      type: DataTypes.STRING
    },
    mailingAddressZip: {
      type: DataTypes.STRING
    },
    mailingAddressPhoneNumber: {
      type: DataTypes.STRING
    },
    mailingAddressCountry: DataTypes.INTEGER,

    //.... LLC
    indirectBeneficialOwnersSubjectFOIAStatutes: {
      type: DataTypes.TEXT
    },
    investorSubType: {
      type: DataTypes.INTEGER
    },
    otherInvestorSubType: {
      type: DataTypes.STRING
    },
    entityName: {
      type: DataTypes.STRING,
      get() {
        let entityName = this.getDataValue('entityName');
        return entityName ? decode(entityName) : entityName;
      }
    },
    entityTitle: {
      type: DataTypes.STRING
    },
    jurisdictionEntityLegallyRegistered: {
      type: DataTypes.STRING
    },
    isEntityTaxExemptForUSFederalIncomeTax: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    isEntityUS501c3: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    istheEntityFundOfFundsOrSimilarTypeVehicle: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    releaseInvestmentEntityRequired: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    // 3
    companiesAct: {
      type: DataTypes.STRING,
    },
    numberOfDirectEquityOwners: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    existingOrProspectiveInvestorsOfTheFund: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    numberOfexistingOrProspectives: {
      type: DataTypes.STRING,
      get() {
        let numberOfexistingOrProspectives = this.getDataValue('numberOfexistingOrProspectives');
        return numberOfexistingOrProspectives ? decode(numberOfexistingOrProspectives) : numberOfexistingOrProspectives;
      }
    },
    //Entity proposing
    entityProposingAcquiringInvestment: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    anyOtherInvestorInTheFund: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    entityHasMadeInvestmentsPriorToThedate: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    partnershipWillNotConstituteMoreThanFortyPercent: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    beneficialInvestmentMadeByTheEntity: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    //ERISA
    employeeBenefitPlan: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    planAsDefinedInSection4975e1: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    benefitPlanInvestor: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    totalValueOfEquityInterests: {
      type: DataTypes.DECIMAL(19, 2),
      allowNull: true,
      defaultValue: null
    },
    fiduciaryEntityIvestment: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    entityDecisionToInvestInFund: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    aggrement1: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    aggrement2: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    lpCapitalCommitment: {
      type: DataTypes.DECIMAL(19, 2),
      get() {
        const lpCapitalCommitment = this.getDataValue('lpCapitalCommitment')
        return Number(lpCapitalCommitment);
      }
    },
    lpCapitalCommitmentPretty: {
      type: DataTypes.VIRTUAL,
      get() {
        const lpCapitalCommitment = this.getDataValue('lpCapitalCommitment')
        return lpCapitalCommitment ? lpCapitalCommitment.toLocaleString('en-US', {
          style: 'currency',
          currency: 'USD',
        }) : lpCapitalCommitment;
      }
    },
    lookThrough: {
      type: DataTypes.VIRTUAL,
      get() {
        const entityProposingAcquiringInvestment = this.getDataValue('entityProposingAcquiringInvestment');
        // const anyOtherInvestorInTheFund = this.getDataValue('anyOtherInvestorInTheFund');
        const entityHasMadeInvestmentsPriorToThedate = this.getDataValue('entityHasMadeInvestmentsPriorToThedate');
        const partnershipWillNotConstituteMoreThanFortyPercent = this.getDataValue('partnershipWillNotConstituteMoreThanFortyPercent');
        const beneficialInvestmentMadeByTheEntity = this.getDataValue('beneficialInvestmentMadeByTheEntity');

        if(entityProposingAcquiringInvestment == null 
          && entityHasMadeInvestmentsPriorToThedate == null && partnershipWillNotConstituteMoreThanFortyPercent == null && beneficialInvestmentMadeByTheEntity == null){
            return null;
        }else{

          return (entityProposingAcquiringInvestment
          && entityHasMadeInvestmentsPriorToThedate && partnershipWillNotConstituteMoreThanFortyPercent && beneficialInvestmentMadeByTheEntity) ? true :false;
        }

      }
    },


    numberOfGrantorsPrettyForPDF: {
      type: DataTypes.VIRTUAL,
      get() {
        const investorType = this.getDataValue('investorType');
        const investorSubType = this.getDataValue('investorSubType');
        const numberOfDirectEquityOwners = this.getDataValue('numberOfDirectEquityOwners');
        return (investorType == 'Trust' && investorSubType == 9) ? numberOfDirectEquityOwners : '___';
      }
    },

    legalTitleDesignationPrettyForPDF: {
      type: DataTypes.VIRTUAL,
      get() {
        const investorType = this.getDataValue('investorType');
        const entityTitle = this.getDataValue('entityTitle');
        const investorSubType = this.getDataValue('investorSubType');
        const legalTitleDesignation = this.getDataValue('legalTitleDesignation');
        const trustTitle = this.getDataValue('trustTitle');
        const legalTitle = this.getDataValue('legalTitle');
        const areYouSubscribingAsJointIndividual = this.getDataValue('areYouSubscribingAsJointIndividual');
        return investorType == 'LLC' ? entityTitle :
          (investorType == 'Trust' ? (investorSubType == 10 ? legalTitleDesignation : trustTitle) :
            (investorType == 'Individual' ? (areYouSubscribingAsJointIndividual ? legalTitle : legalTitleDesignation) : ''));
      }
    },
    jointIndividualTitlePrettyForPDF: {
      type: DataTypes.VIRTUAL,
      get() {
        const investorType = this.getDataValue('investorType');
        const legalTitle = this.getDataValue('legalTitle');
        const areYouSubscribingAsJointIndividual = this.getDataValue('areYouSubscribingAsJointIndividual');
        return (investorType == 'Individual' && areYouSubscribingAsJointIndividual) ? legalTitle : false;
      }
    },
    titlePrettyForPDF: {
      type: DataTypes.VIRTUAL,
      get() {
        const investorType = this.getDataValue('investorType');
        const trustName = this.getDataValue('trustName');
        const entityTitle = this.getDataValue('entityTitle');
        const legalTitle = this.getDataValue('legalTitle');
        const areYouSubscribingAsJointIndividual = this.getDataValue('areYouSubscribingAsJointIndividual');

        return investorType == 'LLC' ? entityTitle : (investorType == 'Trust' ? trustName :
          (investorType == 'Individual' ? (areYouSubscribingAsJointIndividual ? legalTitle : null) : 'N/A'))

      }
    },
    numberOfGrantorsOfTheTrust: DataTypes.INTEGER,
    trustLegallyDomiciled: DataTypes.INTEGER,
    selectState: DataTypes.INTEGER,
    trustName:{
      type:DataTypes.STRING,
      get() {
        let trustName = this.getDataValue('trustName');
        return trustName ? decode(trustName) : trustName;
      }
    },
    isTrust501c3: DataTypes.BOOLEAN,
    legalTitleDesignation: DataTypes.STRING,
    status: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    investorCount: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    subscriptionAgreementPath: {
      type: DataTypes.BLOB,
      get() {
        let subscriptionAgreementPath = this.getDataValue('subscriptionAgreementPath');
        return subscriptionAgreementPath ? JSON.parse(subscriptionAgreementPath) : subscriptionAgreementPath;
      },
      set(val) {
        this.setDataValue('subscriptionAgreementPath', JSON.stringify(val));
      }
    },
    subscriptionHtml: {
      type: DataTypes.BLOB,
      get() {
        const subscriptionHtml = this.getDataValue('subscriptionHtml');
        return subscriptionHtml ? JSON.parse(subscriptionHtml) : subscriptionHtml;
      },
      set(val) {
        this.setDataValue('subscriptionHtml', JSON.stringify(val));
      }
    },
    subscriptionHtmlLastUpdated: {
      type: DataTypes.DATE,
    },
    isAdditionalQuestionAdded: {
      type: DataTypes.BOOLEAN
    },
    isSubscriptionContentUpdated: {
      type: DataTypes.BOOLEAN
    },
    noOfSignaturesRequired: {
      type: DataTypes.INTEGER,
      default: 0
    },
    stateResidenceId: {
      type: DataTypes.INTEGER
    },
    countryResidenceId: {
      type: DataTypes.INTEGER
    },
    stateDomicileId: {
      type: DataTypes.INTEGER
    },
    countryDomicileId: {
      type: DataTypes.INTEGER
    },
    organizationName:{
      type: DataTypes.STRING,
      get() {
        let organizationName = this.getDataValue('organizationName');
        return organizationName ? decode(organizationName) : organizationName;
      }
    },
    lpClosedDate: {
      type: DataTypes.DATE,
    },
    invitedDate: {
      type: DataTypes.DATE,
      defaultValue:null
    },
    isInvestorInvited:{
      type: DataTypes.INTEGER,
      defaultValue:0
    },
    isLpSignatoriesNotified:{
      type: DataTypes.BOOLEAN,
      defaultValue:0
    },
    deletedAt: {
      type: DataTypes.DATE,
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }

  }, {
      paranoid: true,
      hooks: {
        beforeSave: async function(fundSubscription, options) {
          console.log('BeforeSave Enter');
          if (fundSubscription.investorType != fundSubscription.previous('investorType')) {
            console.log('BeforeSave Enter exec');
            fundSubscription.mailingAddressStreet = null;

            // as these fields are only belong to Individual, for remaining types it should be null on type change.
            if(fundSubscription.investorType != 'Individual' ) {
              fundSubscription.areYouSubscribingAsJointIndividual = null;
            }

            fundSubscription.mailingAddressCity = null;
            fundSubscription.mailingAddressState = null;
            fundSubscription.mailingAddressZip = null;
            fundSubscription.mailingAddressPhoneNumber = null;
            fundSubscription.mailingAddressCountry = null;
            fundSubscription.areYouAccreditedInvestor = null;
            fundSubscription.areYouQualifiedPurchaser = null;
            fundSubscription.areYouQualifiedClient = null;
            fundSubscription.companiesAct = null;
            fundSubscription.numberOfDirectEquityOwners = 1;
            fundSubscription.investorCount = 1;
            fundSubscription.existingOrProspectiveInvestorsOfTheFund = null;
            fundSubscription.numberOfexistingOrProspectives = null;
            fundSubscription.entityProposingAcquiringInvestment = null;
            fundSubscription.anyOtherInvestorInTheFund = null;
            fundSubscription.entityHasMadeInvestmentsPriorToThedate = null;
            fundSubscription.partnershipWillNotConstituteMoreThanFortyPercent = null;
            fundSubscription.beneficialInvestmentMadeByTheEntity = null;
            fundSubscription.employeeBenefitPlan = null;
            fundSubscription.planAsDefinedInSection4975e1 = null;
            fundSubscription.benefitPlanInvestor = null;
            fundSubscription.fiduciaryEntityIvestment = null;
            fundSubscription.entityDecisionToInvestInFund = null;
            fundSubscription.totalValueOfEquityInterests = null;
            fundSubscription.aggrement1 = null;
            fundSubscription.aggrement2 = null;
            fundSubscription.lpCapitalCommitment = 0;
            fundSubscription.isLpSignatoriesNotified = 0;
          }

        
        }
      }
  });

 

  FundSubscription.associate = function (models) {
    models.FundSubscription.hasMany(models.DocumentsForSignature, { as: 'documentsForSignature', sourceKey: 'id', foreignKey: 'subscriptionId' });
    models.FundSubscription.belongsTo(models.User, { as: 'lp', foreignKey: 'lpId' });
    models.FundSubscription.belongsTo(models.OfflineUsers, { as: 'offlineLp',foreignKey: 'offlineUserId',targetKey:'id'  });
    models.FundSubscription.belongsTo(models.Country, { as: 'mailingAddressCountryData', foreignKey: 'mailingAddressCountry' });
    models.FundSubscription.belongsTo(models.State, { as: 'mailingAddressStateData', foreignKey: 'mailingAddressState' });
    models.FundSubscription.belongsTo(models.Country, { as: 'countryResidenceList', foreignKey: 'countryResidenceId' });
    models.FundSubscription.belongsTo(models.Country, { as: 'countryDomicileList', foreignKey: 'countryDomicileId' });
    models.FundSubscription.belongsTo(models.State, { as: 'stateResidenceList', foreignKey: 'stateResidenceId' });
    models.FundSubscription.belongsTo(models.State, { as: 'stateDomicileList', foreignKey: 'stateDomicileId' });
    models.FundSubscription.belongsTo(models.Fund, { as: 'fund', foreignKey: 'fundId' });
    models.FundSubscription.hasOne(models.SignatureTrack, { as: 'signature', foreignKey: 'subscriptionId' });
    models.FundSubscription.belongsTo(models.InvestorSubType, { as: 'investorSubTypeInfo', foreignKey: 'investorSubType' });
    models.FundSubscription.belongsTo(models.FundStatus, { as: 'subscriptionStatus', foreignKey: 'status' });
    models.FundSubscription.hasMany(models.ChangeCommitment, { as: 'commitmentHistory', foreignKey: 'subscriptionId', sourceKey: 'id' });
    models.FundSubscription.hasMany(models.FundClosings, { as: 'fundClosingInfo', foreignKey: 'subscriptionId', sourceKey: 'id' });
    models.FundSubscription.hasMany(models.InvestorAnswers, { as: 'investorAnswers', sourceKey: 'id', foreignKey: 'subscriptionId' });
    models.FundSubscription.hasMany(models.LpSignatories, { as: 'lpSignatoriesSelected', sourceKey: 'id', foreignKey: 'subscriptionId' });
    models.FundSubscription.hasMany(models.GpDelegateApprovals, { as: 'GpDelegateApprovals', sourceKey: 'id', foreignKey: 'subscriptionId' });
    models.FundSubscription.hasMany(models.GpSignatoryApproval, { as: 'GpSignatoryApprovals', sourceKey: 'id', foreignKey: 'subscriptionId' });
    models.FundSubscription.hasMany(models.SignatureTrack, { as: 'signatureTracks', sourceKey: 'id', foreignKey: 'subscriptionId' });
    models.FundSubscription.hasMany(models.LpDelegate, { as: 'lpDelegates', sourceKey: 'lpId',foreignKey: 'lpId' });

  };

  // class methods are defined right on the model
  FundSubscription.getSubscription = function (subscriptionId, models) {
    return this.findOne({
      where: {
        id: subscriptionId
      },
      include: [{
        model: models.Fund,
        as: 'fund',
        required: true,
        include: [{
          model: models.User,
          as: 'gp',
        },
        {
          model: models.InvestorQuestions,
          as: 'investorQuestions',
          required: false,
          where: {
            deletedAt: null
          },
          include: [{
            model: models.InvestorAnswers,
            as: 'answers',
            where: {
              subscriptionId: subscriptionId
            },
            required: false
          }]
        }
        ]
      }, {
        model: models.User,
        as: 'lp',
        required: true
      }, {
        attributes: ['id', 'name'],
        model: models.FundStatus,
        as: 'subscriptionStatus',
        required: false
      }, {
        model: models.InvestorSubType,
        required: false,
        as: 'investorSubTypeInfo'
      }]
    })
  }


  //offline subscription
  FundSubscription.getOfflineSubscription = function (subscriptionId, models) {
    return this.findOne({
      where: {
        id: subscriptionId
      },
      include: [{
        model: models.Fund,
        as: 'fund',
        required: true,
        include: [{
          model: models.User,
          as: 'gp',
        },
        {
          model: models.InvestorQuestions,
          as: 'investorQuestions',
          required: false,
          where: {
            deletedAt: null
          },
          include: [{
            model: models.InvestorAnswers,
            as: 'answers',
            where: {
              subscriptionId: subscriptionId
            },
            required: false
          }]
        }
        ]
      }, {
        model: models.OfflineUsers,
        as: 'offlineLp',
        required: true
      }, {
        attributes: ['id', 'name'],
        model: models.FundStatus,
        as: 'subscriptionStatus',
        required: false
      }, {
        model: models.InvestorSubType,
        required: false,
        as: 'investorSubTypeInfo'
      }]
    })
  }
  
  return FundSubscription;

};


