'use strict';
module.exports = (sequelize, DataTypes) => {
  const DocumentsForSignatureHistory = sequelize.define('DocumentsForSignatureHistory', {
    documentId: {
      type: DataTypes.INTEGER
    },
    fundId: {
      type: DataTypes.INTEGER
    },
    subscriptionId: {
      type: DataTypes.INTEGER
    },  
    filePath: {
      type: DataTypes.STRING
    },
    docType: {
      type: DataTypes.STRING
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      default: true
    },
    isArchived: {
      type: DataTypes.BOOLEAN,
      default: false
    },    
    previousDocumentId: {
      type: DataTypes.INTEGER,
      default: null
    },
    amendmentId: {
      type: DataTypes.INTEGER,
      defaultValue: null
    }
  }, {});
  DocumentsForSignatureHistory.associate = function(models) {
    // associations can be defined here
  };
  return DocumentsForSignatureHistory;
};