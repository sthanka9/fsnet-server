'use strict';
const commonHelper = require('../helpers/commonHelper');
const config = require('../config/index');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {

    firstName: {
      type: DataTypes.STRING,
      get() {
        let firstName = this.getDataValue('firstName');
        return firstName ? entities.decode(firstName) : firstName;
      }
    },
    lastName: {
      type: DataTypes.STRING,
      get() {
        let lastName = this.getDataValue('lastName');
        return lastName ? entities.decode(lastName) : lastName;
      }
    },
    middleName: {
      type: DataTypes.STRING,
      get() {
        let middleName = this.getDataValue('middleName');
        return middleName ? entities.decode(middleName) : middleName;
      },
      defaultValue: null
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING
    },
    organizationName: {
      type: DataTypes.STRING,
      get() {
        let organizationName = this.getDataValue('organizationName');
        return organizationName ? entities.decode(organizationName) : organizationName;
      }
    },
    city: {
      type: DataTypes.STRING
    },
    state: {
      type: DataTypes.STRING
    },
    country: {
      type: DataTypes.STRING
    },
    cellNumber: {
      type: DataTypes.STRING
    },
    profilePic: {
      type: DataTypes.TEXT,
      get() {
        let profilePic = this.getDataValue('profilePic')
        profilePic = profilePic ? JSON.parse(profilePic) : null
        if (profilePic) {
          profilePic['url'] =  `${config.get('serverURL')}${profilePic['path'].replace('./','/')}`;
          return profilePic
        } else { return null }
      }
    },
    isImpersonatingAllowed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    isEmailConfirmed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    emailConfirmCode: {
      type: DataTypes.STRING
    },
    lastCodeGeneratedAt: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.DATE
    },
    accountType: {
      type: DataTypes.ENUM,
      values: ['FSNETAdministrator', 'GP', 'GPDelegate', 'LP', 'LPDelegate', 'SecondaryGP', 'SecondaryLP'],
      allowNull: false
    },
    loginFailAttempts: {
      allowNull: true,
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    loginFailAttemptsAt: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.DATE
    },
    lastLoginAt: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.DATE
    },
    streetAddress1: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.STRING
    },
    streetAddress2: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.STRING
    },
    zipcode: {
      defaultValue: null,
      allowNull: true,
      type: DataTypes.STRING
    },
    accountTypeMask: {
      type: DataTypes.VIRTUAL,
      get() {
        const val = this.getDataValue('accountType');
        if(val == 'LP') {
            return 'Primary Signatory';
        } else if(val == 'GP') {
          return 'Primary Signatory';
        } else if(val == 'GPDelegate' || val == 'LPDelegate') {
          return 'Delegate';
        } else if(val == 'SecondaryGP' || val == 'SecondaryLP') {
          return 'Secondary Signatory';
        } 
      }
    },
    signaturePic: {
      type: DataTypes.TEXT
    },
    signaturePicUrl: {      
      type: DataTypes.VIRTUAL,
      get() {
        let signaturePicUrl = this.getDataValue('signaturePic')
        signaturePicUrl = signaturePicUrl ? signaturePicUrl : null
        if (signaturePicUrl) {
          signaturePicUrl = signaturePicUrl.replace('./','/')
          signaturePicUrl =  `${config.get('localServerURL')}${signaturePicUrl}`;
          return signaturePicUrl
        } else { return null }
      }
    },    
    primaryContact: {
      type: DataTypes.TEXT,
      get() {
        const primaryContact = this.getDataValue('primaryContact')
        return primaryContact ? JSON.parse(primaryContact) : null
      },
    },
    isEmailNotification: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    accountId: {
      type: DataTypes.INTEGER
    },    
    deletedAt: DataTypes.DATE,
  }, { paranoid: true });
  User.associate = function (models) {
    models.User.belongsToMany(models.Role, {
      as: 'roles',
      through: models.UserRole
    });

    models.User.hasOne(models.VcFrimLp, { as: 'lps', foreignKey: 'lpId' });
    
    models.User.belongsTo(models.FundSubscription, { as: 'subscription', targetKey: 'lpId', foreignKey :'id' });
    models.User.belongsTo(models.Account, { as: 'account', targetKey: 'defaultUserId', foreignKey :'id' });
    models.User.belongsTo(models.Account, { as: 'accountDetails', targetKey: 'id', foreignKey :'accountId' });
    models.User.hasOne(models.GpDelegates, { foreignKey: 'delegateId' });    
    models.User.hasOne(models.VcFrimDelegate, { foreignKey: 'delegateId' });
    models.User.belongsTo(models.State, { as:'stateData', foreignKey: 'state'});
    models.User.belongsTo(models.Country, { as:'countryData', foreignKey: 'country' });
    models.User.hasMany(models.Fund, { as: 'fundsSelected', foreignKey: 'gpId', sourceKey: 'id' });
    models.User.hasOne(models.GpSignatories, { foreignKey: 'signatoryId' });
    models.User.hasOne(models.VcFirmSignatories, {as: 'vcFirmSignatories', foreignKey: 'signatoryId' });
    
  };
  return User;
};