'use strict';
module.exports = (sequelize, DataTypes) => {
  var RolePermission = sequelize.define('RolePermission', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    roleId: DataTypes.INTEGER,
    permissionId: DataTypes.INTEGER
  }, {});
  RolePermission.associate = function(models) {
    // associations can be defined here
  };
  return RolePermission;
};