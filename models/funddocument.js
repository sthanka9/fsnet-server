'use strict';
module.exports = (sequelize, DataTypes) => {
  var FundDocument = sequelize.define('FundDocument', {
    fundId: DataTypes.INTEGER,
    fundDocument: DataTypes.BLOB,
    pagesDocument: DataTypes.BLOB,
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    isArchived: DataTypes.BOOLEAN
  }, {});
  FundDocument.associate = function(models) {
    // if any relationships

  };
  return FundDocument;
};