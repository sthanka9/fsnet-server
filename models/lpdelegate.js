'use strict';
module.exports = (sequelize, DataTypes) => {
  var LpDelegate = sequelize.define('LpDelegate', {
    fundId: DataTypes.INTEGER,
    delegateId: DataTypes.INTEGER,
    lpId: DataTypes.INTEGER,
    subscriptionId: DataTypes.INTEGER,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    deletedAt: DataTypes.DATE,
    invitedDate: {
      type: DataTypes.DATE,
      defaultValue:null
    },
    isInvestorInvited:{
      type: DataTypes.INTEGER,
      defaultValue:0
    },    
  }, {paranoid: true});
  LpDelegate.associate = function(models) {
    models.LpDelegate.belongsTo(models.User,{as: 'details', foreignKey: 'delegateId'});
  };
  return LpDelegate;
};