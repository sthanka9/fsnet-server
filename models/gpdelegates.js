'use strict';
module.exports = (sequelize, DataTypes) => {
  var GpDelegates = sequelize.define('GpDelegates', {
    fundId: DataTypes.INTEGER,
    delegateId: DataTypes.INTEGER,
    gpId: DataTypes.INTEGER,
    gPDelegateRequiredDocuSignBehalfGP: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    gPDelegateRequiredConsentHoldClosing: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    deletedAt: DataTypes.DATE,
  }, {paranoid: true});
  GpDelegates.associate = function(models) {
    models.GpDelegates.belongsTo(models.User,{as: 'details', foreignKey: 'delegateId'});
    models.GpDelegates.belongsTo(models.User,{as: 'gpuserdetails', foreignKey: 'gpId'});
    models.GpDelegates.hasOne(models.GpDelegateApprovals, { as: 'approverDetails', foreignKey: 'delegateId', sourceKey: 'delegateId' });
    models.GpDelegates.hasMany(models.Fund, { as: 'gpDelegateFunds', foreignKey: 'id', sourceKey: 'fundId' });
  };
  return GpDelegates;
};