'use strict';
module.exports = (sequelize, DataTypes) => {
  const GpSignatoryApproval = sequelize.define('GpSignatoryApproval', {
    fundId: DataTypes.INTEGER,
    signatoryId: DataTypes.INTEGER,
    subscriptionId:DataTypes.INTEGER,
    gpId: DataTypes.INTEGER, 
    lpId: DataTypes.INTEGER,
    isAddedToConsolidatedAmendmentAgreement: DataTypes.INTEGER,
    amendmentId: {
      type: DataTypes.INTEGER,
      defaultValue: null
    }
  }, {});
  GpSignatoryApproval.associate = function(models) {
    // associations can be defined here
    models.GpSignatoryApproval.hasMany(models.User, { as: 'gpSignatory',  sourceKey: 'signatoryId', foreignKey: 'id'})
  };
  return GpSignatoryApproval;
};