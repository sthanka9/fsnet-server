'use strict';
module.exports = (sequelize, DataTypes) => {
  var GpSignatories = sequelize.define('GpSignatories', {
    fundId: DataTypes.INTEGER,
    signatoryId: DataTypes.INTEGER,
    gpId: DataTypes.INTEGER,    
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    deletedAt: DataTypes.DATE,
  }, {paranoid: true});
  GpSignatories.associate = function(models) {
    models.GpSignatories.belongsTo(models.User,{as: 'signatoryDetails', foreignKey: 'signatoryId'});    
    models.GpSignatories.hasMany(models.Fund, { as: 'gpSignatoryFunds', foreignKey: 'id', sourceKey: 'fundId' });
  };
  return GpSignatories;
};