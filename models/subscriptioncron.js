'use strict';

const commonHelper = require('../helpers/commonHelper');

module.exports = (sequelize, DataTypes) => {
  const SubscriptionCron = sequelize.define('SubscriptionCron', {
    fundId: {
      type: DataTypes.INTEGER
    },
    subscriptionId: {
      type: DataTypes.INTEGER
    },
    partnershipDocumentPageCount: {
      type: DataTypes.INTEGER
    },
    uploadedFilePath: {
      type: DataTypes.TEXT,
      get() {
        let uploadedFilePath = this.getDataValue('uploadedFilePath');
        uploadedFilePath = uploadedFilePath ? JSON.parse(uploadedFilePath) : null;
        if (uploadedFilePath) {
          uploadedFilePath['url'] = commonHelper.getLocalUrl(uploadedFilePath.path);
          return uploadedFilePath;
        } else { return null; }
      }
    },
    originalPartnershipDocument:{
      type: DataTypes.TEXT,
      get() {
        let originalPartnershipDocument = this.getDataValue('originalPartnershipDocument');
        originalPartnershipDocument = originalPartnershipDocument ? JSON.parse(originalPartnershipDocument) : null;
        if (originalPartnershipDocument) {
          originalPartnershipDocument['url'] = commonHelper.getLocalUrl(originalPartnershipDocument.path);
          return originalPartnershipDocument;
        } else { return null; }
      }
    },
    partnershipDocumentToSwap:{
      type: DataTypes.STRING
    },
    swappedPartnershipAgreement:{
      type: DataTypes.STRING
    },
    partnershipDocumentToReplace:{
      type: DataTypes.STRING
    },
    docType:{
      type: DataTypes.STRING
    },
    userId: {
      type: DataTypes.INTEGER
    },
    amendmentId: {
      type: DataTypes.INTEGER
    },
    fundCronId:{
      type: DataTypes.INTEGER
    },
    status:{
      type: DataTypes.INTEGER,
      defaultValue:null   
    },
    count:{
      type: DataTypes.INTEGER,
      defaultValue:null   
    }
  }, {});
  SubscriptionCron.associate = function(models) {
    // associations can be defined here
  };
  return SubscriptionCron;
};