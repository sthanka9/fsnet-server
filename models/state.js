'use strict';
module.exports = (sequelize, DataTypes) => {
  var State = sequelize.define('State', {
    name: DataTypes.STRING,
    country_id: DataTypes.INTEGER,
    order: DataTypes.INTEGER
  }, {});
  State.associate = function(models) {
    // associations can be defined here
  };
  return State;
};