'use strict';
module.exports = (sequelize, DataTypes) => {
  const SideLetter = sequelize.define('SideLetter', {
    fundId: DataTypes.INTEGER,
    subscriptionId: DataTypes.INTEGER,
    isActive: DataTypes.BOOLEAN,
    sideLetterDocumentPath: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER    

  }, {});
  SideLetter.associate = function(models) {
    // associations can be defined here
    
  };
  return SideLetter;
};