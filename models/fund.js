'use strict';
const config = require('../config/index')
const commonHelper = require('../helpers/commonHelper');
const path = require('path');


module.exports = (sequelize, DataTypes) => {
  var Fund = sequelize.define('Fund', {
    vcfirmId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    gpId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    // fundTargetCommitment
    legalEntity: {
      type: DataTypes.STRING
    },
    fundHardCap: {
      type: DataTypes.DECIMAL(19, 2),
      allowNull: true,
      defaultValue: null,
      set(val){
        val = val !== '' ? val : null
        this.setDataValue('fundHardCap', val);
      }
    },
    fundManagerLegalEntityName: {
      type: DataTypes.STRING
    },
    fundTargetCommitment: {
      type: DataTypes.DECIMAL(19, 2),
      allowNull: true,
      defaultValue: null,
      set(val){
        val = val !== '' ? val : null
        this.setDataValue('fundTargetCommitment', val);
      }
    },
    percentageOfLPCommitment: {
      type: DataTypes.STRING
    },
    percentageOfLPAndGPAggregateCommitment: {
      type: DataTypes.STRING
    },
    capitalCommitmentByFundManager: {
      type: DataTypes.DECIMAL(19, 2),
      allowNull: true,
      defaultValue: null,
      set(val){
        val = val !== '' ? val : null
        this.setDataValue('capitalCommitmentByFundManager', val);
      }
    },
    fundImage: {
      type: DataTypes.TEXT,
      get() {
        let fundImage = this.getDataValue('fundImage')
        fundImage = fundImage ? JSON.parse(fundImage) : null
        if (fundImage) {
          fundImage['url'] = `${config.get('serverURL')}${fundImage['path'].replace('./','/')}`; 
          return fundImage;
        } else { return null; }

      }
    },
    partnershipDocument: {
      type: DataTypes.TEXT,
      get() {
        let partnershipDocument = this.getDataValue('partnershipDocument');
        partnershipDocument = partnershipDocument ? JSON.parse(partnershipDocument) : null;
        if (partnershipDocument) {
          partnershipDocument['url'] = commonHelper.getLocalUrl(partnershipDocument.path);
          return partnershipDocument;
        } else { return null; }
      }
    },
    partnershipDocumentPageCount:{
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    generalPartnersCapitalCommitmentindicated: {
      type: DataTypes.INTEGER
    },
    fundType: {
      type: DataTypes.INTEGER,
      set(val){
        val = val !== '' ? val : null
        this.setDataValue('fundType', val);
      }
    },
    deactivateReason: {
      type: DataTypes.STRING
    },
    statusId: {
      type: DataTypes.INTEGER
    },
    fundEntityType:{
      type: DataTypes.INTEGER,
    },
    timezone:{
      type: DataTypes.INTEGER,
    },    
    fundCommonName: {
      type: DataTypes.STRING,
    },
    fundManagerTitle: {
      type: DataTypes.STRING,
    },
    hardCapApplicationRule: {
      type: DataTypes.STRING,
    },
    fundManagerCommonName: {
      type: DataTypes.STRING,
    },
    subscriptionAgreementPath: {
      type: DataTypes.BLOB,
      get() {
        const subscriptionAgreementPath = this.getDataValue('subscriptionAgreementPath');
        return subscriptionAgreementPath ? JSON.parse(subscriptionAgreementPath) : subscriptionAgreementPath;
      },
      set(val) {
        this.setDataValue('subscriptionAgreementPath', JSON.stringify(val));
      }
    },
    subscriptionHtml: {
      type: DataTypes.BLOB,
      get() {
        const subscriptionHtml = this.getDataValue('subscriptionHtml');
        return subscriptionHtml ? JSON.parse(subscriptionHtml) : subscriptionHtml;
      },
      set(val) {
        this.setDataValue('subscriptionHtml', JSON.stringify(val));
      }
    },
    additionalSignatoryPages: {
      type: DataTypes.STRING,
      get() {
        const additionalSignatoryPages = this.getDataValue('additionalSignatoryPages');
        return additionalSignatoryPages ? JSON.parse(additionalSignatoryPages) :additionalSignatoryPages ;
      },
       set(val) {
         this.setDataValue('additionalSignatoryPages', JSON.stringify(val));
      }
    },
    additionalSignatoryPagesUpdatedDate: {
      type: DataTypes.DATE,
      allowNull: null
    },
    isAdditionalSignatoryPagesAddedToConslidatedPDF: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    subscriptionHtmlLastUpdated: {
      type: DataTypes.DATE,
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    deletedAt: DataTypes.DATE,
    noOfSignaturesRequiredForClosing: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    noOfSignaturesRequiredForCapitalCommitment: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    noOfSignaturesRequiredForSideLetter: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },    
    noOfSignaturesRequiredForFundAmmendments: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },    
    isFundLocked:{
      type: DataTypes.BOOLEAN,
      default: false
    },
    isNotificationsEnabled:{
      type: DataTypes.BOOLEAN,
      default: 0
    }    
  }, { paranoid: true });
  Fund.associate = function (models) {
    models.Fund.hasMany(models.FundSubscription, { as: 'fundInvestor', foreignKey: 'fundId' });
    models.Fund.belongsTo(models.User, { as: 'gp', targetKey: 'id', foreignKey: 'gpId' });
    models.Fund.hasMany(models.GpDelegates, { as: 'gpDelegatesSelected', foreignKey: 'fundId', sourceKey: 'id' });
    models.Fund.hasMany(models.LpDelegate, { as: 'lpDelegatesSelected', foreignKey: 'fundId', sourceKey: 'id' });
    models.Fund.hasMany(models.InvestorQuestions, { as: 'investorQuestions', foreignKey: 'fundId' });
    models.Fund.belongsTo(models.FundStatus, { as: 'fundStatus', foreignKey: 'statusId' });
    models.Fund.hasMany(models.GpSignatories, { as: 'gpSignatoriesSelected', foreignKey: 'fundId', sourceKey: 'id' });
    models.Fund.belongsTo(models.timezone, { as: 'timeZone', foreignKey: 'timezone' });
  };
  return Fund;
};
