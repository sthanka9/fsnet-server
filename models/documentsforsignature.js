'use strict';
module.exports = (sequelize, DataTypes) => {
  const DocumentsForSignature = sequelize.define('DocumentsForSignature', {
    fundId: {
      type: DataTypes.INTEGER
    },
    subscriptionId: {
      type: DataTypes.INTEGER
    },
    changeCommitmentId: {
      type: DataTypes.INTEGER
    },
    changedPagesId: {
      type: DataTypes.INTEGER
    },    
    sideletterId: {
      type: DataTypes.INTEGER
    },      
    filePath: {
      type: DataTypes.STRING
    },
    docType: {
      type: DataTypes.STRING
    },
    gpSignCount: {
      type: DataTypes.INTEGER,
      default: 0
    },
    lpSignCount: {
      type: DataTypes.INTEGER,
      default: 0
    },
    isPrimaryGpSigned: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    isPrimaryLpSigned: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    isDocumentEffectuated: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    deletedAt: {
      type: DataTypes.DATE,
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      default: true
    },
    isArchived: {
      type: DataTypes.BOOLEAN,
      default: false
    },    
    isAllGpSignatoriesSigned:{
      type: DataTypes.BOOLEAN,
      default: false
    },
    isAllLpSignatoriesSigned:{
      type: DataTypes.BOOLEAN,
      default: false
    },
    isAddedToFundAgreement: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    previousDocumentId: {
      type: DataTypes.INTEGER,
      default: null
    },
    amendmentId: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    lpSignedDate:{
      type: DataTypes.DATE,
      defaultValue: null
    },
    originalPartnershipDocumentPageCount:{
      type: DataTypes.INTEGER,
      defaultValue: null
    }
  }, {
    paranoid: true,
      hooks: {
        afterUpdate: (doc, options) => {
          if(!doc.isActive) {
            sequelize.models.SignatureTrack.update({ isActive: 0 }, {
              where: {
                documentId: doc.id
              }
            });
          }
        }
      }
    });
  DocumentsForSignature.associate = function (models) {
    models.DocumentsForSignature.belongsTo(models.ChangeCommitment, { as: 'changeCommitment', foreignKey: 'changeCommitmentId' });
    models.DocumentsForSignature.belongsTo(models.SideLetter, { as: 'changeSideletter', foreignKey: 'sideletterId' });
    models.DocumentsForSignature.hasMany(models.SignatureTrack, { as: 'signatureTrackList', sourceKey: 'subscriptionId', foreignKey: 'subscriptionId'  });
    models.DocumentsForSignature.hasOne(models.SignatureTrack, { as: 'signatureTrack', foreignKey: 'documentId'  });
    models.DocumentsForSignature.hasMany(models.SignatureTrack, { as: 'signatureTracks', sourceKey: 'id', foreignKey: 'documentId'   });
    models.DocumentsForSignature.belongsTo(models.FundSubscription, { as: 'subscription', foreignKey: 'subscriptionId' });
    models.DocumentsForSignature.belongsTo(models.FundAmmendment, { as: 'dfsamendments', foreignKey: 'amendmentId' });
    models.DocumentsForSignature.belongsTo(models.Fund, { as: 'fund', foreignKey: 'fundId' });
  };
  return DocumentsForSignature;
};
