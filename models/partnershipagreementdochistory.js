'use strict';
module.exports = (sequelize, DataTypes) => {
  const partnershipAgreementDocHistory = sequelize.define('partnershipAgreementDocHistory', {
    fundId: DataTypes.INTEGER,
    docPath:DataTypes.STRING,
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  partnershipAgreementDocHistory.associate = function(models) {
    // associations can be defined here
  };
  return partnershipAgreementDocHistory;
};