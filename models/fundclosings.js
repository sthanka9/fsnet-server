'use strict';
module.exports = (sequelize, DataTypes) => {
  const FundClosings = sequelize.define('FundClosings', {

    subscriptionId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },

    fundId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    
    lpCapitalCommitment: {
      type: DataTypes.DECIMAL(19, 2),
      get() {
        const lpCapitalCommitment = this.getDataValue('lpCapitalCommitment')
        return Number(lpCapitalCommitment);
      }
    },

    gpConsumed: {
      type: DataTypes.DECIMAL(19, 2),
      get() {
        const gpConsumed = this.getDataValue('gpConsumed')
        return Number(gpConsumed);
      }
    },
    isActive: { type: DataTypes.BOOLEAN },
    deletedAt: {
      type: DataTypes.DATE,
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    
    updatedBy: {
      type: DataTypes.INTEGER
    }

  }, { paranoid: true });
  FundClosings.associate = function (models) {
    // associations can be defined here
  };
  return FundClosings;
};