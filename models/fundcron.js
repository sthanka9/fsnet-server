'use strict';

const commonHelper = require('../helpers/commonHelper');

module.exports = (sequelize, DataTypes) => {
  const FundCron = sequelize.define('FundCron', {
    fundId: {
      type: DataTypes.INTEGER
    },
    uploadedFilePath: {
      type: DataTypes.TEXT,
      get() {
        let uploadedFilePath = this.getDataValue('uploadedFilePath');
        uploadedFilePath = uploadedFilePath ? JSON.parse(uploadedFilePath) : null;
        if (uploadedFilePath) {
          uploadedFilePath['url'] = commonHelper.getLocalUrl(uploadedFilePath.path);
          return uploadedFilePath;
        } else { return null; }
      }
    },
    uploadType:{
      type: DataTypes.STRING
    },
    status:{
      type: DataTypes.INTEGER,
      defaultValue:null   
    },
    userId: {
      type: DataTypes.INTEGER
    },
    amendmentId: {
      type: DataTypes.INTEGER
    }
  }, {});
  FundCron.associate = function(models) {
    // associations can be defined here
  };
  return FundCron;
};
