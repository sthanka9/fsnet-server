'use strict';
const config = require('../config/index')
const commonHelper = require('../helpers/commonHelper');
const path = require('path');


module.exports = (sequelize, DataTypes) => {
  var FundAgreementsHistory = sequelize.define('FundAgreementsHistory', {
    fundId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    partnershipDocument: {
      type: DataTypes.TEXT,
      get() {
        let partnershipDocument = this.getDataValue('partnershipDocument');
        partnershipDocument = partnershipDocument ? JSON.parse(partnershipDocument) : null;
        if (partnershipDocument) {
          partnershipDocument['url'] = commonHelper.getLocalUrl(partnershipDocument.path);
          return partnershipDocument;
        } else { return null; }
      }
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }

  });
  
  FundAgreementsHistory.associate = function (models) {
  };
  return FundAgreementsHistory;
};