'use strict';
module.exports = (sequelize, DataTypes) => {
  var SendMessage = sequelize.define('SendMessage', {
    message: DataTypes.TEXT,
    toUserId: DataTypes.INTEGER,
    formUserId: DataTypes.INTEGER,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {});
  SendMessage.associate = function(models) {
    
  };
  return SendMessage;
};