'use strict';
module.exports = (sequelize, DataTypes) => {
  var UserRole = sequelize.define('UserRole', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    userId: DataTypes.INTEGER,
    roleId: DataTypes.INTEGER
  }, {});
  UserRole.associate = function(models) {
    // associations can be defined here
  };
  return UserRole;
};