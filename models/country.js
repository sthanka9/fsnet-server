'use strict';
module.exports = (sequelize, DataTypes) => {
  var Country = sequelize.define('Country', {
    shortname: DataTypes.STRING,
    name: DataTypes.STRING,
    order: DataTypes.INTEGER,
    isEEACountry: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
  }, {});
  Country.associate = function(models) {
    // associations can be defined here
  };
  return Country;
};