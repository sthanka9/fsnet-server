'use strict';
module.exports = (sequelize, DataTypes) => {
  var History = sequelize.define('History', {
    accountType: DataTypes.STRING,
    userId: DataTypes.INTEGER,
    fundId: DataTypes.INTEGER,
    action:{
      type: DataTypes.STRING
    },
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
   
  }, {});
  History.associate = function(models) {
    // associations can be defined here
    models.History.belongsTo(models.User, { as: 'actionsByUser', targetKey: 'id', foreignKey: 'userId' });
  };
  return History;
};

