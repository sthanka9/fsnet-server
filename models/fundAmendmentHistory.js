'use strict';
const commonHelper = require('../helpers/commonHelper');


module.exports = (sequelize, DataTypes) => {
  const FundAmendmentHistory = sequelize.define('FundAmendmentHistory', {
    fundAmendmentId: {
      type: DataTypes.INTEGER
    },
    fundId: {
      type: DataTypes.INTEGER
    },
    document: {
      type: DataTypes.TEXT,
      get() {
        let document = this.getDataValue('document');
        document = document ? JSON.parse(document) : null;
        if (document) {
          document['url'] = commonHelper.getLocalUrl(document.path);
          return document;
        } else { return null; }
      }
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  FundAmendmentHistory.associate = function(models) {
    // associations can be defined here
  };
  return FundAmendmentHistory;
};