'use strict'; 

module.exports = (sequelize, DataTypes) => {
  const timezone = sequelize.define('timezone', {     
    displayName:{
      type: DataTypes.TEXT
    },
    code:{
      type: DataTypes.STRING
    },    
    status:{
      type: DataTypes.INTEGER,
      defaultValue:null   
    },
    isDefault: {
      type: DataTypes.BOOLEAN,
      defaultValue: 0
    }
  }, {});
  timezone.associate = function(models) {
     
  };
  return timezone;
}
