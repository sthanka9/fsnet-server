'use strict';
module.exports = (sequelize, DataTypes) => {
  var InvestorAnswers = sequelize.define('InvestorAnswers', {    
    subscriptionId: DataTypes.INTEGER,    
    questionId: DataTypes.INTEGER,
    questionResponse: DataTypes.STRING,
    deletedAt: DataTypes.DATE
    
  }, {});
  InvestorAnswers.associate = function(models) {
    models.InvestorAnswers.belongsTo(models.InvestorQuestions, { as: 'answersData', foreignKey: 'questionId' });
    models.InvestorAnswers.belongsTo(models.FundSubscription, { as: 'subscriptionAnswers', foreignKey: 'subscriptionId' });
  };
  return InvestorAnswers;
};