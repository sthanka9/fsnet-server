
const config = require('../config/index');
const models = require('../models/index');

const subscriptionSectionsDocFiles = {
    /*
    one: `/default/DefaultSection1.docx`,
    two: `/default/DefaultSection2.docx`,
    three: `/default/DefaultSection3.docx`,
    four: `/default/DefaultSection4.docx`,
    five: `/default/DefaultSection5.docx`*/

    one: `/default/DefaultSection1.pdf`,
    two: `/default/DefaultSection2.pdf`
    /*,
    three: `/default/DefaultSection3.pdf`,
    four: `/default/DefaultSection4.pdf`,
    five: `/default/DefaultSection5.pdf`    
    */
    
 };

const upload = async (req, res, next) => {

    const { fundId, subscriptionId = 0, section } = req.body;

    let originalfilename = req.file.originalname

    const sectionFileUpload = req.file.destination + req.file.filename;

    let data;

    if (subscriptionId && subscriptionId != 0) {

        const subscription = await models.FundSubscription.findOne({
            attributes: ['subscriptionAgreementPath'],
            where: {
                id: subscriptionId
            }
        });

        data = subscription.subscriptionAgreementPath || {};

        data[section] = sectionFileUpload;

        data[section+"_originalfilename"] = originalfilename
    
        await models.FundSubscription.update({
            subscriptionAgreementPath: data
        }, {
            where: {
                id: subscriptionId
            }
        });
        
        return res.json(data);
        
    } else {

        const fund = await models.Fund.findOne({
            attributes: ['subscriptionAgreementPath'],
            where: {
                id: fundId
            }
        });

        data = fund.subscriptionAgreementPath || {};

        data[section] = sectionFileUpload;

        data[section+"_originalfilename"] = originalfilename
        
        await models.Fund.update({
            subscriptionAgreementPath: data
        }, {
            where: { id: fundId }
        });

        return res.json(data);

    }


}

const download = async (req, res, next) => {

    const section = req.params.section;
    const subscriptionId = req.params.subscriptionId;
    const fundId = req.params.fundId;


    if (subscriptionId && subscriptionId !=0) {

        const subscription = await models.FundSubscription.findOne({
            attributes: ['subscriptionAgreementPath'],
            where: {
                id: subscriptionId
            }
        });

        let blobPaths = subscription.subscriptionAgreementPath;

        if(blobPaths && blobPaths[section]){
            blobPaths =blobPaths[section]
        }else{
            blobPaths= subscriptionSectionsDocFiles[section]
        }
        const url = `${config.get('serverURL')}${blobPaths.replace('./', '/')}`

        return res.json({
            url: url
        });

    } else {

        const fund = await models.Fund.findOne({
            attributes: ['subscriptionAgreementPath'],
            where: {
                id: fundId
            }
        });

        
        let blobPaths = fund.subscriptionAgreementPath;

        if(blobPaths && blobPaths[section]){
            blobPaths =blobPaths[section]
        }else{
            blobPaths=subscriptionSectionsDocFiles[section]
        }

        const url = `${config.get('serverURL')}${blobPaths.replace('./', '/')}`;

        return res.json({
            url: url
        });

    }
}

module.exports = {
    upload,
    download
}