const models = require('../models/index');
const moment = require('moment');
const reIssueSubscriptionAgreementForFund = require('./subscription/reIssueSubscriptionAgreementForFund');
const messageHelper = require('../helpers/messageHelper');
const errorHelper = require('../helpers/errorHelper');

const add = async (req, res, next) => {

    try {

        const { fundId, subscriptionId, question, questionTitle, typeOfQuestion, isRequired } = req.body;

        const fundInvestor = models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            }
        })

        if (fundInvestor && fundInvestor.status == 10) {
            // closed status
            return errorHelper.error_400(res, 'status', messageHelper.questionErrorMessage);
        }


        await models.InvestorQuestions.create({
            fundId: fundId,
            subscriptionId: subscriptionId,
            questionTitle: questionTitle,
            question: question,
            typeOfQuestion: typeOfQuestion,
            isRequired: isRequired
        })

        // if lp is in close-ready move to in progress
        //add question/update question =2
        const subscriptions = await reIssueSubscriptionAgreementForFund.run(fundId, req, true,'question',0);
        if(subscriptions.length ){
            reIssueSubscriptionAgreementForFund.additionalQuestionsModifiedAlertToLps(subscriptions, fundId, req);
        }

        return res.json({ "msg": messageHelper.questionAddSuccessMessage});

    } catch (error) {
        next(error);
    }
}

const list =  async(req, res, next) => {
    try {

        const { fundId, subscriptionId } = req.params
        
        if (!Number(fundId)) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }
        
        const questionsList = await models.InvestorQuestions.findAll({
            where: {
                fundId: fundId,
                subscriptionId: subscriptionId,
                deletedAt: null
            }
        });        
        
        return res.json({questionsList});

    } catch (error) {
        next(error);
    }
}

const update =  async(req, res, next) => {
    try {
        
        const { questionId } = req.params;

        const { fundId, subscriptionId, question, questionTitle, typeOfQuestion, isRequired } = req.body;

        const fundInvestor = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            }
        })

        if (fundInvestor && fundInvestor.status == 10) {
            // closed status
            return errorHelper.error_400(res, 'fundId', messageHelper.questionErrorMessage);
        }

        await models.InvestorQuestions.update({ 
            fundId: fundId,
            subscriptionId: subscriptionId,
            questionTitle: questionTitle, 
            question: question,
            typeOfQuestion: typeOfQuestion,
            isRequired: isRequired
        }, {
            where: {
                id: questionId
            }
        });

        await models.InvestorAnswers.update({
            deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
        }, {
            where: {
                questionId: questionId
            }
        });

        // if lp is in close-ready move to in progress
        //add question/update question =2.
        const subscriptions = await reIssueSubscriptionAgreementForFund.run(fundId, req, true,'question',0);
        reIssueSubscriptionAgreementForFund.additionalQuestionsModifiedAlertToLps(subscriptions, fundId, req);

        return res.json({ "msg": messageHelper.questionUpdateSuccessMessage});

    } catch (error) {
        next(error);
    }
}

const questionDelete =  async(req, res, next) => {
    try {
        
        const { questionId } = req.params;

        const question = await models.InvestorQuestions.findOne({
            where: {
                id: questionId
            }
        })

        await models.InvestorQuestions.update({
            deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
        }, {
            where: {
                id: questionId
            }
        });

        await models.InvestorAnswers.update({
            deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
        }, {
            where: {
                questionId: questionId
            }
        });

        // if lp is in close-ready move to in progress
        //add question/update question=3 
        const subscriptions = await reIssueSubscriptionAgreementForFund.run(question.fundId, req,'question',0);
        reIssueSubscriptionAgreementForFund.additionalQuestionsModifiedAlertToLps(subscriptions, question.fundId, req);
        

        return res.json({ "msg": messageHelper.questionDeleteSuccessMessage});

    } catch (error) {
        next(error);
    }
}

module.exports = {
    add,
    list,
    update,
    questionDelete,
}