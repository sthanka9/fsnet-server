const models = require('../models/index');
const _ = require('lodash');
const messageHelper = require('../helpers/messageHelper');
const errorHelper = require('../helpers/errorHelper');
const commonHelper = require('../helpers/commonHelper');
const logger = require('../helpers/logger');
const { Op } = require('sequelize');
const path = require('path');
const config = require('../config/index');
const uuidv1 = require('uuid/v1');
const fs = require('fs');
const scissors = require('scissors');
const moment = require('moment');
const notificationHelper = require('../helpers/notificationHelper');
const emailHelper = require('../helpers/emailHelper');
const effectuateHelper = require('../helpers/effectuateHelper');
const util = require('util');
const swapFundAmendmentsHelper = require('../helpers/swapFundAmendmentsHelper');


//upload fa and cp
const uploadFANonClosedInvestors = async (req, res, next) => {
    try {
        //input params
        const { fundId } = req.params

        logger.info('*********************FAC change for non closed investors start*********************')

        if (req.files) {       
            let nonClosedInvestorStatus = [2, 7, 16]
            //get list of investors with inprogress,close-ready
            let listSubscriptionData = await models.FundSubscription.findAll({
                where: {
                    fundId: fundId,
                    status: {
                        [Op.in]: nonClosedInvestorStatus
                    }
                },
                include: [{
                    model: models.User,
                    as: 'lp',
                    required: false
                }, {
                    model: models.OfflineUsers,
                    as: 'offlineLp',
                    required: false
                }]

            })

            let faDoc = _.find(req.files, { fieldname: 'fundagreement' })
            let cpDoc = _.find(req.files, { fieldname: 'changedpages' })

            //update previous changedpages
            await models.ChangedPage.update({ isActive: 0 }, { where: { fundId } })

            let changedPagesId = null

            //cpdoc and save 
            if (cpDoc) {
                let changedPagesinfo = await models.ChangedPage.create({
                    fundId: fundId,
                    isActive: 1,
                    documentPath: "./" + cpDoc.path,
                    createdBy: req.userId,
                    updatedBy: req.userId
                })

                changedPagesId = changedPagesinfo.id
            }

            const partnershipDocumentPageCount = await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(faDoc.path);

            //fadocument
            if (faDoc) {
                logger.info('******FAC change for non closed investors : fund agreement document added  *********************'+faDoc.path)
                //update filepath into fund table
                await models.Fund.update({
                    partnershipDocument: await commonHelper.getFileInfo(faDoc),
                    partnershipDocumentPageCount,
                    updatedBy: req.userId
                }, {
                    where: {
                        id: fundId
                    }
                })
                //update old docs and update investor status to inprogress from closed-ready as FA changes

                let faDocument = []
                for (let subscription of listSubscriptionData) {
                    let investorStatus = subscription.status
                    let isOfflineInvestor = subscription.isOfflineInvestor
                    let isInsert = true

                    //update fundsubscription with changedpages id
                    await models.FundSubscription.update({changedPagesId},{where:{id:subscription.id}})

                    //check user is in progress first time or changed from closed-ready
                    if (investorStatus == 2) {
                        let faCount = await models.DocumentsForSignature.count({
                            where: {
                                docType: 'FUND_AGREEMENT',
                                subscriptionId: subscription.id,
                                fundId: fundId
                            }
                        })

                        if (faCount == 0) {
                            isInsert = false
                        }
                    }

                    await models.SignatureTrack.update({ isActive: 0 }, {
                        where: {
                            documentType: 'FUND_AGREEMENT',
                            subscriptionId: subscription.id,
                            isActive: 1
                        }
                    })

                    await models.DocumentsForSignature.update({
                        isActive: 0,
                        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z'),
                        //isArchived:1
                    }, {
                        where: {
                            fundId: fundId,
                            docType: 'FUND_AGREEMENT',
                            subscriptionId: subscription.id,
                            isActive: 1
                        }
                    })

                    let lpSignatoriesSelected = []

                    //for online investor only
                    if (subscription.lp) {

                        lpSignatoriesSelected = await models.LpSignatories.findAll({
                            where: {
                                fundId: fundId,
                                lpId: subscription.lp.id,
                                subscriptionId: subscription.id
                            },
                            include: [{
                                model: models.User,
                                as: 'signatoryDetails',
                                required: false
                            }]
                        })
                    }                

                    //closed-ready to inprogress
                    if (investorStatus == 7) {
                        // set status to inprogress based on lp signatories
                        let status = lpSignatoriesSelected.length > 0 ? 16 : 2

                        await models.FundSubscription.update({ status: status }, { where: { id: subscription.id } })
                    }


                    if (isInsert || isOfflineInvestor) {

                        let dirToCheck = `./assets/funds/${fundId}/${subscription.id}`
                        commonHelper.ensureDirectoryExistence(dirToCheck)
                        let revisedFAInvestorPath = dirToCheck + `/${uuidv1()}.pdf`
                        fs.copyFileSync(faDoc.path, revisedFAInvestorPath)

                        faDocument.push({
                            fundId: fundId,
                            subscriptionId: subscription.id,
                            originalPartnershipDocumentPageCount: partnershipDocumentPageCount,
                            changeCommitmentId: null,
                            filePath: revisedFAInvestorPath,
                            isAllLpSignatoriesSigned: false,
                            docType: 'FUND_AGREEMENT',
                            gpSignCount: 0,
                            lpSignCount: 0,
                            isPrimaryGpSigned: 0,
                            isPrimaryLpSigned: false,
                            isActive: 1,
                            createdBy: req.userId,
                            updatedBy: req.userId
                        })

                    }

                }
                await models.DocumentsForSignature.bulkCreate(faDocument)

            }

            await models.DocumentsForSignature.update({
                isDocumentEffectuated: true
            }, {
                where: {
                    fundId: fundId,
                    docType: 'CONSOLIDATED_FUND_AGREEMENT',
                    isActive: 1,
                    isArchived: 0,
                    deletedAt: null
                }
            });

            //send alerts to lps for FA change
            sendFAChangesAlertsToNonClosedInvestors(fundId, listSubscriptionData, req, res)

            logger.info('*********************END FAC change for non closed investors *********************')

            return res.status(201).json({
                message: 'Success'
            })

        }
    } catch (e) {
        next(e)
    }
}

//send alerts for non closed investors
const sendFAChangesAlertsToNonClosedInvestors = async (fundId, subscriptions, req, res) => {

    const fund = await models.Fund.findOne({
        where: {
            id: fundId
        }
    })


    //send notification to LP
    for (let subscription of subscriptions) {

        let emailSubject = 'Fund Agreement Changed by Fund Manager'

        //for only online investor send emails
        if (subscription.isOfflineInvestor == 0) {

            const alertDataToLPandLPSignatories = {
                htmlMessage: messageHelper.fundAgreementChangeAlertDataToLPandLPSignatories,
                firstName: subscription.lp.firstName,
                lastName: subscription.lp.lastName,
                middleName: subscription.lp.middleName,
                lpId: subscription.lp.id,
                subscriptionId: subscription.id,
                fundManagerCommonName: fund.fundManagerCommonName,
                individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
                investorName: await commonHelper.getInvestorName(subscription),
                fundCommonName: fund.fundCommonName,
                fundId: fund.id,
                sentBy: req.userId
            }


            let fundAgreementChangeAlertDataToLP = messageHelper.fundAgreementChangeAlertDataToLP
            fundAgreementChangeAlertDataToLP = fundAgreementChangeAlertDataToLP.replace('[fundCommonName]', fund.fundCommonName)
            fundAgreementChangeAlertDataToLP = fundAgreementChangeAlertDataToLP.replace('[fundManagerCommonName]', fund.fundManagerCommonName)

            const alertDatatoLP = {
                htmlMessage: fundAgreementChangeAlertDataToLP,
                firstName: subscription.lp.firstName,
                lastName: subscription.lp.lastName,
                middleName: subscription.lp.middleName,
                lpId: subscription.lp.id,
                subscriptionId: subscription.id,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
                investorName: await commonHelper.getInvestorName(subscription),
                fundId: fund.id,
                sentBy: req.userId
            }

            let htmlFileName = 'fundAgreementChangedAlertToLP.html'

            //send notification to delegates       
            let lpDelegatesSelected = await models.LpDelegate.findAll({
                where: {
                    fundId: fund.id,
                    lpId: subscription.lp.id,
                    subscriptionId: subscription.id
                },
                include: [{
                    model: models.User,
                    as: 'details',
                    require: true
                }]
            })

            let lpDelegates = [];
            lpDelegatesSelected.map(i => {
                lpDelegates.push({
                    id: i.details.id,
                    firstName: i.details.firstName,
                    lastName: i.details.lastName,
                    middleName: i.details.middleName,
                    email: i.details.email,
                    isEmailNotification: i.details.isEmailNotification,
                    accountId: i.details.accountId
                })
            })

            notificationHelper.triggerNotification(req, req.userId, false, lpDelegates, alertDataToLPandLPSignatories,null,null);

            // send email
            for (let delegate of lpDelegates) {

                let data = {
                    firstName: delegate.firstName,
                    lastName: delegate.lastName,
                    middleName: delegate.middleName,
                    fundManagerCommonName: fund.fundManagerCommonName,
                    fundCommonName: fund.fundCommonName,
                    name: commonHelper.getFullName(delegate),
                    clientURL: config.get('clientURL')
                }

                logger.info('******FAC change for non closed investors : delegate Email Sent to  *********************'+delegate.email)

                emailHelper.triggerAlertEmailNotification(delegate.email, emailSubject, data, htmlFileName);

            }
            //---end      

            //send notfication to LP Signatories
            let lpSignatoriesSelected = await models.LpSignatories.findAll({
                where: {
                    fundId: fund.id,
                    lpId: subscription.lp.id,
                    subscriptionId: subscription.id
                },
                include: [{
                    model: models.User,
                    as: 'signatoryDetails',
                    required: false
                }]
            })

            let lpSignatories = [];
            lpSignatoriesSelected.map(i => {
                lpSignatories.push({
                    id: i.signatoryDetails.id,
                    firstName: i.signatoryDetails.firstName,
                    lastName: i.signatoryDetails.lastName,
                    middleName: i.signatoryDetails.middleName,
                    email: i.signatoryDetails.email,
                    isEmailNotification: i.signatoryDetails.isEmailNotification,
                    accountId: i.signatoryDetails.accountId
                })
            })

            notificationHelper.triggerNotification(req, req.userId, false, lpSignatories, alertDataToLPandLPSignatories,null,null);    

            // send email
            for (let signatory of lpSignatories) {

                let data = {
                    firstName: signatory.firstName,
                    lastName: signatory.lastName,
                    middleName: signatory.middleName,
                    fundManagerCommonName: fund.fundManagerCommonName,
                    fundCommonName: fund.fundCommonName,
                    name: commonHelper.getFullName(signatory),
                    clientURL: config.get('clientURL'),
                    investorName: await commonHelper.getInvestorName(subscription),
                    fundId: fund.id,
                }

                logger.info('******FAC change for non closed investors : signatory Email Sent to  *********************'+signatory.email)

                emailHelper.triggerAlertEmailNotification(signatory.email, emailSubject, data, 'fundAgreementChangedAlertToLP.html');
            }

            //---end


            //send notification to LP start

            if (lpSignatories.length == 0) {
                notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDataToLPandLPSignatories, subscription.lp.accountId)
            } else {
                notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDatatoLP, subscription.lp.accountId)
                htmlFileName = 'fundAgreementChangedAlertToLPSignatory.html'
            }

            logger.info('******FAC change for non closed investors : LP Email Sent to  *********************'+subscription.lp.email)

            emailHelper.sendEmail({
                toAddress: subscription.lp.email,
                subject: 'Fund Agreement Changed by Fund Manager',
                data: {
                    firstName: subscription.lp.firstName,
                    lastName: subscription.lp.lastName,
                    middleName: subscription.lp.middleName,
                    fundManagerCommonName: fund.fundManagerCommonName,
                    fundCommonName: fund.fundCommonName,
                    name: commonHelper.getFullName(subscription.lp),
                    individualContent:await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
                    investorName: await commonHelper.getInvestorName(subscription),
                    clientURL: config.get('clientURL')
                },
                htmlPath: 'alerts/' + htmlFileName
            });

            //---end

 

        }

    }
}

async function getSubscriptionsRelatedToFund(fundId) {
    return await models.Fund.findOne({
        where: {
            id: fundId
        },
        include: [{
            attributes: ['status', 'id', 'lpId'],
            model: models.FundSubscription,
            as: 'fundInvestor',
            required: true,
            where: {
                deletedAt: null
            }
        }]
    });
}

async function getAmmendments(fundId, amendmentId) {

    if (!amendmentId) {
        return await models.FundAmmendment.findAll({
            where: {
                fundId,
                isActive: 1,
                isAmmendment: true,
                isAffectuated: true
            }
        });
    } else {
        return await models.FundAmmendment.findOne({
            where: {
                id: amendmentId,
                fundId,
                isActive: 1,
                isAmmendment: true,
                isAffectuated: true
            }
        });


    }
}

const checkFundStatus = async (req, res, next) => {

    try {
        const { fundId } = req.params

        if (!Number(fundId)) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }

        let fund = await getSubscriptionsRelatedToFund(fundId);

        const fundFinalClosing = (fund.statusId == 10) ? true : false;
        const subscriptions = fund.fundInvestor;

        let atleastOnefundSubscriptionClosing = false;
        if (subscriptions.length > 0) {
            const subscriptionStatuses = _.map(subscriptions, 'status');
            // if fund has had any closings
            atleastOnefundSubscriptionClosing = (_.includes(subscriptionStatuses, 10)) ? true : false;

        }

        return res.json({
            fundFinalClosing,
            atleastOnefundSubscriptionClosing,
            isFundLocked: fund.isFundLocked
        });


    } catch (e) {
        next(e);
    }
}

const existingFAandAmmendments = async (req, res, next) => {
    try {

        const { fundId } = req.params;
        if (!Number(fundId)) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }


        let fundAgreements = [];
        let fundObj = {};

        let ammendments = [];
        let ammendmentsObj = {};

        let fund = await getSubscriptionsRelatedToFund(fundId);

        let ammendmentsList = await getAmmendments(fundId, false);


        fundObj.originalname = fund.partnershipDocument.originalname;
        fundObj.name = fund.partnershipDocument.name;
        fundObj.path = `${config.get('serverURL')}${fund.partnershipDocument.uri}`;

        fund["dataValues"].fundAgreementsData = fundObj;
        fundAgreements.push(fund);


        if (ammendmentsList.length > 0) {

            ammendmentsList.forEach(ammendment => {

                ammendmentsObj.id = ammendment.id;
                ammendmentsObj.originalname = ammendment.document.originalname;
                ammendmentsObj.name = ammendment.document.name;
                ammendmentsObj.path = `${config.get('serverURL')}${ammendment.document.uri}`;

                ammendment.ammendmentsData = ammendmentsObj;
                ammendments.push(ammendment);

            })
        }

        return res.json({
            fundAgreements,
            ammendments

        });


    } catch (e) {
        next(e);
    }
}

const withoutInvestorConsentRFA = async (req, res, next) => {
    try {

        const { fundId, uploadType } = req.params;

        const { amendmentId } = req.body;

        let fund = await getSubscriptionsRelatedToFund(fundId);

        let subscriptionIds = _.map(fund.fundInvestor, 'id');

        const filePath = req.file.destination + req.file.filename;
        let uploadedFilePath = path.join(__dirname, '../' + filePath);


        const fundAgreementPath = path.join(__dirname.replace('controllers', ''), fund.partnershipDocument.path);

        const existingFAPageCount = await scissors(fundAgreementPath).getNumPages();

        if (!existingFAPageCount) {
            return res.json({ error: true, message: "No fund agreement found." })
        }

        let count = 0;
        if (subscriptionIds.length) {

            const ext = path.extname(req.file.originalname);

            //Documents where investors are signed
            documents = await models.DocumentsForSignature.findAll({
                where: {
                    fundId,
                    subscriptionId: {
                        [Op.or]: [{ [Op.in]: subscriptionIds }, null]

                    },
                    docType: ['FUND_AGREEMENT', 'CONSOLIDATED_FUND_AGREEMENT'],
                    deletedAt: null,
                    isActive: 1,
                    isArchived: 0,
                    isPrimaryLpSigned: 1
                }
            });


            //Documents where no investors are signed
            documentsWhereNoInvestorSigned = await models.DocumentsForSignature.findAll({
                where: {
                    fundId,
                    subscriptionId: {
                        [Op.or]: [{ [Op.in]: subscriptionIds }, null]
                    },
                    docType: ['FUND_AGREEMENT', 'CONSOLIDATED_FUND_AGREEMENT'],
                    deletedAt: null,
                    isActive: 1,
                    isArchived: 0,
                    isPrimaryLpSigned: 0
                }
            });


            //Check if atleast one doc exists in DFS
            if (documents.length) {
                console.log("====INVESTORS SIGNED====")

                //Get consoidated FA if exists
                let cfaExisitngDoc = _.find(documents, { fundId: fundId, docType: 'CONSOLIDATED_FUND_AGREEMENT' });

                documents.forEach(async document => {

                    // let faExistingDoc = _.find(documents,{fundId:fundId,subscriptionId:document.subscriptionId,docType:'FUND_AGREEMENT'});

                    let faFilePathToSave = `./assets/funds/${fundId}/${document.subscriptionId}/FUND_AGREEMENT_${uuidv1()}${ext}`;

                    const faOutputFilePath = path.resolve(__dirname, '.' + faFilePathToSave);

                    if (document && document.docType == 'FUND_AGREEMENT') {
                        //status for logs 0-start 1-success 2-fail
                        const logs = await models.LogsTable.create({
                            fundId,
                            subscriptionId: document.subscriptionId,
                            status: 0,
                            docType: 'FUND_AGREEMENT',
                            createdBy: req.userId,
                            startTime: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
                        });

                        count = count + 1

                        let data = {
                            existingAgreement: document.filePath,
                            existingAgreementPageCount: existingFAPageCount,
                            uploadedFilePath: uploadedFilePath,
                            outputFilePath: faOutputFilePath,
                            fundId: fundId,
                            subscriptionId: document.subscriptionId,
                            logs: logs,
                            faFilePathToSave: faFilePathToSave,
                            docType: 'FUND_AGREEMENT',
                            // docTypes: cfaExisitngDoc ? ['FUND_AGREEMENT','CONSOLIDATED_FUND_AGREEMENT']:['FUND_AGREEMENT'],
                            user: req.user,
                            uploadedFile: req.file,
                            fund: fund,
                            length: documents.length,
                            amendmentId: amendmentId,
                            count
                        }


                        swapFundAmendmentsHelper.swapAgreements(data);

                    }

                });

                //If there is a consolidated FA
                let cfaFilePathToSave = `./assets/funds/${fundId}/CONSOLIDATED_FUND_AGREEMENT_${uuidv1()}${ext}`;

                const cfaOutputFilePath = path.resolve(__dirname, '.' + cfaFilePathToSave);

                if (cfaExisitngDoc) {
                    //status for logs 0-start 1-success 2-fail
                    const logs = await models.LogsTable.create({
                        fundId,
                        status: 0,
                        docType: 'CONSOLIDATED_FUND_AGREEMENT',
                        createdBy: req.userId,
                        startTime: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
                    });

                    count = count + 1

                    let data = {
                        existingAgreement: cfaExisitngDoc.filePath,
                        existingAgreementPageCount: existingFAPageCount,
                        uploadedFilePath: uploadedFilePath,
                        outputFilePath: cfaOutputFilePath,
                        fundId: fundId,
                        subscriptionId: null,
                        logs: logs,
                        faFilePathToSave: cfaFilePathToSave,
                        docType: 'CONSOLIDATED_FUND_AGREEMENT',
                        // docTypes: ['FUND_AGREEMENT','CONSOLIDATED_FUND_AGREEMENT'],
                        user: req.user,
                        uploadedFile: req.file,
                        fund: fund,
                        subscriptionIds: subscriptionIds,
                        length: documents.length,
                        amendmentId: amendmentId,
                        count
                    }

                    swapFundAmendmentsHelper.swapAgreements(data);

                }

                models.FundAgreementsHistory.create({
                    fundId: fundId,
                    partnershipDocument: JSON.stringify(fund.partnershipDocument),
                    createdBy: req.userId,
                    updatedBy: req.userId
                });


            }

            if (documentsWhereNoInvestorSigned.length) {

                documentsWhereNoInvestorSigned.forEach(document => {

                    console.log("====NO INVESTORS SIGNED====")

                    let faFilePathToSave = `./assets/funds/${fundId}/${document.subscriptionId}/FUND_AGREEMENT_${uuidv1()}${ext}`;

                    //Update with the new file path 
                    models.DocumentsForSignature.update({
                        filePath: faFilePathToSave
                    }, {
                        where: {
                            fundId,
                            subscriptionId: document.subscriptionId,
                            docType: 'FUND_AGREEMENT',
                            deletedAt: null,
                            isActive: 1,
                            isArchived: 0,
                            isPrimaryLpSigned: 0
                        }
                    });
                })

                models.Fund.update({
                    // isFundLocked: false,
                    partnershipDocument: await commonHelper.getFileInfo(req.file, true),
                    updatedBy: req.userId
                }, {
                    where: {
                        id: fundId
                    }
                });

            }

            if (!documents.length && !documentsWhereNoInvestorSigned.length) {
                console.log("====NO DOCUMENTS====")

                models.Fund.update({
                    // isFundLocked: false,
                    partnershipDocument: await commonHelper.getFileInfo(req.file, true),
                    updatedBy: req.userId
                }, {
                    where: {
                        id: fundId
                    }
                });

            }

            models.FundAgreementsHistory.create({
                fundId: fundId,
                partnershipDocument: JSON.stringify(fund.partnershipDocument),
                createdBy: req.userId,
                updatedBy: req.userId
            });

            return res.send('Success');


        } else {
            return res.json({ error: true, message: "No Subscriptions found." })

        }


    } catch (e) {
        next(e);
    }
}

const withoutInvestorConsentAmendment = async (req, res, next) => {
    try {

        const { fundId, uploadType } = req.params;

        const { amendmentId } = req.body;


        let fund = await getSubscriptionsRelatedToFund(fundId);

        let subscriptionIds = _.map(fund.fundInvestor, 'id');

        let ammendment = await getAmmendments(fundId, amendmentId);

        const filePath = req.file.destination + req.file.filename;
        let uploadedFilePath = path.join(__dirname, '../' + filePath);

        const ammendmentPath = path.join(__dirname.replace('controllers', ''), ammendment.document.path);

        const existingAmmendmentPageCount = await scissors(ammendmentPath).getNumPages();

        if (!existingAmmendmentPageCount) {
            return res.json({ error: true, message: "No Ammendments found." })
        }

        let count = 0;
        if (subscriptionIds.length) {

            const ext = path.extname(req.file.originalname);

            //Documents where investors are signed
            documents = await models.DocumentsForSignature.findAll({
                where: {
                    fundId,
                    subscriptionId: {
                        [Op.or]: [{ [Op.in]: subscriptionIds }, null]
                    },
                    docType: ['AMENDMENT_AGREEMENT', 'CONSOLIDATED_AMENDMENT_AGREEMENT'],
                    amendmentId,
                    isActive: 1,
                    isArchived: 0,
                    isPrimaryLpSigned: 1
                }
            });

            //Documents where no investors are signed
            documentsWhereNoInvestorSigned = await models.DocumentsForSignature.findAll({
                where: {
                    fundId,
                    subscriptionId: {
                        [Op.or]: [{ [Op.in]: subscriptionIds }, null]
                    },
                    docType: ['AMENDMENT_AGREEMENT', 'CONSOLIDATED_AMENDMENT_AGREEMENT'],
                    amendmentId,
                    isActive: 1,
                    isArchived: 0,
                    isPrimaryLpSigned: 0
                }
            });

            let cAmmendmentExisitngDoc = _.find(documents, { fundId: fundId, amendmentId: Number(amendmentId), docType: 'CONSOLIDATED_AMENDMENT_AGREEMENT' });

            let ammendmentFilePathToSave = `./assets/funds/${fundId}/ammendments/AMENDMENT_AGREEMENT_${uuidv1()}${ext}`;

            const ammendmentOutputFilePath = path.resolve(__dirname, '.' + ammendmentFilePathToSave);


            //Replica of existing record(Archival)
            models.FundAmmendment.findOne({
                where: {
                    fundId: fundId,
                    id: amendmentId,
                    isAmmendment: true,
                    isAffectuated: true,
                    isActive: 1,
                    isArchived: 0
                },
                raw: true
            }).then(async data => {

                //Archive existing doc
                delete data.id
                // data.deletedAt = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
                data.isActive = 0
                data.createdBy = req.userId
                data.updatedBy = req.userId
                data.isArchived = 1

                await models.FundAmmendment.create(data);

                //Update with the new file path 
                await models.FundAmmendment.update({
                    document: await commonHelper.getFileInfo(req.file, true)
                }, {
                    where: {
                        fundId: fundId,
                        id: amendmentId,
                        isAmmendment: true,
                        isAffectuated: true,
                        isActive: 1,
                        isArchived: 0
                    }
                });

            });

            //If only lp is signed on the document
            if (documents.length) {
                console.log("====INVESTORS SIGNED====")

                documents.forEach(async document => {

                    if (document && document.docType == 'AMENDMENT_AGREEMENT') {
                        //status for logs 0-start 1-success 2-fail
                        const logs = await models.LogsTable.create({
                            fundId,
                            subscriptionId: document.subscriptionId,
                            status: 0,
                            docType: 'AMENDMENT_AGREEMENT',
                            createdBy: req.userId,
                            startTime: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
                        });

                        count = count + 1
                        let data = {
                            existingAgreement: document.filePath,
                            existingAgreementPageCount: existingAmmendmentPageCount,
                            uploadedFilePath: uploadedFilePath,
                            outputFilePath: ammendmentOutputFilePath,
                            fundId: fundId,
                            subscriptionId: document.subscriptionId,
                            logs: logs,
                            faFilePathToSave: ammendmentFilePathToSave,
                            docType: 'AMENDMENT_AGREEMENT',
                            fund: fund,
                            subscriptionIds: subscriptionIds,
                            amendmentId: amendmentId,
                            user: req.user,
                            count: count,
                            length: documents.length,
                            uploadedFile: req.file

                        };

                        swapFundAmendmentsHelper.swapAgreements(data);


                    }

                });

                let cAmmendmentFilePathToSave = `./assets/funds/${fundId}/ammendments/CONSOLIDATED_AMENDMENT_AGREEMENT_${uuidv1()}${ext}`;

                const cAmmendmentOutputFilePath = path.resolve(__dirname, '.' + cAmmendmentFilePathToSave);


                if (cAmmendmentExisitngDoc) {
                    //status for logs 0-start 1-success 2-fail
                    const logs = await models.LogsTable.create({
                        fundId,
                        status: 0,
                        createdBy: req.userId,
                        docType: 'CONSOLIDATED_AMENDMENT_AGREEMENT',
                        startTime: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
                    });

                    count = count + 1;

                    let data = {
                        existingAgreement: cAmmendmentExisitngDoc.filePath,
                        existingAgreementPageCount: existingAmmendmentPageCount,
                        uploadedFilePath: uploadedFilePath,
                        outputFilePath: cAmmendmentOutputFilePath,
                        fundId: fundId,
                        subscriptionId: null,
                        logs: logs,
                        faFilePathToSave: cAmmendmentFilePathToSave,
                        docType: 'CONSOLIDATED_AMENDMENT_AGREEMENT',
                        fund: fund,
                        subscriptionIds: subscriptionIds,
                        amendmentId: amendmentId,
                        // docTypes: cAmmendmentExisitngDoc ? ['AMENDMENT_AGREEMENT','CONSOLIDATED_AMENDMENT_AGREEMENT']:['AMENDMENT_AGREEMENT'],
                        user: req.user,
                        count: count,
                        length: documents.length,
                        uploadedFile: req.file
                    };

                    swapFundAmendmentsHelper.swapAgreements(data);

                }
            }

            if (documentsWhereNoInvestorSigned.length) {

                console.log("====NO INVESTORS SIGNED====")

                models.DocumentsForSignature.update({
                    filePath: ammendmentFilePathToSave
                }, {
                    where: {
                        fundId,
                        subscriptionId: {
                            [Op.in]: subscriptionIds
                        },
                        docType: 'AMENDMENT_AGREEMENT',
                        amendmentId,
                        isActive: 1,
                        isArchived: 0,
                        isPrimaryLpSigned: 0
                    }
                });

            }

            return res.send('Success');

        } else {
            return res.json({ error: true, message: "No Subscriptions found." })

        }



    } catch (e) {
        next(e);
    }
}

const uploadDocsToClosedInvestors = async (req, res, next) => {
    try {
        const { fundId, uploadType } = req.params
        const { targetPercentage, considerOnlyLpOfferedAmount } = req.body     

        logger.info('******FAC change for closed investors Start***********')

        const fund = await models.Fund.findOne({
            attributes: ['noOfSignaturesRequiredForFundAmmendments'],
            where: {
                id: fundId
            }
        })
        let faDoc = _.find(req.files, { fieldname: 'fundDoc' })
        let changedPagesId = null
        //changed pages for revised fund agreement
        if (uploadType == 'Fund_Agreement') {
            let cpDoc = _.find(req.files, { fieldname: 'changedpages' })
            if (cpDoc) {
                let changedPages =  await models.ChangedPage.create({
                    fundId: fundId,
                    isActive: 0,
                    documentPath: "./" + cpDoc.path,
                    createdBy: req.userId,
                    updatedBy: req.userId
                })
                changedPagesId = changedPages.id
            }
        }         

        //Create Fund Ammendment
        let ammendments = await models.FundAmmendment.create({
            fundId,
            considerOnlyLpOfferedAmount,
            document: await commonHelper.getFileInfo(faDoc),
            targetPercentage,
            createdBy: req.userId,
            updatedBy: req.userId,
            isAmmendment: (uploadType == 'Amendment') ? true : false,
            noOfSignaturesRequiredForFundAmmendments: fund.noOfSignaturesRequiredForFundAmmendments,
            changedPagesId
        })
        //get ammendment id
        let amendmentId = ammendments.id
        let uploadedDocType = (uploadType == 'Fund_Agreement') ? 'FUND_AGREEMENT' : 'AMENDMENT_AGREEMENT'    

    

        //uploaded type document        
        if (uploadType == 'Fund_Agreement') {
            uploadedDocType = 'FUND_AGREEMENT'
        } else if (uploadType == 'Amendment') {
            uploadedDocType = 'AMENDMENT_AGREEMENT'
        } 

        const partnershipDocumentPageCount = await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(faDoc.path);     

        //get closed investors info
        let listSubscriptionData = await models.FundSubscription.findAll({
            attributes: ['id', 'status', 'lpId'],
            where: {
                status: 10,
                fundId: fundId,
                deletedAt: null
            }
        })

        for (let subscription of listSubscriptionData) {

            let subscriptionId = subscription.id

            logger.info('******FAC change for closed investors***subscription id********'+subscriptionId)

            //insert records to documentsforsignatures 
            let dirToCheck = `./assets/funds/${fundId}/${subscriptionId}`
            commonHelper.ensureDirectoryExistence(dirToCheck)
            let revisedPath = dirToCheck + `/${uuidv1()}${subscriptionId}.pdf`
            fs.copyFileSync(faDoc.path, revisedPath)

            //upload doc           
            let data = {
                fundId: fundId,
                subscriptionId: subscriptionId,
                originalPartnershipDocumentPageCount: uploadType == 'Fund_Agreement' ? partnershipDocumentPageCount : null,
                filePath: revisedPath,
                docType: uploadedDocType,
                isAllLpSignatoriesSigned: false,
                gpSignCount: 0,
                lpSignCount: 0,
                isPrimaryGpSigned: 0,
                isPrimaryLpSigned: false,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId,
                amendmentId: amendmentId,
                changedPagesId
            }
            //save to documentforsignatures
            await models.DocumentsForSignature.create(data)
        }


        //send alerts to lps for FA change
        sendFAChangesAlertsToClosedInvestors(req, res, fundId, uploadedDocType)

        logger.info('******END FAC change for closed investors***********')

        return res.send('Success')
    } catch (e) {
        next(e);
    }
}

const ammendmentSummary = async (req, res, next) => {
    try {
        const fundId = req.params.fundId;

        let fundAmmendments = await models.FundAmmendment.findAll({
            where: {
                fundId,
                isAffectuated: false,
                isTerminated: false,
                isArchived: false,
                isActive: true

            },
            include: [{
                attributes: ['timezone'],
                model: models.Fund,
                as: 'fund',
                required: false,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }]
            },{
                model: models.GpSignatoryApproval,
                as: 'gpSignatoryAprovedFundAmmendments',
                required: false
            }]
        });

        let amendmentIds = _.map(fundAmmendments, 'id');

        let ammendmentsOrRevisedAgreements = [];
        let documents = await models.DocumentsForSignature.findAll({
            where: {
                amendmentId: {
                    [Op.in]: amendmentIds
                },
                isArchived: false,
                isActive: true,
                deletedAt: null,
            }
        });

        let isSecondaryGpEffectuate = true;
        let isGpEffectuate = false;
        let isSecondaryGpApproved;

        let currentSignatoryId = req.userId;

        for (let fundAmmendment of fundAmmendments) {
            let investorsApproved = _.filter(documents, document => {
                if (document.amendmentId == fundAmmendment.id && document.isPrimaryLpSigned) {
                    return document
                }
            })

            if (req.user.accountType == 'SecondaryGP') {

                isSecondaryGpEffectuate = fundAmmendment.noOfSignaturesRequiredForFundAmmendments == 1 ? false : (_.includes(_.map(fundAmmendment.gpSignatoryAprovedFundAmmendments, 'signatoryId'), currentSignatoryId) ? false : true);
                isSecondaryGpApproved = fundAmmendment.noOfSignaturesRequiredForFundAmmendments == 1 ? false : _.includes(_.map(fundAmmendment.gpSignatoryAprovedFundAmmendments, 'signatoryId'), currentSignatoryId)


            } else if (req.user.accountType == 'GP') {

                // To cross check whether to include GP in the settings count  
                isGpEffectuate = fundAmmendment.noOfSignaturesRequiredForFundAmmendments == 1 ? true : (fundAmmendment.noOfSignaturesRequiredForFundAmmendments == Number(_.map(fundAmmendment.gpSignatoryAprovedFundAmmendments, 'signatoryId').length) + 1)
            }

            let issued_date = await commonHelper.getDateTimeWithTimezone(fundAmmendment.fund.timeZone, fundAmmendment.createdAt, 1);
            let affectuated_date = await commonHelper.getDateTimeWithTimezone(fundAmmendment.fund.timeZone, fundAmmendment.affectuatedDate, 1);

            ammendmentsOrRevisedAgreements.push({
                isAmmendment:fundAmmendment.isAmmendment,
                issuedDate: issued_date,
                affectuatedDate: affectuated_date,
                targetPercentage: fundAmmendment.targetPercentage,
                approvedPercentage: fundAmmendment.approvedPercentage ? fundAmmendment.approvedPercentage : 0,
                investorsApproved: investorsApproved.length,
                amendmentId: fundAmmendment.id,
                isSecondaryGpEffectuate,
                isGpEffectuate,
                documentPath: `${config.get('serverURL')}/api/v1/document/view/displayamendment/${fundAmmendment.id}`,
                isSecondaryGpApproved
            })
        }


        return res.json(ammendmentsOrRevisedAgreements);

    } catch (e) {
        next(e)
    }
}

const investorsDetails = async (req, res, next) => {
    try {

        const amendmentId = req.params.amendmentId;
        const fundId = req.params.fundId;


        let documents = await models.DocumentsForSignature.findAll({
            where: {
                amendmentId,
                fundId,
                isArchived: false,
                isActive: true,
                deletedAt: null,
                isPrimaryLpSigned: true
            }
        });

        let subscriptionIds = _.map(documents, 'subscriptionId');

        const investors = await models.FundSubscription.findAll({
            where: {
                fundId,
                id: {
                    [Op.in]: subscriptionIds
                },
                deletedAt: null
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }],
            },{
                model: models.User,
                required: false,
                as: 'lp'
            }, {
                model: models.OfflineUsers,
                as: 'offlineLp',
                required: false
            }]
        });

        let investorsDetails = [];

        for (let investor of investors) {

            let onlineOrOfflineInv =  investor.lp ?  investor.lp : investor.offlineLp;

            let ammendmentSignedDate = _.find(documents, { fundId: fundId, subscriptionId: investor.id, amendmentId: amendmentId }).lpSignedDate
            let docType = _.find(documents, { fundId: fundId, amendmentId: amendmentId }).docType

            let investorname = ((investor.investorType == 'LLC' ? investor.entityName :
                (investor.investorType == 'Trust' ? investor.trustName :
                    (investor.areYouSubscribingAsJointIndividual ? investor.legalTitleDesignationPrettyForPDF : '')))
                || (investor.organizationName ? commonHelper.getFullName(onlineOrOfflineInv) + ' - ' + investor.organizationName : commonHelper.getFullName(onlineOrOfflineInv)) ||
                commonHelper.getFullName(onlineOrOfflineInv))


            let investorCommonname = (investor.isOfflineInvestor) ? commonHelper.getFullName(investor.offlineLp) : commonHelper.getFullName(investor.lp)

            let ammendment_signed_date = commonHelper.getDateTimeWithTimezone(investor.fund.timeZone, ammendmentSignedDate, 1, 0, 1);

            investorsDetails.push({
                //investorName: investor.lp ? commonHelper.getFullName(investor.lp):commonHelper.getFullName(investor.offlineLp),
                investorName: (investor.investorType == 'Individual' && investor.areYouSubscribingAsJointIndividual != 1) ? investorname : investorCommonname + ' - ' + investorname,
                ammendmentSignedDate: ammendment_signed_date,
                timeZone: investor.fund.timeZone && investor.fund.timeZone.displayName ? investor.fund.timeZone.displayName : '',
                profilePic: investor.lp ? investor.lp.profilePic : investor.offlineLp.profilePic,
                docType
            });

        }


        return res.json(investorsDetails);

    } catch (e) {
        next(e)
    }
}

//send alerts for non closed investors
const sendFAChangesAlertsToClosedInvestors = async (req, res, fundId, type) => {
    logger.info('closed investors sending alerts.')
 
    //get fund details
    const fund = await models.Fund.findOne({
        where: {
            id: fundId
        }
    })
    // get subscriptions info
    let subscriptions = await models.FundSubscription.findAll({
        where: {
            fundId: fundId,
            status: 10
        },
        include: [{
            model: models.User,
            as: 'lp',
            required: true
        }]
    })
 

    //send notification to LP
    for (let subscription of subscriptions) {

        let emailSubject = (type == 'AMENDMENT_AGREEMENT') ? 'Amendment to the Fund Agreement has been issued' : 'Request for your consent to Revised Fund Agreement'

        let investorName = await commonHelper.getInvestorName(subscription);

        let alertDataToLPandLPSignatories = {
            htmlMessage: type == 'AMENDMENT_AGREEMENT' ? messageHelper.AmmendmentClosedLPandLPSignatories : messageHelper.FAClosedLPandLPSignatories,
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId,
            investorName: investorName,
            individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
        }

        let fundAgreementChangeAlertDataToLP = type == 'AMENDMENT_AGREEMENT' ? messageHelper.AmmendmentDataToClosedLP : messageHelper.FADataToClosedLP
        fundAgreementChangeAlertDataToLP = fundAgreementChangeAlertDataToLP.replace('[fundCommonName]', fund.fundCommonName)
        fundAgreementChangeAlertDataToLP = fundAgreementChangeAlertDataToLP.replace('[fundManagerCommonName]', fund.fundManagerCommonName)

        let alertDatatoLP = {
            htmlMessage: fundAgreementChangeAlertDataToLP,
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId,
            investorName: investorName,
            individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
        }

        let htmlFileName = (type == 'AMENDMENT_AGREEMENT') ? 'AmmendmentDataToClosedLP.html' : 'FADataToClosedLP.html'

  

        //send notification to delegates       
        let lpDelegatesSelected = await models.LpDelegate.findAll({
            where: {
                fundId: fund.id,
                lpId: subscription.lp.id,
                subscriptionId: subscription.id
            },
            include: [{
                model: models.User,
                as: 'details',
                require: true
            }]
        })

        let lpDelegates = [];
        lpDelegatesSelected.map(i => {
            lpDelegates.push({
                id: i.details.id,
                firstName: i.details.firstName,
                lastName: i.details.lastName,
                middleName: i.details.middleName,
                email: i.details.email,
                isEmailNotification: i.details.isEmailNotification,
                accountId: i.details.accountId
            })
        })

        notificationHelper.triggerNotification(req, req.userId, false, lpDelegates, alertDataToLPandLPSignatories,null,null);

        // send email
        for (let delegate of lpDelegates) {

            let data = {
                firstName: delegate.firstName,
                lastName: delegate.lastName,
                middleName: delegate.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                name: commonHelper.getFullName(delegate),
                clientURL: config.get('clientURL')
            } 

            logger.info('******FAC change for closed investors***delegate Email Sent to********'+delegate.email)

            emailHelper.triggerAlertEmailNotification(delegate.email, emailSubject, data, htmlFileName);

        }
        //---end  


        //send notfication to LP Signatories
        let lpSignatoriesSelected = await models.LpSignatories.findAll({
            where: {
                fundId: fund.id,
                lpId: subscription.lp.id,
                subscriptionId: subscription.id
            },
            include: [{
                model: models.User,
                as: 'signatoryDetails',
                required: false
            }]
        })

        let lpSignatories = [];
        lpSignatoriesSelected.map(i => {
            lpSignatories.push({
                id: i.signatoryDetails.id,
                firstName: i.signatoryDetails.firstName,
                lastName: i.signatoryDetails.lastName,
                middleName: i.signatoryDetails.middleName,
                email: i.signatoryDetails.email,
                isEmailNotification: i.signatoryDetails.isEmailNotification,
                accountId: i.signatoryDetails.accountId
            })
        })

        notificationHelper.triggerNotification(req, req.userId, false, lpSignatories, alertDataToLPandLPSignatories,null,null);

        // send email
        let htmlFileNameSignatories = (type == 'AMENDMENT_AGREEMENT') ? 'AmmendmentDataToClosedLP.html' : 'FADataToClosedLP.html'

        for (let signatory of lpSignatories) {

            let data = {
                firstName: signatory.firstName,
                lastName: signatory.lastName,
                middleName: signatory.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                name: commonHelper.getFullName(signatory),
                investorName: investorName,
                individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
                clientURL: config.get('clientURL')
            } 

            logger.info('******FAC change for closed investors***Signatory Email Sent to********'+signatory.email)


            emailHelper.triggerAlertEmailNotification(signatory.email, emailSubject, data, htmlFileNameSignatories);

        }


        //---end


        //send notification to LP start

        if (lpSignatories.length == 0) {
            notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDataToLPandLPSignatories, subscription.lp.accountId, null)
        } else {
            notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDatatoLP, subscription.lp.accountId, null)
            htmlFileName = (type == 'AMENDMENT_AGREEMENT') ? 'AmmendmentClosedLPandLPSignatories.html' : 'FAClosedLPandLPSignatories.html'
        } 

        logger.info('******FAC change for closed investors***Investpr Email Sent to********'+subscription.lp.email)

        emailHelper.sendEmail({
            toAddress: subscription.lp.email,
            subject: emailSubject,
            data: {
                firstName: subscription.lp.firstName,
                lastName: subscription.lp.lastName,
                middleName: subscription.lp.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                name: commonHelper.getFullName(subscription.lp),
                clientURL: config.get('clientURL'),
                user: subscription.lp,
                individualContent:await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
                investorName: await commonHelper.getInvestorName(subscription),
            },
            htmlPath: 'alerts/' + htmlFileName
        });

        //---end

    }

}


//effectuatetheammendment
const effectuateAmendment = async (req, res, next) => {
    try {
        const { amendmentId, fundId } = req.body
        let docType = ''

        logger.info('******Effectuate Amendment********')        

        //get ammendmentinfo
        let ammendmentInfo = await models.FundAmmendment.findOne({
            where: {
                id: amendmentId,
                isActive: 1,
                isArchived: 0
            }
        })

        let approvedPercentage = ammendmentInfo.approvedPercentage

        //effectute should happen atlease for one sign
        if (approvedPercentage == null || approvedPercentage <= 0) {
            return errorHelper.error_403(res, 'documentId', messageHelper.noDataFound);
        }

        if (req.user.accountType == 'SecondaryGP') {
            let currentSignatoryId = req.userId;

            await models.GpSignatoryApproval.create({
                fundId,
                amendmentId,
                signatoryId: currentSignatoryId
            });

            return res.json({ message: "Signatory has approved to Effectuate." });

        } else {

            //update the effectuate data

            await models.FundAmmendment.update({
                isAffectuated: 1,
                affectuatedDate: moment().format('M/D/YYYY')
            }, {
                where: {
                    id: amendmentId
                }
            })

            let fileObject = ammendmentInfo.document
            let isAmmendment = ammendmentInfo.isAmmendment
            let changedPagesId =  ammendmentInfo.changedPagesId 

            //delete the old unsigned docs from DFS and remove the pending action               
            await models.DocumentsForSignature.update({
                deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z'),
                isActive: 0,
                updatedBy: req.userId
            }, {
                where: {
                    amendmentId: amendmentId,
                    isPrimaryLpSigned: 0
                }
            })

            const partnershipDocumentPageCount = await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(fileObject.path);

            //send pending action type
            if (isAmmendment) {
                docType = 'AMENDMENT_AGREEMENT'

            } else {
                docType = 'FUND_AGREEMENT'

                if(changedPagesId!=null && changedPagesId>0){
                    //update old changed pages status to 0
                    await models.ChangedPage.update({ isActive: 0 }, { where: { fundId } })
                    //new changedpages is 1
                    await models.ChangedPage.update({ isActive:1 },{ where:{ id:changedPagesId }})
                }else{

                    //Change all changed pages to inactive when only FA is effectuated
                    await models.ChangedPage.update({ isActive: 0 }, { where: { fundId } })
                }

                //get fund info
                const fund = await models.Fund.findOne({
                    attributes: ['partnershipDocument'],
                    where: {
                        id: ammendmentInfo.fundId
                    }
                })


                await models.DocumentsForSignature.update({
                    isActive: 0,
                }, {
                    where: {
                        fundId: ammendmentInfo.fundId,
                        isActive: 1,
                        docType: 'AMENDMENT_AGREEMENT'
                    }
                })

                //update to history
                await models.FundAgreementsHistory.create({
                    fundId: ammendmentInfo.fundId,
                    partnershipDocument: JSON.stringify(fund.partnershipDocument),
                    createdBy: req.userId,
                    updatedBy: req.userId
                })

                //old amendments with effectuated will be archived 
                await models.FundAmmendment.update({
                    deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z'),
                    isActive: 0,
                    isArchived: 1
                }, {
                    where: {
                        isActive: 1,
                        isAmmendment: true,
                        isAffectuated: true
                    }
                })

                //update filepath into fund table
                await models.Fund.update({
                    partnershipDocument: JSON.stringify(fileObject),
                    updatedBy: req.userId,
                    partnershipDocumentPageCount: partnershipDocumentPageCount
                }, {
                    where: {
                        id: ammendmentInfo.fundId
                    }
                })

            }

            //change closed ready investors to inprogress and add pending action to sign ammendment agreement
            let listSubscriptionData = await models.FundSubscription.findAll({
                attributes: ['id', 'status', 'lpId'],
                where: {
                    status: {
                        [Op.in]: [2, 7, 16]
                    },
                    fundId: ammendmentInfo.fundId,
                    deletedAt: null
                }
            })

            for (let subscription of listSubscriptionData) {

                let subscriptionId = subscription.id
                let isInsert = true
                let investorStatus = subscription.status
                let isOfflineInvestor = subscription.isOfflineInvestor


                //update fundsubscription with changedpages id
                await models.FundSubscription.update({changedPagesId},{where:{id:subscription.id}})


                if (investorStatus == 2 ) {
                    let faCount = await models.DocumentsForSignature.count({
                        where: {
                            docType: 'FUND_AGREEMENT',
                            subscriptionId: subscription.id,
                            fundId: ammendmentInfo.fundId
                        }
                    })

                    if (faCount == 0) {
                        isInsert = false
                    }
                }

                let ammendmentFilePath = fileObject.path

                //change investor status to inprogress if user in closed ready
                if (investorStatus == 7) {
                    await models.FundSubscription.update({ status: 2 }, { where: { id: subscriptionId } })
                }

                //insert records to documentsforsignatures 

                let dirToCheck = `./assets/funds/${ammendmentInfo.fundId}/${subscriptionId}`
                commonHelper.ensureDirectoryExistence(dirToCheck)
                let revisedPath = dirToCheck + `/${uuidv1()}${subscriptionId}.pdf`
                fs.copyFileSync(ammendmentFilePath, revisedPath)

         
                if (isInsert || isOfflineInvestor) {
                    //upload doc           
                    let data = {
                        fundId: ammendmentInfo.fundId,
                        subscriptionId: subscriptionId,
                        filePath: revisedPath,
                        docType: docType,
                        isAllLpSignatoriesSigned: false,
                        gpSignCount: 0,
                        lpSignCount: 0,
                        isPrimaryGpSigned: 0,
                        isPrimaryLpSigned: false,
                        isActive: 1,
                        createdBy: req.userId,
                        updatedBy: req.userId,
                        amendmentId: amendmentId,
                        originalPartnershipDocumentPageCount: isAmmendment ? null : partnershipDocumentPageCount 
                    }
                    //save to documentforsignatures
                    await models.DocumentsForSignature.create(data)
                }
            }

            //inarchive old fund agreements

            let getFundSubscriptionIds = await models.FundSubscription.findAll({
                attributes: ['id', 'status'],
                where: {
                    fundId: ammendmentInfo.fundId
                    //status:10
                }
            })

            for (let subscription of getFundSubscriptionIds) {

                let faCount = await models.DocumentsForSignature.count({
                    where: {
                        docType: 'FUND_AGREEMENT',
                        isActive: 1,
                        deletedAt: null,
                        subscriptionId: subscription.id,
                        fundId: fundId
                    }
                })
                //if at least one signed record then only archive
                if (faCount > 1) {
                    let updateRecordInfo = await models.DocumentsForSignature.findOne({
                        attributes: ['id'],
                        where: {
                            subscriptionId: subscription.id,
                            docType: 'FUND_AGREEMENT'
                        },
                        order: [['id', 'DESC']],
                    })

                    if (updateRecordInfo) {

                        //update the previous unsigned fund agreements
                        await models.DocumentsForSignature.update({
                            isArchived: 1,
                            isActive:0
                        }, {
                            where: {
                                id: {
                                    [Op.ne]: updateRecordInfo.id
                                },
                                subscriptionId: subscription.id,
                                docType: 'FUND_AGREEMENT',
                                isPrimaryGpSigned:false
                            }
                        })


                        //update the previous unsigned fund agreements
                        await models.DocumentsForSignature.update({
                            isArchived: 1                   
                        }, {
                            where: {
                                id: {
                                    [Op.ne]: updateRecordInfo.id
                                },
                                subscriptionId: subscription.id,
                                docType: 'FUND_AGREEMENT',
                                isPrimaryGpSigned:true
                            }
                        })

                    }

                }
            }

            //---end

            //send alerts and emails                      
            sendEffectuateAlertsToNonClosedInvestors(req, res, fundId, docType, amendmentId)
            sendEffectuateAlertsToClosedInvestors(req, res, fundId, docType, amendmentId)
            sendEffectuateAlertsToGP(req, res, fundId, docType, amendmentId)

            //consolidated fund agreement
            if (isAmmendment) {
                effectuateHelper.consolidatedAmmendment(amendmentId, ammendmentInfo, req.user)
            } else {
                effectuateHelper.consolidatedFullRestatedFundAgreement(amendmentId, ammendmentInfo, req.user, partnershipDocumentPageCount)
            }

            //gp sign and gp signatory sign for amendments
            if (isAmmendment) {

                effectuateHelper.addGpSignatureAmmendment(req, amendmentId, ammendmentInfo, 'AMENDMENT_AGREEMENT', req.user)
            } else {
                effectuateHelper.addGpSignatureAmmendment(req, amendmentId, ammendmentInfo, 'FUND_AGREEMENT', req.user)
            }

            return res.send('Success');
        }

    } catch (e) {
        next(e)
    }
}

const sendRemainderToClosedInvestors = async (req, res, next) => {
    try {

        const amendmentId = req.params.amendmentId;
        const fundId = req.params.fundId;

        const documents = await models.DocumentsForSignature.findAll({
            where: {
                amendmentId,
                fundId,
                isActive: 1,
                isArchived: 0,
                isPrimaryLpSigned: 0,
                docType: ['AMENDMENT_AGREEMENT', 'FUND_AGREEMENT']
            },
            include: [{
                model: models.FundSubscription,
                as: 'subscription',
                required: true,
                include: [{
                    attributes: ['firstName', 'lastName', 'middleName', 'email', 'id', 'accountId', 'organizationName'],
                    model: models.User,
                    as: 'lp',
                    required: false
                }, {
                    model: models.Fund,
                    as: 'fund',
                    required: true
                }]
            }]
        });

        const user = await models.User.findOne({
            where: {
                id: req.userId
            }
        });

        if (documents.length > 0) {
            documents.forEach(async document => {

                if (document.subscription && document.subscription.lp) {
                    let subjectType = document.docType == 'AMENDMENT_AGREEMENT' ? 'Amendment' : 'Restated Fund Agreement'

                    emailHelper.sendEmail({
                        toAddress: document.subscription.lp.email,
                        subject: `${subjectType} Reminder - Vanilla`,
                        data: {
                            name: commonHelper.getFullName(document.subscription.lp),
                            clientURL: config.get('clientURL'),
                            docType: document.docType == 'AMENDMENT_AGREEMENT' ? 'Amendment to the Fund Agreement' : 'Amended and Restated Fund Agreement',
                            fundCommonName: document.subscription.fund.fundCommonName,
                            user: document.subscription.lp,
                            individualContent: await commonHelper.getIndividualContent(document.subscription,document.subscription.fund.fundCommonName),
                            investorName: await commonHelper.getInvestorName(document.subscription),
                        },
                        htmlPath: "alerts/sendReminderToClosedInvestors.html"
                    });
                    const alertFrom = req.userId;
                    const alertTo = document.subscription.lp.id;

                    const alertData = {

                        name: commonHelper.getFullName(user),
                        htmlMessage: messageHelper.sendReminderToClosedInvestors,
                        subscriptionId: document.subscription.id,
                        fundId: document.subscription.fund.id,
                        fundCommonName: document.subscription.fund.fundCommonName,
                        docType: document.docType == 'AMENDMENT_AGREEMENT' ? 'Amendment to the Fund Agreement' : 'Amended and Restated Fund Agreement',
                        individualContent: await commonHelper.getIndividualContent(document.subscription,document.subscription.fund.fundCommonName),
                        investorName: await commonHelper.getInvestorName(document.subscription),
                        sentBy: req.userId
                    };
                    notificationHelper.triggerNotification(req, alertFrom, alertTo, [], alertData, document.subscription.lp.accountId, null); 
                }


            })
        }


        return res.send('success')


    } catch (e) {
        next(e)
    }
}

//affectuatetheammendment
const terminateAmendment = async (req, res, next) => {
    try {
        const { amendmentId, fundId } = req.body
        //update the affectuate data
        await models.FundAmmendment.update({
            isTerminated: 1,
            isActive: 0
        }, {
            where: {
                id: amendmentId
            }
        })
        //update the DFS for lp not signed 
        await models.DocumentsForSignature.update({
            isActive: 0,
            deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
        }, {
            where: {
                fundId: fundId,
                isPrimaryLpSigned: 0,
                amendmentId: amendmentId,
                isActive: 1
            }
        })

        return res.send('Success');

    } catch (e) {
        next(e)
    }
}


//send effetuate alerts
const sendEffectuateAlertsToNonClosedInvestors = async (req, res, fundId, type, amendmentId) => {

    //need to send emails and alerts for non closed investors has signed on the documents

    logger.info('effectuate sending alerts for non closed .')
 
    //get fund details
    const fund = await models.Fund.findOne({
        where: {
            id: fundId
        }
    })

    // get subscriptions info
    let subscriptions = await models.FundSubscription.findAll({
        where: {
            fundId: fundId,
            status: {
                [Op.in]: [2, 7, 16]
            }
        },
        include: [{
            model: models.User,
            as: 'lp',
            required: true
        }]
    })
 

    //send notification to LP
    for (let subscription of subscriptions) {


        let emailSubject = (type == 'AMENDMENT_AGREEMENT') ? 'Amendment to the Fund Agreement has been approved and made effective' : 'Fund Agreement has been approved and made effective '

        let alertDataToLPandLPSignatories = {
            htmlMessage: type == 'AMENDMENT_AGREEMENT' ? messageHelper.AffectuateAmmendmentLPandLPSignatories : messageHelper.AffectuateFundAmmendmentLPandLPSignatories,
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
            investorName: await commonHelper.getInvestorName(subscription),
            fundId: fund.id,
            sentBy: req.userId
        }

        let fundAgreementChangeAlertDataToLP = type == 'AMENDMENT_AGREEMENT' ? messageHelper.AffectuateAmmendmentDataToLP : messageHelper.AffectuateFADataToLP
        fundAgreementChangeAlertDataToLP = fundAgreementChangeAlertDataToLP.replace('[fundCommonName]', fund.fundCommonName)
        fundAgreementChangeAlertDataToLP = fundAgreementChangeAlertDataToLP.replace('[fundManagerCommonName]', fund.fundManagerCommonName)

        let alertDatatoLP = {
            htmlMessage: fundAgreementChangeAlertDataToLP,
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId,
            individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
            investorName: await commonHelper.getInvestorName(subscription)
        }

        //send notification to LP start
        let htmlFileName = (type == 'AMENDMENT_AGREEMENT') ? 'AffectuateAmmendmentDataToLP.html' : 'AffectuateFADataToLP.html'

        //send notification to delegates       
        let lpDelegatesSelected = await models.LpDelegate.findAll({
            where: {
                fundId: fund.id,
                lpId: subscription.lp.id,
                subscriptionId: subscription.id
            },
            include: [{
                model: models.User,
                as: 'details',
                require: true
            }]
        })

        let lpDelegates = [];
        lpDelegatesSelected.map(i => {
            lpDelegates.push({
                id: i.details.id,
                firstName: i.details.firstName,
                lastName: i.details.lastName,
                middleName: i.details.middleName,
                email: i.details.email,
                isEmailNotification: i.details.isEmailNotification,
                accountId: i.details.accountId
            })
        })

        notificationHelper.triggerNotification(req, req.userId, false, lpDelegates, alertDataToLPandLPSignatories,null,null);

        // send email
        for (let delegate of lpDelegates) {

            let data = {
                firstName: delegate.firstName,
                lastName: delegate.lastName,
                middleName: delegate.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                name: commonHelper.getFullName(delegate),
                clientURL: config.get('clientURL')
            }

            logger.info('delegate Email Sent to *************'+delegate.email)

            emailHelper.triggerAlertEmailNotification(delegate.email, emailSubject, data, htmlFileName);

        }
        //---end            

        //send notfication to LP Signatories
        let lpSignatoriesSelected = await models.LpSignatories.findAll({
            where: {
                fundId: fund.id,
                lpId: subscription.lp.id,
                subscriptionId: subscription.id
            },
            include: [{
                model: models.User,
                as: 'signatoryDetails',
                required: false
            }]
        })

        let lpSignatories = [];
        lpSignatoriesSelected.map(i => {
            lpSignatories.push({
                id: i.signatoryDetails.id,
                firstName: i.signatoryDetails.firstName,
                lastName: i.signatoryDetails.lastName,
                middleName: i.signatoryDetails.middleName,
                email: i.signatoryDetails.email,
                isEmailNotification: i.signatoryDetails.isEmailNotification,
                accountId: i.signatoryDetails.accountId
            })
        })

        notificationHelper.triggerNotification(req, req.userId, false, lpSignatories, alertDataToLPandLPSignatories,null,null);

        let htmlFileNameSignatories = (type == 'AMENDMENT_AGREEMENT') ? 'AffectuateAmmendmentDataToLP.html' : 'AffectuateFADataToLP.html'

        // send email
        for (let signatory of lpSignatories) {

            let data = {
                firstName: signatory.firstName,
                lastName: signatory.lastName,
                middleName: signatory.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                name: commonHelper.getFullName(signatory),
                individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
                investorName: await commonHelper.getInvestorName(subscription),
                clientURL: config.get('clientURL')
            }

            logger.info('Signatory Email Sent to *************'+signatory.email)

            emailHelper.triggerAlertEmailNotification(signatory.email, emailSubject, data, htmlFileNameSignatories);

        }
        //---end    



        if (lpSignatories.length == 0) {
            notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDataToLPandLPSignatories, subscription.lp.accountId, null)
        } else {
            notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDatatoLP, subscription.lp.accountId, null)
            htmlFileName = (type == 'AMENDMENT_AGREEMENT') ? 'AffectuateAmmendmentLPandLPSignatories.html' : 'AffectuateFALPandLPSignatories.html'
        }

        logger.info('Investor Email Sent to *************'+subscription.lp.email)

        emailHelper.sendEmail({
            toAddress: subscription.lp.email,
            subject: emailSubject,
            data: {
                firstName: subscription.lp.firstName,
                lastName: subscription.lp.lastName,
                middleName: subscription.lp.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                name: commonHelper.getFullName(subscription.lp),
                clientURL: config.get('clientURL'),
                user: subscription.lp,
                individualContent:  await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
                investorName: await commonHelper.getInvestorName(subscription),


            },
            htmlPath: 'alerts/' + htmlFileName
        });

        //---end

    }

}


//send effetuate alerts
const sendEffectuateAlertsToClosedInvestors = async (req, res, fundId, type, amendmentId) => {

    //need to send emails and alerts for closed investors has signed on the documents

    logger.info('effectuate sending alerts closed investors.')
  
    //get fund details
    const fund = await models.Fund.findOne({
        where: {
            id: fundId
        }
    })

    //get subscription ids for signed amendment
    let closedSignedSubscription = await models.DocumentsForSignature.findAll({
        attributes: ['subscriptionId'],
        where: {
            fundId,
            docType: ['FUND_AGREEMENT', 'AMENDMENT_AGREEMENT'],
            deletedAt: null,
            isPrimaryLpSigned: 1,
            amendmentId: amendmentId
        }
    })

    let closedSubscriptionIds = _.map(closedSignedSubscription, 'subscriptionId');


    // get subscriptions info
    let subscriptions = await models.FundSubscription.findAll({
        where: {
            fundId: fundId,
            id: {
                [Op.in]: closedSubscriptionIds
            }
        },
        include: [{
            model: models.User,
            as: 'lp',
            required: true
        }]
    })


    //send notification to LP
    for (let subscription of subscriptions) {

        let emailSubject = (type == 'AMENDMENT_AGREEMENT') ? 'Amendment to the Fund Agreement has been approved and made effective' : 'Fund Agreement has been approved and made effective '

        let alertDataToLPandLPSignatories = {
            htmlMessage: type == 'AMENDMENT_AGREEMENT' ? messageHelper.AffectuateAmmendmentLPandLPSignatories : messageHelper.AffectuateFundAmmendmentLPandLPSignatories,
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId,
            individualContent:  await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
            investorName: await commonHelper.getInvestorName(subscription),
        }

        //send notification to LP start
        let htmlFileName = (type == 'AMENDMENT_AGREEMENT') ? 'AffectuateAmmendmentDataToLP.html' : 'AffectuateFADataToLP.html'

        notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDataToLPandLPSignatories, subscription.lp.accountId, null)

        logger.info('Investor Email Sent to *************'+subscription.lp.email)

        emailHelper.sendEmail({
            toAddress: subscription.lp.email,
            subject: emailSubject,
            data: {
                firstName: subscription.lp.firstName,
                lastName: subscription.lp.lastName,
                middleName: subscription.lp.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                name: commonHelper.getFullName(subscription.lp),
                clientURL: config.get('clientURL'),
                user: subscription.lp,
                individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
                investorName: await commonHelper.getInvestorName(subscription),

            },
            htmlPath: 'alerts/' + htmlFileName
        });

        //---end

    }

}


//send effetuate alerts to GP
const sendEffectuateAlertsToGP = async (req, res, fundId, type, amendmentId) => {

    //need to send emails and alerts for gp
    logger.info('effectuate sending alerts GP.')
 

    //get fund details  
    const fund = await models.Fund.findOne({
        include: [{
            model: models.User,
            as: 'gp',
            required: true
        }],
        where: {
            id: fundId
        }
    })

    let alertDataToGP = {
        htmlMessage: type == 'AMENDMENT_AGREEMENT' ? messageHelper.effectuateAmendmentAlertToGP : messageHelper.effectuateFundAgreementAlertToGP,
        firstName: fund.gp.firstName,
        lastName: fund.gp.lastName,
        middleName: fund.gp.middleName,
        fundManagerCommonName: fund.fundManagerCommonName,
        fundCommonName: fund.fundCommonName,
        fundId: fund.id,
        sentBy: req.userId
    }

    let gpemailBodyContent = {
        firstName: fund.gp.firstName,
        lastName: fund.gp.lastName,
        middleName: fund.gp.middleName,
        name: commonHelper.getFullName(fund.gp),
        fundManagerName: commonHelper.getFullName(fund.gp),
        fundCommonName: fund.fundCommonName,
        clientURL: config.get('clientURL'),
        user:fund.gp
    }

    //notification and email to gp
    notificationHelper.triggerNotification(req, fund.gpId, fund.gpId, {}, alertDataToGP, fund.gp.accountId, fund.gp);

    //send notification to LP start
    let htmlFileName = (type == 'AMENDMENT_AGREEMENT') ? 'AffectuateAmmendmentDataToGP.html' : 'AffectuateFADataToGP.html'

    let emailSubject = (type == 'AMENDMENT_AGREEMENT') ? 'Amendment to the Fund Agreement has been approved and made effective' : 'Fund Agreement has been approved and made effective'

    logger.info('GP Email Sent to *************'+fund.gp.email)

    emailHelper.triggerAlertEmailNotification(fund.gp.email, emailSubject, gpemailBodyContent, htmlFileName);


}


//get all gp affectuated amendments
const getGPAmendments = async (req, res, next) => {
    try {
        const { fundId } = req.params;

        const { orderCol = 'createdAt', order = 'DESC' } = req.query;

        let orderFilter = [[orderCol, order]]

        const fundAmendments = await models.FundAmmendment.findAll({
            where: {
                isAffectuated: 1,
                // isAmmendment: 1,
                fundId: fundId,
                // isArchived: 0
            },
            order: orderFilter,
            include: [{
                attributes: ['timezone'],
                model: models.Fund,
                as: 'fund',
                required: false,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }]
            }]
        })


        const docs = [];

        for (const fundAmendment of fundAmendments) {
            console.log(fundAmendment.fund);
            //date conversion based on fund timezone
            let timezone_date = ''; let timezone = ''; let timezone_createdAt_date = '';
            if(fundAmendment.fund.timeZone && fundAmendment.fund.timeZone.code) {
                timezone_date = await commonHelper.setDateUtcToOffset(fundAmendment.affectuatedDate, fundAmendment.fund.timeZone.code).then(function (value) {
                    return value;
                });
                timezone_createdAt_date = await commonHelper.setDateUtcToOffset(fundAmendment.createdAt, fundAmendment.fund.timeZone.code).then(function (value) {
                    return value;
                });
                timezone = fundAmendment.fund.timeZone.displayName;
            } else {
                timezone_date = await commonHelper.getDateAndTime(fundAmendment.affectuatedDate);
                timezone_createdAt_date = await commonHelper.getDateAndTime(fundAmendment.createdAt);
            }

            docs.push({
                id: fundAmendment.id,
                createdAt: timezone_createdAt_date,
                affectuatedDate: timezone_date,
                filename: fundAmendment.document.originalname,
                isAmendment: fundAmendment.isAmmendment,
                url: `${config.get('serverURL')}/api/v1/document/view/displayamendment/${fundAmendment.id}`,
                timezone: timezone
            })
            

        }

        return res.json({
            data: docs
        });


    } catch (error) {
        next(error);
    }

}

const withoutInvestorConsentUpload = async (req, res, next) => {
    try {
        const { fundId, uploadType } = req.params;

        const { amendmentId } = req.body;


        let docType = uploadType == 'Fund_Agreement' ? ['FUND_AGREEMENT', 'CONSOLIDATED_FUND_AGREEMENT'] : ['AMENDMENT_AGREEMENT', 'CONSOLIDATED_AMENDMENT_AGREEMENT']

        let amendment = uploadType == 'Fund_Agreement' ? null : amendmentId

        let uploadedFileType = uploadType == 'Fund_Agreement' ? 'FUND_AGREEMENT' : 'AMENDMENT_AGREEMENT'

        let fund = await getSubscriptionsRelatedToFund(fundId);

        let subscriptionIds = _.map(fund.fundInvestor, 'id');

        documents = await models.DocumentsForSignature.findAll({
            attributes: [['id', 'documentId'], 'fundId', 'subscriptionId', 'filePath', 'docType', 'previousDocumentId', 'amendmentId', 'isActive', 'isArchived'],
            where: {
                fundId,
                subscriptionId: {
                    [Op.or]: [{ [Op.in]: subscriptionIds }, null]
                },
                docType: docType,
                deletedAt: null,
                isActive: 1,
                isArchived: 0,
                amendmentId: amendment
            },
            raw: true
        });

        models.DocumentsForSignatureHistory.bulkCreate(documents);

        if (uploadType == 'Fund_Agreement') {
            models.FundAgreementsHistory.create({
                fundId: fundId,
                partnershipDocument: JSON.stringify(fund.partnershipDocument),
                createdBy: req.userId,
                updatedBy: req.userId
            });
        } else {
            models.FundAmmendment.findOne({
                where: {
                    id: amendmentId,
                    isActive: 1,
                    isArchived: 0
                },
                raw: true
            }).then(amendment => {
                models.FundAmendmentHistory.create({
                    fundAmendmentId: amendmentId,
                    fundId,
                    document: JSON.stringify(amendment.document),
                    amendmentId,
                    createdBy: req.userId,
                    updatedBy: req.userId
                })
            })


        }

        await models.FundCron.create({
            fundId,
            uploadType: uploadedFileType,
            amendmentId,
            uploadedFilePath: await commonHelper.getFileInfo(req.file, true),
            status: 0,
            userId: req.userId

        });

        await models.Fund.update({
            isFundLocked: true,
        }, {
            where: {
                id: fundId
            }
        });

        return res.send('Success');

    } catch (e) {
        next(e)
    }
}

module.exports = {
    checkFundStatus,
    uploadFANonClosedInvestors,
    existingFAandAmmendments,
    uploadDocsToClosedInvestors,
    ammendmentSummary,
    investorsDetails,
    effectuateAmendment,
    terminateAmendment,
    sendRemainderToClosedInvestors,
    getGPAmendments,
    withoutInvestorConsentAmendment,
    withoutInvestorConsentRFA,
    withoutInvestorConsentUpload
}
