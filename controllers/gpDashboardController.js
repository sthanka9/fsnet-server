
const models = require('../models/index');
const { Op } = require('sequelize')
const _ = require('lodash')
const messageHelper = require('../helpers/messageHelper');

const fundsList = async (req, res, next) => {

    try {

        const { search, limit = 0 } = req.params
        const skip = 1000;

        var whereFilter = {}

  

        if (req.user.accountType == 'GP') {
            const vcFirm = await models.VCFirm.findOne({
                where: {
                    gpId: req.userId
                }
            });

            if(!vcFirm) {
                return res.json({
                    message:  'Invalid GP Account.'
                });
            }
        
            whereFilter.vcfirmId = vcFirm.id;

        } else if (req.user.accountType == 'GPDelegate') {

            let fundIds = await models.GpDelegates.findAll({
                attributes: ['fundId'],
                where: {
                    delegateId: req.userId
                },
                include: [{
                    model: models.User,  
                    as : 'gpuserdetails',                  
                    required: true        
                   
                }]                
            }) 

            fundIds = _.map(fundIds, 'fundId');

            whereFilter.id = {
                [Op.in]: fundIds
            }
        } else if (req.user.accountType == 'SecondaryGP') {
            // secondary GP
            let fundIds = await models.GpSignatories.findAll({
                attributes: ['fundId'],
                where: {
                    signatoryId: req.userId
                }
            })

            fundIds = _.map(fundIds, 'fundId');

            whereFilter.id = {
                [Op.in]: fundIds
            }

        } else {
            return res.status(401).json({
                data: messageHelper.authrizationCheck
            })
        }


        if (search) {
            whereFilter.legalEntity = {
                [Op.like]: `%${search}%`
            }
        }

        let funds = await models.Fund.findAndCountAll({
            attributes: ['id', 'fundHardCap', 'legalEntity', 'fundTargetCommitment', 'fundImage'],
            limit: skip,
            offset: limit,
            order: [
                ['legalEntity', 'ASC'],
            ],
            where: whereFilter,
            include: [{
                model: models.FundStatus,
                as: 'fundStatus',
                required: true,
            }, {
                attributes: ['id', 'lpCapitalCommitment', 'status'],
                model: models.FundSubscription,
                as: 'fundInvestor',
                required: false,
                where: {
                    deletedAt: null,
                    status:{
                        [Op.ne]:17
                    }
                },
                include: [{
                    attributes: ['id', 'gpConsumed'],
                    model: models.FundClosings,
                    as: 'fundClosingInfo',
                    required: false,
                    where: {
                        isActive: 1
                    }
                }]
            }]
        })

        const itemCount = funds.count;
        const pageCount = Math.ceil(funds.count / req.query.limit);

        funds = funds.rows.map(fund => {

            fund = fund.toJSON();
            const inProgress = _.filter(fund.fundInvestor, { 'status': 2 })
            const waitingForSignature = _.filter(fund.fundInvestor, { 'status': 16 });
            const closeReady = _.filter(fund.fundInvestor, { 'status': 7 })
            const closed = _.filter(fund.fundInvestor, { 'status': 10 })

            fund.inProgress = inProgress.length;
            fund.closeReady = closeReady.length;
            fund.closed = closed.length;

            fund.status = fund.fundStatus.name;

            const sumOfCapitalCommitmentForInProgress = _.sumBy(inProgress, 'lpCapitalCommitment') || 0 + (_.sumBy(waitingForSignature, 'lpCapitalCommitment') || 0);
            const sumOfCapitalCommitmentForCloseReady = _.sumBy(closeReady, 'lpCapitalCommitment') || 0;
            const sumOfCapitalCommitmentForClosed = _.sumBy(closed, function (o) { return (o.fundClosingInfo && _.last(o.fundClosingInfo)) ? _.last(o.fundClosingInfo).gpConsumed : 0; }) || 0;

            let fundHardCap30PercentAdded = fund.fundHardCap * 1.3;

            if (fundHardCap30PercentAdded > 0) {

                while (sumOfCapitalCommitmentForInProgress + sumOfCapitalCommitmentForCloseReady + sumOfCapitalCommitmentForClosed > fundHardCap30PercentAdded) {
                    fundHardCap30PercentAdded = fundHardCap30PercentAdded * 1.3
                }

                fund.fundHardCapPercent = (fund.fundHardCap / fundHardCap30PercentAdded) * 100
                fund.fundTargetCommitmentPercent = (fund.fundTargetCommitment / fundHardCap30PercentAdded) * 100

                fund.sumOfCapitalCommitmentForInProgress = sumOfCapitalCommitmentForInProgress;
                fund.sumOfCapitalCommitmentForCloseReady = sumOfCapitalCommitmentForCloseReady;
                fund.sumOfCapitalCommitmentForClosed = sumOfCapitalCommitmentForClosed;

                fund.inProgressPercent = (sumOfCapitalCommitmentForInProgress / fundHardCap30PercentAdded) * 100 || 0;
                fund.closeReadyPercent = (sumOfCapitalCommitmentForCloseReady / fundHardCap30PercentAdded) * 100 || 0;
                fund.closedPercent = (sumOfCapitalCommitmentForClosed / fundHardCap30PercentAdded) * 100 || 0;

            } else {
                fund.fundHardCapPercent = 0
                fund.fundTargetCommitmentPercent = 0

                fund.sumOfCapitalCommitmentForInProgress = sumOfCapitalCommitmentForInProgress;
                fund.sumOfCapitalCommitmentForCloseReady = sumOfCapitalCommitmentForCloseReady;
                fund.sumOfCapitalCommitmentForClosed = sumOfCapitalCommitmentForClosed;

                fund.inProgressPercent =  0;
                fund.closeReadyPercent =  0;
                fund.closedPercent = 0;
            }

            return fund;

        });

        return res.json({
            itemCount,
            pageCount,
            data: funds
        })

    } catch (e) {
        next(e)
    }
}

module.exports = {
    fundsList
}  