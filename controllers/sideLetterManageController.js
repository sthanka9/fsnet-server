const config = require('../config/index');
const models = require('../models/index');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const _ = require('lodash')
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const path = require('path');
const fs = require('fs');
const { Op, literal } = require('sequelize');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const logger = require('../helpers/logger');
const eventHelper = require('../helpers/eventHelper');
const notificationHelper = require('../helpers/notificationHelper');

//upload sideletter doc
const upload = async (req, res, next) => {    
    try {

        //input params
        const { fundId, subscriptionId = 0} = req.body;
        
        //get fund info
        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.GpDelegates,
                as: 'gpDelegatesSelected',
                required: false,
                where: {
                    fundId: fundId
                }
            }]
        })      


    //get the uploaded file extension name
    let filename = req.file.filename

    const filePath = `./assets/funds/${req.body.fundId}/${req.body.subscriptionId}/${filename}`

    //inactive the previous files
    await models.SideLetter.update({ isActive: 0 }, { where: { fundId, subscriptionId} })

    //get subscription details and update status
    const subscription = await models.FundSubscription.findOne({
        where: {
            id: subscriptionId
        },
        include:[{
            model: models.User,
            as: 'lp',
            required: false
        }]
    });

    let subscriptionInvestorStatus = subscription.status

    console.log("LP Status ******************",subscriptionInvestorStatus)

    // if status is close-ready then change status to inprogress
    if(subscriptionInvestorStatus==7){
        await models.FundSubscription.update({ status: 2 }, { where: { id: subscriptionId } })
    }

    //create record in sideletter 
    const changeSideletter = await models.SideLetter.create({
        fundId: fundId,
        subscriptionId: subscriptionId,
        isActive:1,
        sideLetterDocumentPath:filePath,
        createdBy: req.userId,
        updatedBy: req.userId        
    }) 


    //update status as 0 for previous sideleter agreement
    await models.DocumentsForSignature.update({ isActive: 0 }, {
        where: {
            fundId: fundId,
            subscriptionId:subscriptionId,
            docType: 'SIDE_LETTER_AGREEMENT',
        }
    })

    //create a new record for document signatures
    await models.DocumentsForSignature.create({
        fundId: fundId,
        subscriptionId: subscriptionId,
        sideletterId: changeSideletter.id,
        filePath: filePath,
        docType: 'SIDE_LETTER_AGREEMENT',
        gpSignCount: 0,
        lpSignCount: 0,
        isPrimaryGpSigned: 0,
        isPrimaryLpSigned: 0,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    });     
   

    //get lp details/lpid for the subscription
    let lpdetails = await models.FundSubscription.findOne({
        where: {
            fundId: fundId,
            id:subscriptionId
        }
    })

    //lp id
    let lpId = lpdetails.lpId
    
    //get lp details
    const lp = await models.User.findOne({
        where: {
            id: lpId
        }
    })
     
    logger.info('Cancel Capital Commitment : get lp signatories ')

    // get lp signatories
    let lpSignatoriesSelected = await models.LpSignatories.findAll({
        where: {
            fundId: fundId,
            lpId: lpId,
            subscriptionId: subscriptionId
        },
        include: [{
            model: models.User,
            as: 'signatoryDetails',
            required: false
        }]
    })

    let lpSignatories = [];
    lpSignatoriesSelected.map(i => {
        lpSignatories.push({
            id: i.signatoryDetails.id,
            firstName: i.signatoryDetails.firstName,
            lastName: i.signatoryDetails.lastName,
            middleName: i.signatoryDetails.middleName,
            email: i.signatoryDetails.email,
            isEmailNotification: i.signatoryDetails.isEmailNotification,
            accountId: i.signatoryDetails.accountId
        })
    })


    //send notification to lps
    let lpDelegatesSelected = await models.LpDelegate.findAll({
        where: {
            fundId: fundId,
            lpId: lpId,
            subscriptionId: subscriptionId
        },
        include: [{
            model: models.User,
            as: 'details',
            required: false
        }]
    })

    let lpDelegates = [];
    lpDelegatesSelected.map(i => {
        lpDelegates.push({
            id: i.details.id,
            firstName: i.details.firstName,
            lastName: i.details.lastName,
            middleName: i.details.middleName,
            email: i.details.email,
            isEmailNotification: i.details.isEmailNotification,
            accountId:i.details.accountId

        })
    })
    //save log info
    eventHelper.saveEventLogInformation('GP issues Side Letter', fund.id, subscriptionId, req);

    //send notifications for closed and closed ready status only
    if(subscriptionInvestorStatus==7 || subscriptionInvestorStatus==10){

    //send alert data notification
    let documentInfo = await models.DocumentsForSignature.findOne({
        attributes: ['id'],
        where: { sideletterId:changeSideletter.id }
    });        

    let documentId = documentInfo.id

    const alertData = {
        htmlMessage: messageHelper.issuedSideLetterAlertMessage,
        message: 'has issued Side Letter',
        lpName: commonHelper.getInvestorName(subscription),
        subscriptionId: subscriptionId,
        docType: 'SIDE_LETTER_AGREEMENT',
        fundManagerCommonName: fund.fundManagerCommonName,
        fundName: fund.fundCommonName,
        fundId: fund.id,
        sentBy: req.userId,
        documentId:documentId
    };

    let lpcustommessage = messageHelper.issuesSideLetterAlertMessageLPWithSignatory
    lpcustommessage = lpcustommessage.replace('[FundCommonName]', fund.fundCommonName)

    let lpWithSignatoryAlertData =  {
        htmlMessage: lpcustommessage,
        message: 'has issued Side Letter',
        lpName: commonHelper.getInvestorName(subscription),
        subscriptionId: subscriptionId,
        docType: 'SIDE_LETTER_AGREEMENT',
        fundManagerCommonName: fund.fundManagerCommonName,
        fundName: fund.fundCommonName,
        fundId: fund.id,
        sentBy: req.userId,
        documentId:documentId
    }
    

    // if lp has signatories then alert will not contain lnk and send notification to LP
    if(lpSignatories.length>0){         
        notificationHelper.triggerNotification(req, fund.gpId, lpId,lpDelegates,lpWithSignatoryAlertData,lp.accountId,null);
    } else {        
        notificationHelper.triggerNotification(req, fund.gpId, lpId, lpDelegates, alertData,lp.accountId,null);
    }  

    //send alert to LP sign only
    notificationHelper.triggerNotification(req, fund.gpId, false, lpSignatories, alertData,null,null);

    let emailBodyContentLP = {
        'firstName': lp.firstName,
        'name':commonHelper.getInvestorName(subscription),
        'lastName': lp.lastName,
        'middleName': lp.middleName,
        'fundCommonName': fund.fundCommonName        
    }

    //send email to LP
    if(lpSignatories.length>0){
        emailHelper.triggerAlertEmailNotification(lp.email, 'Fund Manager has issued a Side Letter', emailBodyContentLP, 'gpSignatoriesChangeSideLetter.html');
    } else {
        emailHelper.triggerAlertEmailNotification(lp.email, 'Fund Manager has issued a Side Letter', emailBodyContentLP, 'gpChangeSideLetter.html');
    }    

    // LP signatories
    for (let signatory of lpSignatories) {   
        
        let emailBodyContentLPSign = {
            'firstName': signatory.firstName,
            'name':commonHelper.getFullName(signatory),
            'lastName': signatory.lastName,
            'middleName': signatory.middleName,
            'fundCommonName': fund.fundCommonName        
        }

        emailHelper.triggerAlertEmailNotification(signatory.email, 'Fund Manager has issued a Side Letter', emailBodyContentLPSign, 'gpChangeSideLetter.html');      
    }          

    // LP Delegate
    for (let delegate of lpDelegates) {    

        let emailBodyContentLPDelegate = {
            'firstName': delegate.firstName,
            'name':commonHelper.getFullName(delegate),
            'lastName': delegate.lastName,
            'middleName': delegate.middleName,
            'fundCommonName': fund.fundCommonName        
        }        
        emailHelper.triggerAlertEmailNotification(delegate.email, 'Fund Manager has issued a Side Letter', emailBodyContentLPDelegate, 'gpChangeSideLetterDelegates.html');       
    } 
    
    }
    
    return res.status(201).json({
        message: messageHelper.lpCommitmentsCreated
    });
        
 
    }catch (e) {

        next(e);

    }
}

async function generateSideLetterSignedPage(data, htmlFilePath) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}

async function sideLetterAgreementSignDoc(data, changeCommitmentAgreementTempFile) {
    //lp sign empty block
    const lphtmlFilePath = "./views/sideletterAgreementLPSign.html";
    const lpsignedLastPage = await generateSideLetterSignedPage(data, lphtmlFilePath); 
    const lpfileName = `LPSIDE_LETTER_AGREEMENT_${uuidv1()}.pdf`;
    const lpoutputFile = path.resolve(__dirname, `../assets/temp/${lpfileName}`);
    await commonHelper.pdfMerge([changeCommitmentAgreementTempFile, lpsignedLastPage.path], lpoutputFile);
    return { path: lpoutputFile, name: lpfileName , urlpath:'assets/temp/'+lpfileName};
}

//get side letter document
const getSideLetterAgreement = async (req, res, next) => {
    try {
        const documentId = req.params.id
        //get document path
        let documentsForSignature = await models.DocumentsForSignature.findOne({
            where: {
                id: documentId
            },
            include: [{
                model: models.SideLetter,
                as: 'changeSideletter',
                required: true
            }]
        });
        //if file not found
        if (!documentsForSignature) {
            return res.json({ error: true, message: messageHelper.envelopeValidate })
        }

        const subscription = await models.FundSubscription.findOne({
            where: {
                id: documentsForSignature.subscriptionId
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true
            }]
        });

        const changeCommitmentAgreementTempFile = documentsForSignature.filePath;

        const lpSignedDoc = await sideLetterAgreementSignDoc({             
            jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
            investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
            legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
            lpName: commonHelper.getFullName(req.user),
            areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
            date: moment().format('M/D/YYYY'),
            lpSignaturePic: '',            
            
        }, changeCommitmentAgreementTempFile);
 
        const outputFile = lpSignedDoc.urlpath;        
 

        //response
        return res.json({
            filePathTempPublicUrl: commonHelper.getLocalUrl(outputFile)
        });

    } catch (error) {
        next(error);
    }

}

//get all gp side letters
const getGPSideLetters = async (req, res, next) => {
    try {
        const { fundId } = req.params;

        const { orderCol = 'createdAt', order = 'DESC', docType, lpId } = req.query;

        let orderFilter = [[orderCol, order]];

        if (orderCol === 'firstName') {
            orderFilter = [
                ['subscription', 'lp', orderCol, order]
            ];
        }

        if (orderCol === 'docType') {
            orderFilter = [[orderCol, order]];
        }

        let documentFiler = {
            fundId: fundId,
            docType:'SIDE_LETTER_AGREEMENT',
            //[Op.or]: [{gpSignCount: 1}, {isPrimaryGpSigned: 1}]
        };

        if (docType) {
            documentFiler.docType = docType;
        }

        let ipFilter = {};

        if (lpId) {
            ipFilter = {
                id:lpId
            }
        }

        const documentsForSignature = await models.DocumentsForSignature.findAll({
            where: documentFiler,
            order: orderFilter,
            include: [{
                attributes: ['id'],
                model: models.FundSubscription,
                as: 'subscription',
                required: false,
                where: {
                    fundId: fundId
                },
                include: [{
                    where: ipFilter,
                    attributes: ['firstName','middleName', 'lastName', 'profilePic'],
                    model: models.User,
                    as: 'lp',
                    required: false
                },{
                    where: ipFilter,
                    attributes: ['firstName','middleName', 'lastName'],
                    model: models.OfflineUsers,
                    as: 'offlineLp',
                    required: false
                },{
                    attributes: ['timezone'],
                    model: models.Fund,
                    as: 'fund',
                    required: false,
                    include: [{
                        model: models.timezone,
                        as: 'timeZone',
                        required: false
                    }]
                }]
            }]
        });


        const docs = [];
        let timezone_date = ''; let timezone = '';
        for (const documentForSignature of documentsForSignature) {
            if(documentForSignature.subscription && (documentForSignature.subscription.lp || documentForSignature.subscription.offlineLp)){
                if(documentForSignature.subscription.fund.timeZone && documentForSignature.subscription.fund.timeZone.code) {
                    timezone_date = await commonHelper.setDateUtcToOffset(documentForSignature.createdAt, documentForSignature.subscription.fund.timeZone.code).then(function (value) {
                        return value;
                    });
                    timezone = documentForSignature.subscription.fund.timeZone.displayName;
                } else {
                    timezone_date =  await commonHelper.getDateAndTime(documentForSignature.createdAt);
                }
                docs.push({
                    id: documentForSignature.id,
                    subscriptionId: documentForSignature.subscriptionId,
                    fundId: documentForSignature.fundId,
                    firstName: documentForSignature.subscription && documentForSignature.subscription.lp ? 
                        documentForSignature.subscription.lp.firstName : (documentForSignature.subscription && documentForSignature.subscription.offlineLp ? documentForSignature.subscription.offlineLp.firstName : null),
                    lastName: documentForSignature.subscription && documentForSignature.subscription.lp ? 
                        documentForSignature.subscription.lp.lastName : (documentForSignature.subscription && documentForSignature.subscription.offlineLp ? documentForSignature.subscription.offlineLp.lastName : null),
                    middleName: documentForSignature.subscription && documentForSignature.subscription.lp ? 
                        documentForSignature.subscription.lp.middleName: (documentForSignature.subscription && documentForSignature.subscription.offlineLp ? documentForSignature.subscription.offlineLp.middleName : null),
                    documentType: documentForSignature.docType,
                    createdAt: documentForSignature.createdAt,
                    documentUrl: `${config.get('serverURL')}/api/v1/document/view/viewiasia/${documentForSignature.id}`,
                    profilePic: documentForSignature.subscription && documentForSignature.subscription.lp? documentForSignature.subscription.lp.profilePic : null,
                    isPrimaryGpSigned: documentForSignature.isPrimaryGpSigned,
                    isPrimaryLpSigned: documentForSignature.isPrimaryLpSigned,
                    isActive: documentForSignature.isActive,
                    timezone: timezone,
                    timezone_date: timezone_date
                })
            }
        }

        return res.json({
            data: docs
        });


    } catch (error) {
        next(error);
    }

}

async function generateSignedPage(data, htmlFilePath) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
   // const outputFile = path.resolve(__dirname, `../../${fileName}`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}


// generate last page signed pdf
async function sideLetterAgreementSign(data, changeCommitmentAgreementTempFile) {

    const htmlFilePath = "./views/sideletterAgreementGPSign.html";
    const signedLastPage = await generateSignedPage(data, htmlFilePath);     
    const fileName = `SIDE_LETTER_AGREEMENT_${uuidv1()}.pdf`;
    const outputFile = `./assets/funds/${data.fundId}/${data.subscriptionId}/${fileName}`    
    await commonHelper.pdfMerge([changeCommitmentAgreementTempFile, signedLastPage.path], outputFile)   
    return { path: outputFile, name: fileName };
}

//counter sign by gp for sideletter

const gpSign = async (req, res, next) => {

    try {
        const { documentId, date, timeZone, proceed = false } = req.body

        const userId = req.userId;

        const gpData = await models.User.findOne({
            where: {
                id: userId
            }
        })

        //let gpSignatoryPicObject = JSON.parse(gpData.signaturePic)
        //const gpSignaturePath = commonHelper.getLocalUrl(gpSignatoryPicObject.path)

        let gpSignatureUrl = gpData.signaturePicUrl
        let gpSignaturePath = gpData.signaturePic           

        if (!gpData) {
            return res.status(404).json({
                "errors": [
                    {
                        "msg": messageHelper.userValidate
                    }
                ]
            });
        }


        const documentsForSignature = await models.DocumentsForSignature.findOne({
            where: {
                id: documentId,
                isPrimaryLpSigned: 1,
            },
            include: [{
                model: models.SideLetter,
                as: 'changeSideletter',
                required: true
            }, {
                model: models.FundSubscription,
                as: 'subscription',
                required: true,
                include:[{
                    model: models.User,
                    as: 'lp',
                    required: false
                }]
            }]
        })

        if (!documentsForSignature) {
            return res.status(404).json({
                "errors": [
                    {
                        "msg": messageHelper.envelopeValidate
                    }
                ]
            });
        }


        const fund = await models.Fund.findOne({
            include: [{
                model: models.timezone,
                as: 'timeZone',
                required: false
            },{
                model: models.User,
                as: 'gp',
                required: true
            }],
            where: {
                id: documentsForSignature.fundId
            }
        })


        // if gp already counter-signed don't allow to sign again
        if (documentsForSignature.isPrimaryGpSigned === true) {

            return errorHelper.error_400(res,
                'isPrimaryGpSigned',
                'Your Sideletter has already been signed. Once it is accepted and signed by your Fund Manager, please visit your Document Locker to view the document.',
                {
                    "CODE": 'SIDE_LETTER_AGREEMENT_ERROR',
                    "proceed": false,
                });

        }


        //making date with timezone of a fund
        let date_for_sign = await commonHelper.getDate();
        date_for_sign = commonHelper.getDateTimeWithTimezone(fund.timeZone, date_for_sign, 0, 0);

        const newSideLetterTempFile = await sideLetterAgreementSign({
            gpName: commonHelper.getFullName(gpData),
            fundManagerTitle: fund.fundManagerTitle,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            fundLegalEntity: fund.legalEntity,
            fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
            gpSignaturePic: gpSignatureUrl,
            date: date_for_sign,
            fundId:documentsForSignature.fundId,
            subscriptionId:documentsForSignature.subscriptionId
        }, documentsForSignature.filePath); 

        let isAllGpSignatoriesSigned = req.user.accountType == 'GP' ? true : documentsForSignature.gpSignCount + 1 >= fund.noOfSignaturesRequiredForSideLetter - 1;

        await models.DocumentsForSignature.update({
            isPrimaryGpSigned: req.user.accountType == 'GP' ? true : false,
            isAllGpSignatoriesSigned: isAllGpSignatoriesSigned,
            gpSignCount: literal('gpSignCount + 1'),
            filePath: newSideLetterTempFile.path,
            gpIpAddress: req.ipInfo
            //gpSignedDateTime: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
        }, {
                where: {
                    id: documentId,
                }
            });

        //const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);
        const signedDateconvertedToUserTimezone = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')

        await models.SignatureTrack.create({
            documentId: documentId,
            documentType: 'SIDE_LETTER_AGREEMENT',
            signaturePath: gpSignaturePath,
            subscriptionId: documentsForSignature.subscriptionId,
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: req.body.timeZone,
            signedDateInGMT: req.body.date,
            signedUserId: req.user.id,
            signedByType: req.user.accountType,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        });      
       
        eventHelper.saveEventLogInformation('GP Counter-Sign SideLetter', documentsForSignature.fundId, documentsForSignature.subscriptionId, req);  

 

        // get lpdelegates for the fund based on lpid
        let lpDelegatesSelected = await models.LpDelegate.findAll({
            where: {
                fundId: fund.id,
                lpId: documentsForSignature.subscription.lpId
            },
            include: [{
                model: models.User,
                as: 'details',
                required: false
            }]
        })

        let lpDelegates = [];
        lpDelegatesSelected.map(i => {
            lpDelegates.push({
                id: i.details.id,
                firstName: i.details.firstName,
                lastName: i.details.lastName,
                email: i.details.email,
                isEmailNotification: i.details.isEmailNotification,
                accountId: i.details.accountId
            })
        }) 
        // LP
        const lpData = await models.User.findOne({
            where: {
                id: documentsForSignature.subscription.lpId
            }
        });

       //no of signatures for side letter
       let noOfSignaturesRequiredForSideLetter = fund.noOfSignaturesRequiredForSideLetter 

        //get total gp signatories count
        const signatoriesCount = await models.GpSignatories.count({ where: { fundId: fund.id } })

       //if secondary gp counter sign the document , then send notification and email to GP
        
       if(req.user.accountType=='SecondaryGP' && noOfSignaturesRequiredForSideLetter>1 && (signatoriesCount==documentsForSignature.gpSignCount + 1)){
      
            const alertDatastoGP = {
                htmlMessage: messageHelper.signedSideLetterGPWithOutSignatoryAlertMessage,
                message: 'Investor has signed Side Letter',
                lpName: await commonHelper.getInvestorName(documentsForSignature.subscription),
                subscriptionId: documentsForSignature.subscriptionId,
                docType: 'SIDE_LETTER_AGREEMENT',
                fundManagerCommonName: fund.fundManagerCommonName,
                fundName: fund.fundCommonName,
                fundId: fund.id,
                sentBy: req.userId,
                documentId:documentId
            };    

            let gpemailBodyContent = {
                firstName: fund.gp.firstName,
                lastName: fund.gp.lastName,
                middleName: fund.gp.middleName,
                name: commonHelper.getFullName(lpData),
                fundManagerName:commonHelper.getFullName(fund.gp),
                fundCommonName: fund.fundCommonName,
                clientURL: config.get('clientURL'),
                user:fund.gp
            }
            
            //notification and email to gp
            notificationHelper.triggerNotification(req, lpData.id, fund.gpId, {}, alertDatastoGP,fund.gp.accountId,fund.gp);          
            emailHelper.triggerAlertEmailNotification(fund.gp.email, 'Investor has signed Side Letter', gpemailBodyContent, 'lpSignedSideLetter.html');            

       }        

        const alertData = {
            htmlMessage: messageHelper.executedSideLetterAlertMessage,
            message: 'has executed Side Letter',          
            subscriptionId: documentsForSignature.changeSideletter.subscriptionId,
            documentId: documentId,
            docType: 'SIDE_LETTER_AGREEMENT',
            fundManagerCommonName: fund.fundManagerCommonName,
            fundName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId
        };

        let getLpAccountInfo = await models.User.findOne({
            attributes:['accountId'],
            where: {
                id: documentsForSignature.subscription.lpId
            }
        })

        notificationHelper.triggerNotification(req, fund.gpId, documentsForSignature.subscription.lpId, lpDelegates, alertData,getLpAccountInfo.accountId,null);

        //send only on gp sign
        if(req.user.accountType=='GP')
        {       

        let emailBodyContent = {
            'firstName': lpData.firstName,
            'lastName': lpData.lastName,
            'middleName': lpData.middleName,
            'name': commonHelper.getInvestorName(documentsForSignature.subscription),
            'fundCommonName': fund.fundCommonName
        }

        if (lpData.isEmailNotification) { // if setting is enabled
            emailHelper.triggerAlertEmailNotification(lpData.email, 'Fund Manager has executed Side Letter and it has become binding', emailBodyContent, 'gpExecutedSideLetter.html');
        }

        // LP Delegate
        for (let delegate of lpDelegates) {
            if (delegate.isEmailNotification) { // if setting is enabled

                let emailBodyContentDelegate = {
                    'firstName': delegate.firstName,
                    'lastName': delegate.lastName,
                    'middleName': delegate.middleName,
                    'name': commonHelper.getFullName(delegate),
                    'fundCommonName': fund.fundCommonName
                }

                emailHelper.triggerAlertEmailNotification(delegate.email, 'Fund Manager has executed Side Letter and it has become binding', emailBodyContentDelegate, 'gpExecutedSideLetter.html');
            }
        }

        }

        return res.json({ url: `${config.get('serverURL')}/api/v1/document/view/viewsia/${documentsForSignature.subscriptionId}` })
        

    } catch (error) {
        next(error);
    }

}

module.exports = {
    upload,
    getSideLetterAgreement,
    getGPSideLetters,
    gpSign
}
