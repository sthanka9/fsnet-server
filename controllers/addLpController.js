
const models = require('../models/index');
const config = require('../config/index')
const _ = require('lodash')
const { Op } = require('sequelize')
const moment = require('moment');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const logger = require('../helpers/logger');
const eventHelper = require('../helpers/eventHelper');
const notificationHelper = require('../helpers/notificationHelper');

function checkEmail(email,isOfflineLp) {
    let splittedArray = email.split('@');
    if (_.includes(splittedArray, 'temporary.com')) {
        return isOfflineLp ? false : true;
    } else {
        return isOfflineLp ? true : false;
    }
}

module.exports.isNewLP = async (req, res, next) => {

    try {
        const { email, fundId } = req.body;

        let firstName = "";
        let lastName = "";
        let middleName = "";
        let organizationName = "";
        let cellNumber = "";

        if (!email) {
            return errorHelper.error_400(res, 'email', messageHelper.emailValidate);
        }

        if (!fundId) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }

        let status = null;

        const user = await models.User.findOne({
            attributes: ['id', 'middleName', 'firstName', 'lastName', 'organizationName', 'cellNumber'],
            where: { 
                email: email,
                 accountType: 'LP' 
            }
        });


        if (Boolean(user)) {

            firstName = _.get(user, 'firstName', '')
            lastName = _.get(user, 'lastName', '')
            middleName = _.get(user, 'middleName', '')
            organizationName = _.get(user, 'organizationName', '')
            cellNumber = _.get(user, 'cellNumber', '')

            const fund = await models.Fund.findOne({
                attributes: ['id', 'vcfirmId', 'gpId'],
                where: {
                    id: fundId
                }
            });

            const isAlreadyAddedAsLpToFund = await models.VcFrimLp.findOne({
                attributes: ['id'],
                where: {
                    vcfirmId: fund.vcfirmId,
                    lpId: user.id,
                    isDeleted: 0,
                }
            })

            status = Boolean(isAlreadyAddedAsLpToFund) ? 2 : 1;

        } else {

            const account = await models.Account.findOne({
                attributes: ['id', 'firstName', 'lastName', 'middleName', 'organizationName', 'cellNumber'],
                where: { email: email }
            });

            if (account) {
                firstName = _.get(account, 'firstName', '');
                lastName = _.get(account, 'lastName', '');
                middleName = _.get(account, 'middleName', '');
                organizationName = _.get(account, 'organizationName', '');
                cellNumber = _.get(account, 'cellNumber', '');
                status = 3;
            } else {
                status = 0;
            }
        }


        return res.json({
            status: status,
            firstName: firstName,
            lastName: lastName,
            middleName: middleName,
            email: email,
            organizationName: organizationName,
            cellNumber: cellNumber
        });

    } catch (error) {
        next(error);
    }
}

module.exports.isNewOfflineLP = async (req, res, next) => {

    try {
        const { email } = req.body;

        if (!email) {
            return errorHelper.error_400(res, 'email', messageHelper.emailValidate);
        }

        const user = await models.OfflineUsers.findOne({
            attributes: ['id', 'middleName', 'firstName', 'lastName', 'organizationName', 'cellNumber'],
            where: { email }
        });

        const status = Boolean(user) ? 2 : 0;

        return res.json({
            status,
            firstName: _.get(user, 'firstName', ''),
            lastName: _.get(user, 'lastName', ''),
            middleName: _.get(user, 'middleName', ''),
            email,
            organizationName: _.get(user, 'organizationName'),
            cellNumber: _.get(user, 'cellNumber')
        });

    } catch (error) {
        next(error);
    }
}

module.exports.addLp = async (req, res, next) => {

    try {

        const { fundId, firstName, middleName, lastName, email, organizationName } = req.body

        const userId = req.userId;

        const fund = await models.Fund.findOne({
            attributes:['id','vcfirmId'],
            where: {
                id: fundId
            }
        });

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        const emailConfirmCode = commonHelper.generateToken(email)
        const lastCodeGeneratedAt = new Date().toISOString();

        models.User.update({
            organizationName: organizationName
        }, {
            where: {
                email: email, 
                accountType: 'LP'
            }
        });

        //create/get account details                       
        let accounttype = 'old';
        let account = await models.Account.findOne({
            where: { email: email }
        });

        //account created but he was not registered then change emailcode and send him again
        if (account && account.isEmailConfirmed == 0) {

            await models.Account.update({
                emailConfirmCode: emailConfirmCode,
                lastCodeGeneratedAt: lastCodeGeneratedAt
            }, { 
               where: { 
                    id: account.id 
                } 
            });

            account.emailConfirmCode = emailConfirmCode
        }
        if (!account) {
            account = await models.Account.create({
                email: email,
                firstName,
                lastName,
                middleName,
                emailConfirmCode: emailConfirmCode,
                isEmailConfirmed: 0,
                lastCodeGeneratedAt: lastCodeGeneratedAt
            })
            accounttype = 'new'
        }
        let accountId = account.id
        return models.sequelize.transaction(function (t) {

            return models.User.findOrCreate({
                where: { email: email, accountType: 'LP' }, defaults: {
                    firstName,
                    lastName,
                    middleName,
                    organizationName,
                    accountType: 'LP',
                    email,
                    accountId,
                    isEmailConfirmed: account.isEmailConfirmed
                }
            }).spread((user, created) => {
                return Promise.all([
                    user,
                    models.UserRole.findOrCreate({ where: { roleId: 4, userId: user.id }, defaults: { roleId: 4, userId: user.id } }), // 4 = role LP
                    models.VcFrimLp.findOrCreate({ paranoid: false, where: { vcfirmId: fund.vcfirmId, lpId: user.id, isDeleted: 0 }, defaults: { vcfirmId: fund.vcfirmId, lpId: user.id, createdBy: userId, updatedBy: userId } }).spread((vcFrimLp, created) => vcFrimLp.restore()),
                    //Status of the subscription will be invitation pending status
                    models.FundSubscription.create({ lpId: user.id, fundId: fundId, createdBy: userId, updatedBy: userId, isPrimaryInvestment: true, status: 17, isInvestorInvited: 1, invitedDate: commonHelper.getDateTime() })
                ])
            })

        }).then(async function (result) {

            const user = result[0] // user
            //update default userid in accounts table
            if (accounttype == 'new') {
                models.Account.update({ defaultUserId: user.id }, { where: { id: accountId } });
            }
                
            await models.FundSubscription.update({
                organizationName: organizationName
            }, {
                where: {
                    lpId: user.id,
                    fundId: fundId
                }
            });

            const subscriptionResult = result[3];

            //To display invitation pending or not of an investor
            const getStatus = await models.FundStatus.findOne({
                attributes: ['name'],
                where: {
                    id: subscriptionResult.status
                }
            })

            eventHelper.saveEventLogInformation('Invite sent to Investor', fundId, subscriptionResult.id, req);

            return res.json({
                data: {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    middleName: user.middleName,
                    organizationName: user.organizationName,
                    accountType: user.accountType,
                    email: user.email,
                    status: null,
                    invitedDate: subscriptionResult.invitedDate,
                    isInvestorInvited: subscriptionResult.isInvestorInvited,
                    subscriptionId: subscriptionResult.id

                }
            });

        }).catch(function (err) {
            return next(err)
        });
    } catch (e) {
        next(e)
    }

}

module.exports.updateLp = async (req, res, next) => {

    try {
        const { fundId, lpId, firstName,middleName, lastName, organizationName, email, tempObj } = req.body

        const fund = await models.Fund.findOne({
            attributes: ['id'],
            where: {
                id: fundId
            },
            include: [{
                attributes: ['email','id','firstName','lastName','middleName','accountType'],
                model: models.User,
                as: 'gp'
            }]
        });

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        const user = await models.User.findOne({
            where: {
                id: lpId
            }
        })        

        if (tempObj.email == email && tempObj.firstName == firstName && tempObj.lastName == lastName && tempObj.organizationName == organizationName && tempObj.middleName == middleName ) {      

            return res.json({
                data: {
                    id: lpId,
                    firstName: firstName,
                    lastName: lastName,
                    middleName: middleName,
                    accountType: user.accountType,
                    email: email,
                    organizationName,
                    message: messageHelper.updateMessage    
                }  
            })
        }

        const checkUser = await models.User.findOne({
            where: {
                id: {
                    [Op.ne]: lpId
                },
                accountType: 'LP',
                email
            }
        });



        if (checkUser !== null) {
            return errorHelper.error_400(res, 'email', messageHelper.emailExists);
        } else {


            
            let accountId = user.accountId 

            let accountData = {
                firstName,
                lastName,
                middleName,
                organizationName,
                email
            }
            
        //update profile to all users
        await models.User.update(accountData, { where: { accountId: accountId } });       
        //update profile to account
        await models.Account.update(accountData, { where: { id: accountId } });            

            /*
            await models.User.update({
                firstName,
                lastName,
                middleName,
                organizationName,
                email
            }, {
                where: { id: lpId }
            });

            */

            await models.FundSubscription.update({
                organizationName: organizationName
            }, {
                    where: {
                        lpId: lpId,
                        fundId: fundId
                    }
                });

            // email to GP
            const oldLpName = commonHelper.getFullName({ firstName: tempObj.firstName, lastName: tempObj.lastName, middleName:tempObj.middleName });
            const newLpName = commonHelper.getFullName({ firstName, lastName,middleName });
            const gpName = commonHelper.getFullName(fund.gp);

            if (tempObj.firstName != firstName || tempObj.lastName != lastName) {
                triggerOnNameUpdate(fund.gp.email, gpName, fund, oldLpName, newLpName, email)
            }

            // email to LP
            if (tempObj.email != email) {
                triggerOnEmailUpdate(email, gpName, fund, oldLpName, newLpName, fund.gp.email)
            }

            return res.json({
                data: {
                    id: lpId,
                    firstName: firstName,
                    lastName: lastName,
                    middleName: middleName,
                    accountType: user.accountType,
                    email: email,
                    organizationName,
                    message: messageHelper.updateMessage               
    
                }
            })             

           
        }

    } catch (e) {
        next(e)
    }
}

module.exports.addOfflineLp = async (req, res, next) => {
    try {

        const { fundId, firstName, middleName, lastName, email, address, cellNumber, organizationName } = req.body;

        const userId = req.userId;
        const userExist = await models.OfflineUsers.findOne({
            attributes: ['id', 'email', 'accountType'],
            where: { email, accountType: 'OfflineLP', deletedAt: null }
        })

        if (userExist) {
            return errorHelper.error_400(res, 'email', messageHelper.emailExists);
        }

        return models.sequelize.transaction(function (t) {
            return models.OfflineUsers.findOrCreate({
                where: { email },
                defaults: {
                    fundId,
                    firstName,
                    middleName,
                    lastName,
                    email,
                    address,
                    cellNumber,
                    organizationName,
                    accountType: 'OfflineLP'
                }
            }).spread((user, created) => {
                return Promise.all([
                    user,
                    models.FundSubscription.findOrCreate({ paranoid: false, where: { offlineUserId: user.id, fundId, isOfflineInvestor: 1 }, defaults: { offlineUserId: user.id, fundId, createdBy: userId, updatedBy: userId, isOfflineInvestor: 1, status: 2 } })
                ])
            })
        }).then(async function (result) {

            const user = result[0] // user

            await models.FundSubscription.update({
                organizationName: organizationName,
                invitedDate: moment().format('MM-DD-YYYY')
            }, {
                where: {
                    lpId: user.id,
                    fundId: fundId
                }
            });

            return res.json({
                data: {
                    id: user.id,
                    firstName: firstName,
                    lastName: lastName,
                    middleName: middleName,
                    organizationName: organizationName,
                    accountType: user.accountType,
                    email: email,
                    subscription: result[1],
                    cellNumber,
                    invitedDate: moment().format('MM-DD-YYYY')
                }
            });
        }).catch(function (err) {
            return next(err)
        });


    } catch (e) {
        next(e);
    }
}

module.exports.updateOfflineLp = async (req, res, next) => {
    try {

        const { fundId, lpId, firstName, middleName, lastName, email, address, cellNumber, organizationName, isLpOfflineToOnline } = req.body;
        const userId = req.userId;

        const fund = await models.Fund.findOne({
            attributes: ['fundManagerCommonName','fundCommonName','id','vcfirmId'],
            where: {
                id: fundId
            }
        });

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        if (!isLpOfflineToOnline) {
            const tempEmail = await checkEmail(email,false);
            if (!tempEmail) {
                return errorHelper.error_400(res, 'email', messageHelper.temporaryEmailValidate);
            }
            const existingUser = await models.OfflineUsers.findOne({
                attributes:['id'],
                where: {
                    email,
                    id: {
                        [Op.ne]: lpId
                    },
                    deletedAt: null
                },
                defaults: {
                    fundId, 
                    firstName,
                     middleName, 
                    lastName,
                    email,
                    address, 
                    cellNumber, 
                    organizationName,
                    accountType: 'OfflineLP'
                }
            });

            if (existingUser) {
                return errorHelper.error_400(res, 'email', messageHelper.emailExists);
            }


            await models.OfflineUsers.update({
                firstName,
                middleName,
                lastName,
                email,
                address,
                cellNumber,
                organizationName,
                updatedBy: userId
            }, {
                    where: { id: lpId }
                });

            await models.FundSubscription.update({
                organizationName
            }, {
                    where: {
                        lpId,
                        fundId
                    }
                });

                return res.json({
                    data: {
                        id: existingUser.id,
                        firstName: firstName,
                        lastName: lastName,
                        middleName: middleName,
                        organizationName: organizationName,
                        accountType: 'OfflineLP',
                        email: email,
                        cellNumber,
                        message: messageHelper.updateMessage                             
                    }
                })   

        

        } else {

            const validEmail = await checkEmail(email,true);
            if (!validEmail) {
                return errorHelper.error_400(res, 'email', messageHelper.emailValidate);
            }

            const emailExist = await models.User.findOne({
                where: {
                    email,
                    id: {
                        [Op.ne]: lpId
                    }
                }
            })
            const lastCodeGeneratedAt = new Date().toISOString();

            if (emailExist) {
                return errorHelper.error_400(res, 'email', messageHelper.emailExists);
            }

            const emailConfirmCode = commonHelper.generateToken(email)

            //create/get account details                       
            let account = {}
            let accounttype = 'old'
            account = await models.Account.findOne({
                where: { email: email }
            })
            //account created but he was not registered then change emailcode and send him again
            if (account && account.isEmailConfirmed == 0) {
                await models.Account.update({
                    emailConfirmCode: emailConfirmCode,
                    lastCodeGeneratedAt: lastCodeGeneratedAt
                }, { where: { id: account.id } })

                account.emailConfirmCode = emailConfirmCode
            }
            if (!account) {
                account = await models.Account.create({
                    email: email,
                    firstName: firstName,
                    lastName: lastName,
                    middleName: middleName,
                    organizationName: organizationName,
                    emailConfirmCode: emailConfirmCode,
                    isEmailConfirmed: 0,
                    lastCodeGeneratedAt: lastCodeGeneratedAt
                })
                accounttype = 'new'
            }
            let accountId = account.id

            const createUser = await models.User.create({
                firstName: firstName,
                lastName: lastName,
                middleName: middleName,
                organizationName: organizationName,
                accountType: 'LP',
                email,
                accountId,
                isEmailConfirmed: account.isEmailConfirmed
            });

            await models.UserRole.create({
                roleId: 4,
                userId: createUser.id
            }); // 4 = role LP

            //updating with offline investor id with new investor id
            await models.FundSubscription.update({
                lpId: createUser.id,
                offlineUserId: null,
                isOfflineInvestor: 0,
                isInvestorInvited: 2,
                invitedDate: moment().format('MM-DD-YYYY'),
            }, {
                    where: {
                        offlineUserId: lpId,
                        fundId
                    }
                });

            await models.VcFrimLp.create({
                vcfirmId: fund.vcfirmId,
                lpId: createUser.id,
                createdBy: userId,
                updatedBy: userId
            });

            await models.OfflineUsers.destroy({
                where: {
                    id: lpId
                }
            });


            // Send email to Lp 
            const alertData = {
                htmlMessage: messageHelper.invitationToJoinParticularFundAlertMessage,
                message: 'Invitation to join particular fund',
                fundManagerCommonName: fund.fundManagerCommonName,
                fundName: fund.fundCommonName,
                fundId: fund.id,
                sentBy: req.userId,
                type:'FUND_INVITED'
            };

            //update default userid in accounts table
            if (accounttype == 'new')
                models.Account.update({ defaultUserId: createUser.id }, { where: { id: accountId } })

            notificationHelper.triggerNotification(req, req.userId, createUser.id, false, alertData,createUser.accountId,null);

            const registerLink = `${config.get('clientURL')}/register/${account.emailConfirmCode}`;
            const content = account.isEmailConfirmed ? "to login and get going." : "to setup your account and get going."

            emailHelper.sendEmail({
                toAddress: account.email,
                subject: messageHelper.addLPEmailSubject,
                data: {
                    name: commonHelper.getFullName(createUser),
                    loginOrRegisterLink: registerLink,
                    fundCommonName: fund.fundCommonName,
                    content,
                    user:account
                },
                htmlPath: "fundLPInviation.html"
            }).catch(error => {
                commonHelper.errorHandle(error)
            })


            return res.json({
                data: {
                    id: createUser.id,
                    firstName: firstName,
                    lastName: lastName,
                    middleName: middleName,
                    organizationName: organizationName,
                    accountType: createUser.accountType,
                    email: email,
                    isInvestorInvited: 2,
                    invitedDate: moment().format('MM-DD-YYYY'),
                    message: messageHelper.updateMessage                             
                }
            })            
 

        }



    } catch (e) {
        next(e);
    }
}

module.exports.deleteLp = async (req, res, next) => {

    try {
        const { fundId, lpId } = req.body

        const { deleteClosedLpConfirm = false } = req.query

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.User,
                as: 'gp'
            }]
        })

        // get investor status
        let subscription = await models.FundSubscription.findOne({
            where: {
                fundId: fundId,
                lpId: lpId
            }
        });

        if (subscription && subscription.status == 10 && deleteClosedLpConfirm === false) {

            return errorHelper.error_400(res, 'status', messageHelper.fundIdValidate, {
                "isClosed": true
            });

        }

        const subscriptionId = _.get(subscription, 'id');

        deleteInvestor(fundId, lpId, fund.vcfirmId, subscriptionId);

        eventHelper.saveEventLogInformation('Investor Deleted', fundId, subscriptionId, req);

        return res.json({
            message: messageHelper.deleteMessage
        })

    } catch (e) {
        next(e)
    }
}

module.exports.deleteOfflineLp = async (req, res, next) => {
    try {

        const { fundId, lpId } = req.body

        const { deleteClosedLpConfirm = false } = req.query
        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.User,
                as: 'gp'
            }]
        })

        // get investor status
        let subscription = await models.FundSubscription.findOne({
            where: {
                fundId,
                offlineUserId: lpId
            }
        });

        if (subscription && subscription.status == 10 && deleteClosedLpConfirm === false) {

            return errorHelper.error_400(res, 'status', messageHelper.fundIdValidate, {
                "isClosed": true
            });
        }

        const subscriptionId = _.get(subscription, 'id');

        deleteOfflineInvestor(fundId, lpId, fund.vcfirmId, subscriptionId);

        eventHelper.saveEventLogInformation('Investor Deleted', fundId, subscriptionId, req);

        return res.json({
            message: messageHelper.deleteMessage
        })



    } catch (e) {
        next(e);
    }
}


module.exports.sendAllLpInvitations = async (req, res, next) => {
    try {

        const { fundId } = req.params;

        // fund status, not start - status 1,
        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        }) 
            //status-17 invitation pending status
            const investors = await models.FundSubscription.findAll({
                where: {
                    fundId,
                    status: 17,
                    isInvestorInvited: 1
                },
                include: [{
                    model: models.User,
                    required: true,
                    as: 'lp',
                    include:[{
                        model: models.Account,
                        as:'accountDetails',
                        required:true
                    }]                    
                }, {
                    model: models.Fund,
                    required: true,
                    as: 'fund'
                }, {
                    model: models.LpDelegate,
                    required: false,
                    as: 'lpDelegates',
                    include: [{
                        model: models.User,
                        required: true,
                        as: 'details',
                        include:[{
                            model: models.Account,
                            as:'accountDetails',
                            required:true
                        }]                        
                    }]
                }]
            });
            

            for (let investor of investors) {

                //add action history
                await models.History.create({
                    accountType: 'LP',
                    userId: investor.lp.id,
                    fundId: fundId,
                    action: 'Invited',
                    updatedBy: req.userId,
                    createdBy: req.userId
                })
                //---end                

                // GP has invited you to join fund (existing account) - LP
                const alertData = {
                    htmlMessage: messageHelper.invitationToJoinParticularFundAlertMessage,
                    message: 'Invitation to join particular fund',
                    fundManagerCommonName: investor.fund.fundManagerCommonName,
                    fundName: investor.fund.fundCommonName,
                    fundId: investor.fund.id,
                    sentBy: req.userId,
                    type:'FUND_INVITED'
                };

                notificationHelper.triggerNotification(req, req.userId, investor.lp.id, false, alertData,investor.lp.accountId,null);

                const loginOrRegisterLink = investor.lp.accountDetails.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${investor.lp.accountDetails.emailConfirmCode}`;
                const content = investor.lp.accountDetails.isEmailConfirmed ? "to login and get going." : "to setup your account and get going."

                if (investor.lp.accountDetails.isEmailNotification && req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
                    emailHelper.sendEmail({
                        toAddress: investor.lp.email,
                        subject: messageHelper.addLPEmailSubject,
                        data: {
                            name: commonHelper.getFullName(investor.lp),
                            loginOrRegisterLink: loginOrRegisterLink,
                            fundCommonName: investor.fund.fundCommonName,
                            content,
                            user:investor.lp
                        },
                        htmlPath: "fundLPInviation.html"
                    }).catch(error => {
                        commonHelper.errorHandle(error)
                    })
                }

                for (let investorDelegate of investor.lpDelegates) {

                    notificationHelper.triggerNotification(req, req.userId, investorDelegate.id, false, alertData,investorDelegate.accountId,null);

                    const loginOrRegisterLink = investorDelegate.details.accountDetails.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${investorDelegate.details.accountDetails.emailConfirmCode}`;
                    const content = investorDelegate.details.accountDetails.isEmailConfirmed ? "to login and get going." : "to setup your account and get going."

                    if (investorDelegate.details.accountDetails.isEmailNotification || req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
                        emailHelper.sendEmail({
                            toAddress: investorDelegate.details.email,
                            subject: messageHelper.assignLpDelegateEmailSubject,
                            data: {
                                name: commonHelper.getFullName(investorDelegate.details),
                                fundName: fund.legalEntity,
                                lpName: commonHelper.getInvestorName(investor),
                                loginOrRegisterLink: loginOrRegisterLink,
                                content,
                                user: investorDelegate.details
                            },
                            htmlPath: "fundLpDelegateInviation.html"
                        }).catch(error => {
                            commonHelper.errorHandle(error)
                        })
                    }
                }

            }

            //Invitation pending status to Open
            await models.FundSubscription.update({
                status: 1,
                updatedBy: req.userId,
                invitedDate: moment().format('MM-DD-YYYY'),
                isInvestorInvited: 2
            }, {
                    where: {
                        fundId,
                        status: 17,
                        isInvestorInvited: 1                         
                    }
                });

            //update fund status to open-ready
            await models.Fund.update({ statusId: 13 }, { where: { id:fundId } })

        return res.json({
            message: messageHelper.invitationsSent
        })

    } catch (e) {
        next(e);
    }
}

module.exports.sendLpInvitations = async (req, res, next) => {
    try {

        const { fundId, lpIds } = req.body;

        // fund status, not start - status 1,
        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        });

        if (fund.statusId == 13) {
            //status-17 invitation pending status
            const investors = await models.FundSubscription.findAll({
                where: {
                    fundId,
                    status: 17,
                    lpId: {
                        [Op.in]: lpIds
                    }
                },
                include: [{
                    model: models.User,
                    required: true,
                    as: 'lp',
                    include:[{
                        model: models.Account,
                        as:'accountDetails',
                        required:true
                    }]                    
                }, {
                    model: models.Fund,
                    required: true,
                    as: 'fund'
                }, {
                    model: models.LpDelegate,
                    required: false,
                    as: 'lpDelegates',
                    include: [{
                        model: models.User,
                        required: true,
                        as: 'details',
                        include:[{
                            model: models.Account,
                            as:'accountDetails',
                            required:true
                        }]                        
                    }]
                }]
            });
            

            for (let investor of investors) {

                // GP has invited you to join fund (existing account) - LP
                const alertData = {
                    htmlMessage: messageHelper.invitationToJoinParticularFundAlertMessage,
                    message: 'Invitation to join particular fund',
                    fundManagerCommonName: investor.fund.fundManagerCommonName,
                    fundName: investor.fund.fundCommonName,
                    fundId: investor.fund.id,
                    sentBy: req.userId,
                    type:'FUND_INVITED'
                };

                notificationHelper.triggerNotification(req, req.userId, investor.lp.id, false, alertData,investor.lp.accountId,null);

                const loginOrRegisterLink = investor.lp.accountDetails.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${investor.lp.accountDetails.emailConfirmCode}`;
                const content = investor.lp.accountDetails.isEmailConfirmed ? "to login and get going." : "to setup your account and get going."

                if (investor.lp.accountDetails.isEmailNotification && req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
                    emailHelper.sendEmail({
                        toAddress: investor.lp.email,
                        subject: messageHelper.addLPEmailSubject,
                        data: {
                            name: commonHelper.getFullName(investor.lp),
                            loginOrRegisterLink: loginOrRegisterLink,
                            fundCommonName: investor.fund.fundCommonName,
                            content,
                            user:investor.lp
                        },
                        htmlPath: "fundLPInviation.html"
                    }).catch(error => {
                        commonHelper.errorHandle(error)
                    })
                }

                for (let investorDelegate of investor.lpDelegates) {

                    notificationHelper.triggerNotification(req, req.userId, investorDelegate.id, false, alertData,investorDelegate.accountId,null);

                    const loginOrRegisterLink = investorDelegate.details.accountDetails.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${investorDelegate.details.accountDetails.emailConfirmCode}`;
                    const content = investorDelegate.details.accountDetails.isEmailConfirmed ? "to login and get going." : "to setup your account and get going."

                    if (investorDelegate.details.accountDetails.isEmailNotification || req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
                        emailHelper.sendEmail({
                            toAddress: investorDelegate.details.email,
                            subject: messageHelper.assignLpDelegateEmailSubject,
                            data: {
                                name: commonHelper.getFullName(investorDelegate.details),
                                fundName: fund.legalEntity,
                                lpName: commonHelper.getInvestorName(investor),
                                loginOrRegisterLink: loginOrRegisterLink,
                                content,
                                user: investorDelegate.details
                            },
                            htmlPath: "fundLpDelegateInviation.html"
                        }).catch(error => {
                            commonHelper.errorHandle(error)
                        })
                    }
                }

            }

            //Invitation pending status to Open
            await models.FundSubscription.update({
                status: 1,
                updatedBy: req.userId,
                invitedDate: moment().format('MM-DD-YYYY'),
                isInvestorInvited: 2
            }, {
                    where: {
                        fundId,
                        status: 17,
                        lpId: {
                            [Op.in]: lpIds
                        }
                    }
                });

        } else {
            await models.FundSubscription.update({
                updatedBy: req.userId,
                invitedDate: moment().format('MM-DD-YYYY'),
                isInvestorInvited: 1
            }, {
                    where: {
                        fundId,
                        status: 17,
                        lpId: {
                            [Op.in]: lpIds
                        }
                    }
                });

            await models.FundSubscription.update({
                updatedBy: req.userId,
                invitedDate: moment().format('MM-DD-YYYY'),
                isInvestorInvited: 0
            }, {
                    where: {
                        fundId,
                        status: 17,
                        lpId: {
                            [Op.notIn]: lpIds
                        }
                    }
                });

        }

        return res.json({
            message: messageHelper.invitationsSent
        })

    } catch (e) {
        next(e);
    }
}

async function deleteOfflineInvestor(fundId, lpId, vcfirmId, subscriptionId) {

    models.FundSubscription.destroy({
        where: {
            offlineUserId: lpId,
            fundId
        }
    });

    models.OfflineUsers.destroy({
        where: {
            id: lpId
        }
    })

    if (subscriptionId) {
        models.ChangeCommitment.destroy({
            where: {
                subscriptionId,
                fundId,
                isActive: 1
            }
        });

        models.DocumentsForSignature.destroy({
            where: {
                subscriptionId,
                fundId,
                isActive: 1
            }
        });

        models.FundClosings.destroy({
            where: {
                subscriptionId,
                fundId,
                isActive: 1
            }
        });
    }


    return;

}

async function deleteInvestor(fundId, lpId, vcfirmId, subscriptionId) {

    // update status(i.e., deleted for the fund but he has to be available for old funds)    
    models.VcFrimLp.destroy({
        where: {
            vcfirmId: vcfirmId,
            lpId: lpId,
        }
    });


    models.FundSubscription.destroy({
        where: {
            lpId: lpId,
            fundId: fundId
        }
    });

    if (subscriptionId) {
        models.ChangeCommitment.destroy({
            where: {
                subscriptionId: subscriptionId,
                fundId: fundId,
                isActive: 1
            }
        });

        models.DocumentsForSignature.destroy({
            where: {
                subscriptionId: subscriptionId,
                fundId: fundId,
                isActive: 1
            }
        });

        models.FundClosings.destroy({
            where: {
                subscriptionId: subscriptionId,
                fundId: fundId,
                isActive: 1
            }
        });
    }


    return;

}

async function triggerOnNameUpdate(gpEmail, gpName, fund, oldLpName, newLpName, newEmail) {

    emailHelper.sendEmail({
        toAddress: gpEmail,
        subject: `Email Notification - Vanilla`,
        data: {
            oldLpName: oldLpName,
            newLpName: newLpName,
            gpName: gpName,
            fundName: fund.legalEntity,
            updatedUserAccountType:'Investor',
            user:fund.gp
        },
        htmlPath: "nameChangeToGP.html"
    }).catch(error => {
        commonHelper.errorHandle(error)
    })

    emailHelper.sendEmail({
        toAddress: newEmail,
        subject: `Email Notification - Vanilla`,
        data: {
            oldLpName: oldLpName,
            newLpName: newLpName,
            fundName: fund.legalEntity
        },
        htmlPath: "nameChangeToLP.html"
    }).catch(error => {
        commonHelper.errorHandle(error)
    })

}

async function triggerOnEmailUpdate(email, gpName, fund, oldLpName, newLpName, gpEmail) {

    emailHelper.sendEmail({
        toAddress: gpEmail,
        subject: `Email Notification - Vanilla`,
        data: {
            oldLpName: oldLpName,
            newLpName: newLpName,
            gpName: gpName,
            fundName: fund.legalEntity,
            email: email
        },
        htmlPath: "emailChangeToGP.html"
    }).catch(error => {
        commonHelper.errorHandle(error)
    })

    emailHelper.sendEmail({
        toAddress: email,
        subject: `Email Notification - Vanilla`,
        data: {
            oldLpName: oldLpName,
            newLpName: newLpName,
            fundName: fund.legalEntity,
            email: email
        },
        htmlPath: "emailChangeToLP.html"
    }).catch(error => {
        commonHelper.errorHandle(error)
    })
}

module.exports.assignLp = async (req, res, next) => {

    const { fundId, lpId } = req.body;

    const userId = req.userId;

    if (!Number(fundId) && !Number(lpId)) {
        return res.status(400).json({
            error: true,
            message: 'fundId and lpId required.'
        });
    }

    const subscriptionOfLP = await models.FundSubscription.count({
        where: {
            lpId: lpId,
            fundId: fundId,
            //Rescind-6, Not Interested-3 
            status: {
                [Op.notIn]: [6, 3]
            }
        }
    });

    const data = { lpId: lpId, fundId: fundId, createdBy: userId, updatedBy: userId, isPrimaryInvestment: 1, isInvestorInvited: 1, status: 17 };

    if (subscriptionOfLP > 0) {
        data.isPrimaryInvestment = 0;
    }

    subscription = await models.FundSubscription.create(data);
    
    const lp = await models.User.findOne({
        where: {
            id: lpId
        }
    });

    const trackerTitle = subscription.investorType == 'LLC' ? subscription.entityName :
        (subscription.investorType == 'Trust' ? subscription.trustName : (subscription.investorType == 'Individual' ? (subscription.organizationName || lp.organizationName) : null)) || '(New Investor Pending Input)';

    //To display invitation pending or not of an investor
    const getStatus = await models.FundStatus.findOne({
        where: {
            id: subscription.status
        }
    })

    return res.json({
        subscriptionId: subscription.id,
        id: lp.id,
        firstName: lp.firstName,
        trackerTitle: trackerTitle,
        lastName: lp.lastName,
        middleName: lp.middleName,
        email: lp.email,
        profilePic: lp.profilePic,
        organizationName: subscription.organizationName || lp.organizationName,
        updatedAt: lp.updatedAt,
        accountType: lp.accountType,
        isInvestorInvited: subscription.isInvestorInvited,
        status: getStatus.name
    });

}

//assign investors to fund from unassigned
module.exports.assignInvestors = async (req, res, next) => {
    const { fundId, lpIds } = req.body
    const userId = req.userId
    let result = []
    logger.info("*****Assign investors to the fund Start*******"+fundId)
    //To display invitation pending or not of an investor
    let getStatus = await models.FundStatus.findOne({
        attributes:['name'],
        where: {
            id: 17
        }
    })  
    //check for primary investment 
    let subscriptionOfLPs = await models.FundSubscription.findAll({ 
        attributes:['id','lpId'],       
        where: {        
            lpId: {
                [Op.in]: lpIds
            },
            fundId: fundId,            
            status: {
                [Op.notIn]: [6, 3]   //Rescind-6, Not Interested-3 
            }
        }
    }) 
    //get all investors info
    let lpUsers = await models.User.findAll({
        where: {
            id:{
                [Op.in]: lpIds
            }  
        }
    })
    //save all data into fund subscription
    for(let lpId of lpIds){

        logger.info("*****Assign investors to the fund investor id*******"+lpId)

        let isPrimaryInvestment = (_.find(subscriptionOfLPs, { lpId: lpId})) ? 0 : 1

        let data = { lpId: lpId, fundId: fundId, createdBy: userId, updatedBy: userId, isPrimaryInvestment: isPrimaryInvestment, isInvestorInvited: 1, status: 17 }

        let subscription = await models.FundSubscription.create(data)
        
        let lp = _.find(lpUsers, { id: lpId})
       
        let trackerTitle = subscription.investorType == 'LLC' ? subscription.entityName :
        (subscription.investorType == 'Trust' ? subscription.trustName : (subscription.investorType == 'Individual' ? (subscription.organizationName || lp.organizationName) : null)) || '(New Investor Pending Input)'

        result.push(
            {
            subscriptionId: subscription.id,
            id: lp.id,
            firstName: lp.firstName,
            trackerTitle: trackerTitle,
            lastName: lp.lastName,
            middleName: lp.middleName,
            email: lp.email,
            profilePic: lp.profilePic,
            organizationName: subscription.organizationName || lp.organizationName,
            updatedAt: lp.updatedAt,
            accountType: lp.accountType,
            isInvestorInvited: subscription.isInvestorInvited,
            status: null
            }
        )   
        
    } 
    logger.info("*****End ----Assign investors to the fund *******")
    return res.json(result)
}

//update lp delegate
module.exports.updateLpDelegate = async (req, res, next) => {
    try {
        const { fundId, userId, firstName,middleName, lastName, email, tempObj } = req.body

        const fund = await models.Fund.findOne({
            attributes: ['id'],
            where: {
                id: fundId
            },
            include: [{
                attributes: ['email','id','firstName','lastName','middleName'],
                model: models.User,
                as: 'gp'
            }]
        });

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }


        const user = await models.User.findOne({
            where: {
                id: userId
            }
        })
                
        if (tempObj.email == email && tempObj.firstName == firstName && tempObj.lastName == lastName && tempObj.middleName == middleName ) {         

            return res.json({
                data: {
                    id: userId,
                    firstName: firstName,
                    lastName: lastName,
                    middleName: middleName,
                    accountType: user.accountType,
                    email: email,
                    message: messageHelper.updateMessage               
    
                }
            })  
        }

        const checkUser = await models.User.findOne({
            where: {
                id: {
                    [Op.ne]: userId
                },
                accountType: 'LPDelegate',
                email
            }
        })

        if (checkUser !== null) {
            return errorHelper.error_400(res, 'email', messageHelper.emailExists);
        } else {

        
        let accountId = user.accountId 

        let accountData = {
            firstName,
            lastName,
            middleName,
            email
        }
            
        //update profile to all users
        await models.User.update(accountData, { where: { accountId: accountId } });       
        //update profile to account
        await models.Account.update(accountData, { where: { id: accountId } });  
       
  
        return res.json({
            data: {
                id: userId,
                firstName: firstName,
                lastName: lastName,
                middleName: middleName,
                accountType: user.accountType,
                email: email,
                message: messageHelper.updateMessage               

            }
        }) 
        
        }

    } catch (e) {
        next(e)
    }
}