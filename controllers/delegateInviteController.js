
const models = require('../models/index');
const config = require('../config/index');
const _ = require('lodash');
const { Op } = require('sequelize');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const eventHelper = require('../helpers/eventHelper');
const notificationHelper = require('../helpers/notificationHelper');

const isGpDelegateNew = async (req, res, next) => {

    try {
        const { email, fundId } = req.body;

        let status = null;

        let firstName = ""
        let lastName  = ""        
        let middleName  = ""

        const user = await models.User.findOne({
            attributes: ['id', 'firstName','middleName', 'lastName'],
            where: { email: email, accountType: 'GPDelegate' }
        })

        if (Boolean(user)) {

            firstName = _.get(user, 'firstName', '')
            lastName = _.get(user, 'lastName', '')
            middleName = _.get(user, 'middleName', '')            

            const isAlreadyAddedAsDelegateToFund = await models.GpDelegates.findOne({
                attributes: ['id', 'gpId'],
                where: {
                    fundId: fundId,
                    delegateId: user.id
                }
            });

            status = Boolean(isAlreadyAddedAsDelegateToFund) ? 2 : 1;

        } else {

            const account = await models.Account.findOne({
                attributes: ['id', 'firstName', 'lastName','middleName'],
                where: { email: email}
            })
            if(account){
                firstName = _.get(account, 'firstName', '')
                lastName =  _.get(account, 'lastName', '')
                middleName =  _.get(account, 'middleName', '')
                status = 3
            } else {
                status = 0
            }                
 
        }

        return res.json({
            status: status,
            firstName: firstName,
            lastName: lastName,
            middleName: middleName,
            email: email
        });

    } catch (error) {
        next(error);
    }

}


const fundGpDelegateInvite = async (req, res, next) => {

    try {
        const { fundId, firstName, lastName,middleName, email } = req.body

        const userId = req.userId

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.User,
                as: 'gp'
            }]
        })



        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }

        const emailConfirmCode = commonHelper.generateToken(email) // generate token 

        const lastCodeGeneratedAt = new Date().toISOString(); 

        //create/get account details                       
        let account = {}
        let accounttype = 'old'
        account = await models.Account.findOne({
            where: { email: email }
        })
        //account created but he was not registered then change emailcode and send him again
        if(account && account.isEmailConfirmed==0){ 
            await models.Account.update({                 
                emailConfirmCode:emailConfirmCode,
                lastCodeGeneratedAt:lastCodeGeneratedAt                
            },{where:{id:account.id}})

            account.emailConfirmCode = emailConfirmCode
        }        
        if(!account){
            account = await models.Account.create({
                email:email,
                firstName,
                lastName,
                middleName,
                emailConfirmCode:emailConfirmCode,
                isEmailConfirmed:0,
                lastCodeGeneratedAt:lastCodeGeneratedAt                
            })
            accounttype = 'new'
        }        
        let accountId = account.id        

        return models.sequelize.transaction(function (t) {

            return models.User.findOrCreate({
                where: { email: email, accountType: 'GPDelegate' }, defaults: {
                    firstName,
                    lastName,
                    middleName,
                    accountType: 'GPDelegate',
                    email,
                    accountId,
                    isEmailConfirmed:account.isEmailConfirmed
                }
            }).spread((user, created) => {
                return Promise.all([
                    user,
                    models.UserRole.findOrCreate({ where: { roleId: 3, userId: user.id }, defaults: { roleId: 3, userId: user.id } }),
                    models.GpDelegates.findOrCreate({ paranoid: false, where: { fundId: fundId, gpId: fund.gp.id, delegateId: user.id }, defaults: { fundId: fundId, delegateId: user.id, gpId: fund.gp.id, createdBy: userId, updatedBy: userId } }).spread((gpDelegates, created) => gpDelegates.restore()),
                    models.VcFrimDelegate.findOrCreate({ paranoid: false, where: { vcfirmId: fund.vcfirmId, delegateId: user.id, type: 'GP' }, defaults: { vcfirmId: fund.vcfirmId, delegateId: user.id, type: 'GP', createdBy: userId, updatedBy: userId } }).spread((vcFrimDelegate, created) => vcFrimDelegate.restore())
                ]);
            })

        }).then(function (result) {

            const user = result[0];

            //update default userid in accounts table
            if(accounttype=='new')
                models.Account.update({defaultUserId:user.id},{where:{id:accountId}})

            //if (fund.statusId == 13) { // 'Open-Ready' 13
            const loginOrRegisterLink = account.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${account.emailConfirmCode}`;
            const content = account.isEmailConfirmed ? "to login and get going." : "to setup your account and get going." 

           // if (user.isEmailNotification && req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
                emailHelper.sendEmail({
                    toAddress: user.email,
                    subject: messageHelper.assignLpDelegateEmailSubject,
                    data: {
                        name: commonHelper.getFullName({firstName, lastName,middleName}),
                        fundName: fund.legalEntity,
                        gpName: commonHelper.getFullName(req.user),
                        loginOrRegisterLink: loginOrRegisterLink,
                        content,
                        user
                    },
                    htmlPath: "fundGPInviation.html"
                });
           // }
            //}

            eventHelper.saveEventLogInformation('Invite Sent To Delegate', fund.id, '', req);

            // Invitation to help in particular fund - GP delegate (Exisitng account)
            const alertData = {
                htmlMessage: messageHelper.invitationToHelpParticularFundAlertMessage,
                message: 'Invitation to help in particular fund',
                gpName: commonHelper.getFullName(fund.gp),
                fundManagerCommonName: fund.fundManagerCommonName,
                fundName: fund.fundCommonName,
                fundId: fund.id,
                sentBy: req.userId,
                type:'FUND_INVITED'
            };

            notificationHelper.triggerNotification(req, req.userId, user.id, false, alertData,user.accountId,user);

            return res.json({
                data: {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    middleName: user.middleName,
                    accountType: user.accountType,
                    email: user.email,
                    isEmailConfirmed:account.isEmailConfirmed,
                    emailConfirmCode:account.emailConfirmCode
                }
            })

        }).catch(function (err) {
            return next(err)
        });
    } catch (e) {
        next(e)
    }

}

const isLpDelegateNew = async (req, res, next) => {

    try {
        const { email, fundId, subscriptionId } = req.body;

        let firstName = ""
        let lastName  = ""
        let middleName = ""        

        const user = await models.User.findOne({
            attributes: ['id', 'firstName', 'middleName', 'lastName'],
            where: { email: email, accountType: 'LPDelegate' }
        })

        let status = null;

        if (!Boolean(user)) {

            const account = await models.Account.findOne({
                attributes: ['id', 'firstName', 'lastName','middleName'],
                where: { email: email}
            })
            if(account){
                firstName = _.get(account, 'firstName', '')
                lastName =  _.get(account, 'lastName', '')                
                middleName =  _.get(account, 'middleName', '')
                status = 3
            } else {
                status = 0
            }    

           
        } else {

            firstName = _.get(user, 'firstName', '')
            lastName = _.get(user, 'lastName', '')
            middleName = _.get(user, 'middleName', '')

            const delegate = await models.LpDelegate.findOne({
                attributes: ['id', 'lpId'],
                where: {
                    fundId: fundId,
                    subscriptionId: subscriptionId,
                    delegateId: _.get(user, 'id')
                }
            })
            if (Boolean(delegate)) {
                status = 2; // already added as LP delegate
            } else {
                status = 1; // existing lp delegate, but not for this subscription
            }

        }

        return res.json({
            status: status,
            firstName: firstName,
            lastName: lastName,
            middleName: middleName,
            email: email
        })

    } catch (error) {
        next(error);
    }

}


const fundLpDelegateInvite = async (req, res, next) => {

    try {
        const { fundId, firstName, middleName, lastName, cellNumber, email, organizationName, subscriptionId,lpId,isInvestorDelegateBulkInvite=false} = req.body

        //lpId when LpDelegate is added from Gp login
        const userId = lpId? lpId:req.userId

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        })

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        const emailConfirmCode = commonHelper.generateToken(email) // generate token 

        const lastCodeGeneratedAt = new Date().toISOString();

        //create/get account details                       
        let account = {}
        let accounttype = 'old'
        account = await models.Account.findOne({
            where: { email: email }
        })
        //account created but he was not registered then change emailcode and send him again
        if(account && account.isEmailConfirmed==0){ 
            await models.Account.update({                 
                emailConfirmCode:emailConfirmCode,
                lastCodeGeneratedAt:lastCodeGeneratedAt                
            },{where:{id:account.id}})

            account.emailConfirmCode = emailConfirmCode
        }        
        if(!account){
            account = await models.Account.create({
                email:email,
                firstName,
                lastName,
                middleName,
                emailConfirmCode:emailConfirmCode,
                isEmailConfirmed:0,
                lastCodeGeneratedAt:lastCodeGeneratedAt                
            })
            accounttype = 'new'
        }        
        let accountId = account.id  
                        

        return models.sequelize.transaction(function (t) {

            return models.User.findOrCreate({
                where: { email: email, accountType: 'LPDelegate' }, defaults: {
                    firstName,
                    lastName,
                    middleName,
                    accountType: 'LPDelegate',
                    email,
                    accountId,
                    isEmailConfirmed:account.isEmailConfirmed
                }
            }).spread((user, created) => {

                return Promise.all([

                    user,
                    models.UserRole.findOrCreate({
                        where: { roleId: 5, userId: user.id }, defaults: { roleId: 5, userId: user.id }
                    }),
                    models.LpDelegate.findOrCreate({
                        paranoid: false,
                        where: { fundId: fundId, lpId: userId, delegateId: user.id },
                        defaults: { fundId: fundId, delegateId: user.id, lpId: userId, subscriptionId: subscriptionId, createdBy: userId, updatedBy: userId }
                    })
                        .spread((lpDelegate, created) => lpDelegate.restore()),
                    models.VcFrimDelegate.findOrCreate({
                        paranoid: false,
                        where: { vcfirmId: userId, delegateId: user.id, type: 'LP' },
                        defaults: { vcfirmId: userId, delegateId: user.id, type: 'LP', createdBy: userId, updatedBy: userId }
                    })
                        .spread((vcFrimDelegate, created) => vcFrimDelegate.restore()),
                    
                    created
                ])

            })

        }).then(function (result) {
            const user = result[0];
            let isNewUser = result[4]

            //update default userid in accounts table
            if(accounttype=='new')
                models.Account.update({defaultUserId:user.id},{where:{id:accountId}})
                
            
            //If not bulk invite 
            if(!isInvestorDelegateBulkInvite){

                const loginOrRegisterLink = account.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${account.emailConfirmCode}`;
                const content = account.isEmailConfirmed ? "to login and get going." : "to setup your account and get going." 

                
                if (user.isEmailNotification || req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
                    
                    emailHelper.sendEmail({
                        toAddress: user.email,
                        subject: messageHelper.assignLpDelegateEmailSubject,
                        data: {
                            name: commonHelper.getFullName(user),
                            fundName: fund.legalEntity,
                            lpName: commonHelper.getFullName(req.user),
                            loginOrRegisterLink: loginOrRegisterLink,
                            content,
                            user
                        },
                        htmlPath: "fundLpDelegateInviation.html"
                    });
                }
    
                // Invitation to help in particular fund - LP delegate            
                const alertData = {
                    htmlMessage: messageHelper.invitationToHelpParticularFundAlertMessageToLP,
                    message: 'Invitation to help in particular fund',
                    subscriptionId: subscriptionId,
                    lpName: commonHelper.getFullName(req.user),
                    fundManagerCommonName: fund.fundManagerCommonName,
                    fundName: fund.fundCommonName,
                    fundId: fund.id,
                    sentBy: req.userId,
                    type:'FUND_INVITED'
                };
    
                notificationHelper.triggerNotification(req, req.userId, user.id, false, alertData,user.accountId,user);
            }
                
            eventHelper.saveEventLogInformation('Invite Sent To Delegate', fund.id, subscriptionId, req);

            return res.json({
                data: {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    middleName: user.middleName,
                    organizationName: user.organizationName,
                    accountType: user.accountType,
                    email: user.email, 
                    createdAt:user.createdAt
                },
                isNewUser
            })

        }).catch(function (err) {
            return next(err)
        });
    } catch (e) {
        return next(e)
    }

}

const getVCFirmsOfDelegate = async(req, res, next) => {

    const delegateId = req.params.delegateId;

    const user = req.user;

    const vcFirmDelegates =  user.accountType == 'GPDelegate' ? await models.VcFrimDelegate.findAll({
        where:{
            delegateId
        }
    }) :  await models.VcFirmSignatories.findAll({
        where:{
            signatoryId : delegateId
        }
    });

    //for signatory -- hide the FM with same email id
    //for delegate  -- show the FM with same email id
    let userWhereFilter = {
        isEmailConfirmed : 1
    }
    if(req.user.accountType=='SecondaryGP'){
        userWhereFilter.email = {
            [Op.ne]: req.user.email
        }        
    }

    const vcfirmIds = _.map(vcFirmDelegates,'vcfirmId')

    const vcFirms = await models.VCFirm.findAll({
        where:{
            id:{
                [Op.in]:vcfirmIds
            }
        },
        attributes:['id'],
        include:[{
            model: models.User,
            attributes:['firstName','lastName','middleName'],
            as: 'gp',
            require: true,
            where : userWhereFilter
        }]
    });

    const data = [];
    _.forEach(vcFirms,vcFirm=>{
        data.push({
            id:vcFirm.id,
            name:commonHelper.getFullName(vcFirm.gp)
        })
    })

       
    return res.json(data);
}

module.exports = {
    fundGpDelegateInvite,
    fundLpDelegateInvite,
    isLpDelegateNew,
    isGpDelegateNew,
    getVCFirmsOfDelegate
}