const models = require('../models/index');
const config = require('../config/index');
const { Op } = require('sequelize');
const commonHelper = require('../helpers/commonHelper');
const _ = require('lodash');

const getDocumentTypes = async (req, res, next) => {
    try {

        const documentTypes = [
            {
                type: 'CHANGE_COMMITMENT_AGREEMENT',
                name: 'Capital Commitment Increase Letter'
            },
            // {
            //     type: 'FUND_AGREEMENT',
            //     name: 'Fund Agreement'
            // },
            {
                type: 'CONSOLIDATED_FUND_AGREEMENT',
                name: 'Consolidated Fund Agreement'
            },
            {
                type: 'SUBSCRIPTION_AGREEMENT',
                name: 'Subscription Agreement'
            },
            {
                type: 'SIDE_LETTER_AGREEMENT',
                name: 'Side Letter Agreement'
            },
            {
                type: 'CONSOLIDATED_AMENDMENT_AGREEMENT',
                name: 'Consolidated Amendment Agreement'
            },
            {
                type: 'Archived',
                name: 'Archived'
            }
        ]

        return res.json({
            data: {
                documentTypes
            }
        })

    } catch (e) {
        next(e)
    }
}

const getGPDocuments = async (req, res, next) => {
    try {
        const { fundId } = req.params;

        const { orderCol = 'createdAt', order = 'DESC', docType, lpId } = req.query;

        let orderFilter = [[orderCol, order]];

        if (orderCol === 'firstName') {
            orderFilter = [
                ['subscription', 'lp', orderCol, order]
            ];
        }

        if (orderCol === 'docType') {
            orderFilter = [[orderCol, order]];
        }

        let documentFilter = {
            fundId: fundId,
            isPrimaryGpSigned: true,
            isPrimaryLpSigned: true,
            docType: {
                [Op.ne] : 'FUND_AGREEMENT'

            }
        };


        if(docType == 'Archived'){
            
            documentFilter.docType={
                [Op.in] : ['CONSOLIDATED_FUND_AGREEMENT','CONSOLIDATED_AMENDMENT_AGREEMENT']
            }
        
            documentFilter.isArchived = 1

        }else if(docType !== undefined && docType !== 'Archived'){
            documentFilter.isArchived = 0
            documentFilter.docType = docType
        }

        let ipFilter = {};

        if (lpId) {
            ipFilter = {
                id:lpId
            }
        }


        const documentsForSignature = await models.DocumentsForSignature.findAll({
            where: documentFilter,
            order: orderFilter,
            include: [{
                attributes: ['id'],
                model: models.FundSubscription,
                as: 'subscription',
                required: false,
                where: {
                    fundId: fundId
                },
                include: [{
                    attributes: ['timezone'],
                    model: models.Fund,
                    as: 'fund',
                    required: false,
                    include: [{
                        model: models.timezone,
                        as: 'timeZone',
                        required: false
                    }]
                },{
                    where: ipFilter,
                    attributes: ['firstName','middleName', 'lastName', 'profilePic'],
                    model: models.User,
                    as: 'lp',
                    required: false
                },{
                    where: ipFilter,
                    attributes: ['firstName','middleName', 'lastName'],
                    model: models.OfflineUsers,
                    as: 'offlineLp',
                    required: false
                }]
            }]
        });


        const docs = [];
            
      

        //To display the doctype Archived with numberic naming convention
        let countForConsolidatedFA =1;
        let countForConsolidatedA =1;

        for (const documentForSignature of documentsForSignature) {

            let condition = lpId ? (documentForSignature.subscription && (documentForSignature.subscription.lp || documentForSignature.subscription.offlineLp) && documentForSignature.docType !== 'AMENDMENT_AGREEMENT') : documentForSignature && documentForSignature.docType !== 'AMENDMENT_AGREEMENT';

            if(condition){
                // send id to document viewer call if doctype is 'FUND_AGREEMENT' or 'CONSOLIDATED_FUND_AGREEMENT'
                let docurl =  (documentForSignature.docType ==  'SIDE_LETTER_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/viewsia/${documentForSignature.subscriptionId}`: (documentForSignature.docType ==  'FUND_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/docs/${documentForSignature.id}`: (documentForSignature.docType ==  'SUBSCRIPTION_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/sa/${documentForSignature.subscriptionId}` : (documentForSignature.docType ==  'CONSOLIDATED_FUND_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/docs/${documentForSignature.id}`  : (documentForSignature.docType ==  'CHANGE_COMMITMENT_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/viewcc/${documentForSignature.subscriptionId}` : (documentForSignature.docType == 'CONSOLIDATED_AMENDMENT_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/docs/${documentForSignature.id}` :''


                let documentType = ((documentForSignature.docType ==  'CONSOLIDATED_FUND_AGREEMENT' && documentForSignature.isArchived) ? `Prior Consolidated Fund Agreement ${countForConsolidatedFA}` : documentForSignature.docType == 'CONSOLIDATED_FUND_AGREEMENT' ? 'Consolidated Fund Agreement' : (documentForSignature.docType ==  'CONSOLIDATED_AMENDMENT_AGREEMENT' && documentForSignature.isArchived) ? `Prior Consolidated Amendment Agreement ${countForConsolidatedA}` : documentForSignature.docType == 'CONSOLIDATED_AMENDMENT_AGREEMENT' ? 'Consolidated Amendment Agreement': documentForSignature.docType) 


                if(documentForSignature.docType ==  'CONSOLIDATED_FUND_AGREEMENT' && documentForSignature.isArchived){
                    countForConsolidatedFA = countForConsolidatedFA+1;
                }

                if(documentForSignature.docType ==  'CONSOLIDATED_AMENDMENT_AGREEMENT' && documentForSignature.isArchived){
                    countForConsolidatedA = countForConsolidatedA+1;
                }
                //date conversion based on fund timezone
                let timezone_date = ''; let timezone = '';
                if(documentForSignature.subscription && documentForSignature.subscription.fund.timeZone && documentForSignature.subscription.fund.timeZone.code) {
                    timezone_date = await commonHelper.setDateUtcToOffset(documentForSignature.createdAt, documentForSignature.subscription.fund.timeZone.code).then(function (value) {
                        return value;
                    });
                    timezone = documentForSignature.subscription.fund.timeZone.displayName;
                } else {
                    timezone_date = await commonHelper.getDateAndTime(documentForSignature.createdAt);
                }

                docs.push({
                    id: documentForSignature.id,
                    subscriptionId: documentForSignature.subscriptionId,
                    fundId: documentForSignature.fundId,
                    firstName: documentForSignature.subscription && documentForSignature.subscription.lp ? 
                        documentForSignature.subscription.lp.firstName : (documentForSignature.subscription && documentForSignature.subscription.offlineLp ? documentForSignature.subscription.offlineLp.firstName : null),
                    lastName: documentForSignature.subscription && documentForSignature.subscription.lp ? 
                        documentForSignature.subscription.lp.lastName : (documentForSignature.subscription && documentForSignature.subscription.offlineLp ? documentForSignature.subscription.offlineLp.lastName : null),
                    middleName: documentForSignature.subscription && documentForSignature.subscription.lp ? 
                        documentForSignature.subscription.lp.middleName: (documentForSignature.subscription && documentForSignature.subscription.offlineLp ? documentForSignature.subscription.offlineLp.middleName : null),
                    documentType: documentForSignature.docType,
                    name: documentType,
                    createdAt: timezone_date,
                    documentUrl: docurl,
                    profilePic: documentForSignature.subscription && documentForSignature.subscription.lp? documentForSignature.subscription.lp.profilePic : null,
                    isPrimaryGpSigned: documentForSignature.isPrimaryGpSigned,
                    isPrimaryLpSigned: documentForSignature.isPrimaryLpSigned,
                    isActive: documentForSignature.isActive,
                    timezone: timezone,
                })
            }
        }

        return res.json({
            data: docs
        });


    } catch (error) {
        next(error);
    }

}


const getLPDocuments = async (req, res, next) => {

    try {
        const { subscriptionId } = req.params;

        const documentsForSignature = await models.DocumentsForSignature.findAll({
            where: {
                subscriptionId: subscriptionId,
                isPrimaryGpSigned: true,
                isPrimaryLpSigned: true,
                isArchived:0,
                isActive: true
            },
            include: [{
                attributes: ['id'],
                model: models.FundSubscription,
                as: 'subscription',
                required: false,
                include: [{
                    attributes: ['timezone'],
                    model: models.Fund,
                    as: 'fund',
                    required: false,
                    include: [{
                        model: models.timezone,
                        as: 'timeZone',
                        required: false
                    }]
                }]
            }] 
             
        });

        const docs = [];

        let i = 1;
        let j = 1;

        for (const documentForSignature of documentsForSignature) {

            //date conversion based on fund timezone
            let timezone_date = ''; let timezone = '';
            if(documentForSignature.subscription && documentForSignature.subscription.fund.timeZone && documentForSignature.subscription.fund.timeZone.code) {
                timezone_date = await commonHelper.setDateUtcToOffset(documentForSignature.createdAt, documentForSignature.subscription.fund.timeZone.code).then(function (value) {
                    return value;
                });
                timezone = documentForSignature.subscription.fund.timeZone.displayName;
            } else {
                timezone_date = await commonHelper.getDateAndTime(documentForSignature.createdAt);
            }

            let docurl =  (documentForSignature.docType ==  'SIDE_LETTER_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/viewsia/${documentForSignature.subscriptionId}`: (documentForSignature.docType ==  'FUND_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/docs/${documentForSignature.id}`  : (documentForSignature.docType ==  'SUBSCRIPTION_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/sa/${documentForSignature.subscriptionId}` : (documentForSignature.docType ==  'CONSOLIDATED_FUND_AGREEMENT') ? `${config.get('serverURL')}/api/v1//document/view/docs/${documentForSignature.id}` : (documentForSignature.docType ==  'CHANGE_COMMITMENT_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/viewcc/${documentForSignature.subscriptionId}` : (documentForSignature.docType ==  'AMENDMENT_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/docs/${documentForSignature.id}` : (documentForSignature.docType ==  'CONSOLIDATED_AMENDMENT_AGREEMENT') ? `${config.get('serverURL')}/api/v1/document/view/docs/${documentForSignature.id}` :''    
            
            let docType = (documentForSignature.docType ==  'FUND_AGREEMENT' && documentForSignature.isArchived) ? `Prior Fund Agreement ${i}` : documentForSignature.docType == 'FUND_AGREEMENT' ? 'Fund Agreement':    (documentForSignature.docType == 'AMENDMENT_AGREEMENT' && documentForSignature.isArchived) ? `Prior Amendment Agreement ${j}`: documentForSignature.docType == 'AMENDMENT_AGREEMENT' ?'Amendment Agreement' : documentForSignature.docType
            
            i = (documentForSignature.docType ==  'FUND_AGREEMENT' && documentForSignature.isArchived) ? i+=1 : i;
            j = (documentForSignature.docType ==  'AMENDMENT_AGREEMENT' && documentForSignature.isArchived) ? j+=1 : j;

            docs.push({
                id: documentForSignature.id,
                name: docType,
                subscriptionId: documentForSignature.subscriptionId,
                fundId: documentForSignature.fundId,
                documentType: documentForSignature.docType,
                createdAt: timezone_date,
                timezone: timezone,
                documentUrl: docurl
            })
        }

        return res.json({
            data: docs
        });

    } catch (error) {
        next(error);
    }
}


module.exports = {
    getDocumentTypes,
    getGPDocuments,
    getLPDocuments
}
