const models = require('../../models/index');
const _ = require('lodash');
const commonHelper = require('../../helpers/commonHelper');
const emailHelper = require('../../helpers/emailHelper');
const notificationHelper = require('../../helpers/notificationHelper');
const SAPDFGeneratorBySubscriptionIDHelper = require("../../helpers/SAPDFGeneratorBySubscriptionIDHelper");
const config = require('../../config/index');
const uuidv1 = require('uuid/v1');
const { Op } = require('sequelize');
const fs = require('fs');

const run = async (fundId, req, inProgressLp = false,from,lpSubscriptionId=0) => {

    const statusIds = (inProgressLp === true) ? [2, 7, 16] : [7];

    let subscriptions;
    let subscriptionIds;

    //fund level edit subscription form
    if(lpSubscriptionId==0){
        subscriptions = await models.FundSubscription.findAll({
            where: {
                fundId: fundId,
                status: {
                    [Op.in]: statusIds
                },
                [Op.or]: [{isOfflineInvestor: null}, {isOfflineInvestor: 0}]            
            },
            include: [{
                model: models.User,
                as: 'lp',
                required: true
            }]
        }) 

    } else { //individual LP
        subscriptions = await models.FundSubscription.findAll({
            where: {
                fundId: fundId,
                id:lpSubscriptionId,
                status: {
                    [Op.in]: statusIds
                },
                [Op.or]: [{isOfflineInvestor: null}, {isOfflineInvestor: 0}]            
            },
            include: [{
                model: models.User,
                as: 'lp',
                required: true
            }]
        }) 

    }    

    subscriptionIds = _.map(subscriptions, 'id');

    if (!inProgressLp) {
        const documents = await models.DocumentsForSignature.findAll({
            where: {
                fundId: fundId,
                isActive: 1,
                lpSignCount: {
                    [Op.gt]: 0
                }
            }
        })

        subscriptionIds = _.uniq(_.map(documents, 'subscriptionId'));
    }
  

    if (subscriptionIds.length > 0) {

        //update old docs
        let updateObject = { 
            isActive: 0             
        }

        await models.SignatureTrack.update(updateObject, {
            where: {
                documentType: 'SUBSCRIPTION_AGREEMENT',
                subscriptionId: {
                    [Op.in]: subscriptionIds
                }
            }
        })
      
        await models.DocumentsForSignature.update(updateObject, {
            where: {
                fundId: fundId,
                docType: 'SUBSCRIPTION_AGREEMENT',
                subscriptionId: {
                    [Op.in]: subscriptionIds
                }
            }
        });
        
        await models.SignatureTrack.update(updateObject, {
            where: {
                documentType: 'FUND_AGREEMENT',
                subscriptionId: {
                    [Op.in]: subscriptionIds
                }
            }
        });
        
        await models.DocumentsForSignature.update(updateObject, {
            where: {
                fundId: fundId,
                docType: 'FUND_AGREEMENT',
                subscriptionId: {
                    [Op.in]: subscriptionIds
                }
            }
        });

        for (let subscription of subscriptions) {

            //get side letter info
            let sideLetterInfo = await models.DocumentsForSignature.findOne({
                where: {
                    isActive: 1,
                    docType: 'SIDE_LETTER_AGREEMENT',
                    subscriptionId:subscription.id
                },
                order: [['id', 'DESC']]
            })
            //if user has side letter then update signaturetrack,documentforsignature to inactive in both tables and create the new record
            if(sideLetterInfo){            

                await models.SignatureTrack.update(updateObject, {
                    where: {
                        documentType: 'SIDE_LETTER_AGREEMENT',
                        subscriptionId:subscription.id
                    }
                });       

                await models.DocumentsForSignature.update(updateObject, {
                    where: {
                        fundId: fundId,
                        docType: 'SIDE_LETTER_AGREEMENT',
                        subscriptionId:subscription.id
                    }
                });   
                
                //get side letter original file
                let sideLetterDetails = await models.SideLetter.findOne({
                    attributes:['sideLetterDocumentPath','id'],
                    where: {
                        id: sideLetterInfo.sideletterId
                    }
                })            

                let sideLetterObject = 
                {
                    fundId: sideLetterInfo.fundId,
                    subscriptionId: sideLetterInfo.subscriptionId,
                    changeCommitmentId: sideLetterInfo.changeCommitmentId,
                    filePath: sideLetterDetails.sideLetterDocumentPath,
                    sideletterId:sideLetterInfo.sideletterId,
                    docType: 'SIDE_LETTER_AGREEMENT',
                    gpSignCount: sideLetterInfo.gpSignCount,
                    isAllLpSignatoriesSigned: sideLetterInfo.isAllLpSignatoriesSigned,
                    isAllGpSignatoriesSigned: sideLetterInfo.isAllGpSignatoriesSigned,
                    lpSignCount: sideLetterInfo.lpSignCount,
                    isPrimaryGpSigned: sideLetterInfo.isPrimaryGpSigned,
                    isPrimaryLpSigned: sideLetterInfo.isPrimaryLpSigned,
                    isActive: 1,
                    createdBy: sideLetterInfo.createdBy,
                    updatedBy: sideLetterInfo.updatedBy
                }
            
                await models.DocumentsForSignature.create(sideLetterObject)                    
            }

            
            let lpSignatoriesSelected = await models.LpSignatories.findAll({
                where: {
                    fundId: fundId,
                    lpId: subscription.lp.id,
                    subscriptionId: subscription.id
                },
                include: [{
                    model: models.User,
                    as: 'signatoryDetails',
                    required: false
                }]
            })

            // set status to inprogress based on lp signatories
            let status = lpSignatoriesSelected.length>0 ? 16: 2;
            
            // if question added/updated make it true otherwise false 

            const isAdditionalQuestionAdded = (from == 'question') ? 1 : 0
            const isSubscriptionContentUpdated = (from == 'subscription') ? 1 : 0
            
            await models.FundSubscription.update({
                status: status, isSubscriptionContentUpdated: isSubscriptionContentUpdated, isAdditionalQuestionAdded: isAdditionalQuestionAdded
            }, {
                    where: {
                        id: subscription.id
                    }
            });

        }
        //--------------------------------------end

        //create empty records to send notification to lp signatory
        let data = [];         
        for (let subscriptionId of subscriptionIds) {
       

            let getSubscriptionData = await models.FundSubscription.getSubscription(subscriptionId, models);          
            const fundAgreementPath = `./assets/funds/${fundId}/${subscriptionId}/FUND_AGREEMENT_${uuidv1()}.pdf`;
            //check file exists before copy
            if(fs.existsSync(getSubscriptionData.fund.partnershipDocument.path)){
                commonHelper.ensureDirectoryExistence(fundAgreementPath)
                fs.copyFileSync(getSubscriptionData.fund.partnershipDocument.path, fundAgreementPath);           
            }

            const subscriptionAgreementTempPath = await SAPDFGeneratorBySubscriptionIDHelper(subscriptionId);const subscriptionAgreementPath = `./assets/funds/${fundId}/${subscriptionId}/SUBSCRIPTION_AGREEMENT_${uuidv1()}.pdf`;
            fs.copyFileSync(subscriptionAgreementTempPath.path, subscriptionAgreementPath);


            data.push({
                fundId: fundId,
                subscriptionId: subscriptionId,
                changeCommitmentId: null,
                filePath: subscriptionAgreementPath,
                docType: 'SUBSCRIPTION_AGREEMENT',
                gpSignCount: 0,
                lpSignCount: 0,
                isPrimaryGpSigned: 0,
                isPrimaryLpSigned: 0,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            })

            data.push({
                fundId: fundId,
                subscriptionId: subscriptionId,
                changeCommitmentId: null,
                filePath: fundAgreementPath,
                docType: 'FUND_AGREEMENT',
                gpSignCount: 0,
                lpSignCount: 0,
                isPrimaryGpSigned: 0,
                isPrimaryLpSigned: false,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            })            
            
        }
        
        await models.DocumentsForSignature.bulkCreate(data)
        //-----end        

     
        
        


    }

 
   
    return subscriptions;
}


const sendSubscriptionChangeAlertToFundInvestors = async (subscriptions, fund, req) => {

    //send notification to LP
    for (let subscription of subscriptions) {

        const alertDataToLPandLPSignatories = {
            htmlMessage: `[fundManagerCommonName] has changed your Subscription Agreement with respect to your proposed investment in [fundCommonName]. This will require you to electronically re-execute the Subscription Agreement. You will not have to re-enter other data, though you will have a chance to review it prior to electronically re-executing. Please return to the Vanilla application to electronically re-execute your Subscription Agreement`,
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId
        };


        const alertDatatoLP = {
            htmlMessage: fund.fundManagerCommonName+' has changed your Subscription Agreement with respect to your proposed investment in '+fund.fundCommonName+'. This will require you to electronically re-execute the Subscription Agreement. You will not have to re-enter other data, though you will have a chance to review it prior to electronically re-executing. Please return to the Vanilla application to electronically re-execute your Subscription Agreement. If you have assigned a Secondary Signatory(s) to your Fund Subscription, they will need to review and sign the Subscription Agreement prior to your review and signature.',
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId
        };

        //send notfication to LP Signatories
        let lpSignatoriesSelected = await models.LpSignatories.findAll({
            where: {
                fundId: fund.id,
                lpId: subscription.lp.id,
                subscriptionId: subscription.id
            },
            include: [{
                model: models.User,
                as: 'signatoryDetails',
                required: false
            }]
        })

        let lpSignatories = [];
        lpSignatoriesSelected.map(i => {
            lpSignatories.push({
                id: i.signatoryDetails.id,
                firstName: i.signatoryDetails.firstName,
                lastName: i.signatoryDetails.lastName,
                middleName: i.signatoryDetails.middleName,
                email: i.signatoryDetails.email,
                isEmailNotification: i.signatoryDetails.isEmailNotification,
                accountId: i.signatoryDetails.accountId
            })
        })    

        notificationHelper.triggerNotification(req, req.userId, false, lpSignatories, alertDataToLPandLPSignatories,null,null);

       // send email
       for (let signatory of lpSignatories) {        

        let data = {
            firstName: signatory.firstName,
            lastName: signatory.lastName,
            middleName: signatory.middleName,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.legalEntity,
            name: commonHelper.getFullName(signatory),
            loginLink: config.get('clientURL')
        }        
       
        emailHelper.triggerAlertEmailNotification(signatory.email, 'Subscription Agreement Changed By GP', data, 'subscriptionAgreementChangedAlertToLP.html');
       
        }            
        //---end


        //send notification to LP start
        let htmlFileName = 'subscriptionAgreementChangedAlertToLP.html'
        if(lpSignatories.length==0){
            notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDataToLPandLPSignatories,subscription.lp.accountId, null)
        } else {
            notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDatatoLP,subscription.lp.accountId, null) 
            htmlFileName = 'subscriptionAgreementChangedAlertToLPSignatory.html'           
        }

        emailHelper.sendEmail({
            toAddress: subscription.lp.email,
            subject: 'Subscription Agreement Changed By Fund manager',
            data: {
                firstName: subscription.lp.firstName,
                lastName: subscription.lp.lastName,
                middleName: subscription.lp.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                name: commonHelper.getFullName(subscription.lp),
                loginLink: config.get('clientURL'),
                user:subscription.lp
            },
            htmlPath: htmlFileName
        });

        //---end

    }
}

const additionalQuestionsModifiedAlertToLps = async (subscriptions, fundId, req) => {

    const fund = await models.Fund.findOne({
        where: {
            fundId: fundId
        }
    })

    for (let subscription of subscriptions) {
        const alertData = {
            htmlMessage: `In respect of your investment in [FundCommonName], the Subscription Agreement has been modified since the time you executed it to include changes. In order to subscribe to the Fund, you will need to [approve] these changes.`,
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId,
            type:'FUND_APPROVE'
        };

        notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertData,subscription.lp.accountId, null);

    }
}

module.exports = {
    run,
    sendSubscriptionChangeAlertToFundInvestors,
    additionalQuestionsModifiedAlertToLps
}