const models = require('../../models/index');
const _ = require('lodash');
const commonHelper = require('../../helpers/commonHelper');
const emailHelper = require('../../helpers/emailHelper');
const notificationHelper = require('../../helpers/notificationHelper');
const SAPDFGeneratorBySubscriptionIDHelper = require("../../helpers/SAPDFGeneratorBySubscriptionIDHelper");
const uuidv1 = require('uuid/v1');
const { Op } = require('sequelize');
const config = require('../../config/index');
const fs = require('fs');

module.exports = async (subscription, req) => {

    //if subscription status is closed ready, then only run below process. closed ready means = 7, 
    if(subscription.isOfflineInvestor){
        return;
    }
    const lpsignCount = await models.DocumentsForSignature.findOne({
        where:{
            fundId: subscription.fundId,
            subscriptionId: subscription.id,
            isActive:1,
            lpSignCount:{
                [Op.gt]:0
            }
        }
    })

    if(subscription.status !== 7 || !lpsignCount) {
        return;
    }

    //update old docs
    let updateObject = { 
            isActive: 0
    }


    // remove old subscription data
    await models.SignatureTrack.update(updateObject, {
        where: {
            documentType: 'SUBSCRIPTION_AGREEMENT',
            subscriptionId: subscription.id
        }
    });    
    await models.SignatureTrack.update(updateObject, {
        where: {
            documentType: 'FUND_AGREEMENT',
            subscriptionId: subscription.id
        }
    });   
      

    // remove old subscription data
    await models.DocumentsForSignature.update(updateObject, {
        where: {
            fundId: subscription.fundId,
            docType: 'SUBSCRIPTION_AGREEMENT',
            subscriptionId: subscription.id
        }
    });
    await models.DocumentsForSignature.update(updateObject, {
        where: {
            fundId: subscription.fundId,
            docType: 'FUND_AGREEMENT',
            subscriptionId: subscription.id
        }
    });
 
        //get side letter info
        let sideLetterInfo = await models.DocumentsForSignature.findOne({
            where: {
                isActive: 1,
                docType: 'SIDE_LETTER_AGREEMENT',
                subscriptionId: subscription.id
                
            },
            order: [['id', 'DESC']]
        })    

        //if user has side letter then update signaturetrack,documentforsignature to inactive in both tables and create the new record
        if(sideLetterInfo){            

            await models.SignatureTrack.update(updateObject, {
                where: {
                    documentType: 'SIDE_LETTER_AGREEMENT',
                    subscriptionId: subscription.id
                }
            });       

            await models.DocumentsForSignature.update(updateObject, {
                where: {
                    fundId: subscription.fundId,
                    docType: 'SIDE_LETTER_AGREEMENT',
                    subscriptionId: subscription.id
                }
            });   
            
            //get side letter original file
            let sideLetterDetails = await models.SideLetter.findOne({
                attributes:['sideLetterDocumentPath','id'],
                where: {
                    id: sideLetterInfo.sideletterId
                }
            })            

            let sideLetterObject = 
            {
                fundId: sideLetterInfo.fundId,
                subscriptionId: sideLetterInfo.subscriptionId,
                changeCommitmentId: sideLetterInfo.changeCommitmentId,
                filePath: sideLetterDetails.sideLetterDocumentPath,
                sideletterId:sideLetterInfo.sideletterId,
                docType: 'SIDE_LETTER_AGREEMENT',
                gpSignCount: sideLetterInfo.gpSignCount,
                isAllLpSignatoriesSigned: sideLetterInfo.isAllLpSignatoriesSigned,
                isAllGpSignatoriesSigned: sideLetterInfo.isAllGpSignatoriesSigned,
                lpSignCount: sideLetterInfo.lpSignCount,
                isPrimaryGpSigned: sideLetterInfo.isPrimaryGpSigned,
                isPrimaryLpSigned: sideLetterInfo.isPrimaryLpSigned,
                isActive: 1,
                createdBy: sideLetterInfo.createdBy,
                updatedBy: sideLetterInfo.updatedBy
            }
        
            await models.DocumentsForSignature.create(sideLetterObject)
            
        }
        //--------------------------------------end

        //create empty records to send notification to lp signatory
        let data = [];
    
            

            //get all subscription info for fund agreement
    let getSubscriptionData = await models.FundSubscription.getSubscription(subscription.id, models);          
            const fundAgreementPath = `./assets/funds/${subscription.fundId}/${subscription.id}/FUND_AGREEMENT_${uuidv1()}.pdf`;
            fs.copyFileSync(getSubscriptionData.fund.partnershipDocument.path, fundAgreementPath);

            const subscriptionAgreementTempPath = await SAPDFGeneratorBySubscriptionIDHelper(subscription.id);
            const subscriptionAgreementPath = `./assets/funds/${subscription.fundId}/${subscription.id}/SUBSCRIPTION_AGREEMENT_${uuidv1()}.pdf`;
            fs.copyFileSync(subscriptionAgreementTempPath.path, subscriptionAgreementPath);

            data.push({
                fundId: subscription.fundId,
                subscriptionId: subscription.id,
                changeCommitmentId: null,
                filePath: subscriptionAgreementPath,
                docType: 'SUBSCRIPTION_AGREEMENT',
                gpSignCount: 0,
                lpSignCount: 0,
                isPrimaryGpSigned: 0,
                isPrimaryLpSigned: 0,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            })

            data.push({
                fundId: subscription.fundId,
                subscriptionId: subscription.id,
                changeCommitmentId: null,
                filePath: fundAgreementPath,
                docType: 'FUND_AGREEMENT',
                gpSignCount: 0,
                lpSignCount: 0,
                isPrimaryGpSigned: 0,
                isPrimaryLpSigned: false,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            })            
            
            await models.DocumentsForSignature.bulkCreate(data)


    // change status to in  progress (after invitation accept)
    await models.FundSubscription.update({ status: 2, isSubscriptionContentUpdated: true }, {
        where: {
            id: subscription.id
        }
    }); 
 

    const lp = await models.User.findOne({
        where: {
            id: subscription.lpId
        }
    });

    const fund = await models.Fund.findOne({
        where: {
            id: subscription.fundId
        }
    });
    sendSubscriptionChangeAlertToFundInvestor(lp, fund, subscription, req);
    return;
}


const sendSubscriptionChangeAlertToFundInvestor = async (lp, fund, subscription, req) => {

    const alertDataToLPandLPSignatories = {
        htmlMessage: `[fundManagerCommonName] has changed your Subscription Agreement with respect to your proposed investment in [fundCommonName]. This will require you to electronically re-execute the Subscription Agreement. You will not have to re-enter other data, though you will have a chance to review it prior to electronically re-executing. Please return to the Vanilla application to electronically re-execute your Subscription Agreement`,
        firstName: lp.firstName,
        lastName: lp.lastName,
        middleName: lp.middleName,
        lpId: lp.id,
        subscriptionId: subscription.id,
        fundManagerCommonName: fund.fundManagerCommonName,
        fundCommonName: fund.fundCommonName,
        fundId: fund.id,
        sentBy: req.userId
    };


    const alertDatatoLP = {
        htmlMessage: fund.fundManagerCommonName+' has changed your Subscription Agreement with respect to your proposed investment in '+fund.fundCommonName+'. This will require you to electronically re-execute the Subscription Agreement. You will not have to re-enter other data, though you will have a chance to review it prior to electronically re-executing. Please return to the Vanilla application to electronically re-execute your Subscription Agreement. If you have assigned a Secondary Signatory(s) to your Fund Subscription, they will need to review and sign the Subscription Agreement prior to your review and signature.',
        firstName: lp.firstName,
        lastName: lp.lastName,
        middleName: lp.middleName,
        lpId: lp.id,
        subscriptionId: subscription.id,
        fundManagerCommonName: fund.fundManagerCommonName,
        fundCommonName: fund.fundCommonName,
        fundId: fund.id,
        sentBy: req.userId
    } 


        //send notfication to LP Signatories
        let lpSignatoriesSelected = await models.LpSignatories.findAll({
            where: {
                fundId: fund.id,
                lpId: lp.id,
                subscriptionId: subscription.id
            },
            include: [{
                model: models.User,
                as: 'signatoryDetails',
                required: false
            }]
        })

        let lpSignatories = [];
        lpSignatoriesSelected.map(i => {
            lpSignatories.push({
                id: i.signatoryDetails.id,
                firstName: i.signatoryDetails.firstName,
                lastName: i.signatoryDetails.lastName,
                middleName: i.signatoryDetails.middleName,
                email: i.signatoryDetails.email,
                isEmailNotification: i.signatoryDetails.isEmailNotification,
                accountType : i.signatoryDetails.accountType,
                accountId: i.signatoryDetails.accountId
            })
        })    

        notificationHelper.triggerNotification(req, req.userId, false, lpSignatories, alertDataToLPandLPSignatories, null, null);

       // send email
       for (let signatory of lpSignatories) {        

            let data = {
                firstName: signatory.firstName,
                lastName: signatory.lastName,
                middleName: signatory.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.legalEntity,
                name: commonHelper.getFullName(signatory),
                loginLink: config.get('clientURL'),
                user:signatory
            }        
        
            emailHelper.triggerAlertEmailNotification(signatory.email, 'Subscription Agreement Changed By GP', data, 'subscriptionAgreementChangedAlertToLP.html');
       
        }            
        //---end

        //send notification to LP start
        let htmlFileName = 'subscriptionAgreementChangedAlertToLP.html'
        if(lpSignatories.length==0){
            notificationHelper.triggerNotification(req, req.userId, lp.id, false, alertDataToLPandLPSignatories,lp.accountId, null)
        } else {
            notificationHelper.triggerNotification(req, req.userId, lp.id, false, alertDatatoLP,lp.accountId, null) 
            htmlFileName = 'subscriptionAgreementChangedAlertToLPSignatory.html'           
        }

        emailHelper.sendEmail({
            toAddress: subscription.lp.email,
            subject: 'Subscription Agreement Changed By Fund manager',
            data: {
                firstName: lp.firstName,
                lastName: lp.lastName,
                middleName: lp.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                name: commonHelper.getFullName(lp),
                loginLink: config.get('clientURL'),
                user:subscription.lp
            },
            htmlPath: htmlFileName
        });        
   

}