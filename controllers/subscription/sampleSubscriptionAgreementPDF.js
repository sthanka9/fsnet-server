
const models = require('../../models/index');
const _ = require('lodash')
const SAPDFGeneratorBySubscriptionIDHelper = require("../../helpers/SAPDFGeneratorBySubscriptionIDHelper");
const SAPDFGeneratorHelper = require("../../helpers/SAPDFGeneratorHelper");

module.exports.generate = async (req, res, next) => {

    try {

        const { subscriptionId, fundId } = req.params;

        if(Number(subscriptionId)){
            const tempSubscription = await SAPDFGeneratorBySubscriptionIDHelper(subscriptionId);
            return res.sendFile(tempSubscription.path);

        } else {
            const fund = await models.Fund.findOne({
                where:{
                    id:fundId
                }
            })
            let subscriptionStepsData = fund.subscriptionAgreementPath 

            const data = {
                fundManagerTitle: '',
                gpName: '',
                fundManagerLegalEntityName: '',
                fundLegalEntityName: '',
                legalTitle: '',
                fundLegalEntity: '',
                areYouSubscribingAsJointIndividual: '',
                fundTopLegalName: 'FUND LEGAL ENTITY NAME',
                investorType: '',
                investorSubType: '',
                noOfGrantors: '',
                entityInvestor: '',
                lpName: '',
                entityTitle: '',
                legalTitleDesignation: '',
                fundManagerCommonName: '',
                fundManagerInfo: '',
                otherInvestorSubType: '',
                otherInvestorAttributes: '',
                lpCapitalCommitment: '',
                isSubjectToDisqualifyingEvent: '',
                releaseInvestmentEntityRequired: '',
                benefitPlanInvestor: '',
                planAsDefinedInSection4975e1: '',
                employeeBenefitPlan: '',
                numberOfDirectEquityOwners:'',
                beneficialInvestmentMadeByTheEntity: '',
                partnershipWillNotConstituteMoreThanFortyPercent: '',
                anyOtherInvestorInTheFund: '',
                entityProposingAcquiringInvestment: '',
                entityHasMadeInvestmentsPriorToThedate: '',
                companiesAct: '',
                areYouQualifiedClient: '',
                areYouQualifiedPurchaser: '',
                areYouAccreditedInvestor: '',
                totalValueOfEquityInterests: '',
                investorQuestions: '',
                typeOfLegalOwnership:'',
        
            }; 

            let tempSubscription = await SAPDFGeneratorHelper(subscriptionStepsData, data) 

            return res.sendFile(tempSubscription.path);

        }
 
  

    } catch (error) {
        next(error);
    }
}