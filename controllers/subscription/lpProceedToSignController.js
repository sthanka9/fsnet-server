const models = require('../../models/index');
const _ = require('lodash')
const config = require('../../config/index');
const path = require('path');
const uuidv1 = require('uuid/v1');
const { Op, literal } = require('sequelize');
const bcrypt = require('bcryptjs');
const fs = require('fs');
const moment = require('moment');
const messageHelper = require('../../helpers/messageHelper');
const commonHelper = require('../../helpers/commonHelper');
const emailHelper = require('../../helpers/emailHelper');
const errorHelper = require('../../helpers/errorHelper');
const logger = require('../../helpers/logger');
const eventHelper = require('../../helpers/eventHelper');
const swapFundAmendmentsHelper = require('../../helpers/swapFundAmendmentsHelper');
const closeFundValidationRules = require('../../helpers/closeFundValidationRules');
const SAPDFGeneratorBySubscriptionIDHelper = require('../../helpers/SAPDFGeneratorBySubscriptionIDHelper');
const notificationHelper = require('../../helpers/notificationHelper');

const generateSubscriptionAndFundAgreement = async (req, res, next) => {

    const { subscriptionId } = req.body;

    let subscription = await models.FundSubscription.getSubscription(subscriptionId, models);

    const subscriptionAgreement = `./assets/funds/${subscription.fund.id}/${subscriptionId}/SUBSCRIPTION_AGREEMENT_${uuidv1()}.pdf`;
    await SAPDFGeneratorBySubscriptionIDHelper(subscription, subscriptionAgreement);

    //get side letter info
    let sideLetterInfo = await models.DocumentsForSignature.findOne({
        where: {
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            docType: 'SIDE_LETTER_AGREEMENT'
        }
    })      

    await models.SignatureTrack.update({
        isActive: 0
    }, {
            where: {
                subscriptionId: subscription.id,
            }
        });

    await models.DocumentsForSignature.update({
        isActive: 0
    }, {
            where: {
                fundId: subscription.fund.id,
                subscriptionId: subscription.id,
            }
        });

    const lpFundAgreement = `./assets/funds/${subscription.fund.id}/${subscription.id}/FUND_AGREEMENT_${uuidv1()}.pdf`;
    commonHelper.ensureDirectoryExistence(lpFundAgreement);

    const partnershipDocumentPageCount = await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(subscription.fund.partnershipDocument.path);

    fs.copyFileSync(subscription.fund.partnershipDocument.path, lpFundAgreement);

    const documentData = [{
        fundId: subscription.fund.id,
        subscriptionId: subscription.id,
        changeCommitmentId: null,
        filePath: lpFundAgreement,
        originalPartnershipDocumentPageCount: partnershipDocumentPageCount,
        docType: 'FUND_AGREEMENT', 
        gpSignCount: 0,
        isAllLpSignatoriesSigned: false,
        isAllGpSignatoriesSigned: false,
        lpSignCount: 0,
        isPrimaryGpSigned: 0,
        isPrimaryLpSigned: false,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    }, {
        fundId: subscription.fund.id,
        subscriptionId: subscription.id,
        changeCommitmentId: null,
        filePath: subscriptionAgreement,
        docType: 'SUBSCRIPTION_AGREEMENT',
        gpSignCount: 0,
        lpSignCount: 0,
        isPrimaryGpSigned: 0,
        isPrimaryLpSigned: false,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    }];

    await models.DocumentsForSignature.bulkCreate(documentData);

    //if subscription has side letter then create
    if (sideLetterInfo) {

        //update this record as deleted as we are creating new record AND not display in the side letters for gp
        await models.DocumentsForSignature.update(
            {
                deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
            },
            {
                where:{sideletterId:sideLetterInfo.sideletterId}
        })        
 
        //get side letter original file
        let sideLetterDetails = await models.SideLetter.findOne({
            attributes: ['sideLetterDocumentPath', 'id'],
            where: {
                id: sideLetterInfo.sideletterId
            }
        }) 

        let sideletterobj =
        {
            fundId: subscription.fund.id,
            subscriptionId: subscription.id,
            changeCommitmentId: null,
            filePath: sideLetterDetails.sideLetterDocumentPath,
            sideletterId: sideLetterInfo.sideletterId,
            docType: 'SIDE_LETTER_AGREEMENT',
            gpSignCount: 0,
            isAllLpSignatoriesSigned: false,
            isAllGpSignatoriesSigned: false,
            lpSignCount: 0,
            isPrimaryGpSigned: 0,
            isPrimaryLpSigned: false,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        }

        await models.DocumentsForSignature.create(sideletterobj);
    }

    //ammendement docs create
    let getAmmendments = await models.FundAmmendment.findAll({
        attributes:['id','document'],
        where:{
            fundId:subscription.fund.id,
            deletedAt:null,
            isActive:1,
            isAmmendment:true,
            isAffectuated:true
        }
    })   

    for (let documentsForSignature of getAmmendments) {

        let documentInfo = documentsForSignature.document

        let ammendmentAgreementPath = `./assets/funds/${subscription.fund.id}/${subscription.id}/AMENDMENT_AGREEMENT_${uuidv1()}${documentsForSignature.id}.pdf`;

        fs.copyFileSync(documentInfo.path, ammendmentAgreementPath) 

        let data = {
            fundId: subscription.fund.id,
            subscriptionId: subscription.id,
            amendmentId: documentsForSignature.id,
            filePath: ammendmentAgreementPath,
            docType: 'AMENDMENT_AGREEMENT',
            gpSignCount: 0,
            isAllLpSignatoriesSigned: false,
            isAllGpSignatoriesSigned: false,
            lpSignCount: 0,
            isPrimaryGpSigned: 0,
            isPrimaryLpSigned: false,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        } 

        await models.DocumentsForSignature.create(data)
    }      
    
    //----end of ammendments

    await models.FundSubscription.update({
        status: 16, updatedBy: req.userId
    }, {
            where: {
                id: subscriptionId
            }
        });

    // notify lp signatories by email
    await notifySignatories(subscription.fund.id, subscription.id, subscription.fund.legalEntity);

    return res.json({
        error: false,
        code: 'DOCUMENTS_SENT_FOR_SIGNATURES'
    });


}

const verifySignaturePassword = async (req, res, next) => {

    try {

        const { signaturePassword } = req.body;


        if (!signaturePassword) {
            return errorHelper.error_400(res, 'fundId', 'signaturePassword missing in the post body.');
        }

        const userId = req.userId;


        const user = await models.User.findOne({
            where: {
                id: userId
            }
        });

        // user not valid
        if (!user) {
            return errorHelper.error_400(res, 'userId', messageHelper.userNotFound);
        }

        let accountId = user.accountId

        const account = await models.Account.findOne({
            where: {
                id: accountId
            }
        });

        if (!account) {
            return errorHelper.error_400(res, 'userId', messageHelper.userNotFound);
        }

        // password is invalid. Check signature password with user password
        if (!bcrypt.compareSync(signaturePassword, account.password)) {
            return errorHelper.error_400(res, 'signaturePassword', messageHelper.old_password_not_match);
        } else {
            return next();
        }

    } catch (error) {
        next(error);
    }
}


const getCurrentLinks = async (req, res, next) => {

    const subscriptionId = req.params.subscriptionId   
    
    let isSADisplay = true
    let isFADisplay = true  
    let isFullyRestatedFADisplay = false 

    let subscription = await models.FundSubscription.getSubscription(subscriptionId, models)

    let investorStatus = subscription.status

    if (!subscription) {
        return errorHelper.error_400(res, 'subscriptionId', messageHelper.subscriptionIdReq)
    } 
    
    //get subscription agreement
    let subscriptionAgreement = await models.DocumentsForSignature.findOne({
        where: {
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            isPrimaryLpSigned:1,
            docType: {
                [Op.in]: ['SUBSCRIPTION_AGREEMENT']
            }
        }
    })  
    
    if(subscriptionAgreement)
        isSADisplay = false

    //get fund agreement
    let fundAgreement = await models.DocumentsForSignature.findOne({
        attributes:['id'],
        where: {
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            isPrimaryLpSigned:1,
            docType: {
                [Op.in]: ['FUND_AGREEMENT']
            }
        }
    })  


    //get fund agreement
    let fullyreStatedFundAgreement = await models.DocumentsForSignature.findOne({
        attributes:['id'],
        where: {
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            isPrimaryLpSigned:0,
            docType: 'FUND_AGREEMENT'
        }
    })      
    
    if(fundAgreement)
        isFADisplay = false   
        
    //check amendment
    if(fullyreStatedFundAgreement)
        isFullyRestatedFADisplay = true  
         

    let documentsForSignature = await models.DocumentsForSignature.findAll({
        where: {
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            isPrimaryLpSigned:0,
            docType: {
                [Op.in]: ['SIDE_LETTER_AGREEMENT','CHANGE_COMMITMENT_AGREEMENT','AMENDMENT_AGREEMENT']
            }
        }
    })
 
    let sideLetterAgreement = _.find(documentsForSignature, {
        docType: 'SIDE_LETTER_AGREEMENT'
    }, false)  

    let amendmentAgreement = _.find(documentsForSignature, {
        docType: 'AMENDMENT_AGREEMENT'
    }, false)  
 
    /*
    let cpAgreement = await models.ChangedPage.findAll({
        attributes:['documentPath'],
        where: {             
            fundId: subscription.fund.id,
            isActive: 1
        }
    })
    */
    
    let ammendments = []
  
    //for inprogress/invited investors only
    //get subscription agreement
    let signatureTrack = false   

    if(subscriptionAgreement){
        signatureTrack = await models.SignatureTrack.findOne({
            attributes:['id'],
            where: {
                documentId: subscriptionAgreement.id,
                signedUserId: req.userId,
                isActive: 1
            }
        }) 
    }
    
    if(amendmentAgreement){ 
        let ammendmentUrl = `${config.get('serverURL')}/api/v1/document/view/ammendment/${subscriptionId}/${amendmentAgreement.amendmentId}` 
        ammendments.push(ammendmentUrl)

    } 
    
    if(investorStatus==2 || investorStatus==16 && !signatureTrack){
        //get list of affectuated ammendments
        let getAmmendments = await models.FundAmmendment.findAll({
            where:{
                fundId:subscription.fund.id,
                deletedAt:null,
                isActive:1,
                isAmmendment:true,
                isAffectuated:true
            }
        })
         
             
        if(getAmmendments){        
            for (let ammendmentAgreement of getAmmendments) {

                let alreadySignedCount = await models.DocumentsForSignature.count({
                    where: {
                        fundId: subscription.fund.id,
                        subscriptionId: subscription.id,      
                        isPrimaryLpSigned:1,
                        isActive: 1,
                        amendmentId:ammendmentAgreement.id,    
                    }
                })   

                if(alreadySignedCount==0){
                    let ammendmentUrl = `${config.get('serverURL')}/api/v1/document/view/ammendment/${subscriptionId}/${ammendmentAgreement.id}` 
                    ammendments.push(ammendmentUrl)
                }
            }
        }
    }
    ammendments = _.uniq(ammendments) 

    return res.json({

        subscriptionAgreementUrl: isSADisplay ?  `${config.get('serverURL')}/api/v1/document/view/sa/${subscription.id}` : '',
        fundAgreementUrl: isFADisplay ? `${config.get('serverURL')}/api/v1/document/view/fa/${subscription.id}` :  isFullyRestatedFADisplay ? `${config.get('serverURL')}/api/v1/document/view/docs/${fullyreStatedFundAgreement.id}` :   '',
        sideLetterUrl: sideLetterAgreement ? `${config.get('serverURL')}/api/v1/document/view/sia/${subscription.id}` : '',
        changedPageUrl: subscription.changedPagesId>0 && subscription.changedPagesId!=null  && investorStatus!=10 ? `${config.get('serverURL')}/api/v1/document/view/viewcp/${subscription.changedPagesId}` : '',      
        ammendmentUrl:ammendments,


        subscription: {
            dynamicTitle: subscription.legalTitleDesignationPrettyForPDF,
            investorType: subscription.investorType,
            lp: {
                firstName: subscription.lp.firstName,
                lastName: subscription.lp.lastName,
                middleName: subscription.lp.middleName,
            },
            subscriptionAgreementName: '',
        }
    });

}

// const _getOnlyLinks = async (subscription, req, res, next) => {

//     let sideLetterDocs = await models.DocumentsForSignature.findOne({
//         attributes: ['id'],
//         where: {
//             subscriptionId: subscription.id,
//             fundId: subscription.fund.id,
//             isActive: 1,
//             docType: {
//                 [Op.in]: ['SIDE_LETTER_AGREEMENT']
//             }
//         }
//     });
//     let sideLetterUrl;
//     if (sideLetterDocs) {
//         sideLetterUrl = `${config.get('serverURL')}/api/v1/document/view/sia/${subscription.id}`;
//     }

//     return res.json({
//         subscriptionAgreementUrl: `${config.get('serverURL')}/api/v1/document/view/sa/${subscription.id}`,
//         fundAgreementUrl: `${config.get('serverURL')}/api/v1/document/view/fa/${subscription.id}`,
//         sideLetterUrl: sideLetterUrl,
//         subscription: {
//             subscriptionAgreementName: '',
//             dynamicTitle: subscription.legalTitleDesignationPrettyForPDF,
//             investorType: subscription.investorType,
//             lp: {
//                 firstName: subscription.lp.firstName,
//                 lastName: subscription.lp.lastName,
//                 middleName: subscription.lp.middleName,
//             }
//         }
//     });
// }



const signaturePicSave = (imgdata) => {

    // to create some random id or name for your image name
    const imgname = Date.now() + "_" + Math.random().toString(26).slice(2)

    // to declare some path to store your converted image
    const path = './assets/signatures/' + imgname + '.png'

    // to convert base64 format into random filename
    const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');

    fs.writeFileSync(path, base64Data, { encoding: 'base64' });

    return path;

}

const signatorySignOnSubscriptionAndPartnershipAgreement = async (req, res, next,isSASigned) => {
    const { subscriptionId, signaturePic, date, timeZone } = req.body;
    const subscription = await models.FundSubscription.findOne({
        where: {
            id: subscriptionId
        },
        include: [{
            model: models.Fund,
            as: 'fund',
            required: true,
            include: [{
                model: models.User,
                as: 'gp',
                required: true
            }],
        }, {
            model: models.User,
            as: 'lp',
            required: true
        }]
    })

    const lpSignaturePic = await signaturePicSave(signaturePic);

    const documentsForSignature = await models.DocumentsForSignature.findAll({
        where: {
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            isArchived:0,
            docType: {
                [Op.in]: ['SUBSCRIPTION_AGREEMENT', 'FUND_AGREEMENT', 'SIDE_LETTER_AGREEMENT']
            }
        }
    });

    const SUBSCRIPTION_AGREEMENT = _.find(documentsForSignature, {
        docType: 'SUBSCRIPTION_AGREEMENT'
    }, false);


    const FUND_AGREEMENT = _.find(documentsForSignature, {
        docType: 'FUND_AGREEMENT'
    }, false); 

    if (!SUBSCRIPTION_AGREEMENT && !FUND_AGREEMENT) {
        return res.json({
            error: true,
            message: 'Invalid data.'
        })
    }


    //Data to be rendered in HTML
    const agreementSignData = {
        // name: subscription.titlePrettyForPDF,
        jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
        investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
        legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
        lpName: commonHelper.getFullName(req.user),
        areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
        date: moment().format('M/D/YYYY'),
        lpSignaturePic: commonHelper.getSignaturePicUrl(lpSignaturePic),
        lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
        name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
    }

    //console.log(agreementSignData, 'agreementSignData');
    //add condition - if lp signatory already signed the SA , then FA changed - do not add signatory
    let isSignatorySigned = false
    if(req.user.accountType=='SecondaryLP'){         
        const subscriptionSignatureTrack = await models.SignatureTrack.findOne({
            where: {
                documentId: SUBSCRIPTION_AGREEMENT.id,
                signedUserId: req.userId,
                isActive: 1
            }
        })
        //if signatory already signed
        if(subscriptionSignatureTrack){
            isSignatorySigned = true
        }
    }
    console.log("****************isSignatorySigned on SA*************",isSignatorySigned)
    if(!isSignatorySigned){
        const subscriptionAgreementTempPath = await subscriptionAgreementSign(subscription, SUBSCRIPTION_AGREEMENT.filePath, agreementSignData);
        fs.copyFileSync(subscriptionAgreementTempPath.path, SUBSCRIPTION_AGREEMENT.filePath);
    }

    const fundAgreementTempPath = await fundAgreementSign(subscription, FUND_AGREEMENT.filePath, agreementSignData);

    
    fs.copyFileSync(fundAgreementTempPath.path, FUND_AGREEMENT.filePath);

    const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);

    //const subscriptionUrl = commonHelper.getLocalUrl(SUBSCRIPTION_AGREEMENT.filePath);
    //const fundAgreementUrl = commonHelper.getLocalUrl(FUND_AGREEMENT.filePath);
    //req.user.accountType == 'LP' ? true : false

    let isPrimaryLpSigned = req.user.accountType == 'LP' ? true : ( isSASigned ? true : false)

    await models.DocumentsForSignature.update({
        fundId: subscription.fund.id,
        subscriptionId: subscription.id,
        changeCommitmentId: null,
        docType: 'SUBSCRIPTION_AGREEMENT',
        gpSignCount: 0,
        isAllLpSignatoriesSigned: subscription.noOfSignaturesRequired == 0 ? true : (req.user.accountType == 'LP' ? true : (SUBSCRIPTION_AGREEMENT.lpSignCount + 1 === subscription.noOfSignaturesRequired)),
        isAllGpSignatoriesSigned: false,
        lpSignCount: literal('lpSignCount + 1'),
        isPrimaryGpSigned: 0,
        isPrimaryLpSigned: isPrimaryLpSigned,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    }, {
            where: {
                id: SUBSCRIPTION_AGREEMENT.id
            }
        })


    await models.DocumentsForSignature.update({
        fundId: subscription.fund.id,
        subscriptionId: subscription.id,
        changeCommitmentId: null,
        isAllLpSignatoriesSigned: subscription.noOfSignaturesRequired == 0 ? true : (req.user.accountType == 'LP' ? true : (FUND_AGREEMENT.lpSignCount + 1 === subscription.noOfSignaturesRequired)),
        isAllGpSignatoriesSigned: false,
        docType: 'FUND_AGREEMENT',
        gpSignCount: 0,
        lpSignCount: literal('lpSignCount + 1'),
        isPrimaryGpSigned: 0,
        isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    }, {
            where: {
                id: FUND_AGREEMENT.id
            }
        });

 
    //check side letter and sign on the side letter document
    sideLetterDocumentsSign(req,res,subscription)     
    //check ammendmnets sign
    ammendmentDocumentsSign(req,res,subscription,'inprogresssignatory')

    models.SignatureTrack.bulkCreate([{
        documentType: 'FUND_AGREEMENT',
        documentId: FUND_AGREEMENT.id,
        signaturePath: lpSignaturePic,
        subscriptionId: FUND_AGREEMENT.subscriptionId,
        ipAddress: req.ipInfo,
        signedDateTime: signedDateconvertedToUserTimezone,
        timezone: req.body.timeZone,
        signedDateInGMT: req.body.date,
        signedUserId: req.user.id,
        signedByType: req.user.accountType,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    }, {
        documentType: 'SUBSCRIPTION_AGREEMENT',
        documentId: SUBSCRIPTION_AGREEMENT.id,
        signaturePath: lpSignaturePic,
        subscriptionId: SUBSCRIPTION_AGREEMENT.subscriptionId,
        ipAddress: req.ipInfo,
        signedDateTime: signedDateconvertedToUserTimezone,
        timezone: req.body.timeZone,
        signedDateInGMT: req.body.date,
        signedUserId: req.user.id,
        signedByType: req.user.accountType,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    }]);

    let statusID = 16;

    if (req.user.accountType == 'LP') {
        statusID = 7;
    }

    const subscriptionAgreement = await models.DocumentsForSignature.findOne({
        where: {
            isActive: 1,
            subscriptionId: subscriptionId,
            docType: 'SUBSCRIPTION_AGREEMENT'
        }
    });

    await models.FundSubscription.update({
        status: statusID, updatedBy: req.userId
    }, {
            where: {
                id: subscriptionId
            }
        });


    if (subscription.noOfSignaturesRequired === subscriptionAgreement.lpSignCount) {
        await notifyPrimaryLP(req,subscription.lpId, subscription.fund.legalEntity,subscription);
    }

    if (req.user.accountType == 'LP') {
        eventHelper.saveEventLogInformation('Investor Becomes Close-Ready', subscription.fundId, subscription.id, req);
        triggerNotificationOnSubscriptionSign(subscription, req);
    }
    //	Fund Closing Validation Alert with LP and LPSignatory
    closeFundValidationRules(subscription.fund);

    return res.json({ 
        urlOfSubscriptionAgreement: `${config.get('serverURL')}/api/v1/document/view/sa/${subscription.id}`,
        urlOfPartnershipAgreement: `${config.get('serverURL')}/api/v1/document/view/fa/${subscription.id}`
    });

}

const signSubscriptionAndPartnershipAgreement = async (req, res, next) => {
    try {

        const { subscriptionId, signaturePic, date, timeZone, entityTitle, trustTitle,legalTitle } = req.body;

        let newTitlesData = false;
        if (entityTitle) { newTitlesData = { entityTitle: entityTitle }; }
        if (trustTitle) { newTitlesData = { trustTitle: trustTitle }; }
        if (legalTitle) { newTitlesData = { legalTitle: legalTitle }; }

        if (newTitlesData) {
            await models.FundSubscription.update(newTitlesData, {
                where: {
                    id: subscriptionId
                }
            });
        }

        const subscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                },{
                    model: models.User,
                    as: 'gp',
                    required: true
                },{
                    model: models.InvestorQuestions,
                    as: 'investorQuestions',
                    required: false,
                    where: {
                        deletedAt: null
                    },
                    include: [{
                        model: models.InvestorAnswers,
                        as: 'answers',
                        where: {
                            subscriptionId: subscriptionId
                        },
                        required: false
                    }]
                }],
            }, {
                model: models.User,
                as: 'lp',
                required: true
            }]
        }) 
         
        const documentsForSignature = await models.DocumentsForSignature.findOne({
            where: {
                subscriptionId: subscription.id,
                fundId: subscription.fund.id,
                isActive: 1,
                docType: 'SUBSCRIPTION_AGREEMENT'
            }
        });


        const documentsForSignatureFA = await models.DocumentsForSignature.findOne({
            where: {
                subscriptionId: subscription.id,
                fundId: subscription.fund.id,
                isActive: 1,
                docType: 'FUND_AGREEMENT'
            }
        });    

        const partnershipDocumentPageCount = await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(subscription.fund.partnershipDocument.path);

        if (documentsForSignature && subscription.noOfSignaturesRequired > 0) {

            console.log("**************************Signatory Sign *********************************")

            const signatureTrack = await models.SignatureTrack.findOne({
                where: {
                    documentId: documentsForSignature.id,
                    signedUserId: req.userId,
                    isActive: 1
                }
            })

            const signatureTrackFA = await models.SignatureTrack.findOne({
                where: {
                    documentId: documentsForSignatureFA.id,
                    signedUserId: req.userId,
                    isActive: 1
                }
            })


            //check sa already signed or not
            let isSASigned = false
            if(signatureTrack){
                isSASigned = true
            }


            if (signatureTrack && signatureTrackFA) {
                return errorHelper.error_400(res, 'subscriptionId', null, {
                    code: 'YOU_ALREADY_SIGNED'
                });
            } else {
                // EXISTING DOCUMENT SIGNATURE.
                return await signatorySignOnSubscriptionAndPartnershipAgreement(req, res, next,isSASigned);
            }

        } else {
            console.log("************************** Primary Sign *********************************")

            const lpSignaturePic = await signaturePicSave(signaturePic)

            const fundAgreementTempFile = subscription.fund.partnershipDocument.path;

            const subscriptionAgreement = await SAPDFGeneratorBySubscriptionIDHelper(subscription);

            const subscriptionTempFile = subscriptionAgreement.path;

            //making date with timezone of a fund
            let date_for_sign = await commonHelper.getDate();
            date_for_sign = commonHelper.getDateTimeWithTimezone(subscription.fund.timeZone, date_for_sign, 0, 0);


            const agreementSignData = {
                jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                lpName: commonHelper.getFullName(req.user),
                date: date_for_sign,
                acceptedOn: date_for_sign,
                lpSignaturePic: commonHelper.getSignaturePicUrl(lpSignaturePic),
                lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
                name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
            }

            const subscriptionAgreementTempPath = await subscriptionAgreementSign(subscription, subscriptionTempFile, agreementSignData);

            const fundAgreementTempPath = await fundAgreementSign(subscription, fundAgreementTempFile, agreementSignData);

            const subscriptionAgreementPath = `./assets/funds/${subscription.fund.id}/${subscription.id}/SUBSCRIPTION_AGREEMENT_${uuidv1()}.pdf`;
            const fundAgreementPath = `./assets/funds/${subscription.fund.id}/${subscription.id}/FUND_AGREEMENT_${uuidv1()}.pdf`;

            commonHelper.ensureDirectoryExistence(subscriptionAgreementPath);
            fs.copyFileSync(subscriptionAgreementTempPath.path, subscriptionAgreementPath);
            fs.copyFileSync(fundAgreementTempPath.path, fundAgreementPath);

            // update/insert this information in the database
            //const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone)
            // const signedDateconvertedToUserTimezone = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
            const signedDateconvertedToUserTimezone = date_for_sign;
            await inActivateOldSubscriptions(subscription);
            saveSubscriptionSignInformationInDatabase(subscription, subscriptionAgreementPath, lpSignaturePic, signedDateconvertedToUserTimezone, req);
            saveFundAgreementSignInformationInDatabase(subscription, fundAgreementPath, lpSignaturePic, signedDateconvertedToUserTimezone,partnershipDocumentPageCount, req);
            eventHelper.saveEventLogInformation('LP signatures on Subscription Agreement', subscription.fundId, subscription.id, req);
            eventHelper.saveEventLogInformation('LP signatures on Fund Agreement', subscription.fundId, subscription.id, req);


            const subscriptionBlobTempUrl = commonHelper.getLocalUrl(subscriptionAgreementPath);
            const fundAgreementBlobTempUrl = commonHelper.getLocalUrl(fundAgreementPath);


            const subscriptionAfterSign = subscription.noOfSignaturesRequired > 0 ? { status: 16, updatedBy: req.userId } : { status: 7, updatedBy: req.userId };
            // Update the status to close-ready=7
            await models.FundSubscription.update(subscriptionAfterSign, {
                where: {
                    id: subscriptionId
                }
            });

            eventHelper.saveEventLogInformation('Investor Becomes Close-Ready', subscription.fundId, subscription.id, req);

            triggerNotificationOnSubscriptionSign(subscription, req);
            closeFundValidationRules(subscription.fund)

            //check side letter and sign on the side letter document
            sideLetterDocumentsSign(req,res,subscription) 
            //check ammendmnets sign
            ammendmentDocumentsSign(req,res,subscription,'inprogresslp')

            return res.json({
                urlOfSubscriptionAgreement: subscriptionBlobTempUrl,
                urlOfPartnershipAgreement: fundAgreementBlobTempUrl
            });
        }

    } catch (error) {
        next(error);
    }

}

async function generateSideLetterSignedPage(data, htmlFilePath) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../../assets/temp/${fileName}`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}

async function generateSignedPageforsideLetter(data, htmlFilePath) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../../assets/temp/${fileName}`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}

// generate last page sined pdf from html and append to current subscription
async function subscriptionAgreementSign(subscription, subscriptionTempFile, data) {

    const htmlFilePath = "./views/subscriptionAgreementLpSign.html";
    const signedLastPage = await generateSignedPage(data, htmlFilePath);
    const fileName = `SUBSCRIPTION_AGREEMENT_${uuidv1()}.pdf`;
    return await mergeAgreementDocAndSigndPage(fileName, subscriptionTempFile, signedLastPage);
}

// generate last page sined pdf from html and append to current fund agreement
async function ammendmentAgreementSign(subscription, fundAgreementTempFile, data) {
    const htmlFilePath = "./views/fundAgreementLpSign.html";
    const signedLastPage = await generateSignedPage(data, htmlFilePath);
    const fileName = `AMENDMENT_AGREEMENT_${uuidv1()}.pdf`;
    return await mergeAgreementDocAndSigndPage(fileName, fundAgreementTempFile, signedLastPage);
}

// generate last page sined pdf from html and append to current fund agreement
async function fundAgreementSign(subscription, fundAgreementTempFile, data) {
    const htmlFilePath = "./views/fundAgreementLpSign.html";
    const signedLastPage = await generateSignedPage(data, htmlFilePath);
    const fileName = `FUND_AGREEMENT_${uuidv1()}.pdf`;
    return await mergeAgreementDocAndSigndPage(fileName, fundAgreementTempFile, signedLastPage);
}

// generate last page sined pdf from html and append to current fund agreement
async function sideLetterAgreementSign(subscription, fundAgreementTempFile, data) {
    let original_sideletter = path.resolve(__dirname, '../../' + fundAgreementTempFile)
    const htmlFilePath = "./views/sideletterAgreementLPSign.html";
    const signedLastPage = await generateSignedPageforsideLetter(data, htmlFilePath);
    const fileName = `SIDE_LETTER_AGREEMENT_${uuidv1()}.pdf`;
    return await mergeAgreementDocAndSigndPagesideLetter(fileName, original_sideletter, signedLastPage, subscription);
}

async function mergeAgreementDocAndSigndPage(fileName, agreementTempFile, lpSignedLastPage) {
    const outputFile = path.resolve(__dirname, `../../assets/temp/${fileName}`);
    agreementTempFile = path.resolve(__dirname, "../../", agreementTempFile);
    await commonHelper.pdfMerge([agreementTempFile, lpSignedLastPage.path], outputFile);
    return { path: outputFile, name: fileName };
}

async function mergeAgreementDocAndSigndPagesideLetter(fileName, agreementTempFile, lpSignedLastPage, subscription) {
    //const outputFile = path.resolve(__dirname, `../../assets/temp/${fileName}`);
    let outputFile = `./assets/funds/${subscription.fund.id}/${subscription.id}/${fileName}`
    await commonHelper.pdfMerge([agreementTempFile, lpSignedLastPage.path], outputFile);
    return { path: outputFile, name: fileName };
}

async function generateSignedPage(data, htmlFilePath) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../../assets/temp/${fileName}`);
    // const outputFile = path.resolve(__dirname, `../../${fileName}`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}

const inActivateOldSubscriptions = async (subscription) => {
    //deactivate old siganture tracks
    await models.SignatureTrack.update({ isActive: 0 }, {
        where: {
            documentType: {
                [Op.in]: ['SUBSCRIPTION_AGREEMENT', 'FUND_AGREEMENT']
            },
            subscriptionId: subscription.id
        }
    });

    // deactivate old subscription and fund details.
    return await models.DocumentsForSignature.update({ isActive: 0 }, {
        individualHooks: true,
        where: {
            fundId: subscription.fund.id,
            docType: {
                [Op.in]: ['SUBSCRIPTION_AGREEMENT', 'FUND_AGREEMENT']
            },
            subscriptionId: subscription.id
        }
    });
}

const saveSubscriptionSignInformationInDatabase = (subscription, subscriptionAgreementPath, lpSignaturePic, signedDateconvertedToUserTimezone, req) => {

    return models.DocumentsForSignature.create({
        fundId: subscription.fund.id,
        subscriptionId: subscription.id,
        changeCommitmentId: null,
        filePath: subscriptionAgreementPath,
        docType: 'SUBSCRIPTION_AGREEMENT',
        gpSignCount: 0,
        lpSignCount: 0,
        isPrimaryGpSigned: 0,
        isAllLpSignatoriesSigned: req.user.accountType == 'LP' ? true : (subscription.noOfSignaturesRequired > 1 ? false : true),
        isAllGpSignatoriesSigned: false,
        isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    }).then((document) => {

        models.SignatureTrack.create({
            documentType: 'SUBSCRIPTION_AGREEMENT',
            documentId: document.id,
            subscriptionId: document.subscriptionId,
            signaturePath: lpSignaturePic,
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: req.body.timeZone,
            signedDateInGMT: req.body.date,
            signedUserId: req.user.id,
            signedByType: req.user.accountType,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        });

        models.DocumentsForSignature.increment('lpSignCount', { where: { id: document.id } });

    })
}

const saveFundAgreementSignInformationInDatabase = (subscription, fundAgreementPath, lpSignaturePic, signedDateconvertedToUserTimezone,partnershipDocumentPageCount, req) => {
    return models.DocumentsForSignature.create({
        fundId: subscription.fund.id,
        subscriptionId: subscription.id,
        originalPartnershipDocumentPageCount: partnershipDocumentPageCount,
        changeCommitmentId: null,
        filePath: fundAgreementPath,
        docType: 'FUND_AGREEMENT',
        gpSignCount: 0,
        lpSignCount: 0,
        isAllGpSignatoriesSigned: false,
        isPrimaryGpSigned: 0,
        isAllLpSignatoriesSigned: req.user.accountType == 'LP' ? true : (subscription.noOfSignaturesRequired > 1 ? false : true),
        isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    }).then((document) => {
        models.SignatureTrack.create({
            documentType: 'FUND_AGREEMENT',
            documentId: document.id,
            subscriptionId: document.subscriptionId,
            signaturePath: lpSignaturePic,
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: req.body.timeZone,
            signedDateInGMT: req.body.date,
            signedUserId: req.user.id,
            signedByType: req.user.accountType,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        });
        models.DocumentsForSignature.increment('lpSignCount', { where: { id: document.id } });
    })
}

const getGpDelegates = async (fund) => {
    // gp delegate 
    return models.GpDelegates.findAll({
        where: {
            fundId: fund.id,
            gpId: fund.gpId
        },
        include: [{
            model: models.User,
            as: 'details',
            required: true,
            where:{isEmailConfirmed : 1}
        }]
    });
}

//get gp signatories
const getGpSignatories = (fund) => {

    return models.GpSignatories.findAll({
        where: {
            fundId: fund.id,
            gpId: fund.gpId
        },
        include: [{
            model: models.User,
            as: 'signatoryDetails',
            required: true
        }]
    });
}

const triggerNotificationOnSubscriptionSign = (subscription, req) => {
    getGpDelegates(subscription.fund).then(gpDelegates => {
        triggerEmailOnSubscriptionAgreementSign(subscription, gpDelegates);
        triggerAlertNotifictionOnSubscriptionAgreementSignedByLP(subscription, gpDelegates, req);
    }).catch(console.log)
}

// trigger email notifictions to gp and gp delegates.
const triggerEmailOnSubscriptionAgreementSign = async (subscription, gpDelegates) => {
    let emailBodyContent = {
        name: commonHelper.getInvestorName(subscription),
        fundCommonName: subscription.fund.fundCommonName,
        capitalCommitmentOfferedAmount: subscription.lpCapitalCommitmentPretty,
        clientURL: config.get('clientURL'),
    };

    models.User.findOne({
        where: {
            id: subscription.fund.gpId
        }
    }).then(gp => {

        // console.log('trigger go gp close ready', gp);
        if (gp && subscription.fund.isNotificationsEnabled) { // if setting enabled
            emailBodyContent.fundManagerName = commonHelper.getFullName(subscription.fund.gp);
            emailBodyContent.user = gp;

            emailHelper.triggerAlertEmailNotification(gp.email, 'Investor has become close ready', emailBodyContent, 'lpBecomeCloseReady.html');
        }
    });

    for (const gpDelegate of gpDelegates) {
        if (gpDelegate.details.isEmailNotification) { // if setting enabled
            emailBodyContent.fundManagerName = commonHelper.getFullName(gpDelegate.details);
            emailHelper.triggerAlertEmailNotification(gpDelegate.details.email, 'Investor has become close ready', emailBodyContent, 'lpBecomeCloseReady.html');
        }
    }

}

const triggerAlertNotifictionOnSubscriptionAgreementSignedByLP = async (subscription, gpDelegates, req) => {

    const alertFrom = subscription.lp.id;
    const alertTo = subscription.fund.gp.id;

    // Investor has signed subscription document(LP has become close ready) - to GP, GP delegate
    const alertData = {
        // lpName: commonHelper.getFullName(req.user), //`${subscription.lp.firstName} ${subscription.lp.lastName}`,
        lpName: commonHelper.getInvestorName(subscription),
        htmlMessage: messageHelper.signedSubscriptionDocument,
        CapitalCommitmentOfferedAmount: subscription.lpCapitalCommitmentPretty,
        subscriptionId: subscription.id,
        fundId: subscription.fund.id,
        fundName: subscription.fund.fundCommonName,
        documentId: null,
        docType: 'SUBSCRIPTION_AGREEMENT',
        sentBy: req.userId
    };

    //gp accountinfo
    /*
    let getGpAccountInfo = await models.User.findOne({
        attributes:['accountId'],
        where: {
            id: alertTo
        }
    })*/

    // GP & // GP delegate
    notificationHelper.triggerNotification(req, alertFrom, alertTo, gpDelegates, alertData, subscription.fund.gp.accountId,subscription.fund.gp);

}

const capitalCommitmentDocumentsSign = async (req, res, subscription) => {

    logger.info('capitalCommitment Sign ')  
    
    const { subscriptionId, signaturePic, date, timeZone } = req.body

    const document = await models.DocumentsForSignature.findOne({
        where: {
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            docType: 'CHANGE_COMMITMENT_AGREEMENT'
        }
    }) 
    
    if(document){

        const lpSignaturePic = await signaturePicSave(signaturePic); // './assets/signatures/lp/1542781273557_83hpae8c73.png'; 


        const lpSignedDoc = await changeCommitmentAgreementSign({
            jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
            investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
            legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
            lpName: commonHelper.getFullName(req.user),
            areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
            date: moment().format('M/D/YYYY'),
            lpSignaturePic: commonHelper.getSignaturePicUrl(lpSignaturePic),
            lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
            name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
        }, document.filePath);


        const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);

        fs.copyFileSync(lpSignedDoc.path, document.filePath);

        models.DocumentsForSignature.update({
            gpSignCount: 0,
            lpSignCount: literal('lpSignCount + 1'),
            isPrimaryGpSigned: 0,
            isAllGpSignatoriesSigned: (subscription.fund.noOfSignaturesRequiredForCapitalCommitment - 1) ? false : true,
            isAllLpSignatoriesSigned: subscription.noOfSignaturesRequired == 0 ? true : (req.user.accountType == 'LP' ? true : (document.lpSignCount + 1 === subscription.noOfSignaturesRequired)),
            isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
            isActive: 1,
            updatedBy: req.userId
        }, {
                where: {
                    id: document.id,
                    docType: 'CHANGE_COMMITMENT_AGREEMENT'
                }
            }).then(() => {
                models.SignatureTrack.create({
                    documentType: 'CHANGE_COMMITMENT_AGREEMENT',
                    documentId: document.id,
                    subscriptionId: document.subscriptionId,
                    signaturePath: lpSignaturePic,
                    ipAddress: req.ipInfo,
                    signedDateTime: signedDateconvertedToUserTimezone,
                    timezone: req.body.timeZone,
                    signedDateInGMT: req.body.date,
                    signedUserId: req.user.id,
                    signedByType: req.user.accountType,
                    isActive: 1,
                    createdBy: req.userId,
                    updatedBy: req.userId
                });
            })   

        //update inactive 
        if (req.user.accountType == 'LP') {        
            await models.DocumentsForSignature.update({isActive: 0,updatedBy: req.userId}, 
            { where: { id: document.id } })        
        }
        
    
    }

}


const signChangeCommitment = async (req, res, next) => {

    const { documentId, signaturePic, date, timeZone } = req.body;

    const document = await models.DocumentsForSignature.findOne({
        where: {
            id: documentId,
            isActive: 1,
            docType: 'CHANGE_COMMITMENT_AGREEMENT'
        }
    })

    if (!document) {
        return errorHelper.error_400(res, 'documentId', messageHelper.noDataFound);
    }

    const subscription = await models.FundSubscription.findOne({
        where: {
            id: document.subscriptionId
        },
        include: [{
            model: models.Fund,
            as: 'fund',
            required: true,
            include: [{
                model: models.timezone,
                as: 'timeZone',
                required: false
            }]
        }, {
            model: models.User,
            as: 'lp',
            required: true
        }]
    })

    if (subscription.noOfSignaturesRequired && subscription.noOfSignaturesRequired > 0 && req.user.accountType === "LP" && subscription.noOfSignaturesRequired != document.lpSignCount) {

        return errorHelper.error_400(res, 'documentId', 'All Investor Signatories must sign first.', {
            CODE: 'ALL_LP_SIGNATORIES_SIGN_NEEDED'
        });

    }

    const lpSignaturePic = await signaturePicSave(signaturePic); // './assets/signatures/lp/1542781273557_83hpae8c73.png'; 


    //making date with timezone of a fund
    let date_for_sign = await commonHelper.getDate();
    date_for_sign = commonHelper.getDateTimeWithTimezone(subscription.fund.timeZone, date_for_sign, 0, 0);

    const lpSignedDoc = await changeCommitmentAgreementSign({
        jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
        investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
        legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
        lpName: commonHelper.getFullName(req.user),
        areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
        date: date_for_sign, //moment().format('M/D/YYYY'),
        lpSignaturePic: commonHelper.getSignaturePicUrl(lpSignaturePic),
        lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
        name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
    }, document.filePath);


    const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);

    fs.copyFileSync(lpSignedDoc.path, document.filePath);

    models.DocumentsForSignature.update({
        gpSignCount: 0,
        lpSignCount: literal('lpSignCount + 1'),
        isPrimaryGpSigned: 0,
        isAllGpSignatoriesSigned: (subscription.fund.noOfSignaturesRequiredForCapitalCommitment - 1) ? false : true,
        isAllLpSignatoriesSigned: subscription.noOfSignaturesRequired == 0 ? true : (req.user.accountType == 'LP' ? true : (document.lpSignCount + 1 === subscription.noOfSignaturesRequired)),
        isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
        isActive: 1,
        updatedBy: req.userId
    }, {
            where: {
                id: document.id,
                docType: 'CHANGE_COMMITMENT_AGREEMENT'
            }
        }).then(() => {
            models.SignatureTrack.create({
                documentType: 'CHANGE_COMMITMENT_AGREEMENT',
                documentId: document.id,
                subscriptionId: document.subscriptionId,
                signaturePath: lpSignaturePic,
                ipAddress: req.ipInfo,
                signedDateTime: signedDateconvertedToUserTimezone,
                timezone: req.body.timeZone,
                signedDateInGMT: req.body.date,
                signedUserId: req.user.id,
                signedByType: req.user.accountType,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            });
        })

    eventHelper.saveEventLogInformation('LP Signatures on Capital Commitment Increases', document.fundId, document.subscriptionId, req);
    const alertData = {
        htmlMessage: messageHelper.signedCapitalCommitmentIncreaseLetterAlertMessage,
        message: 'Investor has signed Capital Commitment Increase',
        lpName: await commonHelper.getInvestorName(subscription),
        subscriptionId: document.subscriptionId,
        docType: 'CHANGE_COMMITMENT_AGREEMENT',
        fundManagerCommonName: subscription.fund.fundManagerCommonName,
        fundName: subscription.fund.fundCommonName,
        fundId: subscription.fund.id,
        sentBy: req.userId,
        documentId: documentId
    };

    //if lp signs - send notifications and email to GP's
    if (req.user.accountType == 'LP') {

        let alertDatastoGPWithSignatory = {
            htmlMessage: messageHelper.signedCapitalCommitmentIncreaseLetterGPWithSignatoryAlertMessage,
            message: 'Investor has signed Capital Commitment Increase',
            lpName: await commonHelper.getInvestorName(subscription),
            subscriptionId: document.subscriptionId,
            docType: 'CHANGE_COMMITMENT_AGREEMENT',
            fundManagerCommonName: subscription.fund.fundManagerCommonName,
            fundName: subscription.fund.fundCommonName,
            fundId: subscription.fund.id,
            sentBy: req.userId,
            documentId: documentId
        }

        let alertDatastoGPWithOutSignatory = {
            htmlMessage: messageHelper.signedCapitalCommitmentIncreaseLetterGPWithOutSignatoryAlertMessage,
            message: 'Investor has signed Capital Commitment Increase',
            lpName: await commonHelper.getInvestorName(subscription),
            subscriptionId: document.subscriptionId,
            docType: 'CHANGE_COMMITMENT_AGREEMENT',
            fundManagerCommonName: subscription.fund.fundManagerCommonName,
            fundName: subscription.fund.fundCommonName,
            fundId: subscription.fund.id,
            sentBy: req.userId,
            documentId: documentId
        }

        let gp = await models.User.findOne({
            where: {
                id: subscription.fund.gpId
            }
        })

        const gpDelegates = await getGpDelegates(subscription.fund)
        const GpSignatories = await getGpSignatories(subscription.fund)

        //no of signatures for capital commitment
        let noOfSignaturesRequiredForCapitalCommitment = subscription.fund.noOfSignaturesRequiredForCapitalCommitment

        //notification and email to gp     
        let gpemailBodyContent = {
            firstName: gp.firstName,
            lastName: gp.lastName,
            middleName: gp.middleName,
            fundCommonName: subscription.fund.fundCommonName,
            name: commonHelper.getInvestorName(subscription),
            fundManagerName: commonHelper.getFullName(gp),
            clientURL: config.get('clientURL'),
            user:gp
        }
        //if more than 1 signature 
        if (noOfSignaturesRequiredForCapitalCommitment > 1 && GpSignatories.length > 0) {
            console.log("i am in gp if")
            notificationHelper.triggerNotification(req, subscription.lpId, subscription.fund.gpId, {}, alertDatastoGPWithSignatory, gp.accountId,gp);
            emailHelper.triggerAlertEmailNotification(gp.email, 'Investor has signed Capital Commitment Increase', gpemailBodyContent, 'lpSignedCapitalCommitmentWithGPSignatory.html');
        } else {
            console.log("i am in gp else")
            notificationHelper.triggerNotification(req, subscription.lpId, subscription.fund.gpId, {}, alertDatastoGPWithOutSignatory, gp.accountId,gp);
            emailHelper.triggerAlertEmailNotification(gp.email, 'Investor has signed Capital Commitment Increase', gpemailBodyContent, 'lpSignedCapitalCommitment.html');
        }

        //email notification and alerts to gp delegates             
        for (let gpDelegate of gpDelegates) {
            let gpdelegateemailBodyContent = {
                'firstName': gpDelegate.details.firstName,
                'lastName': gpDelegate.details.lastName,
                'middleName': gpDelegate.details.middleName,
                'fundCommonName': subscription.fund.fundCommonName,
                'name': commonHelper.getInvestorName(subscription),
                'fundManagerName': commonHelper.getFullName(gpDelegate.details),
            }
            notificationHelper.triggerNotification(req, subscription.lpId, gpDelegate.details.id, {}, alertData, gpDelegate.details.accountId,null)
            emailHelper.triggerAlertEmailNotification(gpDelegate.details.email, 'Investor has signed Capital Commitment Increase', gpdelegateemailBodyContent, 'lpSignedCapitalCommitment.html');
        }

        //email notification and alerts to gp signatories
        if (noOfSignaturesRequiredForCapitalCommitment > 1 && GpSignatories.length > 0) {
            for (let GpSignatory of GpSignatories) {
                let gpsignatoryemailBodyContent = {
                    'firstName': GpSignatory.signatoryDetails.firstName,
                    'lastName': GpSignatory.signatoryDetails.lastName,
                    'middleName': GpSignatory.signatoryDetails.middleName,
                    'fundCommonName': subscription.fund.fundCommonName,
                    'name': commonHelper.getInvestorName(subscription),
                    'fundManagerName': commonHelper.getFullName(GpSignatory.signatoryDetails),
                }
                notificationHelper.triggerNotification(req, subscription.lpId, GpSignatory.signatoryDetails.id, {}, alertData, GpSignatory.signatoryDetails.accountId,null);
                emailHelper.triggerAlertEmailNotification(GpSignatory.signatoryDetails.email, 'Investor has signed Capital Commitment Increase', gpsignatoryemailBodyContent, 'lpSignedCapitalCommitment.html');
            }
        }

    }

    logger.info('Send alert email to LP')

    const lp = await models.User.findOne({
        where: {
            id: subscription.lpId
        }
    })

    //send notification to LP
    if (req.user.accountType == 'SecondaryLP' &&  (document.lpSignCount + 1 === subscription.noOfSignaturesRequired) ) {


        const alertData = {
            htmlMessage: messageHelper.issuedCapitalCommitmentIncreaseLetterAlertMessage,
            message: 'has issued Capital Commitment Increase Letter',
            lpName: commonHelper.getInvestorName(subscription),
            subscriptionId: document.subscriptionId,
            docType: 'CHANGE_COMMITMENT_AGREEMENT',
            fundManagerCommonName: subscription.fund.fundManagerCommonName,
            fundName: subscription.fund.fundCommonName,
            fundId: subscription.fund.id,
            sentBy: req.userId,
            documentId: documentId
        }

        notificationHelper.triggerNotification(req, subscription.fund.gpId, subscription.lpId, {}, alertData, lp.accountId,null);

        let emailBodyContentLP = {
            'firstName': lp.firstName,
            'lastName': lp.lastName,
            'middleName': lp.middleName,
            'name': commonHelper.getInvestorName(subscription),
            'fundCommonName': subscription.fund.fundCommonName
        }              

        emailHelper.triggerAlertEmailNotification(lp.email, 'Fund Manager has issued Capital Commitment Increase Letter', emailBodyContentLP, 'gpChangeCapitalCommitment.html');
    } 
    
    return res.json({
        url: `${config.get('serverURL')}/api/v1/document/view/viewcc/${document.subscriptionId}`
    });

}

//sign the sideletter
const signChangeSideletter = async (req, res, next) => {

    const { documentId, signaturePic, date, timeZone } = req.body;

    const document = await models.DocumentsForSignature.findOne({
        where: {
            id: documentId,
            isActive: 1,
            docType: 'SIDE_LETTER_AGREEMENT'
        }
    })

    if (!document) {
        return errorHelper.error_400(res, 'documentId', messageHelper.noDataFound);
    }

    const subscription = await models.FundSubscription.findOne({
        where: {
            id: document.subscriptionId
        },
        include: [{
            model: models.Fund,
            as: 'fund',
            required: true,
            include: [{
                model: models.timezone,
                as: 'timeZone',
                required: false
            }]
        }, {
            model: models.User,
            as: 'lp',
            required: true
        }]
    })

    if (subscription.noOfSignaturesRequired && subscription.noOfSignaturesRequired > 0 && req.user.accountType === "LP" && subscription.noOfSignaturesRequired != document.lpSignCount) {

        return errorHelper.error_400(res, 'documentId', 'All Investor Signatories must sign first.', {
            CODE: 'ALL_LP_SIGNATORIES_SIGN_NEEDED'
        });

    }

    const lpSignaturePic = await signaturePicSave(signaturePic); // './assets/signatures/lp/1542781273557_83hpae8c73.png'; 

    const changeSideletterAgreementTempFile = document.filePath;

    //making date with timezone of a fund
    let date_for_sign = await commonHelper.getDate();
    date_for_sign = commonHelper.getDateTimeWithTimezone(subscription.fund.timeZone, date_for_sign, 0, 0);


    let agreementSignData = {
        jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
        investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
        legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
        areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
        lpName: commonHelper.getFullName(req.user),
        date: date_for_sign, // moment().format('M/D/YYYY'),
        lpSignaturePic: commonHelper.getSignaturePicUrl(lpSignaturePic),
        lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
        name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
    }

    const sideLetterAgreementTempPath = await sideLetterAgreementSign(subscription, changeSideletterAgreementTempFile, agreementSignData);
    let lpsigned_sideletter = sideLetterAgreementTempPath.path
    sideLetterurl = await commonHelper.getLocalUrl(lpsigned_sideletter)

    const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);

    models.DocumentsForSignature.update({
        gpSignCount: 0,
        lpSignCount: literal('lpSignCount + 1'),
        filePath: lpsigned_sideletter,
        isPrimaryGpSigned: 0,
        isAllGpSignatoriesSigned: (subscription.fund.noOfSignaturesRequiredForSideLetter - 1) ? false : true,
        isAllLpSignatoriesSigned: subscription.noOfSignaturesRequired == 0 ? true : (req.user.accountType == 'LP' ? true : (document.lpSignCount + 1 === subscription.noOfSignaturesRequired)),
        isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
        isActive: 1,
        updatedBy: req.userId
    }, {
            where: {
                id: document.id,
                docType: 'SIDE_LETTER_AGREEMENT'
            }
        }).then(() => {
            models.SignatureTrack.create({
                documentType: 'SIDE_LETTER_AGREEMENT',
                documentId: document.id,
                subscriptionId: document.subscriptionId,
                signaturePath: lpSignaturePic,
                ipAddress: req.ipInfo,
                signedDateTime: signedDateconvertedToUserTimezone,
                timezone: req.body.timeZone,
                signedDateInGMT: req.body.date,
                signedUserId: req.user.id,
                signedByType: req.user.accountType,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            });
        })

    eventHelper.saveEventLogInformation('LP Signatures on Sideletter', document.fundId, document.subscriptionId, req);

    //if lp signs only - send notification to GP
    if (req.user.accountType == 'LP') {


        //alert data to gp with gp signatory
        let alertDatastoGPWithSignatory = {
            htmlMessage: messageHelper.signedSideLetterGPWithSignatoryAlertMessage,
            message: 'Investor has signed Side Letter',
            lpName: await commonHelper.getInvestorName(subscription),
            subscriptionId: document.subscriptionId,
            docType: 'SIDE_LETTER_AGREEMENT',
            fundManagerCommonName: subscription.fund.fundManagerCommonName,
            fundName: subscription.fund.fundCommonName,
            fundId: subscription.fund.id,
            sentBy: req.userId,
            documentId: documentId
        }

        //alert data to gp with outsignatory
        let alertDatastoGPWithOutSignatory = {
            htmlMessage: messageHelper.signedSideLetterGPWithOutSignatoryAlertMessage,
            message: 'Investor has signed Side Letter',
            lpName: await commonHelper.getInvestorName(subscription),
            subscriptionId: document.subscriptionId,
            docType: 'SIDE_LETTER_AGREEMENT',
            fundManagerCommonName: subscription.fund.fundManagerCommonName,
            fundName: subscription.fund.fundCommonName,
            fundId: subscription.fund.id,
            sentBy: req.userId,
            documentId: documentId
        }


        let gp = await models.User.findOne({
            where: {
                id: subscription.fund.gpId
            }
        })

        const gpDelegates = await getGpDelegates(subscription.fund)
        const GpSignatories = await getGpSignatories(subscription.fund)

        //no of signatures for side letter
        let noOfSignaturesRequiredForSideLetter = subscription.fund.noOfSignaturesRequiredForSideLetter

        //email content to GP
        let gpemailBodyContent = {
            firstName: gp.firstName,
            lastName: gp.lastName,
            middleName: gp.middleName,
            name: commonHelper.getInvestorName(subscription),
            fundManagerName: commonHelper.getFullName(gp),
            fundCommonName: subscription.fund.fundCommonName,
            clientURL: config.get('clientURL'),
            user:gp
        }

        //if more than 1 signature 
        if (noOfSignaturesRequiredForSideLetter > 1 && GpSignatories.length > 0) {
            //notification and email to gp
            notificationHelper.triggerNotification(req, subscription.lpId, subscription.fund.gpId, {}, alertDatastoGPWithSignatory, gp.accountId,gp);
            emailHelper.triggerAlertEmailNotification(gp.email, 'Investor has signed Side Letter', gpemailBodyContent, 'lpSignedSideLetterWithGPSignatory.html');
        } else {
            //notification and email to gp
            notificationHelper.triggerNotification(req, subscription.lpId, subscription.fund.gpId, {}, alertDatastoGPWithOutSignatory, gp.accountId,gp);
            emailHelper.triggerAlertEmailNotification(gp.email, 'Investor has signed Side Letter', gpemailBodyContent, 'lpSignedSideLetter.html');
        }

        //email notification and alerts to gp delegates     
        for (let gpDelegate of gpDelegates) {
            let gpdelegateemailBodyContent = {
                'firstName': gpDelegate.details.firstName,
                'lastName': gpDelegate.details.lastName,
                'middleName': gpDelegate.details.middleName,
                'name':  commonHelper.getInvestorName(subscription),
                'fundManagerName': commonHelper.getFullName(gpDelegate.details),
                'fundCommonName': subscription.fund.fundCommonName,
                'clientURL': config.get('clientURL')
            }
            notificationHelper.triggerNotification(req, subscription.lpId, gpDelegate.details.id, {}, alertDatastoGPWithOutSignatory, gpDelegate.details.accountId,null);
            emailHelper.triggerAlertEmailNotification(gpDelegate.details.email, 'Investor has signed Side Letter', gpdelegateemailBodyContent, 'lpSignedSideLetter.html');
        }

        //email notification and alerts to gp signatories         
        //if more than 1 signature 
        if (noOfSignaturesRequiredForSideLetter > 1 && GpSignatories.length > 0) {

            let alertDatastoGPSignatorys = {
                htmlMessage: messageHelper.signedSideLetterGPSignatoryAlertMessage,
                message: 'Investor has signed Side Letter',
                lpName: await commonHelper.getInvestorName(subscription),
                subscriptionId: document.subscriptionId,
                docType: 'SIDE_LETTER_AGREEMENT',
                fundManagerCommonName: subscription.fund.fundManagerCommonName,
                fundName: subscription.fund.fundCommonName,
                fundId: subscription.fund.id,
                sentBy: req.userId,
                documentId: documentId
            }

            for (let GpSignatory of GpSignatories) {
                let gpsignatoryemailBodyContent = {
                    'firstName': GpSignatory.signatoryDetails.firstName,
                    'lastName': GpSignatory.signatoryDetails.lastName,
                    'middleName': GpSignatory.signatoryDetails.middleName,
                    'fundCommonName': subscription.fund.fundCommonName,
                    'name': commonHelper.getInvestorName(subscription),
                    'fundManagerName': commonHelper.getFullName(GpSignatory.signatoryDetails),
                    'clientURL': config.get('clientURL')
                }

                notificationHelper.triggerNotification(req, subscription.lpId, GpSignatory.signatoryDetails.id, {}, alertDatastoGPSignatorys, GpSignatory.signatoryDetails.accountId,null);

                emailHelper.triggerAlertEmailNotification(GpSignatory.signatoryDetails.email, 'Investor has signed Side Letter', gpsignatoryemailBodyContent, 'lpSignedSideLetterGPSignatory.html');
            }
        }



    }

    //send notification to LP
    if (req.user.accountType == 'SecondaryLP' &&  (document.lpSignCount + 1 === subscription.noOfSignaturesRequired) ) {

        const alertData = {
            htmlMessage: messageHelper.issuedSideLetterAlertMessage,
            message: 'has issued Side Letter',
            lpName: commonHelper.getInvestorName(subscription),
            subscriptionId: document.subscriptionId,
            docType: 'SIDE_LETTER_AGREEMENT',
            fundManagerCommonName: subscription.fund.fundManagerCommonName,
            fundName: subscription.fund.fundCommonName,
            fundId: subscription.fund.id,
            sentBy: req.userId,
            documentId: document.id
        };

        notificationHelper.triggerNotification(req, subscription.fund.gpId, subscription.lpId, {}, alertData, subscription.lp.accountId, null);

        let emailBodyContentLP = {
            'firstName': subscription.lp.firstName,
            'name': commonHelper.getInvestorName(subscription),
            'lastName': subscription.lp.lastName,
            'middleName': subscription.lp.middleName,
            'fundCommonName': subscription.fund.fundCommonName        
        }

        emailHelper.triggerAlertEmailNotification(subscription.lp.email, 'Fund Manager has issued a Side Letter', emailBodyContentLP, 'gpChangeSideLetter.html');        
    }

    //if user is in-progress , then change user status to close-ready     
    if (req.user.accountType == 'LP') {
        changeInvestorToCloseReady(subscription)       
    } 

    return res.json({
        url:`${config.get('serverURL')}/api/v1/document/view/docs/${documentId}`
    });

}

// generate last page sined pdf
async function changeCommitmentAgreementSign(data, changeCommitmentAgreementTempFile) {

    const htmlFilePath = "./views/changeCommitmentAgreementLpSign.html";
    const lpSignedLastPage = await generateSignedPage(data, htmlFilePath);

    const fileName = `CHANGE_COMMITMENT_AGREEMENT_${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../../assets/temp/${fileName}`);
    await commonHelper.pdfMerge([changeCommitmentAgreementTempFile, lpSignedLastPage.path], outputFile);
    return { path: outputFile, name: fileName };
}

async function notifySignatories(fundId, subscriptionId, legalEntity) {

    const lpSignatoriesList = await models.LpSignatories.findAll({
        attributes: ['signatoryId'],
        where: {
            fundId: fundId,
            subscriptionId: subscriptionId
        },
        include: [{
            model: models.User,
            as: 'signatoryDetails',
            required: true
        }]
    });

    //get all signatories account info start
    logger.info('get all signatories account info start')
    let accountsinfo = {}
    if (lpSignatoriesList.length > 0) {
        let lpinvestorsaccountids = []
        for (let lpSignatory of lpSignatoriesList) {
            lpinvestorsaccountids.push(lpSignatory.signatoryDetails.accountId)
        }
        let getallaccountsinfo = await models.Account.findAll({
            attributes: ['id', 'emailConfirmCode', 'isEmailConfirmed', 'isEmailNotification'],
            where: { id: { [Op.in]: lpinvestorsaccountids } }
        })
        for (let allaccounts of getallaccountsinfo) {
            let tempobj = {}
            tempobj.emailConfirmCode = allaccounts.emailConfirmCode
            tempobj.isEmailConfirmed = allaccounts.isEmailConfirmed
            tempobj.isEmailNotification = allaccounts.isEmailNotification
            accountsinfo[allaccounts.id] = tempobj
        }
    }
    //--end

    if (lpSignatoriesList.length > 0) {
        for (let lpSignatory of lpSignatoriesList) {

            const loginOrRegisterLink = accountsinfo[lpSignatory.signatoryDetails.accountId].isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${accountsinfo[lpSignatory.signatoryDetails.accountId].emailConfirmCode}`;

            emailHelper.sendEmail({
                toAddress: lpSignatory.signatoryDetails.email,
                subject: messageHelper.assignLpSignatoryEmailSubject,
                data: {
                    name: commonHelper.getFullName(lpSignatory.signatoryDetails),
                    fundName: legalEntity,
                    loginOrRegisterLink: loginOrRegisterLink,
                    user:lpSignatory.signatoryDetails
                },
                htmlPath: "secondaryLPSignature.html"
            });
        }

        await models.FundSubscription.update({
            isLpSignatoriesNotified: 1
        }, {
                where: {
                    id: subscriptionId
                }
            })
    }

}

async function notifyPrimaryLP(req,lpId, legalEntity,subscription) {
    const lpData = await models.User.findOne({
        where: {
            id: lpId
        }
    });

    const loginOrRegisterLink = lpData.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${lpData.emailConfirmCode}`;

    emailHelper.sendEmail({
        toAddress: lpData.email,
        subject: 'Email Notification - Vanilla',
        data: {
            name: commonHelper.getFullName(lpData),
            fundName: legalEntity,
            loginOrRegisterLink: loginOrRegisterLink,
            user:lpData
        },
        htmlPath: "primaryLPSignature.html"
    });

    //add alert
    /*
    const alertDataToLP = {
        htmlMessage: `[fundManagerCommonName] has changed your Subscription Agreement with respect to your proposed investment in [fundCommonName]. This will require you to electronically re-execute the Subscription Agreement. You will not have to re-enter other data, though you will have a chance to review it prior to electronically re-executing. Please return to the Vanilla application to electronically re-execute your Subscription Agreement`,
        firstName: lpData.firstName,
        lastName: lpData.lastName,
        middleName: lpData.middleName,
        lpId: lpData.id,
        subscriptionId: subscription.id,
        fundManagerCommonName: subscription.fund.fundManagerCommonName,
        fundCommonName: subscription.fund.fundCommonName,
        fundId: subscription.fund.id,
        sentBy: req.userId
    }

    notificationHelper.triggerNotification(req, req.userId, lpData.id, false, alertDataToLP,lpData.accountId)*/

}
// const getOnlyLinksExternal = _getOnlyLinks;

const getSubscriptionData = async (subscriptionId) => {

    return await models.FundSubscription.findOne({
        where: {
            id: subscriptionId
        },
        include: [{
            model: models.Fund,
            as: 'fund',
            required: true,
            include: [{
                model: models.User,
                as: 'gp',
                required: true
            }],
        }, {
            model: models.User,
            as: 'lp',
            required: false
        },{
            attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
            model: models.FundClosings,
            required: false,
            as: 'fundClosingInfo',
            where: {
                isActive: 1
            }
        }]
    })

}

const getFundInvestorsData = async (fundId) => {

    return await models.FundSubscription.findAll({
        where: {
            fundId: fundId,
            deletedAt: null,
            status: 10
        },
        include: [{
            model: models.Fund,
            as: 'fund',
            required: true,
        },{
            attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
            model: models.FundClosings,
            required: false,
            as: 'fundClosingInfo',
            where: {
                isActive: 1
            }
        }, {
            attributes: ['id', 'name', 'lpSideName'],
            model: models.FundStatus,
            as: 'subscriptionStatus',
            required: true
        }]
    })

}

//sign ammendments documents
const signPendingAmmendmentAgreement = async (req, res, next) => {
try {

    const { subscriptionId, documentId, signaturePic, date, timeZone } = req.body
    //get subscription data
    const subscription = await getSubscriptionData(subscriptionId)
    logger.info('sign on the ammendments ')     
    const documentForSignature = await models.DocumentsForSignature.findOne({
        where: {                      
            isActive: 1,
            id:documentId,
            docType: {
                [Op.in]: ['AMENDMENT_AGREEMENT']
            }
        }
    })
    if (!documentForSignature) {
        return res.json({
            error: true,
            message: 'Invalid data.'
        })
    }    
    let amendmentId = documentForSignature.amendmentId  
    const lpSignaturePic = await signaturePicSave(signaturePic) 

    //Data to be rendered in HTML
    let agreementSignData = {
        // name: subscription.titlePrettyForPDF,
        jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
        investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
        legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
        lpName: commonHelper.getFullName(req.user),
        areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
        date: moment().format('M/D/YYYY'),
        lpSignaturePic: commonHelper.getSignaturePicUrl(lpSignaturePic),
        lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
        name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
    }        
 
    let ammendmentAgreementTempPath = await ammendmentAgreementSign(subscription, documentForSignature.filePath, agreementSignData)

    fs.copyFileSync(ammendmentAgreementTempPath.path, documentForSignature.filePath)
    
    let signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone)

    await models.DocumentsForSignature.update({
        gpSignCount: 0,  
        lpSignCount: literal('lpSignCount + 1'),    
        isPrimaryGpSigned: 0,   
        isAllLpSignatoriesSigned: subscription.noOfSignaturesRequired == 0 ? true : (req.user.accountType == 'LP' ? true : (documentForSignature.lpSignCount + 1 === subscription.noOfSignaturesRequired)),
        isAllGpSignatoriesSigned: false,
        docType: 'AMENDMENT_AGREEMENT',            
        isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
        isActive: 1,
        updatedBy: req.userId
    }, {
        where: {
            id: documentForSignature.id
        }
    })
        
    await models.SignatureTrack.create({
        documentType: 'AMENDMENT_AGREEMENT',
        documentId: documentForSignature.id,
        signaturePath: lpSignaturePic,
        subscriptionId: documentForSignature.subscriptionId,
        ipAddress: req.ipInfo,
        signedDateTime: signedDateconvertedToUserTimezone,
        timezone: req.body.timeZone,
        signedDateInGMT: req.body.date,
        signedUserId: req.user.id,
        signedByType: req.user.accountType,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    })   

    //check and caliculate percentage for Fully Restated Amendment and Amendment for closed investors
    if(req.user.accountType=='LP'){

        //for closed investors only , caliculate percentage
        if(subscription.status==10){
            console.log("**********amendmentId",amendmentId)
            //get investors data
            let fundInvestors = await getFundInvestorsData(subscription.fundId)
            
            //caliculate percentage data
            calculateAmendmentPercentages(subscription,fundInvestors,amendmentId,'AMENDMENT_AGREEMENT',documentId)

        }

        //if user is in-progress , then change user status to close-ready          
        changeInvestorToCloseReady(subscription)   
    }

    return res.json({
        url  : `${config.get('serverURL')}/api/v1/document/view/docs/${documentId}` 
    }) 

} catch (error) {
    next(error)
}

}


//AMMENDMENT SIGNATURES
const ammendmentDocumentsSign = async(req,res,subscription,type) => {

    logger.info('sign on the ammendments ')

    console.log(" ******* i am in ammendmentDocumentsSign ******* ")

    const { subscriptionId, signaturePic, date, timeZone } = req.body

    const lpSignaturePic = await signaturePicSave(signaturePic);

    let date_for_sign = await commonHelper.getDate();
    date_for_sign = commonHelper.getDateTimeWithTimezone(subscription.fund.timeZone, date_for_sign, 0, 0);

    //Data to be rendered in HTML
    const agreementSignData = {
        // name: subscription.titlePrettyForPDF,
        jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
        investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
        legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
        lpName: commonHelper.getFullName(req.user),
        areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
        date: date_for_sign,
        lpSignaturePic: commonHelper.getSignaturePicUrl(lpSignaturePic),
        lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
        name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
    }         

    //for signatory and for closed lp -- need to sign the dfs
    if(type=='closed' || type=='inprogresssignatory'){

        console.log(" ******* i am in ammendmentDocumentsSign  closed or inprogresssignatory ******* ")

        let getAmmendments = await models.DocumentsForSignature.findAll({
            where: {
                subscriptionId: subscription.id,
                fundId: subscription.fund.id,
                isActive: 1,
                isPrimaryLpSigned:0,
                docType: {
                    [Op.in]: ['AMENDMENT_AGREEMENT']
                }
            }
        })
    

        for (let documentsForSignature of getAmmendments) {  
     
            let ammendmentAgreementTempPath = await ammendmentAgreementSign(subscription, documentsForSignature.filePath, agreementSignData)
    
            fs.copyFileSync(ammendmentAgreementTempPath.path, documentsForSignature.filePath)
            
            // let signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone)
            let signedDateconvertedToUserTimezone = date_for_sign;

    
            await models.DocumentsForSignature.update({
                gpSignCount: 0,  
                lpSignCount: literal('lpSignCount + 1'),    
                isPrimaryGpSigned: 0,   
                isAllLpSignatoriesSigned: subscription.noOfSignaturesRequired == 0 ? true : (req.user.accountType == 'LP' ? true : (documentsForSignature.lpSignCount + 1 === subscription.noOfSignaturesRequired)),
                isAllGpSignatoriesSigned: false,
                docType: 'AMENDMENT_AGREEMENT',            
                isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
                isActive: 1,
                updatedBy: req.userId
            }, {
                    where: {
                        id: documentsForSignature.id
                    }
            })
                
            await models.SignatureTrack.create({
                documentType: 'AMENDMENT_AGREEMENT',
                documentId: documentsForSignature.id,
                signaturePath: lpSignaturePic,
                subscriptionId: documentsForSignature.subscriptionId,
                ipAddress: req.ipInfo,
                signedDateTime: signedDateconvertedToUserTimezone,
                timezone: req.body.timeZone,
                signedDateInGMT: req.body.date,
                signedUserId: req.user.id,
                signedByType: req.user.accountType,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            })
                    
            //update inactive 
            if (req.user.accountType == 'LP') {        
                await models.DocumentsForSignature.update({isActive: 0,updatedBy: req.userId}, 
                { where: { id: documentsForSignature.id } })        
            }                

            
        }

    } else { //inprogress lp --- need to create records in DFS
        
        console.log(" ******* i am in ammendmentDocumentsSign  inprogres ******* ")

        let getAmmendments = await models.FundAmmendment.findAll({
            where:{
                fundId:subscription.fund.id,
                deletedAt:null,
                isActive:1,
                isAmmendment:true,
                isAffectuated:true
            }
        })      
         
        
        // investor with out signatory

        for (let documentsForSignature of getAmmendments) { 

            let signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone)
            
            let isAmendmentExists = await models.DocumentsForSignature.findOne({
                where: {
                    fundId: subscription.fund.id,
                    subscriptionId: subscription.id,                    
                    docType: 'AMENDMENT_AGREEMENT',
                    isActive: 1,
                    amendmentId:documentsForSignature.id,    
                }
            })                 

            if(!isAmendmentExists)
            {       

                console.log(" ******* i am in ammendmentDocumentsSign  inprogres  if ******* ")  
                
                let documentInfo = documentsForSignature.document
  
                let ammendmentAgreementTempPath = await ammendmentAgreementSign(subscription, documentInfo.path, agreementSignData)
        
                let ammendmentAgreementPath = `./assets/funds/${subscription.fund.id}/${subscription.id}/AMENDMENT_AGREEMENT_${uuidv1()}.pdf`;
        
                fs.copyFileSync(ammendmentAgreementTempPath.path, ammendmentAgreementPath)                
    
                models.DocumentsForSignature.create({
                    fundId: subscription.fund.id,
                    subscriptionId: subscription.id,
                    changeCommitmentId: null,
                    filePath: ammendmentAgreementPath,
                    docType: 'AMENDMENT_AGREEMENT',
                    gpSignCount: 0,
                    lpSignCount: 0,
                    isPrimaryGpSigned: 0,
                    isAllLpSignatoriesSigned: req.user.accountType == 'LP' ? true : (subscription.noOfSignaturesRequired > 1 ? false : true),
                    isAllGpSignatoriesSigned: false,
                    isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
                    isActive: 1,
                    amendmentId:documentsForSignature.id,
                    createdBy: req.userId,
                    updatedBy: req.userId
                }).then(async (document) => {
            
                    models.SignatureTrack.create({
                        documentType: 'AMENDMENT_AGREEMENT',
                        documentId: document.id,
                        subscriptionId: document.subscriptionId,
                        signaturePath: lpSignaturePic,
                        ipAddress: req.ipInfo,
                        signedDateTime: signedDateconvertedToUserTimezone,
                        timezone: req.body.timeZone,
                        signedDateInGMT: req.body.date,
                        signedUserId: req.user.id,
                        signedByType: req.user.accountType,
                        isActive: 1,
                        createdBy: req.userId,
                        updatedBy: req.userId
                    })        
                    models.DocumentsForSignature.increment('lpSignCount', { where: { id: document.id } })    
            
                }) 
            
            } else {

                console.log(" ******* i am in ammendmentDocumentsSign  inprogres  else else ******* ")
                               
  
                let ammendmentAgreementTempPath = await ammendmentAgreementSign(subscription, isAmendmentExists.filePath, agreementSignData)         
        
                fs.copyFileSync(ammendmentAgreementTempPath.path, isAmendmentExists.filePath)

            await models.DocumentsForSignature.update({
                gpSignCount: 0,  
                lpSignCount: literal('lpSignCount + 1'),    
                isPrimaryGpSigned: 0,   
                isAllLpSignatoriesSigned: subscription.noOfSignaturesRequired == 0 ? true : (req.user.accountType == 'LP' ? true : (isAmendmentExists.lpSignCount + 1 === subscription.noOfSignaturesRequired)),
                isAllGpSignatoriesSigned: false,
                docType: 'AMENDMENT_AGREEMENT',            
                isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
                isActive: 1,
                updatedBy: req.userId
            }, {
                    where: {
                        id: isAmendmentExists.id
                    }
            })
                
            await models.SignatureTrack.create({
                documentType: 'AMENDMENT_AGREEMENT',
                documentId: isAmendmentExists.id,
                signaturePath: lpSignaturePic,
                subscriptionId: isAmendmentExists.subscriptionId,
                ipAddress: req.ipInfo,
                signedDateTime: signedDateconvertedToUserTimezone,
                timezone: req.body.timeZone,
                signedDateInGMT: req.body.date,
                signedUserId: req.user.id,
                signedByType: req.user.accountType,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            })

            }
          
        }  

    }
}



//FULLY RESTATED FUND AGREEMENST SIGNATURES
const signPendingFundAgreement = async(req,res,next) => {
    try {

    const { subscriptionId, documentId, signaturePic, date, timeZone } = req.body
    
    console.log('sign on the fully restated fund agreement')

    if (!documentId) {
        return errorHelper.error_400(res, 'documentId', messageHelper.noDataFound);
    }    

    //get subscription data
    const subscription = await getSubscriptionData(subscriptionId)
       
    const documentForSignature = await models.DocumentsForSignature.findOne({
        where: {        
            isActive: 1,
            id:documentId,
            docType: {
                [Op.in]: ['FUND_AGREEMENT']
            }
        },
        include: [{
            attributes: ['timezone'],
            model: models.Fund,
            as: 'fund',
            required: false,
            include: [{
                model: models.timezone,
                as: 'timeZone',
                required: false
            }]
        }]
    });

    if (!documentForSignature) {
        return res.json({
            error: true,
            message: 'Invalid data.'
        })
    } 

    console.log("**************documentForSignature***********",documentForSignature)
    console.log("**************documentForSignature id***********",documentForSignature.id)
    
    let amendmentId = documentForSignature.amendmentId
    const lpSignaturePic = await signaturePicSave(signaturePic);

    // converting date time with timezone
    let date_for_sign = await commonHelper.getMomentUTCDate();
    date_for_sign = commonHelper.getDateTimeWithTimezone(documentForSignature.fund.timeZone, date_for_sign, 0, 0);

    //Data to be rendered in HTML
    let agreementSignData = {
        // name: subscription.titlePrettyForPDF,
        jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
        investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
        legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
        lpName: commonHelper.getFullName(req.user),
        areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
        date: date_for_sign,
        lpSignaturePic: commonHelper.getSignaturePicUrl(lpSignaturePic),
        lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
        name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
    }        
    
    let ammendmentAgreementTempPath = await ammendmentAgreementSign(subscription, documentForSignature.filePath, agreementSignData)

    fs.copyFileSync(ammendmentAgreementTempPath.path, documentForSignature.filePath)
    
    let signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone)

    await models.DocumentsForSignature.update({
        gpSignCount: 0,  
        lpSignCount: literal('lpSignCount + 1'),    
        isPrimaryGpSigned: 0,   
        isAllLpSignatoriesSigned: subscription.noOfSignaturesRequired == 0 ? true : (req.user.accountType == 'LP' ? true : (documentForSignature.lpSignCount + 1 === subscription.noOfSignaturesRequired)),
        isAllGpSignatoriesSigned: false,
        docType: 'FUND_AGREEMENT',            
        isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
        isActive: 1,
        updatedBy: req.userId
    }, {
            where: {
                id: documentForSignature.id
            }
    })
        
    await models.SignatureTrack.create({
        documentType: 'FUND_AGREEMENT',
        documentId: documentForSignature.id,
        signaturePath: lpSignaturePic,
        subscriptionId: documentForSignature.subscriptionId,
        ipAddress: req.ipInfo,
        signedDateTime: signedDateconvertedToUserTimezone,
        timezone: req.body.timeZone,
        signedDateInGMT: req.body.date,
        signedUserId: req.user.id,
        signedByType: req.user.accountType,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    })   
    
    //check and caliculate percentage for Fully Restated Amendment and Amendment for closed investors
    if(req.user.accountType=='LP'){ 

        //for closed investors only , caliculate percentage
        if(subscription.status==10){

            //get investors data
            let fundInvestors = await getFundInvestorsData(subscription.fundId)

            //caliculate percentage data
            calculateAmendmentPercentages(subscription,fundInvestors,amendmentId,'FUND_AGREEMENT',documentId)

        }

        //if user is in-progress , then change user status to close-ready          
        changeInvestorToCloseReady(subscription);
        //================email  
        
        let emailBodyContent = {
            name: commonHelper.getInvestorName(subscription),
            fundCommonName: subscription.fund.fundCommonName,
            capitalCommitmentOfferedAmount: subscription.lpCapitalCommitmentPretty,
            clientURL: config.get('clientURL'),
            fundManagerName : commonHelper.getFullName(subscription.fund.gp)
        };
        if (subscription.fund.gp && subscription.fund.isNotificationsEnabled) { // if setting enabled

            emailHelper.triggerAlertEmailNotification(subscription.fund.gp.email, 'Investor has become close ready', emailBodyContent, 'lpBecomeCloseReady.html');
        }
    
    } 

    return res.json({
        url  : `${config.get('serverURL')}/api/v1/document/view/docs/${documentId}` 
    })     
    
    }catch (error) {
        next(error)
    }
}

//Calculate Amendment
const calculateAmendmentPercentages = async(subscription,totalClosedInvetors,amendmentId,documentType,documentId) => {

    let closedCommitment=0;
    let approvedPercentage=0;

    //Remove this model if already fetched  in the begining
    const amendmentInfo = await models.FundAmmendment.findOne({
        where:{
            id: amendmentId,
            isArchived:0,
            isActive:1
        }
    });

    let documents = await models.DocumentsForSignature.findAll({
        where: {
            fundId: subscription.fund.id,
            isActive: 1,
            isPrimaryLpSigned:1,
            docType:documentType,
            amendmentId
        }
    });

    let subscriptionIds = _.map(documents,'subscriptionId');

    let fundInvestors = await models.FundSubscription.findAll({
        where: {
            fundId: subscription.fund.id,
            id: {
                [Op.in]:subscriptionIds
            },
            deletedAt: null,
            status: 10
        },
        include: [{
            model: models.Fund,
            as: 'fund',
            required: true,
        },{
            attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
            model: models.FundClosings,
            required: false,
            as: 'fundClosingInfo',
            where: {
                isActive: 1
            }
        }, {
            attributes: ['id', 'name', 'lpSideName'],
            model: models.FundStatus,
            as: 'subscriptionStatus',
            required: true
        }]
    });

    await models.DocumentsForSignature.update({
        lpSignedDate: moment().format('MM/DD/YYYY HH:mm:ss.ssss Z')
    },{
        where:{
            fundId: subscription.fund.id,
            isActive: 1,
            isPrimaryLpSigned:1,
            docType: documentType,
            id:documentId
        }
    });

    totalClosedInvetors.forEach(closedInvetor=>{
        closedCommitment = closedCommitment +(_.last(closedInvetor.fundClosingInfo) ? _.last(closedInvetor.fundClosingInfo).gpConsumed:0);

    });

    fundInvestors.forEach(fundInvestor=>{
        
        const gpCommitmentPercent = (fundInvestor.fund.percentageOfLPAndGPAggregateCommitment !== '0') ? (100 - fundInvestor.fund.percentageOfLPAndGPAggregateCommitment) : 0;
        
        const closedFMCommitmentValue = fundInvestor.fund.percentageOfLPCommitment !== '0' ? ((fundInvestor.fund.percentageOfLPCommitment / 100) * closedCommitment) :
            (fundInvestor.fund.capitalCommitmentByFundManager !== 0.00 ? fundInvestor.fund.capitalCommitmentByFundManager :
                (fundInvestor.fund.percentageOfLPAndGPAggregateCommitment !== '0' ? (closedCommitment * (100 / gpCommitmentPercent)) - closedCommitment : ''));   

        let investorCommitment =  _.last(fundInvestor.fundClosingInfo).gpConsumed 
                ? _.last(fundInvestor.fundClosingInfo).gpConsumed 
                : fundInvestor.lpCapitalCommitment

        let denominator= amendmentInfo.considerOnlyLpOfferedAmount ? closedCommitment : (closedCommitment + closedFMCommitmentValue);


        approvedPercentage =  approvedPercentage+((investorCommitment/denominator) * 100)


    });

    let docType = amendmentInfo.isAmmendment ? 'Amendment' :'Restated Fund Agreement'

    if(approvedPercentage > amendmentInfo.targetPercentage){

        emailHelper.sendEmail({
            toAddress: subscription.fund.gp.email,
            subject: 'Email Notification - Vanilla',
            data: {
                name: commonHelper.getFullName(subscription.fund.gp) ,
                fundCommonName: subscription.fund.fundCommonName,
                clientURL: config.get('clientURL'),
                docType,
                user:subscription.fund.gp
            },
            htmlPath: "alerts/gpTargetPercentageReached.html"
        });

        const alertFrom = subscription.lp.id;
        const alertTo = subscription.fund.gp.id;
    
        const alertData = {
            fundManager: commonHelper.getFullName(subscription.fund.gp),
            htmlMessage: messageHelper.gpTargetPercentageReached,
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            fundCommonName: subscription.fund.fundCommonName,
            docType: docType,
            sentBy: subscription.lp.id
        };
        notificationHelper.triggerNotification(null, alertFrom, alertTo, [], alertData, subscription.fund.gp.accountId, subscription.fund.gp);

    }

    await models.FundAmmendment.update({
        approvedPercentage: _.round(approvedPercentage,2)
    },{
        where:{
            id:amendmentId,
            isArchived:0,
            isActive:1
        }
    });
}


//SIDE LETTER AGREEMENT SIGNATURE
const sideLetterDocumentsSign = async(req,res,subscription) => {

    logger.info('Side Letter Agreement Sign ')

    const { documentId, signaturePic, date, timeZone } = req.body;
    
    const documentsForSideLetter = await models.DocumentsForSignature.findOne({
        where: {
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            docType: 'SIDE_LETTER_AGREEMENT'
        }
    })
    
    if (documentsForSideLetter) {

        const lpSignaturePic = await signaturePicSave(signaturePic);

        let date_for_sign = await commonHelper.getDate();
        date_for_sign = commonHelper.getDateTimeWithTimezone(subscription.fund.timeZone, date_for_sign, 0, 0);

        let agreementSignData = {
            jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
            investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
            legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
            areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
            lpName: commonHelper.getFullName(req.user),
            date: date_for_sign,
            lpSignaturePic: commonHelper.getSignaturePicUrl(lpSignaturePic),
            lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
            name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
        }        
    
    const sideLetterAgreementTempPath = await sideLetterAgreementSign(subscription, documentsForSideLetter.filePath, agreementSignData)

    const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);
    
    fs.copyFileSync(sideLetterAgreementTempPath.path, documentsForSideLetter.filePath)
           
        await models.DocumentsForSignature.update({
            fundId: subscription.fund.id,
            subscriptionId: subscription.id,
            changeCommitmentId: null,
            isAllLpSignatoriesSigned: subscription.noOfSignaturesRequired == 0 ? true : (req.user.accountType == 'LP' ? true : (documentsForSideLetter.lpSignCount + 1 === subscription.noOfSignaturesRequired)),
            isAllGpSignatoriesSigned: false,
            docType: 'SIDE_LETTER_AGREEMENT',
            gpSignCount: 0,
            lpSignCount: literal('lpSignCount + 1'),
            isPrimaryGpSigned: 0,
            isPrimaryLpSigned: req.user.accountType == 'LP' ? true : false,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        }, {
                where: {
                    id: documentsForSideLetter.id
                }
            });
    
    
        models.SignatureTrack.create({
            documentType: 'SIDE_LETTER_AGREEMENT',
            documentId: documentsForSideLetter.id,
            signaturePath: lpSignaturePic,
            subscriptionId: documentsForSideLetter.subscriptionId,
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: req.body.timeZone,
            signedDateInGMT: req.body.date,
            signedUserId: req.user.id,
            signedByType: req.user.accountType,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        })     

    }
    
}

//update close-ready status  
const changeInvestorToCloseReady = async(subscription) => {
    //for non-closed investors check
    if(subscription.status !=10){
        let documentsForSignature = await models.DocumentsForSignature.count({ 
            where: { 
                    subscriptionId:subscription.id,
                    isActive:1,
                    isPrimaryLpSigned:0,
                    isArchived:0
                }        
        })
        //if there are no documents for signature , then inprogress user will change to close-ready
        if(documentsForSignature==0){
            await models.FundSubscription.update({ status: 7 }, { where: { id: subscription.id } })
        }
    }
}


const closedLPRevisedFundAgreement = async (req, res, next) => {
    try {
        const accountType = req.user.accountType
        const subscriptionId  = req.params.subscriptionId
        const documentId  = req.params.documentId
        
        if (!documentId || !subscriptionId) {
            return errorHelper.error_403(res, 'documentId', messageHelper.noDataFound);
        }
        if(accountType =='LP' || accountType =='SecondaryLP') {
         
            let document = await models.DocumentsForSignature.findOne({
                attributes:['changedPagesId'],
                where: {
                    id:documentId
                }          
            })        

            return res.json({
            fundAgreementUrl:  `${config.get('serverURL')}/api/v1/document/view/afa/${subscriptionId}/${documentId}`, 
            changedPageUrl: document.changedPagesId!=null && document.changedPagesId>0  ? `${config.get('serverURL')}/api/v1/document/view/viewcp/${document.changedPagesId}` : '' 
         
            })            
            
        } else {
            return errorHelper.error_403(res, 'documentId', messageHelper.noDataFound);
        }      

    } catch (e) {
        next(e);
    }
}

module.exports = {
    signSubscriptionAndPartnershipAgreement,    
    verifySignaturePassword,
    signChangeSideletter,
    signChangeCommitment,
    generateSubscriptionAndFundAgreement,
    getCurrentLinks,
    signPendingAmmendmentAgreement,
    signPendingFundAgreement,
    closedLPRevisedFundAgreement
  //  getOnlyLinksExternal
}
