const models = require('../../models/index');
const _ = require('lodash');

const list = async (req, res, next) => {
    
    try {

        const { limit = 1, fundId, userId, eventType } = req.query;

        const perPage = 10;

        let whereFilter = {};

        if (fundId && fundId != '') {
            whereFilter.fundId = fundId;
        }
        if (userId && userId != '') {            
            whereFilter.userId = userId;
        }
        if (eventType && eventType != '') {
            whereFilter.eventType = eventType;
        }

        let eventsList = await models.Events.findAndCountAll({
            attributes: ['id', 'eventType', 'eventTimestamp', 'ipAddress', 'userId', 'fundId'],
            limit: Number(perPage),
            offset: (Number(limit) - 1) * perPage,
            where: whereFilter,
            include: [{
                attributes: ['id', 'firstName', 'middleName', 'lastName', 'email', 'accountType'],
                model: models.User,
                as: 'eventUser',
                required: true,
            },
            {
                attributes: ['id', 'legalEntity', 'gpId'],
                model: models.Fund,
                as: 'eventFund',
                required: true,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }]
            },
            {
                attributes: ['id', 'status'],
                model: models.FundSubscription,
                as: 'FundSubscription',
                required: false,
                include: [{
                    model: models.User,
                    attributes:['firstName','lastName','middleName'],
                    as: 'lp',
                    required: false
                },{
                    model: models.OfflineUsers,
                    attributes:['firstName','lastName','middleName'],
                    as: 'offlineLp',
                    required: false
                }]                
            }            
            ]
        });
        
        return res.json({
           eventsList
        });

    } catch (e) {
        next(e);
    }
}

const listUsers = async (req, res, next) => {
    
    try {

        models.User.findAll({
            attributes: ['id', 'firstName', 'middleName', 'lastName', 'email', 'accountType'],            
            order: [['firstName', 'ASC']]
        }).then(data => res.json(data)).catch(error => res.json(error))

    } catch (e) {
        next(e);
    }
}

const eventTypes = async (req, res, next) => {
    
    try {

        models.Events.findAll({
            attributes: [[models.sequelize.fn('DISTINCT', models.sequelize.col('eventType')), 'eventType']],
        }).then(data => res.json(data)).catch(error => res.json(error))

    } catch (e) {
        next(e);
    }
}

module.exports = {
    list,
    listUsers,
    eventTypes,
}
