const models = require('../../models/index');
const _ = require('lodash');
const config = require('../../config/index');
const { Op } = require('sequelize');
const bcrypt = require('bcryptjs');
const moment = require('moment');
const messageHelper = require('../../helpers/messageHelper');
const commonHelper = require('../../helpers/commonHelper');
const emailHelper = require('../../helpers/emailHelper');
const errorHelper = require('../../helpers/errorHelper');

const gpList = async (req, res, next) => {

    try {

        const { search = '', order = 'ASC', orderBy = 'firstName', fundId } = req.query;

        const page = req.query.page || 1;

        // Sorting
        const orderByFirstname = [
            [orderBy, order],
        ];

        let whereFilter = {
            accountType: ['GP', 'GPDelegate', 'SecondaryGP'],
            deletedAt: null
        }

        // Search option
        if (search && search != 'null') {
            whereFilter = {
                accountType: ['GP', 'GPDelegate', 'SecondaryGP'],
                deletedAt: null,
                [Op.or]: [
                    { firstName: { [Op.like]: `%${search}%` } },
                    { lastName: { [Op.like]: `%${search}%` } }
                ]
            }
        }

        const perPage = 10;

        // Filter by fund        
        let fundGpIds = [];
        if (fundId && fundId != '') {
            const fund = await models.Fund.findOne({
                attributes: ['id', 'legalEntity', 'gpId'],
                where: {
                    id: fundId,
                    deletedAt: null
                }
            });
            fundGpIds.push(fund.gpId);

            const gpDelegates = await models.GpDelegates.findAll({
                attributes: ['fundId', 'delegateId'],
                where: {
                    fundId: fundId
                }
            });

            let fundGpDelegateIds = ''
            if (gpDelegates != null) {
                fundGpDelegateIds = _.map(gpDelegates, 'delegateId')
            }

            const gpSignatories = await models.GpSignatories.findAll({
                attributes: ['fundId', 'signatoryId'],
                where: {
                    fundId: fundId
                }
            });

            let fundGpSignatoryIds = ''
            if (gpDelegates != null) {
                fundGpSignatoryIds = _.map(gpSignatories, 'signatoryId')
            }

            const fundUserIds = _.concat(fundGpIds, fundGpDelegateIds, fundGpSignatoryIds)

            whereFilter = {
                accountType: ['GP', 'GPDelegate', 'SecondaryGP'],
                id: fundUserIds
            }
        }


        const users = await models.User.findAndCountAll({
            attributes: ['id', 'firstName','middleName', 'lastName', 'email', 'accountType', 'accountTypeMask', 'profilePic', 'isEmailNotification','isEmailConfirmed'],
            offset: (Number(page) - 1) * perPage,
            limit: Number(perPage),
            order: orderByFirstname,
            where: whereFilter,
            include:[{
                model:models.Account,
                attributes:['loginFailAttempts'],
                as:'account'
            }]
        });

        let usersList =[];
        let userObj={};
        users.rows.forEach(user => {
            user.loginFailAttempts = user.account?user.account.loginFailAttempts:0
            usersList.push(user)
        });

        userObj.count = users.count;
        userObj.rows = usersList;

        return res.json({
            users
        });

    } catch (e) {
        next(e);
    }
}

const lpList = async (req, res, next) => {

    try {

        const { search = '', order = 'ASC', orderBy = 'firstName', fundId } = req.query;
        const page = req.query.page || 1;

        // Sorting
        const orderByFirstname = [
            [orderBy, order],
        ];

        let whereFilter = {
            accountType: ['LP', 'LPDelegate', 'SecondaryLP'],
            deletedAt: null
        }

        // Search option
        if (search && search != 'null') {
            whereFilter = {
                accountType: ['LP', 'LPDelegate', 'SecondaryLP'],
                deletedAt: null,
                [Op.or]: [
                    { firstName: { [Op.like]: `%${search}%` } },
                    { lastName: { [Op.like]: `%${search}%` } }
                ]
            }
        }

        const perPage = 10;

        // Filter by fund
        if (fundId && fundId != '') {
            const subscriptions = await models.FundSubscription.findAll({
                attributes: ['fundId', 'lpId'],
                where: {
                    fundId: fundId
                }
            });

            const fundLpIds = _.map(subscriptions, 'lpId');

            const lpDelegate = await models.LpDelegate.findAll({
                attributes: ['fundId', 'delegateId'],
                where: {
                    fundId: fundId
                }
            });

            let fundLpDelegateIds = ''
            if (lpDelegate != null) {
                fundLpDelegateIds = _.map(lpDelegate, 'delegateId');
            }

            // lp signatories
            const lpSignatory = await models.LpSignatories.findAll({
                attributes: ['fundId', 'signatoryId'],
                where: {
                    fundId: fundId
                }
            });

            let fundLpSignatoryIds = ''
            if (lpSignatory != null) {
                fundLpSignatoryIds = _.map(lpSignatory, 'signatoryId');
            }

            const lpUserIds = _.concat(fundLpIds, fundLpDelegateIds, fundLpSignatoryIds)

            whereFilter = {
                accountType: ['LP', 'LPDelegate', 'SecondaryLP'],
                id: lpUserIds
            }
        }

        const users = await models.User.findAndCountAll({
            attributes: ['id', 'firstName', 'middleName','lastName', 'email', 'accountType', 'accountTypeMask', 'profilePic', 'isEmailNotification','isEmailConfirmed'],
            offset: (Number(page) - 1) * perPage,
            limit: Number(perPage),
            order: orderByFirstname,
            where: whereFilter,
            include:[{
                model:models.Account,
                attributes:['loginFailAttempts'],
                as:'account'
            }]
        });

        let usersList =[];
        let userObj={};
        users.rows.forEach(user => {
            user.loginFailAttempts = user.account?user.account.loginFailAttempts:0
            usersList.push(user)
        });

        userObj.count = users.count;
        userObj.rows = usersList;
        return res.json({
            users:userObj
        });

    } catch (e) {
        next(e);
    }
}


const setNotificationSetting = async (req, res, next) => {

    try {

        const { userId } = req.params;
        const { isEmailNotification, isUnlockUser} = req.body;

        if (!Number(userId)) {
            return errorHelper.error_400(res, 'userId', messageHelper.userNotFound);
        }

        const user = await models.User.findOne({
            attributes: ['id', 'accountId'],
            where: { id : userId }
        });

        let accountId = user.accountId

        const account = await models.Account.findOne({
            attributes: ['id', 'loginFailAttempts'],
            where: { id : accountId }
        });

        await models.User.update({ 
              isEmailNotification: isEmailNotification ? 1 : 0,
              loginFailAttempts: isUnlockUser ? 0: account.loginFailAttempts},
            { where: { accountId: accountId } }
        );

        await models.Account.update({ 
              isEmailNotification: isEmailNotification ? 1 : 0,
              loginFailAttempts: isUnlockUser ? 0: account.loginFailAttempts},
            { where: { id: accountId } }
        );

        return res.json({
            error: false,
            message: messageHelper.userSettingsUpdated
        });

    } catch (e) {
        next(e);
    }
}


const listFunds = (req, res, next) => {

    try {

        models.Fund.findAll({
            attributes: ['id', 'legalEntity'],
            order: [['legalEntity', 'ASC']]
        }).then(data => res.json(data)).catch(error => res.json(error))

    } catch (error) {
        next(error);
    }
}

const listLpsByFundId = async (req, res, next) => {

    try {
        const { fundId } = req.params;

        const investors = await models.FundSubscription.findAll({
            where: {
                fundId:  fundId
            },
            include: [{
                attributes: ['id', 'firstName','middleName', 'lastName','organizationName'],
                model: models.User,
                required: true,
                as: 'lp'
            }]
        });
        const data = [];
        for(investor of investors) {
            const trackerTitle = ((investor.investorType == 'LLC' ? investor.entityName : 
            (investor.investorType == 'Trust' ? investor.trustName : null )) || (investor.organizationName ? investor.organizationName:investor.lp.organizationName) || 
            commonHelper.getFullName(investor.lp));
            data.push({
                title : trackerTitle,
                lpId:  investor.lpId
            })
        }
        return res.json(data);

    } catch (error) {
        next(error);
    }
}

const addGpDelegate = async (req, res, next) => {
    try {

        const { fundId, firstName,middleName, lastName, email, cellNumber} = req.body

        const password = commonHelper.randomString(7);
        const salt = bcrypt.genSaltSync(10);

        const passwordHash = bcrypt.hashSync(password, salt);

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        });

        const gpData = await models.User.findOne({ where: { id: fund.gpId } })

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        //create/get account details                       
        let account = {}     
        let accounttype = 'old' 
        account = await models.Account.findOne({
            where: { email: email }
        })       
        if(!account){
            account = await models.Account.create({
                email:email,
                firstName,
                lastName,
                middleName,
                isEmailConfirmed:1,
                cellNumber:cellNumber,                
                password: passwordHash
            })
            accounttype = 'new'
        } else {
            await models.Account.update({                 
                firstName,
                lastName,
                middleName,
                cellNumber
            },{where:{id:account.id}})
        }   

        let accountId = account.id
        //--account details        

        return models.sequelize.transaction(function (t) {

            // create user
            return models.User.findOrCreate({
                where: { email: email, accountType: 'GPDelegate' }, defaults: {
                    firstName,
                    lastName,
                    middleName,
                    email,
                    isEmailConfirmed: 1,
                    accountType: 'GPDelegate',
                    cellNumber: cellNumber,
                    accountId,
                    isEmailConfirmed:account.isEmailConfirmed
                }
            }).spread((user, created) => {
                return Promise.all([
                    user,
                    // assign role
                    models.UserRole.findOrCreate({
                        where: { roleId: 3, userId: user.id },
                        defaults: { roleId: 3, userId: user.id }
                    }),
                    // assign fund to created gpdelegate
                    models.GpDelegates.findOrCreate({
                        paranoid: false,
                        where: { fundId: fundId, gpId: fund.gpId, delegateId: user.id }, defaults: {
                            fundId: fundId,
                            delegateId: user.id,
                            gpId: fund.gpId,
                            createdBy: req.userId,
                            updatedBy: req.userId
                        }
                    }).spread((gpDelegates, created) => gpDelegates.restore()),

                    models.VcFrimDelegate.findOrCreate({
                        paranoid: false,
                        where: { vcfirmId: fund.vcfirmId, delegateId: user.id, type: 'GP' },
                        defaults: { vcfirmId: fund.vcfirmId, delegateId: user.id, type: 'GP', createdBy: req.userId, updatedBy: req.userId }
                    }).spread((vcFrimDelegate, created) => vcFrimDelegate.restore()),

                ]) //promise end
            })
        }).then(function (result) {
            const user = result[0];

            //update default userid in accounts table
            if(accounttype=='new')
                models.Account.update({defaultUserId:user.id},{where:{id:accountId}})   

            //send email here
            emailHelper.sendEmail({
                toAddress: account.email,
                subject: messageHelper.welcomeEmailSubject,
                data: {
                    name: commonHelper.getFullName(user),
                    email: account.email,
                    passwordText: password,
                    fundLegalEntityName: fund.legalEntity,
                    gpName: commonHelper.getFullName(gpData),
                    loginLink: config.get('clientURL'),
                    user: account
                },
                htmlPath: "gpDelegateAccountDetails.html"
            });

            return res.json({
                message: messageHelper.delegateAddSuccessMessage
            });

        }).catch(function (err) {
            return next(err)
        });
    } catch (error) {
        next(error);
    }
}

const addLp = async (req, res, next) => {
    try {

        const { fundId, firstName, lastName,middleName, email, cellNumber} = req.body

        const password = commonHelper.randomString(7);
        const salt = bcrypt.genSaltSync(10);

        const passwordHash = bcrypt.hashSync(password, salt);
        let invitedDate =  moment().format('MM-DD-YYYY');

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        });

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }  
      
        //create/get account details                       
        let account = {}     
        let accounttype = 'old' 
        account = await models.Account.findOne({
            where: { email: email }
        })       
        if(!account){
            account = await models.Account.create({
                email:email,
                firstName,
                lastName,
                middleName,
                isEmailConfirmed:1,
                cellNumber:cellNumber,                
                password: passwordHash
            })
            accounttype = 'new'
        } else {
            await models.Account.update({                 
                firstName,
                lastName,
                middleName,
                cellNumber
            },{where:{id:account.id}})            
        }        
        let accountId = account.id
        //--account details

        return models.sequelize.transaction(function (t) {

            // create user
            return models.User.findOrCreate({
                where: { email: email, accountType: 'LP' }, defaults: {
                    firstName,
                    lastName,
                    middleName,
                    email,
                    isEmailConfirmed: 1,
                    accountType: 'LP',
                    cellNumber: cellNumber,
                    accountId,
                    isEmailConfirmed:account.isEmailConfirmed
                }
            }).spread((user, created) => {
                return Promise.all([
                    user,
                    // assign role
                    models.UserRole.findOrCreate({
                        where: { roleId: 4, userId: user.id },
                        defaults: { roleId: 4, userId: user.id }
                    }),
                   
                    models.VcFrimLp.findOrCreate({
                        paranoid: false,
                        where: { vcfirmId: fund.vcfirmId, lpId: user.id, isDeleted: 0 },
                        defaults: { vcfirmId: fund.vcfirmId, lpId: user.id, createdBy: req.userId, updatedBy: req.userId }
                    }).spread((vcFrimLp, created) => vcFrimLp.restore()),

                    models.FundSubscription.findOrCreate({
                        paranoid: false,
                        where: { lpId: user.id, fundId: fundId },
                        defaults: { lpId: user.id, fundId: fundId, status: 1, createdBy: req.userId, updatedBy: req.userId,invitedDate:invitedDate,isInvestorInvited:2}
                    }).spread((fundSubscription, created) => fundSubscription.status == 10 ?
                        models.FundSubscription.create({ lpId: user.id, fundId: fundId, status: 1, createdBy: req.userId, updatedBy: req.userId })
                        : fundSubscription.restore()),

                ]) //promise end
            })
        }).then(function (result) {
            const user = result[0];

            //update default userid in accounts table
            if(accounttype=='new')
                models.Account.update({defaultUserId:user.id},{where:{id:accountId}})            

            //send email here            
            emailHelper.sendEmail({
                toAddress: account.email,
                subject: messageHelper.welcomeEmailSubject,
                data: {
                    name: commonHelper.getFullName(user),
                    email: account.email,
                    passwordText: password,
                    fundLegalEntityName: fund.legalEntity,
                    loginLink: config.get('clientURL'),
                    user: account
                },
                htmlPath: "lpAccountDetails.html"
            });

            return res.json({
                message: messageHelper.lpAddSuccessMessage
            });

        }).catch(function (err) {
            return next(err)
        });
    } catch (error) {
        next(error);
    }
}

const addLpDelegate = async (req, res, next) => {
    try {

        const { fundId, lpId, firstName,middleName, lastName, email, cellNumber } = req.body

        const password = commonHelper.randomString(7);
        const salt = bcrypt.genSaltSync(10);

        const passwordHash = bcrypt.hashSync(password, salt);

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        });

        const lpData = await models.User.findOne({ where: { id: lpId } })

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }


        //create/get account details                       
        let account = {}     
        let accounttype = 'old' 
        account = await models.Account.findOne({
            where: { email: email }
        })       
        if(!account){
            account = await models.Account.create({
                email:email,
                firstName,
                lastName,
                middleName,
                isEmailConfirmed:1,
                cellNumber:cellNumber,                
                password: passwordHash
            })
            accounttype = 'new'
        } else {
            await models.Account.update({                 
                firstName,
                lastName,
                middleName,
                cellNumber
            },{where:{id:account.id}})            
        }        
        let accountId = account.id
        //--account details

        return models.sequelize.transaction(function (t) {

            // create user
            return models.User.findOrCreate({
                where: { email: email, accountType: 'LPDelegate' }, defaults: {
                    firstName,
                    lastName,
                    email,
                    middleName,
                    isEmailConfirmed: 1,
                    accountType: 'LPDelegate',
                    cellNumber: cellNumber,
                    accountId,
                    isEmailConfirmed:account.isEmailConfirmed
                }
            }).spread((user, created) => {

                return Promise.all([
                    user,
                    // assign role
                    models.UserRole.findOrCreate({
                        where: { roleId: 5, userId: user.id },
                        defaults: { roleId: 5, userId: user.id }
                    }),

                    models.FundSubscription.findOrCreate({
                        paranoid: false,
                        where: { lpId: lpId, fundId: fundId },
                        defaults: { lpId: lpId, fundId: fundId, status: 1, createdBy: req.userId, updatedBy: req.userId }
                    }).spread((fundSubscription, created) => fundSubscription.status == 10 ?
                        models.FundSubscription.create({ lpId: lpId, fundId: fundId, status: 1, createdBy: req.userId, updatedBy: req.userId }) : fundSubscription.restore()),

                    // assign fund to created lpdelegate
                    models.LpDelegate.findOrCreate({
                        paranoid: false,
                        where: {
                            fundId: fundId, lpId: lpId, delegateId: user.id
                        },
                        defaults: {
                            fundId: fundId,
                            delegateId: user.id,
                            lpId: lpId,
                            createdBy: req.userId,
                            updatedBy: req.userId
                        }
                    }).spread((lpDelegate, created) => lpDelegate.restore()),

                    models.VcFrimDelegate.findOrCreate({
                        paranoid: false,
                        where: { vcfirmId: lpId, delegateId: user.id, type: 'LP' },
                        defaults: { vcfirmId: lpId, delegateId: user.id, type: 'LP', createdBy: req.userId, updatedBy: req.userId }
                    }).spread((vcFrimDelegate, created) => vcFrimDelegate.restore()),

                ]) //promise end
            })
        }).then(function (result) {


            const user = result[0];
            const subscriptionResult = result[2];

            models.LpDelegate.update({
                subscriptionId: subscriptionResult.id,
            }, {
                    where: {
                        fundId: fundId,
                        delegateId: user.id,
                        lpId: lpId,
                    }
                });

        //update default userid in accounts table
        if(accounttype=='new')
            models.Account.update({defaultUserId:user.id},{where:{id:accountId}})  

            //send email here
            emailHelper.sendEmail({
                toAddress: account.email,
                subject: messageHelper.welcomeEmailSubject,
                data: {
                    name: commonHelper.getFullName(user),
                    email: account.email,
                    passwordText: password,
                    fundLegalEntityName: fund.legalEntity,
                    lpName: commonHelper.getFullName(lpData),
                    loginLink: config.get('clientURL'),
                    user:account
                },
                htmlPath: "lpDelegateAccountDetails.html"
            });

            return res.json({
                message: messageHelper.delegateAddSuccessMessage
            });

        }).catch(function (err) {
            return next(err)
        });
    } catch (error) {
        next(error);
    }
}

const addGpSignatory = async (req, res, next) => {

    try {

        const { fundId, firstName,middleName, lastName, email, cellNumber } = req.body

        const password = commonHelper.randomString(7);
        const salt = bcrypt.genSaltSync(10);

        const passwordHash = bcrypt.hashSync(password, salt);

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        });

        const gpData = await models.User.findOne({ where: { id: fund.gpId } })

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        // check total no. of signatoties to be added are 3
        const signatoriesCount = await models.GpSignatories.count({ where: { fundId: fundId } });

        if (signatoriesCount >= 3) {
            return errorHelper.error_400(res, 'fundId', messageHelper.numberOfSignatoriesCheck);
        }

        //create/get account details                       
        let account = {}     
        let accounttype = 'old' 
        account = await models.Account.findOne({
            where: { email: email }
        })       
        if(!account){
            account = await models.Account.create({
                email:email,
                firstName,
                lastName,
                middleName,
                isEmailConfirmed:1,
                cellNumber:cellNumber,                
                password: passwordHash
            })
            accounttype = 'new'
        } else {
            await models.Account.update({                 
                firstName,
                lastName,
                middleName,
                cellNumber
            },{where:{id:account.id}})            
        }        
        let accountId = account.id
        //--account details        

        return models.sequelize.transaction(function (t) {

            // create user
            return models.User.findOrCreate({
                where: { email: email, accountType: 'SecondaryGP' }, defaults: {
                    firstName,
                    lastName,
                    email,
                    middleName,
                    isEmailConfirmed: 1,
                    accountType: 'SecondaryGP',
                    cellNumber: cellNumber,
                    accountId,
                    isEmailConfirmed:account.isEmailConfirmed
                }
            }).spread((user, created) => {
                return Promise.all([
                    user,
                    // assign role
                    models.UserRole.findOrCreate({
                        where: { roleId: 6, userId: user.id },
                        defaults: { roleId: 6, userId: user.id }
                    }),
                    // assign fund to created secondary gp
                    models.GpSignatories.findOrCreate({
                        paranoid: false,
                        where: { fundId: fundId, gpId: fund.gpId, signatoryId: user.id },
                        defaults: {
                            fundId: fundId,
                            signatoryId: user.id,
                            gpId: fund.gpId,
                            createdBy: req.userId,
                            updatedBy: req.userId
                        }
                    }).spread((gpSignatories, created) => gpSignatories.restore()),

                    models.VcFirmSignatories.findOrCreate({
                        paranoid: false,
                        where: { vcfirmId: fund.vcfirmId, signatoryId: user.id, type: 'SecondaryGP' },
                        defaults: {
                            vcfirmId: fund.vcfirmId,
                            signatoryId: user.id,
                            type: 'SecondaryGP',
                            createdBy: req.userId,
                            updatedBy: req.userId
                        }
                    }).spread((vcFirmSignatories, created) => vcFirmSignatories.restore()),

                ]) //promise end
            })
        }).then(function (result) {
            const user = result[0];

        //update default userid in accounts table
        if(accounttype=='new')
            models.Account.update({defaultUserId:user.id},{where:{id:accountId}})                    

            //send email here
            emailHelper.sendEmail({
                toAddress: account.email,
                subject: messageHelper.welcomeEmailSubject,
                data: {
                    name: commonHelper.getFullName(user),
                    email: account.email,
                    passwordText: password,
                    fundLegalEntityName: fund.legalEntity,
                    gpName: commonHelper.getFullName(gpData),
                    loginLink: config.get('clientURL'),
                    user: account
                },
                htmlPath: "gpSignatoryAccountDetails.html"
            });

            return res.json({
                message: messageHelper.signatoryAddSuccessMessage,
            });

        }).catch(function (err) {
            return next(err)
        });
    } catch (error) {
        next(error);
    }
}

const addLpSignatory = async (req, res, next) => {
    try {

        const { fundId, lpId, firstName, lastName,middleName, email, cellNumber } = req.body

        const password = commonHelper.randomString(7);
        const salt = bcrypt.genSaltSync(10);

        const passwordHash = bcrypt.hashSync(password, salt);

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        });

        const lpData = await models.User.findOne({ where: { id: lpId } })

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        // check total no. of signatoties to be added are 3
        const signatoriesCount = await models.LpSignatories.count({ where: { fundId: fundId } })
        if (signatoriesCount >= 3) {
            return errorHelper.error_400(res, 'fundId', messageHelper.numberOfSignatoriesCheck);
        }


        //create/get account details                       
        let account = {}     
        let accounttype = 'old' 
        account = await models.Account.findOne({
            where: { email: email }
        })       
        if(!account){
            account = await models.Account.create({
                email:email,
                firstName,
                lastName,
                middleName,
                isEmailConfirmed:1,
                cellNumber:cellNumber,                
                password: passwordHash
            })
            accounttype = 'new'
        } else {
            await models.Account.update({                 
                firstName,
                lastName,
                middleName,
                cellNumber
            },{where:{id:account.id}})
        }        
        let accountId = account.id
        //--account details

        return models.sequelize.transaction(function (t) {

            // create user
            return models.User.findOrCreate({
                where: { email: email, accountType: 'SecondaryLP' }, defaults: {
                    firstName,
                    lastName,
                    email,
                    middleName,
                    isEmailConfirmed: 1,
                    accountType: 'SecondaryLP',
                    cellNumber: cellNumber,
                    accountId,
                    isEmailConfirmed:account.isEmailConfirmed
                }
            }).spread((user, created) => {

                return Promise.all([
                    user,
                    // assign role
                    models.UserRole.findOrCreate({
                        where: { roleId: 7, userId: user.id },
                        defaults: { roleId: 7, userId: user.id }
                    }),

                    models.FundSubscription.findOrCreate({
                        paranoid: false,
                        where: { lpId: lpId, fundId: fundId },
                        defaults: {
                            lpId: lpId, fundId: fundId, status: 1,
                            createdBy: req.userId, updatedBy: req.userId
                        }
                    }).spread((fundSubscription, created) => fundSubscription.status == 10 ? models.FundSubscription.create({
                        lpId: lpId, fundId: fundId, status: 1,
                        createdBy: req.userId, updatedBy: req.userId
                    }) : fundSubscription.restore()),

                    // assign fund to created lpdelegate
                    models.LpSignatories.findOrCreate({
                        paranoid: false,
                        where: { fundId: fundId, lpId: lpId, signatoryId: user.id },
                        defaults: {
                            fundId: fundId,
                            signatoryId: user.id,
                            lpId: lpId,
                            createdBy: req.userId,
                            updatedBy: req.userId
                        }
                    }).spread((lpSignatories, created) => lpSignatories.restore()),

                    models.VcFirmSignatories.findOrCreate({
                        paranoid: false,
                        where: { vcfirmId: lpId, signatoryId: user.id, type: 'SecondaryLP' },
                        defaults: {
                            vcfirmId: lpId,
                            signatoryId: user.id,
                            type: 'SecondaryLP',
                            createdBy: req.userId,
                            updatedBy: req.userId
                        }
                    }).spread((vcFirmSignatories, created) => vcFirmSignatories.restore()),

                ]) //promise end
            })
        }).then(function (result) {

            //
            const user = result[0];
            const subscriptionResult = result[2];

            models.LpSignatories.update({
                subscriptionId: subscriptionResult.id,
            }, {
                    where: {
                        fundId: fundId,
                        signatoryId: user.id,
                        lpId: lpId,
                    }
                });

            //update default userid in accounts table
            if(accounttype=='new')
                models.Account.update({defaultUserId:user.id},{where:{id:accountId}})

            //send email here
            emailHelper.sendEmail({
                toAddress: account.email,
                subject: messageHelper.welcomeEmailSubject,
                data: {
                    name: commonHelper.getFullName(user),
                    email: account.email,
                    passwordText: password,
                    fundLegalEntityName: fund.legalEntity,
                    lpName: commonHelper.getFullName(lpData),
                    loginLink: config.get('clientURL'),
                    user:account
                },
                htmlPath: "lpSignatoryAccountDetails.html"
            });

            return res.json({
                message: messageHelper.signatoryAddSuccessMessage
            });

        }).catch(function (err) {
            return next(err)
        });
    } catch (error) {
        next(error);
    }
}

const listLps = (req, res, next) => {

    try {

        models.User.findAll({
            attributes: ['id', 'firstName', 'lastName'],
            where: {
                accountType: 'LP'
            }
        }).then(data => res.json(data)).catch(error => res.json(error))

    } catch (error) {
        next(error);
    }
}

async function deleteGpDelegate(id) {

    // remove GP delegates belongs to all funds
    await models.GpDelegates.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z'),
    }, {
            where: {
                delegateId: id
            }
        });

    await models.VcFrimDelegate.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                delegateId: id
            }
        });

    // remove the assigned role
    await models.UserRole.destroy({
        where: {
            userId: id,
            roleId: 3
        }
    });

    // remove the user
    await models.User.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                id: id
            }
        });
}

async function deleteLp(id) {

    await models.VcFrimLp.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                lpId: id
            }
        });

    // get subscriptions
    const subscriptions = await models.FundSubscription.findAll({
        attributes: ['id'],
        where: {
            lpId: id
        }
    });

    let subscriptionIds = [];
    if (subscriptions != null) {
        subscriptionIds = _.map(subscriptions, 'id');
    }

    await models.ChangeCommitment.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                 subscriptionId: {
                     [Op.in] : subscriptionIds
                 },
                isActive: 1
            }
        });

    await models.DocumentsForSignature.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                subscriptionId: subscriptionIds,
                isActive: 1
            }
        });

    await models.FundClosings.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                subscriptionId: subscriptionIds,
                isActive: 1
            }
        });

    await models.FundSubscription.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                id: {
                    [Op.in] : subscriptionIds
                },
            }
        });

    // remove the assigned role
    await models.UserRole.destroy({
        where: {
            userId: id,
            roleId: 4
        }
    });

    // remove lp delegates for the deleted lp
    await models.LpDelegate.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                lpId: id
            }
        });

    // remove the user
    await models.User.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                id: id
            }
        });
}

async function deleteLpDelegate(id) {

    // remove LP delegates belongs to all funds
    await models.LpDelegate.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                delegateId: id
            }
        });

    await models.VcFrimDelegate.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                delegateId: id
            }
        });

    // remove the assigned role
    await models.UserRole.destroy({
        where: {
            userId: id,
            roleId: 5
        }
    });

    // remove the user
    await models.User.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                id: id
            }
        });
}

async function deleteGpSignatory(id) {

    // remove GP signatories belongs to all funds
    await models.GpSignatories.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z'),
    }, {
            where: {
                signatoryId: id
            }
        });

    await models.VcFirmSignatories.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                signatoryId: id
            }
        });

    // remove the assigned role
    await models.UserRole.destroy({
        where: {
            userId: id,
            roleId: 6
        }
    });

    // remove the user
    await models.User.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                id: id
            }
        });
}

async function deleteLpSignatory(id) {

    // remove LP signatories belongs to all funds
    await models.LpSignatories.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z'),
    }, {
            where: {
                signatoryId: id
            }
        });

    await models.VcFirmSignatories.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                signatoryId: id
            }
        });

    // remove the assigned role
    await models.UserRole.destroy({
        where: {
            userId: id,
            roleId: 7
        }
    });

    // remove the user
    await models.User.update({
        deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
    }, {
            where: {
                id: id
            }
        });
}

const deleteUser = async (req, res, next) => {
    try {
        const { id } = req.body

        const user = await models.User.findOne({
            where: {
                id: id
            }
        });

        if (user.accountType == 'GPDelegate') {
            deleteGpDelegate(id);
        } else if (user.accountType == 'LP') {
            deleteLp(id);
        } else if (user.accountType == 'LPDelegate') {
            deleteLpDelegate(id);
        } else if (user.accountType == 'SecondaryGP') {
            deleteGpSignatory(id);
        } else if (user.accountType == 'SecondaryLP') {
            deleteLpSignatory(id);
        }

        return res.json({
            message: messageHelper.deleteMessage
        });

    } catch (error) {
        next(error);
    }
}

module.exports = {
    gpList,
    lpList,
    setNotificationSetting,
    listFunds,
    listLpsByFundId,
    addGpDelegate,
    addLp,
    addLpDelegate,
    addGpSignatory,
    addLpSignatory,
    listLps,
    deleteUser
}