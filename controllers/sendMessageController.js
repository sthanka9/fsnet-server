
const models = require('../models/index');
const emailHelper = require('../helpers/emailHelper');

const sendMessage = async (req, res, next) => {

    try {

        const { message, toUserID } = req.body

        const toUser = await models.User.findOne({ where: { id: toUserID } });
        const fromUser = await models.User.findOne({ where: { id: req.userId } });

        await models.SendMessage.create({
            message: message,
            toUserId: toUser.id,
            formUserId: fromUser.id,
            createdBy: req.userId,
            updatedBy: req.userId,
        })

        emailHelper.sendEmail({
            toAddress: toUser.email,
            subject: `Email Notification - Vanilla`,
            data: {
                message: message,
                toUser: toUser,
                fromUser: fromUser,
                user:toUser
            },
            htmlPath: "sendMessage.html"
        });

        return res.json({
            error: false,
            message: 'Message has been sent.'
        });

    } catch (e) {
        next(e);
    }
}



module.exports = {
    sendMessage
}