const models = require('../models/index');
const messageHelper = require('../helpers/messageHelper');
const errorHelper = require('../helpers/errorHelper');

module.exports = async (req, res, next) => {

    try {

        const { vcfirmId, fundId } = req.params


        if (!Number(fundId)) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }   

        if (!Number(vcfirmId)) {
            return errorHelper.error_400(res, 'vcfirmId', messageHelper.vcfirmIdValidate);
        }   

        const vcFirm = await models.VCFirm.findOne({
            where: {
                id: vcfirmId
            }
        })

        if (!vcFirm) {
            return errorHelper.error_400(res, 'vcfirmId', messageHelper.vcfirmNotFound);
        }

        let gpDelegates = await models.VcFrimDelegate.findAll({
            where: {
                vcfirmId: vcfirmId,
                type: 'GP'
            },
            include: [{
                model: models.User,
                as: 'details',
                required: true
            },{
                model: models.GpDelegates,
                as: 'fund'
            }]
        })

        gpDelegates = gpDelegates.map(data => {
            return {
                firstName: data.details.firstName,
                lastName: data.details.lastName,
                middleName: data.details.middleName,
                email: data.details.email,
                id: data.details.id,
                profilePic: data.details.profilePic,
                selected: data.fund.fundId == fundId
            };
        });

        return res.json({ data: gpDelegates })

    } catch (e) {
        next(e)
    }

}
