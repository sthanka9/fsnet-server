const models = require('../models/index');
const _ = require('lodash')
const path = require('path');
const { Op } = require('sequelize');
const moment = require('moment');
const fs = require('fs');
const commonHelper = require('../helpers/commonHelper');
const SAPDFGeneratorBySubscriptionIDHelper = require("../helpers/SAPDFGeneratorBySubscriptionIDHelper");
const signaturePageHelper = require('../helpers/signaturePageHelper');
const messageHelper = require('../helpers/messageHelper');
const errorHelper = require('../helpers/errorHelper');

const getFundAgreement = async (req, res, next) => {

    try {

        const accountType = req.user.accountType;
        const subscriptionId  = req.params.subscriptionId; 
   

        let agreementSignData={};
        let htmlFilePath; 
        let subscription;
        
        subscription = await models.FundSubscription.getSubscription(subscriptionId, models);
        //for offline
        if(!subscription){
            console.log("i am offline lp FA")
            subscription = await models.FundSubscription.getOfflineSubscription(subscriptionId, models);                
        }

        let whereFilter={
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            isArchived:0,
            docType:'FUND_AGREEMENT'
        };


        // fetching timezone form fund table
        const fundSubscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }],
            }]
        })
        //making date with timezone of a fund
        let date_for_sign = await commonHelper.getDate();
        date_for_sign = commonHelper.getDateTimeWithTimezone(fundSubscription.fund.timeZone, date_for_sign, 0, 0);

        
        //If Investors check lpsigncount
        if(accountType =='LP' || accountType =='SecondaryLP') {
            whereFilter.lpSignCount ={
                [Op.gte]:1
            };

            //Data to be rendered in HTML - Lp Signature blocks
            agreementSignData = {
                jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                lpName: commonHelper.getFullName(req.user),
                areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                date: date_for_sign,
                lpSignaturePic: '',
                lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
                // name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
                name: ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) || 
                commonHelper.getFullName(subscription.lp))
            }

            htmlFilePath = "./views/fundAgreementLpSign.html";
            
        }else{
        
            //Data to be rendered in HTML - Gp Signature blocks
            agreementSignData = {
                gpName: commonHelper.getFullName(req.user),
                fundManagerTitle: subscription.fund.fundManagerTitle,
                fundManagerCommonName: subscription.fund.fundManagerCommonName,
                fundCommonName: subscription.fund.fundCommonName,
                fundLegalEntity: subscription.fund.legalEntity,
                fundManagerLegalEntityName: subscription.fund.fundManagerLegalEntityName,
                gpSignaturePic: '',
                date: date_for_sign
            };

            htmlFilePath = "./views/fundAgreementGpSign.html";
        }

        let document = await models.DocumentsForSignature.findOne({
            where: whereFilter,
            include: [{
                model: models.SignatureTrack,
                as: 'signatureTrack',
                required: false,
                where: {
                    signedUserId: req.userId,
                    isActive:1,
                    documentType: 'FUND_AGREEMENT'
                }
            }]
        });
        

        const fundAgreement = document && document.filePath ? document.filePath :  subscription.fund.partnershipDocument.path;
        
        const fundAgreementPath = (accountType !=='LPDelegate' && accountType !=='GPDelegate') ? ((document && (document.signatureTrack || document.lpSignedDate)) ? { path :path.join(__dirname.replace('controllers',''),document.filePath) } : await signaturePageHelper.agreementSign('FA', fundAgreement, agreementSignData, htmlFilePath)) : { path: path.join(__dirname.replace('controllers',''),fundAgreement) };

        return res.sendFile(fundAgreementPath.path);

    } catch (e) {
        next(e);
    }


}


const getAmendmentFundAgreement = async (req, res, next) => {
    try {
        const accountType = req.user.accountType;
        const subscriptionId  = req.params.subscriptionId; 
        const documentId  = req.params.documentId;   
        
        if (!documentId || !subscriptionId) {
            return errorHelper.error_403(res, 'documentId', messageHelper.noDataFound);
        }        

        let agreementSignData={};
        let htmlFilePath; 
        let subscription;
        
        subscription = await models.FundSubscription.getSubscription(subscriptionId, models);
        //for offline
        if(!subscription){
            console.log("i am offline lp FA")
            subscription = await models.FundSubscription.getOfflineSubscription(subscriptionId, models);                
        }

        let whereFilter={
             id:documentId
        };

        // getting fund timeZone
        const FundSubscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            },
            include: [{
                attributes: ['timezone'],
                model: models.Fund,
                as: 'fund',
                required: false,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }]
            }]
        });
        
        //If Investors check lpsigncount
        if(accountType =='LP' || accountType =='SecondaryLP') {

            let date_for_sign = await commonHelper.getMomentUTCDate();
            date_for_sign = commonHelper.getDateTimeWithTimezone(FundSubscription.fund.timeZone, date_for_sign, 0, 0);

            //Data to be rendered in HTML - Lp Signature blocks
            agreementSignData = {
                jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                lpName: commonHelper.getFullName(req.user),
                areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                date: date_for_sign,
                lpSignaturePic: '',
                lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
                // name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
                name: ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) || 
                commonHelper.getFullName(subscription.lp))
            }

            htmlFilePath = "./views/fundAgreementLpSign.html";
            
        } 

        let document = await models.DocumentsForSignature.findOne({
            where: whereFilter          
        })
        
        let ammendementTempPath = document.filePath 
 
         let ammendmentPath = await signaturePageHelper.agreementSign('FA', ammendementTempPath, agreementSignData, htmlFilePath)         
 
         return res.sendFile(ammendmentPath.path) 

    } catch (e) {
        next(e);
    }


}

const getSubscriptionAgreement = async (req, res, next) => {
    try {

        const accountType = req.user.accountType;
        const subscriptionId  = req.params.subscriptionId; 
        let subscription;
        subscription = await models.FundSubscription.getSubscription(subscriptionId, models);

        //for offline
        if(!subscription){
            console.log("i am offline lp SA")
            subscription = await models.FundSubscription.getOfflineSubscription(subscriptionId, models);     
        }        
        let subscriptionAgreementUrl = null;
        let agreementSignData={};
        let htmlFilePath;

        let whereFilter={
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            docType:'SUBSCRIPTION_AGREEMENT'
            
        };

        // fetching timezone form fund table
        const fundSubscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }],
            }]
        })

        //making date with timezone of a fund
        let date_for_sign = await commonHelper.getDateTime();
        date_for_sign = commonHelper.getDateTimeWithTimezone(fundSubscription.fund.timeZone, date_for_sign, 0, 0);


        //If Investors check lpsigncount
        if(accountType =='LP' || accountType =='SecondaryLP' ){
            whereFilter.lpSignCount ={
                [Op.gte]:1
            };

            //Data to be rendered in HTML - Lp Signature blocks
            agreementSignData = {
                jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                lpName: commonHelper.getFullName(req.user),
                areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                date: date_for_sign,
                lpSignaturePic: '',
                lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
                // name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
                name: ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) || 
                commonHelper.getFullName(subscription.lp))
            }

        
            htmlFilePath = "./views/subscriptionAgreementLpSign.html";
            
        }else{

            //Data to be rendered in HTML - Gp Signature blocks
            agreementSignData = {
                gpName: commonHelper.getFullName(req.user),
                fundManagerTitle: subscription.fund.fundManagerTitle,
                fundManagerCommonName: subscription.fund.fundManagerCommonName,
                fundCommonName: subscription.fund.fundCommonName,
                fundLegalEntity: subscription.fund.legalEntity,
                fundManagerLegalEntityName: subscription.fund.fundManagerLegalEntityName,
                gpSignaturePic: '',
                date: date_for_sign,
                acceptedOn: date_for_sign,
                fundEntityType: (subscription.fund.fundEntityType==1) ? 'Limited Partnership' : (subscription.fund.fundEntityType==2) ? 'Limited Liability company' : 'Type of Equity Holder (Limited Partner, Member),'
            };

            htmlFilePath = "./views/subscriptionAgreementGpSign.html";
        }
        
        let document = await models.DocumentsForSignature.findOne({
            where: whereFilter,
            include: [{
                model: models.SignatureTrack,
                as: 'signatureTrack',
                required: false,
                where: {
                    signedUserId: req.userId,
                    isActive:1,
                    documentType: 'SUBSCRIPTION_AGREEMENT'
                }
            }]
        });

        let subscriptionAgreement;
        //
        if(document && document.filePath) {
            subscriptionAgreement =  document.filePath;
        } else {
            const tempSubscriptionFile = await SAPDFGeneratorBySubscriptionIDHelper(subscription);
            subscriptionAgreement = tempSubscriptionFile.path;
        }
        
        
        const subscriptionAgreementPath = (accountType !=='LPDelegate' && accountType !=='GPDelegate') ? ( (document && document.signatureTrack) 
        ? { path :path.join(__dirname.replace('controllers',''),document.filePath) } 
        : await signaturePageHelper.agreementSign('SA',subscriptionAgreement, agreementSignData, htmlFilePath)): { path: path.join(__dirname.replace('controllers',''),subscriptionAgreement) };
       
        return res.sendFile(subscriptionAgreementPath.path);

    } catch (e) {
        next(e);
    }

}

//get side letter agreement
const getSideLetterAgreement = async (req, res, next) => {
    try {         
        const accountType = req.user.accountType;
        const subscriptionId = req.params.subscriptionId;

        let subscription = await models.FundSubscription.getSubscription(subscriptionId, models);
        let sideLetterUrl = null;
        let agreementSignData={};
        let htmlFilePath;

        let whereFilter={
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            docType:'SIDE_LETTER_AGREEMENT'
            
        };


        // fetching timezone form fund table
        const fundSubscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }],
            }]
        })
        //making date with timezone of a fund
        let date_for_sign = await commonHelper.getDate();
        date_for_sign = commonHelper.getDateTimeWithTimezone(fundSubscription.fund.timeZone, date_for_sign, 0, 0);


        //If Investors check lpsigncount
        if(accountType =='LP' || accountType =='SecondaryLP' || accountType =='LPDelegate'){            

            //Data to be rendered in HTML - Lp Signature blocks
            agreementSignData = {
                jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                lpName: commonHelper.getFullName(req.user),
                areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                date: date_for_sign,
                lpSignaturePic: '',
                lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
                // name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
                name: ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) || 
                commonHelper.getFullName(subscription.lp))
            }

            htmlFilePath = "./views/sideletterAgreementLPSign.html";
            
        }else{
            /*
            whereFilter.gpSignCount ={
                [Op.gte]:1
            };*/
            //Data to be rendered in HTML - Gp Signature blocks
            agreementSignData = {
                gpName: commonHelper.getFullName(req.user),
                fundManagerTitle: subscription.fund.fundManagerTitle,
                fundManagerCommonName: subscription.fund.fundManagerCommonName,
                fundCommonName: subscription.fund.fundCommonName,
                fundLegalEntity: subscription.fund.legalEntity,
                fundManagerLegalEntityName: subscription.fund.fundManagerLegalEntityName,
                gpSignaturePic: '',
                date: date_for_sign
            };

            htmlFilePath = "./views/sideletterAgreementGPSign.html";
        }
   
        
        let documentsForSignature = await models.DocumentsForSignature.findOne({
            where: whereFilter
        });

        if(!documentsForSignature){
            return errorHelper.error_403(res, 'documentId', messageHelper.noDataFound);
        }

        const SIDE_LETTER_AGREEMENT = _.find(documentsForSignature, {
            docType: 'SIDE_LETTER_AGREEMENT'
        });   
        
        const currentUserSigned = await models.SignatureTrack.findOne({
            where:{
                signedUserId : req.userId,
                isActive:1,
                documentType: 'SIDE_LETTER_AGREEMENT',
                documentId:documentsForSignature.id
            }
        });

        if(!currentUserSigned && accountType !=='lpdelegate' && accountType !=='gpdelegate'){          
            if(SIDE_LETTER_AGREEMENT) {
                const SLTempPath = await signaturePageHelper.agreementSign('SL',SIDE_LETTER_AGREEMENT.filePath,agreementSignData,htmlFilePath);
                console.log("*******i am in if**************",SLTempPath.path)
                sideLetterUrl = SLTempPath.path
            }

        }else{
            if(SIDE_LETTER_AGREEMENT) {                
                sideLetterUrl = path.join(__dirname.replace('controllers',''),SIDE_LETTER_AGREEMENT.filePath)                
                console.log("****************i am in else**************",sideLetterUrl)
            }
        }

        console.log("*************sideLetterUrl********",sideLetterUrl)
        let ifFileExits = fs.existsSync(sideLetterUrl)
        if(ifFileExits)
            return res.sendFile(sideLetterUrl);

    } catch (e) {
        next(e);
    }
}



const getChangeCommitmentAgreement = async (req, res, next) => {

    try {

        const accountType = req.user.accountType;
        const subscriptionId = req.params.subscriptionId;

        let subscription = await models.FundSubscription.getSubscription(subscriptionId, models);
        let changeCommitmentUrl = '';
        let agreementSignData={};
        let htmlFilePath;


        let whereFilter={
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            docType: 'CHANGE_COMMITMENT_AGREEMENT'
            
        };


        // fetching timezone form fund table
        const fundSubscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }],
            }]
        })
        //making date with timezone of a fund
        let date_for_sign = await commonHelper.getDate();
        date_for_sign = commonHelper.getDateTimeWithTimezone(fundSubscription.fund.timeZone, date_for_sign, 0, 0);


        //If Investors check lpsigncount
        if(accountType =='LP' || accountType =='SecondaryLP' || accountType =='LPDelegate'){
            //Data to be rendered in HTML - Lp Signature blocks
            agreementSignData = {
                jointIndividualTitlePrettyForPDF:subscription.jointIndividualTitlePrettyForPDF,
                investorType:subscription.investorType == 'LLC'?'ENTITY': _.toUpper(subscription.investorType),
                legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                lpName: commonHelper.getFullName(req.user),
                areYouSubscribingAsJointIndividual:subscription.areYouSubscribingAsJointIndividual,
                date: date_for_sign,
                lpSignaturePic:'',
                lpCapitalCommitment:subscription.lpCapitalCommitmentPretty,
                // name:subscription.investorType == 'LLC' ?subscription.entityName:subscription.trustName
                name: ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) || 
                commonHelper.getFullName(subscription.lp))
            }

            htmlFilePath = "./views/changeCommitmentAgreementLpSign.html";
            
        }else{
            //Data to be rendered in HTML - Gp Signature blocks
            agreementSignData = {
                gpName: commonHelper.getFullName(req.user),
                date: date_for_sign,
                gpSignaturePic:'',
                fundManagerTitle: subscription.fund.fundManagerCommonName,
                fundCommonName: subscription.fund.fundCommonName,
                fundLegalEntity: subscription.fund.legalEntity,
                fundManagerLegalEntityName: subscription.fund.fundManagerLegalEntityName,
            }

            htmlFilePath = "./views/changeCommitmentAgreementGPSign.html";
        }
   
        
        let document = await models.DocumentsForSignature.findOne({
            where: whereFilter,
            // include: [{
            //     model: models.ChangeCommitment,
            //     as: 'changeCommitment',
            //     required: true
            // }]
            include: [{
                model: models.SignatureTrack,
                as: 'signatureTrack',
                required: false,
                where: {
                    signedUserId: req.userId,
                    isActive:1,
                    documentType: 'CHANGE_COMMITMENT_AGREEMENT'
                }
            }]
        }); 
    

        if(document && document.signatureTrack) { 
            changeCommitmentUrl = commonHelper.getLocalUrl(document.filePath)
        }else{
            changeCommitmentUrl = await signaturePageHelper.agreementSign('CC',document.filePath,agreementSignData,htmlFilePath);            
            changeCommitmentUrl = changeCommitmentUrl.url;
        }      

        return res.send({filePathTempPublicUrl:changeCommitmentUrl});

    } catch (e) {
        next(e);
    }
}


const getChangeCommitmentAgreementFile = async (req, res, next) => {

    try {

        const accountType = req.user.accountType;
        const subscriptionId = req.params.subscriptionId;

        let subscription = await models.FundSubscription.getSubscription(subscriptionId, models);
        let changeCommitmentUrl = '';
        let agreementSignData={};
        let htmlFilePath;


        let whereFilter={
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            docType: 'CHANGE_COMMITMENT_AGREEMENT'
            
        };

        // fetching timezone form fund table
        const fundSubscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }],
            }]
        })
        //making date with timezone of a fund
        let date_for_sign = await commonHelper.getDate();
        date_for_sign = commonHelper.getDateTimeWithTimezone(fundSubscription.fund.timeZone, date_for_sign, 0, 0);

        //If Investors check lpsigncount
        if(accountType =='LP' || accountType =='SecondaryLP' || accountType =='LPDelegate'){
            //Data to be rendered in HTML - Lp Signature blocks
            agreementSignData = {
                jointIndividualTitlePrettyForPDF:subscription.jointIndividualTitlePrettyForPDF,
                investorType:subscription.investorType == 'LLC'?'ENTITY': _.toUpper(subscription.investorType),
                legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                lpName: commonHelper.getFullName(req.user),
                areYouSubscribingAsJointIndividual:subscription.areYouSubscribingAsJointIndividual,
                date: date_for_sign,
                lpSignaturePic:'',
                lpCapitalCommitment:subscription.lpCapitalCommitmentPretty,
                // name:subscription.investorType == 'LLC' ?subscription.entityName:subscription.trustName
                name: ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) || 
                commonHelper.getFullName(subscription.lp))
            }

            htmlFilePath = "./views/changeCommitmentAgreementLpSign.html";
            
        }else{
            //Data to be rendered in HTML - Gp Signature blocks
            agreementSignData = {
                gpName: commonHelper.getFullName(req.user),
                date: date_for_sign,
                gpSignaturePic:'',
                fundManagerTitle: subscription.fund.fundManagerCommonName,
                fundCommonName: subscription.fund.fundCommonName,
                fundLegalEntity: subscription.fund.legalEntity,
                fundManagerLegalEntityName: subscription.fund.fundManagerLegalEntityName,
            }

            htmlFilePath = "./views/changeCommitmentAgreementGPSign.html";
        }
   
        
        let document = await models.DocumentsForSignature.findOne({
            where: whereFilter,    
            include: [{
                model: models.SignatureTrack,
                as: 'signatureTrack',
                required: false,
                where: {
                    signedUserId: req.userId,
                    isActive:1,
                    documentType: 'CHANGE_COMMITMENT_AGREEMENT'
                }
            }]
        }); 
    
        if (!document) {
            return errorHelper.error_403(res, 'documentId', messageHelper.noDataFound);
        }      
      
        changeCommitmentUrl = await signaturePageHelper.agreementSign('CC',document.filePath,agreementSignData,htmlFilePath)   

        return res.sendFile(changeCommitmentUrl.path)

    } catch (e) {
        next(e);
    }
}


const getGPChangeCommitmentAgreement = async (req, res, next) => {

    try {
 
        const subscriptionId = req.params.subscriptionId;

        if (!subscriptionId) {
            return errorHelper.error_403(res, 'documentId', messageHelper.noDataFound);
        }        

        let subscription = await models.FundSubscription.getSubscription(subscriptionId, models);  

        //for offline
        if(!subscription){
            console.log("i am offline lp CC")
            subscription = await models.FundSubscription.getOfflineSubscription(subscriptionId, models);                
        }        

        let whereFilter={
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            docType: 'CHANGE_COMMITMENT_AGREEMENT'
            
        }

        // fetching timezone form fund table
        const fundSubscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }],
            }]
        })
        //making date with timezone of a fund
        let date_for_sign = await commonHelper.getDate();
        date_for_sign = commonHelper.getDateTimeWithTimezone(fundSubscription.fund.timeZone, date_for_sign, 0, 0);

        //Data to be rendered in HTML - Gp Signature blocks
        agreementSignData = {
            gpName: commonHelper.getFullName(req.user),
            date: date_for_sign,
            gpSignaturePic:'',
            fundManagerTitle: subscription.fund.fundManagerCommonName,
            fundCommonName: subscription.fund.fundCommonName,
            fundLegalEntity: subscription.fund.legalEntity,
            fundManagerLegalEntityName: subscription.fund.fundManagerLegalEntityName,
        }

        let htmlFilePath = "./views/changeCommitmentAgreementGPSign.html";
        
        let document = await models.DocumentsForSignature.findOne({
            where: whereFilter
        })

        if (!document) {
            return errorHelper.error_403(res, 'documentId', messageHelper.noDataFound);
        }                
        let changeCommitmentUrl = await signaturePageHelper.agreementSign('CC',document.filePath,agreementSignData,htmlFilePath)
        
        return res.sendFile(changeCommitmentUrl.path);

    } catch (e) {
        next(e);
    }
}



const viewChangeCommitmentAgreement = async (req, res, next) => {
    try {
     
        const subscriptionId = req.params.subscriptionId; 

        let whereFilter={
            subscriptionId: subscriptionId,            
            isActive: 1,
            docType: 'CHANGE_COMMITMENT_AGREEMENT'            
        }   
        
        let document = await models.DocumentsForSignature.findOne({
            attributes:['filePath'],
            where: whereFilter 
        })  

        let docPath = path.join(__dirname.replace('controllers',''),document.filePath)

        return res.sendFile(docPath)

    } catch (e) {
        next(e);
    }
}


const viewInActiveSideLetterAgreement = async (req, res, next) => {
    try {
        const { id } = req.params;       
 
        //get document path
        let documentsForSignature = await models.DocumentsForSignature.findOne({
            where: {
                id: id,
                docType:'SIDE_LETTER_AGREEMENT',
                 
            },
            include: [{
                model: models.SideLetter,
                as: 'changeSideletter',
                required: true
            }]
        })         

        const outputFile = path.resolve(__dirname,'../'+documentsForSignature.filePath);           

        return res.sendFile(outputFile);

    } catch (e) {
        next(e);
    }
}

//get side letter agreement
const viewSideLetterAgreement = async (req, res, next) => {
    try {
        const { subscriptionId } = req.params;       
 
        //get document path
        let documentsForSignature = await models.DocumentsForSignature.findOne({
            where: {
                subscriptionId: subscriptionId,
                docType:'SIDE_LETTER_AGREEMENT',
                isActive:1
            },
            include: [{
                model: models.SideLetter,
                as: 'changeSideletter',
                required: true
            }]
        });              

        const outputFile = path.resolve(__dirname,'../'+documentsForSignature.filePath);           

        return res.sendFile(outputFile);

    } catch (e) {
        next(e);
    }
}

//get consolidatd fund agreement
const getConsolidatedFundAgreement = async (req, res, next) => {
    try {
       
        const fundId  = req.params.fundId;     

        let whereFilter={
            fundId: fundId,
            docType:'CONSOLIDATED_FUND_AGREEMENT',
            isArchived:0,
            isActive:1
        }; 

        let document = await models.DocumentsForSignature.findOne({
            where: whereFilter 
        }) 
        
        let docPath = path.join(__dirname.replace('controllers',''),document.filePath)

        return res.sendFile(docPath);

    } catch (e) {
        next(e);
    }


}


//get changed pages
const getChangedPagesAgreement = async (req, res, next) => {
    try {
       
        const fundId  = req.params.fundId;     

        let document = await models.ChangedPage.findOne({
            attributes:['documentPath'],
            where: {             
                fundId: fundId,
                isActive: 1
            }
        })
        
        let docPath = path.join(__dirname.replace('controllers',''),document.documentPath)

        return res.sendFile(docPath);

    } catch (e) {
        next(e);
    }
}


//view changed pages
const viewChangedPagesAgreement = async (req, res, next) => {
    try {
       
        const id  = req.params.id 

        let document = await models.ChangedPage.findOne({
            attributes:['documentPath'],
            where: {             
                id: id
            }
        })
        
        let docPath = path.join(__dirname.replace('controllers',''),document.documentPath)

        return res.sendFile(docPath);

    } catch (e) {
        next(e);
    }
}


const getDocumentPath = async (req,res,next)=>{

    try{

        const documentId = req.params.documentId;

        let whereFilter={
            id: documentId
        }; 

        let document = await models.DocumentsForSignature.findOne({
            where: whereFilter 
        }) 
        
        let docPath = path.join(__dirname.replace('controllers',''),document.filePath)

        return res.sendFile(docPath);

    } catch (e) {
        next(e);
    }
    

}

//get dfs ammendment
const getDFSAmmendmentPath = async (req,res,next)=>{
    try{
        const documentId = req.params.documentId;
        const accountType = req.user.accountType;

        let whereFilter={
            id: documentId
        } 

        let document = await models.DocumentsForSignature.findOne({
            attributes:['subscriptionId','filePath'],
            where: whereFilter 
        }) 

        const subscriptionId = document.subscriptionId;

        let subscription = await models.FundSubscription.getSubscription(subscriptionId, models)    
        let agreementSignData={}
        let htmlFilePath  

        
        //If Investors check lpsigncount
        if(accountType =='LP' || accountType =='SecondaryLP' || accountType =='LPDelegate'){            

            //Data to be rendered in HTML - Lp Signature blocks
            agreementSignData = {
                jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                lpName: commonHelper.getFullName(req.user),
                areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                date: moment().format('M/D/YYYY'),
                lpSignaturePic: '',
                lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
                // name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
                name: ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) || 
                commonHelper.getFullName(subscription.lp))
            }

            htmlFilePath = "./views/fundAgreementLpSign.html";
            
        } 

        let ammendementTempPath = path.join(__dirname.replace('controllers',''),document.filePath)

        let ammendmentPath = await signaturePageHelper.agreementSign('AA', ammendementTempPath, agreementSignData, htmlFilePath)       

        return res.sendFile(ammendmentPath.path)

    } catch (e) {
        next(e);
    }    

}


//get dfs ammendment
const getAmmendmentPath = async (req,res,next)=>{
    try{
        const amendmentId = req.params.amendmentId
        const subscriptionId = req.params.subscriptionId
        const accountType = req.user.accountType

        let whereFilter={
            id: amendmentId
        } 

        let ammendmetDocument = await models.FundAmmendment.findOne({
            attributes:['document'],
            where: whereFilter 
        }) 

        let fileInfo = ammendmetDocument.document

        console.log("fileInfo**************",fileInfo.path)

        let subscription = await models.FundSubscription.getSubscription(subscriptionId, models)    
        let agreementSignData={}
        let htmlFilePath  
        
        //If Investors check lpsigncount
        if(accountType =='LP' || accountType =='SecondaryLP' || accountType =='LPDelegate'){            

            //Data to be rendered in HTML - Lp Signature blocks
            agreementSignData = {
                jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                lpName: commonHelper.getFullName(req.user),
                areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                date: moment().format('M/D/YYYY'),
                lpSignaturePic: '',
                lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
                // name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName
                name: ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) || 
                commonHelper.getFullName(subscription.lp))
            }

            htmlFilePath = "./views/fundAgreementLpSign.html";
            
        } 

        let ammendementTempPath = fileInfo.path

       // console.log("ammendementTempPath**************",ammendementTempPath)

        let ammendmentPath = await signaturePageHelper.agreementSign('AA', ammendementTempPath, agreementSignData, htmlFilePath)     
        
       // console.log("ammendmentPath**************",ammendmentPath)

        return res.sendFile(ammendmentPath.path)

    } catch (e) {
        next(e);
    }    

}

//get amendment file  
const displayAmendment = async (req,res,next)=>{
    try{
        const amendmentId = req.params.amendmentId   

        let amendment = await models.FundAmmendment.findOne({
            attributes:['document'],
            where: {
                id:amendmentId
            }
        })

        let docPath = path.join(__dirname.replace('controllers',''),amendment.document.path)

        return res.sendFile(docPath)

    } catch (e) {
        next(e);
    }    

}

//displayFA
const displayFA = async (req,res,next)=>{
    try{
        const fundId = req.params.fundId   

        const fund = await models.Fund.findOne({
            attributes:['partnershipDocument'],
            where: {
                id: fundId
            }
        }) 
        let docPath = path.join(__dirname.replace('controllers',''),fund.partnershipDocument.path)

        return res.sendFile(docPath)

    } catch (e) {
        next(e);
    }    

}


 
const sectionView = async (req,res,next)=>{
    try{
        let fundId = req.params.fundId   
        let section = req.params.section   
        let filePath = ''
        let docPath =  ''

        let fund = await models.Fund.findOne({
            attributes:['subscriptionAgreementPath'],
            where: {
                id: fundId
            }
        }) 

        if(fund.subscriptionAgreementPath){
            filePath = (section == 'one') ? fund.subscriptionAgreementPath.one : (section == 'two') ? fund.subscriptionAgreementPath.two : ''
        }

        if(filePath!=''){
            docPath = path.join(__dirname.replace('controllers',''),filePath)
        }

        return res.sendFile(docPath)      

    } catch (e) {
        next(e);
    }    

}

module.exports = {
    getFundAgreement,
    getConsolidatedFundAgreement,
    getSubscriptionAgreement,
    getChangeCommitmentAgreement,
    getChangeCommitmentAgreementFile,
    getGPChangeCommitmentAgreement,
    getSideLetterAgreement,
    viewSideLetterAgreement,
    viewChangeCommitmentAgreement,
    viewInActiveSideLetterAgreement,
    getChangedPagesAgreement,
    viewChangedPagesAgreement,
    getDocumentPath,
    getDFSAmmendmentPath,
    getAmmendmentPath,
    getAmendmentFundAgreement,
    displayAmendment,
    displayFA,
    sectionView
}
