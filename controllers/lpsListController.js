const models = require('../models/index');
const _ = require('lodash');
const messageHelper = require('../helpers/messageHelper');
const errorHelper = require('../helpers/errorHelper');

module.exports = async (req, res, next) => {

    try {

        const { vcfirmId, orderBy = 'firstName', order = 'DESC', fundId } = req.params


        if (!Number(vcfirmId)) {
            return errorHelper.error_400(res, 'vcfirmId', messageHelper.vcfirmIdValidate);
        }  

        const vcFirm = await models.VCFirm.findOne({
            where: {
                id: vcfirmId
            }
        })



        if (!vcFirm) {
            return errorHelper.error_400(res, 'vcfirmId', messageHelper.vcfirmNotFound);
        }

        let lps = await models.User.findAll({
            order: [
                [orderBy, order],
            ],
            where: {
                accountType: 'LP'
            },
            include: [{
                model: models.VcFrimLp,
                as: 'lps',
                where: {
                    vcfirmId: vcfirmId,
                },
            }]
        })

        let fundSubscriptionsForLps = await models.FundSubscription.findAll({
            where:{
                fundId: fundId
            }
        });
        
         
        lps = lps.map(user => {

            const getSingleSubscriptionForLp = _.find(fundSubscriptionsForLps, {lpId: user.id}) 

            return {
                firstName: user.firstName,
                lastName: user.lastName,
                middleName: user.middleName,
                email: user.email,
                id: user.id,
                profilePic: user.profilePic,
                organizationName: _.get(getSingleSubscriptionForLp,'organizationName',_.get(user, 'organizationName', null)),
                selected: Boolean(getSingleSubscriptionForLp)
            };
        });

        return res.json({ data: lps })

    } catch (e) {
        next(e)
    }

}
