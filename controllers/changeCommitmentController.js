const models = require('../models/index');
const _ = require('lodash')
const config = require('../config/index');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const path = require('path');
const fs = require('fs');
const { literal } = require('sequelize');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const logger = require('../helpers/logger');
const eventHelper = require('../helpers/eventHelper');
const notificationHelper = require('../helpers/notificationHelper');

//To save the requested lp Commitment amounts
const saveLpCommitments = async (req, res, next) => {

    try {

        let { fundId, subscriptionId, amount, comments } = req.body;

        await models.ChangeCommitment.update({ isActive: 0 }, { where: { fundId, subscriptionId } })

        const changeCommitment = await models.ChangeCommitment.create({ fundId, subscriptionId, amount, comments, isActive: 1 })

        const subscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            },
            include: [{
                model: models.Country,
                as: 'mailingAddressCountryData',
                required: false,
            }, {
                model: models.State,
                as: 'mailingAddressStateData',
                required: false,
            }, {
                model: models.User,
                as: 'lp',
                required: true
            }]
        });


        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.GpDelegates,
                as: 'gpDelegatesSelected',
                required: false,
                where: {
                    fundId: fundId
                }
            }]
        })


        const previousClosingInfo = await models.FundClosings.findAll({
            where: {
                fundId: fundId,
                subscriptionId: subscriptionId
            },
            limit: 1,
            order: [['id', 'DESC']],
        });

        const gpConsumed = parseFloat(previousClosingInfo[0].gpConsumed)
        amount = parseFloat(amount)

        // const lp = await models.User.findOne({
        //     where: {
        //         id: subscription.lpId
        //     }
        // })

        const gp = await models.User.findOne({
            where: {
                id: fund.gpId
            },
            include: [{
                model: models.State,
                as: 'stateData',
                required: false,
            }, {
                model: models.Country,
                as: 'countryData',
                required: false,
            }]
        })

        let gpMailingAddressText = _.remove([gp.streetAddress1, gp.streetAddress2, gp.city, _.get(gp.stateData, 'name', null), _.get(gp.countryData, 'name', null), gp.zipcode], _.identity).join(', ');
        gpMailingAddressText = gpMailingAddressText ? gpMailingAddressText + '.' : '';
        // let gpMailingAddressPart1 = _.remove([gp.streetAddress1, gp.streetAddress2, gp.city], _.identity).join(', ');
        // let gpMailingAddressPart2 = _.remove([ _.get(gp.stateData, 'name', null), _.get(gp.countryData, 'name', null), gp.zipcode], _.identity).join(', ');
        // gpMailingAddressPart2 = gpMailingAddressPart2 ? gpMailingAddressPart2 + '.' : '';

        let investorMailingAddressPart1 = _.remove([subscription.mailingAddressStreet, subscription.mailingAddressCity], _.identity).join(', ');
        let investorMailingAddressPart2 = _.remove([_.get(subscription.mailingAddressStateData, 'name', null), _.get(subscription.mailingAddressCountryData, 'name', null), subscription.mailingAddressZip], _.identity).join(', ');
        investorMailingAddressPart2 = investorMailingAddressPart2 ? investorMailingAddressPart2 + '.' : '';

        let data = {
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            email: subscription.lp.email,
            legalEntity: fund.legalEntity,
            fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
            fundManagerTitle: fund.fundManagerTitle,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            lpSubscriptionName: subscription.name || subscription.trustName || subscription.entityName,
            legalTitleDesignation: subscription.legalTitleDesignation || subscription.entityTitle || subscription.name || "N/A",
            gpMailingAddress: gpMailingAddressText,
            date: moment().format('M/D/YYYY'),
            investorLegalEntityName: ((subscription.investorType == 'LLC' ? subscription.entityName :
            (subscription.investorType == 'Trust' ? subscription.trustName : null)) || 
            commonHelper.getFullName(subscription.lp)),
            lpCapitalCommitment: (gpConsumed).toLocaleString('en-US', {
                style: 'currency',
                currency: 'USD',
            }),
            newCapitalCommitment: parseFloat(amount).toLocaleString('en-US', {
                style: 'currency',
                currency: 'USD',
            }),
            gpName: commonHelper.getFullName(gp),
            lpName: commonHelper.getFullName(subscription.lp),
            increaseOfCapitalCommitment: (amount - gpConsumed).toLocaleString('en-US', {
                style: 'currency',
                currency: 'USD',
            }),
            // investorMailingAddress: investorMailingAddressText,
            // gpMailingAddressPart1,
            // gpMailingAddressPart2,
            investorMailingAddressPart1,
            investorMailingAddressPart2
        }


        await lpCommitmentChangePDFGenerateAndCreateEnvelopes(changeCommitment, data, req);

        logger.info('Cancel Capital Commitment : get lp signatories ')

        // get lp signatories
        let lpSignatoriesSelected = await models.LpSignatories.findAll({
            where: {
                fundId: fundId,
                lpId: subscription.lpId,
                subscriptionId: subscriptionId
            },
            include: [{
                model: models.User,
                as: 'signatoryDetails',
                required: false
            }]
        })

        let lpSignatories = [];
        lpSignatoriesSelected.map(i => {
            lpSignatories.push({
                id: i.signatoryDetails.id,
                firstName: i.signatoryDetails.firstName,
                lastName: i.signatoryDetails.lastName,
                middleName: i.signatoryDetails.middleName,
                email: i.signatoryDetails.email,
                isEmailNotification: i.signatoryDetails.isEmailNotification,
                accountId: i.signatoryDetails.accountId
            })
        })


        let lpDelegatesSelected = await models.LpDelegate.findAll({
            where: {
                fundId: fundId,
                lpId: subscription.lpId,
                subscriptionId: subscriptionId
            },
            include: [{
                model: models.User,
                as: 'details',
                required: false
            }]
        })

        let lpDelegates = [];
        lpDelegatesSelected.map(i => {
            lpDelegates.push({
                id: i.details.id,
                firstName: i.details.firstName,
                lastName: i.details.lastName,
                middleName: i.details.middleName,
                email: i.details.email,
                isEmailNotification: i.details.isEmailNotification,
                accountId:i.details.accountId
            })
        })

        eventHelper.saveEventLogInformation('GP issues Capital Commitment Increase', fund.id, subscriptionId, req);

        // GP has issued Capital Commitment Increase Letter - LP        
        // LP      
         
        let documentinfo = await models.DocumentsForSignature.findOne({
            attributes: ['id'],
            where: { changeCommitmentId:changeCommitment.id }
        });        

        let documentid = documentinfo.id

        const alertData = {
            htmlMessage: messageHelper.issuedCapitalCommitmentIncreaseLetterAlertMessage,
            message: 'has issued Capital Commitment Increase Letter',
            lpName: commonHelper.getFullName(subscription.lp),
            subscriptionId: subscriptionId,
            docType: 'CHANGE_COMMITMENT_AGREEMENT',
            fundManagerCommonName: fund.fundManagerCommonName,
            fundName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId,
            documentId:documentid
        };
        //send custom message 
        let lpcustommessage = messageHelper.issuedCapitalCommitmentIncreaseLetterAlertMessageLPWithSignatory
        lpcustommessage = lpcustommessage.replace('[FundName]', fund.fundCommonName)

        let lpwithsignatoryalertdata =  {
            htmlMessage: lpcustommessage,
            lpName: commonHelper.getFullName(subscription.lp),
            subscriptionId: subscriptionId,
            docType: '',
            fundManagerCommonName: fund.fundManagerCommonName,
            fundName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId,
            documentId:documentid
        }

        // if lp has signatories then alert will not contain lnk
        if(lpSignatories.length>0){         
            notificationHelper.triggerNotification(req, fund.gpId, subscription.lpId,{},lpwithsignatoryalertdata,subscription.lp.accountId,null);
        } else {
            notificationHelper.triggerNotification(req, fund.gpId, subscription.lpId, {}, alertData,subscription.lp.accountId,null);
        }   

        //send alert to LP delegate only
        notificationHelper.triggerNotification(req, fund.gpId, false, lpDelegates, alertData,null,null);

        //send alert to LP sign only
        notificationHelper.triggerNotification(req, fund.gpId, false, lpSignatories, alertData,null,null);

        let emailBodyContentLP = {
            'firstName': subscription.lp.firstName,
            'lastName': subscription.lp.lastName,
            'middleName': subscription.lp.middleName,
            'name': commonHelper.getInvestorName(subscription),
            'fundCommonName': fund.fundCommonName
        }      

        //if (lp.isEmailNotification) { // if setting is enabled
            
            if(lpSignatories.length>0){
                emailHelper.triggerAlertEmailNotification(subscription.lp.email, 'Fund Manager has issued Capital Commitment Increase Letter', emailBodyContentLP, 'gpSignatoriesChangeCapitalCommitment.html');
            } else {
                emailHelper.triggerAlertEmailNotification(subscription.lp.email, 'Fund Manager has issued Capital Commitment Increase Letter', emailBodyContentLP, 'gpChangeCapitalCommitment.html');
            }
        //}

       // LP signatories
       for (let signatory of lpSignatories) {

        let emailBodyContentLPSign = {
            'firstName': signatory.firstName,
            'name':commonHelper.getFullName(signatory),
            'lastName': signatory.lastName,
            'middleName': signatory.middleName,
            'fundCommonName': fund.fundCommonName        
        }
       
        emailHelper.triggerAlertEmailNotification(signatory.email, 'Fund Manager has issued Capital Commitment Increase Letter', emailBodyContentLPSign, 'gpChangeCapitalCommitment.html');
       
        }        

        // LP Delegate
        for (let delegate of lpDelegates) {
            let emailBodyContentLPDelegate = {
                'firstName': delegate.firstName,
                'name':commonHelper.getFullName(delegate),
                'lastName': delegate.lastName,
                'middleName': delegate.middleName,
                'fundCommonName': fund.fundCommonName        
            }     
            
            emailHelper.triggerAlertEmailNotification(delegate.email, 'Fund Manager has issued Capital Commitment Increase Letter', emailBodyContentLPDelegate, 'gpChangeCapitalCommitment.html');
          
        }

        return res.status(201).json({
            message: messageHelper.lpCommitmentsCreated
        });

    } catch (e) {
        next(e);
    }
}


const lpCommitmentChangePDFGenerateAndCreateEnvelopes = async (changeCommitment, data, req) => {

    const fileName = `CHANGE_COMMITMENT_AGREEMENT_${uuidv1()}.pdf`;

    const filePath = `./assets/funds/${changeCommitment.fundId}/${changeCommitment.subscriptionId}/${fileName}`;
    let html = "./views/capital/CapitalCommitmentIncreaseLetter.html";

    html = fs.readFileSync(html);
    html = html.toString();

    await commonHelper.makePDF(html, data, filePath);

    await models.DocumentsForSignature.update({ isActive: 0 }, {
        where: {
            fundId: changeCommitment.fundId,
            subscriptionId: changeCommitment.subscriptionId,
            docType: 'CHANGE_COMMITMENT_AGREEMENT',
        }
    });

    await models.DocumentsForSignature.create({
        fundId: changeCommitment.fundId,
        subscriptionId: changeCommitment.subscriptionId,
        changeCommitmentId: changeCommitment.id,
        filePath: filePath,
        docType: 'CHANGE_COMMITMENT_AGREEMENT',
        gpSignCount: 0,
        lpSignCount: 0,
        isPrimaryGpSigned: 0,
        isPrimaryLpSigned: 0,
        isActive: 1,
        createdBy: req.userId,
        updatedBy: req.userId
    });
}

const getCapitalCommitmentChangeAgreement = async (req, res, next) => {

    try {

        const documentId = req.params.id

        let documentsForSignature = await models.DocumentsForSignature.findOne({
            where: {
                id: documentId
            },
            include: [{
                model: models.ChangeCommitment,
                as: 'changeCommitment',
                required: true
            }]
        });


        if (!documentsForSignature) {
            return res.json({ error: true, message: messageHelper.envelopeValidate })
        }

        return res.json({
            filePathTempPublicUrl: commonHelper.getLocalUrl(documentsForSignature.filePath)
        });

    } catch (error) {
        next(error);
    }

}


const gpSign = async (req, res, next) => {

    try {
        const { documentId, date, timeZone, proceed = false } = req.body

        const userId = req.userId;

        const gpData = await models.User.findOne({
            where: {
                id: userId
            }
        })

        if (!gpData) {
            return res.status(404).json({
                "errors": [
                    {
                        "msg": messageHelper.userValidate
                    }
                ]
            });
        }


        const documentsForSignature = await models.DocumentsForSignature.findOne({
            where: {
                id: documentId,
                isPrimaryLpSigned: 1,
            },
            include: [{
                model: models.ChangeCommitment,
                as: 'changeCommitment',
                required: true
            }, {
                model: models.FundSubscription,
                as: 'subscription',
                required: true,
                include:[{
                    model: models.OfflineUsers,
                    as: 'offlineLp',
                    required: false
                },{
                    model: models.User,
                    as: 'lp',
                    required: false
                }]
            }]
        })

        if (!documentsForSignature) {
            return res.status(404).json({
                "errors": [
                    {
                        "msg": messageHelper.envelopeValidate
                    }
                ]
            });
        }


        const fund = await models.Fund.findOne({
            include: [{
                model: models.timezone,
                as: 'timeZone',
                required: false
            },{
                model: models.User,
                as: 'gp',
                required: true
            }],
            where: {
                id: documentsForSignature.fundId
            }
        })


        // if gp already counter-signed don't allow to sign again
        if (documentsForSignature.isPrimaryGpSigned === true) {

            return errorHelper.error_400(res,
                'isPrimaryGpSigned',
                'Your Capital Commitment Increase Form has already been signed. Once it is accepted and signed by your Fund Manager, please visit your Document Locker to view the document.',
                {
                    "CODE": 'CAPITAL_COMMITMENT_ERROR',
                    "proceed": false,
                });

        }


        // Hardcap validation
        if (fund.fundHardCap < (documentsForSignature.changeCommitment.amount)) {

            return errorHelper.error_400(res,
                'isPrimaryGpSigned',
                'Amount in excess of hard cap',
                {
                    "CODE": 'AMOUNT_EXCESS_HARD_CAP',
                    "proceed": false,
                });

        }

        const erisaPercentage = await erisaValidation(documentsForSignature.fundId, documentsForSignature.subscription.lpId)
        if (erisaPercentage > 25 && proceed === false) {

            return errorHelper.error_400(res,
                'isPrimaryGpSigned',
                'You are attempting to close a constituency of investors such that the Fund would have over 25% ERISA Benefit Plan money and would become subject to regulation as a Fund holding Plan Assets unless the Fund operates as a venture capital operating company.  Please consult your legal adviser.',
                {
                    "CODE": 'ERISA_WARNINGS',
                    "proceed": true,
                });

        } 

        //let gpSignatoryPicObject = JSON.parse(gpData.signaturePic)
        //const gpSignaturePath = commonHelper.getLocalUrl(gpSignatoryPicObject.path)

        let gpSignatureUrl = gpData.signaturePicUrl

        console.log("**************gpSignatureUrl**********************",gpSignatureUrl)

        let gpSignaturePath = gpData.signaturePic


        //making date with timezone of a fund
        let date_for_sign = await commonHelper.getDate(); let timeZone_name = req.body.timeZone;
        date_for_sign = commonHelper.getDateTimeWithTimezone(fund.timeZone, date_for_sign, 0, 0);


        const newChangeCommitmentTempFile = await changeCommitmentAgreementSign({
            gpName: commonHelper.getFullName(gpData),
            fundManagerTitle: fund.fundManagerTitle,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            fundLegalEntity: fund.legalEntity,
            fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
            gpSignaturePic: gpSignatureUrl,
            date: date_for_sign
        }, documentsForSignature.filePath);


        fs.copyFileSync(newChangeCommitmentTempFile.path, documentsForSignature.filePath);

        let isAllGpSignatoriesSigned = req.user.accountType == 'GP' ? true : documentsForSignature.gpSignCount + 1 >= fund.noOfSignaturesRequiredForCapitalCommitment - 1;

        await models.DocumentsForSignature.update({
            isPrimaryGpSigned: req.user.accountType == 'GP' ? true : false,
            isAllGpSignatoriesSigned: isAllGpSignatoriesSigned,
            gpSignCount: literal('gpSignCount + 1'),
            gpIpAddress: req.ipInfo
            //gpSignedDateTime: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
        }, {
                where: {
                    id: documentId,
                }
            });

        //const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);
        const signedDateconvertedToUserTimezone = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')

        await models.SignatureTrack.create({
            documentId: documentId,
            documentType: 'CHANGE_COMMITMENT_AGREEMENT',
            signaturePath: gpSignaturePath,
            subscriptionId: documentsForSignature.subscriptionId,
            ipAddress: req.ipInfo,
            signedDateTime: date_for_sign, //signedDateconvertedToUserTimezone,
            timezone: timeZone_name,// req.body.timeZone,
            signedDateInGMT: req.body.date,
            signedUserId: req.user.id,
            signedByType: req.user.accountType,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        });

        await models.FundSubscription.update({ lpCapitalCommitment: documentsForSignature.changeCommitment.amount }, {
            where: {
                id: documentsForSignature.subscriptionId
            }
        })


        await models.FundClosings.update({ isActive: 0 }, {
            where: {
                subscriptionId: documentsForSignature.subscriptionId
            }
        })

        await models.FundClosings.create({
            lpCapitalCommitment: documentsForSignature.changeCommitment.amount,
            gpConsumed: documentsForSignature.changeCommitment.amount,
            createdBy: userId,
            updatedBy: userId,
            fundId: fund.id,
            subscriptionId: documentsForSignature.changeCommitment.subscriptionId,
            isActive: 1,
        })

        eventHelper.saveEventLogInformation('GP Counter-Sign Capital Commitment Increase', documentsForSignature.fundId, documentsForSignature.subscriptionId, req);

        //commented as gp getting fundclosing alert email after countersign (VANL-922)
        //closeFundValidationRules(fund)

        // get lpdelegates for the fund based on lpid
        let lpDelegatesSelected = await models.LpDelegate.findAll({
            where: {
                fundId: fund.id,
                lpId: documentsForSignature.subscription.lpId
            },
            include: [{
                model: models.User,
                as: 'details',
                required: false
            }]
        })

        let lpDelegates = [];
        lpDelegatesSelected.map(i => {
            lpDelegates.push({
                id: i.details.id,
                firstName: i.details.firstName,
                lastName: i.details.lastName,
                email: i.details.email,
                isEmailNotification: i.details.isEmailNotification,
                accountId: i.details.accountId
            })
        })

        // GP has executed Capital Commitment Increase Letter and it has become binding - LP
        const changeCommitmentAmount = parseFloat(documentsForSignature.changeCommitment.amount).toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD',
        });
        // LP
        let lpData = await models.User.findOne({
            where: {
                id: documentsForSignature.subscription.lpId
            }
        });

        if(!lpData){
            lpData =   documentsForSignature.subscription.offlineLp
        }

        //get total gp signatories count
        const signatoriesCount = await models.GpSignatories.count({ where: { fundId: fund.id } })

       //no of signatures for side letter
       let noOfSignaturesRequiredForCapitalCommitment = fund.noOfSignaturesRequiredForCapitalCommitment 

       //if secondary gp counter sign the document , then send notification and email to GP
       if(req.user.accountType=='SecondaryGP' && noOfSignaturesRequiredForCapitalCommitment>1 && (signatoriesCount==documentsForSignature.gpSignCount + 1)){
      
            const alertDatastoGP = {
                htmlMessage: messageHelper.signedCapitalCommitmentIncreaseLetterGPWithOutSignatoryAlertMessage,
                message: 'Investor has signed Capital Commitment Increase',
                lpName: await commonHelper.getInvestorName(documentsForSignature.subscription),
                subscriptionId: documentsForSignature.subscriptionId,
                docType: 'CHANGE_COMMITMENT_AGREEMENT',
                fundManagerCommonName: fund.fundManagerCommonName,
                fundName: fund.fundCommonName,
                fundId: fund.id,
                sentBy: req.userId,
                documentId:documentId
            };    

            let gpemailBodyContent = {
                'firstName': fund.gp.firstName,
                'lastName': fund.gp.lastName,
                'middleName': fund.gp.middleName,
                'name': commonHelper.getInvestorName(documentsForSignature.subscription),
                'fundManagerName':commonHelper.getFullName(fund.gp),
                'fundCommonName': fund.fundCommonName,
                'clientURL': config.get('clientURL'),
                user:fund.gp
            }
            
            //notification and email to gp
            notificationHelper.triggerNotification(req, lpData.id, fund.gpId, {}, alertDatastoGP,fund.gp.accountId,fund.gp);          
            emailHelper.triggerAlertEmailNotification(fund.gp.email, 'Investor has signed Capital Commitment Increase', gpemailBodyContent, 'lpSignedCapitalCommitment.html');            

       }        


        const alertData = {
            htmlMessage: messageHelper.executedCapitalCommitmentIncreaseLetterAlertMessage,
            message: 'has executed Capital Commitment Increase Letter',
            CapitalCommitmentAccepted: changeCommitmentAmount,
            subscriptionId: documentsForSignature.changeCommitment.subscriptionId,
            documentId: documentId,
            docType: 'CHANGE_COMMITMENT_AGREEMENT',
            fundManagerCommonName: fund.fundManagerCommonName,
            fundName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId
        };

        let getLpAccountInfo = await models.User.findOne({
            attributes:['accountId'],
            where: {
                id: documentsForSignature.subscription.lpId
            }
        })

        let lpAccountId =  (getLpAccountInfo) ?  getLpAccountInfo.accountId : 0

        notificationHelper.triggerNotification(req, fund.gpId, documentsForSignature.subscription.lpId, lpDelegates, alertData,lpAccountId,null);
        //send only on gp sign
        if(req.user.accountType=='GP')
        {   
            //only for online users
            if(lpData)
            {
            let emailBodyContent = {
                'firstName': lpData.firstName,
                'lastName': lpData.lastName,
                'middleName': lpData.middleName,
                'fundCommonName': fund.fundCommonName,
                'capitalCommitmentAccepted': changeCommitmentAmount,
                'name':commonHelper.getFullName(lpData),
            }        

            if(lpData){    
                if (lpData.isEmailNotification) { // if setting is enabled
                    emailHelper.triggerAlertEmailNotification(lpData.email, 'Fund Manager has executed Capital Commitment Increase Letter and it has become binding', emailBodyContent, 'gpExecutedCapitalCommitment.html');
                }
            }

            // LP Delegate
            for (let delegate of lpDelegates) {
                if (delegate.isEmailNotification) { // if setting is enabled

                    let emailBodyContent = {
                        'firstName': delegate.firstName,
                        'lastName': delegate.lastName,
                        'middleName': delegate.middleName,
                        'fundCommonName': fund.fundCommonName,
                        'capitalCommitmentAccepted': changeCommitmentAmount,
                        'name':commonHelper.getFullName(delegate),
                    }      

                    emailHelper.triggerAlertEmailNotification(delegate.email, 'Fund Manager has executed Capital Commitment Increase Letter and it has become binding', emailBodyContent, 'gpExecutedCapitalCommitment.html');
                }
            }

            }
        }        

        return res.json({ url: `${config.get('serverURL')}/api/v1/document/view/viewcc/${documentsForSignature.subscriptionId}` });

    } catch (error) {
        next(error);
    }

}

// generate last page sined pdf
async function changeCommitmentAgreementSign(data, changeCommitmentAgreementTempFile) {

    const htmlFilePath = "./views/changeCommitmentAgreementGPSign.html";

    const signedLastPage = await generateSignedPage(data, htmlFilePath);

    const fileName = `CHANGE_COMMITMENT_AGREEMENT_${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
    await commonHelper.pdfMerge([changeCommitmentAgreementTempFile, signedLastPage.path], outputFile);
    return { path: outputFile, name: fileName };
}

async function generateSignedPage(data, htmlFilePath) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}


async function erisaValidation(fundId, lpId) {
    //get closed status investors for the fund
    const fundInvestorsClosed = await models.FundSubscription.findAll({
        where: {
            fundId: fundId,
            status: 10 // closed
        }
    })

    //get closed-ready status (selected investor) investors for the fund
    const fundInvestorsSelected = await models.FundSubscription.findAll({
        where: {
            fundId: fundId,
            lpId: lpId,
            status: 7 // close ready               
        }
    })

    let closedYesAmount = 0;
    let closedTotalAmount = 0;

    for (let eachrow of fundInvestorsClosed) {
        if (eachrow.employeeBenefitPlan || eachrow.planAsDefinedInSection4975e1 || eachrow.benefitPlanInvestor) {
            closedYesAmount += eachrow.lpCapitalCommitment
        }

        closedTotalAmount += eachrow.lpCapitalCommitment
    }


    let selectedInvestorYesAmount = 0;
    let selectedInvestorTotalAmount = 0;


    for (let eachrow of fundInvestorsSelected) {
        if (eachrow.employeeBenefitPlan || eachrow.planAsDefinedInSection4975e1 || eachrow.benefitPlanInvestor) {
            selectedInvestorYesAmount += eachrow.lpCapitalCommitment
        }

        selectedInvestorTotalAmount += eachrow.lpCapitalCommitment
    }


    const erisaPercentage = ((selectedInvestorYesAmount + closedYesAmount) / (selectedInvestorTotalAmount + closedTotalAmount)) * 100

    return erisaPercentage;
}



//cancel commitment change increase letter
const CancelCommitmentChange = async (req, res, next) => {
    try {
        const { subscriptionId} = req.params
        logger.info('Cancel Capital Commitment Start')

        //pre check for cancel capital commitment
        const documentsForSignature = await models.DocumentsForSignature.findOne(       
        {   
            attributes:['id','changeCommitmentId','fundId'],
            where: {
                subscriptionId: subscriptionId,
                docType:'CHANGE_COMMITMENT_AGREEMENT',
                isActive: 1,
                lpSignCount:0,
                isPrimaryLpSigned:0,
                isAllLpSignatoriesSigned:0
            },
            limit: 1,
            order: [['id', 'DESC']],            
        })

        logger.info('Cancel Capital Commitment : get Document')

        //if no document
        if (!documentsForSignature) {
            logger.error('Capital Commitment Record Not Found')
            return res.status(400).json({
                "errors": [
                    {
                        "msg": messageHelper.subscriptionIdReq
                    }
                ]
            });
        }

        let id = documentsForSignature.id
        let ccid = documentsForSignature.changeCommitmentId
        let fundId = documentsForSignature.fundId
        logger.info('Cancel Capital Commitment : update document status')
        //update
        await models.ChangeCommitment.update({ isActive: 0 }, { where: { id:ccid } })
        await models.DocumentsForSignature.update({
            isActive: 0,
            deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
        }, {
                where: { id: id }
        })

        const subscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            },
            include: [{
                model: models.Country,
                as: 'mailingAddressCountryData',
                required: false,
            }, {
                model: models.State,
                as: 'mailingAddressStateData',
                required: false,
            }]
        });


        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.GpDelegates,
                as: 'gpDelegatesSelected',
                required: false,
                where: {
                    fundId: fundId
                }
            }]
        })

        logger.info('Cancel Capital Commitment : get lp info ')

        //get lp info
        const lp = await models.User.findOne({
            where: {
                id: subscription.lpId
            }
        })

        logger.info('Cancel Capital Commitment : get lp delegates ')

        let lpDelegatesSelected = await models.LpDelegate.findAll({
            where: {
                fundId: fundId,
                lpId: subscription.lpId,
                subscriptionId: subscriptionId
            },
            include: [{
                model: models.User,
                as: 'details',
                required: false
            }]
        })

        let lpDelegates = [];
        lpDelegatesSelected.map(i => {
            lpDelegates.push({
                id: i.details.id,
                firstName: i.details.firstName,
                lastName: i.details.lastName,
                middleName: i.details.middleName,
                email: i.details.email,
                isEmailNotification: i.details.isEmailNotification,
                accountId:i.details.accountId
            })
        })
      
        
        logger.info('Cancel Capital Commitment : get lp signatories ')

        // get lp signatories
        let lpSignatoriesSelected = await models.LpSignatories.findAll({
            where: {
                fundId: fundId,
                lpId: subscription.lpId,
                subscriptionId: subscriptionId
            },
            include: [{
                model: models.User,
                as: 'signatoryDetails',
                required: false
            }]
        })

        let lpSignatories = [];
        lpSignatoriesSelected.map(i => {
            lpSignatories.push({
                id: i.signatoryDetails.id,
                firstName: i.signatoryDetails.firstName,
                lastName: i.signatoryDetails.lastName,
                middleName: i.signatoryDetails.middleName,
                email: i.signatoryDetails.email,
                isEmailNotification: i.signatoryDetails.isEmailNotification,
                accountId:i.signatoryDetails.accountId
            })
        })
 
        logger.info('Cancel Capital Commitment : send notification to LP ')

        eventHelper.saveEventLogInformation('GP cancelled Capital Commitment Increase', fund.id, subscriptionId, req);
        
        
        // GP has cancelled Capital Commitment Increase Letter - LP        
        // LP        
        const alertData = {
            htmlMessage: messageHelper.cancelledCapitalCommitmentIncreaseLetterAlertMessage,
            message: 'has cancelled Capital Commitment Increase Letter',
            lpName: commonHelper.getFullName(lp),
            subscriptionId: subscriptionId,
            docType: 'CHANGE_COMMITMENT_AGREEMENT',
            fundManagerCommonName: fund.fundManagerCommonName,
            fundName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId
        };

        logger.info('Cancel Capital Commitment : send notification to signatories ')

        notificationHelper.triggerNotification(req, fund.gpId, false, lpSignatories, alertData,null,null); 
        
        notificationHelper.triggerNotification(req, fund.gpId, false, lpDelegates, alertData,null,null);

        notificationHelper.triggerNotification(req, fund.gpId, subscription.lpId, {}, alertData,lp.accountId,null);        
        
        logger.info('Cancel Capital Commitment End')
        
        return res.status(201).json({
            message: messageHelper.updateMessage
        });
 

    } catch (error) {
        next(error);
    }

}

module.exports = {
    saveLpCommitments,
    getCapitalCommitmentChangeAgreement,
    gpSign,
    CancelCommitmentChange
}
