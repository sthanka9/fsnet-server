
const models = require('../models/index');
const config = require('../config/index')
const _ = require('lodash')
const { Op,literal} = require('sequelize');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const triggerNotifications = require('../helpers/triggerNotifications');

const isGpSignatoryNew = async (req, res, next) => {

    try {
        const { email, fundId } = req.body;
        let firstName = ""
        let lastName  = ""

        if (!email) {
            return errorHelper.error_400(res, 'email', messageHelper.emailValidate);
        }

        let status = null;

        const user = await models.User.findOne({
            attributes: ['id', 'firstName', 'lastName'],
            where: { email: email, accountType: 'SecondaryGP' }
        })

        if (Boolean(user)) {

            firstName = _.get(user, 'firstName', '')
            lastName =  _.get(user, 'lastName', '')

            const isAlreadyAddedAsSignatoryToFund = await models.GpSignatories.findOne({
                attributes: ['id', 'gpId'],
                where: {
                    fundId: fundId,
                    signatoryId: user.id
                }
            });

            status = Boolean(isAlreadyAddedAsSignatoryToFund) ? 2 : 1

        } else {

            const account = await models.Account.findOne({
                attributes: ['id', 'firstName', 'lastName'],
                where: { email: email}
            })
            if(account){
                firstName = _.get(account, 'firstName', '')
                lastName =  _.get(account, 'lastName', '')                
                status = 3
            } else {
                status = 0
            }            
        }

        return res.json({
            status: status,
            firstName:firstName,
            lastName: lastName,
            email: email
        });

    } catch (error) {
        next(error);
    }
}

const fundGpSignatoryInvite = async (req, res, next) => {

    try {
        
        const { fundId, firstName, lastName, middleName, email } = req.body;

        const userId = req.userId

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.User,
                as: 'gp'
            }]
        })

        if (!fund) {
            return errorHelper.error_400(res,'fundId',messageHelper.fundNotFound);
              
        }

        //from admin : gp email should not be add as secondary signatory
        if(fund.gp.email==email){
            return errorHelper.error_400(res,'email',messageHelper.secondaySignatoryValidation);
        }

        // check total no. of signatoties to be added are 3
        const signatoriesCount = await models.GpSignatories.count({ where: { fundId: fundId } })
        if (signatoriesCount >= 4) {
            return errorHelper.error_400(res,'numberOfSignatoriesCheck',messageHelper.numberOfSignatoriesCheck);
        }

        const emailConfirmCode = commonHelper.generateToken(email) // generate token 

        const lastCodeGeneratedAt = new Date().toISOString();         

        //create/get account details                       
        let account = {}
        let accounttype = 'old'
        account = await models.Account.findOne({
            where: { email: email }
        })
        //account created but he was not registered then change emailcode and send him again
        if(account && account.isEmailConfirmed==0){ 
            await models.Account.update({                 
                emailConfirmCode:emailConfirmCode,
                lastCodeGeneratedAt:lastCodeGeneratedAt                
            },{where:{id:account.id}})

            account.emailConfirmCode = emailConfirmCode
        }        
        if(!account){
            account = await models.Account.create({
                email:email,
                firstName,
                lastName,
                middleName,
                emailConfirmCode:emailConfirmCode,
                isEmailConfirmed:0,
                lastCodeGeneratedAt:lastCodeGeneratedAt                
            })
            accounttype = 'new'            
        }  

        let accountId = account.id

        return models.sequelize.transaction(function (t) {

            return models.User.findOrCreate({
                where: { email: email, accountType: 'SecondaryGP' }, defaults: {
                    firstName,
                    lastName,
                    middleName,
                    accountType: 'SecondaryGP',
                    email,
                    accountId,
                    isEmailConfirmed:account.isEmailConfirmed
                }
            }).spread((user, created) => {
                

                return Promise.all([
                    user,
                    models.UserRole.findOrCreate({
                        where:{roleId: 6, userId: user.id},
                        defaults:{ roleId: 6, userId: user.id }
                    }),
                    models.GpSignatories.findOrCreate({
                        paranoid: false,
                        where: { fundId:fund.id , signatoryId: user.id, gpId:fund.gp.id  }, defaults: {
                            fundId:fund.id , signatoryId: user.id, gpId: fund.gp.id ,
                            createdBy: userId, updatedBy: userId
                        }
                    }).spread((gpSignatories, created) => gpSignatories.restore()),
                    models.VcFirmSignatories.findOrCreate({
                        paranoid: false,
                        where: { vcfirmId: fund.vcfirmId, signatoryId: user.id, type: 'SecondaryGP' }, defaults: {
                            vcfirmId: fund.vcfirmId, signatoryId: user.id, type: 'SecondaryGP',
                            createdBy: userId, updatedBy: userId
                        }
                    }).spread((vcFirmSignatories, created) => vcFirmSignatories.restore())
                ])
               
            })

        }).then(function (results) {
            const user = results[0]
            //update default userid in accounts table
            if(accounttype=='new')
                models.Account.update({defaultUserId:user.id},{where:{id:accountId}})

            const loginOrRegisterLink = account.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${account.emailConfirmCode}`;
            //if (user.isEmailNotification && req.user.accountType != 'FSNETAdministrator') { 
                // if setting is enabled               
                emailHelper.sendEmail({
                    toAddress: user.email,
                    subject: messageHelper.assignLpDelegateEmailSubject,
                    data: {
                        name: commonHelper.getFullName(user),
                        fundName: fund.legalEntity,
                        gpName: commonHelper.getFullName(req.user),
                        loginOrRegisterLink: loginOrRegisterLink,
                        user
                    },
                    htmlPath: "fundGPSignatoryInvitation.html"
                });
            //}

            return res.json({
                data: {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    middleName: user.middleName,
                    accountType: user.accountType,
                    email: user.email,
                    isEmailConfirmed:account.isEmailConfirmed,
                    emailConfirmCode:account.emailConfirmCode
                }
            })

        }).catch(function (err) {
            return next(err)
        });
    } catch (e) {
        next(e)
    }
}

const assignGpSignatoryToFund = async (req, res, next) => {

    try {
        const { fundId, gpSignatories = [],vcfirmId } = req.body

        //remove records from gpsignatory approvals
        if(gpSignatories.length==0){
            await models.GpSignatoryApproval.destroy({
                where: {
                    fundId: fundId
                }
            })
        } else {
            await models.GpSignatoryApproval.destroy({
                where: {
                    signatoryId: {
                        [Op.notIn] : gpSignatories
                    },
                    fundId: fundId
                }
            })
        }

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.User,
                as: 'gp'
            }]
        })

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        if (gpSignatories.length >= 4) {
            return errorHelper.error_400(res,'numberOfSignatoriesCheck',messageHelper.numberOfSignatoriesCheck);
        }


         // remove old lp signatories
         await models.GpSignatories.destroy({
            where: {
                signatoryId: {
                    [Op.notIn] : gpSignatories
                },
                fundId: fundId
            }
        });

        //restore if already in the table
        await models.GpSignatories.update({
            deletedAt: null
        },{
            paranoid: false,
            where: {
                signatoryId: {
                    [Op.in] : gpSignatories
                },
                fundId: fundId
            }
        });

        await models.VcFirmSignatories.update({
            deletedAt: null
        },{
            paranoid: false,
            where: {
                signatoryId: {
                    [Op.in] : gpSignatories
                },
                vcfirmId: vcfirmId
            }
        });

        // existing signatories for fund.
        const gpSignatoriesList = await models.GpSignatories.findAll({
            attributes: ['signatoryId'],
            where: {
                fundId: fundId
            }
        });


        const gpSignatoriesIds = _.map(gpSignatoriesList, 'signatoryId'); // previous gp signatories   

        // get new signatories, post data.
        const fundNewSignatoryIds = _.difference(gpSignatories, gpSignatoriesIds);

        const newSignatories = [];

        for (let signatory of fundNewSignatoryIds) {
            // add new gp signatories
            newSignatories.push({
                fundId: fundId,
                signatoryId: signatory,
                gpId: fund.gpId,
                createdBy: req.userId,
                updatedBy: req.userId
            });
        }

        // insert new signatories
        models.GpSignatories.bulkCreate(newSignatories);

        if (req.user.accountType != 'FSNETAdministrator' && fundNewSignatoryIds) {

            triggerNotifications.triggerEmailNotificationsToSignatories(fundNewSignatoryIds, fund, req.user.accountType, 'fundGPSignatoryInvitation.html');
        }

        return res.json(true);

    } catch (e) {
        next(e)
    }
}

const removeFundGPSignatory = async (req, res, next) => {

    try {
        const { userId, fundId } = req.body;

        await models.GpSignatories.destroy({
            where: {
                signatoryId: userId,
                fundId: fundId
            }
        })

        await models.VcFirmSignatories.destroy({
            where: {
                signatoryId: userId
            }
        })


        await models.History.destroy({
            where: {
                userId: userId,
                fundId: fundId
            }
        })

        return res.json(true)

    } catch (e) {
        next(e)
    }
}

const removeFundGpDelegate = async (req, res, next) => {

    try {
        const { userId, fundId } = req.body;

        await models.GpDelegates.destroy({
            where: {
                delegateId: userId,
                fundId: fundId
            }
        })

        await models.VcFrimDelegate.destroy({
            where: {
                delegateId: userId
            }
        })

        await models.History.destroy({
            where: {
                userId: userId,
                fundId: fundId
            }
        })        

        return res.json(true)

    } catch (e) {
        next(e)
    }
}

const signatureSettings = async (req, res, next) => {

    try {
        const { fundId, noOfSignaturesRequiredForClosing, noOfSignaturesRequiredForCapitalCommitment, noOfSignaturesRequiredForSideLetter, noOfSignaturesRequiredForFundAmmendments } = req.body;

        const signatoriesCount = await models.GpSignatories.count({ where: { fundId: fundId } });

        if (signatoriesCount >= 4) {
            return errorHelper.error_400(res, 'numberOfSignatoriesCheck', messageHelper.numberOfSignatoriesCheck);
        }


        const documents = await models.DocumentsForSignature.findAll({
            where: {
                isPrimaryLpSigned: true,
                isPrimaryGpSigned: false,
                fundId: fundId,
                isActive: 1,
                isArchived:0,
                deletedAt:null
            },
            include: [{
                model: models.SignatureTrack,
                as: 'signatureTrackList',
                where: {
                    isActive: 1
                },
                required: false
            }]
        });


        for (document of documents) {
            const signedCount = document.signatureTrackList ? document.signatureTrackList.length : 0;
            if (document.docType == 'CHANGE_COMMITMENT_AGREEMENT') {
                const isAllGpSignatoriesSigned = noOfSignaturesRequiredForCapitalCommitment == 1 ? true : (noOfSignaturesRequiredForCapitalCommitment <= signedCount ? true : false);
                await models.DocumentsForSignature.update({
                    isAllGpSignatoriesSigned: isAllGpSignatoriesSigned
                }, {
                        where: {
                            id: document.id
                        }
                    });
            }

            if (document.docType == 'SIDE_LETTER_AGREEMENT') {
                const isAllGpSignatoriesSigned = noOfSignaturesRequiredForSideLetter == 1 ? true : (noOfSignaturesRequiredForSideLetter <= signedCount ? true : false);
                await models.DocumentsForSignature.update({
                    isAllGpSignatoriesSigned: isAllGpSignatoriesSigned
                }, {
                        where: {
                            id: document.id
                        }
                    });
            }            

            if (document.docType == 'SUBSCRIPTION_AGREEMENT') {
                const isAllGpSignatoriesSigned = noOfSignaturesRequiredForClosing == 1 ? true : (noOfSignaturesRequiredForClosing <= signedCount ? true : false);
                await models.DocumentsForSignature.update({
                    isAllGpSignatoriesSigned: isAllGpSignatoriesSigned
                }, {
                        where: {
                            id: document.id
                        }
                    });
            }
           
        }

        // Master GP can set how many signatures will be required for each signing event.
        await models.Fund.update({
            noOfSignaturesRequiredForClosing: noOfSignaturesRequiredForClosing,
            noOfSignaturesRequiredForCapitalCommitment: noOfSignaturesRequiredForCapitalCommitment,
            noOfSignaturesRequiredForSideLetter: noOfSignaturesRequiredForSideLetter,
            noOfSignaturesRequiredForFundAmmendments:noOfSignaturesRequiredForFundAmmendments
        }, {
            where: {
                id: fundId
            }
        });

        const fundAmmendment = await models.FundAmmendment.findOne({
            where:{
                fundId,
                isActive:1,
                isArchived:0
            }
        });

        //To update the noOfSignaturesRequiredForFundAmmendments if fundAmmendment already exists
        if(fundAmmendment){
            await models.FundAmmendment.update({
                noOfSignaturesRequiredForFundAmmendments:noOfSignaturesRequiredForFundAmmendments
            },{
                where:{
                    fundId,
                    isActive:1,
                    isArchived:0
                }
            });
        }


        return res.json({
            message: messageHelper.signatureSettings
        });

    } catch (e) {
        next(e)
    }
}


const fundLpSignatoryInvite = async (req, res, next) => {

    try {
        const { fundId, firstName, lastName, middleName, email, subscriptionId } = req.body

        const userId = req.userId

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        })

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        const emailConfirmCode = commonHelper.generateToken(email) // generate token 

        const lastCodeGeneratedAt = new Date().toISOString();

        //create/get account details                       
        let account = {}
        let accounttype = 'old'
        account = await models.Account.findOne({
            where: { email: email }
        })
        //account created but he was not registered then change emailcode and send him again
        if(account && account.isEmailConfirmed==0){ 
            await models.Account.update({                 
                emailConfirmCode:emailConfirmCode,
                lastCodeGeneratedAt:lastCodeGeneratedAt                
            },{where:{id:account.id}})

            account.emailConfirmCode = emailConfirmCode
        }        
        if(!account){
            account = await models.Account.create({
                email:email,
                firstName,
                lastName,
                middleName,
                emailConfirmCode:emailConfirmCode,
                isEmailConfirmed:0,
                lastCodeGeneratedAt:lastCodeGeneratedAt                
            })
            accounttype = 'new'
        }        
        let accountId = account.id  
                

        return models.sequelize.transaction(function (t) {

            return models.User.findOrCreate({
                where: { email: email, accountType: 'SecondaryLP' }, defaults: {
                    firstName,
                    lastName,
                    middleName,
                    accountType: 'SecondaryLP',
                    email,
                    accountId,
                    isEmailConfirmed:account.isEmailConfirmed
                }
            }).spread(async(user, created) => {

                await models.UserRole.findOrCreate({
                    where: { roleId: 7, userId: user.id },
                    defaults: { roleId: 7, userId: user.id }
                });

                await models.LpSignatories.findOrCreate({
                    paranoid: false,
                    where: { fundId: fundId, lpId: userId, subscriptionId: subscriptionId, signatoryId: user.id }, defaults: {
                        fundId: fundId, signatoryId: user.id, lpId: userId,
                        subscriptionId: subscriptionId, createdBy: userId, updatedBy: userId
                    }
                }).spread((lpSignatories, created) => lpSignatories.restore());

                await models.VcFirmSignatories.findOrCreate({
                    paranoid: false,
                    where: { vcfirmId: userId, signatoryId: user.id, type: 'SecondaryLP' }, defaults: {
                        vcfirmId: userId, signatoryId: user.id, type: 'SecondaryLP',
                        createdBy: userId, updatedBy: userId
                    }
                }).spread((vcFirmSignatories, created) => vcFirmSignatories.restore());


                models.LpSignatories.update({
                    subscriptionId: subscriptionId,
                }, {
                    where: {
                        fundId: fundId,
                        signatoryId: user.id,
                        lpId: userId,
                    }
                });
    

                //update default userid in accounts table
                if(accounttype=='new')
                    models.Account.update({defaultUserId:user.id},{where:{id:accountId}})


                const subscription = await models.FundSubscription.findOne({
                    where:{
                        id:subscriptionId
                    }
                });
                
                if(subscription.status != 7){
                    await models.FundSubscription.update({
                        noOfSignaturesRequired : literal('noOfSignaturesRequired + 1'),
                    },{
                        where:{
                            id:subscriptionId
                        }
                    });
                }

                //If close ready
                if(subscription.status == 7){

                    //If any new signatory is added, make the subscription inprogress
                    await models.FundSubscription.update({
                        noOfSignaturesRequired : literal('noOfSignaturesRequired + 1'),
                        status:2
                    },{
                        where:{
                            id:subscriptionId
                        }
                    });


                    await models.DocumentsForSignature.update({
                        isActive: 0,
                    }, {
                        where: {
                            subscriptionId: subscriptionId,
                            docType: {
                                [Op.in]: ['SUBSCRIPTION_AGREEMENT', 'FUND_AGREEMENT', 'SIDE_LETTER_AGREEMENT']
                            },              
                        }
                    });
        
                    await models.SignatureTrack.update({ isActive: 0 }, {         
                        where: {
                            documentType: {
                                [Op.in]: ['SUBSCRIPTION_AGREEMENT', 'FUND_AGREEMENT','SIDE_LETTER_AGREEMENT']
                            },
                            subscriptionId: subscription.id
                        }
                    });

                }else if(subscription.isLpSignatoriesNotified && subscription.status !== 7){

                    if (account.isEmailNotification || req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
                        const loginOrRegisterLink = account.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${account.emailConfirmCode}`;
                        emailHelper.sendEmail({
                            toAddress: user.email,
                            subject: messageHelper.assignLpSignatoryEmailSubject,
                            data: {
                                name: commonHelper.getFullName(user),
                                fundName: fund.legalEntity,
                                lpName: commonHelper.getFullName(req.user),
                                loginOrRegisterLink: loginOrRegisterLink,
                                user
                            },
                            htmlPath: "secondaryLPSignature.html"
                        });
                    }
                }


                return res.json({
                    data: {
                        id: user.id,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        middleName: user.middleName,
                        //organizationName: fundSubscriptionOfLp.organizationName ? fundSubscriptionOfLp.organizationName :user.organizationName,
                        accountType: user.accountType,
                        email: user.email
                    }
                })
            
            })

        }).catch(function (err) {
            return next(err)
        });
    } catch (e) {
        return next(e)
    }
}

const assignLpSignatoryToSubscription = async (req, res, next) => {

    try {
        const { fundId, subscriptionId, lpSignatoryChangeEvent, lpSignatories = [] } = req.body

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.User,
                as: 'gp'
            }]
        })

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        if (lpSignatories.length >= 4) {
            return errorHelper.error_400(res,'lpSignatoriesCheck',messageHelper.numberOfSignatoriesCheck);
        }
      
        await models.LpSignatories.destroy({
            where: {
                fundId: fundId,
                subscriptionId: subscriptionId,
                signatoryId: {
                    [Op.notIn]: lpSignatories
                }
            }
        });

        // restore if reselected 
        await models.LpSignatories.update({ deletedAt: null }, {
            paranoid: false, 
            where: {
                subscriptionId: subscriptionId,
                signatoryId: {
                    [Op.in]: lpSignatories
                },
                fundId: fundId
            }
        });

        // get current signatories.
        const currentLpSignatoriesList = await models.LpSignatories.findAll({
            attributes: ['signatoryId', 'fundId', 'subscriptionId', 'lpId'],
            where: {
                fundId: fundId,
                subscriptionId: subscriptionId
            }
        });
        
        const currentLpSignatoriesIds = _.map(currentLpSignatoriesList, 'signatoryId');

        // get new signatories, post data.
        const newSignatoryIds = _.difference(lpSignatories, currentLpSignatoriesIds);


        //  new signatories
        const newSignatories = [];
        for (let signatoryId of newSignatoryIds) {
            newSignatories.push({
                fundId: fundId,
                subscriptionId: subscriptionId,
                signatoryId: signatoryId,
                lpId: req.userId,
                createdBy: req.userId,
                updatedBy: req.userId
            })
        }

        // insert new signatories
        models.LpSignatories.bulkCreate(newSignatories);

        const noOfSignaturesRequired = lpSignatories.length;

       const isAnybodySignedBefore = await models.DocumentsForSignature.count({ 
            where: {
            subscriptionId: subscriptionId,
            isActive: 1,
            lpSignCount: {
                [Op.gte] : 1
            }
        }});

        let subscriptionData = { noOfSignaturesRequired };

        if (lpSignatoryChangeEvent && isAnybodySignedBefore) {

            subscriptionData =  {
                noOfSignaturesRequired,
                status: 2
            } 

            await models.DocumentsForSignature.update({
                isActive: 0
            }, {
                where: {
                    subscriptionId: subscriptionId ,
                    docType: {
                        [Op.in]: ['SUBSCRIPTION_AGREEMENT', 'FUND_AGREEMENT', 'SIDE_LETTER_AGREEMENT']
                    }              
                }
            });

            await models.SignatureTrack.update({ isActive: 0 }, {         
                where: {
                    documentType: {
                        [Op.in]: ['SUBSCRIPTION_AGREEMENT', 'FUND_AGREEMENT','SIDE_LETTER_AGREEMENT']
                    },
                    subscriptionId: subscriptionId
                }
            });
        }

        await models.FundSubscription.update(subscriptionData, {
            where: {
                id: subscriptionId
            }
        });

        return res.json(true);

    } catch (e) {
        return next(e)
    }
}

const removeFundLpSignatory = async (req, res, next) => {
    try {

        const { signatoryId, fundId, subscriptionId } = req.body;

        await models.LpSignatories.destroy({
            where: {
                signatoryId: signatoryId,
                fundId: fundId,
                subscriptionId: subscriptionId
            }
        });

        await models.VcFirmSignatories.destroy({
            where: {
                signatoryId: signatoryId
            }
        });

        return res.json(true)

    } catch (e) {
        next(e)
    }
}

const isLpSignatoryNew = async (req, res, next) => {

    try {
        const { email, fundId, subscriptionId } = req.body;

        if (!email) {
            return errorHelper.error_400(res, 'email', messageHelper.emailValidate);
        }


        let firstName = ""
        let lastName  = ""
        let middleName;  

        const user = await models.User.findOne({
            attributes: ['id', 'firstName', 'lastName','middleName'],
            where: { email: email, accountType: 'SecondaryLP' }
        })

        let status = null;

        if (!Boolean(user)) {
            const account = await models.Account.findOne({
                attributes: ['id', 'firstName', 'lastName','middleName'],
                where: { email: email}
            })
            if(account){
                firstName = _.get(account, 'firstName', '')
                lastName =  _.get(account, 'lastName', '')
                middleName =  _.get(account, 'middleName', '')
                status = 3
            } else {
                status = 0
            }    
        } else {
            firstName = _.get(user, 'firstName', '')
            lastName = _.get(user, 'lastName', '')            
            middleName = _.get(user, 'middleName', '')            
            const delegate = await models.LpSignatories.findOne({
                attributes: ['id', 'lpId'],
                where: {
                    fundId: fundId,
                    subscriptionId: subscriptionId,
                    signatoryId: _.get(user, 'id')
                }
            })
            if (Boolean(delegate)) {
                status = 2; // already added as secondary LP
            } else {
                status = 1; // existing secondary lp , but not for this subscription
            }

        }

        return res.json({
            status: status,
            firstName: firstName,
            lastName: lastName,
            middleName: middleName,
            email: email
        })

    } catch (error) {
        next(error);
    }
}

module.exports = {
    isGpSignatoryNew,
    fundGpSignatoryInvite,
    assignGpSignatoryToFund,
    removeFundGPSignatory,
    signatureSettings,
   isLpSignatoryNew,
    fundLpSignatoryInvite,
    assignLpSignatoryToSubscription,
    removeFundLpSignatory,
    removeFundGpDelegate
}