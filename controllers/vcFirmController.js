const models = require('../models/index');
const config = require('../config/index');
const { Op } = require('sequelize');
const _ = require('lodash');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const eventHelper = require('../helpers/eventHelper');

const createVCFirm = async (req, res, next) => {

    try {
        
        const { firmName,middleName, firstName, lastName, email, subscriptonType} = req.body;

        const isUserExist = await models.User.findOne({
            where: {
                email: email,
                accountType: 'GP'
            }
        });

        if (isUserExist) {
            return errorHelper.error_400(res, 'email', messageHelper.emailAlreadyUsed);
        }        

        let account = {}
        let accounttype = 'old'        

        account = await models.Account.findOne({
            where: {
                email: email
            }
        });
 
        //account created 
        if(account){ 
            await models.Account.update({                 
                firstName,
                lastName,
                middleName,            
            },{where:{id:account.id}})
        }        
        if(!account){

            let code = commonHelper.generateToken(); 

            account = await models.Account.create({
                email:email,
                firstName,
                lastName,
                middleName,
                isEmailConfirmed:0,
                emailConfirmCode: code,
                lastCodeGeneratedAt: new Date().toISOString()                             
            })
            accounttype = 'new'
        }   
        
        
        let accountId = account.id

        const logo = await commonHelper.getFileInfo(req.file)

        return models.sequelize.transaction(function (t) {

            // chain all your queries here. make sure you return them.
            return models.User.create({
                firstName,middleName, lastName, email, accountType: 'GP',accountId
            }, { transaction: t }).then(function (user) {

                //update default userid to account
                if(accounttype == 'new')
                    models.Account.update({defaultUserId:user.id},{where:{id:accountId}})

                return Promise.all([
                    user,
                    models.VCFirm.create({
                        firmName, firstName, lastName,middleName, email, gpId: user.id, subscriptonType, VCFirmLogo: logo
                    }, { transaction: t }),

                    models.UserRole.create({ roleId: 2, userId: user.id }, { transaction: t }),

                ]).catch(function (err) {
                    throw new Error();
                });

            }).catch(function (err) {
                return next(err);
            });

        }).then(function (result) {

            const vcfirm = result[1];            

            // user.clientURL = config.get('clientURL')

            // emailHelper.sendEmail({
            //     toAddress: user.email,
            //     subject: messageHelper.welcomeEmailSubject,
            //     data: user,
            //     htmlPath: "newVcGpCreate.html"
            // })

            eventHelper.saveEventLogInformation('Firm Created', '', '', req);
            eventHelper.saveEventLogInformation('New Account Created', '', '', req);

            return res.json({
                vcfirmId: vcfirm.id
            });

        }).catch(function (err) {
            return next(err)
        });

    } catch (e) {
        return next(e)
    }


}

const get = (req, res, next) => {

    const { order = 'asc', orderBy = 'firmName' } = req.query;


    const skip = 10;
    const limit = req.query.limit || 1;

    const orderByFirmKind = orderBy === 'firmName' ? [
        [orderBy, order],
    ] : [];
    // const orderByGPKind = orderBy == 'firstName' ? [
    //     ['gp', orderBy, order],
    // ] : [];

    models.VCFirm.findAndCountAll({
        attributes: ['firmName', 'id', 'isGPNotified'],
        order: orderByFirmKind,
        limit: Number(skip),
        offset: (Number(limit) - 1) * skip,
        include: [
            {
                order: [['gp', 'firstName', 'ASC']],
                attributes: ['firstName', 'middleName', 'lastName', 'profilePic','isEmailConfirmed'],
                model: models.User,
                where: {
                    accountType: 'GP'
                },
                as: 'gp',
                required: true,
            }
        ],
    }).then(vcFirms => {

        const itemCount = vcFirms.count;

        const pageCount = Math.ceil(vcFirms.count / limit);

        vcFirms = vcFirms.rows.map((vcFirm) => {
            vcFirm = vcFirm.toJSON();
            return {
                vcfirmId: vcFirm.id,
                firmName: vcFirm.firmName,
                firstName: vcFirm.gp.firstName,
                middleName: vcFirm.gp.middleName,
                lastName: vcFirm.gp.lastName,
                isGPNotified: vcFirm.isGPNotified,
                profilePic: vcFirm.gp.profilePic,
                isEmailConfirmed: vcFirm.gp.isEmailConfirmed
            }
        });
        return res.json({
            itemCount,
            pageCount,
            skip,
            data: vcFirms

        });
    }).catch(error => res.json({ error: error.message }))

}

const triggerGPAccountDetails = async (req, res, next) => {

    try {
        const { vcfirmId } = req.body;

        const vcFirm = await models.VCFirm.findOne({
            where: {
                id: vcfirmId
            }
        });


        if (!vcFirm) {
            return res.send({
                error: true,
                message: messageHelper.vcfirmNotFound
            });
        }

        const user = await models.User.findOne({
            where: {
                id: vcFirm.gpId
            }
        });  

        if (!user) {
            return res.send({
                error: true,
                message: messageHelper.userNotFound
            });
        }

        const account = await models.Account.findOne({
            where: {
                id: user.accountId
            }
        });              

        let code = commonHelper.generateToken();

        await models.Account.update({
            emailConfirmCode: code,
            lastCodeGeneratedAt: new Date().toISOString(),
            isEmailConfirmed: 0
        }, {
                where: {
                    id: user.accountId
                }
            });


        //update users emailconfirmed
        await models.User.update({isEmailConfirmed: 0}, {where: {id: user.id}});      

        user.clientURL = config.get('clientURL')

        await models.VCFirm.update({
            isGPNotified: true
        }, {
            where: {
                id: vcfirmId
            }
        })

        emailHelper.sendEmail({
            toAddress: account.email,
            subject: messageHelper.welcomeEmailSubject,
            data: {
                name: commonHelper.getFullName(user),
                clientURL: config.get('clientURL'),
                email: account.email,
                link: `${config.get('clientURL')}/user/setPassword?code=${code}&id=${account.id}`
            },
            htmlPath: "gpAccountDetails.html"
        });

        

        return res.send(true);
    } catch (error) {
        
        next(error);
    }

}

const fundList = async (req, res, next) => {

    const { vcfirmId } = req.params;

    const { order = 'ASC', orderBy = 'legalEntity', search, limit = 1 } = req.query;

    const skip = 10;

    const whereFilter = { vcfirmId: vcfirmId }

    if (search && search != 'null') {
        whereFilter.legalEntity = {
            [Op.like]: `%${search}%`
        }
    }

    let funds = await models.Fund.findAndCountAll({
        attributes: ['id', 'legalEntity', 'statusId', 'createdAt', 'gpId'],
        limit: Number(skip),
        offset: (Number(limit) - 1) * skip,
        order: [
            [orderBy, order]
        ],
        where: whereFilter,
        include: [{
            model: models.FundStatus,
            as: 'fundStatus',
            required: true,
        }, {
            model: models.FundSubscription,
            as: 'fundInvestor',
            required: false,
            where: {
                deletedAt: null
            }
        }]
    });

    const itemCount = funds.count;
    const pageCount = Math.ceil(funds.count / limit);

    funds = funds.rows.map(fund => {

        fund = fund.toJSON();

        return {
            id: fund.id,
            legalEntity: fund.legalEntity,
            gpId: fund.gpId,
            totalInvestor: fund.fundInvestor ? fund.fundInvestor.length : 0,
            status: fund.fundStatus,
            createdAt: fund.createdAt,
            enableEmailTriggerButton: fund.statusId == 15 ? true : false
        };
    });

    return res.json({
        itemCount,
        pageCount,
        data: funds
    });
}

const triggerAccountAndFundDetailsEmail = async (req, res, next) => {

    try {
        //console.log("*******************************i am in triggerAccountAndFundDetailsEmail")
        const { fundId } = req.body;

        if (!fundId) {

            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.User,
                as: 'gp'
            }]
        })


        if (!fund) {
            return res.send({
                error: true,
                message: 'Fund not found.'
            });
        }


        const user = fund.gp;
        let htmlPath;
        user.clientURL = config.get('clientURL');
        user.newUser = false;

        let code = commonHelper.generateToken();

        const userdetails = await models.User.findOne({
            where: {
                id:user.id
            }
        })
        
        let accountId = userdetails.accountId
      
        let account = await models.Account.findOne({
            where: { id: accountId }
        })     
        
        if (!account.password) {
            // new gp user
            user.newUser = true;

            await models.Account.update({
                emailConfirmCode: code,
                lastCodeGeneratedAt: new Date().toISOString(),
                isEmailConfirmed: 0
            }, {
                    where: {
                        id: accountId
                    }
            });            
           
            await models.User.update({isEmailConfirmed: 0}, { where: {id: user.id } });

            await models.VCFirm.update({
                isGPNotified: true
            }, {
                where: {
                    id: fund.vcfirmId
                }
            });
        }         

        // // update status to open
        // await models.Fund.update({
        //     statusId: 12,
        // }, {
        //     where: {
        //         id: fundId
        //     }
        // });

        if (user.newUser) {
            // new gp user
            htmlPath = "gpAccountFundInvitationDetails.html";
        } else {
            // existing gp user            
            htmlPath = "gpFundDetails.html";
        }

        emailHelper.sendEmail({
            toAddress: user.email,
            subject: messageHelper.welcomeEmailSubject,
            data: {
                name: commonHelper.getFullName(user),
                fundLegalEntityName: fund.legalEntity,
                clientURL: config.get('clientURL'),                
                email: user.email,
                link: `${config.get('clientURL')}/user/setPassword?code=${code}&id=${account.id}`,
                user
            },
            htmlPath: htmlPath
        });

        //return res.send(true);
        return res.send({
            success: true,
            message: user.newUser
        });
    } catch (error) {        
        next(error);
    }

}

module.exports = {
    createVCFirm,
    get,
    fundList,
    triggerGPAccountDetails,
    triggerAccountAndFundDetailsEmail
}