const models = require('../models/index');
const { Op } = require('sequelize');
const _ = require("lodash");

module.exports = async (req, res, next) => {

    try {

        const { orderBy = 'firstName', order = 'DESC', fundId, subscriptionId } = req.params

   
        let lpDelegates = await models.LpDelegate.findAll({
            order: [
                ['details', orderBy, order]
            ],
            where: {
                fundId: fundId,
                subscriptionId: subscriptionId
            },
            include: [{
                model: models.User,
                as: 'details',
                require: true
            }]
        })

        let lpIds = []
        lpDelegates.map(lpDelegate => {
            lpIds.push(lpDelegate.details.id);
        })

        let fundSubscriptionsForLps = await models.FundSubscription.findAll({
            where:{
                lpId:{
                    [Op.in]:lpIds
                }
            }
        });


        lpDelegates = lpDelegates.map(data => {

            const getSingleSubscriptionForLp = _.find(fundSubscriptionsForLps, {lpId: data.details.id}) 

            return {
                firstName: data.details.firstName,
                lastName: data.details.lastName,
                middleName: data.details.middleName,
                email: data.details.email,
                id: data.details.id,
                profilePic: data.details.profilePic,
                organizationName: getSingleSubscriptionForLp ? getSingleSubscriptionForLp.organizationName :data.details.organizationName
            };
        });

        return res.json({ data: lpDelegates })

    } catch (e) {
        next(e)
    }

}
