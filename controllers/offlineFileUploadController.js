const models = require('../models/index');
const _ = require('lodash')
const config = require('../config/index');
const path = require('path');
const uuidv1 = require('uuid/v1');
const fs = require('fs');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const errorHelper = require('../helpers/errorHelper');
const notificationHelper = require('../helpers/notificationHelper');
const moment = require('moment');
const { Op } = require('sequelize');
const emailHelper = require('../helpers/emailHelper');

const getOfflinePendingDocuments = async (req, res, next) => {
    try {
        const { subscriptionId } = req.params         
        let docs = []    
        let documents = []  
        let subscription = await models.FundSubscription.getOfflineSubscription(subscriptionId, models)  
        //closed-ready offline - upload signature again
        if(subscription.status==7)
        {
            documents = await models.DocumentsForSignature.findAll({
                attributes:['id','amendmentId','docType','filePath'],
                where:{
                    subscriptionId:subscriptionId,
                    docType:['FUND_AGREEMENT','AMENDMENT_AGREEMENT','SUBSCRIPTION_AGREEMENT'],
                    deletedAt:null,
                    isActive:1,
                    isArchived:0,
                    isPrimaryLpSigned:1
                },            
                include:[{
                    model:models.FundAmmendment,
                    as:'dfsamendments',
                    required:false
                }]
    
            })
            
            for (let documentforsignature of documents) { 
                let filename = ''
                if(documentforsignature.docType=='AMENDMENT_AGREEMENT'){  
                    filename = documentforsignature.dfsamendments.document.originalname
                }
                let tempObj = {}
                let url =  `${config.get('serverURL')}/api/v1/document/view/docs/${documentforsignature.id}`
                tempObj.docType = documentforsignature.docType
                tempObj.documentId = documentforsignature.id
                tempObj.url = url
                tempObj.filename = filename
                tempObj.amendmentId = documentforsignature.amendmentId
                docs.push(tempObj)
            }               
            
        }
    else{
     
        documents = await models.DocumentsForSignature.findAll({
            attributes:['id','amendmentId','docType','filePath'],
            where:{
                subscriptionId:subscriptionId,
                docType:['FUND_AGREEMENT','AMENDMENT_AGREEMENT','SUBSCRIPTION_AGREEMENT'],
                deletedAt:null,
                isActive:1,
                isPrimaryLpSigned:0
            },            
            include:[{
                model:models.FundAmmendment,
                as:'dfsamendments',
                required:false
            }]

        })
        
        for (let documentforsignature of documents) { 
            let filename = ''
            if(documentforsignature.docType=='AMENDMENT_AGREEMENT'){  
                filename = documentforsignature.dfsamendments.document.originalname
            }
            let tempObj = {}
            let url =  `${config.get('serverURL')}/api/v1/document/view/docs/${documentforsignature.id}`
            tempObj.docType = documentforsignature.docType
            tempObj.documentId = documentforsignature.id
            tempObj.url = url
            tempObj.filename = filename
            tempObj.amendmentId = documentforsignature.amendmentId
            docs.push(tempObj)
        }       

        //check closed ready user or inprogress user
        let signatureTrackCount = await models.SignatureTrack.count({
            where: {                
                subscriptionId: subscriptionId,                    
                documentType: 'SUBSCRIPTION_AGREEMENT'    
            }
        })          

        //for invited/inprogress user first time
        if(  (subscription.status==1 || subscription.status==2) && signatureTrackCount==0 ){
            let tempObj = {}
            let url =  `${config.get('serverURL')}/api/v1/document/view/sa/${subscriptionId}`
            tempObj.docType = 'SUBSCRIPTION_AGREEMENT'
            tempObj.documentId = 0
            tempObj.url = url
            tempObj.filename = ''
            docs.push(tempObj) 
            
            let tempObj1 = {}
            let url1=  `${config.get('serverURL')}/api/v1/document/view/fa/${subscriptionId}`
            tempObj1.docType = 'FUND_AGREEMENT'
            tempObj1.documentId = 0
            tempObj1.url = url1
            tempObj1.filename = ''
            docs.push(tempObj1)          

        }


            //check for amendments
            let getAmmendments = await models.FundAmmendment.findAll({
                attributes:['id','document'],
                where:{
                    fundId:subscription.fundId,
                    deletedAt:null,
                    isActive:1,
                    isAmmendment:true,
                    isAffectuated:true
                }
            })
            
            if(getAmmendments){        
                for (let ammendmentAgreement of getAmmendments) {

                    let aaCount = await models.DocumentsForSignature.count({
                        where: {
                            fundId: subscription.fundId,
                            subscriptionId: subscriptionId,                    
                            docType: 'AMENDMENT_AGREEMENT',
                            isActive: 1,
                            amendmentId:ammendmentAgreement.id,    
                        }
                    })              
                    
                    if(aaCount==0){

                    //insert records to documentsforsignatures 
                    let dirToCheck = `./assets/funds/${subscription.fundId}/${subscriptionId}`
                    commonHelper.ensureDirectoryExistence(dirToCheck)
                    let revisedPath = dirToCheck+`/OFFLINE_AMENDMENT_AGREEMENT_${uuidv1()}${subscriptionId}.pdf`
                    fs.copyFileSync(ammendmentAgreement.document.path,revisedPath)                       

                    //upload doc           
                    let data =  {
                        fundId: subscription.fundId,
                        subscriptionId: subscriptionId,
                        filePath: revisedPath,
                        docType: 'AMENDMENT_AGREEMENT',
                        isAllLpSignatoriesSigned:false,
                        gpSignCount: 0,
                        lpSignCount: 0,
                        isPrimaryGpSigned: 0,
                        isPrimaryLpSigned: false,
                        isActive: 1,
                        createdBy: req.userId,
                        updatedBy: req.userId,
                        amendmentId:ammendmentAgreement.id
                    }
                     
                    //save to documentforsignatures
                    let documentInfo = await models.DocumentsForSignature.create(data) 

                    let tempObj1 = {} 
                    tempObj1.docType = 'AMENDMENT_AGREEMENT'
                    tempObj1.documentId = documentInfo.id
                    tempObj1.url = `${config.get('serverURL')}/api/v1/document/view/docs/${documentInfo.id}`
                    tempObj1.filename = ammendmentAgreement.document  && ammendmentAgreement.document.originalname ? ammendmentAgreement.document.originalname : ''
                    tempObj1.amendmentId = ammendmentAgreement.id
                    docs.push(tempObj1)

                    }
                }
            }       
            
        }

        let saDocument = _.find(docs,{docType:'SUBSCRIPTION_AGREEMENT'})
        let faDocument = _.find(docs,{docType:'FUND_AGREEMENT'})                   
        let atDocument = _.filter(docs,{docType:'AMENDMENT_AGREEMENT'})
        
        return res.json({
            subscriptionAgreementUrl: saDocument ? saDocument : '',
            fundAgreementUrl: faDocument ? faDocument : '',                 
            atDocument:atDocument ? atDocument : ''
        })

    } catch (e) {
        next(e);
    }
}


const offlineSubscriptionUploadController = async (req, res, next) => {
    try {

        const { subscriptionId, fundId,date, timeZone } = req.body;

        //const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);
        const signedDateconvertedToUserTimezone = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
     
        const ext = path.extname(req.file.originalname);

        const subscription = await models.FundSubscription.findOne({
            where:{
                id:subscriptionId
            }
        })
        
        await models.DocumentsForSignature.update({ isActive: 0 }, {
            where: {
                subscriptionId:subscriptionId,
                docType: 'SUBSCRIPTION_AGREEMENT',
            }
        })

        const createdDocument = await models.DocumentsForSignature.create({
            fundId: fundId,
            subscriptionId: subscriptionId,
            changeCommitmentId: null,
            filePath: req.file.path,
            docType: 'SUBSCRIPTION_AGREEMENT',
            gpSignCount: 1,
            lpSignCount: 1,
            isPrimaryGpSigned: false,
            isAllLpSignatoriesSigned: true,
            isAllGpSignatoriesSigned: false,
            isPrimaryLpSigned: true,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        });

        models.SignatureTrack.create({
            documentType: 'SUBSCRIPTION_AGREEMENT',
            documentId: createdDocument.id,
            signaturePath: '',
            subscriptionId: subscriptionId,
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: timeZone,
            signedDateInGMT: date,
            signedUserId:subscription.offlineUserId,
            signedByType: 'OfflineLP',
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        })

        const subscriptionBlobTempUrl = commonHelper.getLocalUrl(createdDocument.path);

        return res.json({
            urlOfSubscriptionAgreement: subscriptionBlobTempUrl,
        });

    } catch (e) {
        next(e);
    }
}


const offlineLpSignaturesUploadController = async (req, res, next) => {
    try {

        const { subscriptionId, fundId,date, timeZone } = req.body;

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        })
        //const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);
        const signedDateconvertedToUserTimezone = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')

        let lpSignedPage = req.file.path
        const ext = path.extname(req.file.path);

        const lpSignedLastPage = path.resolve(__dirname, `../${lpSignedPage}`);

        const fundAgreementTempPath = await fundAgreementSign(fund.partnershipDocument.path, lpSignedLastPage);
        const fundAgreementPath = `./assets/funds/${fund.id}/${subscriptionId}/FUND_AGREEMENT_${uuidv1()}.pdf`;
        fs.copyFileSync(fundAgreementTempPath.path, fundAgreementPath);
        
        const fundAgreementTempUrl = commonHelper.getLocalUrl(fundAgreementPath);

        const subscription = await models.FundSubscription.findOne({
            where:{
                id:subscriptionId
            }
        })

        await models.DocumentsForSignature.update({ isActive: 0 }, {
            where: {
                subscriptionId:subscriptionId,
                docType: 'FUND_AGREEMENT',
            }
        })        

        const createdDocument = await models.DocumentsForSignature.create({
            fundId,
            subscriptionId,
            changeCommitmentId: null,
            filePath: fundAgreementPath,
            isAllLpSignatoriesSigned: true,
            isAllGpSignatoriesSigned: true,
            docType: 'FUND_AGREEMENT',
            gpSignCount: 1,
            lpSignCount: 1,
            isPrimaryGpSigned: 0,
            isPrimaryLpSigned: true,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        });

        await models.SignatureTrack.create({
            documentType: 'FUND_AGREEMENT',
            documentId: createdDocument.id,
            signaturePath: lpSignedLastPage,
            subscriptionId: subscriptionId,
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: timeZone,
            signedDateInGMT: date,
            signedUserId: subscription.offlineUserId,
            signedByType: 'OfflineLP',
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        })


        // Update the status to close-ready=7
        await models.FundSubscription.update({ status: 7, updatedBy: req.userId }, {
            where: {
                id: subscriptionId,
                fundId
            }
        });

        // return res.json({
        //     subscriptionUrl:`${config.get('serverURL')}/${subscriptionAgreementTempPath.path}`,
        //     fundAgreementUrl:`${config.get('serverURL')}/${fundAgreementTempPath.path}`
        // });

        return res.json({
            urlOfPartnershipAgreement: fundAgreementTempUrl
        })
    } catch (e) {
        next(e);
    }
}


const offlineLpCapitalCommitment = async (req, res, next) => {
    try {

        let { fundId, subscriptionId, amount,date, timeZone } = req.body;

        //const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);
        const signedDateconvertedToUserTimezone = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')

        await models.ChangeCommitment.update({ isActive: 0 }, { where: { fundId, subscriptionId } })

        const subscription = await models.FundSubscription.findOne({
            where:{
                id:subscriptionId
            }
        });

        const changeCommitment = await models.ChangeCommitment.create({ fundId, subscriptionId, amount, isActive: 1 });

        let capitalCommitmentPath = req.file.path

        const ext = path.extname(capitalCommitmentPath);

        await models.DocumentsForSignature.update({ isActive: 0 }, {
            where: {
                fundId: changeCommitment.fundId,
                subscriptionId: changeCommitment.subscriptionId,
                docType: 'CHANGE_COMMITMENT_AGREEMENT',
            }
        });

        const findIfSignaturesExist = await models.GpSignatories.count({
            where:{
                fundId: changeCommitment.fundId,
                gpId:req.userId
            }
        })

        let isAllGpSignatoriesSigned = findIfSignaturesExist? 0:1;

        const document = await models.DocumentsForSignature.create({
            fundId: changeCommitment.fundId,
            subscriptionId: changeCommitment.subscriptionId,
            changeCommitmentId: changeCommitment.id,
            filePath: req.file.path,
            docType: 'CHANGE_COMMITMENT_AGREEMENT',
            gpSignCount: 0,
            lpSignCount: 1,
            isPrimaryGpSigned: 0,
            isPrimaryLpSigned: 1,
            isAllGpSignatoriesSigned,
            isAllLpSignatoriesSigned:1,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        })


        
        await models.SignatureTrack.create({
            documentType: 'CHANGE_COMMITMENT_AGREEMENT',
            documentId: document.id,
            subscriptionId : document.subscriptionId,
            signaturePath: '',
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: req.body.timeZone,
            signedDateInGMT: req.body.date,
            signedUserId: subscription.offlineUserId,
            signedByType: 'OfflineLP',
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        });

        return res.json({
            filePathTempPublicUrl: commonHelper.getLocalUrl(req.file.path)
        });
    

    } catch (e) {
        next(e);
    }
}

const offlineSideletterUploadController = async (req, res, next) => {
    try {
        const { fundId, subscriptionId,date, timeZone } = req.body;

        //const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);
        const signedDateconvertedToUserTimezone = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
        let filePath = req.file.path;
        let filename = req.file.filename;
        let ext = path.extname(filename) 

        const addSideLetter = await models.SideLetter.create({
            fundId: fundId,
            subscriptionId: subscriptionId,
            isActive:1,
            sideLetterDocumentPath:filePath
        }) 

        const subscription = await models.FundSubscription.findOne({
            where:{
                id:subscriptionId
            }
        })

        await models.DocumentsForSignature.update({ isActive: 0 }, {
            where: {
                fundId: fundId,
                subscriptionId:subscriptionId,
                docType: 'SIDE_LETTER_AGREEMENT',
            }
        })

        const document = await models.DocumentsForSignature.create({
            fundId: fundId,
            subscriptionId: subscriptionId,
            sideletterId: addSideLetter.id,
            filePath: filePath,
            docType: 'SIDE_LETTER_AGREEMENT',
            gpSignCount: 0,
            lpSignCount: 0,
            isPrimaryGpSigned: 1,
            isPrimaryLpSigned: 1,
            isAllGpSignatoriesSigned:1,
            isAllLpSignatoriesSigned:1,
            isCurrentUserSigned:1,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        });     

        await models.SignatureTrack.bulkCreate([{
            documentType: 'SIDE_LETTER_AGREEMENT',
            documentId: document.id,
            subscriptionId : document.subscriptionId,
            signaturePath: '',
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: req.body.timeZone,
            signedDateInGMT: req.body.date,
            signedUserId: req.userId,
            signedByType: 'GP',
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        },{
            documentType: 'SIDE_LETTER_AGREEMENT',
            documentId: document.id,
            subscriptionId : document.subscriptionId,
            signaturePath: '',
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: req.body.timeZone,
            signedDateInGMT: req.body.date,
            signedUserId: subscription.offlineUserId,
            signedByType: 'OfflineLP',
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        }]);

        return res.json({
            message:messageHelper.sideLetterUpload
        })

    } catch (e) {
        next(e);
    }
}

const offlinefundAmendmentSignatures = async (req,res,next) =>{
    try{

        
        let { fundId,subscriptionId,documents,date,docType, timeZone } = req.body;

        if(documents.length){

            documents= JSON.parse(documents);
        }else{
            return errorHelper.error_400(res, 'No Docs Found', 'No Documents found.');
        }

        const signedDateconvertedToUserTimezone = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z');

        let lpSignedPage = req.file.path;

        const lpSignedLastPage = path.resolve(__dirname, `../${lpSignedPage}`);

        const subscription = await models.FundSubscription.findOne({
            where:{
                id:subscriptionId
            },
            include:[{
                model: models.OfflineUsers,
                as: 'offlineLp',
                required: false
            },{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.User,
                    as: 'gp',
                    required: true
                }]
            }]
        });


        documents.map(async document=>{

            let amendmentId = document.amendmentId
            let updatedFilePath = document.filePath
            //append the signtaure
            if(amendmentId>0){

                let ammendmentInfo = await models.FundAmmendment.findOne({ 
                    attributes:['document'],                
                    where:{
                        id:amendmentId
                    }
                })      
                
                let originalFilePath = ammendmentInfo.document.path
                let tempPath = await fundAgreementSign(originalFilePath, lpSignedLastPage);
                let destinationPath = `./assets/funds/${fundId}/${subscriptionId}/AMENDMENT_AGREEMENT_${uuidv1()}.pdf`;
                fs.copyFileSync(tempPath.path, destinationPath)
                updatedFilePath = destinationPath           

            }

            await models.DocumentsForSignature.update({
                isPrimaryLpSigned: true,
                lpSignedDate: moment().format('MM/DD/YYYY HH:mm:ss.ssss Z'),
                filePath:updatedFilePath
            },{
                where:{
                    id:document.documentId,
                    fundId,
                    subscriptionId,
                    isActive:1,
                    isArchived:0
                }
            });
    
            await models.SignatureTrack.create({
                documentType: docType,
                documentId: document.documentId,
                signaturePath: lpSignedLastPage,
                subscriptionId: subscriptionId,
                ipAddress: req.ipInfo,
                signedDateTime: signedDateconvertedToUserTimezone,
                timezone: timeZone,
                signedDateInGMT: date,
                signedUserId: subscription.offlineUserId,
                signedByType: 'OfflineLP',
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            });


            if(subscription.status == 10){
                //get closed investors data
                let fundInvestors = await getFundInvestorsData(subscription.fundId);
                
                //caliculate percentage data
                calculateAmendmentPercentages(subscription,fundInvestors,document.amendmentId,docType,document.documentId);
    
            }else{
    
                //change inprogress to close ready
                //changeInvestorToCloseReady(subscription);
                await models.FundSubscription.update({ 
                    status: 7,
                    updatedBy: req.userId 
                }, { 
                    where: { 
                        id: subscription.id,
                        fundId 
                    } 
                });

            }
        });


        return res.send('Success');
    }catch(e){
        next(e);
    }
}

async function fundAgreementSign(fundAgreementTempFile, signedLastPage) {
    // const fileName = `fund/FUND_AGREEMENT_${uuidv1()}.pdf`;
    const fileName = `FUND_AGREEMENT_${uuidv1()}.pdf`
    return await mergeAgreementDocAndSigndPage(fileName, fundAgreementTempFile, signedLastPage);
}


async function mergeAgreementDocAndSigndPage(fileName, agreementTempFile, lpSignedLastPage) {
    // const outputFile =  `assets/offlineUserUploads/${fileName}`;
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
    await commonHelper.pdfMerge([agreementTempFile, lpSignedLastPage], outputFile);
    return { path: outputFile, name: fileName };
}

const calculateAmendmentPercentages = async(subscription,totalClosedInvetors,amendmentId,documentType,documentId) => {

    console.log("********amendmentId*******",amendmentId)
    console.log("********documentId*******",documentId)

    let closedCommitment=0;
    let approvedPercentage=0;

    //Remove this model if already fetched  in the begining
    const amendmentInfo = await models.FundAmmendment.findOne({
        where:{
            id: amendmentId,
            isArchived:0,
            isActive:1
        }
    });
    
    let documents = await models.DocumentsForSignature.findAll({
        where: {
            fundId: subscription.fund.id,
            isActive: 1,
            isPrimaryLpSigned:1,
            docType: documentType,
            amendmentId
        }
    });

    let subscriptionIds = _.map(documents,'subscriptionId');


    let fundInvestors = await models.FundSubscription.findAll({
        where: {
            fundId: subscription.fund.id,
            id: {
                [Op.in]:subscriptionIds
            },
            deletedAt: null,
            status: 10
        },
        include: [{
            model: models.Fund,
            as: 'fund',
            required: true,
        },{
            attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
            model: models.FundClosings,
            required: false,
            as: 'fundClosingInfo',
            where: {
                isActive: 1
            }
        }, {
            attributes: ['id', 'name', 'lpSideName'],
            model: models.FundStatus,
            as: 'subscriptionStatus',
            required: true
        }]
    });

    await models.DocumentsForSignature.update({
        lpSignedDate: moment().format('MM/DD/YYYY HH:mm:ss.ssss Z')
    },{
        where:{
            fundId: subscription.fund.id,
            isActive: 1,
            isPrimaryLpSigned:1,
            docType: documentType,
            id:documentId
        }
    });
    
    totalClosedInvetors.forEach(closedInvetor=>{
        closedCommitment = closedCommitment +(_.last(closedInvetor.fundClosingInfo) ? _.last(closedInvetor.fundClosingInfo).gpConsumed:0);

    });

    fundInvestors.forEach(fundInvestor=>{
        
        const gpCommitmentPercent = (fundInvestor.fund.percentageOfLPAndGPAggregateCommitment !== '0') ? (100 - fundInvestor.fund.percentageOfLPAndGPAggregateCommitment) : 0;
        
        const closedFMCommitmentValue = fundInvestor.fund.percentageOfLPCommitment !== '0' ? ((fundInvestor.fund.percentageOfLPCommitment / 100) * closedCommitment) :
            (fundInvestor.fund.capitalCommitmentByFundManager !== 0.00 ? fundInvestor.fund.capitalCommitmentByFundManager :
                (fundInvestor.fund.percentageOfLPAndGPAggregateCommitment !== '0' ? (closedCommitment * (100 / gpCommitmentPercent)) - closedCommitment : ''));   

        let investorCommitment =  _.last(fundInvestor.fundClosingInfo).gpConsumed 
                ? _.last(fundInvestor.fundClosingInfo).gpConsumed 
                : fundInvestor.lpCapitalCommitment

        let denominator= amendmentInfo.considerOnlyLpOfferedAmount ? closedCommitment : (closedCommitment + closedFMCommitmentValue);


        approvedPercentage =  approvedPercentage+((investorCommitment/denominator) * 100)
    });


    let docType = amendmentInfo.isAmmendment ? 'Amendment' :'Restated Fund Agreement'

    if(approvedPercentage > amendmentInfo.targetPercentage){

        emailHelper.sendEmail({
            toAddress: subscription.fund.gp.email,
            subject: 'Email Notification - Vanilla',
            data: {
                name: commonHelper.getFullName(subscription.fund.gp) ,
                fundCommonName: subscription.fund.fundCommonName,
                clientURL: config.get('clientURL'),
                docType,
                user:subscription.fund.gp
            },
            htmlPath: "alerts/gpTargetPercentageReached.html"
        });

        const alertFrom = subscription.offlineLp.id;
        const alertTo = subscription.fund.gp.id;
    
        const alertData = {
            fundManager: commonHelper.getFullName(subscription.fund.gp),
            htmlMessage: messageHelper.gpTargetPercentageReached,
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            fundCommonName: subscription.fund.fundCommonName,
            docType: docType,
            sentBy: subscription.offlineLp.id
        };
        notificationHelper.triggerNotification(null, alertFrom, alertTo, [], alertData, subscription.fund.gp.accountId,subscription.fund.gp);

    }

    await models.FundAmmendment.update({
        approvedPercentage:_.round(approvedPercentage,2)
    },{
        where:{
            id:amendmentId,
            isArchived:0,
            isActive:1
        }
    });
}


const getFundInvestorsData = async (fundId) => {

    return await models.FundSubscription.findAll({
        where: {
            fundId: fundId,
            deletedAt: null,
            status: 10
        },
        include: [{
            model: models.Fund,
            as: 'fund',
            required: true,
        },{
            attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
            model: models.FundClosings,
            required: false,
            as: 'fundClosingInfo',
            where: {
                isActive: 1
            }
        }, {
            attributes: ['id', 'name', 'lpSideName'],
            model: models.FundStatus,
            as: 'subscriptionStatus',
            required: true
        }]
    })

}


const changeInvestorToCloseReady = async(subscription) => {

    //for non-closed investors check
    if(subscription.status !=10){
        let documentsForSignature = await models.DocumentsForSignature.count({ 
            where: { 
                    subscriptionId:subscription.id,
                    isActive:1,
                    isPrimaryLpSigned:0
                }        
        });

        //if there are no documents for signature , then inprogress user will change to close-ready
        if(documentsForSignature == 0){
            await models.FundSubscription.update({ status: 7,updatedBy: req.userId }, { where: { id: subscription.id,fundId } })
        }
    }
}


module.exports = {
    offlineSubscriptionUploadController,
    offlineSideletterUploadController,
    offlineLpSignaturesUploadController,
    offlineLpCapitalCommitment,
    getOfflinePendingDocuments,
    offlinefundAmendmentSignatures

}