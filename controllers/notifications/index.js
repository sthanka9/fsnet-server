const models = require('../../models/index');
const messageHelper = require('../../helpers/messageHelper');
const errorHelper = require('../../helpers/errorHelper');
const { Op } = require('sequelize')
const _ = require('lodash');

const getNotifications = async (req, res, next) => {
    try {

        const id = Number(req.params.id);

        if (!id) {
            return errorHelper.error_400(res, 'id', messageHelper.userIdNotFound);
        }

        const fromUser = await models.User.findOne({ where: { id: id } });

        const notificationData = await models.Notification.findAll({
            where: {
                toUserId: id,
                isRead: 0
            },
            order: [
                ['id', 'DESC']
            ]
        })

        let result = [];
        for (let notifyData of notificationData) {
            let obj = {
                id: notifyData.id,
                fromUserId: notifyData.fromUserId,
                toUserId: notifyData.toUserId,
                fundId: notifyData.fundId,
                isRead: notifyData.isRead,
                notification: notifyData.notification,
                createdAt: notifyData.createdAt,
                firstName: fromUser.firstName,
                lastName: fromUser.lastName,
                middleName: fromUser.middleName,
                accountType: fromUser.accountType,
            }
            result.push(obj);
        }

        return res.json({ data: result })

    } catch (e) {
        next(e);
    }
}

const dismissNotification = async (req, res, next) => {
    try {
        const { notificationId, isRead } = req.body;

        if (isRead === undefined) {
            return errorHelper.error_400(res, 'isRead', messageHelper.isReadAllowedReq);
        }

        const notification = await models.Notification.findOne({
            where: {
                id: notificationId
            }
        });

        // notification not valid
        if (!notification) {

            return errorHelper.error_400(res, 'notificationId', messageHelper.notificationNotFound);

        }

        await models.Notification.update(
            { isRead: isRead ? 1 : 0 },
            { where: { id: notificationId } }
        );

        return res.json({
            error: false,
            message: messageHelper.notificationDismissed
        });


    } catch (e) {
        next(e);
    }
}

const dismissAllNotifications = async (req, res, next) => {
    try {
        const userId = req.userId;

        await models.Notification.findOne({
            where: {
                id: userId
            }
        });

        await models.Notification.update(
            { isRead: 1 },
            { where: { toUserId: userId } }
        );

        return res.json({
            error: false,
            message: messageHelper.notificationDismissed
        });


    } catch (e) {
        next(e);
    }
}

//get notifications for an account 
const getAccountNotifications = async (req, res, next) => {
    try {
        //get account id
        const id = Number(req.params.id);

        //id not found
        if (!id) {
            return errorHelper.error_400(res, 'id', messageHelper.userIdNotFound);
        }

        //get account details
        const fromAccount = await models.User.findOne({ attributes:['accountId'],where: { id: id } });   

        //account not found
        if (!fromAccount) {
            return errorHelper.error_400(res, 'id', messageHelper.userIdNotFound);
        }

        //get all notifications data by account
        const notificationData = await models.Notification.findAll({
            where: {
                accountId: fromAccount.accountId,
                isRead: 0
            },
            order: [
                ['id', 'DESC']
            ]
        })

        //get all user roles and names
        let toUserIds = []
        let userInfo = {}

        //get all users ids
        for (let notifyData of notificationData) {
            toUserIds.push(notifyData.toUserId)
        }  
        //remove duplicates      
        toUserIds = _.uniq(toUserIds)

        //get all users role name
        let getAllUserInfo = await models.User.findAll({
            attributes: ['id','accountType'],
            where: { id: { [Op.in]: toUserIds } }
        })   
        
        //build an array with user id and role name
        for (let userinfo of getAllUserInfo) {
            userInfo[userinfo.id] = userinfo.accountType
        }        

        let result = [];
        //send result
        for (let notifyData of notificationData) {
            let obj = {
                id: notifyData.id,
                fromUserId: notifyData.fromUserId,
                toUserId: notifyData.toUserId,
                fundId: notifyData.fundId,
                isRead: notifyData.isRead,
                notification: notifyData.notification,
                createdAt: notifyData.createdAt,
                firstName: fromAccount.firstName,
                lastName: fromAccount.lastName,
                middleName: fromAccount.middleName,
                accountType: userInfo[notifyData.toUserId],
                accountId:notifyData.accountId
            }
            result.push(obj);
        }
        return res.json({ data: result })
    } catch (e) {
        next(e);
    }
}

//dismiss all account notifications
const dismissAllAccountNotifications = async (req, res, next) => {
    try {        
        const userId = req.userId;
        //get account details
        const fromAccount = await models.User.findOne({ attributes:['accountId'],where: { id: userId } });   
        const accountId = fromAccount.accountId     

        //update all notfications as read by account id
        await models.Notification.update(
            { isRead: 1 },
            { where: { accountId: accountId } }
        );
        
        //send result
        return res.json({
            error: false,
            message: messageHelper.notificationDismissed
        })

    } catch (e) {
        next(e);
    }
}

module.exports = {
    getNotifications,
    getAccountNotifications,
    dismissNotification,
    dismissAllNotifications,
    dismissAllAccountNotifications
}