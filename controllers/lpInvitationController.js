
const models = require('../models/index');
const _ = require('lodash');
const config = require("../config");
const { Op } = require('sequelize');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const eventHelper = require('../helpers/eventHelper');
const notificationHelper = require('../helpers/notificationHelper');

const acceptFundSubscription = async (req, res, next) => {


    try {
        const { subscriptionId } = req.body;
        const userId = req.userId;

        if (!subscriptionId) {
            return errorHelper.error_400(res, 'subscriptionId', messageHelper.subscriptionIdReq);
        }

        const subscription =  await models.FundSubscription.getSubscription(subscriptionId, models);

        if (!subscription) {
            return errorHelper.error_400(res, 'subscriptionId', messageHelper.subscriptionIdReq);
        }

        // populate profile mailing address to the lp subscription
        const user = await models.User.findOne({
            where: {
                id: userId
            }
        });

        const data = await models.FundSubscription.update({
            status: 2,
            isSubscriptionContentUpdated:0,
            mailingAddressCountry: user.country, mailingAddressState: user.state,
            mailingAddressCity: user.city, mailingAddressZip: user.zipcode,
            mailingAddressPhoneNumber: user.cellNumber,
            mailingAddressStreet: user.streetAddress1 ? user.streetAddress1 : '' +' '+user.streetAddress2 ? user.streetAddress2 : '',
            updatedBy: req.userId
        }, {
                where: {
                    id: subscriptionId
                }
            });

        const status = 'accepted';

        await models.History.create({
            accountType: user.accountType,
            userId: user.id,
            fundId: subscription.fundId,
            action: 'Accepted',
            updatedBy: userId,
            createdBy: userId
        });

        let isNotificationsEnabled = commonHelper.isNotificationsEnabledForFundGP(subscription.fundId)

        if(isNotificationsEnabled){
            await _triggerFundInvitationEmailAlertToGp(subscription, status);
        }

        await _triggerFundInvitationAlertToGpAndToDelegates(req, subscription, status);

        eventHelper.saveEventLogInformation('Invitation Accepted: Investor', subscription.fundId, subscription.id, req);

        return res.json(data);

    } catch (error) {
        next(error);
    }
}


const _triggerFundInvitationEmailAlertToGp = (subscription, status) => {
    if (subscription.fund.gp.isEmailNotification) {
        emailHelper.sendEmail({
            toAddress: subscription.fund.gp.email,
            subject: `Email Notification - Vanilla`,
            data: {
                lpName: commonHelper.getInvestorName(subscription),
                lpStatus: status,
                fundName: subscription.fund.legalEntity,
                fundManagerName: commonHelper.getFullName(subscription.fund.gp),
                user:subscription.fund.gp
            },
            htmlPath: "fundInvitationLpActionStatus.html"
        }).catch(error => {
            commonHelper.errorHandle(error)
        });
    }
}

const _triggerFundInvitationAlertToGpAndToDelegates = async (req, subscription, status) => {

    const gpDelegatesSelected = await models.GpDelegates.findAll({
        where: {
            fundId: subscription.fund.id
        },
        include: [{
            model: models.User,
            as: 'details',
            required: true
        }]
    })

    let gpDelegates = [];
    gpDelegatesSelected.map(i => {
        gpDelegates.push({
            id: i.details.delegateId,
            firstName: i.details.firstName,
            lastName: i.details.lastName,
            middleName: i.details.middleName,
            email: i.details.email,
            isEmailNotification: i.details.isEmailNotification,
            accountId:i.details.accountId
        })
    })

    const alertData = {
        htmlMessage: status == 'accepted' ? messageHelper.acceptedToJoinFund : messageHelper.rejectedToJoinFund,
        firstName: subscription.lp.firstName,
        middleName: subscription.lp.middleName,
        lastName: subscription.lp.lastName,
        lpName: await commonHelper.getInvestorName(subscription),
        lpId: subscription.lp.id,
        subscriptionId: subscription.id,
        fundManagerCommonName: subscription.fund.fundManagerCommonName,
        fundName: subscription.fund.fundCommonName,
        fundId: subscription.fund.id,
        sentBy: req.userId
    };
    /*
    let getGpAccountInfo = await models.User.findOne({
        attributes:['accountId'],
        where: {
            id: subscription.fund.gp.id
        }
    })*/    

    notificationHelper.triggerNotification(req, req.userId, subscription.fund.gp.id, gpDelegates, alertData,subscription.fund.gp.accountId,subscription.fund.gp);

}


const rejectFundSubscription = async (req, res, next) => {

    try {
        const { subscriptionId } = req.body;

        if (!subscriptionId) {
            return errorHelper.error_400(res, 'subscriptionId', messageHelper.subscriptionIdReq);
        }

        const subscription =  await models.FundSubscription.getSubscription(subscriptionId, models);

        if (!subscription) {
            return errorHelper.error_400(res, 'subscriptionId', messageHelper.subscriptionIdReq);
        }

        const data = await models.FundSubscription.update({ status: 3, updatedBy: req.userId }, {
            where: {
                id: subscriptionId
            }
        });


        const status = 'declined';
        const gpDelegates = await getGpDelegatesIfExist(subscription.fund);
        await _triggerFundInvitationAlertToGpAndToDelegates(req, subscription, status);
        await _triggerEmailToGpAndGpDelegates(subscription,gpDelegates);

        return res.json('success');

    } catch (error) {
        next(error);
    }
}

const reinviteLp = async (req, res, next) => {
    try {

        const { subscriptionId, fundId } = req.body;

        let subscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId,
                deletedAt: null
            },
            include: [{
                attributes: ['id', 'lastName','firstName','middleName','email','isEmailConfirmed','emailConfirmCode','accountId'],
                model: models.User,
                as: 'lp',
                required: true
            }, {
                attributes: ['id', 'fundCommonName'],
                model: models.Fund,
                as: 'fund',
                required: true,
            }]
        });

        if(!subscription) {
            return res.status(404).json({
                "errors": [
                    {
                        "msg": messageHelper.fundNotFound
                    }
                ]
            });
        }


        if(subscription.status ==3){
            //updating to default status when declined
            await models.FundSubscription.update({ status: 1, updatedBy: req.userId }, {
                where: {
                    id: subscriptionId,
                    fundId
                }
            });
        }else{
            //updating to in progress when rescinded
            await models.FundSubscription.update({ status: 2, updatedBy: req.userId }, {
                where: {
                    id: subscriptionId,
                    fundId
                }
            });
        }

        const loginOrRegisterLink = subscription.lp.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${subscription.lp.emailConfirmCode}`;
        const content = subscription.lp.isEmailConfirmed ? "to login and get going." : "to setup your account and get going." 

         // GP has invited you to join fund (existing account) - LP
         const alertData = {
            htmlMessage: messageHelper.invitationToJoinParticularFundAlertMessage,
            message: 'Invitation to join particular fund',
            fundManagerCommonName: subscription.fund.fundManagerCommonName,
            fundName: subscription.fund.fundCommonName,
            fundId: subscription.fund.id,
            sentBy: req.userId,
            type:'FUND_INVITED'
        };

        notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertData,subscription.lp.accountId,subscription.lp);

        emailHelper.sendEmail({
            toAddress: subscription.lp.email,
            subject: messageHelper.addLPEmailSubject,
            data: {
                name: commonHelper.getFullName(subscription.lp),
                loginOrRegisterLink: loginOrRegisterLink,
                fundCommonName: subscription.fund.fundCommonName,
                content,
                user:subscription.lp
            },
            htmlPath: "fundLPInviation.html"
        }).catch(error => {
            commonHelper.errorHandle(error)
        })

        return res.json({
            data: 'Status updated successfully.'
        });


    } catch (error) {
        next(error);
    }
}

async function getGpDelegatesIfExist(fund) {
    let gpDelegates = false;

    let gpDelegatesIDs = await models.GpDelegates.findAll({
        attributes: ['delegateId'],
        where: {
            fundId: fund.id,
            gpId: fund.gp.id
        }
    });

    if (gpDelegatesIDs || gpDelegatesIDs.length > 0) {
        gpDelegatesIDs = _.map(gpDelegatesIDs, 'delegateId')
        return await models.User.findAll({
            where: {
                id: {

                    [Op.in]: gpDelegatesIDs

                }
            }
        })
    } else {
        return gpDelegates;
    }
}

function _triggerEmailToGpAndGpDelegates(subscription,gpDelegates) { 
    
    let isNotificationsEnabled = commonHelper.isNotificationsEnabledForFundGP(subscription.fundId)

     
    if(subscription.fund.gp.isEmailNotification && isNotificationsEnabled){
        emailHelper.sendEmail({
            toAddress: subscription.fund.gp.email,
            subject: 'Email Notification - Vanilla',
            data: {  
                fundManagerName: commonHelper.getFullName(subscription.fund.gp),
                lpName: commonHelper.getInvestorName(subscription),
                fundName: subscription.fund.legalEntity ,
                user:subscription.fund.gp
            },
            htmlPath: "alerts/rejectAlert.html"
        })
    }
    for (let gpDelegate of gpDelegates) {
        if (gpDelegate.isEmailNotification) { // if setting is enabled
            emailHelper.sendEmail({
                toAddress: gpDelegate.email,
                subject: 'Email Notification - Vanilla',
                data: {  
                    fundManagerName: commonHelper.getFullName(gpDelegate),
                    lpName: commonHelper.getInvestorName(subscription),
                    fundName: subscription.fund.legalEntity ,
                    user:gpDelegate
                },
                htmlPath: "alerts/rejectAlert.html"
            })
        }
    }
}

module.exports = {
    acceptFundSubscription,
    rejectFundSubscription,
    reinviteLp
}