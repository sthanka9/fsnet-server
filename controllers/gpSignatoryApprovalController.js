const models = require('../models/index');
const _ = require('lodash');
const moment = require('moment');
const path = require('path');
const uuidv1 = require('uuid/v1');
const { Op, literal } = require('sequelize');
const fs = require('fs');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const errorHelper = require('../helpers/errorHelper');


const signatoryApprovalForDocs= async (req, res, next) => { 

    const { subscriptionIds = [],  fundId, date, timeZone  } = req.body;

    const user = await models.User.findOne({ where: { id: req.userId } });

    if (!user && !user.signaturePic) {
        return errorHelper.error_400(res, 'id', messageHelper.signaturePicNotFound);
    }

    if (subscriptionIds.length <= 0) {
        return errorHelper.error_400(res, 'subscriptionIds', messageHelper.validDataCheck);
    }

    //let gpSignatoryPicObject = JSON.parse(user.signaturePic)
    //let signatorySignaturePic = commonHelper.getLocalUrl(gpSignatoryPicObject.path)   
    
    let gpSignatureUrl = user.signaturePicUrl
    let gpSignaturePath = user.signaturePic       
    
    const subscriptions = await models.FundSubscription.findAll({
        where: {
            id: {
                [Op.in] : subscriptionIds
            }
        },
        include: [{
            model: models.Fund,
            as: 'fund',
            required: true,
            include: [{
                model: models.User,
                as: 'gp',
                required: true,
            }]
        }, {
            model: models.User,
            as: 'lp',
            required: false
        },{
            model: models.OfflineUsers,
            as: 'offlineLp',
            required: false
        }]
    });


    for(const subscription of subscriptions) {

        const subscriptionSignInfo = await models.DocumentsForSignature.findOne({
            where: {
                isActive: 1,
                fundId: fundId,
                subscriptionId: subscription.id,
                docType: 'SUBSCRIPTION_AGREEMENT'
            }
        });

        const fundAgreementSignInfo = await models.DocumentsForSignature.findOne({
            where: {
                isActive: 1,
                fundId: fundId,
                subscriptionId: subscription.id,
                docType: 'FUND_AGREEMENT',
                isPrimaryLpSigned: 1,
                isArchived:0
            }
        });

        const amendmentAgreementSignInfo = await models.DocumentsForSignature.findOne({
            where: {
                isActive: 1,
                fundId: fundId,
                subscriptionId: subscription.id,
                docType: 'AMENDMENT_AGREEMENT',
                isPrimaryLpSigned: 1,
                isArchived:0
            }
        });


        const sideletterSignInfo = await models.DocumentsForSignature.findOne({
            where: {
                isActive: 1,
                fundId: fundId,
                subscriptionId: subscription.id,
                docType: 'SIDE_LETTER_AGREEMENT'
            }
        });        

        const subscriptionAgreementTempPath = await subscriptionAgreementSign(subscription, subscriptionSignInfo.filePath, gpSignatureUrl,req);
        const fundAgreementTempPath = await fundAgreementSign(subscription, fundAgreementSignInfo.filePath, gpSignatureUrl,req);
        
        fs.copyFileSync(subscriptionAgreementTempPath.path,subscriptionSignInfo.filePath);
        fs.copyFileSync(fundAgreementTempPath.path, fundAgreementSignInfo.filePath);

        const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone);
        
        //if amendment
        if(amendmentAgreementSignInfo){
            const amendmentAgreementTempPath = await fundAgreementSign(subscription, amendmentAgreementSignInfo.filePath, gpSignatureUrl,req)
            fs.copyFileSync(amendmentAgreementTempPath.path, amendmentAgreementSignInfo.filePath) 

            await models.DocumentsForSignature.update({
                fundId: subscription.fund.id,
                subscriptionId: subscription.id,
                changeCommitmentId: null,
                isAllGpSignatoriesSigned: subscriptionSignInfo.gpSignCount+1 === subscription.fund.noOfSignaturesRequiredForClosing,
                docType: 'AMENDMENT_AGREEMENT',
                gpSignCount: literal('gpSignCount + 1'),
                createdBy: req.userId,
                updatedBy: req.userId
            }, {
                    where: {
                        id: amendmentAgreementSignInfo.id
                    }
            })


            await models.SignatureTrack.create({
                documentType: 'AMENDMENT_AGREEMENT',
                documentId: amendmentAgreementSignInfo.id,
                signaturePath: gpSignaturePath,
                subscriptionId : amendmentAgreementSignInfo.subscriptionId,
                ipAddress: req.ipInfo,
                signedDateTime: signedDateconvertedToUserTimezone,
                timezone: req.body.timeZone,
                signedDateInGMT: req.body.date,
                signedUserId: req.user.id,
                signedByType: req.user.accountType,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            })   

        }

        

        await models.DocumentsForSignature.update({
            fundId: subscription.fund.id,
            subscriptionId: subscription.id,
            changeCommitmentId: null,
            isAllGpSignatoriesSigned: subscriptionSignInfo.gpSignCount+1 === subscription.fund.noOfSignaturesRequiredForClosing,
            docType: 'SUBSCRIPTION_AGREEMENT',
            gpSignCount: literal('gpSignCount + 1'),
            isPrimaryGpSigned: 0,
            createdBy: req.userId,
            updatedBy: req.userId
        }, {
                where: {
                    id: subscriptionSignInfo.id
                }
            });


        await models.DocumentsForSignature.update({
            fundId: subscription.fund.id,
            subscriptionId: subscription.id,
            changeCommitmentId: null,
            isAllGpSignatoriesSigned: subscriptionSignInfo.gpSignCount+1 === subscription.fund.noOfSignaturesRequiredForClosing,
            docType: 'FUND_AGREEMENT',
            gpSignCount: literal('gpSignCount + 1'),
            createdBy: req.userId,
            updatedBy: req.userId
        }, {
                where: {
                    id: fundAgreementSignInfo.id
                }
            });

        models.GpSignatoryApproval.create({
            fundId: subscription.fund.id,
            subscriptionId: subscription.id,
            signatoryId: req.userId,	
            gpId:	subscription.fund.gpId,
            lpId:subscription.lpId
        });


        models.SignatureTrack.bulkCreate([{
            documentType: 'FUND_AGREEMENT',
            documentId: fundAgreementSignInfo.id,
            signaturePath: gpSignaturePath,
            subscriptionId : fundAgreementSignInfo.subscriptionId,
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: req.body.timeZone,
            signedDateInGMT: req.body.date,
            signedUserId: req.user.id,
            signedByType: req.user.accountType,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        }, {
            documentType: 'SUBSCRIPTION_AGREEMENT',
            documentId: subscriptionSignInfo.id,
            signaturePath: gpSignaturePath,
            subscriptionId : subscriptionSignInfo.subscriptionId,
            ipAddress: req.ipInfo,
            signedDateTime: signedDateconvertedToUserTimezone,
            timezone: req.body.timeZone,
            signedDateInGMT: req.body.date,
            signedUserId: req.user.id,
            signedByType: req.user.accountType,
            isActive: 1,
            createdBy: req.userId,
            updatedBy: req.userId
        }]);

        //if subscription has sideletter
        if(sideletterSignInfo &&  subscription.fund.noOfSignaturesRequiredForClosing>1){
            
            let sideletterAgreemenPath = await sideletterAgreementSign(subscription, sideletterSignInfo.filePath, gpSignatureUrl,req);

            fs.copyFileSync(sideletterAgreemenPath.path,sideletterSignInfo.filePath);

            await models.DocumentsForSignature.update({
                fundId: subscription.fund.id,
                subscriptionId: subscription.id,
                changeCommitmentId: null,
                isAllGpSignatoriesSigned: sideletterSignInfo.gpSignCount+1 === subscription.fund.noOfSignaturesRequiredForClosing,
                filePath: sideletterSignInfo.filePath,
                docType: 'SIDE_LETTER_AGREEMENT',
                gpSignCount: literal('gpSignCount + 1'),
                isPrimaryGpSigned: 0,
                createdBy: req.userId,
                updatedBy: req.userId
            }, {
                    where: {
                        id: sideletterSignInfo.id
                    }
            });

            models.SignatureTrack.create({
                documentType: 'SIDE_LETTER_AGREEMENT',
                documentId: sideletterSignInfo.id,
                signaturePath: gpSignaturePath,
                subscriptionId : sideletterSignInfo.subscriptionId,
                ipAddress: req.ipInfo,
                signedDateTime: signedDateconvertedToUserTimezone,
                timezone: req.body.timeZone,
                signedDateInGMT: req.body.date,
                signedUserId: req.user.id,
                signedByType: req.user.accountType,
                isActive: 1,
                createdBy: req.userId,
                updatedBy: req.userId
            });            
    
        }

    }

    return res.json({
        error: false,
        message: 'Successfully updated.'
    })
}



async function generateSideLetterSignedPage(data, htmlFilePath) {
    const fileName = uuidv1();
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}.pdf`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}


// generate last page signed pdf from html and append to current sideletter
async function sideletterAgreementSign(subscription, subscriptionTempFile, signatorySignaturePic,req) {
    const fund = subscription.fund;
    const data = {
        gpName: commonHelper.getFullName(req.user),
        fundManagerTitle: fund.fundManagerTitle,
        fundManagerCommonName: fund.fundManagerCommonName,
        fundCommonName: fund.fundCommonName,
        fundLegalEntity:fund.legalEntity,
        fundManagerLegalEntityName:fund.fundManagerLegalEntityName,
        gpSignaturePic:signatorySignaturePic,
        date:moment().format('M/D/YYYY')
        
    };
    const htmlFilePath = "./views/sideletterAgreementGPSign.html";
    const signedLastPage = await generateSideLetterSignedPage(data, htmlFilePath);
    return await sideLettermergeAgreementDocAndSigndPage(subscriptionTempFile, signedLastPage);
}

async function sideLettermergeAgreementDocAndSigndPage(agreementTempFile,lpSignedLastPage) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
    await commonHelper.pdfMerge([agreementTempFile, lpSignedLastPage.path], outputFile)
    return { path: outputFile, name: fileName };
}

// generate last page sined pdf from html and append to current subscription
async function subscriptionAgreementSign(subscription, subscriptionTempFile, signatorySignaturePic,req) {
    const fund = subscription.fund;
    const data = {
        gpName: commonHelper.getFullName(req.user),
        fundManagerTitle: fund.fundManagerTitle,
        fundManagerCommonName: fund.fundManagerCommonName,
        fundCommonName: fund.fundCommonName,
        fundLegalEntity:fund.legalEntity,
        fundManagerLegalEntityName:fund.fundManagerLegalEntityName,
        gpSignaturePic:signatorySignaturePic,
        capitalCommitment:subscription.lpCapitalCommitmentPretty,
        acceptedOn:moment().format('M/D/YYYY'),
        fundEntityType: (fund.fundEntityType==1) ? 'Limited Partnership' : (fund.fundEntityType==2) ? 'Limited Liability company' : 'Type of Equity Holder (Limited Partner, Member),'
        
    };
    const htmlFilePath = "./views/subscriptionAgreementGpSign.html";
    const signedLastPage = await generateSignedPage(data, htmlFilePath);
    return await mergeAgreementDocAndSigndPage(subscriptionTempFile, signedLastPage);
}

// generate last page sined pdf from html and append to current fund agreement
async function fundAgreementSign(subscription, fundAgreementTempFile, lpSignaturePic,req) {
    const fund = subscription.fund;
    const data = {
        gpName: commonHelper.getFullName(req.user),
        fundManagerTitle: fund.fundManagerTitle,
        fundManagerCommonName: fund.fundManagerCommonName,
        fundCommonName: fund.fundCommonName,
        fundLegalEntity:fund.legalEntity,
        fundManagerLegalEntityName:fund.fundManagerLegalEntityName,
        gpSignaturePic:lpSignaturePic,
        capitalCommitment:subscription.lpCapitalCommitmentPretty,
        date:moment().format('M/D/YYYY')
    };
    const htmlFilePath = "./views/fundAgreementGpSign.html";
    const signedLastPage = await generateSignedPage(data, htmlFilePath);
   return await mergeAgreementDocAndSigndPage(fundAgreementTempFile, signedLastPage);
}

async function mergeAgreementDocAndSigndPage(agreementTempFile,lpSignedLastPage) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
    await commonHelper.pdfMerge([agreementTempFile, lpSignedLastPage.path], outputFile);
    return { path: outputFile, name: fileName };
}

async function generateSignedPage(data, htmlFilePath) {
    const fileName = uuidv1();
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}.pdf`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}


module.exports ={
    signatoryApprovalForDocs
}