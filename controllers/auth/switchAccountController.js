const models = require('../../models/index');
const authHelper = require('../../helpers/authHelper');
const messageHelper = require('../../helpers/messageHelper');
const errorHelper = require('../../helpers/errorHelper');
const logger = require('../../helpers/logger');
const redis = require('redis')
const uuidv1 = require('uuid/v1')
const config = require("../../config");

/**
 * @swagger
 *
 * /api/v1/switchaccount:
 *   post:
 *     tags:
 *       - Login 
 *     description: switch role to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           example: {
 *             "userid": "88"
 *           }
 *     responses:
 *       200:
 *         description: switch Role
 */
module.exports = async (req, res, next) => {
    try {
        const { userId } = req.body 

        //login with default userid updated in account
        let defaultUserId = userId

        //get user details
        const user = await models.User.findOne({
            where: { id: defaultUserId },
            include: [{
                model: models.Role,
                as: 'roles',
                required: true,
                include: [{
                    model: models.Permission,
                    as: 'permissions',
                    required: true
                }]
            },
            {
                model: models.Account,
                as:'accountDetails',
                required:true
            }]
        });




        //if user not exists
        if (!user) {
            return errorHelper.error_400(res, 'email', messageHelper.loginFail);
        } 
        
        //generate token with current logged in user     
        const token = authHelper.createAuthToken(user) 

        const vcfirm = await models.VCFirm.findOne({
            where: {
                gpId: user.id
            }
        })       

        //get list of roles of users
        let listallusers = await models.User.findAll({
            where: { accountId:user.accountId, deletedAt: null },
            include: [{
                model: models.Role,
                as: 'roles',
                required: true        
            },            {
                model: models.Account,
                as:'accountDetails',
                required:true
            }]
        })

        listallusers = listallusers.map(userdetails=>{
            return {                
                firstName: userdetails.firstName,
                lastName: userdetails.lastName,
                middleName: userdetails.middleName,
                id: userdetails.id,                             
                cellNumber: userdetails.cellNumber,
                accountType: userdetails.accountDetails.accountType,
                profilePic: userdetails.profilePic,
                roleId:userdetails.roles.id
            }
        })           

        //send result
        return res.json({
            token: token,
            user: {
                firstName: user.firstName,
                lastName: user.lastName,
                middleName: user.middleName,
                id: user.id,
                cellNumber: user.cellNumber,
                accountType: user.accountType,
                profilePic: user.profilePic,
                vcfirmId: vcfirm ? vcfirm.id : null
            },
            allusers: listallusers
        });

    } catch (e) {
        next(e);
    }
}


module.exports.listUsersForAccount = async (req, res, next) => {
    try {    
        logger.info('List users for account start')
        //get list of roles of users
        let listallusers = await models.User.findAll({
            where: { email: req.user.email, deletedAt: null },
            include: [{
                model: models.Role,
                as: 'roles',
                required: true        
            }]
        })     
        
        logger.info('query all users with same email id')

        listallusers = listallusers.map(userdetails=>{
            return {                
                firstName: userdetails.firstName,
                lastName: userdetails.lastName,
                middleName: userdetails.middleName,
                id: userdetails.id,                             
                cellNumber: userdetails.cellNumber,
                accountType: userdetails.accountType,
                profilePic: userdetails.profilePic,
                roleId:userdetails.roles.id
            }
        })   
        
        logger.info('List users for account end')
        
        //send result
        return res.json({          
            allusers: listallusers
        });

    } catch (e) {
        next(e);
    }
}

