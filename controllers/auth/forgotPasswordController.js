const models = require('../../models/index');
const _ = require('lodash');
const messageHelper = require('../../helpers/messageHelper');
const commonHelper = require('../../helpers/commonHelper');
const emailHelper = require('../../helpers/emailHelper');
const errorHelper = require('../../helpers/errorHelper');


/**
 * @swagger
 *
 * /api/v1/forgotPasswordTokenValidate:
 *   post:
 *     tags:
 *       - Login 
 *     description: forgotPasswordTokenValidate
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           example: {
 *             "email": "johngp1@mailinator.com",
 *             "code": "1234",
 *             "cellNumber": "1234567890"
 *           }
 *     responses:
 *       200:
 *         description: forgotPasswordTokenValidate
 */
const forgotPasswordTokenValidate = async (req, res, next) => {

    try {
        const { email, code, cellNumber } = req.body;

        const whereFilter = _.pickBy({ email: email, cellNumber: cellNumber }, _.identity) // remove empty keys 

        const account = await models.Account.findOne({
            where: whereFilter
        });

        if (!account) {
            return errorHelper.error_400(res, 'email', messageHelper.emailNotRegistered);
        }

        if (!account.emailConfirmCode) {
            return errorHelper.error_400(res, 'emailConfirmCode', messageHelper.confirmCodeExpired);
        }

        if (account.emailConfirmCode != code) {
            return errorHelper.error_400(res, 'emailConfirmCode', null, {
                code: code,
                email: email,
                isCodeValid: false
            });
        }

        return res.json({
            code: code,
            email: email,
        });

    } catch (e) {
        return next(e)
    }

}

/**
 * @swagger
 *
 * /api/v1/forgotPassword:
 *   post:
 *     tags:
 *       - Login 
 *     description: forgot password
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           example: {
 *             "email": "johngp1@mailinator.com",
 *             "cellNumber": "1234567890"
 *           }
 *     responses:
 *       200:
 *         description: forgot password
 */
const forgotPassword = async (req, res, next) => {
    try {

        const { email, cellNumber} = req.body

        const whereFilter = _.pickBy({ email: email, cellNumber: cellNumber }, _.identity) // remove empty keys  

        const account = await models.Account.findOne({
            where: whereFilter
        });
        
        if (!account) {
            return errorHelper.error_400(res, 'email', messageHelper.emailOrPhoneNotRegistered);
        } else if (!account.isEmailConfirmed) {
            return errorHelper.error_400(res, 'email', messageHelper.emailNotConfirmed);
        }

        let code = commonHelper.generateToken();

        // upate pasword, login fail attempts to 0
        await models.Account.update({
                emailConfirmCode: code,
                lastCodeGeneratedAt: new Date().toISOString(),
                loginFailAttempts: 0, 
                loginFailAttemptsAt: null 
            },{
                where: whereFilter
            }
        );

        account.emailConfirmCode = code;

        //get user details for account
        let defaultUserId = account.defaultUserId

        //Update login fail attempts to 0
        await models.User.update(
            { loginFailAttempts: 0, loginFailAttemptsAt: null },
            { where:{ id: defaultUserId } }
        );

        //get user details
        const user = await models.User.findOne({
            where: { id: defaultUserId }        
        });        

        emailHelper.sendEmail(
            {
                toAddress: account.email,
                subject: messageHelper.resetPasswordEmailSubject,
                data: {
                    name: commonHelper.getFullName(user),
                    emailConfirmCode: account.emailConfirmCode
                },
                htmlPath: "forgotPassword.html"
            });
        
        return res.json({
            message: messageHelper.password_reset_mail
        });

    } catch (e) {
        return next(e)
    }

};


module.exports = {
    forgotPasswordTokenValidate,
    forgotPassword
}