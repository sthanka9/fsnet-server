const models = require('../../models/index');
const bcrypt = require('bcryptjs');
const config = require('../../config/index');
const messageHelper = require('../../helpers/messageHelper');
const commonHelper = require('../../helpers/commonHelper');
const emailHelper = require('../../helpers/emailHelper');
const errorHelper = require('../../helpers/errorHelper');
const logger = require('../../helpers/logger');

module.exports = async (req, res, next) => {
    try {
       
        const { firstName, lastName, middleName, password, emailConfirmCode, cellNumber } = req.body
        //check account exists
        const account = await models.Account.findOne({
            where: {
                emailConfirmCode:emailConfirmCode
            }
        })
        // account not valid
        if (!account) {
            logger.log('error', 'Account Not Found',account)
            return errorHelper.error_400(res, 'user', messageHelper.userNotFound);
        }
        accountId = account.id
        let profilepic =  await commonHelper.getFileInfo(req.file);
        let defaultUserId = account.defaultUserId
        const salt = bcrypt.genSaltSync(10);
        const passwordHash = bcrypt.hashSync(password, salt)     
       
        //user update
        const accountUpdate = {            
            isEmailConfirmed: 1,
            cellNumber: cellNumber,
            firstName : firstName,
            lastName : lastName,
            middleName : middleName,
            profilePic:profilepic,
            email:account.email
        }     
        await models.User.update(accountUpdate, {
            where: {
                id: defaultUserId
            }
        });

        //confirm email for other users
        await models.User.update({ isEmailConfirmed: 1 }, { where: { email:account.email } })

        //account update
        accountUpdate.password = passwordHash
        accountUpdate.emailConfirmCode = null
        accountUpdate.defaultUserId = defaultUserId

        await models.Account.update(accountUpdate, {
            where: {
                id: accountId
            }
        })

        //get user data 
        const user = await models.User.findOne({where: { accountId: accountId}});

        //send email to account
        emailHelper.sendEmail({
            toAddress: account.email,
            subject: messageHelper.welcomeEmailSubject,
            data: {
                name: commonHelper.getFullName(user),
                clientURL: config.get('clientURL')
            },
            htmlPath: "welcome.html"
        });

        return res.status(201).json({
            message: messageHelper.accountCreated,
        });


    } catch (e) {
        next(e);
    }

}