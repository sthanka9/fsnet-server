const models = require('../../models/index');
const bcrypt = require('bcryptjs');
const messageHelper = require('../../helpers/messageHelper');
const errorHelper = require('../../helpers/errorHelper');


module.exports = async (req, res, next) => {
    try {         
         
        const accountid =  req.accountId;
        
        const { oldPassword, newPassword } = req.body;

        const account = await models.Account.findOne({
            where: {
                id: accountid
            }
        });

        // account not valid
        if (!account) {
            return errorHelper.error_400(res, 'user', messageHelper.userNotFound);
        }

        // password is invalid
        if (!bcrypt.compareSync(oldPassword, account.password)) {
            return errorHelper.error_400(res, 'oldPassword', messageHelper.old_password_not_match);
        }

        const salt = bcrypt.genSaltSync(10);
        const passwordHash = bcrypt.hashSync(newPassword, salt);

        // update the password
        await models.Account.update(
            { password: passwordHash },
            { where: { id: accountid } }
        );

        return res.json({
            error: false,
            message: messageHelper.password_updated
        });
    } catch (e) {
        next(e);
    }
}