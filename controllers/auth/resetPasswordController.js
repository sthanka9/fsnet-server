const models = require('../../models/index');
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const messageHelper = require('../../helpers/messageHelper');

module.exports = async (req, res, next) => {

    try {

        const { password, code, email, cellNumber } = req.body;

        const salt = bcrypt.genSaltSync(10);

        const passwordHash = bcrypt.hashSync(password, salt);

        const whereFilter = _.pickBy({ email: email, cellNumber: cellNumber }, _.identity) // remove empty keys
      
        whereFilter.emailConfirmCode = code;

        const account = await models.Account.update({ password: passwordHash, emailConfirmCode: null }, { where: whereFilter });

        return res.json({ msg: messageHelper.passwordUpdateSuccessfully, data: account });

    } catch (e) {
        next(e);
    }
}