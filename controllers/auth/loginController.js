
const models = require('../../models/index');
const bcrypt = require('bcryptjs');
const MAX_LOGIN_ATTEMPTS = 5;
const _ = require('lodash');
const authHelper = require('../../helpers/authHelper');
const messageHelper = require('../../helpers/messageHelper');
const errorHelper = require('../../helpers/errorHelper');
const redis = require('redis')
const config = require("../../config");
const jwt = require("jsonwebtoken");

const recordLoginAttement = (account) => {
    models.Account.update({ loginFailAttempts: models.sequelize.literal('loginFailAttempts + 1'), loginFailAttemptsAt: new Date().toISOString() }, { where: { id: account.id } });
}



/**
 * @swagger
 *
 * /api/v1/login:
 *   post:
 *     tags:
 *       - Login 
 *     description: Login to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           example: {
 *             "email": "seangp1@mailinator.com",
 *             "password": "menlo@123"
 *           }
 *     responses:
 *       200:
 *         description: login
 */
module.exports = async (req, res, next) => {
    try {
        const { email, password } = req.body

        //find account with email and password
        const account = await models.Account.findOne({
            where: { email: email, deletedAt: null }
        });

        //find passwords with email id -- production users 
        let users = await models.User.findAll({
            attributes:['password'],
            where: { email: email, deletedAt: null }
        }) 

        let listOfPasswords = _.map(users,'password')
        listOfPasswords     = _.remove(listOfPasswords,null)
        
        //account not exists
        if (!account) {
            return errorHelper.error_400(res, 'email', messageHelper.loginFail);
        } 

        //account id
        let accountid = account.id

        // check lock status
        if (account.loginFailAttempts >= MAX_LOGIN_ATTEMPTS) {
            recordLoginAttement(account); // record login attemets
            return errorHelper.error_400(res, 'password', messageHelper.maximumInvalidAttempts);
        }

        //compare password
        if (!bcrypt.compareSync(password, account.password || '')) {
            recordLoginAttement(account); // record login attemets 
            return errorHelper.error_400(res, 'password', messageHelper.loginFail);            
            /*
            //check for previous passwords in users table
            let tempCount = 0
            _.forEach(listOfPasswords,userPassword=>{
                //if any password matches
                if (bcrypt.compareSync(password, userPassword)){
                    tempCount = 1
                }
            })            
            if(tempCount==0){
                recordLoginAttement(account); // record login attemets 
                return errorHelper.error_400(res, 'password', messageHelper.loginFail);
            }
            */
        }

        //check account email confirmation
        if (!account.isEmailConfirmed) {
            recordLoginAttement(account); // record login attemets 
            return errorHelper.error_400(res, 'email', messageHelper.emailNotConfirmed);
        }        
        
        //login with default userid updated in account
        let defaultUserId = account.defaultUserId

        //get user details
        const user = await models.User.findOne({
            where: { id: defaultUserId },
            include: [{
                model: models.Role,
                as: 'roles',
                required: true,
                include: [{
                    model: models.Permission,
                    as: 'permissions',
                    required: true
                }]
            }]
        });

        //if user not exists
        if (!user) {
            return errorHelper.error_400(res, 'email', messageHelper.loginFail);
        }         

        //generate token with current logged in user      
        const token = authHelper.createAuthToken(user)      

        const vcfirm = await models.VCFirm.findOne({
            where: {
                gpId: user.id
            }
        })        

        //get list of roles of users
        let listallusers = await models.User.findAll({
            where: { email: email, deletedAt: null },
            include: [{
                model: models.Role,
                as: 'roles',
                required: true        
            }]
        })

        listallusers = listallusers.map(userdetails=>{
            return {                
                firstName: userdetails.firstName,
                lastName: userdetails.lastName,
                middleName: userdetails.middleName,
                id: userdetails.id,                             
                cellNumber: userdetails.cellNumber,
                accountType: userdetails.accountType,
                profilePic: userdetails.profilePic
            }
        })   
        
        //last login update
        models.Account.update({ lastLoginAt: new Date().toISOString() }, { where: { id: accountid } });
        
        //Update login fail attempts to 0
        await models.Account.update({
            loginFailAttempts: 0, 
            loginFailAttemptsAt: null 
        },{
            where: {id: account.id}
        });

        await models.User.update(
            { loginFailAttempts: 0, loginFailAttemptsAt: null },
            { where:{ id: defaultUserId } }
        );

        //send result
        return res.json({
            token: token,
            user: {
                firstName: user.firstName,
                lastName: user.lastName,
                middleName: user.middleName,
                id: user.id,
                cellNumber: user.cellNumber,
                accountType: user.accountType,
                profilePic: user.profilePic,
                vcfirmId: vcfirm ? vcfirm.id : null,
                accountid:user.accountId
            },
            allusers: listallusers
        });

    } catch (e) {
        next(e);
    }
}

//logout user
module.exports.logout = async (req, res, next) => {
    let authToken = req.headers['x-auth-token'] || req.body.authToken || req.query.token; 
    // verifies secret and checks exp
	jwt.verify(authToken, config.get('jwt').privatekey, {
		algorithms: ['HS256']
	}, async function (err, decoded) {
        let client = redis.createClient(config.get('redis').port,config.get('redis').host)      
        let uuid = decoded.user.uuid    
        
        //remove token related data from cache
        await client.del(uuid, function (err, response) {
            if (response == 1) {
                console.log("Deleted Successfully!")
            } else {
                console.log("Cannot delete")
            }
        })        
        
    })

    return res.json({
        message : 'ok'
      });    
}

