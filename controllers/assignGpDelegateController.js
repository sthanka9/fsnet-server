
const models = require('../models/index');
const { Op } = require('sequelize');
const config = require('../config/index');
const _ = require('lodash');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const notificationHelper = require('../helpers/notificationHelper');

module.exports = async (req, res, next) => {

    try {

        const { fundId, gpDelegates = [] } = req.body

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.User,
                as: 'gp'
            }]
        })

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        const delegateIds = _.map(gpDelegates, 'delegateId');      
        const consentRequiredDelegateIdsArray = _.map(_.filter(gpDelegates, {gPDelegateRequiredConsentHoldClosing:1}),'delegateId');  
        
        console.log("******delegateIds**************",delegateIds)
        console.log("******consentRequiredDelegateIds**************",consentRequiredDelegateIdsArray)

        //remove records from gpdelegate approvals
        if(delegateIds.length==0){
            await models.GpDelegates.destroy({
                where: {
                    fundId: fundId
                }
            })

            await models.GpDelegateApprovals.destroy({
                where: {
                    fundId: fundId
                }
            })

        }else{
        await models.GpDelegates.destroy({
            where: {
                fundId: fundId,
                delegateId: {
                    [Op.notIn]: delegateIds
                }
            }
        });
        }

        // restore if reselected gp delegate
        await models.GpDelegates.update({ deletedAt: null }, {
            paranoid: false, where: {
                delegateId: {
                    [Op.in]: delegateIds
                },
                fundId: fundId
            }
        })         

        await models.GpDelegateApprovals.destroy({
            where: {
                delegateId: {
                    [Op.notIn]: consentRequiredDelegateIdsArray
                },
                fundId: fundId
            }
        });

        const currentGpDelegates = await models.GpDelegates.findAll({
            paranoid: false,
            attributes: ['fundId', 'delegateId', 'gpId', 'gPDelegateRequiredDocuSignBehalfGP', 'gPDelegateRequiredConsentHoldClosing'],
            where: {
                fundId: fundId
            }
        });

        const currentGpDelegateIds = _.map(currentGpDelegates, 'delegateId');

        let newDelegateIds = _.difference(delegateIds, currentGpDelegateIds);

        // get all delegate ids 

    // separate old and new delegates
        const newDelegates = [], oldDelegates = [];
        for (let delegate of gpDelegates) {

             const data = {
                fundId: fundId,
                delegateId: delegate.delegateId,
                gpId: fund.gp.id,
                gPDelegateRequiredDocuSignBehalfGP: delegate.gPDelegateRequiredDocuSignBehalfGP,
                gPDelegateRequiredConsentHoldClosing: delegate.gPDelegateRequiredConsentHoldClosing,
                createdBy: req.userId,
                updatedBy: req.userId
        };

          newDelegateIds.includes(delegate.delegateId)  ? newDelegates.push(data) :  oldDelegates.push(data)

        }
      

      
        const updateDelegate = []
        // updated old delegates
        for (let oldDelegate of oldDelegates) {
            delete oldDelegate.createdBy;
            oldDelegate.deletedAt = null;
            updateDelegate.push(models.GpDelegates.update(oldDelegate, {
                paranoid: false,
                where: {
                    fundId: oldDelegate.fundId,
                    delegateId: oldDelegate.delegateId
                }
            }))
        }

        // insert new deletates
        await models.GpDelegates.bulkCreate(newDelegates);

        return Promise.all([...updateDelegate])
            .then(async() => {
                const newDelegatesIds = _.map(newDelegates, 'delegateId');

                if (req.user.accountType != 'FSNETAdministrator') {

                    // GP has invited you to help in fund (existing account) - GP Delegate
                    const alertData = {
                        htmlMessage: messageHelper.invitationToHelpParticularFundAlertMessage,
                        message: 'Invitation to help in fund',
                        gpName: commonHelper.getFullName(fund.gp),
                        fundManagerCommonName: fund.fundManagerCommonName,
                        fundName: fund.fundCommonName,
                        fundId: fund.id,
                        sentBy: req.userId
                    };

                    //get all accountids
                    let allUserIds = []
                    let userInfo = {}            
                    //get all users ids
                    for (let gpDelegateId of newDelegatesIds) {
                        allUserIds.push(gpDelegateId)
                    }    
                    //get all users accountId
                    let getAllUserInfo = await models.User.findAll({
                        attributes: ['id','accountId'],
                        where: { id: { [Op.in]: allUserIds } }
                    })                     
                    //build an array with user id and account id
                    for (let userinfo of getAllUserInfo) {
                        userInfo[userinfo.id] = userinfo.accountId
                    }
                    for (let gpDelegateId of newDelegatesIds) {
                        notificationHelper.triggerNotification(req, req.userId, gpDelegateId, false, alertData,userInfo[gpDelegateId],null);
                    }

                    triggerEmailNotifictionsToGp(newDelegatesIds, fund, req) // trigger emails to new gps
                }


                return res.json(true);
            })
            .catch(error => next(error))

    } catch (e) {
        next(e)
    }

}


function triggerEmailNotifictionsToGp(delegateIds, fund, req) {

    models.User.findAll({
        where: {
            id: {
                [Op.in]: delegateIds
            }
        },
        include:[{
            model: models.Account,
            as:'accountDetails',
            required:true
        }]  
    }).then(users => {

        if (!users) return; 

        users.forEach(function (user) {

            const loginOrRegisterLink = user.accountDetails.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${user.accountDetails.emailConfirmCode}`;

            if (user.isEmailNotification || req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
                emailHelper.sendEmail({
                    toAddress: user.email,
                    subject: messageHelper.assignGpDelegateEmailSubject,
                    data: {
                        name: commonHelper.getFullName(user),
                        fundName: fund.legalEntity,
                        gpName: commonHelper.getFullName(fund.gp),
                        loginOrRegisterLink: loginOrRegisterLink,
                        user
                    },
                    htmlPath: "fundGPInviation.html"
                }).catch(error => {
                    commonHelper.errorHandle(error)
                })
            }


        });
    }).catch(error => {
        commonHelper.errorHandle(error)
    })
}