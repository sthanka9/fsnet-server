
const models = require('../models/index');
const _ = require('lodash');
const config = require('../config/index');
const { Op } = require('sequelize');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const eventHelper = require('../helpers/eventHelper');
const notificationHelper = require('../helpers/notificationHelper');
const pendingActionsHelper = require('../helpers/pendingActionsHelper');


const restoreDeclinedInvestment = async (req,res,next) => {
    try{

        const { subscriptionId, fundId, title, investorType,lpId } = req.body;

        //Rescind-6
        if (investorType == 'LLC') {
            whereFilter = {
                fundId: fundId,
                entityName: title,
                investorType: 'LLC',
                lpId: lpId,
                status:6
            };
        }
    
        if (investorType == 'Trust') {
            whereFilter = {
                fundId: fundId,
                trustName: title,
                investorType: 'Trust',
                lpId: lpId,
                status:6
            };
        }
    
        if (investorType == 'Individual' || investorType == 'jointIndividual') {
            whereFilter = {
                fundId: fundId,
                investorType: 'Individual',
                lpId: lpId,
                status:6
            };
        }

        await models.FundSubscription.destroy({
            where: {
                id:subscriptionId
            },
            force:true
        });

        //Restoring back the declined fund to inprogress
        await models.FundSubscription.update({
            status:2
        },{
            where:whereFilter
        });

        //Get inprogress subscriptions
        whereFilter.status=2
        const getSubscription = await models.FundSubscription.findOne({
            where:whereFilter
        });

        return res.json({
            data:getSubscription
        });


    }catch(e){
        next(e)
    }
}

const titleUniqueCheck = async (req, res) => {

    const { fundId, title, investorType, subscriptionId, lpId } = req.body;

    if (!(Number(fundId) && Number(subscriptionId) && (investorType == 'Individual' ? true : title) && investorType)) {
        return res.json({
            error: true,
            message: 'Invalid data provided.'
        });
    }


    let whereFilter = {};

    if (investorType == 'LLC') {
        whereFilter = {
            fundId: fundId,
            entityName: title,
            investorType: 'LLC',
            lpId: lpId,
            id: {
                [Op.ne]: subscriptionId
            }
        };
    }

    if (investorType == 'Trust') {
        whereFilter = {
            fundId: fundId,
            trustName: title,
            investorType: 'Trust',
            lpId: lpId,
            id: {
                [Op.ne]: subscriptionId
            }
        };
    }

    if (investorType == 'Individual' || investorType == 'jointIndividual') {
        whereFilter = {
            fundId: fundId,
            investorType: 'Individual',
            lpId: lpId,
            id: {
                [Op.ne]: subscriptionId
            }
        };
    }

    const subscription = await models.FundSubscription.findOne({
        attributes: ['id','status'],
        where: whereFilter,
        paranoid:false
    });
    

    return res.json({
        allowed: subscription ? false : true,
        investmentAvailableForRestore: _.find(subscription,{status:6}) ? true:false
    })
}

const fundsList = async (req, res, next) => {

    try {

        var whereFilter = {};

        if (req.user.accountType == 'LP') {

            whereFilter = {
                lpId: req.userId,
                deletedAt: null,
                //Invitation - Pending 
                status: {
                    [Op.ne]: 17
                }
            }

        } else if (req.user.accountType == 'LPDelegate') {

            let subscriptionIds = await models.LpDelegate.findAll({
                attributes: ['subscriptionId'],
                where: {
                    delegateId: req.userId
                }
            })

            subscriptionIds = _.map(subscriptionIds, 'subscriptionId');

            whereFilter = {
                id: {
                    [Op.in]: subscriptionIds
                },
                deletedAt: null,
                // status:{
                //     [Op.ne]:1
                // }
            }


        } else if (req.user.accountType == 'SecondaryLP') {
            // secondary LP
            let subscriptionIds = await models.LpSignatories.findAll({
                attributes: ['subscriptionId'],
                where: {
                    signatoryId: req.userId
                }
            })

            subscriptionIds = _.map(subscriptionIds, 'subscriptionId');

            whereFilter = {
                id: {
                    [Op.in]: subscriptionIds
                },
                deletedAt: null,
                isLpSignatoriesNotified:true
                // status:{
                //     [Op.ne]:1
                // }
            }

        } else {
            return res.status(401).json({
                data: messageHelper.authorizationCheck
            })
        }

        let fundInvestors = await models.FundSubscription.findAll({
            order: [
                models.sequelize.literal("CASE [FundSubscription].[status] WHEN '1' THEN 1  ELSE 2 END"),
                ['fund', 'createdAt', 'DESC']
            ],
            where: whereFilter,
            include: [{
                attributes: ['id', 'legalEntity', 'fundImage', 'partnershipDocument', 'createdAt'],
                model: models.Fund,
                as: 'fund',
                required: true,
                where: {
                    statusId: {
                        [Op.in]: [13, 14, 10],  // 'Open-Ready', 'Deactivated', 'Closed'
                    }
                },
                include: [{
                    attributes: ['id', 'firstName', 'middleName', 'lastName', 'email', 'city', 'state', 'country', 'organizationName', 'cellNumber'],
                    model: models.User,
                    as: 'gp',
                    required: true
                }]
            }, {
                attributes: ['id', 'name', 'lpSideName'],
                model: models.FundStatus,
                as: 'subscriptionStatus',
                required: true
            }, {
                attributes: ['id', 'firstName', 'middleName', 'lastName', 'city', 'state', 'country', 'organizationName', 'cellNumber'],
                model: models.User,
                as: 'lp',
                where: {
                    isEmailConfirmed: 1
                },
                required: true
            }, {
                model: models.FundClosings,
                as: 'fundClosingInfo',
                required: false,
                where: {
                    isActive: 1
                }
            }, {
                model: models.SignatureTrack,
                as: 'signature',
                required: false,
                where: {
                    isActive: 1
                }
            },{
                model: models.DocumentsForSignature,
                as: 'documentsForSignature',
                required: false,
                where: {
                    isActive: 1,
                    deletedAt:null,
                    docType:'SUBSCRIPTION_AGREEMENT'
                }
            }]
        })


        const lpSubscriptions = [];

        let nonRescindedFunds=[] ;

        for (let fundInvestor of fundInvestors) {

            fundInvestor = fundInvestor.toJSON();
            //fundInvestor.isCurrentUserSigned = fundInvestor.signature && fundInvestor.signature.signedUserId == req.userId ? true : false;
            fundInvestor.isCurrentUserSigned = await checkCurrentUserSigned(fundInvestor.id,req.userId)
            fundInvestor.pendingActionsCount = await getPendingActionsCount(fundInvestor, req,res)
            fundInvestor.trackerTitle = ((fundInvestor.investorType == 'LLC' ? fundInvestor.entityName :
                (fundInvestor.investorType == 'Trust' ? fundInvestor.trustName : null)) || (fundInvestor.organizationName ? fundInvestor.organizationName : fundInvestor.lp.organizationName) ||
                commonHelper.getFullName(fundInvestor.lp));
            if (req.user.accountType != 'LP') {
                //investor not accepted fund then show the error message, Accept Lp
                fundInvestor.isErrorMessage = fundInvestor.status == 1 ? true : false;
            }

            
            //To show isAllLpSignatoriesSigned flag 
            //fundInvestor.isAllLpSignatoriesSigned = fundInvestor.documentsForSignature.length > 0 ?  fundInvestor.documentsForSignature[0].isAllLpSignatoriesSigned  : null

            fundInvestor.isAllLpSignatoriesSigned = await checkisAllLpSignatoriesSigned(fundInvestor)           


            lpSubscriptions.push(fundInvestor);
            //Push all the non rescinded funds
            if(!_.includes([6,3],fundInvestor.status)){
                nonRescindedFunds.push(fundInvestor);
            }
        }

        for(let lpSubscriptionKey in lpSubscriptions){
            lpSubscription = lpSubscriptions[lpSubscriptionKey];
            if(lpSubscription.status == 6 && _.find(nonRescindedFunds,{lpId:lpSubscription.lpId,fundId:lpSubscription.fundId})){
                delete lpSubscriptions[lpSubscriptionKey];
            }
        }


        return res.json({
            data: _.filter(lpSubscriptions)
        })

    } catch (e) {
        next(e)
    }
}


async function checkisAllLpSignatoriesSigned(subscription) {
 
    let docTypes = ['SUBSCRIPTION_AGREEMENT','FUND_AGREEMENT','AMENDMENT_AGREEMENT'] 
 
    let documentsForSignature = await models.DocumentsForSignature.findAll({
        attributes:['id','isAllLpSignatoriesSigned'],
        where: 
        {
            subscriptionId: subscription.id,
            docType: {
                [Op.in]: docTypes
            },
            deletedAt:null,
            isActive: 1      
        }

    })
    
    let isAllLpSignatoriesSigned = true
    
    for (let documentForSignature of documentsForSignature) {
        if(documentForSignature.isAllLpSignatoriesSigned==0){
            isAllLpSignatoriesSigned = false
        }
    }
  
    return isAllLpSignatoriesSigned
}

const store = async (req, res, next) => {

    try {
        var {
            subscriptionId = null,
            step = null,
            fundId = null,
            investorType = null,
            otherInvestorAttributes = null,
            areYouSubscribingAsJointIndividual = null,
            legalTitle = null,
            typeOfLegalOwnership = null,
            areYouAccreditedInvestor = null,
            areYouQualifiedPurchaser = null,
            areYouQualifiedClient = null,
            mailingAddressStreet = null,
            mailingAddressCity = null,
            mailingAddressState = null,
            mailingAddressZip = null,
            mailingAddressPhoneNumber = null,
            investorSubType = 0,
            otherInvestorSubType = null,
            jurisdictionEntityLegallyRegistered = null,
            entityName = null,
            numberOfGrantorsOfTheTrust = null,
            selectState = null,
            trustName = null,
            trustLegallyDomiciled = null,
            isEntityTaxExemptForUSFederalIncomeTax = null,
            isEntityUS501c3 = null,
            isTrust501c3 = null,
            legalTitleDesignation = null,
            istheEntityFundOfFundsOrSimilarTypeVehicle = null,
            releaseInvestmentEntityRequired = null,
            numberOfDirectEquityOwners = 1,
            existingOrProspectiveInvestorsOfTheFund = null,
            numberOfexistingOrProspectives = null,
            entityProposingAcquiringInvestment = null,
            anyOtherInvestorInTheFund = null,
            entityHasMadeInvestmentsPriorToThedate = null,
            partnershipWillNotConstituteMoreThanFortyPercent = null,
            beneficialInvestmentMadeByTheEntity = null,
            companiesAct = null,
            mailingAddressCountry = null,
            employeeBenefitPlan = null,
            planAsDefinedInSection4975e1 = null,
            benefitPlanInvestor = null,
            fiduciaryEntityIvestment = null,
            entityDecisionToInvestInFund = null,
            totalValueOfEquityInterests = null,
            indirectBeneficialOwnersSubjectFOIAStatutes = null,
            aggrement1 = null,
            aggrement2 = null,
            isSubjectToDisqualifyingEvent = null,
            fundManagerInfo = null,
            lpCapitalCommitment = 0.0,
            investorQuestions = null,
            stateResidenceId = null,
            countryResidenceId = null,
            stateDomicileId = null,
            countryDomicileId = null,
            isFormChanged

        } = req.body;


        if (!Number(subscriptionId)) {
            return errorHelper.error_400(res, 'subscriptionId', messageHelper.subscriptionIdReq);
        }

        const fundSubscriptionPrevious = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            }
        })

        if (isFormChanged && fundSubscriptionPrevious.status == 7 && !fundSubscriptionPrevious.isOfflineInvestor) {
            await models.FundSubscription.update({
                status: 2
            }, {
                    individualHooks: true,
                    where: {
                        id: subscriptionId
                    }
                });

            await models.DocumentsForSignature.destroy({
                where: {
                    subscriptionId
                }
            })

            await models.SignatureTrack.destroy({
                where: {
                    subscriptionId
                }
            })
        }

        let investorAnswers = '';
        if (investorQuestions) {
            investorAnswers = investorQuestions.map(questionValue => {
                return {
                    subscriptionId: subscriptionId,
                    questionId: questionValue.id,
                    questionResponse: (questionValue.questionResponse == 0 || questionValue.questionResponse) ? String(questionValue.questionResponse) : ''
                }
            });
        }


        if (investorType == 'Individual') {

            let individualData = {};

            if (step == 1) {
                individualData = {
                    fundId,
                    investorType,
                    areYouSubscribingAsJointIndividual,
                    legalTitle,
                    typeOfLegalOwnership,
                    isSubjectToDisqualifyingEvent,
                    fundManagerInfo,
                    //To update remaining fields as null
                    investorSubType,
                    otherInvestorSubType,
                    jurisdictionEntityLegallyRegistered,
                    entityName,
                    isEntityTaxExemptForUSFederalIncomeTax,
                    isEntityUS501c3,
                    istheEntityFundOfFundsOrSimilarTypeVehicle,
                    releaseInvestmentEntityRequired,
                    indirectBeneficialOwnersSubjectFOIAStatutes,
                    otherInvestorAttributes,
                    numberOfGrantorsOfTheTrust,
                    trustName,
                    isTrust501c3,
                    stateResidenceId,
                    countryResidenceId
                }

            } else if (step == 2) {
                individualData = {
                    mailingAddressStreet,
                    mailingAddressCity,
                    mailingAddressState,
                    mailingAddressZip,
                    mailingAddressPhoneNumber,
                    mailingAddressCountry
                }
            } else if (step == 3) {
                individualData = {
                    areYouAccreditedInvestor
                }
            } else if (step == 4) {
                individualData = {
                    areYouQualifiedPurchaser,
                    areYouQualifiedClient
                }
            } else if (step == 5) {
                individualData = {
                    lpCapitalCommitment
                }
            } else if (step == 6) {

                if (investorAnswers) {
                    // remove and
                    await models.InvestorAnswers.destroy({
                        where: {
                            subscriptionId: subscriptionId
                        }
                    })

                    // add new investor answers               
                    models.InvestorAnswers.bulkCreate(investorAnswers)
                }

                let subscriptionFundData = await models.FundSubscription.findOne({
                    where: {
                        id: subscriptionId
                    },
                    include: [{
                        model: models.Fund,
                        as: 'fund',
                        required: true
                    }]
                });

                return res.json({ data: subscriptionFundData });



            }

            if ([1, 2, 3, 4, 5].includes(step)) {

                await models.FundSubscription.update(individualData, {
                    individualHooks: true,
                    where: {
                        id: subscriptionId
                    }
                });

                let subscription = await models.FundSubscription.findOne({
                    where: {
                        id: subscriptionId
                    }, include: [{
                        model: models.Fund,
                        as: 'fund',
                        required: true
                    }]
                });

                let fund = await models.Fund.findOne({
                    where: {
                        id: subscription.fundId
                    },
                    include: [{
                        model: models.User,
                        as: 'gp'
                    }]
                })

                const gpDelegates = await getGpDelegatesIfExist(fund);

                subscription = subscription.toJSON();
                subscription.email = req.user.email;

                const investorName = (subscription.investorType == 'LLC' ? subscription.entityName :
                    (subscription.investorType == 'Trust' ? subscription.trustName : (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null))) || (subscription.organizationName ? subscription.organizationName : fund.gp.organizationName)
                    || commonHelper.getFullName(req.user);

                    
                if (subscription.countryResidenceId != fundSubscriptionPrevious.countryResidenceId) {
                    // Send email for EEA country validation rule                     
                    triggerEmailToGp(countryResidenceId, fund, investorName);

                    notificationHelper.eeaCountryAlert(countryResidenceId, fund, req.user, gpDelegates, investorName,fund.gp.accountId);
                }

                return res.json({ data: subscription });

            }



        } else if (investorType == 'LLC') {

            // Entity Type
            let data = {};

            if (step == 1) {
                data = {
                    investorType,
                    investorSubType,
                    otherInvestorSubType,
                    // state or country domicile
                    jurisdictionEntityLegallyRegistered,
                    entityName,
                    isEntityTaxExemptForUSFederalIncomeTax,
                    isEntityUS501c3,
                    istheEntityFundOfFundsOrSimilarTypeVehicle,
                    releaseInvestmentEntityRequired,
                    indirectBeneficialOwnersSubjectFOIAStatutes,
                    isSubjectToDisqualifyingEvent,
                    fundManagerInfo,
                    otherInvestorAttributes,
                    //To update remaining fields as null
                    areYouSubscribingAsJointIndividual,
                    legalTitle,
                    typeOfLegalOwnership,
                    numberOfGrantorsOfTheTrust,
                    trustName,
                    isTrust501c3,
                    stateResidenceId,
                    countryResidenceId,
                    stateDomicileId,
                    countryDomicileId
                }

            } else if (step == 2) {
                data = {
                    mailingAddressStreet,
                    mailingAddressCity,
                    mailingAddressState,
                    mailingAddressZip,
                    mailingAddressPhoneNumber,
                    mailingAddressCountry
                }
            } else if (step == 3) {
                data = {
                    areYouAccreditedInvestor
                }
            } else if (step == 4) {
                data = {
                    areYouQualifiedPurchaser,
                    areYouQualifiedClient
                }
            } else if (step == 5) {
                data = {
                    companiesAct,
                    numberOfDirectEquityOwners,
                    existingOrProspectiveInvestorsOfTheFund,
                    numberOfexistingOrProspectives
                }

                if (existingOrProspectiveInvestorsOfTheFund) {

                    let fundSubscription = await models.FundSubscription.findOne({
                        where: {
                            id: subscriptionId
                        },
                        include: [{
                            model: models.Fund,
                            as: 'fund',
                            required: true,
                            include: [{
                                model: models.User,
                                as: 'gp'
                            }]
                        }]
                    });

                    let fund = fundSubscription.fund;
                    const gpDelegates = await getGpDelegatesIfExist(fund);

                    fund = fund.toJSON();
                    fund.numberOfexistingOrProspectives = numberOfexistingOrProspectives;
                    const email = fund.gp.email;

                    const investorName = (fundSubscription.investorType == 'LLC' ? fundSubscription.entityName :
                        (fundSubscription.investorType == 'Trust' ? fundSubscription.trustName : (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null))) || (fundSubscription.organizationName ? fundSubscription.organizationName : fundSubscription.fund.gp.organizationName)
                        || commonHelper.getFullName(req.user);
                    let fundManagerName = commonHelper.getFullName(fund.gp);

                    if(fund.isNotificationsEnabled){

                        _emailToEquityOwnersQuestions(email, {
                            investorName,
                            fundManagerName,
                            numberOfexistingOrProspectives: numberOfexistingOrProspectives,
                            user:fund.gp
                        });
                    }

                    //Equity owners alert
                    notificationHelper.equityOwnersAlert(req, fund, gpDelegates, investorName,fund.gp.accountId)

                }


            } else if (step == 6) {
                data = {
                    entityProposingAcquiringInvestment,
                    anyOtherInvestorInTheFund,
                    entityHasMadeInvestmentsPriorToThedate,
                    partnershipWillNotConstituteMoreThanFortyPercent,
                    beneficialInvestmentMadeByTheEntity
                }
            } else if (step == 7) {
                data = {
                    employeeBenefitPlan,
                    planAsDefinedInSection4975e1,
                    benefitPlanInvestor,
                    fiduciaryEntityIvestment,
                    entityDecisionToInvestInFund,
                    totalValueOfEquityInterests,
                    aggrement1,
                    aggrement2
                }

            } else if (step == 8) {
                data = {
                    lpCapitalCommitment
                }
            } else if (step == 9) {

                if (investorAnswers) {
                    // remove and
                    await models.InvestorAnswers.destroy({
                        where: {
                            subscriptionId: subscriptionId
                        }
                    })

                    // add new investor answers               
                    models.InvestorAnswers.bulkCreate(investorAnswers)
                }


                let subscription = await models.FundSubscription.findOne({
                    where: {
                        id: subscriptionId
                    }, include: [{
                        model: models.Fund,
                        as: 'fund',
                        required: true
                    }]
                });

                return res.json({ data: subscription });


            }

            if ([1, 2, 3, 4, 5, 6, 7, 8].includes(step)) {

                //Add request user object to use it in update hook to send EEA alert
                data.user = req.user

                await models.FundSubscription.update(data,
                    {
                        individualHooks: true,
                        where: {
                            id: subscriptionId
                        }
                    })

                let subscription = await models.FundSubscription.findOne({
                    where: {
                        id: subscriptionId
                    }, include: [{
                        model: models.Fund,
                        as: 'fund',
                        required: true
                    }]
                });

                const fund = await models.Fund.findOne({
                    where: {
                        id: subscription.fundId
                    },
                    include: [{
                        model: models.User,
                        as: 'gp'
                    }]
                })
                const gpDelegates = await getGpDelegatesIfExist(fund);

                let investorCount = 1;
                // const investorPercentHolding = await getInvestorPercentHoldingValue(subscription);

                // if (!(subscription.entityProposingAcquiringInvestment == true || subscription.anyOtherInvestorInTheFund == true ||
                //     subscription.entityHasMadeInvestmentsPriorToThedate == true ||
                //     subscription.partnershipWillNotConstituteMoreThanFortyPercent == true ||
                //     subscription.beneficialInvestmentMadeByTheEntity == true) && subscription.companiesAct == 1) {
                //     // If value for "Look Through Issues" is no then investorCount=No. of Beneficial Owners
                //     investorCount = subscription.numberOfDirectEquityOwners;
                // } else if (Math.round(investorPercentHolding) >= 10) {
                //     // then investorCount=No. of Beneficial Owners
                //     investorCount = subscription.numberOfDirectEquityOwners;
                // }

                await models.FundSubscription.update({ investorCount: investorCount }, {
                    individualHooks: true,
                    where: {
                        id: subscriptionId
                    }
                });

                subscription = subscription.toJSON();

                subscription.email = req.user.email;

                const investorName = (subscription.investorType == 'LLC' ? subscription.entityName :
                    (subscription.investorType == 'Trust' ? subscription.trustName : (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null))) || (fundSubscription.organizationName ? fundSubscription.organizationName : fundSubscription.gp.organizationName)
                    || commonHelper.getFullName(req.user);

                if (subscription.countryDomicileId != fundSubscriptionPrevious.countryDomicileId) {

                    // Send email for EEA country validation rule
                    triggerEmailToGp(countryDomicileId, fund, investorName)
                    notificationHelper.eeaCountryAlert(countryDomicileId, fund, req.user, gpDelegates, investorName,fund.gp.accountId)
                }

                return res.json({ data: subscription });

            }



        } else if (investorType == 'Trust') {
            let data = {};

            if (step == 1) {
                data = {
                    investorType,
                    investorSubType,
                    numberOfGrantorsOfTheTrust,
                    trustName,
                    isEntityTaxExemptForUSFederalIncomeTax,
                    isTrust501c3,
                    releaseInvestmentEntityRequired,
                    isSubjectToDisqualifyingEvent,
                    fundManagerInfo,
                    otherInvestorAttributes,
                    //To update remaining fields as null
                    areYouSubscribingAsJointIndividual,
                    legalTitle,
                    typeOfLegalOwnership,
                    otherInvestorSubType,
                    jurisdictionEntityLegallyRegistered,
                    entityName,
                    isEntityUS501c3,
                    istheEntityFundOfFundsOrSimilarTypeVehicle,
                    indirectBeneficialOwnersSubjectFOIAStatutes,
                    stateResidenceId,
                    countryResidenceId

                }

            } else if (step == 2) {
                data = {
                    mailingAddressStreet,
                    mailingAddressCity,
                    mailingAddressCountry,
                    mailingAddressState,
                    mailingAddressZip,
                    legalTitleDesignation,
                    mailingAddressPhoneNumber
                }
            } else if (step == 3) {
                data = {
                    areYouAccreditedInvestor
                }
            } else if (step == 4) {
                data = {
                    areYouQualifiedPurchaser,
                    areYouQualifiedClient
                }
            } else if (step == 5) {
                data = {
                    companiesAct,
                    numberOfDirectEquityOwners,
                    existingOrProspectiveInvestorsOfTheFund,
                    numberOfexistingOrProspectives
                }

                if (existingOrProspectiveInvestorsOfTheFund) {
                    let fundSubscription = await models.FundSubscription.findOne({
                        where: {
                            id: subscriptionId
                        },
                        include: [{
                            model: models.Fund,
                            as: 'fund',
                            required: true,
                            include: [{
                                model: models.User,
                                as: 'gp'
                            }]
                        }]
                    });

                    let fund = fundSubscription.fund;
                    const gpDelegates = await getGpDelegatesIfExist(fund);


                    fund = fund.toJSON();
                    fund.numberOfexistingOrProspectives = numberOfexistingOrProspectives;
                    const email = fund.gp.email;

                    const investorName = (fundSubscription.investorType == 'LLC' ? fundSubscription.entityName :
                        (fundSubscription.investorType == 'Trust' ? fundSubscription.trustName : (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null))) || (fundSubscription.organizationName ? fundSubscription.organizationName : fundSubscription.fund.gp.organizationName)
                        || commonHelper.getFullName(req.user);
                    let fundManagerName = commonHelper.getFullName(fund.gp);

                    if(fund.isNotificationsEnabled){

                    _emailToEquityOwnersQuestions(email, {
                        investorName: investorName,
                        fundManagerName,
                        numberOfexistingOrProspectives: numberOfexistingOrProspectives
                    });

                    }

                    //Equity owners alert
                    notificationHelper.equityOwnersAlert(req, fund, gpDelegates, investorName,fund.gp.accountId)
                }

            } else if (step == 6) {
                data = {
                    entityProposingAcquiringInvestment,
                    anyOtherInvestorInTheFund,
                    entityHasMadeInvestmentsPriorToThedate,
                    partnershipWillNotConstituteMoreThanFortyPercent,
                    beneficialInvestmentMadeByTheEntity
                    
                }
            } else if (step == 7) {
                data = {
                    employeeBenefitPlan,
                    planAsDefinedInSection4975e1,
                    benefitPlanInvestor,
                    fiduciaryEntityIvestment,
                    entityDecisionToInvestInFund,
                    totalValueOfEquityInterests,
                    aggrement1,
                    aggrement2
                }

            } else if (step == 8) {
                data = {
                    lpCapitalCommitment
                }
            } else if (step == 9) {

                if (investorAnswers) {
                    // remove and
                    await models.InvestorAnswers.destroy({
                        where: {
                            subscriptionId: subscriptionId
                        }
                    })

                    // add new investor answers               
                    models.InvestorAnswers.bulkCreate(investorAnswers)
                }

                let subscription = await models.FundSubscription.findOne({
                    where: {
                        id: subscriptionId
                    }, include: [{
                        model: models.Fund,
                        as: 'fund',
                        required: true
                    }]
                });

                return res.json({ data: subscription });

            }



            if ([1, 2, 3, 4, 5, 6, 7, 8].includes(step)) {

                await models.FundSubscription.update(data, {
                    individualHooks: true,
                    where: {
                        id: subscriptionId
                    }
                });

                let subscription = await models.FundSubscription.findOne({
                    where: {
                        id: subscriptionId
                    }, include: [{
                        model: models.Fund,
                        as: 'fund',
                        required: true
                    },
                    {
                        attributes: ['id', 'name'],
                        model: models.FundStatus,
                        as: 'subscriptionStatus',
                        required: true
                    }]
                });

                const fund = await models.Fund.findOne({
                    where: {
                        id: subscription.fundId
                    },
                    include: [{
                        model: models.User,
                        as: 'gp'
                    }]
                })

                const gpDelegates = await getGpDelegatesIfExist(fund);


                let investorCount = 1;
                // // Company Act Matters column is YES & investor holds 10% or more of the closed interests
                // const investorPercentHolding = await getInvestorPercentHoldingValue(subscription);

                // if (!(subscription.entityProposingAcquiringInvestment == true || subscription.anyOtherInvestorInTheFund == true ||
                //     subscription.entityHasMadeInvestmentsPriorToThedate == true ||
                //     subscription.partnershipWillNotConstituteMoreThanFortyPercent == true ||
                //     subscription.beneficialInvestmentMadeByTheEntity == true) && subscription.companiesAct == 1) {
                //     // If value for "Look Through Issues" is no then investorCount=No. of Beneficial Owners
                //     investorCount = subscription.numberOfDirectEquityOwners;
                // } else if (Math.round(investorPercentHolding) >= 10) {
                //     // then investorCount=No. of Beneficial Owners
                //     investorCount = subscription.numberOfDirectEquityOwners;
                // }

                await models.FundSubscription.update({ investorCount: investorCount }, {
                    individualHooks: true,
                    where: {
                        id: subscriptionId
                    }
                });


                subscription = subscription.toJSON()
                subscription.email = req.user.email
                const investorName = (subscription.investorType == 'LLC' ? subscription.entityName :
                    (subscription.investorType == 'Trust' ? subscription.trustName : (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null))) || (subscription.organizationName ? subscription.organizationName : fundSubscription.fund.gp.organizationName)
                    || commonHelper.getFullName(req.user);

                    
                if (subscription.countryResidenceId != fundSubscriptionPrevious.countryResidenceId) {
                    // Send email for EEA country validation rule
                    triggerEmailToGp(countryResidenceId, fund, investorName)
                    notificationHelper.eeaCountryAlert(countryResidenceId, fund, req.user, gpDelegates, investorName,fund.gp.accountId)
                }

                return res.json({ data: subscription });

            }
        }


    } catch (e) {
        next(e)
    }
}


const getInvestorSubType = (req, res, next) => {

    try {


        const { group } = req.params;

        models.InvestorSubType.findAll({
            attributes: ['id', 'name', 'isUS'],
            where: {
                'group': group
            }
        }).then(data => res.json(data)).catch(error => res.json(error))

    } catch (error) {
        next(error);
    }
}

const getUSStates = (req, res, next) => {

    try {
        models.State.findAll({
            attributes: ['id', 'name'],
            order: [
                ['order', 'DESC']
            ],
            where: {
                country_id: 231 // 231 == us
            }
        }).then(data => res.json(data)).catch(error => res.json(error))

    } catch (error) {
        next(error);
    }
}

const getStates = (req, res, next) => {
    try {
        const country_id = req.params.countryId;

        models.State.findAll({
            attributes: ['id', 'name'],
            order: [
                ['order', 'DESC']
            ],
            where: {
                country_id: country_id
            }
        }).then(data => res.json(data)).catch(error => res.json(error))
    } catch (error) {
        next(error);
    }
}

const getTimezones = (req, res, next) => {
    try { 

        models.timezone.findAll({
            attributes: ['id', 'displayName','isDefault'],
            order: [                 
                ['displayName', 'ASC']
            ],
            where: {
                status:1
            }
        }).then(data => res.json(data)).catch(error => res.json(error))
    } catch (error) {
        next(error);
    }

}

const getAllCountries = (req, res, next) => {
    try {

        const { exceptUS = 0 } = req.params

        const where = exceptUS ? {
            id: {
                [Op.notIn]: [231, 232] // 231 == us
            }
        } : null;

        models.Country.findAll({
            attributes: ['id', 'name'],
            order: [
                ['order', 'DESC'],
                ['name', 'ASC']
            ],
            where: where
        }).then(data => res.json(data)).catch(error => res.json(error))
    } catch (error) {
        next(error);
    }

}

const getClosedReadyLpsListForGP = async (subscriptions, data) => {

    const closedReadyLpsList = [];

    const gpDelegatesRequiredSignatureIds = _.map(data.fundGPDelegates, 'delegateId');

    let trackerTitle;
    for (subscription of subscriptions) {
        if (subscription.lp) {
            trackerTitle = ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) ||
                (subscription ? subscription.organizationName : subscription.lp.organizationName)
                || commonHelper.getFullName(subscription.lp));

        } else {

            trackerTitle = ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) ||
                (subscription.organizationName ? subscription.organizationName : subscription.offlineLp.organizationName)
                || commonHelper.getFullName(subscription.offlineLp));


        }

        let delegateApproval = true
            
        
        if(gpDelegatesRequiredSignatureIds.length>0)
            delegateApproval = _.isEqual(_.sortBy(gpDelegatesRequiredSignatureIds), _.sortBy(_.map(subscription.GpDelegateApprovals, 'delegateId')));          

        let signatoriesStatus = data.fund.noOfSignaturesRequiredForClosing == Number(_.map(subscription.GpSignatoryApprovals, 'signatoryId').length) + 1;

        let canSign = delegateApproval ? signatoriesStatus : delegateApproval;

        let warnnings = [];

        // get all EEA countries
        let eEACountries = await models.Country.findAll({
            where: {
                isEEACountry: 1
            }
        })

        eEACountries = eEACountries.map(country => country.id)

        //[14, 21, 33, 56, 57, 58, 68, 74, 75, 82, 85, 99, 105, 107, 120, 126, 127, 135, 155, 175, 176, 180, 197, 198, 205, 211, 230, 100, 125, 164, 54, 212]
        if (_.includes(eEACountries, subscription.countryDomicileId) && subscription.investorType == 'LLC') {
            warnnings.push('EEA');
        }else if(_.includes(eEACountries, subscription.countryResidenceId) && subscription.investorType !== 'LLC'){
            warnnings.push('EEA');

        }

        if (subscription.isSubjectToDisqualifyingEvent) {
            warnnings.push('DQE');
        }

        if (!subscription.areYouAccreditedInvestor) {
            warnnings.push('AI');
        }

        closedReadyLpsList.push({
            trackerTitle: trackerTitle,
            lpId: subscription.lp ? subscription.lp.id : null,
            offlineLpId: subscription.offlineLp ? subscription.offlineLp.id : null,
            lpCapitalCommitment: subscription.lpCapitalCommitment,
            subscriptionId: subscription.id,
            profilePic: subscription.lp ? subscription.lp.profilePic : '',
            canSign: canSign,
            status: canSign ? messageHelper.closingStatus : (delegateApproval ? messageHelper.signatoriesApprovalStatus : messageHelper.delegateApprovalStatus),
            warnnings: warnnings.join(', ')
        });

    }


    return closedReadyLpsList;
}

const getClosedReadyLpsListForGpDelegate = async (subscriptions, data, currentDelegateId) => {
    const closedReadyLpsList = [];

    let trackerTitle;
    for (subscription of subscriptions) {
        if (subscription.lp) {
            trackerTitle = ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) ||
                (subscription ? subscription.organizationName : subscription.lp.organizationName)
                || commonHelper.getFullName(subscription.lp));

        } else {
            trackerTitle = ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) ||
                (subscription.organizationName ? subscription.organizationName : subscription.offlineLp.organizationName)
                || commonHelper.getFullName(subscription.offlineLp));

        }

        let warnnings = [];

        // get all EEA countries
        let eEACountries = await models.Country.findAll({
            where: {
                isEEACountry: 1
            }
        })

        eEACountries = eEACountries.map(country => country.id)

        //[14, 21, 33, 56, 57, 58, 68, 74, 75, 82, 85, 99, 105, 107, 120, 126, 127, 135, 155, 175, 176, 180, 197, 198, 205, 211, 230, 100, 125, 164, 54, 212]
        if (_.includes(eEACountries, subscription.countryDomicileId) && subscription.investorType == 'LLC') {
            warnnings.push('EEA');
        }else if(_.includes(eEACountries, subscription.countryResidenceId) && subscription.investorType !== 'LLC'){
            warnnings.push('EEA');

        }
        if (subscription.isSubjectToDisqualifyingEvent) {
            warnnings.push('DQE');
        }

        if (!subscription.areYouAccreditedInvestor) {
            warnnings.push('AI');
        }


        const canSign = !(_.includes(_.map(subscription.GpDelegateApprovals, 'delegateId'), currentDelegateId));

        closedReadyLpsList.push({
            trackerTitle: trackerTitle,
            lpId: subscription.lp ? subscription.lp.id : null,
            offlineLpId: subscription.offlineLp ? subscription.offlineLp.id : null,
            lpCapitalCommitment: subscription.lpCapitalCommitment,
            subscriptionId: subscription.id,
            profilePic: subscription.lp ? subscription.lp.profilePic : '',
            canSign: canSign,
            status: canSign ? messageHelper.currentuserApprovalStatus : messageHelper.approvedStatus,
            warnnings: warnnings.join(', ')

        });

    }


    return closedReadyLpsList;
}

const getClosedReadyLpsListForSecondaryGp = async (subscriptions, data, currentSignatoryId) => {
    const closedReadyLpsList = [];

    const gpDelegatesRequiredSignatureIds = _.map(data.fundGPDelegates, 'delegateId');

    let trackerTitle;
    for (subscription of subscriptions) {
        if (subscription.lp) {
            trackerTitle = ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) ||
                (subscription ? subscription.organizationName : subscription.lp.organizationName)
                || commonHelper.getFullName(subscription.lp));
        } else {
            trackerTitle = ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : null)) ||
                (subscription.organizationName ? subscription.organizationName : subscription.offlineLp.organizationName)
                || commonHelper.getFullName(subscription.offlineLp));
        }

        let warnnings = [];

        // get all EEA countries
        let eEACountries = await models.Country.findAll({
            where: {
                isEEACountry: 1
            }
        })

        eEACountries = eEACountries.map(country => country.id)

        //[14, 21, 33, 56, 57, 58, 68, 74, 75, 82, 85, 99, 105, 107, 120, 126, 127, 135, 155, 175, 176, 180, 197, 198, 205, 211, 230, 100, 125, 164, 54, 212]
        if (_.includes(eEACountries, subscription.countryDomicileId) && subscription.investorType == 'LLC') {
            warnnings.push('EEA');
        }else if(_.includes(eEACountries, subscription.countryResidenceId) && subscription.investorType !== 'LLC'){
            warnnings.push('EEA');

        }

        if (subscription.isSubjectToDisqualifyingEvent) {
            warnnings.push('DQE');
        }

        if (!subscription.areYouAccreditedInvestor) {
            warnnings.push('AI');
        }

        const delegateApproval = _.isEqual(_.sortBy(gpDelegatesRequiredSignatureIds), _.map(subscription.GpDelegateApprovals, 'delegateId'));

        const signatoriesStatus = (_.includes(_.map(subscription.GpSignatoryApprovals, 'signatoryId'), currentSignatoryId));


        closedReadyLpsList.push({
            trackerTitle: trackerTitle,
            lpId: subscription.lp ? subscription.lp.id : null,
            offlineLpId: subscription.offlineLp ? subscription.offlineLp.id : null,
            lpCapitalCommitment: subscription.lpCapitalCommitment,
            subscriptionId: subscription.id,
            profilePic: subscription.lp ? subscription.lp.profilePic : '',
            canSign: signatoriesStatus ? false : delegateApproval,
            //    status: signatoriesStatus ? 'Approved' : (delegateApproval ? 'Waiting for your approval' : 'Waiting for Delegates Approval') 
            status: signatoriesStatus ? messageHelper.approvedStatus : (delegateApproval ? messageHelper.currentuserApprovalStatus : messageHelper.delegateApprovalStatus),
            warnnings: warnnings.join(', ')
        });


    }


    return closedReadyLpsList;
}


const getClosedReadyLps = async (req, res, next) => {

    try {

        const fundId = req.params.fundId;

        let closedReadyLpsList = [];

        const subscriptions = await models.FundSubscription.findAll({
            where: {
                fundId: fundId,
                status: 7,
            },
            include: [{
                model: models.User,
                as: 'lp',
                required: false
            }, {
                model: models.OfflineUsers,
                as: 'offlineLp',
                required: false
            }, {
                model: models.GpDelegateApprovals,
                as: 'GpDelegateApprovals',
                required: false,
                where: {
                    fundId: fundId
                }
            },{
                model: models.GpSignatoryApproval,
                as: 'GpSignatoryApprovals',
                required: false,
                where: {
                    fundId: fundId
                }
            }]
        })

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        });

        let fundGPDelegates = await models.GpDelegates.findAll({
            where: {
                fundId: fundId,
                gPDelegateRequiredConsentHoldClosing: 1
            }
        });

        let fundGpDelegateApprovals = fundGPDelegates.length > 0 ? await models.GpDelegateApprovals.findAll({
            where: {
                fundId: fundId
            }
        }) : [];


        let fundGpSignatoryApproval = await models.GpSignatoryApproval.findAll({
            where: {
                fundId: fundId,
                gpId: req.userId
            }
        });

        const data = { fund, fundGPDelegates, fundGpDelegateApprovals, fundGpSignatoryApproval };

        if (req.user.accountType == 'GP') {
            closedReadyLpsList = await getClosedReadyLpsListForGP(subscriptions, data);
        } else if (req.user.accountType == 'GPDelegate') {
            closedReadyLpsList = await getClosedReadyLpsListForGpDelegate(subscriptions, data, req.userId);
        } else if (req.user.accountType == 'SecondaryGP') {
            closedReadyLpsList = await getClosedReadyLpsListForSecondaryGp(subscriptions, data, req.userId);
        }

        return res.json(closedReadyLpsList)



    } catch (error) {
        next(error)
    }
}

async function triggerEmailToGp(countryDomicileOrResidenceId, fund, lpName) {

    //is gp notificatons enabled

    if(fund.isNotificationsEnabled){

    // get all EEA countries
    let eEACountries = await models.Country.findAll({
        where: {
            isEEACountry: 1
        }
    })

    eEACountries = eEACountries.map(country => country.id)

    //[14, 21, 33, 56, 57, 58, 68, 74, 75, 82, 85, 99, 105, 107, 120, 126, 127, 135, 155, 175, 176, 180, 197, 198, 205, 211, 230, 100, 125, 164, 54, 212]
    if (_.includes(eEACountries, Number(countryDomicileOrResidenceId))) {

        emailHelper.sendEmail({
            toAddress: fund.gp.email,
            subject: `Email Notification - Vanilla`,
            data: {
                lpName: lpName,
                gpName: commonHelper.getFullName(fund.gp),
                fundName: fund.legalEntity,
                user:fund.gp
            },
            htmlPath: "investorCountry.html"
        }).catch(error => {
            commonHelper.errorHandle(error)
        })
    }

    }

}

const sendForSignatureByDelegate = async (req, res, next) => {
    try {

        const { fundId, subscriptionId } = req.body

        const fundInvestor = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId,
                fundId: fundId
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true
            }, {
                model: models.User,
                as: 'lp'
            }]
        })

        const lpCapitalCommitmentOffered = parseFloat(fundInvestor.lpCapitalCommitment).toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD',
        });

        // notification email
        emailHelper.sendEmail({
            toAddress: fundInvestor.lp.email,
            subject: `Email Notification - Vanilla`,
            data: {
                delegateName: commonHelper.getFullName(req.user),
                fundName: fundInvestor.fund.legalEntity,
                lpName: commonHelper.getFullName(fundInvestor.lp),
                lpCapitalCommitmentOffered: lpCapitalCommitmentOffered,
                clientURL: config.get('clientURL'),
                user:fundInvestor.lp
            },
            htmlPath: "sendForSignatureByDelegate.html"
        }).catch(error => {
            commonHelper.errorHandle(error)
        })

        // alert to LP        
        const alertData = {
            htmlMessage: messageHelper.sendForSignatureAlertMessage,
            message: 'add signature',
            delegateName: commonHelper.getFullName(req.user),
            subscriptionId: subscriptionId,
            vanillaLink: config.get('clientURL'),
            fundManagerCommonName: fundInvestor.fund.fundManagerCommonName,
            fundName: fundInvestor.fund.fundCommonName,
            fundId: fundInvestor.fund.id,
            sentBy: req.userId,
            type:'FUND_SIGNATURE'
        };

        notificationHelper.triggerNotification(req, req.userId, fundInvestor.lp.id, false, alertData,fundInvestor.lp.accountId,null);

        return res.json({
            message: messageHelper.sendForSignature
        })

    } catch (error) {
        next(error)
    }
}


const getPreviousFunds = async (req, res, next) => {
    try {

        const { subscriptionId, search } = req.params

        var whereFilter = {}
        var searchFilter = {}

        if (!Number(subscriptionId)) {
            return errorHelper.error_400(res, 'subscriptionId', messageHelper.subscriptionIdReq);
        }

        if ((req.user.accountType == 'LP' || 'SecondaryLP' || 'GP' || 'GPDelegate' || 'SecondaryGP')) {

            whereFilter = {
                lpId: req.userId,
                deletedAt: null,
                id: {
                    [Op.ne]: subscriptionId
                },
                status: {
                    [Op.in]: [7, 10]
                }
            }

        } else if (req.user.accountType == 'LPDelegate') {
            //  list of fund to select will be specific to the corresponding LP.
            const getLp = await models.FundSubscription.findOne({
                attributes: ['fundId', 'lpId'],
                where: {
                    id: subscriptionId
                }
            })

            whereFilter = {
                lpId: getLp.lpId,
                id: {
                    [Op.ne]: subscriptionId
                },
                status: {
                    [Op.in]: [7, 10]
                }
            }

        } else {
            return res.status(401).json({
                data: messageHelper.authorizationCheck
            })
        }


        if (search) {
            searchFilter.legalEntity = {
                [Op.like]: `%${search}%`
            }
        }


        let funds = await models.Fund.findAll({
            attributes: ['id', 'legalEntity', 'fundCommonName', 'createdAt', 'statusId'],
            order: [
                ['createdAt', 'DESC']
            ],
            where: searchFilter,
            include: [{
                attributes: ['id', 'fundId', 'investorType', 'lpCapitalCommitment'],
                model: models.FundSubscription,
                as: 'fundInvestor',
                required: true,
                where: whereFilter,
                include: [{
                    attributes: ['id', 'name'],
                    model: models.FundStatus,
                    as: 'subscriptionStatus',
                    required: true
                }]
            }]
        })

        return res.json({
            funds
        })


    } catch (error) {
        next(error)
    }
}

const cloneSubscription = async (req, res, next) => {
    try {

        const { selectedSubscriptionId, subscriptionId, lpId } = req.body

        let newSubscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId
            }
        });

        let subscription = await models.FundSubscription.findOne({
            attributes: { exclude: ['id', 'fundId', 'lpId', 'status', 'lpClosedDate', 'noOfSignaturesRequired', 'createdAt', 'updatedAt', 'createdBy', 'updatedBy', 'invitedDate','changedPagesId'] },
            where: {
                id: selectedSubscriptionId
            }
        });

        let whereFilter = {};

        if (subscription.investorType == 'LLC') {
            whereFilter = {
                fundId: newSubscription.fundId,
                entityName: subscription.entityName,
                investorType: 'LLC',
                lpId: lpId,
                id: {
                    [Op.ne]: subscriptionId
                }
            };
        }

        if (subscription.investorType == 'Trust') {
            whereFilter = {
                fundId: newSubscription.fundId,
                trustName: subscription.trustName,
                investorType: 'Trust',
                lpId: lpId,
                id: {
                    [Op.ne]: subscriptionId
                }
            };
        }

        if (subscription.investorType == 'Individual') {
            whereFilter = {
                fundId: newSubscription.fundId,
                investorType: 'Individual',
                lpId: lpId,
                id: {
                    [Op.ne]: subscriptionId
                }
            };
        }

        const subscriptionCheck = await models.FundSubscription.findOne({
            attributes: ['id', 'investorType'],
            where: whereFilter
        });

        if (subscriptionCheck) {
            const message = subscriptionCheck.investorType == 'Individual' ? 'It is not possible to add a second investment as an Individual or Joint Individual. You may add as many distinct Trust or Entity Investors as you would like.' : 'It is not possible to add a second investment as a Trust or Entity in identical legal title to a previously established Investor in the Fund. You may add as many distinct Trust or Entity Investors as you would like.';
            return res.status(400).json({
                error: true,
                message
            });
        }

        let subscriptionFundData = subscription.toJSON()
        subscriptionFundData.subscriptionHtml = null
        subscriptionFundData.lpCapitalCommitment = null
        subscriptionFundData.subscriptionAgreementPath = null
        subscriptionFundData.isSubscriptionContentUpdated = false
        subscriptionFundData.isAdditionalQuestionAdded = false
 

        await models.FundSubscription.update(subscriptionFundData, {
            where: {
                id: subscriptionId
            }
        });

        return res.json({
            message: messageHelper.updateMessage
        })

    } catch (error) {
        next(error)
    }
}

async function getInvestorPercentHoldingValue(subscription) {

    // https://menlo-technologies.atlassian.net/browse/FSNET-324
    //x% = (Capital commitment offered amount /Total Capital Commitments Closed amount )*100
    let fundInvestors = await models.FundSubscription.findAll({
        where: {
            fundId: subscription.fundId,
            status: 10 //Closed
        },
        include: [{
            attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
            model: models.FundClosings,
            required: false,
            as: 'fundClosingInfo',
            where: {
                isActive: 1
            }
        }]
    })

    let totalCapitalCommitmentClosed = 0;
    for (let fundClosing of fundInvestors) {
        if (_.last(fundClosing.fundClosingInfo)) {
            totalCapitalCommitmentClosed += _.last(fundClosing.fundClosingInfo).gpConsumed;
        }
    }

    return (subscription.lpCapitalCommitment / totalCapitalCommitmentClosed) * 100;
}

//check logged in user signed all the pending documents
const checkCurrentUserSigned = async (subscriptionId,userId)=>{

    let documentsCount = await models.DocumentsForSignature.count({
        where: {             
            subscriptionId: subscriptionId,
            isActive:1,
            isArchived:0
        }
    })

    let signedDocumentsCount = await models.SignatureTrack.count({
        where: {             
            subscriptionId: subscriptionId,
            isActive:1,
            signedUserId:userId
        }
    })

  return documentsCount==signedDocumentsCount ? true : false
}


async function getPendingActionsCount(subscription, req,res) {

    let docs = await pendingActionsHelper.getLPPendingActions(req,res,subscription)
    return docs.length

    /*

    let investorStatus = subscription.status


    //side letter no pending actions for in-progress/invite subscriptions
    let docTypes = ['SUBSCRIPTION_AGREEMENT','FUND_AGREEMENT', 'CHANGE_COMMITMENT_AGREEMENT', 'SIDE_LETTER_AGREEMENT','AMENDMENT_AGREEMENT'] 

    const whereFilter = {
        subscriptionId: subscription.id,
        docType: {
            [Op.in]: docTypes
        },
        isActive: 1,
        isArchived:0,
        isPrimaryLpSigned: false
    };

    const accountType = req.user.accountType;
    // if lp signatories available then true otherwise false
    const signatoriesCheck = (subscription.noOfSignaturesRequired > 0) ? true : false;

    if (accountType === 'LP' || accountType === 'LPDelegate') {  
        whereFilter.isAllLpSignatoriesSigned = signatoriesCheck;
    }

    if (accountType === 'SecondaryLP') {
        const signatureTrack = await models.SignatureTrack.findAll({
            attributes: ['documentId'],
            where: {
                signedUserId: req.userId
            }
        });
        const documentIds = _.map(signatureTrack, 'documentId');
        whereFilter.Id = {
            [Op.notIn]: documentIds
        }
    }

    let count = await models.DocumentsForSignature.count({
        where: whereFilter
    })


   if (accountType == 'LP' || accountType == 'SecondaryLP' ) {   
   
    if(accountType == 'LP'){

        let isLPSignatoriesSigned = await models.DocumentsForSignature.count({
            where: {                        
                subscriptionId: subscription.id,  
                isActive: 1,    
                deletedAt:null,
                isAllLpSignatoriesSigned:0,
                docType: {
                    [Op.ne] : 'SUBSCRIPTION_AGREEMENT'            
                }
            }
        })
        
        if(isLPSignatoriesSigned>0 && signatoriesCheck && subscription.status!=10){
            count = 0
        }
    }

    //if user in invited status , then pending action count is 0
    count =  investorStatus==1 ? 0 : count

   } */   
    
}

function _emailToEquityOwnersQuestions(toEmail, data) {

    emailHelper.sendEmail({
        toAddress: toEmail,
        subject: 'Equity Owners Questions - Alert',
        data: data,
        htmlPath: "equityOwnersQuestions.html"
    });
}

const rescindSubscription = async (req, res, next) => {
    try {
        const { subscriptionId } = req.params;

        const subscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId,
                status: {
                    [Op.ne]: 10
                }
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.User,
                    as: 'gp',
                    required: true
                }]
            }, {
                model: models.User,
                as: 'lp',
                required: true
            }]
        });

        if(!subscription){
            return errorHelper.error_400(res, 'subscription', messageHelper.subscriptionNotFound);
        }

        //Rescind-6, Declined-11 Subscription
        await models.FundSubscription.update({
            status: 6
        }, {
            where: {
                id: subscriptionId,
                status: {
                    [Op.ne]: 10
                }
            }
        });

        await models.DocumentsForSignature.update({
            isActive:0
        },{
            where: {
                subscriptionId,
                fundId:subscription.fund.id,
                docType: {
                    [Op.in]:['SUBSCRIPTION_AGREEMENT','SIDE_LETTER_AGREEMENT','FUND_AGREEMENT']
                } 
            }
        });

        await models.SignatureTrack.update({
            isActive:0
        }, {
            where: {
                documentType:{
                    [Op.in]:['SUBSCRIPTION_AGREEMENT','SIDE_LETTER_AGREEMENT','FUND_AGREEMENT']
                },
                subscriptionId
            }
        });
        

        eventHelper.saveEventLogInformation('Investor Rescinds from Fund', subscription.fundId, subscription.id, req);

        const gpDelegates = await getGpDelegatesIfExist(subscription.fund);

        await triggerEmailToGpAndGpDelegates(subscription, gpDelegates);

        return res.send(true);
    } catch (error) {
        next(error);
    }
}

async function getGpDelegatesIfExist(fund) {
    let gpDelegates = false;

    let gpDelegatesIDs = await models.GpDelegates.findAll({
        attributes: ['delegateId'],
        where: {
            fundId: fund.id,
            gpId: fund.gp.id
        }
    });

    if (gpDelegatesIDs || gpDelegatesIDs.length > 0) {
        gpDelegatesIDs = _.map(gpDelegatesIDs, 'delegateId')
        return await models.User.findAll({
            where: {
                id: {

                    [Op.in]: gpDelegatesIDs

                }
            }
        })
    } else {
        return gpDelegates;
    }
}

function triggerEmailToGpAndGpDelegates(subscription, gpDelegates) {
    if (subscription.fund.isNotificationsEnabled) {

        emailHelper.sendEmail({
            toAddress: subscription.fund.gp.email,
            subject: 'Investor Rescinds from Fund - Alert',
            data: {
                fundManagerName: commonHelper.getFullName(subscription.fund.gp),
                fundName: subscription.fund.legalEntity,
                lpName:  commonHelper.getInvestorName(subscription),
                user:subscription.fund.gp
            },
            htmlPath: "alerts/rescindAlert.html"
        })
    }

    for (let gpDelegate of gpDelegates) {
        if (gpDelegate.isEmailNotification) { // if setting is enabled
            emailHelper.sendEmail({
                toAddress: gpDelegate.email,
                subject: 'Investor Rescinds from Fund - Alert',
                data: {
                    fundManagerName: commonHelper.getFullName(subscription.fund.gp),
                    fundName: subscription.fund.legalEntity,
                    lpName:  commonHelper.getInvestorName(subscription),
                    user:gpDelegate
                },
                htmlPath: "alerts/rescindAlert.html"
            })
        }
    }
}




module.exports = {
    fundsList,
    store,
    getInvestorSubType,
    getUSStates,
    getStates,
    getAllCountries,
    getClosedReadyLps,
    sendForSignatureByDelegate,
    getPreviousFunds,
    cloneSubscription,
    rescindSubscription,
    titleUniqueCheck,
    restoreDeclinedInvestment,
    getTimezones
}