
const models = require('../models/index');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const eventHelper = require('../helpers/eventHelper');
const swapFundAmendmentsHelper = require('../helpers/swapFundAmendmentsHelper');
const notificationHelper = require('../helpers/notificationHelper');
const config = require('../config/index');
const path = require('path');
const _ = require('lodash');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const { Op } = require('sequelize');
const moment = require('moment');
const multer = require('multer');
const scissors = require('scissors');
const fs = require('fs');


const additionalSignatoryPagesUpload =  multer({
    storage: multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, "./assets/additionPages/")
        },
        filename: function (req, file, callback) {
            const ext = path.extname(file.originalname)
            callback(null, Date.now() + "_" + Math.random().toString(26).slice(2) + ext)
        }
    }),
    fileFilter: function (req, file, callback) {
        var ext = path.extname(file.originalname)
        if (ext && ext.toLowerCase() !== '.pdf') {
            return callback(new Error(messageHelper.fundDocumentUploadValidate))
        }

        callback(null, true)
    }
}).single('additionalSignatoryPages');


async function sendMailsToGpDelegate(fund,gpDelegates){

    for(let gpDelegate of gpDelegates){

        const loginOrRegisterLink = gpDelegate.details && gpDelegate.details.isEmailConfirmed  ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${gpDelegate.details.accountDetails.emailConfirmCode}`;
    
        if (gpDelegate.details.isEmailNotification || req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
            emailHelper.sendEmail({
                toAddress: gpDelegate.details.email,
                subject:messageHelper.assignGpDelegateEmailSubject,
                data: {
                    name: commonHelper.getFullName(gpDelegate.details),
                    fundName: fund.legalEntity,
                    gpName: commonHelper.getFullName(fund.gp),
                    loginOrRegisterLink: loginOrRegisterLink,
                    user: gpDelegate.details
                },
                htmlPath:"fundGPInviation.html"
                
            }).catch(error => {
    
                commonHelper.errorHandle(error)
            })
        }
    }
    

} 

async function sendMailsToGpSignatories(fund,gpSignatories){
    for(gpSignatory of gpSignatories){
        const loginOrRegisterLink = gpSignatory.signatoryDetails && gpSignatory.signatoryDetails.isEmailConfirmed  ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${gpSignatory.signatoryDetails.accountDetails.emailConfirmCode}`;
    
        if (gpSignatory.signatoryDetails.isEmailNotification || req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
            emailHelper.sendEmail({
                toAddress: gpSignatory.signatoryDetails.email,
                subject:messageHelper.assignGpDelegateEmailSubject,
                data: {
                    name: commonHelper.getFullName(gpSignatory.signatoryDetails),
                    fundName: fund.legalEntity,
                    gpName: commonHelper.getFullName(fund.gp),
                    loginOrRegisterLink: loginOrRegisterLink,
                    user:gpSignatory.signatoryDetails
                },
                htmlPath:"fundGPSignatoryInvitation.html"
                
            }).catch(error => {
    
                commonHelper.errorHandle(error)
            })
        }
    }
    

} 


const getStatusOfSubscriptionForm = async (req, res, next) => {

    try {
        const subscriptionId = req.params.subscriptionId;
        const fundId = req.params.fundId;

        if (subscriptionId != 0) {
            //In progress-7, Close Ready-11, waiting for signatory-16
            const subscription = await models.FundSubscription.findOne({
                attributes: ['subscriptionHtml', 'subscriptionAgreementPath', 'status'],
                where: {
                    status: {
                        [Op.in]:[7,11,16]
                    },
                    id: subscriptionId
                },
                include:[{
                    model:models.DocumentsForSignature,
                    where:{
                        fundId: fundId,
                        isActive:1,
                        lpSignCount:{
                            [Op.gt]:0
                        }
                    },
                    as: 'documentsForSignature',
                    required:true
                }]
            });

            let show = subscription ? true : false;
            return res.json({ show: show });

        } else {

            //In progress-7, Close Ready-11, waiting for signatory-16
            const subscriptions = await models.FundSubscription.findAll({
                attributes: ['subscriptionHtml', 'subscriptionAgreementPath', 'status'],
                where: {
                    status: {
                        [Op.in]:[7,11,16]
                    },
                    fundId: fundId
                },
                include:[{
                    model:models.DocumentsForSignature,
                    where:{
                        fundId: fundId,
                        isActive:1,
                        lpSignCount:{
                            [Op.gt]:0
                        }
                    },
                    as: 'documentsForSignature',
                    required:true
                }]
            });

            let show = (subscriptions && subscriptions.length > 0) ? true : false;

            return res.json({ show: show });
        }




    } catch (error) {
        next(error)
    }

}

async function getLpDelegatesList(lpIds,fundId){
    return await models.LpDelegate.findAll({
        where: {
            fundId,
            lpId:{
               [Op.in]:lpIds
            }
        },
        include: [{
            model: models.User,
            as: 'details',
            require: true,
            include:[{
                model: models.Account,
                as:'accountDetails',
                required:true
            }]             
        }]
    })

}

const _getVCFirmLps = async(vcfirmId, subscriptions, fundId, offset) => {

    const lps = await models.VcFrimLp.findAll({
        where: {
            vcfirmId: vcfirmId
        },
        limit: 100,
        offset: Number(offset),
        order: [['updatedAt', 'DESC']],
        include: [{
            model: models.User,
            as: 'details',
            required: true,
            include:[{
                model: models.Account,
                as:'accountDetails',
                required:true
            }]             
        }]
    });


    const fundLps = [];
    const firmLps = [];

    for(lp of lps) {
        const selectedSubscription = _.find(subscriptions, { lpId: lp.details.id });
        const data = {
            id: lp.details.id,
            firstName: lp.details.firstName,
            lastName: lp.details.lastName,
            middleName: lp.details.middleName,
            email: lp.details.email,
            profilePic: lp.details.profilePic,
            organizationName: lp.details ? lp.details.organizationName :'',
            updatedAt: lp.details.updatedAt,
            createdAt: lp.details.createdAt,
            accountType: lp.details.accountType,
            isEmailConfirmed: (lp.details&& lp.details.accountDetails) ? lp.details.accountDetails.isEmailConfirmed: false,
            copyLink:lp.details && lp.details.accountDetails && lp.details.accountDetails.isEmailConfirmed ? false: true,
            emailConfirmCode: lp.details && lp.details.accountDetails ? lp.details.accountDetails.emailConfirmCode : null   
        };
        if(!selectedSubscription) { firmLps.push(data) }
    }
 
    let lpIds = _.map(subscriptions,'lpId')
    lpIds = _.remove(lpIds, function(n) {
        return n  != null;
    })   
    const lpDelegates = await getLpDelegatesList(lpIds,fundId);
    

    let lpDelegatesInfo = [];
    lpDelegates.map(i => {
        lpDelegatesInfo.push({
            id: i.details.id 
        })
    })     
    
    let lpDelegatesIds = _.map(lpDelegatesInfo,'id')
    lpDelegatesIds = _.remove(lpDelegatesIds, function(n) {
        return n  != null;
    })  
  
    let historyDataLps = await models.History.findAll({
        where:{
            fundId,
            userId:{
                [Op.in]:lpIds
            },
            accountType:'LP'
        },
        order:[['id','DESC']]

    })

    let historyDatalpDelegates = await models.History.findAll({
        where:{
            fundId,
            userId:{
                [Op.in]:lpDelegatesIds
            },
            accountType:'LPDelegate'
        },
        order:[['id','DESC']]

    })    


    for(subscription of subscriptions){
      
        if(subscription.offlineLp){

            //date conversion based on fund timezone
            let timezone_date = ''; let timezone = '';
            if(subscription.fund.timeZone && subscription.fund.timeZone.code) {
                timezone_date = await commonHelper.setDateUtcToOffset(subscription.invitedDate, subscription.fund.timeZone.code).then(function (value) {
                    return value;
                });
                timezone = subscription.fund.timeZone.displayName;
            } else {
                timezone_date = await commonHelper.getDateAndTime(subscription.invitedDate);
            }

            const data = {
                id: subscription.offlineLp.id,
                firstName: subscription.offlineLp.firstName,
                lastName: subscription.offlineLp.lastName,
                middleName: subscription.offlineLp.middleName,
                address: subscription.offlineLp.address,
                cellNumber: subscription.offlineLp.cellNumber,
                email: subscription.offlineLp.email,
                organizationName:  subscription.offlineLp.organizationName, //Do we need to make this fund level
                status: subscription.offlineLp.accountType,
                updatedAt: subscription.offlineLp.updatedAt,
                createdAt: subscription.offlineLp.createdAt,
                invitedDate: timezone_date,
                accountType: 'OfflineLP',
                subscriptionId: subscription.id,
                timezone: timezone
            };
            fundLps.push(data);
        } else {
            let trackerTitle = subscription.investorType == 'LLC' ? subscription.entityName : 
            (subscription.investorType == 'Trust' ? subscription.trustName : (subscription.investorType == 'Individual' ?  (subscription.organizationName && subscription.organizationName!=null || subscription.lp && subscription.lp.organizationName && subscription.lp.organizationName!=null  ) : ''  )) || '(New Investor Pending Input)';            

            const lpDelegatesList = [];

            _.filter(lpDelegates,lpDelegate=>{
                if(subscription.lp && (subscription.lp.id == lpDelegate.lpId)){

                    let historyStatusLpDelegate =  historyDatalpDelegates.length>0 ?  _.find(historyDatalpDelegates,{userId: lpDelegate.details.id,fundId:fundId})   : false  

                    const data={
                        firstName: lpDelegate.details.firstName,
                        lastName: lpDelegate.details.lastName,
                        middleName: lpDelegate.details.middleName,
                        email: lpDelegate.details.email,
                        id: lpDelegate.details.id,
                        profilePic: lpDelegate.details.profilePic,
                        accountType: lpDelegate.details.accountType,
                        isEmailConfirmed: (lpDelegate.details&& lpDelegate.details.accountDetails) ? lpDelegate.details.accountDetails.isEmailConfirmed: false,
                        copyLink:lpDelegate.details && lpDelegate.details.accountDetails && lpDelegate.details.accountDetails.isEmailConfirmed ? false: true,
                        emailConfirmCode: lpDelegate.details && lpDelegate.details.accountDetails ? lpDelegate.details.accountDetails.emailConfirmCode : null,
                        status: historyStatusLpDelegate ? historyStatusLpDelegate.action: null, 
                    }

                    lpDelegatesList.push(data);
                }
                return lpDelegatesList;
            })

     

            //date conversion based on fund timezone
            let timezone_date = ''; let timezone = '';
            if(subscription.fund.timeZone && subscription.fund.timeZone.code) {
                timezone_date = await commonHelper.setDateUtcToOffset(subscription.invitedDate, subscription.fund.timeZone.code).then(function (value) {
                    return value;
                });
                timezone = subscription.fund.timeZone.displayName;
            } else {
                timezone_date = await commonHelper.getDateAndTime(subscription.invitedDate);
            }
 
           let historyStatusLp =  historyDataLps.length>0 ?  _.find(historyDataLps,{userId: subscription.lp.id,fundId:fundId})   : false  

            const data = {
                subscriptionId: subscription.id,
                id: subscription.lp ? subscription.lp.id : null,
                firstName: subscription.lp ? subscription.lp.firstName  : null,
                trackerTitle: trackerTitle,
                lastName: subscription.lp ? subscription.lp.lastName : null,
                middleName: subscription.lp ? subscription.lp.middleName : null,
                email: subscription.lp ? subscription.lp.email : null,
                profilePic: subscription.lp ? subscription.lp.profilePic : null,
                organizationName: subscription.organizationName || subscription.lp ? subscription.lp.organizationName : null,
                updatedAt: subscription.lp ? subscription.lp.updatedAt : null,
                createdAt: subscription.lp ? subscription.lp.createdAt : null,
                invitedDate: timezone_date,
                accountType: subscription.lp ? subscription.lp.accountType : null,
                subscriptionstatus: subscription.lp ? subscription.subscriptionStatus.name : null,
                isInvestorInvited: subscription.isInvestorInvited,
                lpDelegatesList,
                timezone: timezone,
                isEmailConfirmed: (subscription.lp&& subscription.lp.accountDetails) ? subscription.lp.accountDetails.isEmailConfirmed: false,
                copyLink:subscription.lp && subscription.lp.accountDetails && subscription.lp.accountDetails.isEmailConfirmed ? false: true,
                emailConfirmCode: subscription.lp && subscription.lp.accountDetails ? subscription.lp.accountDetails.emailConfirmCode : null,
                status: historyStatusLp ? historyStatusLp.action: subscription.isInvestorInvited==1 ? null  : 'Invited'                 
            };
            fundLps.push(data);
            
        }
    }

    return {
        fundLps,
        firmLps
    }
}


const getFundById = async (req, res, next) => {

    try {
        const { fundId } = req.params

        const {  offset = 0 } = req.query

        if (!Number(fundId)) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }

        let fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [
                {
                    model: models.User,
                    as: 'gp',
                    required: true,
                    include:[{
                        model: models.Account,
                        as:'accountDetails',
                        required:true
                    }]                     
                },
                {
                    model: models.FundStatus,
                    as: 'fundStatus',
                    required: false,
                },
                {
                    model: models.GpSignatories,
                    attributes: ['signatoryId'],
                    as: 'gpSignatoriesSelected',
                    required: false,
                    where: {
                        fundId: fundId
                    }
                },
                {
                    model: models.InvestorQuestions,
                    as: 'investorQuestions',
                    where: {
                        fundId: fundId,
                        deletedAt: null
                    },
                    required: false,
                }
            ],
        });

        fund = fund.toJSON();

        let gpInfo = {}
        gpInfo.firstName = fund.gp.firstName
        gpInfo.lastName = fund.gp.lastName
        gpInfo.middleName = fund.gp.middleName
        gpInfo.email = fund.gp.email
        gpInfo.copyLink = fund.gp.accountDetails.isEmailConfirmed ? false: true
        gpInfo.emailConfirmCode =  fund.gp.accountDetails.emailConfirmCode 

        // get investor status
        let subscriptions = await models.FundSubscription.findAll({
            attributes: ['status', 'id', 'lpId','investorType','entityName','trustName','organizationName','invitedDate','isInvestorInvited'],
            where: {
                fundId: fundId,
                status:{
                    //Declined, Rescind fund, Not Interested
                    [Op.notIn]:[11,6,3]
                }
            },
            include:[{
                model: models.OfflineUsers,
                as: 'offlineLp',
                required: false,
                paranoid:true
            },{
                model: models.User,
                as: 'lp',
                required: false,
                include:[{
                    model: models.Account,
                    as:'accountDetails',
                    required:true
                }]                 
            },{
                model: models.FundStatus,
                as:'subscriptionStatus',
                required:true
            },{
                attributes: ['timezone'],
                model: models.Fund,
                as: 'fund',
                required: false,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }]
            }]
        });

        const lps = await _getVCFirmLps(fund.vcfirmId, subscriptions,fundId, offset);

        //get pending signatories from gpChangeRequests
        let gpPendingSignatoriesList = await models.gpChangeRequest.findAll({
            where : {
                fundId: fundId,
                status:0
            }
        });
 

        let pendinggpids = []
        gpPendingSignatoriesList = gpPendingSignatoriesList.map(pendingsignatory => {
            pendinggpids.push(pendingsignatory.currentGpId)
        })   

        // get gp signatories from firm
        let gpSignatories = await models.VcFirmSignatories.findAll({
            where: {
                vcfirmId: fund.vcfirmId,                
                type: 'SecondaryGP'
            },
            include: [{
                model: models.User,
                as: 'signatoryDetails',
                where : {id : {[Op.notIn] : pendinggpids}},
                required: true,
                include:[{
                    model: models.Account,
                    as:'accountDetails'
                }]
            }]
        });

        let signatoryIds = _.map(gpSignatories,'signatoryId');
        const historyData = await models.History.findAll({
            where:{
                fundId,
                userId:{
                    [Op.in]:signatoryIds
                },
                accountType:'SecondaryGP'
            },
            order:[['id','DESC']]

        });

        gpSignatories = gpSignatories.map(signatory => {

            const selected = _.find(fund.gpSignatoriesSelected, { signatoryId: signatory.signatoryId });
            const historyStatus = _.find(historyData,{userId: signatory.signatoryId,fundId:fundId});

            return {
                id: signatory.signatoryDetails.id,
                firstName: signatory.signatoryDetails.firstName,
                lastName: signatory.signatoryDetails.lastName,
                middleName: signatory.signatoryDetails.middleName,
                email: signatory.signatoryDetails.email,
                accountType: signatory.signatoryDetails.accountType,
                profilePic: signatory.signatoryDetails.profilePic,
                organizationName: signatory.signatoryDetails ? signatory.signatoryDetails.organizationName: '',
                isEmailConfirmed: (signatory.signatoryDetails&& signatory.signatoryDetails.accountDetails) ? signatory.signatoryDetails.accountDetails.isEmailConfirmed: false,
                copyLink: signatory.signatoryDetails && signatory.signatoryDetails.accountDetails && signatory.signatoryDetails.accountDetails.isEmailConfirmed ? false: true,
                emailConfirmCode: signatory.signatoryDetails && signatory.signatoryDetails.accountDetails ? signatory.signatoryDetails.accountDetails.emailConfirmCode : null,
                createdAt: historyStatus ? historyStatus.createdAt :null,
                updatedAt: historyStatus ? historyStatus.updatedAt :null,
                status: historyStatus ? historyStatus.action: null,
                selected: selected ? true : false
            };
        })

        gpSignatories.push({
            id: fund.gp.id,
            firstName: fund.gp.firstName,
            lastName: fund.gp.lastName,
            middleName: fund.gp.middleName,
            email: fund.gp.email,
            accountType: fund.gp.accountType,
            profilePic: fund.gp.profilePic,
            selected: false,
            isPrimaryGp: true,
        });
    

        // get gp signatories from firm
        let gpPendingSignatories = await models.VcFirmSignatories.findAll({
            where: {
                vcfirmId: fund.vcfirmId,                
                type: 'SecondaryGP'
            },
            include: [{
                model: models.User,
                as: 'signatoryDetails',
                where : {id : {[Op.in] : pendinggpids}},
                required: true
            }]
        });


        gpPendingSignatories = gpPendingSignatories.map(pendingsignatory => {        

            gpSignatories.push({
                id: pendingsignatory.signatoryDetails.id,
                firstName: pendingsignatory.signatoryDetails.firstName,
                lastName: pendingsignatory.signatoryDetails.lastName,
                middleName: pendingsignatory.signatoryDetails.middleName,
                email: pendingsignatory.signatoryDetails.email,
                accountType: 'Fund Manager (Pending Approval)',
                profilePic: pendingsignatory.signatoryDetails.profilePic,
                organizationName: pendingsignatory.signatoryDetails ? pendingsignatory.signatoryDetails.organizationName : '',
                selected:  true,
                isPrimaryGp: true, 
            });
        });
 
      

        const consolidateFundAgreement = await models.DocumentsForSignature.findOne({
            where: {
                fundId: fundId,
                isActive: 1,
                docType: 'CONSOLIDATED_FUND_AGREEMENT'
            },
            order: [['id', 'DESC']],
        });

        //Ammendments or Revised FA signatories settings
        const fundAmmendment = await models.FundAmmendment.findOne({
            where:{
                fundId,
                isActive:1,
                isArchived:0,
                deletedAt:null
            }
        });


        if (!fund) {
            return res.status(404).json({
                "errors": [
                    {
                        "msg": messageHelper.fundNotFound
                    }
                ]
            });
        }
        
        if(fund.partnershipDocument){

            fund.partnershipDocument.url = `${config.get('serverURL')}/api/v1/document/view/displayFA/${fund.id}`;
        }

        //subscription data
        let subscriptionAgreementInfo = {}
        if(fund.subscriptionAgreementPath){
            let one = fund.subscriptionAgreementPath.one
            let two = fund.subscriptionAgreementPath.two
            
            if(one){
                let tempObject = {}
                let oneLength = one.split("/")
                tempObject.filename =  fund.subscriptionAgreementPath.one_originalfilename ? fund.subscriptionAgreementPath.one_originalfilename : oneLength[oneLength.length-1]
                tempObject.url =  `${config.get('serverURL')}/api/v1/document/view/one/${fund.id}`  
                subscriptionAgreementInfo.one = tempObject
            }

            if(two){
                let tempObject = {}
                let twoLength = two.split("/")
                tempObject.filename =  fund.subscriptionAgreementPath.two_originalfilename ? fund.subscriptionAgreementPath.two_originalfilename : twoLength[twoLength.length-1]
                tempObject.url =  `${config.get('serverURL')}/api/v1/document/view/two/${fund.id}`  
                subscriptionAgreementInfo.two = tempObject
            }            
            
        }
        
        let additionalSignatoryPages = (typeof fund.additionalSignatoryPages === 'string') ? JSON.parse(fund.additionalSignatoryPages) : fund.additionalSignatoryPages

         

        return res.json({
            data: {
                id: fund.id,
                gpInfo:gpInfo,
                isNotificationsEnabled:fund.isNotificationsEnabled,
                fundEntityType:fund.fundEntityType,              
                timezone:fund.timezone,
                vcfirmId: fund.vcfirmId,
                gpId: fund.gpId,
                legalEntity: fund.legalEntity,
                fundCommonName: fund.fundCommonName,
                additionalSignatoryPages: additionalSignatoryPages,
                fundManagerTitle: fund.fundManagerTitle,
                hardCapApplicationRule: fund.hardCapApplicationRule,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundHardCap: fund.fundHardCap,
                generalPartnersCapitalCommitmentindicated: fund.generalPartnersCapitalCommitmentindicated,
                fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                percentageOfLPCommitment: fund.percentageOfLPCommitment,
                percentageOfLPAndGPAggregateCommitment: fund.percentageOfLPAndGPAggregateCommitment,
                fundTargetCommitment: fund.fundTargetCommitment,
                capitalCommitmentByFundManager: fund.capitalCommitmentByFundManager,
                fundImage: fund.fundImage,
                partnershipDocument: fund.partnershipDocument ,
                consolidateFundAgreementURL: consolidateFundAgreement ? `${config.get('serverURL')}/api/v1/document/view/cfa/${fund.id}` : null,
                statusId: fund.statusId,
                status: fund.fundStatus.name,
                fundType: fund.fundType,
                lps: lps,
                subscriptionHtml: fund.subscriptionHtml ? true : null,
                subscriptionAgreementPath: fund.subscriptionAgreementPath ? true : null,
                subscriptionAgreementInfo:subscriptionAgreementInfo,
                gpDelegates: await _getGpDelegates(fund.vcfirmId, fund.id),
                gpSignatories: gpSignatories,
                //gpPendingSignatories:gpPendingSignatories,
                noOfSignaturesRequiredForClosing: fund.noOfSignaturesRequiredForClosing,
                noOfSignaturesRequiredForCapitalCommitment: fund.noOfSignaturesRequiredForCapitalCommitment,
                noOfSignaturesRequiredForSideLetter: fund.noOfSignaturesRequiredForSideLetter,
                investorQuestions: fund.investorQuestions,
                inProgress: 0,
                closeReady: 0,
                closed: 0,
                isFundLocked: fund.isFundLocked,
                noOfSignaturesRequiredForFundAmmendments:(fundAmmendment && fundAmmendment.noOfSignaturesRequiredForFundAmmendments) ?  fundAmmendment.noOfSignaturesRequiredForFundAmmendments : 1

            }
        });
    } catch (e) {
        next(e)
    }

}

const _getGpDelegates = async (vcFirmId, fundId) => {

    const gpAccountDelegates = await models.VcFrimDelegate.findAll({
        attributes: ['delegateId'],
        where: {
            vcFirmId: vcFirmId,
            type: 'GP'
        },
        include: [{
            model: models.User,
            as: 'details',
            required: true,
            include:[{
                model:models.Account,
                as:'accountDetails'
            }]
        }]
    });

    let delegateIds = _.map(gpAccountDelegates,'delegateId');
    const historyData = await models.History.findAll({
        where:{
            fundId,
            userId:{
                [Op.in]:delegateIds
            },
            accountType:'GPDelegate'
        },
        order:[['id','DESC']]

    });

    if (!gpAccountDelegates.length) {
        return [];
    }

    const fundGpDelegates = await models.GpDelegates.findAll({
        where: {
            fundId: fundId,
        }
    });

    return gpAccountDelegates.map(delegate => {

        const selected = _.find(fundGpDelegates, { delegateId: delegate.delegateId });
        const historyStatus = _.find(historyData,{userId:  delegate.delegateId,fundId:fundId});

        return {
            id: delegate.details.id,
            firstName: delegate.details.firstName,
            lastName: delegate.details.lastName,
            middleName: delegate.details.middleName,
            email: delegate.details.email,
            accountType: delegate.details.accountType,
            profilePic: delegate.details.profilePic,
            organizationName: delegate.details ? delegate.details.organizationName : '',
            status:historyStatus ? historyStatus.action:null,
            selected: selected ? true : false,
            copyLink: delegate.details && delegate.details.accountDetails && delegate.details.accountDetails.isEmailConfirmed ? false: true,
            createdAt: historyStatus ? historyStatus.createdAt :null,
            updatedAt: historyStatus ? historyStatus.updatedAt :null,
            emailConfirmCode: delegate.details && delegate.details.accountDetails ? delegate.details.accountDetails.emailConfirmCode : null,
            gPDelegateRequiredDocuSignBehalfGP: _.get(selected, 'gPDelegateRequiredDocuSignBehalfGP', null),
            gPDelegateRequiredConsentHoldClosing: _.get(selected, 'gPDelegateRequiredConsentHoldClosing', null)
        };

    })

}

const fundStore = async (req, res, next) => {

    try {

        const userId = req.userId;

        const statusId = (req.user.accountType == 'FSNETAdministrator') ? 15 : 12;

        const {
            fundId,
            vcfirmId,
            fundType,
            legalEntity,
            fundCommonName,
            fundManagerTitle,
            hardCapApplicationRule,
            fundManagerCommonName,
            fundHardCap,
            fundManagerLegalEntityName,
            fundTargetCommitment,
            capitalCommitmentByFundManager,
            percentageOfLPCommitment,
            percentageOfLPAndGPAggregateCommitment,
            generalPartnersCapitalCommitmentindicated = null,
            requestContainImage = false,
            fundEntityType,
            timezone,
            isNotificationsEnabled
        } = req.body

        let fundentitytype = (fundEntityType==1) ? 1 : (fundEntityType==2) ? 2 : null
       
        const fundData = {
            vcfirmId:vcfirmId,
            legalEntity,
            fundCommonName,
            fundManagerTitle,
            fundManagerCommonName,
            fundManagerLegalEntityName,
            hardCapApplicationRule,
            fundHardCap,
            fundType: fundType,
            fundTargetCommitment,
            capitalCommitmentByFundManager,
            percentageOfLPCommitment,
            percentageOfLPAndGPAggregateCommitment,
            generalPartnersCapitalCommitmentindicated,
            fundImage: await commonHelper.getFileInfo(req.file),
            statusId: statusId,
            fundEntityType : fundentitytype,
            timezone,
            isNotificationsEnabled
        };



        if (fundId) {

            const fund = await models.Fund.findOne({
                where: {
                    id: fundId
                }

            });

            if (!fund) {
                return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
            }


            if (requestContainImage == false || !requestContainImage || requestContainImage == 'false') {
                delete fundData.fundImage
            }

            fundData.updatedBy = userId;

            delete fundData.statusId;

            await models.Fund.update(fundData, {
                where: {
                    id: fundId
                }
            });
            const fundInfo = await models.Fund.findOne({
                where: {
                    id: fundId
                },
                include: [{
                    model: models.FundStatus,
                    as: 'fundStatus',
                    required: false,
                }]
            });

            return res.json({
                data: {
                    id: fundInfo.id,
                    vcfirmId: fundInfo.vcfirmId,
                    isNotificationsEnabled:fundInfo.isNotificationsEnabled,
                    gpId: fundInfo.gpId,
                    legalEntity: fundInfo.legalEntity,
                    fundCommonName: fundInfo.fundCommonName,
                    fundManagerTitle: fundInfo.fundManagerTitle,
                    hardCapApplicationRule: fundInfo.hardCapApplicationRule,
                    fundManagerCommonName: fundInfo.fundManagerCommonName,
                    fundHardCap: fundInfo.fundHardCap,
                    generalPartnersCapitalCommitmentindicated: fundInfo.generalPartnersCapitalCommitmentindicated,
                    fundManagerLegalEntityName: fundInfo.fundManagerLegalEntityName,
                    percentageOfLPCommitment: fundInfo.percentageOfLPCommitment,
                    percentageOfLPAndGPAggregateCommitment: fundInfo.percentageOfLPAndGPAggregateCommitment,
                    fundTargetCommitment: fundInfo.fundTargetCommitment,
                    capitalCommitmentByFundManager: fundInfo.capitalCommitmentByFundManager,
                    fundImage: fundInfo.fundImage,
                    partnershipDocument: fundInfo.partnershipDocument,
                    statusId: fundInfo.statusId,
                    status: fundInfo.fundStatus.name,
                    fundType: fundInfo.fundType
                }
            });
        } else {
            fundData.createdBy = userId;

            const vcFirm = await models.VCFirm.findOne({
                where: {
                    id: vcfirmId
                }
            });

            fundData.gpId = vcFirm.gpId;

            const fund = await models.Fund.create(fundData);


            if (req.user.accountType == 'GPDelegate') {
                await models.GpDelegates.create({
                    fundId: fund.id,
                    delegateId: req.userId,
                    gpId: fund.gpId,
                    gPDelegateRequiredDocuSignBehalfGP: true,
                    gPDelegateRequiredConsentHoldClosing: true,
                    createdBy: req.userId,
                    updatedBy: req.userId,
                });
            }else if(req.user.accountType == 'SecondaryGP') {
                await models.GpSignatories.create({
                    fundId: fund.id,
                    signatoryId: req.userId,
                    gpId: fund.gpId,
                    createdBy: req.userId,
                    updatedBy: req.userId,
                });
            }

            const fundInfo = await models.Fund.findOne({
                where: {
                    id: fund.id
                },
                include: [{
                    model: models.FundStatus,
                    as: 'fundStatus',
                    required: false,
                }]
            });

            return res.json({
                data: {
                    id: fundInfo.id,
                    vcfirmId: fundInfo.vcfirmId,
                    gpId: fundInfo.gpId,
                    legalEntity: fundInfo.legalEntity,
                    fundCommonName: fundInfo.fundCommonName,
                    fundManagerTitle: fundInfo.fundManagerTitle,
                    hardCapApplicationRule: fundInfo.hardCapApplicationRule,
                    fundManagerCommonName: fundInfo.fundManagerCommonName,
                    fundHardCap: fundInfo.fundHardCap,
                    generalPartnersCapitalCommitmentindicated: fundInfo.generalPartnersCapitalCommitmentindicated,
                    fundManagerLegalEntityName: fundInfo.fundManagerLegalEntityName,
                    percentageOfLPCommitment: fundInfo.percentageOfLPCommitment,
                    percentageOfLPAndGPAggregateCommitment: fundInfo.percentageOfLPAndGPAggregateCommitment,
                    fundTargetCommitment: fundInfo.fundTargetCommitment,
                    capitalCommitmentByFundManager: fundInfo.capitalCommitmentByFundManager,
                    fundImage: fundInfo.fundImage,
                    partnershipDocument: fundInfo.partnershipDocument,
                    statusId: fundInfo.statusId,
                    status: fundInfo.fundStatus.name,
                    fundType: fundInfo.fundType
                }
            });

        }
    } catch (e) {
        next(e);
    }
}

const removeFundGPDelegate = async (req, res, next) => {
    try {
        const { userId, fundId } = req.body;

        if (!Number(fundId)) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }

        await models.GpDelegates.destroy({
            where: {
                delegateId: userId,
                fundId: fundId
            }
        })

        await models.VcFrimDelegate.destroy({
            where: {
                delegateId: userId
            }
        })
                
        return res.json(true)
    } catch (e) {
        next(e)
    }
}


const removeLp = async (req, res, next) => {
    try { // delete fund inv
        const { lpId, fundId } = req.body;
        if (!Number(fundId)) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }
        await models.FundSubscription.destroy({
            where: {
                fundId: fundId,
                lpId: lpId
            }
        })

        return res.json(true)
        

    } catch (e) {
        return next(e)
    }
}

const removeFundLpDelegate = async (req, res, next) => {
    try {

        const { delegateId, fundId } = req.body;

        // if (!Number(fundId)) {

        //     return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);

        // }

        // if (!Number(delegateId)) {
        //     return errorHelper.error_400(res, 'delegateId', messageHelper.delegateIdValidate);
        // }

        await models.LpDelegate.destroy({
            where: {
                delegateId: delegateId,
                fundId: fundId
            }
        });

        return res.json(true)

    } catch (e) {
        next(e)
    }
}

const additionalSignatoryPages = async (req, res, next) => {

    const { fundId } = req.params

    if (!Number(fundId)) {
        return res.status(400).json({
            "errors": [
                {
                    "param": "fundId",
                    "msg": messageHelper.fundIdValidate
                }
            ]
        })
    }

    additionalSignatoryPagesUpload(req, res, async function (err) {
        console.log(req.file,'aldfjsldjsfj');
        if (err) {
            return res.status(400).json({
                "errors": [
                    {
                        "msg": err.message
                    }
                ]
            });
        }

        if (!req.file) {
            return res.status(400).json({
                "errors": [
                    {
                        "msg": messageHelper.selectFileToUpload
                    }
                ]
            });
        }

        const ext = path.extname(req.file.filename)

        console.log({isAdditionalSignatoryPagesAddedToConslidatedPDF: false, 
            additionalSignatoryPagesUpdatedDate: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z') , 
            additionalSignatoryPages: await commonHelper.getFileInfo(req.file), updatedBy: req.userId });

        await models.Fund.update({isAdditionalSignatoryPagesAddedToConslidatedPDF: false, 
            additionalSignatoryPagesUpdatedDate: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z') , 
            additionalSignatoryPages:  await commonHelper.getFileInfo(req.file), updatedBy: req.userId }, {
            where: {
                id: fundId
            }
        });

        return res.json({
            data: {
                additionalSignatoryPages: await commonHelper.getFileInfo(req.file, false)
            }
        });

    })


}


const fundDocumentUpload = async (req, res, next) => {

    const { fundId } = req.params

    if (!Number(fundId)) {
        return res.status(400).json({
            "errors": [
                {
                    "param": "fundId",
                    "msg": messageHelper.fundIdValidate
                }
            ]
        })
    }


    if (!req.file) {
        return res.status(400).json({
            "errors": [
                {
                    "msg": messageHelper.selectFileToUpload
                }
            ]
        });
    }

    //console.log(req.file);

    const ext = path.extname(req.file.filename);
    //console.log(ext,'Hello Arjun....');
    
    const partnershipDocumentPageCount = await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(req.file.path);

    models.Fund.update({ partnershipDocument: await commonHelper.getFileInfo(req.file),partnershipDocumentPageCount, updatedBy: req.userId }, {
        where: {
            id: fundId
        }
    }).then(async (data) => res.json({
        data: {
            partnershipDocument: await commonHelper.getFileInfo(req.file, false)
        }
    })).catch(error => next(error))



}

const removeFundFile = async (req, res, next) => {
    try {
        const { fundId } = req.body;

        models.Fund.update({ partnershipDocument: '' }, {
            where: {
                id: fundId
            }
        }).then(data => res.json(true)).catch(error => next(error))
    } catch (e) {
        next(e)
    }
}

const publishFund = async (req, res, next) => {

    try {
        const { fundId } = req.body;

        let invitedDate =  moment().format('MM-DD-YYYY');

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }, 
            include: [{
                model: models.User,
                as: 'gp'
            }]
        });

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        if (fund.statusId == 12 || fund.statusId == 15) { //Fund is changing from New draft/Admin draft to open ready

            let investors = await models.FundSubscription.findAll({
                where: {
                    fundId,
                    status:17,
                    isInvestorInvited: 1
                },
                include: [{
                    model: models.User,
                    as: 'lp',
                    required: true,
                    include:[{
                        model: models.Account,
                        as:'accountDetails',
                        required:true
                    }]
                },{
                    model: models.Fund,
                    required: true,
                    as: 'fund',
                    include:[
                    {
                        model: models.User,
                        as:'gp',
                        required:true
                    }]
                },{
                    model: models.LpDelegate,
                    as: 'lpDelegates',
                    include:[{
                        model: models.User,
                        required: true,
                        as: 'details',
                        include:[{
                            model: models.Account,
                            as:'accountDetails',
                            required:true
                        }]
                    }]
                }]
            });

            const gpDelegatesAndSignatories = await models.Fund.findAll({
                where:{
                    id:fundId,
                    deletedAt:null
                },
                include:[{
                        model: models.GpDelegates,
                        as: 'gpDelegatesSelected',
                        required:false,
                        include:[{
                            model: models.User,
                            required: true,
                            as: 'details',
                            include:[{
                                model: models.Account,
                                as:'accountDetails',
                                required:true
                            }]
                        }]
                    },{
                        model: models.GpSignatories,
                        as: 'gpSignatoriesSelected',
                        required:false,
                        include:[{
                            model: models.User,
                            required: true,
                            as: 'signatoryDetails',
                            include:[{
                                model: models.Account,
                                as:'accountDetails',
                                required:true
                            }]
                        }]
                    }
                ]
            })

            
            for(let investor of investors){

                // GP has invited you to join fund (existing account) - LP
                const alertData = {
                    htmlMessage: messageHelper.invitationToJoinParticularFundAlertMessage,
                    message: 'Invitation to join particular fund',
                    fundManagerCommonName: investor.fund.fundManagerCommonName,
                    fundName: investor.fund.fundCommonName,
                    fundId: investor.fund.id,
                    sentBy: req.userId,
                    type:'FUND_INVITED'
                };
        
                notificationHelper.triggerNotification(req, req.userId, investor.lp.id, false, alertData,investor.lp.accountId,null);
                
                const loginOrRegisterLink = investor.lp.accountDetails && investor.lp.accountDetails.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${investor.lp.accountDetails.emailConfirmCode}`;
                const content = investor.lp.accountDetails && investor.lp.accountDetails.isEmailConfirmed ? "to login and get going." : "to setup your account and get going." 
        
                if (investor.lp.accountDetails.isEmailNotification && req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
                    emailHelper.sendEmail({
                        toAddress: investor.lp.email,
                        subject: messageHelper.addLPEmailSubject,
                        data: {
                            name: commonHelper.getFullName(investor.lp),
                            loginOrRegisterLink: loginOrRegisterLink,
                            fundCommonName: investor.fund.fundCommonName,
                            content,
                            user:investor.lp
                        },
                        htmlPath: "fundLPInviation.html"
                    }).catch(error => {
                        commonHelper.errorHandle(error)
                    })
                }
                
                for(let investorDelegate of investor.lpDelegates){
            
                    notificationHelper.triggerNotification(req, req.userId, investorDelegate.id, false, alertData,investorDelegate.details.accountId,null);
            
                    const loginOrRegisterLink = investorDelegate.details.accountDetails.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${investorDelegate.details.accountDetails.emailConfirmCode}`;
                    const content = investorDelegate.details.accountDetails.isEmailConfirmed ? "to login and get going." : "to setup your account and get going." 
            
                    if (investorDelegate.details.accountDetails.isEmailNotification ||      req.user.accountType != 'FSNETAdministrator') { // if setting is enabled
                        emailHelper.sendEmail({
                            toAddress: investorDelegate.details.email,
                            subject: messageHelper.assignLpDelegateEmailSubject,
                            data: {
                                name: commonHelper.getFullName(investorDelegate.details),
                                fundName: fund.legalEntity,
                                lpName: commonHelper.getInvestorName(investor),
                                loginOrRegisterLink: loginOrRegisterLink,
                                content,
                                user:investorDelegate
                            },
                            htmlPath: "fundLpDelegateInviation.html"
                        }).catch(error => {
                            commonHelper.errorHandle(error)
                        })
                    }
                }


                // //Fund created from Admin, send mails to GpDelegate and GpSignatory
                if(fund.statusId == 15){
                    let gpDelegateIds = _.map(investor.fund.gpDelegatesSelected,'delegateId');
                    let gpSignatoryIds = _.map(investor.fund.gpSignatoriesSelected,'signatoryId');

                    if(gpDelegateIds.length > 0 || gpSignatoryIds.length>0){
                        await sendMailsToGpDelegateAndSignatories(investor.fund, gpDelegateIds, gpSignatoryIds)
                    }
                }
        
            }

            //Only when created from Admin screen
            if(fund.statusId == 15){

                for(let gpDelegatesAndSignatory of gpDelegatesAndSignatories){
                    await sendMailsToGpDelegate(fund,gpDelegatesAndSignatory.gpDelegatesSelected)
                    await sendMailsToGpSignatories(fund,gpDelegatesAndSignatory.gpSignatoriesSelected)
                }
            }

            // Update as investors are invited, status from investor pending to investor status
            await models.FundSubscription.update({
                invitedDate,
                isInvestorInvited:2,
                status: 1 // status 1 = Open
            }, {
                where: {
                    id : {
                        [Op.in] : _.map(investors,'id')
                    }
                }
            });
        }
        
        await models.Fund.update({ statusId: 13, updatedBy: req.userId }, {
            where: {
                id: fundId
            }
        });


        return res.json(true);
     
    } catch (e) {
        next(e)
    }
}


const sendRemainderToLp = async (req, res, next) => {
    try {
        const { lpId, fundId } = req.body

        const user = await models.User.findOne({
            where: {
                id: lpId
            }
        })

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        })

        emailHelper.sendEmail({
            toAddress: user.email,
            subject: messageHelper.lpSendReminder,
            data: {
                fundName: fund.legalEntity,
                name: commonHelper.getFullName(user),
                user:user
            },
            htmlPath: "lpSendReminder.html"
        }).catch(error => {
            commonHelper.errorHandle(error)
        })

        // GP has sent a reminder
        const alertData = {
            htmlMessage: messageHelper.sentReminderAlertMessage,
            message: 'has sent a reminder',
            fundManagerCommonName: fund.fundManagerCommonName,
            fundName: fund.fundCommonName,
            fundId: fund.id,
            sentBy: req.userId
        };

        notificationHelper.triggerNotification(req, req.userId, lpId, false, alertData,user.accountId,null);

        return res.json({ data: 'ok' })
    } catch (e) {
        next(e)
    }
}

const deactivate = async (req, res, next) => {
    try {
        const { fundId, deactivateReason } = req.body;


        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        })

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        Promise.all([
            models.Fund.update({ statusId: 14, deactivateReason: deactivateReason }, {
                where: {
                    id: fundId
                }
            }),
            models.FundSubscription.update({ status: 14 }, {
                where: {
                    fundId
                }
            })
        ]).then(async response => {

            const lps = await models.FundSubscription.findAll({
                where: {
                    fundId: fundId
                },
                include: [{
                    model: models.User,
                    as: 'lp',
                    required: true
                }]
            });

            for (let lp of lps) {

                const user = lp.lp;
                if (user) {
                    emailHelper.sendEmail({
                        toAddress: user.email,
                        subject: messageHelper.fundDeactivated,
                        data: {
                            gpName: commonHelper.getFullName(user),
                            fundName: fund.legalEntity,
                            user:lp
                        },
                        htmlPath: "fundDeactivated.html"
                    }).catch(error => {
                        commonHelper.errorHandle(error)
                    })
                }
            }

            eventHelper.saveEventLogInformation('Fund Deactivated', fundId, '', req);

            return res.json({ data: fund.lpsSelected })

        }).catch(error => next(error))
    } catch (e) {
        next(e)
    }
}

const removeClosedLp = async (req, res, next) => {

    try {
        const { fundId, lpId } = req.body

        await models.FundSubscription.destroy({
            where: {
                lpId: lpId,
                fundId: fundId
            }
        });

        // delete all gp signatories approved earlier by GP
        await models.GpSignatoryApproval.destroy({
            where: {
                fundId: fundId,
                lpId: lpId,
            }
        });

        return res.json(true)

    } catch (e) {
        next(e)
    }
}

const deleteFund = async (req, res, next) => {

    const { fundId } = req.body;

    try {

        await models.Fund.update({
            deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
        }, {
                where: {
                    id: fundId
                }
            });

        await models.FundSubscription.update({
            deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
        }, {
                where: {
                    fundId: fundId
                }
            });

        return res.send(true);

    } catch (error) {
        next(error);
    }

}

const removeAdditionalSignatoryPages = async (req, res, next) => {
    try {
        const { fundId } = req.body;

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        });

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        if (fund.additionalSignatoryPages && fs.existsSync(fund.additionalSignatoryPages.path)) {
            fs.unlinkSync(fund.additionalSignatoryPages.path);
        }

        models.Fund.update({ additionalSignatoryPagesUpdatedDate: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z'), additionalSignatoryPages: null }, {
            where: {
                id: fundId
            }
        }).then(data => res.json(true)).catch(error => next(error));

    } catch (e) {
        next(e)
    }
}


const changeFundGP = async (req, res, next) => {

    const { fundId, userId, vcfirmIdOld } = req.body; //userId = current gp id
    // create a table called - gpChangeRequests
    // newGPId = get id of the email with type of GP, sometime we might need to create a user.
    // currentGpId, newGpId, convertCurrentGpToSignatory (bool), radomToken, createdBy...., 

    // get user
    const user = await models.User.findOne({
        where: {
            id: userId
        }
    });

    //get fund details
    const fund = await models.Fund.findOne({
        where: {
            id: fundId
        }
    })
    
    //check user exists
    var gpAccount = await models.User.findOne({
        where: {
            email: user.email,
            accountType: 'GP'
        }
    });
    var vcfirm;
    let gprequestdata = {}
    //genrate token
    let code = commonHelper.generateToken(); 

    // create new account with gp signatory details    
    if (!gpAccount) {
        //console.log('newgP');

        let accountId = user.accountId

        // create new user
        gpAccount = await models.User.create({
            firstName: user.firstName,
            lastName: user.lastName,
            middleName: user.middleName,
            email: user.email,
            organizationName: user.organizationName,
            city: user.city,
            state: user.state,
            country: user.country,
            cellNumber: user.cellNumber,
            profilePic: JSON.stringify(user.profilePic),
            isImpersonatingAllowed: 1,
            isEmailConfirmed: 1,
            accountType: 'GP',
            streetAddress1: user.streetAddress1,
            streetAddress2: user.streetAddress2,
            zipcode: user.zipcode,
            signaturePic: user.signaturePic,
            primaryContact: JSON.stringify(user.primaryContact),
            isEmailNotification: user.isEmailNotification,
            accountId:accountId
        });
        //console.log('newgP created');
        vcFirm = await models.VCFirm.create({
            firmName: user.firstName,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            gpId: gpAccount.id,
            subscriptonType: 1
        });
        //console.log('newgP vcfirm');
        // assign gp roles to new user
        await models.UserRole.create({ roleId: 2, userId: gpAccount.id });
        //console.log('newgP role creaed');

        //update old records to status 1
        await models.gpChangeRequest.update({status:1}, {
            where: {
                fundId:fundId,
                currentGpId:userId,
                newGpId:gpAccount.id,
            }
        });        

        //save the data
        gprequestobj = {
            fundId:fundId,
            currentGpId:userId,
            newGpId:gpAccount.id,
            vcfirmIdOld:vcfirmIdOld,
            randomToken:code,
            convertCurrentGpToSignatory:0,
            userId:req.userId,
            status:0
        }
        
        gprequestdata = await models.gpChangeRequest.create(gprequestobj)    
        
        //console.log("requestobj in new user********",gprequestobj)

        // trigger email with new GP account.
        emailHelper.sendEmail(
            {
                toAddress: gpAccount.email,
                subject: 'You have been assigned as Fund Manager.',
                data: {
                    name: commonHelper.getFullName(gpAccount),
                    email: gpAccount.email,
                    previousfundmanager:req.user.firstName+" "+req.user.lastName,
                    fundname:fund.fundManagerCommonName,
                    oldacceptancelink:`${config.get('clientURL')}/user/accept?code=${code}&gprequestid=${gprequestdata.id}`,
                    user:gpAccount
                    //loginOrRegisterLink: `${config.get('clientURL')}/login`
                },
                htmlPath: "newConvertedGPAccount.html"
            });

    } else {

        vcFirm = await models.VCFirm.findOne({
            where: {
                gpId: gpAccount.id
            }
        });

        //update old records to status 1
        await models.gpChangeRequest.update({status:1}, {
            where: {
                fundId:fundId,
                currentGpId:userId,
                newGpId:gpAccount.id,
            }
        });  

        //save the data
        gprequestobj = {
            fundId:fundId,
            currentGpId:userId,
            newGpId:gpAccount.id,
            vcfirmIdOld:vcfirmIdOld,
            randomToken:code,
            convertCurrentGpToSignatory:0,
            userId:req.userId,
            status:0
        }
        //console.log("requestobj in exist user********",gprequestobj)
        //save request data
        gprequestdata = await models.gpChangeRequest.create(gprequestobj)        
        // trigger email with existence GP account.
        emailHelper.sendEmail(
            {
                toAddress: gpAccount.email,
                subject: 'You have been assigned as Fund Manager.',
                data: {
                    name: commonHelper.getFullName(gpAccount),
                    email: gpAccount.email,
                    previousfundmanager:req.user.firstName+" "+req.user.lastName,
                    fundname:fund.fundManagerCommonName,
                    oldacceptancelink:`${config.get('clientURL')}/user/accept?code=${code}&gprequestid=${gprequestdata.id}`,
                    user:gpAccount           
                    //loginOrRegisterLink: `${config.get('clientURL')}/login`
                },
                htmlPath: "newConvertedGPAccount.html"
            });            
    }

    const loginUser = await models.User.findOne({
        where: { id: gpAccount.id },
        include: [{
            model: models.Role,
            as: 'roles',
            required: true,
            include: [{
                model: models.Permission,
                as: 'permissions',
                required: true
            }]
        }]
    });

    if (!loginUser) {
        return errorHelper.error_400(res, 'id', messageHelper.userIdNotFound);
    }
 

    return res.json({  
        gprequestid:gprequestdata.id,   
        user: {
            firstName: loginUser.firstName,
            lastName: loginUser.lastName,
            middleName: loginUser.middleName,
            id: loginUser.id,
            cellNumber: loginUser.cellNumber,
            accountType: loginUser.accountType,
            profilePic: loginUser.profilePic,
            vcfirmId: vcfirm ? vcfirm.id : null
        }
    });

}



const convertGPToSignatory = async (req, res, next) => {

    const { fundId, oldGpId, gpId ,status,gprequestid } = req.body; 

    await models.gpChangeRequest.update({ convertCurrentGpToSignatory: status }, {
        where: {
            id: gprequestid
        }
    });

    return res.json({
        data: true
    })
}

//covert gp signatory
const changeGPToFundManager = async (req,res,next) => {
    
    const { gprequestid,code } = req.body; 
    
    //get changerequest data
    let gpchangerequest = await models.gpChangeRequest.findOne({
        where: {
            id: gprequestid,
            randomToken:code,
            deletedAt:null
        }
    }) 

    //token not matched
    if(!gpchangerequest){    
        return errorHelper.error_400(res, 'id', messageHelper.linkExpired)             
    }    

    let vcfirmIdOld = gpchangerequest.vcfirmIdOld
    let userId = oldGpId = gpchangerequest.currentGpId
    let gpId = gpchangerequest.newGpId
    let fundId = gpchangerequest.fundId
    const currentUserId = gpchangerequest.userId

    // get user
    const user = await models.User.findOne({
        where: {
            id: currentUserId
        }
    });

    //get fund details
    const fund = await models.Fund.findOne({
        where: {
            id: fundId
        }
    })

    const fundfirmid = fund.vcfirmId

    //get gp account details
    const gpAccount = await models.User.findOne({
        where: {
            id: gpchangerequest.newGpId
        }
    })

    //get vcfirm details
    const vcFirm = await models.VCFirm.findOne({
        where: {
            gpId: gpAccount.id
        }
    });    

    // change the user, 
    await models.Fund.update({
        gpId: gpAccount.id,
        vcfirmId: vcFirm.id
    }, {
            where: {
                id: fundId
            }
    });      
    
    // GpDelegates 
    await models.GpDelegates.update({  gpId: gpAccount.id },{
        where: {
            fundId: fundId
        }
    });

    // copy delegates to new gp vc firm
    const gpDelegatesExisting = await models.VcFrimDelegate.findAll({
        where: {
            vcfirmId: vcFirm.id
        }
    })

    const existingVCfirmDelegatesIds = _.map(gpDelegatesExisting,'delegateId');

    const gpDelegatesOldVcFirm = await models.VcFrimDelegate.findAll({
        where: {
            vcfirmId: vcfirmIdOld
        }
    })

    const existingOldVCfirmDelegateIds = _.map(gpDelegatesOldVcFirm,'delegateId');

    const gpDelegates = _.filter(_.difference(existingOldVCfirmDelegateIds, existingVCfirmDelegatesIds));

    const gpDelegatesData = [];
    for(gpDelegate of gpDelegates) {
        gpDelegatesData.push({
            vcfirmId: vcFirm.id,
            delegateId: gpDelegate,
            type: 'GP',
            createdBy: gpAccount.id,
            updatedBy: gpAccount.id
        });
    }

    models.VcFrimDelegate.bulkCreate(gpDelegatesData); 

    // update gp id for signatories.
    await models.GpSignatories.update({  gpId: gpAccount.id },{
        where: {
            fundId: fundId
        }
    });

    // copy signatories to new vcfirm.
    const gpSignatoriesExisting = await models.VcFirmSignatories.findAll({
        where: {
            vcfirmId: vcFirm.id
        }
    });

    const existingVCfirmSignatoriesIds = _.map(gpSignatoriesExisting,'signatoryId');

    const existingOldVCfirmSignatories = await models.VcFirmSignatories.findAll({
        where: {
            vcfirmId:  vcfirmIdOld,
        }
    })

    const existingOldVCfirmSignatoriesIds = _.map(existingOldVCfirmSignatories,'signatoryId');

    const gpSignatories = _.filter(_.difference(existingOldVCfirmSignatoriesIds, existingVCfirmSignatoriesIds));

    //console.log('gpSignatoriesgpSignatoriesgpSignatories-test',existingOldVCfirmSignatoriesIds,existingVCfirmSignatoriesIds,  gpSignatories);

    const data = [];
    for(gpSignatory of gpSignatories) {
        data.push({
            vcfirmId: vcFirm.id,
            signatoryId: gpSignatory,
            type: 'SecondaryGP',
            createdBy:  gpAccount.id,
            updatedBy:  gpAccount.id
        });
    } 

    await models.VcFirmSignatories.bulkCreate(data); 

    // remove current user as signatory
    await models.GpSignatories.destroy({
        paranoid: false,
        where: { fundId: fundId, signatoryId: userId }
    });             
 

    //user accepts
    if(gpchangerequest.convertCurrentGpToSignatory){   
        
        var secondaryGPAccount = await models.User.findOne({
            where: {
                email: user.email,
                accountType: 'SecondaryGP'
            }
        })
        
        
        if (!secondaryGPAccount) {           

            let accountId = user.accountId
            let code = commonHelper.generateToken();
    
            // create new user
            secondaryGPAccount = await models.User.create({
                firstName: user.firstName,
                lastName: user.lastName,
                middleName: user.middleName,
                email: user.email,
                organizationName: user.organizationName,
                city: user.city,
                state: user.state,
                country: user.country,
                cellNumber: user.cellNumber,
                profilePic: JSON.stringify(user.profilePic),
                isImpersonatingAllowed: 1,
                isEmailConfirmed: 1,
                accountType: 'SecondaryGP',
                streetAddress1: user.streetAddress1,
                streetAddress2: user.streetAddress2,
                zipcode: user.zipcode,
                signaturePic: user.signaturePic,
                primaryContact: JSON.stringify(user.primaryContact),
                isEmailNotification: user.isEmailNotification,
                accountId:accountId
            });
    
            //console.log('assignRole');
    
            // assign gp roles to new user
            await models.UserRole.findOrCreate({
                where: { roleId: 6, userId: secondaryGPAccount.id },
                defaults: { roleId: 6, userId: secondaryGPAccount.id }
            });
    
            await models.GpSignatories.findOrCreate({
                paranoid: false,
                where: { fundId: fundId, signatoryId: secondaryGPAccount.id, gpId: gpId }, defaults: {
                    fundId: fundId, signatoryId: secondaryGPAccount.id, gpId: gpId,
                    createdBy: currentUserId, updatedBy: currentUserId
                }
            }).spread((gpSignatories, created) => gpSignatories.restore());
    
            //console.log('VcFirmSignatories');
            await models.VcFirmSignatories.findOrCreate({
                paranoid: false,
                where: { vcfirmId: vcFirm.id, signatoryId: secondaryGPAccount.id, type: 'SecondaryGP' }, defaults: {
                    vcfirmId: vcFirm.id, signatoryId: secondaryGPAccount.id, type: 'SecondaryGP',
                    createdBy: currentUserId, updatedBy: currentUserId
                }
            }).spread((vcFirmSignatories, created) => vcFirmSignatories.restore());
    
    
            //console.log('sendEmail');
            // trigger email with new GP account.
            emailHelper.sendEmail(
                {
                    toAddress: secondaryGPAccount.email,
                    subject: 'Details of your new signatory Account',
                    data: {
                        name: commonHelper.getFullName(secondaryGPAccount),
                        email: secondaryGPAccount.email, 
                        loginOrRegisterLink : `${config.get('clientURL')}/login`,
                        user:secondaryGPAccount
                       // link: secondaryGPAccount.password ? false : `${config.get('clientURL')}/user/setPassword?code=${secondaryGPAccount.emailConfirmCode}&id=${secondaryGPAccount.id}`
                    },
                    htmlPath: "newSignatoryAccount.html"
                });
    
        } else { 

            await models.GpSignatories.findOrCreate({
                paranoid: false,
                where: { fundId: fund.id, signatoryId: secondaryGPAccount.id, gpId: fund.gpId },
                defaults: {
                    fundId: fund.id, signatoryId: secondaryGPAccount.id, gpId: fund.gpId, 
                    createdBy: currentUserId, updatedBy: currentUserId
                }
            }).spread((gpSignatories, created) => gpSignatories.restore());


            //console.log('VcFirmSignatories');
            await models.VcFirmSignatories.findOrCreate({
                paranoid: false,
                where: { vcfirmId: vcFirm.id, signatoryId: secondaryGPAccount.id, type: 'SecondaryGP' }, defaults: {
                    vcfirmId: vcFirm.id, signatoryId: secondaryGPAccount.id, type: 'SecondaryGP',
                    createdBy: currentUserId, updatedBy: currentUserId
                }
            }).spread((vcFirmSignatories, created) => vcFirmSignatories.restore());

        }

        //update gpsignedid in signaturetrack table for exclamation mark issue -- start

        //get subscription ids for fund 
        let fundSubscriptions = await models.FundSubscription.findAll({
            attributes: ['id'],
            where: {
                fundId: fundId
            }
        });        
        
        let subscriptionIds = [];
        if (fundSubscriptions) {
            subscriptionIds = _.map(fundSubscriptions, 'id');
        }
        //update signature track table with new gp id
        await models.SignatureTrack.update({ signedUserId:gpchangerequest.newGpId},{
            where:{
                subscriptionId: {
                    [Op.in]: subscriptionIds
                },
                signedByType:'GP'
                }
        })        

        //--end

    }

    //update token to empty
    await models.gpChangeRequest.update({randomToken:0,status:1,deletedAt: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z') }, {
        where: {
            id: gprequestid
        }
    });


    return res.json({
        data: true
    })
}


const getSubscriptionDetails = async (req, res, next) => {
    try {
        const { fundId,subscriptionId } = req.params       
        let fund;      

        if (!Number(fundId)) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }

        if(subscriptionId && subscriptionId>0){
            fund =  await models.FundSubscription.findOne({
                attributes:['subscriptionAgreementPath'],
                where: {
                    id:subscriptionId
                }
            })
        } else {
            fund = await models.Fund.findOne({
                attributes:['subscriptionAgreementPath'],
                where: {
                    id: fundId
                }
            })            
        }

        if (!fund) {
            return res.status(404).json({
                "errors": [
                    {
                        "msg": messageHelper.fundNotFound
                    }
                ]
            });
        }
        
        let subscriptionAgreementInfo = {}
        if(fund.subscriptionAgreementPath){
            let one = fund.subscriptionAgreementPath.one
            let two = fund.subscriptionAgreementPath.two
            
            if(one){
                let tempObject = {}
                let oneLength = one.split("/")
                tempObject.filename =  fund.subscriptionAgreementPath.one_originalfilename ? fund.subscriptionAgreementPath.one_originalfilename : oneLength[oneLength.length-1]
                tempObject.url =  `${config.get('serverURL')}/api/v1/document/view/one/${fundId}`  
                subscriptionAgreementInfo.one = tempObject
            }

            if(two){
                let tempObject = {}
                let twoLength = two.split("/")
                tempObject.filename =  fund.subscriptionAgreementPath.two_originalfilename ? fund.subscriptionAgreementPath.two_originalfilename : twoLength[twoLength.length-1]
                tempObject.url =  `${config.get('serverURL')}/api/v1/document/view/two/${fundId}`  
                subscriptionAgreementInfo.two = tempObject
            }            
            
        } 

        console.log("*************subscriptionAgreementInfo*****",subscriptionAgreementInfo)

        return res.json({
            data: {               
                subscriptionAgreementInfo:subscriptionAgreementInfo 
            }
        })
    } catch (e) {
        next(e)
    }

}

//get firmlp
const getFirmLPs = async (req, res, next) => {

    try {
        const { fundId } = req.params

        const {  offset = 0 } = req.query

        if (!Number(fundId)) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }

        let fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [
                {
                    model: models.User,
                    as: 'gp',
                    required: true,
                    include:[{
                        model: models.Account,
                        as:'accountDetails',
                        required:true
                    }]                     
                },
                {
                    model: models.FundStatus,
                    as: 'fundStatus',
                    required: false,
                },
                {
                    model: models.GpSignatories,
                    attributes: ['signatoryId'],
                    as: 'gpSignatoriesSelected',
                    required: false,
                    where: {
                        fundId: fundId
                    }
                },
                {
                    model: models.InvestorQuestions,
                    as: 'investorQuestions',
                    where: {
                        fundId: fundId,
                        deletedAt: null
                    },
                    required: false,
                }
            ],
        });

        fund = fund.toJSON();
 
        // get investor status
        let subscriptions = await models.FundSubscription.findAll({
            attributes: ['status', 'id', 'lpId','investorType','entityName','trustName','organizationName','invitedDate','isInvestorInvited'],
            where: {
                fundId: fundId,
                status:{
                    //Declined, Rescind fund, Not Interested
                    [Op.notIn]:[11,6,3]
                }
            },
            include:[{
                model: models.OfflineUsers,
                as: 'offlineLp',
                required: false,
                paranoid:true
            },{
                model: models.User,
                as: 'lp',
                required: false,
                include:[{
                    model: models.Account,
                    as:'accountDetails',
                    required:true
                }]                 
            },{
                model: models.FundStatus,
                as:'subscriptionStatus',
                required:true
            },{
                attributes: ['timezone'],
                model: models.Fund,
                as: 'fund',
                required: false,
                include: [{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }]
            }]
        });

        const lps = await _getVCFirmLps(fund.vcfirmId, subscriptions,fundId, offset); 

        return res.json({
            data: { 
                lps: lps 
            }
        });
    } catch (e) {
        next(e)
    }

}


module.exports = {
    fundStore,
    deleteFund,
    removeFundGPDelegate,
    removeFundLpDelegate,
    fundDocumentUpload,
    getFundById,
    removeFundFile,
    publishFund,
    removeLp,
    deactivate,
    sendRemainderToLp,
    getStatusOfSubscriptionForm,
    removeClosedLp,
    additionalSignatoryPages,
    removeAdditionalSignatoryPages,
    changeFundGP,
    convertGPToSignatory,
    changeGPToFundManager,
    getSubscriptionDetails,
    getFirmLPs
}

