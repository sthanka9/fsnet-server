const models = require('../models/index');
const errorHelper = require('../helpers/errorHelper');
const messageHelper = require('../helpers/messageHelper');
const config = require('../config/index');
const emailHelper = require('../helpers/emailHelper');
const eventHelper = require('../helpers/eventHelper');
const commonHelper = require('../helpers/commonHelper');
const logger = require('../helpers/logger');
const { Op } = require('sequelize');
const moment = require('moment');

module.exports.resendEmail = async (req, res, next) => {
    try {
        logger.info('inside resendEmail Method');
        const { id, accountType, fundId, investorId = null } = req.body;

        let data;
        let typeOfEvent = ''

        const user = await models.User.findOne({
            where: {
                id: id
            },
            include: [{
                model: models.Account,
                as: 'accountDetails'
            }]
        });

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                attributes: ['email', 'id', 'firstName', 'lastName', 'middleName'],
                model: models.User,
                as: 'gp'
            }]
        });

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }
      

        if (!user) {
            return errorHelper.error_400(res, 'No user', messageHelper.userNotFound);
        }

        //If user is already registered, send fund invitation link
        const loginOrRegisterLink = user.accountDetails.isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${user.accountDetails.emailConfirmCode}`;
        const content = user.accountDetails.isEmailConfirmed ? "to login and get going." : "to setup your account and get going."

        //GPDelegate, LP, LPDelegate,SecondaryGP
        logger.info("Initialize send email method");
        if (accountType == 'GP') {
            typeOfEvent = 'Invite Re-sent to GP'
            if(user.accountDetails.isEmailConfirmed){ // if user is already registered
                emailHelper.sendEmail({
                    toAddress: user.accountDetails.email,
                    subject: messageHelper.welcomeEmailSubject,
                    data: {
                        name: commonHelper.getFullName(user),
                        clientURL: config.get('clientURL'),
                        email: user.accountDetails.email,
                        fundLegalEntityName: fund.legalEntity,
                        clientURL: `${config.get('clientURL')}`                         
                    },
                    htmlPath: "gpFundDetails.html"
                }) 

            } else { // if account not at registered
            emailHelper.sendEmail({
                toAddress: user.accountDetails.email,
                subject: messageHelper.welcomeEmailSubject,
                data: {
                    name: commonHelper.getFullName(user),
                    clientURL: config.get('clientURL'),
                    email: user.accountDetails.email,
                    link: `${config.get('clientURL')}/user/setPassword?code=${user.accountDetails.emailConfirmCode}&id=${user.accountDetails.id}`
                },
                htmlPath: "gpAccountDetails.html"
            }) 

            }

        } else if (accountType == 'SecondaryGP') {
            typeOfEvent = 'Invite Re-sent to GPSignatory'
            subject = messageHelper.assignLpDelegateEmailSubject
            data = {
                name: commonHelper.getFullName(user),
                fundName: fund.legalEntity,
                fundCommonName: fund.fundCommonName,
                gpName: commonHelper.getFullName(fund.gp),
                lpName: commonHelper.getFullName(user),
                loginOrRegisterLink: loginOrRegisterLink,
                content
            };

            htmlPath = 'fundGPSignatoryInvitation.html';
            triggerNotificationsToUser(user, htmlPath, data)

        } else if (accountType == 'GPDelegate') {
            typeOfEvent = 'Invite Re-sent to GPDelegate'
            subject = messageHelper.assignLpDelegateEmailSubject
            data = {
                name: commonHelper.getFullName(user),
                fundName: fund.legalEntity,
                gpName: commonHelper.getFullName(req.user),
                loginOrRegisterLink: loginOrRegisterLink,
                content
            };

            htmlPath = 'fundGPInviation.html';
            triggerNotificationsToUser(user, htmlPath, data)

        } else if (accountType == 'LP') {
            typeOfEvent = 'Invite Re-sent to Investor'
            //update fund status to open-ready
            await models.Fund.update({ statusId: 13 }, { where: { id:fundId } })
            //subscription update
            await models.FundSubscription.update({
                status: 1,
                updatedBy: req.userId,
                invitedDate: moment().format('MM-DD-YYYY'),
                isInvestorInvited: 2
            }, {
                    where: {
                        fundId,
                        lpId:id                        
                    }
            })                        

            subject = messageHelper.addLPEmailSubject
            data = {
                name: commonHelper.getFullName(user),
                loginOrRegisterLink: loginOrRegisterLink,
                fundCommonName: fund.fundCommonName,
                content
            };

            htmlPath = 'fundLPInviation.html';
            triggerNotificationsToUser(user, htmlPath, data)

        } else if (accountType == 'LPDelegate') {
            typeOfEvent = 'Invite Re-sent to LPDelegate'
            if (investorId) {
                const investor = await models.User.findOne({
                    where: {
                        id: investorId
                    }
                });
                subject = messageHelper.assignLpDelegateEmailSubject
                data = {
                    name: commonHelper.getFullName(user),
                    fundName: fund.legalEntity,
                    lpName: commonHelper.getFullName(investor),
                    loginOrRegisterLink: loginOrRegisterLink,
                    content
                };
                htmlPath = 'fundLpDelegateInviation.html';
                triggerNotificationsToUser(user, htmlPath, data)
            } else {
                return errorHelper.error_400(res, 'No Investor id', messageHelper.lpNotFound)
            }
        }

        eventHelper.saveEventLogInformation(typeOfEvent,fundId,null,req,id);

        logger.info("Save record in History table");
        await models.History.create({
            accountType: user.accountType,
            userId: user.id,
            fundId: fundId,
            action: 'Resend Email',
            updatedBy: req.userId,
            createdBy: req.userId
        });

        return res.send('Success')
    } catch (e) {

        next(e);

    }
}

module.exports.getActionsHistory = async (req,res,next)=>{

    try{
        const {fundId,userId,accountType} = req.body;

        const histories = await models.History.findAll({
            where:{
                fundId,
                userId,
                accountType
            },
            include:[{
                model: models.User,
                as:'actionsByUser'
            }]
        }); 

        const fund = await models.Fund.findOne({
            include: [{
                model: models.timezone,
                as: 'timeZone',
                required: false
            }],
            where: {
                id: fundId
            }
        })

        let data = [];

        histories.map(history=>{
            data.push({
                action: history.action,
                createdAt: commonHelper.getDateTimeWithTimezone(fund.timeZone, history.createdAt, 1, 0, 1),
                updatedAt:  commonHelper.getDateTimeWithTimezone(fund.timeZone, history.updatedAt, 1, 0, 1),
                firstName : history.actionsByUser.firstName,
                lastName: history.actionsByUser.lastName,
                middleName: history.actionsByUser.middleName,
                profile: history.actionsByUser.profilePic,
                organizationName: history.actionsByUser.organizationName,
                accountType: history.actionsByUser.accountType,
                userOtherDetails: history.actionsByUser,
                timeZone: fund.timeZone && fund.timeZone.displayName ? fund.timeZone.displayName : ''
            })
        })
        return res.json(data);

    }catch(e){

        next(e);

    }

}

module.exports.editGPDelOrSign = async (req, res, next) => {
    try {
        const { fundId, userId, firstName, middleName, lastName, organizationName, email, accountType } = req.body;

        const loggedInUserEmail = req.user.email;
        const checkIfGPDelegateExists = await models.User.findOne({
            where: {
                id: {
                    [Op.ne]: userId
                },
                accountType,
                email
            }
        });

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                attributes: ['email', 'id', 'firstName', 'lastName', 'middleName','accountType'],
                model: models.User,
                as: 'gp'
            }]
        });

        if (!fund) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        if (checkIfGPDelegateExists) {
            return errorHelper.error_400(res, 'email', messageHelper.emailExists);
        }

        const user = await models.User.findOne({
            where: {
                id: userId
            }
        });

        let accountId = user.accountId;
        let accountData = {
            firstName,
            lastName,
            middleName,
            organizationName,
            email
        };

        //update profile to all users
        await models.User.update(accountData, { where: { accountId: accountId } });
        //update profile to account
        await models.Account.update(accountData, { where: { id: accountId } });

        const oldLpName = commonHelper.getFullName({ firstName: user.firstName, lastName: user.lastName, middleName: user.middleName });
        const newLpName = commonHelper.getFullName({ firstName, lastName, middleName });
        const gpName = commonHelper.getFullName(fund.gp);

        if (user.firstName != firstName || user.lastName != lastName) {
            triggerOnNameUpdate(fund.gp.email, gpName, oldLpName, newLpName,email, fund,accountType)
        }

        // email to LP
        if (user.email != email) {
            triggerOnEmailUpdate(email, gpName, fund, oldLpName, newLpName, fund.gp.email,accountType)
        }


        return res.json({
            data: {
                firstName,
                lastName,
                middleName,
                organizationName,
                email,
                fundId, 
                id:userId, 
                accountType
            }
        })         

    } catch (e) {
        next(e)
    }
}

function triggerNotificationsToUser(user,htmlPath,data){
    emailHelper.sendEmail({
        toAddress: user.email,
        subject: messageHelper.assignGpDelegateEmailSubject,
        data: data,
        htmlPath: htmlPath
    }).catch(error => {
        commonHelper.errorHandle(error)
    });
}

async function triggerOnNameUpdate(gpEmail,gpName,  oldLpName, newLpName,newEmail, fund,accountType) {
    let updatedUserAccountType = accountType == 'GPDelegate' ? 'Delegate' : (accountType == 'SecondaryGP' ? 'Signatory' :'')

    emailHelper.sendEmail({
        toAddress: gpEmail,
        subject: `Email Notification - Vanilla`,
        data: {
            oldLpName,
            newLpName,
            gpName: gpName,
            fundName: fund.legalEntity ? fund.legalEntity : ' ',
            updatedUserAccountType,
            user:fund.gp
        },
        htmlPath: "nameChangeToGP.html"
    }).catch(error => {
        commonHelper.errorHandle(error)
    });

    let htmlPath = accountType == 'GPDelegate' ? 'nameChangeToGPDel.html' : (accountType == 'SecondaryGP' ? 'nameChangeToGPSign.html' :'')

    emailHelper.sendEmail({
        toAddress: newEmail,
        subject: `Email Notification - Vanilla`,
        data: {
            oldLpName,
            newLpName,
            fundName: fund.legalEntity
        },
        htmlPath: htmlPath
    }).catch(error => {
        commonHelper.errorHandle(error)
    })
}

async function triggerOnEmailUpdate(email, gpName, fund, oldLpName, newLpName, gpEmail,accountType) {

    emailHelper.sendEmail({
        toAddress: gpEmail,
        subject: `Email Notification - Vanilla`,
        data: {
            oldLpName: oldLpName,
            newLpName: newLpName,
            gpName: gpName,
            fundName: fund.legalEntity,
            email: email,
            user:fund.gp
        },
        htmlPath: "emailChangeToGP.html"
    }).catch(error => {
        commonHelper.errorHandle(error)
    })

    let htmlPath = accountType == 'GPDelegate' ? 'emailChangeToGPDel.html' : (accountType == 'SecondaryGP' ? 'emailChangeToGPSign.html' :'')
    
    emailHelper.sendEmail({
        toAddress: email,
        subject: `Email Notification - Vanilla`,
        data: {
            oldLpName: oldLpName,
            newLpName: newLpName,
            fundName: fund.legalEntity,
            email: email
        },
        htmlPath: htmlPath
    }).catch(error => {
        commonHelper.errorHandle(error)
    })
}

module.exports.copyLink = async (req, res, next) => {
    try {
        logger.info("inside copy link method");
        const {fundId, id } = req.body;
    
        
        const user = await models.User.findOne({
            where: {
                id:id
            },
            include: [{
                model: models.Account,
                as: 'accountDetails'
            }]
        });

        if (!user) {
            return errorHelper.error_400(res, 'fundId', messageHelper.emailNotFound);
        } 

        let accountType = user.accountType

        typeOfEvent = (accountType == 'SecondaryGP') ?  'Invite Link Copied for GPSignatory Invite' :  (accountType == 'GPDelegate') ?  'Invite Link Copied for GPDelegate Invite' :     (accountType == 'LP')  ? 'Invite Link Copied for Investor Invite' :  (accountType == 'LPDelegate')  ?  'Invite Link Copied for LPDelegate Invite' : ''       

      
        if(user.accountDetails.isEmailConfirmed){
            return res.send(messageHelper.alreadyRegistered)
        }
        const loginOrRegisterLink =  `${config.get('clientURL')}/register/${user.accountDetails.emailConfirmCode}`;

        logger.info("save record in history table ");
        await models.History.create({
            accountType: user.accountType,
            userId: user.id,
            fundId: fundId,
            action: 'Copy Link',
            updatedBy: req.userId,
            createdBy: req.userId
        });

        eventHelper.saveEventLogInformation(typeOfEvent,fundId,null,req,id);

        return res.send(loginOrRegisterLink);
        

    } catch (e) {
        next(e);
    }
}


