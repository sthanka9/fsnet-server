const models = require('../models/index');
const _ = require('lodash');
const messageHelper = require('../helpers/messageHelper');
const errorHelper = require('../helpers/errorHelper');
const commonHelper = require('../helpers/commonHelper');
const logger = require('../helpers/logger');
const { Op, literal } = require('sequelize');
const path = require('path');
const config = require('../config/index');
const uuidv1 = require('uuid/v1');
const scissors = require('scissors');
const moment = require('moment');
const emailHelper = require('../helpers/emailHelper');
const effectuateHelper = require('../helpers/effectuateHelper');
const util = require('util');
const swapFundAmendmentsHelper = require('../helpers/swapFundAmendmentsHelper');
const exec = util.promisify(require('child_process').exec);


module.exports.mapAllSubscriptionsRelatedToFund = async(req,res,next)=>{
    try{

        //Save userId also in FundCron table, save entire req.file object in filepath, amendmentId also
        const withoutInvestorConsentUpload = await models.FundCron.findOne({
            where:{
                status:0
            }
        });


        console.log("+++++++++++++++++++++")
        if(withoutInvestorConsentUpload){

            console.log("=========CRON 1 PROCESS STARTED=========");

            if(withoutInvestorConsentUpload.uploadType == 'AMENDMENT_AGREEMENT'){
                models.FundAmmendment.findOne({
                    where:{
                        fundId: withoutInvestorConsentUpload.fundId,
                        id: withoutInvestorConsentUpload.amendmentId,
                        isAmmendment:1,
                        isAffectuated:1,
                        isActive:1,
                        isArchived:0
                    },
                    raw:true
                }).then(async data=>{

    
                    //Archive existing doc
                    delete data.id
                    data.isActive =0
                    data.createdBy = req.userId
                    data.updatedBy = req.userId
                    data.isArchived = 1
                    
                    await models.FundAmmendment.create(data);
    
                    //Update with the new file path 
                    await models.FundAmmendment.update({
                        document: JSON.stringify(withoutInvestorConsentUpload.uploadedFilePath)
                    }, {
                        where: {
                            fundId: withoutInvestorConsentUpload.fundId,
                            id: withoutInvestorConsentUpload.amendmentId,
                            isAmmendment:true,
                            isAffectuated:true,
                            isActive:1,
                            isArchived:0
                        }
                    });
    
                });
            }


            let fundId = withoutInvestorConsentUpload.fundId;
    
            let fund = await getSubscriptionsRelatedToFund(fundId);
    
            let subscriptionIds = _.map(fund.fundInvestor,'id');


            let docTypes = withoutInvestorConsentUpload.uploadType == 'FUND_AGREEMENT'
                ? ['FUND_AGREEMENT','CONSOLIDATED_FUND_AGREEMENT']
                    :  ['AMENDMENT_AGREEMENT','CONSOLIDATED_AMENDMENT_AGREEMENT']
            
            //Documents where investors are signed
            documents = await models.DocumentsForSignature.findAll({
                where:{
                    fundId,
                    subscriptionId:{
                        [Op.or]:[{[Op.in]:subscriptionIds},null]
                        
                    },
                    docType: docTypes,
                    deletedAt:null,
                    isActive:1,
                    isArchived:0,
                    amendmentId: withoutInvestorConsentUpload.amendmentId,
                    [Op.or]:[{
                        isPrimaryLpSigned:1
                    },{
                        lpSignCount:{
                            [Op.gt]:0
                        }
                    }]
                },
                include:[{
                    model: models.FundAmmendment,
                    as:'dfsamendments'
                }]
            });
      
            //Documents where no investors are signed
            documentsWhereNoInvestorSigned = await models.DocumentsForSignature.findAll({
                where:{
                    fundId,
                    subscriptionId:{
                        [Op.in]:subscriptionIds
                    },
                    docType: withoutInvestorConsentUpload.uploadType,
                    deletedAt:null,
                    isActive:1,
                    isArchived:0,
                    amendmentId: withoutInvestorConsentUpload.amendmentId,
                    isPrimaryLpSigned:0,
                    lpSignCount:0
                },
                include:[{
                    model: models.FundAmmendment,
                    as:'dfsamendments'
                }]
            });

            if(documents.length){
    
                documents.forEach(document=>{
    
                    models.SubscriptionCron.findOrCreate({
                        where:{
                            subscriptionId: document.subscriptionId ? document.subscriptionId:null,
                            fundId,
                            docType: document.docType,
                            fundCronId: withoutInvestorConsentUpload.id,
                            amendmentId: document.amendmentId
                        },
                        defaults:{
                            subscriptionId: document.subscriptionId ? document.subscriptionId:null,
                            fundId,
                            originalPartnershipDocument: withoutInvestorConsentUpload.uploadType == 'FUND_AGREEMENT' ?JSON.stringify(fund.partnershipDocument): JSON.stringify(document.dfsamendments.document),
                            partnershipDocumentPageCount : withoutInvestorConsentUpload.uploadType == 'FUND_AGREEMENT'?document.originalPartnershipDocumentPageCount: null,
                            uploadedFilePath: JSON.stringify(withoutInvestorConsentUpload.uploadedFilePath),
                            partnershipDocumentToSwap: document.filePath,
                            swappedPartnershipAgreement: null,
                            docType: document.docType,
                            userId: withoutInvestorConsentUpload.userId,
                            amendmentId: document.amendmentId,
                            fundCronId: withoutInvestorConsentUpload.id,
                            status:0,
                            count:0
                        }
                    }).spread((existing, created)=>{
                        
                        if(!created){

                            models.SubscriptionCron.update({
                                count: literal('count + 1')
                            },{
                                where:{
                                    subscriptionId: document.subscriptionId ? document.subscriptionId:null,
                                    fundId,
                                    docType: document.docType,
                                    fundCronId: withoutInvestorConsentUpload.id,
                                    amendmentId: document.amendmentId
                                }
                            });
                        }
                    })

                });
            }

            if(documentsWhereNoInvestorSigned.length){
    
                documentsWhereNoInvestorSigned.forEach(document=>{

                    models.SubscriptionCron.findOrCreate({
                        where:{
                            subscriptionId: document.subscriptionId ? document.subscriptionId:null,
                            fundId,
                            docType: document.docType,
                            fundCronId: withoutInvestorConsentUpload.id,
                            amendmentId: document.amendmentId
                        },
                        defaults:{
                            subscriptionId: document.subscriptionId ? document.subscriptionId:null,
                            fundId,
                            originalPartnershipDocument: withoutInvestorConsentUpload.uploadType == 'FUND_AGREEMENT' ?JSON.stringify(fund.partnershipDocument): JSON.stringify(document.dfsamendments.document),
                            uploadedFilePath: JSON.stringify(withoutInvestorConsentUpload.uploadedFilePath),
                            partnershipDocumentToReplace: withoutInvestorConsentUpload.uploadedFilePath.path,
                            partnershipDocumentToSwap: document.filePath,
                            swappedPartnershipAgreement: null,
                            docType: document.docType,
                            userId: withoutInvestorConsentUpload.userId,
                            amendmentId: document.amendmentId,
                            fundCronId: withoutInvestorConsentUpload.id,
                            status: 0,
                            count:0
                        }
                 
                    }).spread((existing, created)=>{
                        if(!created){
                            models.SubscriptionCron.update({
                                count: literal('count + 1')
                            },{
                                where:{
                                    subscriptionId: document.subscriptionId ? document.subscriptionId:null,
                                    fundId,
                                    docType: document.docType,
                                    fundCronId: withoutInvestorConsentUpload.id,
                                    amendmentId: document.amendmentId
                                }
                            })
                        }
                    })

                });
            }

            //If investor is in invited status
            if(!documents.length && !documentsWhereNoInvestorSigned.length){
                const partnershipDocument =  JSON.stringify(withoutInvestorConsentUpload.uploadedFilePath);
                const partnershipDocumentPageCount = await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(withoutInvestorConsentUpload.uploadedFilePath.path);
                
                models.Fund.update({ 
                    isFundLocked: false,
                    partnershipDocument: partnershipDocument,
                    partnershipDocumentPageCount: partnershipDocumentPageCount
                }, {
                    where: {
                        id:fundId
                    }
                });

                let user = await models.User.findOne({
                    where:{
                        id:fund.gpId
                    }
                });


                await models.FundCron.update({
                    status:2
                },{
                    where:{
                        id: withoutInvestorConsentUpload.id
                    }
                });

            }else{
                await models.FundCron.update({
                    status:1
                },{
                    where:{
                        id: withoutInvestorConsentUpload.id
                    }
                });
            }
        }

     
        console.log("====SUCCESS CRON 1====")

    }catch(e){
        next(e)
    }
}


module.exports.swapAllDocuments = async (req,res,next)=>{
    try{

        const countLimitExceeds = await models.SubscriptionCron.findAll({
            attributes:['subscriptionId'],
            where:{
                count:5
            }
        });

        if(countLimitExceeds.length){
            let subscriptionIds = _.compact(_.map(countLimitExceeds,'subscriptionId'));
            emailHelper.sendEmail({
                toAddress: ['lakshmia@menlo-technologies.com','sudarshank@menlo-technologies.com'],
                subject:'Count Limit has exceeded',
                data:{
                    subscriptionIds
                },
                htmlPath: 'cronCountLimit.html'
            });

        }else{

            const subscriptionsForFA = await models.SubscriptionCron.findAll({
                where:{
                    status:0,
                    docType:{
                        [Op.in]:['FUND_AGREEMENT','CONSOLIDATED_FUND_AGREEMENT']
                    }
                }
            });
            const subscriptionsForAmendment = await models.SubscriptionCron.findAll({
                where:{
                    status:0,
                    docType:{
                        [Op.in]:['AMENDMENT_AGREEMENT','CONSOLIDATED_AMENDMENT_AGREEMENT']
                    }
                }
            });
    
            if(subscriptionsForFA.length){
                let fundCronIds = _.uniq(_.map(subscriptionsForFA,'fundCronId'));
                let fundIds = _.uniq(_.map(subscriptionsForFA,'fundId'));
    
                let count = 0; 
               
                console.log("=========CRON 2 PROCESS STARTED=========");
                subscriptionsForFA.forEach(async subscription=>{
    
                    const partnershipDocumentPageCount =  await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(subscription.uploadedFilePath.path);
    
                    if(!subscription.partnershipDocumentToReplace){
    
                        let pdfFiles=[];
    
                        let filePathToSaveInDB = subscription.docType == 'FUND_AGREEMENT' ?  
                            `./assets/funds/${subscription.fundId}/${subscription.subscriptionId}/FUND_AGREEMENT_${uuidv1()}.pdf` :
                                `./assets/funds/${subscription.fundId}/CONSOLIDATED_FUND_AGREEMENT_${uuidv1()}.pdf`;
    
                        
                        const filePath = subscription.uploadedFilePath.path;
                        let uploadedFilePathToAppendSigns = path.join(__dirname,'.'+filePath);
                        
                        const outputFilePath = path.resolve(__dirname,'.'+filePathToSaveInDB);
    
                        let tempPathToSaveSigns = `../assets/temp/${uuidv1()}.pdf`;
                        tempPathToSaveSigns = path.join(__dirname,tempPathToSaveSigns);
        
    
                        await exec(`pdftk  ${subscription.partnershipDocumentToSwap} cat  ${subscription.partnershipDocumentPageCount+1}-end  output ${tempPathToSaveSigns}`)
                        pdfFiles.push(uploadedFilePathToAppendSigns,tempPathToSaveSigns);
    
                        if(pdfFiles.length ==2){
                            await commonHelper.pdfMerge(pdfFiles,outputFilePath);
                        }
    
    
                        models.DocumentsForSignature.findOne({
                            where:{
                                fundId: subscription.fundId,
                                subscriptionId: subscription.subscriptionId,
                                docType: subscription.docType,
                                isActive:1,
                                isArchived :0
                            },
                            raw:true
                        }).then(async data=>{
                
                            //Archive existing doc
                            data.previousDocumentId = data.id
                            delete data.id
                            data.isActive =0
                            data.createdBy = subscription.userId
                            data.updatedBy = subscription.userId
                            data.isArchived = 1
                            
                            await models.DocumentsForSignature.create(data);
        
                            //Update with the new file path 
                            await models.DocumentsForSignature.update({
                                filePath: filePathToSaveInDB,
                                originalPartnershipDocumentPageCount: partnershipDocumentPageCount
                            }, {
                                where: {
                                    fundId: subscription.fundId,
                                    subscriptionId: subscription.subscriptionId,
                                    docType: subscription.docType,
                                    isActive:1,
                                    isArchived :0
                                }
                            });
        
                            //Replica of existing record(Archival)
                            models.FundAmmendment.findOne({
                                where:{
                                    fundId: subscription.fundId,
                                    id: subscription.amendmentId,
                                    isAmmendment:false,
                                    isAffectuated:true,
                                    isActive:1,
                                    isArchived:0
                                },
                                raw:true
                            }).then(async data=>{
        
                                if(data){
                                    //Archive existing doc
                                    delete data.id
                                    data.isActive =0
                                    data.createdBy = subscription.userId
                                    data.updatedBy = subscription.userId
                                    data.isArchived = 1
                                    
                                    await models.FundAmmendment.create(data);
                
                                    //Update with the new file path 
                                    await models.FundAmmendment.update({
                                        document: JSON.stringify(subscription.uploadedFilePath)
                                    }, {
                                        where: {
                                            fundId: subscription.fundId,
                                            id: subscription.amendmentId,
                                            isAmmendment:false,
                                            isAffectuated:true,
                                            isActive:1,
                                            isArchived:0
                                        }
                                    });
                                }
        
                            });
        
                            models.SubscriptionCron.update({
                                status: 1,
                                count: literal('count + 1'),
                                swappedPartnershipAgreement: filePathToSaveInDB,
                                partnershipDocumentPageCount: partnershipDocumentPageCount
                            },{
                                where:{
                                    fundCronId : subscription.fundCronId,
                                    fundId: subscription.fundId,
                                    subscriptionId: subscription.subscriptionId,
                                    id:subscription.id
                                }
                            });
                        }); 
    
                        count = count+1;
    
                    }else{
    
                        console.log("=====Having Partnership doc====")
                        models.DocumentsForSignature.update({
                            filePath: subscription.partnershipDocumentToReplace,
                            originalPartnershipDocumentPageCount: partnershipDocumentPageCount
                        }, {
                            where: {
                                fundId: subscription.fundId,
                                subscriptionId: subscription.subscriptionId,
                                docType: subscription.docType,
                                isActive: 1,
                                isArchived:0,
                                isPrimaryLpSigned:0
                            }
                        });
    
                        models.SubscriptionCron.update({
                            status: 1,
                            count: literal('count + 1'),
                        },{
                            where:{
                                fundCronId : subscription.fundCronId,
                                fundId: subscription.fundId,
                                subscriptionId: subscription.subscriptionId,
                                id: subscription.id
                            }
                        });
    
                        count = count+1;
    
                    }
    
                    console.log("=====",subscriptionsForFA.length , count)
    
                    if(subscriptionsForFA.length == count){
                        models.FundCron.update({
                            status:2
                        },{
                            where:{
                                id: {
                                    [Op.in]:fundCronIds
                                },
                                status:1
                            }
                        });
        
                        fundIds.forEach(async fundId=>{
        
                            let subscriptiosToSwap = _.find(subscriptionsForFA,{fundId:fundId});
        
                            const partnershipDocument =  JSON.stringify(subscriptiosToSwap.uploadedFilePath);
                            const partnershipDocumentPageCount = await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(subscriptiosToSwap.uploadedFilePath.path);
        
                            let user = await models.User.findOne({
                                where:{
                                    id:subscriptiosToSwap.userId
                                }
                            });
                            
                            models.Fund.update({ 
                                isFundLocked: false,
                                partnershipDocument: partnershipDocument,
                                partnershipDocumentPageCount: partnershipDocumentPageCount
                            }, {
                                where: {
                                    id:fundId
                                }
                            });
    
                        });
        
                    }
                });
                
            }
    
            if(subscriptionsForAmendment.length){
                let fundCronIds = _.uniq(_.map(subscriptionsForAmendment,'fundCronId'));
                let fundIds = _.uniq(_.map(subscriptionsForAmendment,'fundId'));
    
                let count = 0; 
               
                console.log("=========CRON 2 PROCESS STARTED=========");
                subscriptionsForAmendment.forEach(async subscription=>{
    
    
                    if(!subscription.partnershipDocumentToReplace){
    
                        let pdfFiles=[];
    
                        let filePathToSaveInDB = subscription.docType == 'AMENDMENT_AGREEMENT' ?
                                `./assets/funds/${subscription.fundId}/ammendments/AMENDMENT_AGREEMENT_${uuidv1()}.pdf` :
                                    `./assets/funds/${subscription.fundId}/ammendments/CONSOLIDATED_AMENDMENT_AGREEMENT_${uuidv1()}.pdf` 
    
                        
                        const filePath = subscription.uploadedFilePath.path;
                        let uploadedFilePathToAppendSigns = path.join(__dirname,'.'+filePath);
                        
                        const outputFilePath = path.resolve(__dirname,'.'+filePathToSaveInDB);
    
                        let tempPathToSaveSigns = `../assets/temp/${uuidv1()}.pdf`;
                        tempPathToSaveSigns = path.join(__dirname,tempPathToSaveSigns);
        
                        const ammendmentPath = path.join(__dirname.replace('controllers',''),subscription.originalPartnershipDocument.path);
                        const existingAmmendmentPageCount = await scissors(ammendmentPath).getNumPages();
    
                        await exec(`pdftk  ${subscription.partnershipDocumentToSwap} cat  ${existingAmmendmentPageCount+1}-end  output ${tempPathToSaveSigns}`)
                        pdfFiles.push(uploadedFilePathToAppendSigns,tempPathToSaveSigns);
    
                        if(pdfFiles.length ==2){
                            await commonHelper.pdfMerge(pdfFiles,outputFilePath);
                        }
    
    
                        models.DocumentsForSignature.findOne({
                            where:{
                                fundId: subscription.fundId,
                                subscriptionId: subscription.subscriptionId,
                                docType: subscription.docType,
                                amendmentId: subscription.amendmentId,
                                isActive:1,
                                isArchived :0
                            },
                            raw:true
                        }).then(async data=>{
        
                            if(data){
    
                                //Archive existing doc
                                data.previousDocumentId = data.id
                                delete data.id
                                // data.deletedAt = moment().format('YYYY-MM-DD HH:mm:ss.ssss Z')
                                data.isActive =0
                                data.createdBy = subscription.userId
                                data.updatedBy = subscription.userId
                                data.isArchived = 1
                                
                                await models.DocumentsForSignature.create(data);
                                
                                //Update with the new file path 
                                await models.DocumentsForSignature.update({
                                    filePath: filePathToSaveInDB
                                }, {
                                    where: {
                                        fundId: subscription.fundId,
                                        subscriptionId: subscription.subscriptionId,
                                        docType: subscription.docType,
                                        isActive:1,
                                        deletedAt:null,
                                        isArchived :0,
                                        amendmentId: subscription.amendmentId,
                                    }
                                });
                            }
                        
                            models.SubscriptionCron.update({
                                status: 1,
                                count: literal('count + 1'),
                                swappedPartnershipAgreement: filePathToSaveInDB,
                            },{
                                where:{
                                    fundCronId : subscription.fundCronId,
                                    fundId: subscription.fundId,
                                    subscriptionId: subscription.subscriptionId,
                                    id:subscription.id
                                }
                            });
                        }); 
    
                        count = count+1;
    
                    }else{
    
    
                        models.DocumentsForSignature.update({
                            filePath: subscription.partnershipDocumentToReplace
                        }, {
                            where: {
                                fundId: subscription.fundId,
                                subscriptionId: subscription.subscriptionId,
                                docType: subscription.docType,
                                deletedAt: null,
                                isActive: 1,
                                isArchived:0,
                                isPrimaryLpSigned:0
                            }
                        });
    
                        models.SubscriptionCron.update({
                            status: 1,
                            count: literal('count + 1'),
                        },{
                            where:{
                                fundCronId : subscription.fundCronId,
                                fundId: subscription.fundId,
                                subscriptionId: subscription.subscriptionId,
                                id:subscription.id
                            }
                        });
    
                        count = count+1;
    
                    }
    
                    console.log("=====",subscriptionsForAmendment.length , count)
    
                    if(subscriptionsForAmendment.length == count){
                        models.FundCron.update({
                            status:2
                        },{
                            where:{
                                id: {
                                    [Op.in]:fundCronIds
                                },
                                status:1
                            }
                        });
        
                        fundIds.forEach(async fundId=>{
        
                            let subscriptiosToSwap = _.find(subscriptionsForAmendment,{fundId:fundId});
    
                            models.Fund.update({ 
                                isFundLocked: false
                            }, {
                                where: {
                                    id:fundId
                                }
                            });
        
                            let user = await models.User.findOne({
                                where:{
                                    id:subscriptiosToSwap.userId
                                }
                            });
    
                        });
        
                    }
                });
    
            }
            
        }

        console.log("====SUCCESS CRON 2====")
    

    }catch(e){
        logger.log('error','********Error while swapping out in Cron 2***************',e)
        emailHelper.sendEmail({
            toAddress: ['lakshmia@menlo-technologies.com','sudarshank@menlo-technologies.com'],
            subject:'Error while swapping out the pages in Cron 2',
            htmlPath: 'cronSwappingError.html'
        });
        next(e);
    }
}


async function getSubscriptionsRelatedToFund(fundId){
    return await models.Fund.findOne({
        where: {
            id: fundId
        },
        include: [{
            attributes: ['status','id','lpId'],
            model: models.FundSubscription,
            as: 'fundInvestor',
            required: true,
            where:{
                deletedAt: null
            }
        }]
    });
}