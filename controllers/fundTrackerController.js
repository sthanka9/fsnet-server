
const models = require('../models/index');
const { Op } = require('sequelize')
const _ = require('lodash')
const excel = require("exceljs");
const moment = require('moment')
const config = require("../config/index");
const uuidv1 = require('uuid/v1');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const Promise = require('bluebird');
const errorHelper = require('../helpers/errorHelper');

const getFundById = async (req, res, next) => {

    try {
        const { fundId } = req.params

        if (!Number(fundId)) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundIdValidate);
        }

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            },
            include: [{
                model: models.FundStatus,
                as: 'fundStatus',
                required: true,
            }, {
                model: models.FundSubscription,
                as: 'fundInvestor',
                required: false,
                where: {
                    deletedAt: null
                },
                include: [{
                    model: models.FundClosings,
                    as: 'fundClosingInfo',
                    required: false,
                    where: {
                        isActive: 1
                    }
                }]
            },
            {
                model: models.GpDelegates,
                attributes: ['delegateId', 'gPDelegateRequiredDocuSignBehalfGP', 'gPDelegateRequiredConsentHoldClosing'],
                as: 'gpDelegatesSelected',
                required: false,
                where: {
                    fundId: fundId
                }
            }],
        })

        if (!fund) {
            return res.status(404).json({
                "errors": [
                    {
                        "msg": messageHelper.fundNotFound
                    }
                ]
            });
        }


        const lpsSelected = _.map(fund.lpsSelected, 'lpId');



        let gpDelegates = await models.VcFrimDelegate.findAll({
            where: {
                vcfirmId: fund.vcfirmId
            },
            include: [{
                model: models.User,
                as: 'details',
                required: true
            }]
        })

        const fundSubscription = await models.FundSubscription.findOne({
            where: {
                fundId
            }
        })

        let lps = await models.VcFrimLp.findAll({
            where: {
                vcfirmId: fund.vcfirmId
            },
            include: [{
                model: models.User,
                as: 'details',
                required: true
            }]
        })

        lps = lps.map(lp => {

            return {
                id: lp.details.id,
                firstName: lp.details.firstName,
                lastName: lp.details.lastName,
                middleName: lp.details.middleName,
                email: lp.details.email,
                profilePic: lp.details.profilePic,
                organizationName: fundSubscription ? fundSubscription.organizationName : lp.details.organizationName,
                selected: lpsSelected.includes(lp.details.id)
            };

        })



        gpDelegates = gpDelegates.map(delegate => {

            const selected = _.find(fund.gpDelegatesSelected, { delegateId: delegate.delegateId })

            return {
                id: delegate.details.id,
                firstName: delegate.details.firstName,
                lastName: delegate.details.lastName,
                middleName: delegate.details.middleName,
                email: delegate.details.email,
                profilePic: delegate.details.profilePic,
                selected: selected ? true : false,
                gPDelegateRequiredDocuSignBehalfGP: _.get(selected, 'gPDelegateRequiredDocuSignBehalfGP', null),
                gPDelegateRequiredConsentHoldClosing: _.get(selected, 'gPDelegateRequiredConsentHoldClosing', null)
            };

        })

        const inProgress = _.filter(fund.fundInvestor, { 'status': 2 });
        const waitingForSignature = _.filter(fund.fundInvestor, { 'status': 16 });
        const closeReady = _.filter(fund.fundInvestor, { 'status': 7 });
        const closed = _.filter(fund.fundInvestor, { 'status': 10 });

        const sumOfCapitalCommitmentForInProgress = (_.sumBy(inProgress, 'lpCapitalCommitment') || 0) + (_.sumBy(waitingForSignature, 'lpCapitalCommitment') || 0);
        const sumOfCapitalCommitmentForCloseReady = _.sumBy(closeReady, 'lpCapitalCommitment') || 0;
        const sumOfCapitalCommitmentForClosed = _.sumBy(closed, function (o) { return (o.fundClosingInfo && _.last(o.fundClosingInfo)) ? _.last(o.fundClosingInfo).gpConsumed : 0; }) || 0;

        let fundHardCap30PercentAdded = fund.fundHardCap * 1.3
        let fundHardCapPercent = 0;
        let fundTargetCommitmentPercent = 0;

        if (fundHardCap30PercentAdded > 0) {
            while (sumOfCapitalCommitmentForInProgress + sumOfCapitalCommitmentForCloseReady + sumOfCapitalCommitmentForClosed > fundHardCap30PercentAdded) {
                fundHardCap30PercentAdded = fundHardCap30PercentAdded * 1.3
            }

            fundHardCapPercent = (fund.fundHardCap / fundHardCap30PercentAdded) * 100
            fundTargetCommitmentPercent = (fund.fundTargetCommitment / fundHardCap30PercentAdded) * 100
        }

        return res.json({
            data: {
                fundHardCapPercent: fundHardCapPercent,
                fundTargetCommitmentPercent: fundTargetCommitmentPercent,
                id: fund.id,
                vcfirmId: fund.vcfirmId,
                gpId: fund.gpId,
                legalEntity: fund.legalEntity,
                fundCommonName: fund.fundCommonName,
                fundManagerTitle: fund.fundManagerTitle,
                hardCapApplicationRule: fund.hardCapApplicationRule,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundHardCap: fund.fundHardCap,
                generalPartnersCapitalCommitmentindicated: fund.generalPartnersCapitalCommitmentindicated,
                fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                percentageOfLPCommitment: fund.percentageOfLPCommitment,
                percentageOfLPAndGPAggregateCommitment: fund.percentageOfLPAndGPAggregateCommitment,
                fundTargetCommitment: fund.fundTargetCommitment,
                capitalCommitmentByFundManager: fund.capitalCommitmentByFundManager,
                fundImage: fund.fundImage,
                partnershipDocument: fund.partnershipDocument,
                statusId: fund.statusId,
                status: fund.fundStatus.name,
                lps: lps,
                gpDelegates: gpDelegates,
                inProgress: inProgress.length,
                closeReady: closeReady.length,
                closed: closed.length,
                inProgressPercent: (sumOfCapitalCommitmentForInProgress / fundHardCap30PercentAdded) * 100 || 0,
                closeReadyPercent: (sumOfCapitalCommitmentForCloseReady / fundHardCap30PercentAdded) * 100 || 0,
                closedPercent: (sumOfCapitalCommitmentForClosed / fundHardCap30PercentAdded) * 100 || 0,
                sumOfCapitalCommitmentForInProgress: sumOfCapitalCommitmentForInProgress,
                sumOfCapitalCommitmentForCloseReady: sumOfCapitalCommitmentForCloseReady,
                sumOfCapitalCommitmentForClosed: sumOfCapitalCommitmentForClosed
            }
        });
    } catch (e) {
        next(e)
    }

}

const fundTotals = async (req, res, next) => {

    try {

        var { fundId } = req.params
        const { status = 'closed' } = req.query

        var total = {};
        let whereFilter = {
            fundId: fundId
        }
        // Closed- 10,Close-Ready- 7,open ready draft - 2 (as 'In Progress'),Open - 1 (as 'Invited'), 
        // Not Participating -11,Rescind-6 (as 'Not Participating'),Not Interested-3 (as 'Not Participating')
        var statusFilter = [10];

        if (status == 'closed_and_close_ready') {
            statusFilter = [10, 7];
        } else if (status == 'declined') {
            statusFilter = [11, 6, 3];
        } else if (status == 'other') {
            statusFilter = [1, 2, 4];
        }


        if (status != 'all') {
            whereFilter = {
                fundId: fundId,
                status: {
                    [Op.in]: statusFilter
                }
            }
        }

        let fundInvestors = await models.FundSubscription.findAll({
            where:
                //status: 10, //Closed
                whereFilter
            ,
            include: [{
                attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
                model: models.FundClosings,
                required: false,
                as: 'fundClosingInfo',
                where: {
                    isActive: 1
                }
            }, {
                attributes: ['id', 'name', 'lpSideName'],
                model: models.FundStatus,
                as: 'subscriptionStatus',
                required: true
            },]
        })


        let totalCapitalCommitmentClosed = 0;
        for (let fundClosing of fundInvestors) {
            if (_.last(fundClosing.fundClosingInfo)) {
                totalCapitalCommitmentClosed += _.last(fundClosing.fundClosingInfo).gpConsumed;
            }
        }

        var fundInvestorsGroupByStatus = _.groupBy(fundInvestors, 'status');
        var fundInvestorsGroupByStatusKeys = Object.keys(fundInvestorsGroupByStatus);


        total = { "closedCommitment": 0, "closedReadyCommitment": 0, "inProgressCommitment": 0 }

        fundInvestorsGroupByStatusKeys.map(key => {
            if (key == 10) {
                total["closedCommitment"] = _.sumBy(fundInvestorsGroupByStatus[key], 'lpCapitalCommitment')
            } else if (key == 7) {
                total["closedReadyCommitment"] = _.sumBy(fundInvestorsGroupByStatus[key], 'lpCapitalCommitment')
            } else if (key == 2) {
                total["inProgressCommitment"] = _.sumBy(fundInvestorsGroupByStatus[key], 'lpCapitalCommitment')
            }
        })

        total["closedCommitment"] = totalCapitalCommitmentClosed
        total["totalCommitment"] = total["closedCommitment"] + total["closedReadyCommitment"] + total["inProgressCommitment"]
        return res.json({ total });

    } catch (e) {
        next(e)
    }
}

const leftSideSummary = async (req, res, next) => {

    try {

        const { fundId } = req.params

        let whereFilter = {
            fundId: fundId,
            deletedAt: null,
            status:{
                [Op.ne]:17
            }
        }

        let fundInvestors = await models.FundSubscription.findAll({
            where: whereFilter,
            include: [{
                attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
                model: models.FundClosings,
                required: false,
                as: 'fundClosingInfo',
                where: {
                    isActive: 1
                }
            }, {
                attributes: ['id', 'name', 'lpSideName'],
                model: models.FundStatus,
                as: 'subscriptionStatus',
                required: true
            }]
        });

        let fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        });

        var subscriptionStatuses = _.map(fundInvestors, 'status');

        const inProgress = _.filter(fundInvestors, fundInvestor=>{ 
            if(fundInvestor.status == 2 || fundInvestor.status == 16){
                return fundInvestor;
            }
        })
        const closeReady = _.filter(fundInvestors, { 'status': 7 })
        const closed = _.filter(fundInvestors, { 'status': 10 })

        let status = '';
        let total = {
            "closedCommitment": 0, "closedReadyCommitment": 0, "inProgressCommitment": 0,
            "hypotheticalInvestorCommitment": 0, "hypotheticalFundmanagerCommitment": 0,
            "hypotheticalAggregateCommitment": 0, "closedFundmanagerCommitment": 0,
            "closedAggregateCommitment": 0
        }

        if(closed.length>0){
            closed.forEach(closedInvetsor=>{
                total["closedCommitment"]=total["closedCommitment"]+(_.last(closedInvetsor.fundClosingInfo)? _.last(closedInvetsor.fundClosingInfo).gpConsumed:0);
            });
        }

        if (_.includes(subscriptionStatuses, 10) && fund.statusId !== 10) {
            // During the time on and after the first closing, but prior to the final closing
            status = 2
            total["inProgressCommitment"] = _.sumBy(inProgress, 'lpCapitalCommitment') || 0;
            total["closedReadyCommitment"] = _.sumBy(closeReady, 'lpCapitalCommitment') || 0;

            // GP Commitment Choices - check which choice is available
            // ($100m * (100/98)) - $100m.
            const gpCommitmentPercent = (fund.percentageOfLPAndGPAggregateCommitment !== '0') ? (100 - fund.percentageOfLPAndGPAggregateCommitment) : 0;

            const closedFMCommitmentValue = fund.percentageOfLPCommitment !== '0' ? ((fund.percentageOfLPCommitment / 100) * total["closedCommitment"]) :
                (fund.capitalCommitmentByFundManager !== 0.00 ? fund.capitalCommitmentByFundManager :
                    (fund.percentageOfLPAndGPAggregateCommitment !== '0' ? (total["closedCommitment"] * (100 / gpCommitmentPercent)) - total["closedCommitment"] : ''));

            total["hypotheticalInvestorCommitment"] = total["inProgressCommitment"] + total["closedReadyCommitment"] + total["closedCommitment"];


            total["closedFundmanagerCommitment"] = closedFMCommitmentValue;

            const hypotheticalFMCommitmentValue = fund.percentageOfLPCommitment !== '0' ? ((fund.percentageOfLPCommitment / 100) * total["hypotheticalInvestorCommitment"]) :
                (fund.capitalCommitmentByFundManager !== 0.00 ? fund.capitalCommitmentByFundManager :
                    (fund.percentageOfLPAndGPAggregateCommitment !== '0' ? (total["hypotheticalInvestorCommitment"] * (100 / gpCommitmentPercent)) - total["hypotheticalInvestorCommitment"] : ''));

            total["hypotheticalFundmanagerCommitment"] = hypotheticalFMCommitmentValue;

            total["closedAggregateCommitment"] = total["closedFundmanagerCommitment"] + total["closedCommitment"];
            total["hypotheticalAggregateCommitment"] = total["hypotheticalInvestorCommitment"] + total["hypotheticalFundmanagerCommitment"];

        } else if (_.includes(subscriptionStatuses, 7) || _.includes(subscriptionStatuses, 1) || _.includes(subscriptionStatuses, 2)) {
            // During time before the first closing 
            status = 1;
            total["inProgressCommitment"] = _.sumBy(inProgress, 'lpCapitalCommitment') || 0;
            total["closedReadyCommitment"] = _.sumBy(closeReady, 'lpCapitalCommitment') || 0;

            total["hypotheticalInvestorCommitment"] = total["inProgressCommitment"] + total["closedReadyCommitment"] + total["closedCommitment"];

            // GP Commitment Choices - check which choice is available            
            const gpCommitmentPercent = (fund.percentageOfLPAndGPAggregateCommitment !== '0') ? (100 - fund.percentageOfLPAndGPAggregateCommitment) : 0;

            const hypotheticalFMCommitmentValue = fund.percentageOfLPCommitment !== '0' ? ((fund.percentageOfLPCommitment / 100) * total["hypotheticalInvestorCommitment"]) :
                (fund.capitalCommitmentByFundManager !== 0.00 ? fund.capitalCommitmentByFundManager :
                    (fund.percentageOfLPAndGPAggregateCommitment !== '0' ? ((total["hypotheticalInvestorCommitment"] * (100 / gpCommitmentPercent)) - total["hypotheticalInvestorCommitment"]) : ''));


            total["hypotheticalFundmanagerCommitment"] = hypotheticalFMCommitmentValue;
            total["hypotheticalAggregateCommitment"] = total["hypotheticalInvestorCommitment"] + total["hypotheticalFundmanagerCommitment"];

        } else if (fund.statusId === 10) {
            // During the time on and after the final closing:
            status = 3;

            const gpCommitmentPercent = (fund.percentageOfLPAndGPAggregateCommitment !== '0') ? (100 - fund.percentageOfLPAndGPAggregateCommitment) : 0;

            const closedFMCommitmentValue = fund.percentageOfLPCommitment !== '0' ? ((fund.percentageOfLPCommitment / 100) * total["closedCommitment"]) :
                (fund.capitalCommitmentByFundManager !== 0.00 ? fund.capitalCommitmentByFundManager :
                    (fund.percentageOfLPAndGPAggregateCommitment !== '0' ? (total["closedCommitment"] * (100 / gpCommitmentPercent)) - total["closedCommitment"] : ''));

            total["closedFundmanagerCommitment"] = closedFMCommitmentValue;
            total["closedAggregateCommitment"] = total["closedFundmanagerCommitment"] + total["closedCommitment"];
        }

        return res.json({
            total,
            status
        });

    } catch (e) {
        next(e)
    }
}

const rightSideSummary = async (req, res, next) => {

    try {

        const { fundId } = req.params

        const { status = 'closed' } = req.query

        var statusFilter = [10];

        if (status == 'closed_and_close_ready') {
            statusFilter = [10, 7];
        } else if (status == 'declined') {
            statusFilter = [11, 6, 3];
        } else if (status == 'other') {
            statusFilter = [1, 2, 4,16];
        }

        let whereFilter = {
            fundId: fundId,
            status:{
                [Op.ne]:17
            },
            deletedAt: null
        }

        if (status != 'all') {
            whereFilter = {
                fundId: fundId,
                status: {
                    [Op.in]: statusFilter
                }
            }
        }


        let subscriptions = await models.FundSubscription.findAll({
            where: whereFilter,
            include:[{
                attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
                model: models.FundClosings,
                required: false,
                as: 'fundClosingInfo',
                where: {
                    isActive: 1
                }
            }]
        });

        //If atleast one fund has 'not' QualifiedPurchaser then return notQualified purchase
        //default true
        const getQualifiedPurchaseArray = _.map(subscriptions, 'areYouQualifiedPurchaser');
        const qualifiedPurchaser = _.includes(getQualifiedPurchaseArray,false)  ? '3(c)(1)' : '3(c)(7)';

        let totalCapitalClosedOrHypotheticalCommitment = 0;
        subscriptions.map(closedInvetsor=>{
            //If fund is closed then take gpconsumed amount
            if(closedInvetsor.status == 10){
                totalCapitalClosedOrHypotheticalCommitment = totalCapitalClosedOrHypotheticalCommitment + (_.last(closedInvetsor.fundClosingInfo) ? _.last(closedInvetsor.fundClosingInfo).gpConsumed :0);
            }else{
                totalCapitalClosedOrHypotheticalCommitment = totalCapitalClosedOrHypotheticalCommitment + closedInvetsor.lpCapitalCommitment
            }
        });

        // Total Investor Count
        let investorCountTotal = subscriptions.length;
        let lookThroughIssuesFlag;

        // erisa Participation %
        // const hypotheticalOrClosedCommitmentOffered = (status == 'closed') ? _.sumBy(_.filter(subscriptions, { 'status': 10 }), 'lpCapitalCommitment') : _.sumBy(allSubscriptions, 'lpCapitalCommitment');
      
        let resultYesAmount = [];
        let investorCountArray = [];

        for (let subscription of subscriptions) {

            let gpConsumed=0;
            if (_.last(subscription.fundClosingInfo)) {
                gpConsumed = _.last(subscription.fundClosingInfo).gpConsumed
            }

            //For ERISA % calculations
            if (!subscription.benefitPlanInvestor && (subscription.employeeBenefitPlan || subscription.planAsDefinedInSection4975e1)) {

                resultYesAmount.push(gpConsumed? gpConsumed: subscription.lpCapitalCommitment)
            } else {
                if (subscription.totalValueOfEquityInterests >= 25) {
                    resultYesAmount.push((gpConsumed? gpConsumed: subscription.lpCapitalCommitment) * (subscription.totalValueOfEquityInterests / 100));
                }
            }
           
            //Fund percentage
            let fundPercentage = totalCapitalClosedOrHypotheticalCommitment > 0 ?
                        parseFloat(((gpConsumed ? gpConsumed: subscription.lpCapitalCommitment) / totalCapitalClosedOrHypotheticalCommitment) * 100).toFixed(2) : 0;


            //Look through issues
            if(subscription.lookThrough !== null){
                //Look through issues only for Trust and Entity
                lookThroughIssuesFlag = (!subscription.lookThrough && subscription.investorType !== 'Individual') ? 'Yes'
                : (subscription.investorType !== 'Individual' ? await commonHelper.getLookThroughValues(subscription,getQualifiedPurchaseArray,fundPercentage): 'No' )
                
                //Default investorCount is 1
                const investorCount = (lookThroughIssuesFlag == 'Yes') ? subscription.numberOfDirectEquityOwners : (subscription.investorCount ?subscription.investorCount:1)
                investorCountArray.push( investorCount );
            }else{
                investorCountArray.push(subscription.investorCount ?subscription.investorCount:1);
            }
        }

        const erisaParticipationTotal = totalCapitalClosedOrHypotheticalCommitment > 0 ?
            (_.sum(resultYesAmount) / totalCapitalClosedOrHypotheticalCommitment) * 100 : 0;


        return res.json({
            total: {
                beneficalOwnerCount: _.sum(investorCountArray),
                investorCount: investorCountTotal,
                erisaParticipationCount: parseFloat(erisaParticipationTotal).toFixed(2),
                qualifiedPurchaser
            }
        })

    } catch (e) {
        next(e)
    }
}

const viewFund = async (req, res, next) => {

    try {

        const { fundId } = req.params

        const { status = 'closed', offset = 0, orderCol, order } = req.query

        // Closed- 10,Close-Ready- 7,open ready draft - 2 (as 'In Progress'),Open - 1 (as 'Invited'), 
        // Not Participating -11,Rescind-6 (as 'Not Participating'),Not Interested-3 (as 'Not Participating')
        let sortStatusOrder = [10, 7, 2, 16, 1, 11, 6, 3];
        var statusFilter = [10];

        if (status == 'closed_and_close_ready') {
            statusFilter = [10, 7];
        } else if (status == 'declined') {
            statusFilter = [11, 6, 3];
        } else if (status == 'other') {
            statusFilter = [1, 2, 4, 16];
        }

        let orderLogic;
        if (orderCol != 'firstName') {
            orderLogic = [[orderCol, order]]
        }


        let whereFilter = {
            fundId: fundId,
            status:{
                [Op.ne]:17
            },
            deletedAt: null
        }

        if (status != 'all') {
            whereFilter = {
                fundId: fundId,
                deletedAt: null,
                status: {
                    [Op.in]: statusFilter
                }
            }
        }


        let fundSubscriptions = await models.FundSubscription.findAndCountAll({
            limit: 100,
            offset: Number(offset),
            where: whereFilter,
            order: orderLogic,
            include: [{
                attributes: ['id', 'gpId', 'legalEntity','statusId', 'fundImage', 'partnershipDocument', 'fundManagerLegalEntityName','timezone'],
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    attributes: ['id', 'firstName', 'middleName', 'lastName', 'city', 'state', 'country', 'organizationName', 'cellNumber'],
                    model: models.User,
                    as: 'gp'
                },{
                    model: models.InvestorQuestions,
                    as: 'investorQuestions',
                    where: {
                        fundId: fundId,
                        deletedAt: null
                    },
                    required: false,
                },{
                    model:models.FundStatus,
                    as:'fundStatus',
                    required:true
                },{
                    model: models.timezone,
                    as: 'timeZone',
                    required: false
                }],
            }, {
                model: models.InvestorAnswers,
                as: 'investorAnswers',
                where: {
                    deletedAt: null
                },
                required: false
            }, {
                model: models.DocumentsForSignature,
                as: 'documentsForSignature',
                where: {
                    isActive: true
                },
                include: [{
                    model: models.SignatureTrack,
                    as: 'signatureTrack',
                    where: {
                        isActive: 1
                    },
                    required: false
                }],
                required: false
            }, {
                attributes: ['id', 'name'],
                model: models.InvestorSubType,
                as: 'investorSubTypeInfo',
                required: false
            }, {
                attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
                model: models.FundClosings,
                required: false,
                as: 'fundClosingInfo',
                where: {
                    isActive: 1
                }
            }, {
                attributes: ['id', 'name', 'lpSideName'],
                model: models.FundStatus,
                as: 'subscriptionStatus',
                required: true
            }, {
                model: models.Country,
                as: 'mailingAddressCountryData',
                required: false
            }, {
                model: models.State,
                as: 'mailingAddressStateData',
                required: false
            }, {
                model: models.Country,
                as: 'countryResidenceList',
                required: false
            }, {
                model: models.State,
                as: 'stateResidenceList',
                required: false
            }, {
                model: models.Country,
                as: 'countryDomicileList',
                required: false
            }, {
                model: models.State,
                as: 'stateDomicileList',
                required: false
            }, {
                model: models.User,
                as: 'lp',
                required: false
            }, {
                model: models.OfflineUsers,
                as: 'offlineLp',
                required: false
            }, {
                model: models.ChangeCommitment,
                as: 'commitmentHistory',
                required: false,
                order:[['amount','asc']],
                include: [{
                    model: models.DocumentsForSignature,
                    as: 'document',
                    required: false
                }]
            }]
        });

        const itemCount = fundSubscriptions.count;

        let totalCapitalClosedOrHypotheticalCommitment = 0;
        fundSubscriptions.rows.map(closedInvetsor=>{
            //If fund is closed then take gpconsumed amount
            if(closedInvetsor.status == 10){
                totalCapitalClosedOrHypotheticalCommitment = totalCapitalClosedOrHypotheticalCommitment+ _.last(closedInvetsor.fundClosingInfo).gpConsumed;
            }else{
                totalCapitalClosedOrHypotheticalCommitment = totalCapitalClosedOrHypotheticalCommitment+closedInvetsor.lpCapitalCommitment
            }
        });

        //If atleast one fund has 'not' QualifiedPurchaser then return notQualified purchase
        //default true
        const getQualifiedPurchaseArray = _.map(fundSubscriptions.rows, 'areYouQualifiedPurchaser');

        Promise.map(fundSubscriptions.rows, async subscription => {

            let investorInternal = subscription.toJSON();

            investorInternal.commitmentHistoryPretty = [];


            //Prior commitments if only isLp and isGp primary signed
            if (subscription.commitmentHistory.length > 1) {
                subscription.commitmentHistory = _.orderBy(subscription.commitmentHistory, ['amount'], ['asc'])

                _.map(subscription.commitmentHistory, function (commitmentHistory) {
                    if (commitmentHistory.isPriorAmount || (commitmentHistory.document && commitmentHistory.document.isPrimaryGpSigned && commitmentHistory.document.isPrimaryLpSigned)) {
                        investorInternal.commitmentHistoryPretty.push(`Through ${commonHelper.getDateAndTimeFormat(commitmentHistory.createdAt)} ${commitmentHistory.amountPretty}`);
                    }

                });
                if (investorInternal.commitmentHistoryPretty.length == 1) {
                    investorInternal.commitmentHistoryPretty = []
                }
            }

            let sbuscriptionAgreement = _.get(_.filter(subscription.documentsForSignature, function (item) {
                return item.docType.indexOf('SUBSCRIPTION_AGREEMENT') > -1;
            }), 0);

            let fundAgreement = _.get(_.filter(subscription.documentsForSignature, function (item) {
                return item.docType.indexOf('FUND_AGREEMENT') > -1;
            }), 0);

            let changeCommitmentAgreement = _.get(_.filter(subscription.documentsForSignature, function (item) {
                return item.docType.indexOf('CHANGE_COMMITMENT_AGREEMENT') > -1;
            }), 0);

            let sideletterAgreement =  _.get(_.filter(subscription.documentsForSignature, function (item) {
                return item.docType.indexOf('SIDE_LETTER_AGREEMENT') > -1;
            }), 0);           


            if (fundAgreement) {
                fundAgreement = fundAgreement.toJSON();
                fundAgreement.filePathTempPublicUrl = commonHelper.getLocalUrl(fundAgreement.filePath);
                fundAgreement.isCurrentUserSigned = _.get(fundAgreement, 'signatureTrack', false) ? fundAgreement.signatureTrack.signedUserId == req.userId : false;
            }

            if (sbuscriptionAgreement) {
                sbuscriptionAgreement = sbuscriptionAgreement.toJSON();
                sbuscriptionAgreement.filePathTempPublicUrl = commonHelper.getLocalUrl(sbuscriptionAgreement.filePath);
                sbuscriptionAgreement.isCurrentUserSigned = _.get(sbuscriptionAgreement, 'signatureTrack', false) ? sbuscriptionAgreement.signatureTrack.signedUserId == req.userId : false;
            }
            
            if (changeCommitmentAgreement) {
                changeCommitmentAgreement = changeCommitmentAgreement.toJSON();
                //if investor or signatory haven't signed then we can display cancel link
                let cancelCA = false
                if(changeCommitmentAgreement.lpSignCount==0 &&  changeCommitmentAgreement.isPrimaryLpSigned==0 && changeCommitmentAgreement.isAllLpSignatoriesSigned==0){
                    cancelCA = true
                }
                changeCommitmentAgreement.CancelCommitmentAgreement = cancelCA

                let ccUrl = `${config.get('serverURL')}/api/v1/document/view/cc/${subscription.id}`

                //changeCommitmentAgreement.filePathTempPublicUrl = commonHelper.getLocalUrl(changeCommitmentAgreement.filePath);
                
                changeCommitmentAgreement.filePathTempPublicUrl = ccUrl

                changeCommitmentAgreement.isCurrentUserSigned = _.get(changeCommitmentAgreement, 'signatureTrack', false) ? changeCommitmentAgreement.signatureTrack.signedUserId == req.userId : false;
                
            }

            if (sideletterAgreement) {  

                let sideLetterUrl = `${config.get('serverURL')}/api/v1/document/view/sia/${subscription.id}`

                sideletterAgreement = sideletterAgreement.toJSON();
                //sideletterAgreement.filePathTempPublicUrl = commonHelper.getLocalUrl(sideletterAgreement.filePath);
                sideletterAgreement.filePathTempPublicUrl = sideLetterUrl

                sideletterAgreement.isCurrentUserSigned = _.get(sideletterAgreement, 'signatureTrack', false) ? sideletterAgreement.signatureTrack.signedUserId == req.userId : (subscription.isOfflineInvestor ? true :false);
            }            

            investorInternal.fundAgreement = fundAgreement;
            investorInternal.changeCommitmentAgreement = changeCommitmentAgreement;
            investorInternal.sideletterAgreement = sideletterAgreement;
            investorInternal.sbuscriptionAgreement = sbuscriptionAgreement;

            //  delete  investorInternal.documentsForSignature;
            if(subscription.offlineLp){
                investorInternal.trackerTitle = ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : 
                (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null))) 
                || (subscription.organizationName ? subscription.organizationName : subscription.offlineLp.organizationName) ||
                {name: commonHelper.getFullName(subscription.offlineLp),profilePic:''} );
                
                investorInternal.lp = subscription.offlineLp
            }else{
                investorInternal.trackerTitle = ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : 
                (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null)))  
                || (subscription.organizationName ? subscription.organizationName : subscription.lp.organizationName) ||
                { name: commonHelper.getFullName(subscription.lp), profilePic: subscription.lp.profilePic });
            }


            if (_.last(subscription.fundClosingInfo)) {
                investorInternal.gpConsumed = _.last(subscription.fundClosingInfo).gpConsumed
            }

            // time when lp closed 
            let offset = moment().utcOffset()
            let localText = '' 
            let timezone = ''
            if(subscription.fund.timeZone && subscription.fund.timeZone.code) {
                localText = await commonHelper.setDateUtcToOffset(subscription.lpClosedDate, subscription.fund.timeZone.code).then(function(value){ return value; });
                timezone = subscription.fund.timeZone.displayName;
            } else {
                // localText = moment.utc(subscription.lpClosedDate).utcOffset(offset).format("MMM DD,YYYY LT");
                localText = await commonHelper.getDateAndTime(subscription.lpClosedDate);
            }


            investorInternal.lpClosedDate = (subscription.lpClosedDate !== null) ? localText : '';
            investorInternal.timezone = timezone;

            //Place of Residence
            investorInternal.placeOfResidence = (subscription.investorType == 'Trust' || subscription.investorType == 'Individual') ?

                _.filter([_.get(subscription.countryResidenceList, 'name'),
                _.get(subscription.stateResidenceList, 'name')]).join(', ') :
                ([1, 2, 3, 4, 5].includes(subscription.investorSubType) ? _.get(subscription.stateResidenceList, 'name') :
                    (subscription.stateResidenceList && subscription.countryResidenceList) ?
                        _.filter([_.get(subscription.countryResidenceList, 'name'),
                        _.get(subscription.stateResidenceList, 'name')]).join(', ') : _.get(subscription.countryResidenceList, 'name'))

            // Place of Organization
            investorInternal.placeOfOrganization = null;

            if (subscription.investorType == 'LLC') {
                investorInternal.placeOfOrganization = ([1, 2, 3, 4, 5].includes(subscription.investorSubType) ? _.get(subscription.stateDomicileList, 'name') :
                    (subscription.countryDomicileList && subscription.stateDomicileList) ?
                        _.filter([_.get(subscription.countryDomicileList, 'name'),
                        _.get(subscription.stateDomicileList, 'name')]).join(', ') : _.get(subscription.countryDomicileList, 'name'))
            }
         
            // investor address
            investorInternal.investorAddress = _.compact(_.filter([
                subscription.mailingAddressStreet ? _.trim(subscription.mailingAddressStreet) : '',
                subscription.mailingAddressCity ? _.trim(subscription.mailingAddressCity) : '',
                _.get(subscription.mailingAddressStateData, 'name') ? _.get(subscription.mailingAddressStateData, 'name') : '',
                _.get(subscription.mailingAddressCountryData, 'name') ? _.get(subscription.mailingAddressCountryData, 'name') : '',
                subscription.mailingAddressZip ? subscription.mailingAddressZip : '',
                subscription.mailingAddressPhoneNumber ? subscription.mailingAddressPhoneNumber : ''
            ])).join(', ');

       
            //Fund percentage
            investorInternal.fundPercentage = totalCapitalClosedOrHypotheticalCommitment > 0 ?
                parseFloat(((investorInternal.gpConsumed ?investorInternal.gpConsumed: subscription.lpCapitalCommitment) / totalCapitalClosedOrHypotheticalCommitment) * 100).toFixed(2) : 0;


            const investorAnswers = subscription.investorAnswers;

            let i = 0, investorQuestions = [];
            for (let question of subscription.fund.investorQuestions) {
                investorQuestions[i] = question.dataValues;

                if (investorAnswers.length > 0) {
                    for (let answer of investorAnswers) {
                        if (answer.questionId == question.id) {
                            investorQuestions[i].questionResponse = answer.questionResponse;
                            break;
                        }
                    }
                } else {
                    investorQuestions[i].questionResponse = null;
                }

                i++;
            }

            investorInternal.investorQuestions = investorQuestions;
            //Before starting the subscription
            if(subscription.lookThrough == null){
                investorInternal.lookThroughIssues = 'No' 
            }else{

                let percentageOfInvestment = investorInternal.fundPercentage;
                //Look through issues only for Trust and Entity
                investorInternal.lookThroughIssues = (!subscription.lookThrough && subscription.investorType !== 'Individual') ? 'Yes'
                : (subscription.investorType !== 'Individual' ? await commonHelper.getLookThroughValues(subscription,getQualifiedPurchaseArray,percentageOfInvestment): 'No' )
            }
          

            if( investorInternal.lookThroughIssues == 'Yes'){
                //If lookThroughIssues is 'Yes' then take numberOfDirectEquityOwners else default value 1
                investorInternal.investorCount = subscription.numberOfDirectEquityOwners;
            }

            return investorInternal;
        }).then(fundInvestorsList=>{
//console.log(fundInvestorsList);
            //Only on Default sorting
            if (orderCol == 'firstName') {
                //Display the collection in the mentioned sorted order of status  
                fundInvestorsList = _.sortBy(fundInvestorsList, function (fundInvestorsListObj) {
                    return sortStatusOrder.indexOf(fundInvestorsListObj.status)
                });
            }
            return res.json({
                itemCount,
                data: fundInvestorsList
            })
        })

    } catch (error) {
        return res.json(error.message)
    }
}

const viewFundType = async (req, res, next) => {
    try {
        let fundType = await models.FundType.findAll()
        return res.json(fundType)
    }
    catch (e) {
        next(e)
    }

}

const fundSummary = async (req, res, next) => {
    try {

        const { fundId } = req.params;

        if (!fundId) {
            return res.status(404).json({
                "errors": [
                    {
                        "msg": messageHelper.fundIdValidate
                    }
                ]
            });
        }

        let fundSubscriptions = await models.FundSubscription.findAll({
            where: {
                fundId: fundId,
                deletedAt: null,
                status:{
                    [Op.ne]:17
                }
            },
            include: [
                {
                    attributes: ['id', 'gpId', 'legalEntity', 'fundImage', 'partnershipDocument'],
                    model: models.Fund,
                    as: 'fund',
                    required: true,
                    include: [{
                        attributes: ['id', 'firstName', 'middleName', 'lastName', 'city', 'state', 'country', 'organizationName', 'cellNumber'],
                        model: models.User,
                        as: 'gp'
                    }, {
                        model: models.InvestorQuestions,
                        as: 'investorQuestions',
                        where: {
                            fundId: fundId,
                            deletedAt: null
                        },
                        required: false,
                    }],
                },
                {
                    model: models.InvestorAnswers,
                    as: 'investorAnswers',
                    where: {
                        deletedAt: null
                    },
                    required: false
                }, {
                    attributes: ['id', 'name'],
                    model: models.InvestorSubType,
                    as: 'investorSubTypeInfo',
                    required: false
                }, {
                    attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
                    model: models.FundClosings,
                    required: false,
                    as: 'fundClosingInfo',
                    where: {
                        isActive: 1
                    }
                }, {
                    model: models.User,
                    as: 'lp',
                    required: false
                }, {
                    model: models.OfflineUsers,
                    as: 'offlineLp',
                    required: false
                }, {
                    model: models.ChangeCommitment,
                    as: 'commitmentHistory',
                    required: false,
                    include: [{
                        model: models.DocumentsForSignature,
                        as: 'document',
                        required: false
                    }]
                }, {
                    model: models.Country,
                    as: 'mailingAddressCountryData',
                    required: false
                }, {
                    model: models.State,
                    as: 'mailingAddressStateData',
                    required: false
                }, {
                    model: models.Country,
                    as: 'countryResidenceList',
                    required: false
                }, {
                    model: models.State,
                    as: 'stateResidenceList',
                    required: false
                }, {
                    model: models.Country,
                    as: 'countryDomicileList',
                    required: false
                }, {
                    model: models.State,
                    as: 'stateDomicileList',
                    required: false
                }]
        })


        // Closed- 10,Close-Ready- 7,open ready draft - 2 (as 'In Progress'),Open - 1 (as 'Invited'), 
        // Not Participating -11,Rescind-6 (as 'Not Participating'),Not Interested-3 (as 'Not Participating')
        let sortStatusOrder = [10, 7, 2, 1, 11, 6, 3];

        let totalCapitalClosedOrHypotheticalCommitment = 0;
        fundSubscriptions.map(closedInvetsor=>{
            //If fund is closed then take gpconsumed amount
            if(closedInvetsor.status == 10){
                totalCapitalClosedOrHypotheticalCommitment = totalCapitalClosedOrHypotheticalCommitment+ _.last(closedInvetsor.fundClosingInfo).gpConsumed;
            }else{
                totalCapitalClosedOrHypotheticalCommitment = totalCapitalClosedOrHypotheticalCommitment+closedInvetsor.lpCapitalCommitment
            }
        });

        //If atleast one fund has 'not' QualifiedPurchaser then return notQualified purchase
        //default true
        const getQualifiedPurchaseArray = _.map(fundSubscriptions, 'areYouQualifiedPurchaser');


        let dynamicQuestionHeaders = [], getQuestionTitlesLength;
        Promise.map(fundSubscriptions ,async subscription => {
            let investorInternal = subscription.toJSON();

            investorInternal.commitmentHistoryPretty = [];

            //Prior commitments if only isLp and isGp primary signed
            if (subscription.commitmentHistory.length > 1) {
                subscription.commitmentHistory = _.orderBy(subscription.commitmentHistory, ['amount'], ['asc'])
                _.map(subscription.commitmentHistory, function (commitmentHistory) {
                    if (commitmentHistory.isPriorAmount || (commitmentHistory.document && commitmentHistory.document.isPrimaryGpSigned && commitmentHistory.document.isPrimaryLpSigned)) {
                        investorInternal.commitmentHistoryPretty.push(`Through ${commonHelper.getDateAndTimeFormat(commitmentHistory.createdAt)} ${commitmentHistory.amountPretty}`);
                    }
                });
                if (investorInternal.commitmentHistoryPretty.length == 1) {
                    investorInternal.commitmentHistoryPretty = []
                }
            }

         
            if(subscription.offlineLp){
                investorInternal.trackerTitle = ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : 
                (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null))) 
                || (subscription.organizationName ? subscription.organizationName : 
                commonHelper.getFullName(subscription.offlineLp)));
                investorInternal.signatoryDetails = { name: commonHelper.getFullName(subscription.offlineLp) }
                
            }else{
                investorInternal.trackerTitle = ((subscription.investorType == 'LLC' ? subscription.entityName :
                (subscription.investorType == 'Trust' ? subscription.trustName : 
                (subscription.areYouSubscribingAsJointIndividual ? subscription.legalTitleDesignationPrettyForPDF : null))) 
                || (subscription.organizationName ? subscription.organizationName : subscription.lp.organizationName) ||
                commonHelper.getFullName(subscription.lp));
                investorInternal.signatoryDetails = { name: commonHelper.getFullName(subscription.lp) }

            }

        

            if (_.last(subscription.fundClosingInfo)) {
                investorInternal.gpConsumed = _.last(subscription.fundClosingInfo).gpConsumed
            }

            // time when lp closed 
            var offset = moment().utcOffset();
            var localText = moment.utc(subscription.lpClosedDate).utcOffset(offset).format("MMM DD,YYYY LT");
            investorInternal.lpClosedDate = (subscription.lpClosedDate !== null) ? localText : '';

            //Place of Residence
            investorInternal.placeOfResidence = (subscription.investorType == 'Trust' || subscription.investorType == 'Individual') ?

                _.filter([_.get(subscription.countryResidenceList, 'name'),
                _.get(subscription.stateResidenceList, 'name')]).join(', ') :
                ([1, 2, 3, 4, 5].includes(subscription.investorSubType) ? _.get(subscription.stateResidenceList, 'name') :
                    (subscription.stateResidenceList && subscription.countryResidenceList) ?
                        _.filter([_.get(subscription.countryResidenceList, 'name'),
                        _.get(subscription.stateResidenceList, 'name')]).join(', ') : _.get(subscription.countryResidenceList, 'name'))

            // Place of Organization
            investorInternal.placeOfOrganization = null;

            if (subscription.investorType == 'LLC') {
                investorInternal.placeOfOrganization = ([1, 2, 3, 4, 5].includes(subscription.investorSubType) ? _.get(subscription.stateDomicileList, 'name') :
                    (subscription.countryDomicileList && subscription.stateDomicileList) ?
                        _.filter([_.get(subscription.countryDomicileList, 'name'),
                        _.get(subscription.stateDomicileList, 'name')]).join(', ') : _.get(subscription.countryDomicileList, 'name'))
            }

            getQuestionTitlesLength = _.map(subscription.fund.investorQuestions, 'questionTitle').length

            let questionAndAnwserObject = {};
            for (let question of subscription.fund.investorQuestions) {
                dynamicQuestionHeaders.push({ header: question.questionTitle, key: question.questionTitle, width: 23 })
                if (subscription.investorAnswers.length) {
                    for (let answer of subscription.investorAnswers) {
                        if (answer.questionId == question.id) {
                            if (question.typeOfQuestion == 2) {
                                questionAndAnwserObject[question.questionTitle] = Number(answer.questionResponse) ? 'True' : 'False'
                            } else if (question.typeOfQuestion == 3) {
                                questionAndAnwserObject[question.questionTitle] = Number(answer.questionResponse) ? 'Yes' : 'No'
                            } else {
                                questionAndAnwserObject[question.questionTitle] = answer.questionResponse ? answer.questionResponse : 'N/A'
                            }
                        } else {
                            if (!questionAndAnwserObject[question.questionTitle]) {
                                questionAndAnwserObject[question.questionTitle] = 'N/A'
                            }
                        }
                    }
                } else {
                    questionAndAnwserObject[question.questionTitle] = 'N/A'
                }
            }

            // investor address
            investorInternal.investorAddress = _.compact(_.filter([
                subscription.mailingAddressStreet ? _.trim(subscription.mailingAddressStreet) : '',
                subscription.mailingAddressCity ? _.trim(subscription.mailingAddressCity) : '',
                _.get(subscription.mailingAddressStateData, 'name') ? _.get(subscription.mailingAddressStateData, 'name') : '',
                _.get(subscription.mailingAddressCountryData, 'name') ? _.get(subscription.mailingAddressCountryData, 'name') : '',
                subscription.mailingAddressZip ? subscription.mailingAddressZip : '',
                subscription.mailingAddressPhoneNumber ? subscription.mailingAddressPhoneNumber : ''
            ])).join(', ');

  
            //Fund percentage
            investorInternal.fundPercentage = totalCapitalClosedOrHypotheticalCommitment > 0 ?
            ((investorInternal.gpConsumed ?investorInternal.gpConsumed: subscription.lpCapitalCommitment) / totalCapitalClosedOrHypotheticalCommitment) * 100 : 0;


            //Before starting the subscription
            if(subscription.lookThrough == null){
                investorInternal.lookThroughIssues = 'No' 
            }else{

                let percentageOfInvestment = investorInternal.fundPercentage;
                //Look through issues only for Trust and Entity
                investorInternal.lookThroughIssues = (!subscription.lookThrough && subscription.investorType !== 'Individual') ? 'Yes'
                : (subscription.investorType !== 'Individual' ? await commonHelper.getLookThroughValues(subscription,getQualifiedPurchaseArray,percentageOfInvestment): 'No' )
            }
            

            if( investorInternal.lookThroughIssues == 'Yes'){
                //If lookThroughIssues is 'Yes' then take numberOfDirectEquityOwners else default value 1
                investorInternal.investorCount = subscription.numberOfDirectEquityOwners;
            }

            

            return {
                "investorName": investorInternal.trackerTitle,
                "signatoryName": investorInternal.signatoryDetails.name,
                "investorType": investorInternal.investorType == 'LLC' ? 'Entity' : (investorInternal.investorType ? investorInternal.investorType : 'N/A'),
                "investorSubType": investorInternal.investorType == 'LLC' || investorInternal.investorType == 'Trust' ? (investorInternal.investorSubType ? investorInternal.investorSubTypeInfo.name : (investorInternal.otherInvestorSubType ? investorInternal.otherInvestorSubType : 'N/A')) : 'N/A',
                "otherInvestorAttributes": investorInternal.otherInvestorAttributes && investorInternal.otherInvestorAttributes.length ? investorInternal.otherInvestorAttributes.join(', ') : 'N/A',
                "lpCapitalCommitment": investorInternal.lpCapitalCommitment ? investorInternal.lpCapitalCommitment : 0,
                "gpConsumed": investorInternal.gpConsumed ? investorInternal.gpConsumed : 0,
                "commitmentHistoryPretty": (investorInternal.commitmentHistoryPretty.length ? investorInternal.commitmentHistoryPretty.toString().replace(/[\[\]']+/g, '') : ''),
                "investorCount": investorInternal.investorCount ? investorInternal.investorCount : 1,
                "placeOfResidence": investorInternal.placeOfResidence ? investorInternal.placeOfResidence : 'N/A',
                "placeOfOrganization": investorInternal.placeOfOrganization ? investorInternal.placeOfOrganization : 'N/A',
                "investorAddress": investorInternal.investorAddress ? investorInternal.investorAddress : '',
                "fundPercentage": investorInternal.fundPercentage ? investorInternal.fundPercentage : 0,
                "areYouAccreditedInvestor": investorInternal.areYouAccreditedInvestor ? 'Yes' : 'No',
                "areYouQualifiedPurchaser": investorInternal.areYouQualifiedPurchaser ? 'Yes' : 'No',
                "areYouQualifiedClient": investorInternal.areYouQualifiedClient ? 'Yes' : 'No',
                "companiesAct": investorInternal.companiesAct && investorInternal.companiesAct > 1 ? 'Yes' : 'No',
                "numberOfDirectEquityOwners": investorInternal.numberOfDirectEquityOwners ? investorInternal.numberOfDirectEquityOwners : 1,
                "lookThorughIssues": investorInternal.lookThroughIssues,
                "erisaPlan": (!investorInternal.employeeBenefitPlan && !investorInternal.planAsDefinedInSection4975e1 && !investorInternal.benefitPlanInvestor) ? 'No' : 'Yes',
                "totalValueOfEquityInterests": !investorInternal.employeeBenefitPlan && !investorInternal.planAsDefinedInSection4975e1 && !investorInternal.benefitPlanInvestor ? 0 : (!investorInternal.employeeBenefitPlan && !investorInternal.planAsDefinedInSection4975e1 ? (investorInternal.totalValueOfEquityInterests ? investorInternal.totalValueOfEquityInterests : 0) : 100),
                "releaseInvestmentEntityRequired": investorInternal.releaseInvestmentEntityRequired ? 'Yes' : 'No',
                "isSubjectToDisqualifyingEvent": investorInternal.isSubjectToDisqualifyingEvent ? 'Yes' : 'No',
                "fundManagerInfo": investorInternal.fundManagerInfo ? investorInternal.fundManagerInfo : 'N/A',
                "closedTime": investorInternal.lpClosedDate ? investorInternal.lpClosedDate : 'N/A',
                ...questionAndAnwserObject
            }

        }).then(fundInvestorsList=>{

            //Download the collection in the order of displaying  
            fundInvestorsList = _.sortBy(fundInvestorsList, function (fundInvestorsListObj) {
                return sortStatusOrder.indexOf(fundInvestorsListObj.status)
            });
    
            //Removing duplicate objects based on questions length
            dynamicQuestionHeaders = dynamicQuestionHeaders.splice(0, getQuestionTitlesLength)
    
            const requiredColumns = [
                { header: 'Investor Name', key: 'investorName', width: 23 },
                { header: 'Signatory', key: 'signatoryName', width: 23 },
                { header: 'Investor Type', key: 'investorType', width: 23 },
                { header: 'Investor Sub Type', key: 'investorSubType', width: 23 },
                { header: 'Capital Commitment Offered', key: 'lpCapitalCommitment', width: 23, style: { numFmt: '"$"#,##0.00' } },
                { header: 'Capital Commitment Accepted', key: 'gpConsumed', width: 23, style: { numFmt: '"$"#,##0.00' } },
                { header: 'Prior Commitment Amounts', key: 'commitmentHistoryPretty', width: 33, style: { alignment: { wrapText: true } } },
                { header: '1940 Act Investor Count', key: 'investorCount', width: 23 },
                { header: 'Place of Residence', key: 'placeOfResidence', width: 23, style: { alignment: { wrapText: true } } },
                { header: 'Place of Organization', key: 'placeOfOrganization', width: 23, style: { alignment: { wrapText: true } } },
                { header: 'Investors Addresses', key: 'investorAddress', width: 33, style: { alignment: { wrapText: true } } },
                { header: '% of the Fund', key: 'fundPercentage', width: 23, style: { numFmt: '0.00"%"' } },
                { header: 'Accredited Investor', key: 'areYouAccreditedInvestor', width: 23 },
                { header: 'Qualified Purchaser', key: 'areYouQualifiedPurchaser', width: 23 },
                { header: 'Qualified Client', key: 'areYouQualifiedClient', width: 23 },
                { header: 'Investment Company Act Matters', key: 'companiesAct', width: 23 },
                { header: 'No. of Beneficial Owners', key: 'numberOfDirectEquityOwners', width: 23 },
                { header: 'Look-Through Issues', key: 'lookThorughIssues', width: 23 },
                { header: 'ERISA Representation', key: 'erisaPlan', width: 23 },
                { header: 'Benefit Plan %', key: 'totalValueOfEquityInterests', width: 23, style: { numFmt: '0.00"%"' } },
                { header: 'FOIA and Similar Disclosure Obligations', key: 'releaseInvestmentEntityRequired', width: 23 },
                { header: 'Disqualifying Event', key: 'isSubjectToDisqualifyingEvent', width: 23 },
                { header: 'Special Disclosures', key: 'fundManagerInfo', width: 23 },
                { header: 'Other Investor Attributes', key: 'otherInvestorAttributes', width: 23 },
                { header: 'Closed Time', key: 'closedTime', width: 23 },
                ...dynamicQuestionHeaders
            ];
    
            const workbook = new excel.Workbook();
            const sheet = workbook.addWorksheet('Tracker');
    
            sheet.columns = requiredColumns;
            sheet.addRows(fundInvestorsList);
    
            sheet.getRow(1).font = { bold: true };
            const fileName = `${uuidv1()}.xlsx`
            const filepath = `./assets/temp/${fileName}`;
    
            workbook.xlsx.writeFile(filepath).then(function () {
                return res.json({ url: `${config.get('serverURL')}/assets/temp/${fileName}` })
            });
        });


    } catch (e) {
        next(e)
    }
}



module.exports = {
    viewFund,
    getFundById,
    fundTotals,
    rightSideSummary,
    leftSideSummary,
    viewFundType,
    fundSummary
}
