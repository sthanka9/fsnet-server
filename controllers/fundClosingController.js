
const models = require('../models/index');
const { Op } = require('sequelize');
const config = require('../config/index');
const _ = require('lodash');
const moment = require('moment');
const fs = require('fs');
const uuidv1 = require('uuid/v1');
const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const logger = require('../helpers/logger');
const eventHelper = require('../helpers/eventHelper');
const effectuateHelper = require('../helpers/effectuateHelper');
const notificationHelper = require('../helpers/notificationHelper');
const swapFundAmendmentsHelper = require('../helpers/swapFundAmendmentsHelper');

const closeFund = async (req, res, next) => {

    try {
        var { fundClosingData = [], fundId, finalClosing = false, proceed = false, withOutNewFundAgreement = false } = req.body

        logger.info('******** Close Fund Start***************')

        finalClosing = JSON.parse(finalClosing);
        proceed = JSON.parse(proceed);
        withOutNewFundAgreement = JSON.parse(withOutNewFundAgreement);
        fundClosingData = JSON.parse(fundClosingData);

        var emailsContent = {}
        let errorCodes = []


        if (req.file) {

            logger.info('******** Close Fund : New File Uploaded  ***************'+req.file.filename)
         
            const ext = path.extname(req.file.filename)
        
            await models.Fund.update({ partnershipDocument: await commonHelper.getFileInfo(req.file), updatedBy: req.userId }, {
                where: {
                    id: fundId
                }
            })

            const signedDocs = await models.DocumentsForSignature.findAll({
                where: {
                    fundId: fundId,
                    docType: 'FUND_AGREEMENT',
                    isActive: 1,
                    isPrimaryGpSigned: 0,
                    isArchived:0
                },
                include: [{
                    attributes: ['id'],
                    model: models.FundSubscription,
                    as: 'subscription',
                    required: false,
                    include: [{
                        attributes: ['timezone'],
                        model: models.Fund,
                        as: 'fund',
                        required: false,
                        include: [{
                            model: models.timezone,
                            as: 'timeZone',
                            required: false
                        }]
                    }]
                    },{
                    model: models.SignatureTrack,
                    as: 'signatureTrackList',
                    required: true,
                    include: [{
                        model: models.OfflineUsers,
                        as: 'signedOfflineUser',
                        required: false
                    },{
                        model: models.User,
                        as: 'signeduser',
                        required: false
                    }],
                    where: {
                        documentType: 'FUND_AGREEMENT',
                        isActive: 1
                    }
                }]
            });

            const fundTemp = await models.Fund.findOne({
                where: {
                    id: fundId
                }
            });

            const fundAgreementTempFile = fundTemp.partnershipDocument.path;

            for (signedDoc of signedDocs) {

                const subscription = await models.FundSubscription.findOne({
                    where: {
                        id: signedDoc.subscriptionId,
                    }
                });
                const newSignedPages = [];

                //getting timezone from fund
                let timezone = null; let timezone_code = null;
                if(signedDoc.subscription.fund.timeZone && subscription.fund.timeZone.code){
                    timezone = signedDoc.subscription.fund.timeZone;
                    timezone_code = signedDoc.subscription.fund.timeZone.code;
                }


                for (signature of signedDoc.signatureTrackList) {

                    if(signature.signeduser && signature.signedByType !== 'OfflineLP' ) {
                        let lpSignaturePic = commonHelper.getLocalUrl(signature.signaturePath);

                        let date_for_sign = commonHelper.getDateTimeWithTimezone(timezone, signature.createdAt, 0, 0);
                        console.log('date_for_sign', date_for_sign);
                        console.log('timezone', timezone);
                        console.log('signature.createdAt', signature.createdAt);
                        //Data to be rendered in HTML
                        const agreementSignData = {
                            jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                            investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                            legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                            lpName: signature.signeduser ? commonHelper.getFullName(signature.signeduser) : '',
                            areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                            date: date_for_sign,
                            lpSignaturePic: commonHelper.getSignaturePicUrl(signature.signaturePath),
                            lpCapitalCommitment: subscription.lpCapitalCommitmentPretty,
                            name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName,
                            gpName: signature.signeduser ? commonHelper.getFullName(signature.signeduser) : '',
                            fundManagerTitle: fundTemp.fundManagerTitle,
                            fundManagerCommonName: fundTemp.fundManagerCommonName,
                            fundCommonName: fundTemp.fundCommonName,
                            fundLegalEntity: fundTemp.legalEntity,
                            fundManagerLegalEntityName: fundTemp.fundManagerLegalEntityName,
                            gpSignaturePic: commonHelper.getSignaturePicUrl(signature.signaturePath),
                            capitalCommitment: subscription.lpCapitalCommitmentPretty,
                        }
    

                        const htmlFilePath = ['LP', 'SecondaryLP'].includes(signature.signedByType) ? "./views/fundAgreementLpSign.html" : "./views/fundAgreementGpSign.html";
    
                        const fundAgreementTempPath = await generateSignedPage(agreementSignData, htmlFilePath);
                        newSignedPages.push(fundAgreementTempPath.path);
                    }else{
                        newSignedPages.push(signature.signaturePath)
                    }
                }

                if (newSignedPages.length > 0) {
                   const previousFundAgreement =  await models.DocumentsForSignature.findOne({
                        where: {
                            id: signedDoc.id
                        }
                    });
                    newSignedPages.unshift(fundAgreementTempFile);
                    await commonHelper.pdfMerge(newSignedPages, previousFundAgreement.filePath);
                }
            }
        }

        const fund = await models.Fund.findOne({
            include: [{
                model: models.timezone,
                as: 'timeZone',
                required: false
            },{
                model: models.User,
                as: 'gp',
                required: true
            }],
            where: { id: fundId }
        });

        const partnershipDocumentPath = req.file ? req.file.path : fund.partnershipDocument.path;
        const partnershipDocumentPageCount = await swapFundAmendmentsHelper.getPartnershipDocumentPageCount(partnershipDocumentPath);

        const userId = req.userId

        const subscriptionIds = fundClosingData.map(value => value.subscriptionId);

        let lpIds = fundClosingData.map(value => value.lpId);
        let offlineLpIds = fundClosingData.map(value => value.offlineLpId);

        lpIds = _.compact(lpIds);
        offlineLpIds = _.compact(offlineLpIds);

        // total actual lp commitment
        const actualTotalCapitalCommitment = models.FundSubscription.sum('lpCapitalCommitment', {
            where: {
                id: {
                    [Op.in]: subscriptionIds
                }
            }
        });

        let gpWantedTotalCommitment = parseFloat(_.sumBy(fundClosingData, function (o) { return parseFloat(o.gpConsumed); }));

        if (actualTotalCapitalCommitment < gpWantedTotalCommitment) {
            return errorHelper.error_400(res, 'validationErrors', messageHelper.checkCommitmentValue);
        }


        // check individual commitment and consumed price
        const invalidSubscriptions = []
        for (let d of fundClosingData) {
            if (d.lpCapitalCommitment < d.gpConsumed) {
                invalidSubscriptions.push(d.subscriptionId)
            }
        }

        if (invalidSubscriptions.length !== 0) {
            return errorHelper.error_400(res, 'validationErrors', messageHelper.checkCommitmentValue);
        }


        // CURRENT CLOSING CHECK
        if (Number(fund.fundHardCap) < Number(gpWantedTotalCommitment)) {

            return errorHelper.error_400(res, 'validationErrors', messageHelper.hardCapExceeds, {
                "CODE": 'AMOUNT_EXCESS_HARD_CAP',
            });

        }

        // ALREADY CLOSED FUND CHECK
        let fundClosingAmount = await models.FundClosings.sum('gpConsumed',{ where: { fundId: fund.id, isActive: true } })

        if (fund.fundHardCap < (gpWantedTotalCommitment + fundClosingAmount)) {

            return errorHelper.error_400(res, 'validationErrors', messageHelper.hardCapExceeds, {
                "CODE": 'AMOUNT_EXCESS_HARD_CAP',
            });
        }

        //get close ready or closed status investors that are NOT qualified purchasers.
        const closeAndCloseReadySubscriptions = await models.FundSubscription.findAll({
            where: {
                fundId: fund.id,
                status: {
                    [Op.in]: [10, 7] // closed and close ready.
                }
            },
            include:[{
                model: models.User,
                as: 'lp',
                required: false
            },{
                model: models.OfflineUsers,
                as: 'offlineLp',
                required: false
                
            },{
                attributes: ['fundId', 'subscriptionId', 'lpCapitalCommitment', 'gpConsumed'],
                model: models.FundClosings,
                required: false,
                as: 'fundClosingInfo',
                where: {
                    isActive: 1
                }
            }]
        })

        // get selected lps from close ready and closed status
        let selectedClosedReadyAndClosedSubscriptions = _(closeAndCloseReadySubscriptions)
        .keyBy('id')
        .at(subscriptionIds)
        .value();

        let selectedClosedReadyAndClosedSubscriptionsOfOfflineLp = _(closeAndCloseReadySubscriptions)
        .keyBy('offlineLpId')
        .at(offlineLpIds)
        .value();
        
        selectedClosedReadyAndClosedSubscriptions = [...selectedClosedReadyAndClosedSubscriptions,...selectedClosedReadyAndClosedSubscriptionsOfOfflineLp]

        selectedClosedReadyAndClosedSubscriptions = _.compact(selectedClosedReadyAndClosedSubscriptions);
        
        const closedSubscriptions = _.filter(closeAndCloseReadySubscriptions, {
            status: 10
        });

        //If atleast one fund has 'not' QualifiedPurchaser then return notQualified purchase
        //default true
        const getQualifiedPurchaseArray = _.map(closeAndCloseReadySubscriptions, 'areYouQualifiedPurchaser');

        let totalCapitalClosedOrHypotheticalCommitment = 0;
        closeAndCloseReadySubscriptions.map(closedInvetsor=>{
            //If fund is closed then take gpconsumed amount
            if(closedInvetsor.status == 10){
                totalCapitalClosedOrHypotheticalCommitment = totalCapitalClosedOrHypotheticalCommitment + _.last(closedInvetsor.fundClosingInfo).gpConsumed;
            }else{
                totalCapitalClosedOrHypotheticalCommitment = totalCapitalClosedOrHypotheticalCommitment + closedInvetsor.lpCapitalCommitment
            }
        });

        let investorCountArray = [];
        // let investorCount = _.sumBy(closeAndCloseReadySubscriptions, 'investorCount');
        for(let subscription of closeAndCloseReadySubscriptions){

            let gpConsumed=0;
            if (_.last(subscription.fundClosingInfo)) {
                gpConsumed = _.last(subscription.fundClosingInfo).gpConsumed
            }

            //Fund percentage
            let fundPercentage = totalCapitalClosedOrHypotheticalCommitment > 0 ?
                parseFloat(((gpConsumed ? gpConsumed: subscription.lpCapitalCommitment) / totalCapitalClosedOrHypotheticalCommitment) * 100).toFixed(2) : 0;
 
 
            //Look through issues
            if(subscription.lookThrough){
                lookThroughIssuesFlag = await commonHelper.getLookThroughValues(subscription,getQualifiedPurchaseArray,fundPercentage);

                //Default investorCount is 1
                const investorCountValue = (lookThroughIssuesFlag == 'Yes') ? subscription.numberOfDirectEquityOwners : (subscription.investorCount ?subscription.investorCount:1)
            
                investorCountArray.push( investorCountValue );
            }else{
                investorCountArray.push(subscription.numberOfDirectEquityOwners);
            }
        }

        let investorCount = _.sum(investorCountArray);

        const closeAndCloseReadySubscriptions99 = _.filter(closeAndCloseReadySubscriptions, {
            areYouQualifiedPurchaser: false,
            areYouAccreditedInvestor: true
        });



        const closeAndCloseReadySubscriptions499 = _.filter(closeAndCloseReadySubscriptions, {
            areYouQualifiedPurchaser: true
        });

        const closeSubscriptions499 = _.filter(closedSubscriptions, {
            areYouQualifiedPurchaser: false
        });

        const equityOwnerQuestions = _.filter(closeAndCloseReadySubscriptions,{
            existingOrProspectiveInvestorsOfTheFund:true
        })
    

        if (closeSubscriptions499.length > 0 && closedSubscriptions.length > 0 && investorCount > 99) {

            emailsContent.CLOSED_INVESTORS_COUNT_EXCESS_99 = true

            errorCodes.push('CLOSED_INVESTORS_COUNT_EXCESS_99')

        }


        if (closeAndCloseReadySubscriptions499 && closeAndCloseReadySubscriptions499.length > 0 && closeAndCloseReadySubscriptions.length == closeAndCloseReadySubscriptions499.length && investorCount > 499) {
            emailsContent.INVESTORS_COUNT_EXCESS_499 = true
            errorCodes.push('INVESTORS_COUNT_EXCESS_499')
        }

        if (closeAndCloseReadySubscriptions99 && closeAndCloseReadySubscriptions99.length > 0 && investorCount > 99) {

            emailsContent.INVESTORS_COUNT_EXCESS_99 = true

            errorCodes.push('INVESTORS_COUNT_EXCESS_99')

        }

        //get closed status investors for the fund
        const fundInvestorsClosed = _.filter(closeAndCloseReadySubscriptions, { status: 10 });
        const closeReadySubscriptions = _.filter(closeAndCloseReadySubscriptions, { status: 7 });

        //get closed-ready status (selected investor) investors for the fund
        let fundInvestorsSelected = _(closeReadySubscriptions)
            .keyBy('lpId')
            .at(lpIds)
            .value();
        fundInvestorsSelected = _.filter(fundInvestorsSelected);

        //ERISA – TESTED ONGOING AND AT CLOSING
        let closedYesAmount = 0;
        let closedTotalAmount = 0;

        for (const eachrow of fundInvestorsClosed) {
            if (eachrow.employeeBenefitPlan || eachrow.planAsDefinedInSection4975e1 || eachrow.benefitPlanInvestor) {
                closedYesAmount += eachrow.lpCapitalCommitment
            }
            closedTotalAmount += eachrow.lpCapitalCommitment
        }

        let selectedInvestorYesAmount = 0;
        let selectedInvestorTotalAmount = 0;
        for (const eachrow of fundInvestorsSelected) {
            if (eachrow.employeeBenefitPlan || eachrow.planAsDefinedInSection4975e1 || eachrow.benefitPlanInvestor) {
                selectedInvestorYesAmount += eachrow.lpCapitalCommitment
            }
            selectedInvestorTotalAmount += eachrow.lpCapitalCommitment
        }

        const erisaPercentage = ((selectedInvestorYesAmount + closedYesAmount) / (selectedInvestorTotalAmount + closedTotalAmount)) * 100

        if (erisaPercentage > 25) {
            emailsContent.ERISA_WARNINGS = true
            errorCodes.push('ERISA_WARNINGS')
        }

        //Disqualifying Event – TESTED ONGOING AND AT CLOSING
        const disqualifyingEvent = _.filter(selectedClosedReadyAndClosedSubscriptions, {
            isSubjectToDisqualifyingEvent: true
        })


        if (disqualifyingEvent && disqualifyingEvent.length > 0) {
            emailsContent.DISQUALIFYING_EVENT = true
            errorCodes.push('DISQUALIFYING_EVENT')
        }


        const investorNameArray=[]
        if(equityOwnerQuestions && equityOwnerQuestions.length){
            emailsContent.EQUITY_OWNERS_EVENT = true
            _.filter(closeAndCloseReadySubscriptions,investor=>{
                if(investor.existingOrProspectiveInvestorsOfTheFund){
                    investorNameArray.push((investor.investorType  == 'LLC' ? investor.entityName : 
                    (investor.investorType == 'Trust' ? investor.trustName : '' )))                    
                    return investorNameArray
                }
            })

            emailsContent.fundInvestorNames = investorNameArray.join(', ')
            errorCodes.push('EQUITY_OWNERS_EVENT')
            errorCodes.push({
                investorNameArray,
                numberOfexistingOrProspectives:_.filter(_.uniq(_.map(closeAndCloseReadySubscriptions,'numberOfexistingOrProspectives'))).join(', ')
            })
    
        }

        // EEA country validation
        // get all EEA countries
        let eEACountries = await models.Country.findAll({
            where: {
                isEEACountry: 1
            }
        })

        eEACountries = eEACountries.map(country => country.id)


        const eeaValidationEvent = _.filter(selectedClosedReadyAndClosedSubscriptions, function (data) {

            //[14, 21, 33, 56, 57, 58, 68, 74, 75, 82, 85, 99, 105, 107, 120, 126, 127, 135, 155, 175, 176, 180, 197, 198, 205, 211, 230, 100, 125, 164, 54, 212]
            if (_.includes(eEACountries,data.countryDomicileId) && data.investorType == 'LLC' && (_.includes(lpIds,subscription.lpId) || _.includes(offlineLpIds,subscription.offlineUserId))) {
                return true;
            }else if(_.includes(eEACountries,data.countryResidenceId) && (data.investorType == 'Trust' || data.investorType == 'Individual') ){
                return true;
            }

        })

        const LPResult = [];
        _.filter(selectedClosedReadyAndClosedSubscriptions, function (subscription) {
            const name = (subscription.investorType == 'LLC' ? subscription.entityName : 
            (subscription.investorType == 'Trust' ? subscription.trustName : null )) 
            || (subscription.organizationName ? subscription.organizationName : ((subscription.lp && subscription.lp.organizationName) ? subscription.lp.organizationName : ''  ))
            ||  (subscription.lp ? commonHelper.getFullName(subscription.lp) :  (subscription.offlineLp ?  commonHelper.getFullName(subscription.offlineLp) : ''))
            // check for individual lp country
            if ((_.includes(lpIds,subscription.lpId) || _.includes(offlineLpIds,subscription.offlineUserId)) && _.includes(eEACountries,subscription.countryDomicileId) && subscription.investorType == 'LLC' ) {
                return LPResult.push(name)
            }else if((_.includes(lpIds,subscription.lpId) || _.includes(offlineLpIds,subscription.offlineUserId)) && _.includes(eEACountries,subscription.countryResidenceId) && (subscription.investorType == 'Trust' || subscription.investorType == 'Individual')) {
                return LPResult.push(name)
            }
        })


        if (eeaValidationEvent && eeaValidationEvent.length > 0) {
            emailsContent.EEA_WARNINGS = true
            errorCodes.push('EEA_WARNINGS')
            errorCodes.push({ 'fundName': fund.legalEntity, 'lpNames': LPResult })
        }

        // even one investor that is not an accredited investor Accredited investor
        const accreditedInvestorToValidate = _.filter(selectedClosedReadyAndClosedSubscriptions, {
            areYouAccreditedInvestor: false
        })

        if (accreditedInvestorToValidate && accreditedInvestorToValidate.length > 0) {
            emailsContent.ACCREDITED_EVENT = true
            errorCodes.push('ACCREDITED_EVENT')
            errorCodes.push({ 'fundName': fund.legalEntity })
        }

        if (!proceed && errorCodes.length > 0) {
            return errorHelper.error_400(res, 'validationErrors', null, {
                "CODES": errorCodes
            });
        }

        if (!withOutNewFundAgreement) {
            return errorHelper.error_400(res, 'validationErrors', null, {
                "CODES": 'DO_YOU_WANTTO_CHANGE_FA',
                errorCodes: errorCodes
            });
        }

        fundClosingData = fundClosingData.map(value => { value.createdBy = userId; value.isActive = 1; value.updatedBy = userId; return value; });

        // place the fund closing data.
        const fundClosings = await models.FundClosings.bulkCreate(fundClosingData);


        //Insert records in the change commitment table
        const fundChangeCommitments = fundClosingData.map(value => { value.isActive = 1; value.isPriorAmount = 1; value.amount = value.lpCapitalCommitment; return value; });
        await models.ChangeCommitment.bulkCreate(fundChangeCommitments)

        // update the lp status.
         
        var documentsForSignature = await models.DocumentsForSignature.findAll({
            where: {
                subscriptionId: {
                    [Op.in]: subscriptionIds,
                },
                docType: { [Op.in]: ['SUBSCRIPTION_AGREEMENT', 'FUND_AGREEMENT','SIDE_LETTER_AGREEMENT','AMENDMENT_AGREEMENT'] },
                isActive: 1,
                isPrimaryLpSigned: 1,
                isArchived:0
            }
        })

        if (documentsForSignature.length <= 0) {
            return errorHelper.error_400(res, 'documentsForSignature', 'SOMETHING WORKING WITH DOCUSIGN DOCS');
        }


        await models.FundSubscription.update({ status: 10, lpClosedDate: commonHelper.getMomentUTCDate() }, {
            where: {
                id: {
                    [Op.in]: subscriptionIds
                }
            }
        });

        let gpSignatureUrl = fund.gp.signaturePicUrl
        let gpSignaturePath = fund.gp.signaturePic        

        for (let documentForSignature of documentsForSignature) {

            //documentIds.push(documentForSignature.id);

            let documentTempLocalPath =  ""

            //making date with timezone of a fund
            let date_for_sign = await commonHelper.getDate();
            date_for_sign = commonHelper.getDateTimeWithTimezone(fund.timeZone, date_for_sign, 0, 0);

            //amendment document sign
            if (documentForSignature.docType == 'AMENDMENT_AGREEMENT') {   
                
                logger.info('******** Close Fund : AMENDMENT_AGREEMENT  ***************'+documentForSignature.id)
                       
                documentTempLocalPath = documentForSignature.filePath;

                const fundAgreementTempPath = await amendmentAgreementSign({
                    gpName: commonHelper.getFullName(req.user),
                    fundManagerTitle: fund.fundManagerTitle,
                    fundManagerCommonName: fund.fundManagerCommonName,
                    fundCommonName: fund.fundCommonName,
                    fundLegalEntity: fund.legalEntity,
                    fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                    gpSignaturePic: gpSignatureUrl,
                    date: date_for_sign
                }, documentTempLocalPath);

                let fundAgreementPath = documentForSignature.filePath

                fs.copyFileSync(fundAgreementTempPath.path,fundAgreementPath);             

                await models.DocumentsForSignature.update({
                    updatedBy: req.userId,
                    isPrimaryGpSigned: true,
                    isAllGpSignatoriesSigned: true,
                }, {
                        where: {
                            fundId: fundId,
                            subscriptionId: documentForSignature.subscriptionId,
                            docType: 'AMENDMENT_AGREEMENT'
                        }
                    });

                models.SignatureTrack.create({
                    documentType: 'AMENDMENT_AGREEMENT',
                    documentId: documentForSignature.id,
                    subscriptionId: documentForSignature.subscriptionId,
                    signaturePath: gpSignaturePath,
                    ipAddress: req.ipInfo,
                    signedUserId: req.user.id,
                    signedByType: req.user.accountType,
                    isActive: 1,
                    createdBy: req.userId,
                    updatedBy: req.userId
                })
                 


            } else if (documentForSignature.docType == 'SIDE_LETTER_AGREEMENT') { 
                
                logger.info('******** Close Fund : SIDE_LETTER_AGREEMENT  ***************'+documentForSignature.id)
                       
                const gphtmlFilePath = "./views/sideletterAgreementGPSign.html";
                let emptydata = {
                    gpName: commonHelper.getFullName(req.user),
                    fundManagerTitle: fund.fundManagerTitle,
                    fundManagerCommonName: fund.fundManagerCommonName,
                    fundCommonName: fund.fundCommonName,
                    fundLegalEntity: fund.legalEntity,
                    fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                    gpSignaturePic: gpSignatureUrl,
                    date: date_for_sign
                }
                const gpsignedLastPage = await generateSideLetterSignedPage(emptydata, gphtmlFilePath);          
                const gpfileName = `SIDE_LETTER_AGREEMENT_${uuidv1()}.pdf`;
                const outputFileGP = path.resolve(__dirname, `../assets/temp/${gpfileName}`);             
                await commonHelper.pdfMerge([documentForSignature.filePath, gpsignedLastPage.path], outputFileGP)
                fs.copyFileSync(outputFileGP,documentForSignature.filePath);

                await models.DocumentsForSignature.update({
                    updatedBy:
                    req.userId,
                    isPrimaryGpSigned: true,
                    isAllGpSignatoriesSigned: true,
                    filePath: documentForSignature.filePath
                }, {
                        where: {
                            fundId: fundId,
                            subscriptionId: documentForSignature.subscriptionId,
                            docType: 'SIDE_LETTER_AGREEMENT'
                        }
                    });

                models.SignatureTrack.create({
                    documentType: 'SIDE_LETTER_AGREEMENT',
                    documentId: documentForSignature.id,
                    subscriptionId: documentForSignature.subscriptionId,
                    signaturePath: gpSignaturePath,
                    ipAddress: req.ipInfo,
                    signedUserId: req.user.id,
                    signedByType: req.user.accountType,
                    isActive: 1,
                    createdBy: req.userId,
                    updatedBy: req.userId
                });

            } else if (documentForSignature.docType == 'SUBSCRIPTION_AGREEMENT') {

                logger.info('******** Close Fund : SUBSCRIPTION_AGREEMENT  ***************'+documentForSignature.id)

                documentTempLocalPath = documentForSignature.filePath;
                const currentCloseFundInfo = _.find(fundClosingData, { subscriptionId: documentForSignature.subscriptionId })

                const gpConsumedMoney = (parseFloat(currentCloseFundInfo.gpConsumed)).toLocaleString('en-US', {
                    style: 'currency',
                    currency: 'USD',
                });



                //const signedDateconvertedToUserTimezone = await commonHelper.setDateByTimezone(date, timeZone)

                const subscriptionAgreementTempPath = await subscriptionAgreementSign({
                    gpName: commonHelper.getFullName(fund.gp),
                    fundManagerTitle: fund.fundManagerTitle,
                    fundManagerCommonName: fund.fundManagerCommonName,
                    fundCommonName: fund.fundCommonName,
                    fundLegalEntity: fund.legalEntity,
                    fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                    gpSignaturePic: gpSignatureUrl,
                    capitalCommitment: gpConsumedMoney,
                    acceptedOn: date_for_sign,
                    fundEntityType: (fund.fundEntityType==1) ? 'Limited Partnership' : (fund.fundEntityType==2) ? 'Limited Liability company' : 'Type of Equity Holder (Limited Partner, Member),'
                }, documentTempLocalPath);

                fs.copyFileSync(subscriptionAgreementTempPath.path,documentForSignature.filePath);

                await models.DocumentsForSignature.update({
                        updatedBy: req.userId,
                        isPrimaryGpSigned: true,
                        isAllGpSignatoriesSigned: true,
                    },{
                        where: {
                            fundId: fundId,
                            subscriptionId: documentForSignature.subscriptionId,
                            docType: 'SUBSCRIPTION_AGREEMENT'
                        }
                    }
                );

                models.SignatureTrack.create({
                    documentType: 'SUBSCRIPTION_AGREEMENT',
                    documentId: documentForSignature.id,
                    subscriptionId: documentForSignature.subscriptionId,
                    signaturePath: gpSignaturePath,
                    ipAddress: req.ipInfo,
                    signedUserId: req.user.id,
                    signedByType: req.user.accountType,
                    isActive: 1,
                    createdBy: req.userId,
                    updatedBy: req.userId
                });


            } else if (documentForSignature.docType == 'FUND_AGREEMENT') {

                logger.info('******** Close Fund : FUND_AGREEMENT  ***************'+documentForSignature.id)
                
                documentTempLocalPath = documentForSignature.filePath;

                const fundAgreementTempPath = await fundAgreementSign({
                    gpName: commonHelper.getFullName(req.user),
                    fundManagerTitle: fund.fundManagerTitle,
                    fundManagerCommonName: fund.fundManagerCommonName,
                    fundCommonName: fund.fundCommonName,
                    fundLegalEntity: fund.legalEntity,
                    fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                    gpSignaturePic: gpSignatureUrl,
                    date: date_for_sign
                }, documentTempLocalPath);

                let fundAgreementPath = documentForSignature.filePath;
                if (fund.additionalSignatoryPages) {
                    const additionalSignatoryPagesTempPath = JSON.parse(fund.additionalSignatoryPages).path;
                    const fileNameTemp = `./assets/temp/Fund-Agreement-${uuidv1()}.pdf`;
                    await commonHelper.pdfMerge([fundAgreementTempPath.path, additionalSignatoryPagesTempPath], fileNameTemp);
                    fs.copyFileSync(fileNameTemp,fundAgreementPath);
                } else {
                    fs.copyFileSync(fundAgreementTempPath.path,fundAgreementPath);
                }

                await models.DocumentsForSignature.update({
                    updatedBy: req.userId,
                    isPrimaryGpSigned: true,
                    isAllGpSignatoriesSigned: true,
                }, {
                    where: {
                        fundId: fundId,
                        subscriptionId: documentForSignature.subscriptionId,
                        docType: 'FUND_AGREEMENT'
                    }
                });

                models.SignatureTrack.create({
                    documentType: 'FUND_AGREEMENT',
                    documentId: documentForSignature.id,
                    subscriptionId: documentForSignature.subscriptionId,
                    signaturePath: gpSignaturePath,
                    ipAddress: req.ipInfo,
                    signedUserId: req.user.id,
                    signedByType: req.user.accountType,
                    isActive: 1,
                    createdBy: req.userId,
                    updatedBy: req.userId
                });

            }

            eventHelper.saveEventLogInformation('Counter-Sign Fund Agreement', fundId, documentForSignature.subscriptionId, req);
            eventHelper.saveEventLogInformation('Counter-Sign Subscription Agreement', fundId, documentForSignature.subscriptionId, req);
        }

        // final closing check
        if (finalClosing === true) {
            await fundFinalClosing(fundId);
            // eventHelper.saveEventLogInformation('Final Closing', fundId, documentForSignature.subscriptionId, req);
        }


        const lpDelegatesSelected = await models.LpDelegate.findAll({
            where: {
                subscriptionId: {
                    [Op.in]: subscriptionIds
                }
            },
            include: [{
                model: models.User,
                as: 'details',
                required: false
            }]
        });

        let lpDelegates = []
        lpDelegatesSelected.map(i => {
            lpDelegates.push({
                id: i.details.id,
                firstName: i.details.firstName,
                lastName: i.details.lastName,
                middleName: i.details.middleName,
                email: i.details.email,
                isEmailNotification: i.details.isEmailNotification,
                accountId:i.details.accountId,
                subscriptionId:i.subscriptionId,
                delegateId:i.details.id
            })
        })        

        // Interest Has Been Closed - LP, LP Delegate
        // TO LP
        let capitalAccepted = [];
        for (let fundClosingDataObject of fundClosingData) {

            if(fundClosingDataObject.lpId){

                const documentsForSignatureData = await models.FundSubscription.findOne({
                    where: {
                        id: fundClosingDataObject.subscriptionId,
                        fundId: fundId
                    }
                })
    
                const CapitalCommitmentAccepted = parseFloat(fundClosingDataObject.gpConsumed).toLocaleString('en-US', {
                    style: 'currency',
                    currency: 'USD',
                });
    
                capitalAccepted.push({
                    'lpId': fundClosingDataObject.lpId,
                    'capitalCommitmentAccepted': CapitalCommitmentAccepted
                });
    
                const alertData = {
                    htmlMessage: messageHelper.interestClosedAlertMessage,
                    message: 'Interest Has Been Closed',
                    documentId: documentsForSignatureData.id,
                    docType: 'SUBSCRIPTION_AGREEMENT',
                    capitalCommitmentAcceptedPlain: fundClosingDataObject.gpConsumed,
                    CapitalCommitmentAccepted: CapitalCommitmentAccepted,
                    subscriptionId: fundClosingDataObject.subscriptionId,
                    fundManagerCommonName: fund.fundManagerCommonName,
                    fundName: fund.fundCommonName,
                    fundId: fund.id,
                    sentBy: req.userId
                };
    
                let getLpAccountInfo = await models.User.findOne({
                    attributes:['accountId'],
                    where: {
                        id: fundClosingDataObject.lpId
                    }
                })
                logger.info('******** ALERT TO CLOSED LP ID***************'+fundClosingDataObject.lpId)
                notificationHelper.triggerNotification(req, req.userId, fundClosingDataObject.lpId, lpDelegates, alertData,getLpAccountInfo.accountId,null);
            }
        }

        for (let lpDelegate of lpDelegates) {
            //const documentsForSignatureData = _.find(documentsForSignature, { subscriptionId: lpDelegate.subscriptionId, fundId: fundId })
            let documentsForSignatureData = await models.FundSubscription.findOne({
                attributes: ['id'],
                where: {
                    id: lpDelegate.subscriptionId,
                    fundId: fundId
                }
            })                   

            let capitalCommitmentAcceptedValue = _.find(fundClosingData, { subscriptionId: lpDelegate.subscriptionId }) ? _.find(fundClosingData, { subscriptionId: lpDelegate.subscriptionId }).gpConsumed : null
            capitalCommitmentAcceptedValue = parseFloat(capitalCommitmentAcceptedValue).toLocaleString('en-US', {
                style: 'currency',
                currency: 'USD',
            });

            capitalAccepted.push({
                'lpId': lpDelegate.id,
                'capitalCommitmentAccepted': capitalCommitmentAcceptedValue
            });

            const alertData = {
                htmlMessage: messageHelper.interestClosedAlertMessage,
                message: 'Interest Has Been Closed',
                documentId: documentsForSignatureData.id,
                docType: 'SUBSCRIPTION_AGREEMENT',
                CapitalCommitmentAccepted: capitalCommitmentAcceptedValue,
                subscriptionId: lpDelegate.subscriptionId,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundName: fund.fundCommonName,
                fundId: fund.id,
                sentBy: req.userId
            };

            notificationHelper.triggerNotification(req, userId, lpDelegate.delegateId, false, alertData,lpDelegate.accountId,null);
        }

        if(emailsContent){

            triggerEmailToGpAndDelegates(emailsContent, fund, investorCount)
        }

        if(lpIds.length && capitalAccepted.length){

            triggerEmailToLpAboutClose(fund, lpIds, lpDelegates, capitalAccepted, req);
        }

        consolidatedPartnershipAgreementSign(fund, gpSignatureUrl, req.file,partnershipDocumentPageCount, req);
        //sending pending action to closed user if he is not signed any penidng amendments  
        sendPendingAmendments(req,fundId,subscriptionIds)
        res.json({ data: fundClosings })

    } catch (error) {
        return next(error)
    }
}

//send pending action to closed lps if he is not signed any amendments
const sendPendingAmendments = async(req,fundId,subscriptionIds)=>{
    //ammendement docs
    let getAmendments = await models.FundAmmendment.findAll({
        attributes:['id','document','isAmmendment','changedPagesId'],
        where:{
            fundId:fundId,
            deletedAt:null,
            isActive:1,
            isAffectuated:0,
            isTerminated:0,
            isArchived:0
        }
    })
   
    //loop the docs
    for (let amendmentData of getAmendments) {
        let amendmentId = amendmentData.id
        let uploadedDocType = ''
        let sourceAmendment = amendmentData.document.path
        //uploaded type document        
        if(amendmentData.isAmmendment){            
            uploadedDocType = 'AMENDMENT_AGREEMENT'
        }else {
            uploadedDocType = 'FUND_AGREEMENT'
        }

        for (let subscriptionId of subscriptionIds) {
            let isRecordExists = await models.DocumentsForSignature.count(
                { 
                    where: {                     
                        fundId: fundId,
                        amendmentId:amendmentId,
                        subscriptionId:subscriptionId,
                        isPrimaryLpSigned:1
                    } 
                }
            )
            //send pending action and alert and email    
            if(isRecordExists==0){     
                console.log("i am pending action for cornercase ****",subscriptionId)          
                //insert records to documentsforsignatures 
                let dirToCheck = `./assets/funds/${fundId}/${subscriptionId}`
                commonHelper.ensureDirectoryExistence(dirToCheck)
                let revisedPath = dirToCheck+`/${uuidv1()}${subscriptionId}.pdf`
                fs.copyFileSync(sourceAmendment,revisedPath)                   
                //upload doc           
                let data =  {
                    fundId: fundId,
                    subscriptionId: subscriptionId,
                    filePath: revisedPath,
                    docType: uploadedDocType,
                    isAllLpSignatoriesSigned:false,
                    gpSignCount: 0,
                    lpSignCount: 0,
                    isPrimaryGpSigned: 0,
                    isPrimaryLpSigned: false,
                    isActive: 1,
                    createdBy: req.userId,
                    updatedBy: req.userId,
                    amendmentId:amendmentId,
                    changedPagesId:amendmentData.changedPagesId                    
                }
                //save to documentforsignatures
                await models.DocumentsForSignature.create(data)
                //send pending action
                sendAmendmentAlertsToInvestor(req,subscriptionId,fundId,uploadedDocType)
            }
        }
    }
}


//send alerts for non closed investors
const sendAmendmentAlertsToInvestor = async (req,subscriptionId,fundId,type) => {
 
    //get fund details
    const fund = await models.Fund.findOne({
        where: {
            id: fundId
        }
    })   
    // get subscriptions info
    let subscriptions = await models.FundSubscription.findAll({
        where: {
            id: subscriptionId,
            status: 10                     
        },
        include: [{
            model: models.User,
            as: 'lp',
            required: true
        }]
    })
    
    console.log("******subscriptions total**************",subscriptions.length)
    
    //send notification to LP
    for (let subscription of subscriptions) {    
        
        let emailSubject = (type == 'AMENDMENT_AGREEMENT') ? 'Amendment to the Fund Agreement has been issued' :  'Request for your consent to Revised Fund Agreement'

        let alertDataToLPandLPSignatories = {
            htmlMessage: type == 'AMENDMENT_AGREEMENT' ? messageHelper.AmmendmentClosedLPandLPSignatories :  messageHelper.FAClosedLPandLPSignatories,
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            fundId: fund.id,
            investorName: await commonHelper.getInvestorName(subscription),
            individualContent:  await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
            sentBy: req.userId
        }
    
        let fundAgreementChangeAlertDataToLP = type == 'AMENDMENT_AGREEMENT' ? messageHelper.AmmendmentDataToClosedLP : messageHelper.FADataToClosedLP
        fundAgreementChangeAlertDataToLP = fundAgreementChangeAlertDataToLP.replace('[fundCommonName]',fund.fundCommonName)
        fundAgreementChangeAlertDataToLP = fundAgreementChangeAlertDataToLP.replace('[fundManagerCommonName]',fund.fundManagerCommonName)
    
        let alertDatatoLP = {
            htmlMessage: fundAgreementChangeAlertDataToLP,
            firstName: subscription.lp.firstName,
            lastName: subscription.lp.lastName,
            middleName: subscription.lp.middleName,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            investorName: await commonHelper.getInvestorName(subscription),
            fundId: fund.id,
            sentBy: req.userId
        }

    let htmlFileName =   (type == 'AMENDMENT_AGREEMENT') ? 'AmmendmentDataToClosedLP.html' : 'FADataToClosedLP.html'


    //send notification to delegates       
    let lpDelegatesSelected = await models.LpDelegate.findAll({         
        where: {
            fundId: fund.id,
            lpId: subscription.lp.id,
            subscriptionId: subscription.id
        },
        include: [{
            model: models.User,
            as: 'details',
            require: true
        }]
    })   
    
    let lpDelegates = [];
    lpDelegatesSelected.map(i => {
        lpDelegates.push({
            id: i.details.id,
            firstName: i.details.firstName,
            lastName: i.details.lastName,
            middleName: i.details.middleName,
            email: i.details.email,
            isEmailNotification: i.details.isEmailNotification,
            accountId: i.details.accountId
        })
    })
    
    notificationHelper.triggerNotification(req, req.userId, false, lpDelegates, alertDataToLPandLPSignatories,null,null);

   // send email
   for (let delegate of lpDelegates) {        

    let data = {
        firstName: delegate.firstName,
        lastName: delegate.lastName,
        middleName: delegate.middleName,
        fundManagerCommonName: fund.fundManagerCommonName,
        fundCommonName: fund.fundCommonName,
        name: commonHelper.getFullName(delegate),
        clientURL: config.get('clientURL'),
        individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
    }
    
    console.log('delegate Email Sent to *************',delegate.email)
   
    emailHelper.triggerAlertEmailNotification(delegate.email, emailSubject, data, htmlFileName);
   
    }        
    //---end  
            
    
        //send notfication to LP Signatories
        let lpSignatoriesSelected = await models.LpSignatories.findAll({
            where: {
                fundId: fund.id,
                lpId: subscription.lp.id,
                subscriptionId: subscription.id
            },
            include: [{
                model: models.User,
                as: 'signatoryDetails',
                required: false
            }]
        })
    
        let lpSignatories = [];
        lpSignatoriesSelected.map(i => {
            lpSignatories.push({
                id: i.signatoryDetails.id,
                firstName: i.signatoryDetails.firstName,
                lastName: i.signatoryDetails.lastName,
                middleName: i.signatoryDetails.middleName,
                email: i.signatoryDetails.email,
                isEmailNotification: i.signatoryDetails.isEmailNotification,
                accountId: i.signatoryDetails.accountId
            })
        })    
    
        notificationHelper.triggerNotification(req, req.userId, false, lpSignatories, alertDataToLPandLPSignatories,null,null);
    
       // send email
       let htmlFileNameSignatories = (type == 'AMENDMENT_AGREEMENT') ? 'AmmendmentDataToClosedLP.html' : 'FADataToClosedLP.html'

       for (let signatory of lpSignatories) {        
    
        let data = {
            firstName: signatory.firstName,
            lastName: signatory.lastName,
            middleName: signatory.middleName,
            fundManagerCommonName: fund.fundManagerCommonName,
            fundCommonName: fund.fundCommonName,
            name: commonHelper.getFullName(signatory),
            clientURL: config.get('clientURL'),
            individualContent: await commonHelper.getIndividualContent(subscription,fund.fundCommonName),
        }
        
        console.log('Signatory Email Sent to *************',signatory.email)
       
        emailHelper.triggerAlertEmailNotification(signatory.email, emailSubject, data, htmlFileNameSignatories);
       
        }   
        
        
        //---end
    
    
        //send notification to LP start

        if(lpSignatories.length==0){
            notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDataToLPandLPSignatories,subscription.lp.accountId,null)
        } else {
            notificationHelper.triggerNotification(req, req.userId, subscription.lp.id, false, alertDatatoLP,subscription.lp.accountId,null) 
            htmlFileName = (type == 'AMENDMENT_AGREEMENT') ? 'AmmendmentClosedLPandLPSignatories.html' : 'FAClosedLPandLPSignatories.html'          
        }

        console.log('Investor Email Sent to *************',subscription.lp.email)
    
        emailHelper.sendEmail({
            toAddress: subscription.lp.email,
            subject: emailSubject,
            data: {
                firstName: subscription.lp.firstName,
                lastName: subscription.lp.lastName,
                middleName: subscription.lp.middleName,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                name: commonHelper.getFullName(subscription.lp),
                clientURL: config.get('clientURL'),
                investorName:await commonHelper.getInvestorName(subscription),
                individualContent:  await commonHelper.getIndividualContent(subscription,fund.fundCommonName)
            },
            htmlPath: 'alerts/'+htmlFileName
        });
    
        //---end
    
    }
    
}




// generate last page sined pdf from html and append to current subscription
async function subscriptionAgreementSign(data, subscriptionTempFile) {
    const htmlFilePath = "./views/subscriptionAgreementGpSign.html";
    const signedLastPage = await generateSignedPage(data, htmlFilePath);
    const fileName = `SUBSCRIPTION_AGREEMENT_${uuidv1()}.pdf`;
    return await mergeAgreementDocAndSigndPage(fileName, subscriptionTempFile, signedLastPage)

}

// generate last page sined pdf from html and append to current fund agreement
async function amendmentAgreementSign(data, fundAgreementTempFile) {
    const htmlFilePath = "./views/fundAgreementGpSign.html";
    const signedLastPage = await generateSignedPage(data, htmlFilePath);
    const fileName = `FUND_AGREEMENT_${uuidv1()}.pdf`;
    return await mergeAgreementDocAndSigndPage(fileName, fundAgreementTempFile, signedLastPage)
}

// generate last page sined pdf from html and append to current fund agreement
async function fundAgreementSign(data, fundAgreementTempFile) {
    const htmlFilePath = "./views/fundAgreementGpSign.html";
    const signedLastPage = await generateSignedPage(data, htmlFilePath);
    const fileName = `FUND_AGREEMENT_${uuidv1()}.pdf`;
    return await mergeAgreementDocAndSigndPage(fileName, fundAgreementTempFile, signedLastPage)
}

async function mergeAgreementDocAndSigndPage(fileName, agreementTempFile, lpSignedLastPage) {
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}.pdf`);
    await commonHelper.pdfMerge([agreementTempFile, lpSignedLastPage.path], outputFile);
    return { path: outputFile, name: fileName };
}

async function triggerEmailToLpAboutClose(fund, lpIds, lpDelegates, capitalAccepted, req) {

    logger.info('START******** EMAIL TO CLOSED LP***************')
    
    const lpDelegateIds = _.map(lpDelegates, 'delegateId');
    const lpAndlpDelegateIds = lpDelegateIds.concat(lpIds)

    const users = await models.User.findAll({
        attributes: ['id', 'firstName','middleName', 'lastName', 'email'],
        where: {
            id: {
                [Op.in]: lpAndlpDelegateIds
            }
        }
    });

    for (let user of users) {
        const capitalCommitmentAcceptedValue = _.find(capitalAccepted, { lpId: user.id }) ? _.find(capitalAccepted, { lpId: user.id }).capitalCommitmentAccepted : null

        if (user.isEmailNotification || req.user.accountType != 'FSNETAdministrator') { // if setting is enabled

            logger.info('******** EMAIL TO CLOSED LP***************'+user.email)

            emailHelper.sendEmail({
                toAddress: user.email,
                subject: 'Interest Has Been Closed',
                data: {
                    name: commonHelper.getFullName(user),
                    fundCommonName: fund.fundCommonName,
                    capitalCommitmentAccepted: capitalCommitmentAcceptedValue,
                    loginLink: config.get('clientURL'),
                    user
                },
                htmlPath: "InterestHasBeenClosed.html"
            })
        }
    }

    logger.info('END******** EMAIL TO CLOSED LP***************')
}

async function triggerEmailToGpAndDelegates(emailsContent, fund, investorCount) {


    let gpDelegatesAndGPIds = await models.GpDelegates.findAll({
        attributes: ['delegateId', 'gpId'],
        where: {
            fundId: fund.id,
            gpId: fund.gpId
        }
    });


    let gpDelegatesIDs=[];
    if (gpDelegatesAndGPIds.length) {
        gpDelegatesIDs = _.map(gpDelegatesAndGPIds, 'delegateId')
    }

    if(fund.isNotificationsEnabled)
        gpDelegatesIDs.push(fund.gpId)

    // gpDelegatesIDs = _.compact(_.flattenDeep(gpDelegatesIDs))

    const gpDelegates = await models.User.findAll({
        where: {
            id: {
                [Op.or]: {
                    [Op.in]: gpDelegatesIDs,
                    [Op.eq]: fund.gpId
                }

            }
        }
    })


    var html = ''
    if (emailsContent.INVESTORS_COUNT_EXCESS_99) {
        html += `<p>Please note that the documents in hand for <a href="${config.get('clientURL')}/fund/view/${fund.id}">${fund.legalEntity}</a> in status close-ready and closed would create a situation if closed upon, in that the Fund would be limited to 99 investor slots under Section 3(c)(1) of the Companies Act, and the current investor count if all investors are closed upon would be ${investorCount}.  Please consult your legal adviser.</p>`
    }
    if (emailsContent.INVESTORS_COUNT_EXCESS_499) {
        html += `<p>Please note that the documents in hand for <a href="${config.get('clientURL')}/fund/view/${fund.id}">${fund.legalEntity}</a>  in status close-ready and closed would create a situation if closed upon, in that the Fund would be limited to 499 investor slots under Section 3(c)(7) of the Companies Act, and the current investor count if all investors are closed upon would be ${investorCount}.</p>`
    }
    if (emailsContent.ERISA_WARNINGS) {
        html += `<p>Please note that the documents in hand for <a href="${config.get('clientURL')}/fund/view/${fund.id}">${fund.legalEntity}</a>  in status close-ready and closed would create a situation if closed upon, in that the Fund would have over 25% ERISA Benefit Plan money and would become subject to regulation as a Fund holding Plan Assets unless the Fund operates as a venture capital operating company.  Please consult your legal adviser.</p>`
    }
    if (emailsContent.DISQUALIFYING_EVENT) {
        html += `<p>Please note that the documents in hand for <a href="${config.get('clientURL')}/fund/view/${fund.id}">${fund.legalEntity}</a>  in status close-ready and closed indicate that one or more investors has had a Disqualifying Event.  Please consult your legal adviser.</p>`
    }
    if (emailsContent.CLOSED_INVESTORS_COUNT_EXCESS_99) {
        html += `<p>You are attempting to close a constituency of investors which would cause the Fund be in a position where it could not rely on either Section 3(c)(1) or Section 3(c)(7) of the Companies Act, and thus would likely have no viable exemption from registration under the Companies Act. This is a serious situation which is almost never advisable. Please consult your legal adviser.</p>`
    }
    if(emailsContent.EQUITY_OWNERS_EVENT){
        html += `<p>${emailsContent.fundInvestorNames} has indicated that they are under common control with another Investor which they designated as ${emailsContent.numberOfexistingOrProspectives}.  Please note that if these investors add up collectively to 10% or more of total Investor interests at the applicable closing, you should carefully review with your counsel the 1940 Act Investor Count as Vanilla will not automatically count them as related and therefore the 1940 Investor Act Count may be higher than indicated by Vanilla.  Please discuss this issue with your legal counsel if applicable.</p>`

    }

    for (let gpDelegate of gpDelegates) {
        if (gpDelegate.isEmailNotification) { // if setting is enabled
            emailHelper.sendEmail({
                toAddress: gpDelegate.email,
                subject: 'Fund Closing Validation Alert',
                data: { fund: fund, fundManagerName: commonHelper.getFullName(gpDelegate), user: gpDelegate, html: html },
                htmlPath: "fundClosingWarnnings.html"
            })
        }
    }


}


const consolidatedPartnershipAgreementSign = async (fund, gpSignaturePic, fullRregenrate,partnershipDocumentPageCount, req) => {
      
    const where = fullRregenrate ? {
        fundId: fund.id,
        docType: 'SUBSCRIPTION_AGREEMENT',
        isActive: 1,
        isPrimaryLpSigned: true,
        isPrimaryGpSigned: true,
        isArchived:0
    } : {
            fundId: fund.id,
            docType: 'SUBSCRIPTION_AGREEMENT',
            isActive: 1,
            isPrimaryLpSigned: true,
            isPrimaryGpSigned: true,
            isAddedToFundAgreement: 0,
            isArchived:0
        };

    //check amendments consolidated also    
    let amendmentDocuments =  await models.DocumentsForSignature.findAll({
        where: {
            isActive:1,
            fundId: fund.id,
            docType: 'AMENDMENT_AGREEMENT',
            isPrimaryLpSigned: true,
            amendmentId:{ 
                [Op.gt]:0
            }
        }
    })


    for (let ammendmentAgreement of amendmentDocuments) {

        effectuateHelper.consolidatedAmmendment(ammendmentAgreement.amendmentId,ammendmentAgreement,req.user)  
    }    

    //------end

    

    const documentsForSignature = await models.DocumentsForSignature.findAll({
        where: where
    });

    const subscriptionIds = _.map(documentsForSignature, 'subscriptionId');
    const documentIds = _.map(documentsForSignature, 'id');


    if (subscriptionIds.length == 0) {
        return;
    }

    const subscriptions = await models.FundSubscription.findAll({
        include: [{
            model: models.User,
            as: 'lp',
            required: false
        }, {
            model: models.SignatureTrack,
            as: 'signature',
            include: [{
                model: models.User,
                as: 'signeduser',
                required: false
            },{
                model: models.OfflineUsers,
                as: 'signedOfflineUser',
                required: false
            }],
            where: {
                signedByType: {
                    [Op.in]: ['LP', 'SecondaryLP', 'SecondaryGP','OfflineLP']
                },
                isActive: 1,
                documentType: 'FUND_AGREEMENT',
            },
            required: true
        },{
            model: models.OfflineUsers,
            as: 'offlineLp',
            required: false
        }],
        where: {
            id: {
                [Op.in]: subscriptionIds
            }
        }
    });

    let lpSignedPages = [];
    let signedTrackIds = [];
    let signedUsers = [];
    let offlineSignedUsers = [];


    for (let subscription of subscriptions) {

        let name =  subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName;

        //To avoid multiple GP signatory signs if the investor has multiple investments
        let condition = subscription.signature.signedByType === 'SecondaryGP' 
            ? !_.find(signedUsers,{signedUserId:subscription.signature.signedUserId}) && (!subscription.signature.isAddedToConsolidatedFundAgreement || fullRregenrate) && subscription.signature.signedByType !== 'OfflineLP'
                : !_.find(signedUsers,{signedUserId:subscription.signature.signedUserId,investorType:subscription.investorType,name:name })
                    && (!subscription.signature.isAddedToConsolidatedFundAgreement || fullRregenrate)
                        && subscription.signature.signedByType !== 'OfflineLP' 
               
        if (condition ) {

            if (subscription.signature.signedByType !== 'SecondaryGP' ) {

                const htmlFilePath = "./views/fundAgreementConsolidatedLpSign.html";

                const agreementSignData = {
                    name: subscription.investorType == 'LLC' ? subscription.entityName : subscription.trustName,
                    jointIndividualTitlePrettyForPDF: subscription.jointIndividualTitlePrettyForPDF,
                    investorType: subscription.investorType == 'LLC' ? 'ENTITY' : _.toUpper(subscription.investorType),
                    legalTitleDesignation: subscription.legalTitleDesignationPrettyForPDF,
                    areYouSubscribingAsJointIndividual: subscription.areYouSubscribingAsJointIndividual,
                    lpName: ( subscription.signature && subscription.signature.signeduser) ? commonHelper.getFullName(subscription.signature.signeduser) : '',
                    date: moment().format('M/D/YYYY'),
                    lpSignaturePic: commonHelper.getSignaturePicUrl(subscription.signature.signaturePath),
                    lpCapitalCommitment: subscription.lpCapitalCommitmentPretty
                }

                const signedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);
                lpSignedPages.push(signedLastPage.path);
                signedTrackIds.push(subscription.signature.id);

            }

            if (subscription.signature.signedByType === 'SecondaryGP') {

                const agreementSignData = {
                    gpName: commonHelper.getFullName(subscription.signature.signeduser),
                    fundManagerTitle: fund.fundManagerTitle,
                    fundManagerCommonName: fund.fundManagerCommonName,
                    fundCommonName: fund.fundCommonName,
                    fundLegalEntity: fund.legalEntity,
                    fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                    gpSignaturePic: commonHelper.getSignaturePicUrl(subscription.signature.signaturePath),
                    date: moment().format('M/D/YYYY')
                };

                const htmlFilePath = "./views/fundAgreementConsolidatedGpSign.html";

                const signedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);
                lpSignedPages.push(signedLastPage.path);
                signedTrackIds.push(subscription.signature.id);

            }

        } 
        if (!offlineSignedUsers.includes(subscription.signature.signedUserId) && (!subscription.signature.isAddedToConsolidatedFundAgreement || fullRregenrate)) {
            if(subscription.signature.signedByType =='OfflineLP' && subscription.signature.signedOfflineUser) {
                lpSignedPages.push(subscription.signature.signaturePath)
                signedTrackIds.push(subscription.signature.id);
            }
        } 

        subscription.signature.signedByType =='OfflineLP' ?
        offlineSignedUsers.push(subscription.signature.signedUserId)
        :signedUsers.push({signedUserId:subscription.signature.signedUserId,investorType:subscription.investorType,name:name});
    }

    if (lpSignedPages.length != 0) {

        const consolidateFundAgreement = await models.DocumentsForSignature.findOne({
            where: {
                fundId: fund.id,
                docType: 'CONSOLIDATED_FUND_AGREEMENT',
                deletedAt:null,
                isActive: 1,
                isArchived:0,
                isDocumentEffectuated:{
                    [Op.in]:[0,null]
                }
            }
        });

        let data = false;

        if (!consolidateFundAgreement || fullRregenrate) {
           
            const consolidateFundAgreementTempPath = fund.partnershipDocument.path;

            const agreementSignData = {
                gpName: commonHelper.getFullName(req.user),
                fundManagerTitle: fund.fundManagerTitle,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundCommonName: fund.fundCommonName,
                fundLegalEntity: fund.legalEntity,
                fundManagerLegalEntityName: fund.fundManagerLegalEntityName,
                gpSignaturePic: gpSignaturePic,
                date: moment().format('M/D/YYYY')
            };

            const htmlFilePath = "./views/fundAgreementConsolidatedGpSign.html";

            const gpSignedLastPage = await generateSignedPage(agreementSignData, htmlFilePath);

            data = [consolidateFundAgreementTempPath, gpSignedLastPage.path].concat(lpSignedPages);

        } else {
            const consolidateFundAgreementTempPath = consolidateFundAgreement.filePath;
            data = [consolidateFundAgreementTempPath].concat(lpSignedPages);
        }
        let filePathToSave = (consolidateFundAgreement && consolidateFundAgreement.filePath) ? consolidateFundAgreement.filePath  :`./assets/funds/${fund.id}/CONSOLIDATED_FUND_AGREEMENT_${uuidv1()}.pdf`
        const fileName = `CONSOLIDATED_FUND_AGREEMENT_${uuidv1()}.pdf`;
        const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
        await commonHelper.pdfMerge(data, outputFile)
        fs.copyFileSync(outputFile,filePathToSave)

        await models.DocumentsForSignature.findOrCreate({
            where: {
                fundId: fund.id, 
                docType: 'CONSOLIDATED_FUND_AGREEMENT', 
                isActive: 1,
                deletedAt:null,
                isArchived:0,
                isDocumentEffectuated:0
            }, 
            defaults: {
                fundId: fund.id,
                docType: 'CONSOLIDATED_FUND_AGREEMENT',
                filePath: filePathToSave,
                isPrimaryGpSigned: true,
                isPrimaryLpSigned: true,
                createdBy: req.userId,
                updatedBy: req.userId,
                originalPartnershipDocumentPageCount:partnershipDocumentPageCount,
                isActive:1,
                isArchived:0,
                isDocumentEffectuated:0
        }});
    
        await models.DocumentsForSignature.update({
            isAddedToFundAgreement: 1
        }, {
            where: {
                id: {
                    [Op.in]: documentIds
                }
            }
        });

        //Update previous consolidated FA to archived document
        await models.DocumentsForSignature.update({
            isActive: 0,
            isArchived:1

        },{
            where: {
                fundId: fund.id,
                docType: 'CONSOLIDATED_FUND_AGREEMENT',
                isDocumentEffectuated:{
                    [Op.in]:[1,null]
                }               
            }
        });


        if (signedTrackIds.length > 0) {
            await models.SignatureTrack.update({
                isAddedToConsolidatedFundAgreement: 1
            }, {
                where: {
                    id: {
                        [Op.in]: signedTrackIds
                    }
                }
            });
        }

        return filePathToSave;
    } else {
        console.log('No pages to add.');
    }



    return;

}

async function generateSignedPage(data, htmlFilePath) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}

const fundFinalClosing = async (fundId) => {

    await models.Fund.update(
        { statusId: 10 },
        { where: { id: fundId } }
    );


    let fundInvestors = await models.FundSubscription.findAll({
        attributes: ['id'],
        where: {
            status: {
                [Op.in]: [7, 1, 2]
            },
            fundId: fundId
        },
    });

    const subscriptionIds = _.map(fundInvestors, 'id')

    if (subscriptionIds.length > 0) {
        await models.FundSubscription.update({ status: 11 }, {
            where: {
                id: {
                    [Op.in]: subscriptionIds
                }
            }
        })
    }


    return;

}

async function generateSideLetterSignedPage(data, htmlFilePath) {
    const fileName = `${uuidv1()}.pdf`;
    const outputFile = path.resolve(__dirname, `../assets/temp/${fileName}`);
    let html = fs.readFileSync(htmlFilePath);
    html = html.toString();
    await commonHelper.makePDF(html, data, outputFile);
    return { path: outputFile, name: fileName };
}

//upload sideletter doc
const closeFundDocUpload = async (req, res, next) => {    
    try {

        logger.info('closeFundDocUpload Start')      

        //get the uploaded file extension name
        let filename = req.file.filename
        let ext = path.extname(filename)            

        const filePath = 'assets/temp/'+filename
        
        let url = commonHelper.getLocalUrl(filePath)  
        
        logger.info('closeFundDocUpload End')  
        
        return res.status(201).json({
            url: url
        })       
 
    }catch (e) {

        next(e);

    }
}

module.exports = {
    closeFund,
    closeFundDocUpload
}
