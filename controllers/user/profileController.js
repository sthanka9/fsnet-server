const models = require('../../models/index');
const { Op } = require('sequelize');
const _ = require('lodash');
const messageHelper = require('../../helpers/messageHelper');
const commonHelper = require('../../helpers/commonHelper');
const errorHelper = require('../../helpers/errorHelper');


const getPublicProfile = async (req, res, next) => {

    try {

        const id = Number(req.params.id);

        const accountType = req.query.accountType;
        if (!id) {
            return errorHelper.error_400(res, 'id', messageHelper.userIdNotFound);
        }
        let user ;
        if(accountType == 'OfflineLP'){
            user =  await models.OfflineUsers.findOne({
                attributes: ['id', 'firstName', 'lastName', 'middleName', 'email', 'cellNumber', 'accountType'],
                where: {
                    id
                }
            })
           
        }else{

            user = await models.User.findOne({
                attributes: ['id', 'firstName', 'lastName', 'middleName', 'email', 'cellNumber', 'accountType', 'profilePic'],
                where: {
                    id
                }
            })
        }

        if (user.accountType == 'LP') {
            user.accountType = 'Investor'
        } 
        if (user.accountType == 'GP') {
            user.accountType = 'Fund Manager'
        } 
        if (user.accountType == 'GPDelegate') {
            user.accountType = 'Delegate'
        } 
        if (user.accountType == 'FSNETAdministrator') {
            user.accountType = 'FSNET Administrator'
        } 
        if (user.accountType == 'LPDelegate') {
            user.accountType = 'Delegate'
        }
        if(user.accountType == 'OfflineLP') {
            user.accountType = 'Offline Investor'
        } 

        return res.json({ data: user })

    } catch (error) {
        next(error);
    }
}





const profileEdit = async (req, res, next) => {
    try {         
        //const accountId =  req.accountId; 

        const accountType = req.user.accountType;  

        const { userId, firstName, lastName, middleName, removedProfilePic, cellNumber, email, country, state, city, streetAddress1, streetAddress2, zipcode, primaryContact } = req.body;

        const user = await models.User.findOne({
            where: {
                id: userId
            }
        });

        if (!user) {
            return errorHelper.error_400(res, 'id', messageHelper.profileIdNotFound);
        }
        
        let accountId = user.accountId   

        // check email     
        const isValidEmail = await models.Account.findOne({
            where: {
                email: email,
                id: {
                    [Op.not] : accountId
                }
            }
        });
        
        if(isValidEmail) {
            return errorHelper.error_400(res, 'user', messageHelper.emailExists);
        }
       
        const profilePic = await commonHelper.getFileInfo(req.file)

        let accountData = {
            firstName: firstName,
            lastName: lastName,
            middleName: middleName,
            country: country,
            state: state,
            city: city,
            streetAddress1: streetAddress1,
            streetAddress2: streetAddress2,
            zipcode: zipcode,
            primaryContact: primaryContact,
            email:email,
            cellNumber:cellNumber
        };
 

        if (req.file) {
            accountData.profilePic = profilePic
        } else if (removedProfilePic) {
            accountData.profilePic = null
        }

        //update profile to all users
        await models.User.update(accountData, { where: { accountId: accountId } });       
        //update profile to account
        await models.Account.update(accountData, { where: { id: accountId } });
        //get profile pic
        let getProfilePic = await models.Account.findOne({
            attributes:['profilePic'],
            where: {
                id: accountId
            }
        })
        res.json({ msg: messageHelper.profileUpdateSuccessfully,profilePic:getProfilePic.profilePic });

    } catch (e) {
        return next(e)
    }

}


const profileDetails = async (req, res, next) => {
    try {        
        /*const accountId = req.accountId ; 
        let account = await models.Account.findOne(
            {
                attributes: ['firstName', 'lastName', 'middleName','cellNumber','email', 'city', 'state',
                    'country', 'id', 'profilePic','streetAddress1', 'streetAddress2','isImpersonatingAllowed','zipcode', 'primaryContact', 'signaturePic'], where: { id: accountId }
            }
        );*/
        const userId = req.user.accountType == 'FSNETAdministrator' ? req.params.userId : req.userId ;

        let user = await models.User.findOne(
            {
                attributes: ['firstName', 'lastName', 'middleName',
                    'email', 'cellNumber', 'city', 'state',
                    'country', 'id', 'profilePic', 'accountType',
                    'streetAddress1', 'streetAddress2','isImpersonatingAllowed',
                    'zipcode', 'primaryContact', 'signaturePic'], where: { id: userId }
            }
        );                

        if (!user) {
            return errorHelper.error_400(res, 'id', messageHelper.userIdNotFound);
        }

        return res.json(user);

    } catch (e) {
        next(e)
    }
}

module.exports = {
    profileDetails,
    profileEdit,
    getPublicProfile
}