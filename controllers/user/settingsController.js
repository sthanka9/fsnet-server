const models = require('../../models/index');
const messageHelper = require('../../helpers/messageHelper');
const errorHelper = require('../../helpers/errorHelper');

module.exports = async (req, res, next) => {

    try {
        const userId = req.userId;

        const { isImpersonatingAllowed } = req.body;

        if (isImpersonatingAllowed === undefined) {
            return errorHelper.error_400(res, 'isImpersonatingAllowed', messageHelper.isImpersonatingAllowedReq);
        }

        const user = await models.User.findOne({
            where: {
                id: userId
            }
        });

        // user not valid
        if (!user) {

            return errorHelper.error_400(res, 'user', messageHelper.userNotFound);

        }

        await models.User.update(
            { isImpersonatingAllowed: isImpersonatingAllowed ? 1 : 0 },
            { where: { id: userId } }
        );

        return res.json({
            error: false,
            message: messageHelper.userSettingsUpdated
        });
    } catch (e) {
        next(e);
    }

}