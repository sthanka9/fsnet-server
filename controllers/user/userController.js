
const models = require('../../models/index');
const bcrypt = require('bcryptjs');
const moment = require('moment');
const { Op } = require('sequelize');
const fs = require('fs');
const _ = require('lodash');
const authHelper = require('../../helpers/authHelper');
const messageHelper = require('../../helpers/messageHelper');
const errorHelper = require('../../helpers/errorHelper');
const redis = require('redis')
const uuidv1 = require('uuid/v1')
const config = require("../../config");


const getAccounts = async(req, res, next) => {

    const email = req.user.email;

    const users = await models.User.findAll({
        where: { email: email },
        include: [{
            model: models.Role,
            as: 'roles',
            required: true,
            include: [{
                model: models.Permission,
                as: 'permissions',
                required: true
            }]
        }]
    });

    return  users.map(user => {
        return {
            accountName: user.email,
            id : user.id,
            accountType: user.accountType
        }
    });

}

const impersonateUser = async (req, res, next) => {

    const userId = req.body.userId;


    const user = await models.User.findOne({
        where: { id: userId },
        include: [{
            model: models.Role,
            as: 'roles',
            required: true,
            include: [{
                model: models.Permission,
                as: 'permissions',
                required: true
            }]
        }]
    });

    if(!user) {
        return errorHelper.error_400(res, 'id', messageHelper.userIdNotFound);
    } 

    let accountId = user.accountId  
    const token = authHelper.createAuthToken(user)   

    const vcfirm = await models.VCFirm.findOne({
        where: {
            gpId: user.id
        }
    })

    
    //get list of roles of users
    let listallusers = await models.User.findAll({
        where: { email: user.email, deletedAt: null },
        include: [{
            model: models.Role,
            as: 'roles',
            required: true        
        }]
    })

    listallusers = listallusers.map(userdetails=>{
        return {                
            firstName: userdetails.firstName,
            lastName: userdetails.lastName,
            middleName: userdetails.middleName,
            id: userdetails.id,                             
            cellNumber: userdetails.cellNumber,
            accountType: userdetails.accountType,
            profilePic: userdetails.profilePic
        }
    })   
    
    //last login update
    models.Account.update({ lastLoginAt: new Date().toISOString() }, { where: { id: accountId } });


    return res.json({
        token: token,
        user: {
            firstName: user.firstName,
            lastName: user.lastName,
            middleName: user.middleName,
            id: user.id,
            cellNumber: user.cellNumber,
            accountType: user.accountType,
            profilePic: user.profilePic,
            vcfirmId: vcfirm ? vcfirm.id : null,
            accountid:user.accountId
        },
        allusers: listallusers
    });

}

// unlock users
const unlockUsers = async (req, res, next) => {
    try {
        await models.User.update({ loginFailAttempts: 0, loginFailAttemptsAt: null }, {
            where: {
                loginFailAttemptsAt: {
                    [Op.lte]: moment().subtract(1, 'hours').toISOString()
                }
            }
        });
        return res.send('ok');
    } catch (e) {
        return next(e)
    }

}

const getInvitationData = async (req, res, next) => {
    try {
        //email confirm code
        const { id } = req.params;
        const account = await models.Account.findOne({ where: { emailConfirmCode: id } })
        if (!account) {
            return errorHelper.error_400(res, 'id', messageHelper.inValidConfirmCode);
        }
        if (!account.emailConfirmCode) {
            return errorHelper.error_400(res, 'id', messageHelper.confirmCodeAlreadyUsed);
        }
 
        return res.json({
            data: {
                firstName: account.firstName,
                lastName: account.lastName,
                middleName: account.middleName,
                email: account.email,
                cellNumber: account.cellNumber
            }
        })
        

    } catch (e) {
        return next(e)
    }

}

const signaturePic = async (req, res, next) => {

    try {
        const userId = req.userId;

        // to create some random id or name for your image name
        const imgname = Date.now() + "_" + Math.random().toString(26).slice(2)

        // to declare some path to store your converted image
        const path = './assets/signatures/'+imgname+'.png'

        // image takes from body which you uploaded
        const imgdata = req.body.signaturePic;

        // to convert base64 format into random filename
        const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
        
        fs.writeFile(path, base64Data,  {encoding: 'base64'}, (err) => {            
            console.log(err);
        });

        const newSignaturePic = {            
            name: imgname+'.png',            
            uri: '/assets/signatures/'+imgname+'.png',
            path: path
        };

        await models.User.update( { signaturePic: './assets/signatures/'+imgname+'.png' },{ where: { id: userId } }
        )       

        /*
        await models.User.update(
            { signaturePic: JSON.stringify(newSignaturePic) },
            { where: { id: userId } }
        );*/

        return res.json({
            error: false,
            message: messageHelper.signaturePic_added
        });

    } catch (e) {
        next(e);
    }
}

const verifySignaturePassword = async (req, res, next) => {

    try {

        const userId = req.userId;

        const { signaturePassword } = req.body;

        const user = await models.User.findOne({
            where: {
                id: userId
            }
        });

        // user not valid
        if (!user) {
            return errorHelper.error_400(res, 'id', messageHelper.userNotFound);
        }

        let accountId = user.accountId
        
        const account = await models.Account.findOne({
            where: {
                id: accountId
            }
        });
        
        if (!account) {
            return errorHelper.error_400(res, 'id', messageHelper.userNotFound);
        }           

        // password is invalid. Check signature password with user password
        if (!bcrypt.compareSync(signaturePassword, account.password)) {
            return errorHelper.error_400(res, 'signaturePassword', messageHelper.old_password_not_match);
        } else {

            return res.json({
                error: false,
                message: messageHelper.signaturePassword_verified
            });

        }

    } catch (e) {
        next(e);
    }
}

const setPassword = async(req, res, next) => {
    const { code, id} = req.params; 
    const { password } = req.body;

    const salt = bcrypt.genSaltSync(10);
    const passwordHash = bcrypt.hashSync(password, salt);

    const findCode = await models.Account.findOne({
        where:{
            id, emailConfirmCode: code
        }
    })
    if(!code || !findCode){
        errorHelper.error_400(res,false,messageHelper.password_link_expired)

    }

    //confirm email for other users
    await models.User.update({ isEmailConfirmed: 1 }, { where: { email:findCode.email } })

    // update the password
    await models.Account.update(
        { password: passwordHash },
        { where: { id, emailConfirmCode: code } }
    );

    //Reset the email confirmation code
    await models.Account.update({
        emailConfirmCode:'',
        isEmailConfirmed: 1
    },{
        where:{
            id
        }
    })

    return res.json({
        error: false,
        message: messageHelper.password_updated
    });

}

module.exports = {
    signaturePic,
    getInvitationData,
    unlockUsers,
    verifySignaturePassword,
    impersonateUser,
    getAccounts,
    setPassword
}