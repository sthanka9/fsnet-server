const models = require('../models/index');
const moment = require('moment');
const reIssueSubscriptionAgreementForSubscription  = require('./subscription/reIssueSubscriptionAgreementForSubscription');
const reIssueSubscriptionAgreementForFund  = require('./subscription/reIssueSubscriptionAgreementForFund');
const commonHelper = require('../helpers/commonHelper');
const subscriptionSections = commonHelper.subscriptionSections;

module.exports.reIssueSubscriptionAgreement = async (req, res, next) => {

    try {

        const { subscriptionId, fundId } = req.params;

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        })      

        if (subscriptionId != 0) {     
            const subscriptions = await reIssueSubscriptionAgreementForFund.run(fundId, req,true,'subscription',subscriptionId);
            reIssueSubscriptionAgreementForFund.sendSubscriptionChangeAlertToFundInvestors(subscriptions, fund, req);
            return res.send('ok');            
            //await reIssueSubscriptionAgreementForSubscription(subscriptions, req);
            //return res.send('ok');
        } else {
            const subscriptions = await reIssueSubscriptionAgreementForFund.run(fundId, req,true,'subscription',0);
            reIssueSubscriptionAgreementForFund.sendSubscriptionChangeAlertToFundInvestors(subscriptions, fund, req);
            return res.send('ok');
        }

        
    } catch (e) {
        next(e);
    }

}

module.exports.update = async (req, res, next) => {

    try {

        const { sectionId, fundId, subscriptionId } = req.params;

        let { content } = req.body;

        let data = {};

        if (subscriptionId != 0) {

            const fundSubscription = await models.FundSubscription.findOne({
                where: {
                    id: subscriptionId
                }
            });

            if (fundSubscription.subscriptionHtml) {
                data = fundSubscription.subscriptionHtml;
            } else {
                const fund = await models.Fund.findOne({
                    where: {
                        id: fundId
                    }
                });

                if (fund.subscriptionHtml) {
                    data = fund.subscriptionHtml;
                }
            }

            data[sectionId] = content;

            data = { ...subscriptionSections, ...data };

            await models.FundSubscription.update({ subscriptionHtml: data, subscriptionHtmlLastUpdated: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z') }, {
                where: {
                    id: subscriptionId
                }
            });

        } else {

            const fund = await models.Fund.findOne({
                where: {
                    id: fundId
                }
            });

            if (fund.subscriptionHtml) {
                data = fund.subscriptionHtml;
            }

            data[sectionId] = content;

            data = { ...subscriptionSections, ...data };

            await models.Fund.update({ subscriptionHtml: data, subscriptionHtmlLastUpdated: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z') }, {
                where: {
                    id: fundId
                }
            });
        }


        return res.json(data);
        
    } catch (e) {
        next(e)
    }
}


module.exports.restoreToDefault = async (req, res, next) => {

    try {

        const { sectionId, fundId, subscriptionId } = req.params;

        let data = {};

        if (subscriptionId != 0) {

            const fundSubscription = await models.FundSubscription.findOne({
                where: {
                    id: subscriptionId
                }
            });

            if (fundSubscription.subscriptionAgreementPath) {
                data = fundSubscription.subscriptionAgreementPath;
            } else {
                const fund = await models.Fund.findOne({
                    where: {
                        id: fundId
                    }
                });

                if (fund.subscriptionAgreementPath) {
                    data = fund.subscriptionAgreementPath;
                }
            }

            delete data[sectionId];

            await models.FundSubscription.update({ subscriptionAgreementPath: data, subscriptionHtmlLastUpdated: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z') }, {
                where: {
                    id: subscriptionId
                }
            });

        } else {

            const fund = await models.Fund.findOne({
                where: {
                    id: fundId
                }
            });

            if (fund.subscriptionAgreementPath) {
                data = fund.subscriptionAgreementPath;
            }

            delete data[sectionId];
            await models.Fund.update({ subscriptionAgreementPath: data, subscriptionHtmlLastUpdated: moment().format('YYYY-MM-DD HH:mm:ss.ssss Z') }, {
                where: {
                    id: fundId
                }
            });

        }


        return res.json({
            info: data,
            data
        });
    } catch (e) {
        next(e)
    }

}

module.exports.get = async (req, res, next) => {
    try {
        const { sectionId, fundId, subscriptionId } = req.params;

        let defaultHtml = subscriptionSections;

        if (subscriptionId != 0) {

            const subscription = await models.FundSubscription.findOne({
                attributes: ['subscriptionHtml','fundId'],
                where: {
                    id: subscriptionId
                }
            });

            const fund = await models.Fund.findOne({
                attributes: ['subscriptionHtml'],
                where: {
                    id: subscription.fundId
                }
            });

           const data = (subscription.subscriptionHtml ? subscription.subscriptionHtml : (fund.subscriptionHtml ? fund.subscriptionHtml : defaultHtml));

            if (sectionId != 'all') {
                return res.send(data[sectionId]);
            }

            return res.json(data);

        } else {
            const fund = await models.Fund.findOne({
                attributes: ['subscriptionHtml'],
                where: {
                    id: fundId
                }
            });

            const data = fund.subscriptionHtml ? fund.subscriptionHtml : defaultHtml;

            if (sectionId) {
                return res.send(data[sectionId]);
            }

            return res.json(data);
        }

    } catch (e) {
        next(e)
    }

}