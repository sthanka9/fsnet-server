
const models = require('../models/index');
const _ = require('lodash');
const { Op } = require('sequelize');
const config = require('../config/index');
const moment = require('moment');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const errorHelper = require('../helpers/errorHelper');

const getAmendmentName = async (amendmentId) => {
    let name = ''
    let fundAmendment = await models.FundAmmendment.findOne({
        attributes:['document'],
        where: {
            id:amendmentId
        }    
    })
    if(fundAmendment){
        name = fundAmendment.document.originalname
    }
    return name
}

//get all lpsigned pending documents
const getAllLpSignedPendingDocuments = async (req,subscription) => {
let docs = []
let signedDocs = []
let lpSigneDocuments = {}
let DocumentsForSignature = await models.DocumentsForSignature.findAll({
    attributes:['id','amendmentId','docType','createdAt'],
    where: {
        subscriptionId:subscription.id,
        isPrimaryGpSigned:0,
        isActive:1,
        isArchived:0,
        deletedAt:null
    }  
})
for (let documentForSignature of DocumentsForSignature) {
    let tempAgreement = {}
    let filename = ''
    if(documentForSignature.docType=='AMENDMENT_AGREEMENT'){        
        filename = await getAmendmentName(documentForSignature.amendmentId)
    }    
    tempAgreement.url = `${config.get('serverURL')}/api/v1/document/view/docs/${documentForSignature.id}` 
    tempAgreement.filename = filename
    tempAgreement.id = documentForSignature.id
    tempAgreement.docType = documentForSignature.docType
    tempAgreement.createdAt = documentForSignature.createdAt
    docs.push(tempAgreement)               
}
let signatureTrack = await models.SignatureTrack.findAll({
    attributes: ['documentId','documentType'],
    where: {
        subscriptionId:subscription.id,
        signedUserId:req.userId
    }
})
for (let documentForSignature of signatureTrack) {
    let tempAgreement = {}  
    tempAgreement.documentId = documentForSignature.documentId
    tempAgreement.documentType = documentForSignature.documentType
    tempAgreement.url = `${config.get('serverURL')}/api/v1/document/view/docs/${documentForSignature.documentId}` 
    signedDocs.push(tempAgreement)               
} 

 

let saDocument = _.find(docs,{docType:'SUBSCRIPTION_AGREEMENT'})
let faDocument = _.find(docs,{docType:'FUND_AGREEMENT'})
let slDocument = _.find(docs,{docType:'SIDE_LETTER_AGREEMENT'})                    
let atDocumentAll = _.filter(docs,{docType:'AMENDMENT_AGREEMENT'})
let ccDocument = _.find(docs,{docType:'CHANGE_COMMITMENT_AGREEMENT'})   

  
if(saDocument){
    let Doc = _.find(signedDocs,{'documentId':saDocument.id.toString()})
    if(Doc){
        lpSigneDocuments.subscriptionData = Doc
    }
}

if(faDocument){
    let Doc = _.find(signedDocs,{'documentId':faDocument.id.toString()})
    if(Doc){
        lpSigneDocuments.fundData = Doc
    }
}

if(atDocumentAll){ 
    let amendmentDocs = []
    for (let atDocument of atDocumentAll) {
        let Doc = _.find(signedDocs,{'documentId':atDocument.id.toString()})        
        if(Doc){
            Doc.filename = atDocument.filename
            amendmentDocs.push(Doc)
        }       
    }
    if(amendmentDocs.length>0){
        lpSigneDocuments.amendmentData = amendmentDocs
    }
}

if(slDocument){
    let Doc = _.find(signedDocs,{'documentId':slDocument.id.toString()})
    if(Doc){
        lpSigneDocuments.sideLetterData = Doc
    }
}

if(ccDocument){
    let Doc = _.find(signedDocs,{'documentId':ccDocument.id.toString()})
    if(Doc){
        lpSigneDocuments.capitalCommitmentData = Doc
    }
} 

return lpSigneDocuments

}

const get = async (req, res, next) => {

    try {

        const { subscriptionId } = req.params;
        let subscription = await models.FundSubscription.findOne({
            where: {
                id: subscriptionId,
                deletedAt: null
            },
            include: [{
                model: models.Fund,
                as: 'fund',
                required: true,
                include: [{
                    model: models.InvestorQuestions,
                    as: 'investorQuestions',
                    where: {
                        deletedAt: null
                    },
                    required: false,
                    include: [{
                        model: models.InvestorAnswers,
                        as: 'answers',
                        where: {
                            subscriptionId: subscriptionId,
                            deletedAt: null
                        },
                        required: false
                    }]

                }]
            }, {
                attributes: ['id', 'name'],
                model: models.FundStatus,
                as: 'subscriptionStatus',
                required: true
            }, {
                model: models.ChangeCommitment,
                as: 'commitmentHistory',
                required: false
            },{
                model: models.OfflineUsers,
                as:'offlineLp',
                required:false

            }]
        });

        if (!subscription) {
            return res.status(404).json({
                "errors": [
                    {
                        "msg": 'subscription not found with given id.'
                    }
                ]
            });
        }


        subscription = subscription.toJSON()

        const investorQuestions = subscription.fund.investorQuestions

        const result = investorQuestions.map(function (element) {
            const questionResponse = (element.answers) ? element.answers.questionResponse : null;
            return {
                id: element.id,
                fundId: element.fundId,
                questionTitle: element.questionTitle,
                question: element.question,
                typeOfQuestion: element.typeOfQuestion,
                isRequired: element.isRequired,
                deletedAt: element.deletedAt,
                createdAt: element.createdAt,
                updatedAt: element.updatedAt,
                questionResponse: questionResponse
            }
        })

        subscription.investorQuestions = result


        subscription.offlineLp = subscription.offlineLp;
        subscription.lpSignatories = await _getLpSignatoires(subscription);
        subscription.email = subscription.isOfflineInvestor ? subscription.offlineLp.email : req.user.email;

        const subscriptionAgreement = await models.DocumentsForSignature.findOne({
            where: {
                isActive: 1,
                subscriptionId: subscription.id,
                docType: 'SUBSCRIPTION_AGREEMENT'
            },
            include: [{
                model: models.SignatureTrack,
                as: 'signatureTrack',
                docType: 'SUBSCRIPTION_AGREEMENT',
                where: {
                    isActive: 1
                },
                required: false
            }]
        });

        //list of pending documents
        let allPendingDocs = await getAllLpSignedPendingDocuments(req,subscription)        
        subscription.lpSignedPendingDocuments = allPendingDocs    
         
        //---end

        const fundAgreementAll = await models.DocumentsForSignature.findAll({
            where: {
                isActive: 1,
                subscriptionId: subscription.id,
                docType: 'FUND_AGREEMENT'                
            },
            include: [{
                model: models.SignatureTrack,
                as: 'signatureTrack',
                docType: 'FUND_AGREEMENT',
                where: {
                    isActive: 1
                },
                required: false
            }],
           
        });

        const fundAgreement = _.findLast(fundAgreementAll,{docType: 'FUND_AGREEMENT'}) 

        //sideletter agreement       

        const sideletterAgreement = await models.DocumentsForSignature.findOne({
            where: {
                isActive: 1,
                subscriptionId: subscription.id,
                docType: 'SIDE_LETTER_AGREEMENT'
            },
            include: [{
                model: models.SignatureTrack,
                as: 'signatureTrack',
                docType: 'SIDE_LETTER_AGREEMENT',
                where: {
                    isActive: 1
                },
                required: false
            }]
        });

        // capital commitment
        const ccAgreement = await models.DocumentsForSignature.findOne({
            where: {
                isActive: 1,
                subscriptionId: subscription.id,
                docType: 'CHANGE_COMMITMENT_AGREEMENT'
            },
            include: [{
                model: models.SignatureTrack,
                as: 'signatureTrack',
                docType: 'CHANGE_COMMITMENT_AGREEMENT',
                where: {
                    isActive: 1
                },
                required: false
            }]
        });        

        subscription.subscriptionAgreementUrl = subscriptionAgreement && subscriptionAgreement.lpSignCount > 0
            ? commonHelper.getLocalUrl(subscriptionAgreement.filePath)
            : `${config.get('serverURL')}/api/v1/document/view/sa/${subscription.id}`;

        subscription.fundAgreementUrl = fundAgreement && fundAgreement.lpSignCount > 0
            ? commonHelper.getLocalUrl(fundAgreement.filePath)
            : `${config.get('serverURL')}/api/v1/document/view/fa/${subscription.id}`;



        const signatureTrack = _.get(subscriptionAgreement, 'signatureTrack', false)
        let alldocssigned = Boolean(_.find(signatureTrack, { signedUserId: req.userId }))

        if (ccAgreement) {
            let ccletteragreement = {}   

            const ccTrack = _.get(ccAgreement, 'signatureTrack', false)
            alldocssigned = Boolean(_.find(ccTrack, { signedUserId: req.userId }))
            ccletteragreement.filePathTempPublicUrl = ccAgreement && ccAgreement.lpSignCount > 0
                ? commonHelper.getLocalUrl(ccAgreement.filePath)
                : `${config.get('serverURL')}/api/v1/document/view/cc/${subscription.id}`;
            ccletteragreement.id = ccAgreement.id
            ccletteragreement.isCurrentUserSigned = Boolean(_.find(ccTrack, { signedUserId: req.userId }));
            subscription.ccAgreement = ccletteragreement
        }
                

        if (sideletterAgreement) {
            let sideletteragreement = {}

            const sideletterTrack = _.get(sideletterAgreement, 'signatureTrack', false)
            alldocssigned = Boolean(_.find(sideletterTrack, { signedUserId: req.userId }))
            sideletteragreement.sideLetterUrl = sideletterAgreement && sideletterAgreement.lpSignCount > 0
                ? commonHelper.getLocalUrl(sideletterAgreement.filePath)
                : `${config.get('serverURL')}/api/v1/document/view/sia/${subscription.id}`;
            sideletteragreement.id = sideletterAgreement.id
            sideletteragreement.isCurrentUserSigned = Boolean(_.find(sideletterTrack, { signedUserId: req.userId }));
            subscription.sideletterAgreement = sideletteragreement
        }
        //check fund document signed or not
        if(fundAgreement){
            let faTrack = _.get(fundAgreement, 'signatureTrack', false)
            alldocssigned = Boolean(_.find(faTrack, { signedUserId: req.userId }))            
        }

        subscription.isCurrentUserSigned = alldocssigned

        let isAllSignatoriesSigned = subscriptionAgreement ? subscriptionAgreement.lpSignCount === subscription.noOfSignaturesRequired : false

        //check with fa also
        if(!isAllSignatoriesSigned){
            isAllSignatoriesSigned = fundAgreement ? fundAgreement.lpSignCount === subscription.noOfSignaturesRequired : false
        }

        //check all signatories signed, then only display the pending docs
        if(req.user.accountType == 'LP'){

            const signatoriesCheck = (subscription.noOfSignaturesRequired > 0) ? true : false;

            let isLPSignatoriesSigned = await models.DocumentsForSignature.count({
                where: {                        
                    subscriptionId: subscriptionId,  
                    isActive: 1,    
                    deletedAt:null,
                    isAllLpSignatoriesSigned:0
                }
            })
            
            if(isLPSignatoriesSigned>0 && signatoriesCheck){
                isAllSignatoriesSigned = false
            }
        }        



        subscription.isAllSignatoriesSigned = isAllSignatoriesSigned

        let isPrimaryLpSigned = _.get(subscriptionAgreement, 'isPrimaryLpSigned', false)

        //check user signed on fa also
        if(isPrimaryLpSigned){
            isPrimaryLpSigned = _.get(fundAgreement, 'isPrimaryLpSigned', false)
        }
        
        subscription.isPrimaryLpSigned = isPrimaryLpSigned
        return res.json({ data: subscription });

    } catch (e) {
        next(e)
    }
}


const _getLpSignatoires = async (subscription) => {
    // get lp signatories from firm
    let lpSignatories = await models.VcFirmSignatories.findAll({
        where: {
            vcfirmId: subscription.lpId,
            type: 'SecondaryLP'
        },
        include: [{
            model: models.User,
            as: 'signatoryDetails',
            required: true
        }, {
            model: models.LpSignatories,
            as: 'lpSignatoriesSelected',
            required: false,
            where: {
                subscriptionId: subscription.id,
                fundId: subscription.fundId
            }
        }]
    });


    return lpSignatories.map(signatory => {
        return {
            id: signatory.signatoryDetails.id,
            firstName: signatory.signatoryDetails.firstName,
            lastName: signatory.signatoryDetails.lastName,
            middleName: signatory.signatoryDetails.middleName,
            email: signatory.signatoryDetails.email,
            accountType: signatory.signatoryDetails.accountType,
            profilePic: signatory.signatoryDetails.profilePic,
            selected: signatory.lpSignatoriesSelected ? true : false,
            signatory: signatory  // only for testing, need to remove.
        };
    })
}

const createFormOtherSubscription = async (req, res, next) => {

    const { subscriptionId } = req.body

    let subscription = await models.FundSubscription.findOne({
        where: {
            id: subscriptionId
        }
    });

    let newSubscription = await models.FundSubscription.create({
        lpId: subscription.lpId,
        fundId: subscription.fundId,
        invitedDate : moment().format('MM-DD-YYYY'),
        status: 2,
        isPrimaryInvestment : 0,
        isInvestorInvited : 2,
        createdBy: req.userId,
        updatedBy: req.userId,

    });

    return res.json({
        message: 'New subscription created.',
        subscription: newSubscription
    })
}

const softDeleteSubscription = async (req, res) => {

    const { subscriptionId, lpId, fundId } = req.body;
    let deleteClosedLpConfirm = false

    if (!Number(subscriptionId)) {
        return res.json({
            error: true,
            message: 'Subscription Id is missing.'
        });
    }

    // get investor status
    let subscription = await models.FundSubscription.findOne({
        where: {
            id: subscriptionId
        }
    });

    if (subscription && subscription.status == 10 && deleteClosedLpConfirm === false) {

        return errorHelper.error_400(res, 'status', messageHelper.fundIdValidate, {
            "isClosed": true
        });

    }


    Promise.all([
        models.FundSubscription.destroy({
            where: {
                id: subscriptionId
            }
        }),
        models.ChangeCommitment.destroy({
            where: {
                subscriptionId: subscriptionId
            }
        }),
        models.DocumentsForSignature.destroy({
            where: {
                subscriptionId: subscriptionId
            }
        }),
        models.SignatureTrack.destroy({
            where: {
                subscriptionId: subscriptionId
            }
        }),
        models.FundClosings.destroy({
            where: {
                subscriptionId: subscriptionId
            }
        }),
        models.History.destroy({
            where: {
                userId: lpId,
                fundId: fundId
            }
        })   
    ]
    ).then(async() => {
        const subscription = await models.FundSubscription.findOne({
            where :{
                fundId: fundId,
                lpId : lpId
            }
        });
        return res.json({
            data: true,
            subscriptions: subscription ? true : false
        });
    })
}

const deleteSubscription = async (req, res) => {

    const { subscriptionId } = req.body;

    if (!Number(subscriptionId)) {
        return res.json({
            error: true,
            message: 'Subscription Id is missing.'
        });
    }

    await models.FundSubscription.destroy({
        paranoid: false,
        where: {
            id: subscriptionId
        }
    })

    return res.json({
        data: true
    });

}

module.exports = {
    get,
    createFormOtherSubscription,
    deleteSubscription,
    softDeleteSubscription
}