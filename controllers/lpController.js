const models = require('../models/index');
const { Op } = require('sequelize');
const _ = require('lodash');
const config = require('../config/index');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const notificationHelper = require('../helpers/notificationHelper');

const getAllLpsList = async(req,res,next)=>{
    try{

        const {vcfirmId,fundId} = req.params;

        const fundSubscriptions = await models.FundSubscription.findAll({
            where:{
                fundId
            },
            order: [['updatedAt', 'DESC']],
            include: [{
                model: models.User,
                as: 'lp',
                required: true
            }]
        });

        const assignedLpsObject =[];
        fundSubscriptions.map(fundSubscription=>{
            assignedLpsObject.push({
                firstName:fundSubscription.lp.firstName,
                lastName:fundSubscription.lp.lastName,
                middleName:fundSubscription.lp.middleName,
                profilePic:fundSubscription.lp.profilePic,
                organizationName:fundSubscription.lp.organizationName,
                lpId:fundSubscription.lp.id,
                trackerTitle :((fundSubscription.investorType == 'LLC' ? fundSubscription.entityName :
                (fundSubscription.investorType == 'Trust' ? fundSubscription.trustName : null)) || (fundSubscription.organizationName ? fundSubscription.organizationName : fundSubscription.lp.organizationName) ||
                commonHelper.getFullName(fundSubscription.lp)),
                subscriptionId:fundSubscription.id
            })
        })
        const lpIdsAssigned = _.map(assignedLpsObject,'lpId');

        const lpsList = await models.VcFrimLp.findAll({
            where:{
                vcfirmId,
                lpId:{
                    [Op.notIn]:lpIdsAssigned
                },
                isDeleted: 0,
            },
            order: [['updatedAt', 'DESC']],
            include: [{
                model: models.User,
                as: 'details',
                required: true
            }]
        });

        const notAssignedLpsObject =[];
        lpsList.map(lpObject=>{
            notAssignedLpsObject.push({
                firstName:lpObject.details.firstName,
                lastName:lpObject.details.lastName,
                middleName:lpObject.details.middleName,
                profilePic:lpObject.details.profilePic,
                organizationName:lpObject.details.organizationName,
                lpId:lpObject.details.id
            })
        })

        return res.json({
            notAssignedLps:notAssignedLpsObject,
            assignedLps:assignedLpsObject
        });
    }catch(e){
        next(e);
    }
}

const assignLps = async(req,res,next)=>{
    try{
        let {assignedLps,notAssignedLps,fundId} = req.body;

        const fund = await models.Fund.findOne({
            where: {
                id: fundId
            }
        }); 

        const notAssignedLpIds = _.map(notAssignedLps,'id');
        await models.FundSubscription.destroy({
            paranoid: false,
            where: {
                lpId: {
                    [Op.in]: notAssignedLpIds
                },
                fundId: fundId,

            }
        });

        await models.FundLp.destroy({
            paranoid: false,
            where: {
                lpId: {
                    [Op.in]: notAssignedLpIds
                },
                fundId: fundId
            }
        })

        let previouslyUncheckedSubscriptions = await models.FundSubscription.findAll({
            attributes:['lpId'],
            paranoid: false,
            where: {
                deletedAt:{
                    [Op.ne]:null
                },
                status:10,
                fundId: fundId
            }            
        });

        previouslyUncheckedSubscriptions = _.map(previouslyUncheckedSubscriptions,'lpId')

        assignedLps = _.difference(assignedLps,previouslyUncheckedSubscriptions);

        // restore if reslected lp
        await models.FundLp.update({ deletedAt: null }, {
            paranoid: false, where: {
                lpId: {
                    [Op.in]: previouslyUncheckedSubscriptions
                },
                fundId
            }
        });

        const lpsToAssignForSubscription=[];
        const lpsToAssignForFund=[];
        assignedLps.map(assignedLp=>{
            lpsToAssignForSubscription.push({
                lpId: assignedLp.id,
                fundId: fundId,
                createdBy: userId,
                updatedBy: userId,
                organizationName:assignedLp.organizationName
            });
            lpsToAssignForFund.push({
                lpId: assignedLp.id,
                fundId: fundId,
                createdBy: userId,
                updatedBy: userId
            });
        });

        // add new lps to fundLps
        await models.FundLp.bulkCreate(lpsToAssignForFund);
        await models.FundSubscription.bulkCreate(lpsToAssignForSubscription);

        
        if (fund.statusId == 13 && assignedLps.length) { // 'Open-Ready' 13
            // GP has invited you to join fund
            const alertData = {
                htmlMessage: messageHelper.invitationToJoinParticularFundAlertMessage,
                message: 'has invited to join the fund',
                subscriptionId: fundInvestorsId.id,
                fundManagerCommonName: fund.fundManagerCommonName,
                fundName: fund.fundCommonName,
                fundId: fund.id,
                sentBy: req.userId,
                type:'FUND_INVITED'
            };

            //get all lps account id
            let listAllLps = await models.User.findAll({
                attributes:['id','accountId'],
                where: {
                    id: {
                        [Op.in]: assignedLps
                    }
                }
            })   
            
            //get all users account info start
            let accountsinfo = {}
            for(let allaccounts of listAllLps){                
                accountsinfo[allaccounts.id] = allaccounts.accountId
            }                    
          
            //---end               

            for (let assignedLp of assignedLps) {
                if(!assignedLp.isOffline){
                    notificationHelper.triggerNotification(req, req.userId, assignedLp.id, false, alertData,allaccounts[assignedLp.id],null);
                }
            }

            triggerEmailNotifictionsTolPs(assignedLps, fund);
        }


        return res.json(true)
    }catch(e){
        next(e);
    }
}


function triggerEmailNotifictionsTolPs(lpIds, fund) {

    if (!lpIds.length) return;

    lpIds = _.filter(lpIds,lpId=>{
        return !lpId.isOffline
    })

    models.User.findAll({
        where: {
            id: {
                [Op.in]: lpIds
            }
        }
    }).then(users => {

        if (!users) return;

        //get all users account info start
        let accountsinfo = {}
        let lpinvestorsaccountids = []
        users.forEach(async function (tempuser) {            
            lpinvestorsaccountids.push(tempuser.accountId)            
            let getallaccountsinfo = await models.Account.findAll({
                attributes: ['id', 'emailConfirmCode', 'isEmailConfirmed','isEmailNotification'],
                where:{ id : { [Op.in]: lpinvestorsaccountids } }
            })
            for(let allaccounts of getallaccountsinfo){
                let tempobj = {}
                tempobj.emailConfirmCode = allaccounts.emailConfirmCode
                tempobj.isEmailConfirmed = allaccounts.isEmailConfirmed
                tempobj.isEmailNotification = allaccounts.isEmailNotification
                accountsinfo[allaccounts.id] = tempobj
            }                    
        })
        //---end        

        users.forEach(function (user) {

            const loginOrRegisterLink = accountsinfo[user.accountId].isEmailConfirmed ? `${config.get('clientURL')}/login` : `${config.get('clientURL')}/register/${accountsinfo[user.accountId].emailConfirmCode}`;
            const content = user.isEmailConfirmed ? "to login and get going." : "to setup your account and get going." 

            emailHelper.sendEmail({
                toAddress: user.email,
                subject: messageHelper.addLPEmailSubject,
                data: {
                    name: commonHelper.getFullName(user),
                    fundCommonName: fund.fundCommonName,
                    loginOrRegisterLink: loginOrRegisterLink,
                    content,
                    user
                },
                htmlPath: "fundLPInviation.html"
            }).catch(error => {
                commonHelper.errorHandle(error);
            })


        });
    }).catch(error => {
        commonHelper.errorHandle(error);
    })
}

module.exports={
    getAllLpsList,
    assignLps
}