
const models = require('../models/index')
const { Op } = require('sequelize')
const config = require('../config');
const messageHelper = require('../helpers/messageHelper');
const commonHelper = require('../helpers/commonHelper');
const emailHelper = require('../helpers/emailHelper');
const errorHelper = require('../helpers/errorHelper');
const lpProceedToSignController = require('../controllers/subscription/lpProceedToSignController');

const sendESignatureLink = async (req, res, next) => {

    const { firstName, lastName, middleName, email, subscriptionId } = req.body;

    const eSignature = await models.Esignature.create({
        firstName,
        lastName,
        email,
        middleName,
        subscriptionId
    });

    emailHelper.sendEmail(
        {
            toAddress: user.email,
            //Change subject
            subject: messageHelper.resetPasswordEmailSubject,
            data: {
                name: commonHelper.getFullName({
                    firstName,
                    lastName,
                    middleName
                }),
                user,
                link: `${config.get('serverURL')}/signatory/eSignature/validate/${eSignature.tokenId}`
            },
            htmlPath: "lpSignatoryESignatureInvite.html"
        });

    return res.json({
        data: 'Invitation has been sent successfully'
    });

    // send link to given email address for signatory
    // insert this data in table - firstName, lastName, middleName, email, signature, documentId (object), randomId to validate link, createdAt, updatedAt, isActive 
    //
}

const validateAndGetDocsForSignature = async (req, res, next) => {

    const tokenId = req.params.tokenId;

    const eSignature = await models.Esignature.findOne({
        where: {
            tokenId: tokenId
        }
    });

    if(!eSignature) {
        return errorHelper.error_400(res, 'tokenId', messageHelper.invalidToken);
    }


    let subscription = await models.FundSubscription.getSubscription(eSignature.subscriptionId, models);

    if (!subscription) {
        return errorHelper.error_400(res, 'subscriptionId', messageHelper.subscriptionIdReq);
    }

    let documentsForSignature = await models.DocumentsForSignature.findAll({
        where: {
            subscriptionId: subscription.id,
            fundId: subscription.fund.id,
            isActive: 1,
            docType: {
                [Op.in]: ['SUBSCRIPTION_AGREEMENT', 'FUND_AGREEMENT']
            }
        }
    });

    if((subscription.noOfSignaturesRequired < 0) || documentsForSignature.length === 0) {
        return lpProceedToSignController.getOnlyLinksExternal(subscription, req, res, next);
    } 

    const SUBSCRIPTION_AGREEMENT = _.find(documentsForSignature, {
        docType: 'SUBSCRIPTION_AGREEMENT'
    });

    const FUND_AGREEMENT = _.find(documentsForSignature, {
        docType: 'FUND_AGREEMENT'
    });

    return res.json({
        subscriptionAgreementUrl: commonHelper.getLocalUrl(SUBSCRIPTION_AGREEMENT.filePath),
        fundAgreementUrl: commonHelper.getLocalUrl(FUND_AGREEMENT.filePath),
        subscription: {
            dynamicTitle: subscription.legalTitleDesignationPrettyForPDF,
            investorType: subscription.investorType,
            lp: {
                firstName: subscription.lp.firstName,
                lastName: subscription.lp.lastName,
                middleName: subscription.lp.middleName
            }
        }
    });

}



module.exports = {
    sendESignatureLink,
    validateAndGetDocsForSignature
}