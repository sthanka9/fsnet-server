const models = require('../models/index');
const _ = require('lodash')
const messageHelper = require('../helpers/messageHelper');
const errorHelper = require('../helpers/errorHelper');

const checkConsentForClose = async(req, res, next) => {
    
    try {
    const fundId = req.params.fundId
    const gpDelegateId = req.userId

    if (req.user.accountType == 'GPDelegate') {
        let getConsentForClosing = await models.GpDelegates.findAll({
            attributes: ['gPDelegateRequiredConsentHoldClosing'],
            where: {
                fundId: fundId,
                delegateId: gpDelegateId                
            }
        })

        return res.json(getConsentForClosing[0])

    } else {
        return res.status(401).json({
            data: messageHelper.authorizationCheck
        })
    }
} catch(error) {
    next(error);
}
}

const approvalForClose = async(req, res, next) => {
    try {

        const { lpClosingData = [], fundId } = req.body;

        if (!fundId) {
            return errorHelper.error_400(res, 'fundId', messageHelper.fundNotFound);
        }

        if (lpClosingData.length <= 0) {
            return errorHelper.error_400(res, 'lpClosingData', messageHelper.validDataCheck);
        }

        const approvedLpList = []
        for (let d of lpClosingData) {
            approvedLpList.push({
                fundId: fundId,
                subscriptionId: d.subscriptionId,
                delegateId: req.userId,
                approvedByDelegate: 1,
                lpId: d.lpId
            })
        }

        // insert delegate approved lp's
        const delegateApproved = await models.GpDelegateApprovals.bulkCreate(approvedLpList)

        return res.json({ data: delegateApproved })

    } catch (error) {
        return next(error)
    }
}


module.exports = {
    checkConsentForClose,
    approvalForClose,
    
}