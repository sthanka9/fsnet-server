const models = require('../models/index')
const { Op } = require('sequelize')
const AdmZip = require('adm-zip')
const config = require('../config/index');
const errorHelper = require('../helpers/errorHelper');
const util = require('util');
const _ = require('lodash');
const path = require('path');
const exec = util.promisify(require('child_process').exec);

module.exports.download = async (req, res, next) => {

    try {

        const { fileIds, subscriptionId, fundId, includeAllFile } = req.body;

        // const ids = fileIds.split(',');
        const ids = _.map(fileIds,'id');

        if (ids.length == 0 || !(subscriptionId || fundId)) {
            return errorHelper.error_400(res,'fundIdOrsubscriptionId','Provide all fields');
        }

        let filter = {};

        if (subscriptionId) {
            filter = {
                subscriptionId: subscriptionId,
                id: {
                    [Op.in]: ids
                }
            };
        }

        if (fundId) {
            filter = {
                fundId: fundId,
                id: {
                    [Op.in]: ids
                }
            };
        }


        const files = await models.DocumentsForSignature.findAll({
            where: filter
        });

        const filesForZip = [];

        for (const file of files) {
        
            let fileName = path.basename(file.filePath);
            let ext = path.extname(fileName)
            let updatedFileName = _.find(fileIds,{id:file.id}).name;
            updatedFileName = _.upperCase(updatedFileName)

            let updatedFilePath = file.filePath.replace(fileName,(updatedFileName.replace(/ /g,"_"))+ext)

            await exec(`cp ${file.filePath} ${updatedFilePath}`)

            filesForZip.push(updatedFilePath);
        }

        if (includeAllFile) {
            const fund = await models.Fund.findOne({
                where: {
                    id: fundId
                }
            });

            if (fund.consolidateFundAgreement) {
                filesForZip.push(fund.consolidateFundAgreement)
            }
        }

        const downloadName = `${Date.now()}.zip`;


        await exec(`zip -rDj ./assets/temp/${downloadName} ${filesForZip.join(' ')}`)
        
        return res.json({
            url: `${config.get('serverURL')}/assets/temp/${downloadName}`,           
        });

    } catch (error) {
        next(error);
    }
}


module.exports.amendmentdownload = async (req, res, next) => {

    try {

        const { fileIds} = req.body;
        const ids = fileIds.split(',');
       

        console.log("ids**********",ids)

        if (ids.length == 0) {
            return errorHelper.error_400(res,'fundIdOrsubscriptionId','Provide all fields');
        }

        const files = await models.FundAmmendment.findAll({
            where:{
                id: {
                    [Op.in]: ids
                }
            } 
        })         
 

        const filesForZip = [];

        for (const file of files) {
            let amendmentfilepath = file.document.path
            let amendmentfilename = file.document.originalname
            let updatedFilePath =  './assets/temp/'+amendmentfilename
            updatedFilePath = updatedFilePath.replace(/ /g,"_")
            await exec(`cp ${amendmentfilepath} ${updatedFilePath}`)
            filesForZip.push(updatedFilePath);
        }    

        const downloadName = `${Date.now()}.zip`;
        await exec(`zip -rDj ./assets/temp/${downloadName} ${filesForZip.join(' ')}`)        
        return res.json({
            url: `${config.get('serverURL')}/assets/temp/${downloadName}`,           
        });

    } catch (error) {
        next(error);
    }
}