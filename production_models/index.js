'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(__filename);
var env       = process.env.NODE_ENV || 'development';
var config    = require('../config');
var prod_db        = {};

const prod_sequelize = new Sequelize('vanillavc_production_2019-01-31T15-45Z', config.get('username'), config.get('password'), {
  dialect: 'mssql',
  host: config.get('host'),
  port: 1433,
  logging: false,
  dialectOptions: {
      options: {
        encrypt: true
      }
    },
});

  fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    var model = prod_sequelize['import'](path.join(__dirname, file));
    prod_db[model.name] = model;
  });

Object.keys(prod_db).forEach(modelName => {
  if (prod_db[modelName].associate) {
    prod_db[modelName].associate(prod_db);
  }
});


prod_db.prod_sequelize = prod_sequelize;
prod_db.Sequelize = Sequelize;


module.exports = prod_db;
