'use strict';
module.exports = (sequelize, DataTypes) => {
  var FundSubscription = sequelize.define('FundSubscription', {
    fundId: {
      type: DataTypes.INTEGER
    },
    lpId: {
      type: DataTypes.INTEGER
    },
    investorType: {
      type: DataTypes.ENUM,
      values: ['Individual', 'LLC', 'Trust'],
      allowNull: true
    },
    trustTitle: DataTypes.STRING,
    isSubjectToDisqualifyingEvent: DataTypes.BOOLEAN, 
    fundManagerInfo: DataTypes.TEXT('long'),
    name: {
      type: DataTypes.STRING
    },
    otherInvestorAttributes: {
      type: DataTypes.STRING,
      get() {        
        const otherInvestorAttributes = this.getDataValue('otherInvestorAttributes')
        return otherInvestorAttributes ? otherInvestorAttributes.split(',') : []
      },

      set(val) {
        this.setDataValue('otherInvestorAttributes', ((val && typeof(val) == 'object') ? val.join(',') : val));
      }
    },
    areYouSubscribingAsJointIndividual: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    legalTitle: {
      type: DataTypes.STRING,
      allowNull: true
    },
    areYouAccreditedInvestor: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    areYouQualifiedPurchaser: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    areYouQualifiedClient: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    typeOfLegalOwnership: {
      type: DataTypes.ENUM,
      values: ['communityProperty', 'tenantsInCommon', 'jointTenants'],
      allowNull: true
    },
    whatSTATEIsTheEntityRegistered: {
      type: DataTypes.INTEGER
    },
    mailingAddressStreet: {
      type: DataTypes.STRING
    },
    mailingAddressCity: {
      type: DataTypes.STRING
    },
    mailingAddressState: {
      type: DataTypes.STRING
    },
    mailingAddressZip: {
      type: DataTypes.STRING
    },
    mailingAddressPhoneNumber: {
      type: DataTypes.STRING
    },
    mailingAddressCountry: DataTypes.INTEGER,

    //.... LLC
    indirectBeneficialOwnersSubjectFOIAStatutes: {
      type: DataTypes.STRING
    },
    investorSubType: {
      type: DataTypes.INTEGER
    },
    otherInvestorSubType: {
      type: DataTypes.STRING
    },
    entityName: {
      type: DataTypes.STRING
    },
    entityTitle: {
      type: DataTypes.STRING
    },
    jurisdictionEntityLegallyRegistered: {
      type: DataTypes.STRING
    },
    isEntityTaxExemptForUSFederalIncomeTax: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    isEntityUS501c3: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    istheEntityFundOfFundsOrSimilarTypeVehicle: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    releaseInvestmentEntityRequired: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    // 3
    companiesAct: {
      type: DataTypes.STRING,
    },
    numberOfDirectEquityOwners: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    existingOrProspectiveInvestorsOfTheFund: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    numberOfexistingOrProspectives: {
      type: DataTypes.STRING
    },
    //Entity proposing
    entityProposingAcquiringInvestment: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    anyOtherInvestorInTheFund: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    entityHasMadeInvestmentsPriorToThedate: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    partnershipWillNotConstituteMoreThanFortyPercent: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    beneficialInvestmentMadeByTheEntity: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    //ERISA
    employeeBenefitPlan: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    planAsDefinedInSection4975e1: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    benefitPlanInvestor: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    totalValueOfEquityInterests: {
      type: DataTypes.DECIMAL(19, 2),
      allowNull: true,
      defaultValue: null
    },
    fiduciaryEntityIvestment: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    entityDecisionToInvestInFund: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    aggrement1: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    aggrement2: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    lpCapitalCommitment: {
      type: DataTypes.DECIMAL(19, 2),
      get() {
        const lpCapitalCommitment = this.getDataValue('lpCapitalCommitment')
        return Number(lpCapitalCommitment);
      }
    },
    numberOfGrantorsOfTheTrust: DataTypes.INTEGER,
    trustLegallyDomiciled: DataTypes.INTEGER,
    selectState: DataTypes.INTEGER,
    trustName: DataTypes.STRING,
    isTrust501c3: DataTypes.BOOLEAN,
    legalTitleDesignation: DataTypes.STRING,    
    status: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    previousStatus: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    investorCount: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    subscriptionAgreementPath: {
      type: DataTypes.BLOB,
      get() {
        const subscriptionAgreementPath = this.getDataValue('subscriptionAgreementPath');
        return subscriptionAgreementPath ?  JSON.parse(subscriptionAgreementPath) : subscriptionAgreementPath;
      },
      set(val) {
        this.setDataValue('subscriptionAgreementPath', JSON.stringify(val));
      }
    },
    subscriptionHtml: {
      type: DataTypes.BLOB,
      get() {
        const subscriptionHtml = this.getDataValue('subscriptionHtml');
        return subscriptionHtml ?  JSON.parse(subscriptionHtml) : subscriptionHtml;
      },
      set(val) {
        this.setDataValue('subscriptionHtml', JSON.stringify(val));
      }
    },
    subscriptionHtmlLastUpdated: {
      type: DataTypes.DATE,
    },    
    deletedAt: {
      type: DataTypes.DATE,
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }

  }, {
    hooks: {
      beforeUpdate: (fundSubscription, options) => {
        if(fundSubscription.status == 'RESTORE') { console.log('restore')
          fundSubscription.status = fundSubscription.previousStatus;          
        }  else { console.log('else restore')
          fundSubscription.previousStatus = fundSubscription.previous('status');
        }
      }
    }
  });

 
  FundSubscription.associate = function (models) {    
  };
  return FundSubscription;
};
