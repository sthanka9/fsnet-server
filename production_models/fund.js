'use strict';
const config = require('../config/index')

module.exports = (sequelize, DataTypes) => {
  var Fund = sequelize.define('Fund', {
    vcfirmId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    gpId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },

    // fundTargetCommitment
    legalEntity: {
      type: DataTypes.STRING
    },
    fundHardCap: {
      type: DataTypes.DECIMAL(19, 2),
      allowNull: true,
      defaultValue: null
    },
    fundManagerLegalEntityName: {
      type: DataTypes.STRING
    },
    fundTargetCommitment: {
      type: DataTypes.DECIMAL(19, 2),
      allowNull: true,
      defaultValue: null
    },
    // fundHardCap30Percent: {
    //   type: DataTypes.VIRTUAL,
    //   get() {
    //     const fundHardCap = this.getDataValue('fundHardCap')
    //     return fundHardCap ? fundHardCap * 1.3 : null
    //   }
    // },


    percentageOfLPCommitment: {
      type: DataTypes.STRING
    },
    percentageOfLPAndGPAggregateCommitment: {
      type: DataTypes.STRING
    },
    capitalCommitmentByFundManager: {
      type: DataTypes.DECIMAL(19, 2),
      allowNull: true,
      defaultValue: null
    },
    fundImage: {
      type: DataTypes.TEXT,
      get() {
        let fundImage = this.getDataValue('fundImage')
        fundImage = fundImage ? JSON.parse(fundImage) : null

        if (fundImage) {
          fundImage['url'] = `${config.get('azureStorage').azureBlobURL}${fundImage['path']}`; 
          return fundImage;
        } else { return null; }

      }
    },
    partnershipDocument: {
      type: DataTypes.TEXT,
      get() {
        let partnershipDocument = this.getDataValue('partnershipDocument');
        let fundId = this.getDataValue('id');
        partnershipDocument = partnershipDocument ? JSON.parse(partnershipDocument) : null;
        if (partnershipDocument) {
          partnershipDocument['url'] = `${config.get('assetsURL')}/api/v1/fund/${fundId}/doc/view?p=assets/fund/files/${partnershipDocument['name']}`;
          return partnershipDocument;
        } else { return null; }
      }
    },
    generalPartnersCapitalCommitmentindicated: {
      type: DataTypes.INTEGER
    },
    fundType: {
      type: DataTypes.INTEGER
    },
    deactivateReason: {
      type: DataTypes.STRING
    },
    statusId: {
      type: DataTypes.INTEGER
    },
    fundCommonName: {
      type: DataTypes.STRING,
    },
    fundManagerTitle: {
      type: DataTypes.STRING,
    },
    hardCapApplicationRule: {
      type: DataTypes.STRING,
    },
    fundManagerCommonName: {
      type: DataTypes.STRING,
    },
    // consolidateFundAgreementURL: {
    //   type: DataTypes.VIRTUAL,

    //   get() {
    //     const consolidateFundAgreement = this.getDataValue('consolidateFundAgreement');
    //     let partnershipDocument = this.getDataValue('partnershipDocument');
    //     partnershipDocument = partnershipDocument ? JSON.parse(partnershipDocument) : false;
    //     const fundId = this.getDataValue('id');
    //     if(Boolean(consolidateFundAgreement)) {
    //       return `${config.get('assetsURL')}/api/v1/fund/${fundId}/doc/view?p=${consolidateFundAgreement}`;
    //     } else if(partnershipDocument){
    //       return `${config.get('assetsURL')}/api/v1/fund/${fundId}/doc/view?p=${partnershipDocument.uri}`;
    //     } else {
    //       return null;
    //     }
        
    //   }

    // },
    // consolidateFundAgreement: {
    //   type: DataTypes.STRING
    // },
    // consolidateFundAgreementLastUpdated: {
    //   type: DataTypes.DATE,
    // },
    subscriptionAgreementPath: {
      type: DataTypes.BLOB,
      get() {
        const subscriptionAgreementPath = this.getDataValue('subscriptionAgreementPath');
        return subscriptionAgreementPath ?  JSON.parse(subscriptionAgreementPath) : subscriptionAgreementPath;
      },
      set(val) {
        this.setDataValue('subscriptionAgreementPath', JSON.stringify(val));
      }
    },
    subscriptionHtml: {
      type: DataTypes.BLOB,
      get() {
        const subscriptionHtml = this.getDataValue('subscriptionHtml');
        return subscriptionHtml ?  JSON.parse(subscriptionHtml) : subscriptionHtml;
      },
      set(val) {
        this.setDataValue('subscriptionHtml', JSON.stringify(val));
      }
    },
    additionalSignatoryPages: {
      type: DataTypes.STRING,
      get() {
        const additionalSignatoryPages = this.getDataValue('additionalSignatoryPages');
        return additionalSignatoryPages ?  JSON.parse(additionalSignatoryPages) : additionalSignatoryPages;
      },
      set(val) {
        this.setDataValue('additionalSignatoryPages', JSON.stringify(val));
      }
    },
    additionalSignatoryPagesUpdatedDate: {
      type: DataTypes.DATE,
      allowNull: null
    },
    isAdditionalSignatoryPagesAddedToConslidatedPDF: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    subscriptionHtmlLastUpdated: {
      type: DataTypes.DATE,
    },    
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
  }, {});
  Fund.associate = function (models) {
  };
  return Fund;
};