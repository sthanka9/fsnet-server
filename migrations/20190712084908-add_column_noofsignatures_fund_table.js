'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Funds','noOfSignaturesRequiredForSideLetter', {
      type: Sequelize.INTEGER,
      defaultValue: 1
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Funds', 'noOfSignaturesRequiredForSideLetter');
  }
}