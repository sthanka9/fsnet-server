'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'DocumentsForSignatures',
      'lpSignedDate',
      {
        type: Sequelize.DATE,
        defaultValue: null
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('DocumentsForSignatures', 'lpSignedDate')
  }
};

