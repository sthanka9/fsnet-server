'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'LpDelegates',
        'invitedDate',
        {
          type: Sequelize.DATE,
        }
      )
  },
  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('LpDelegates', 'invitedDate')
  }
};