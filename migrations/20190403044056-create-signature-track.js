'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('SignatureTracks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      documentId: {
        type: Sequelize.STRING
      },
      signaturePath: {
        type: Sequelize.STRING
      },
      ipAddress: {
        type: Sequelize.STRING
      },
      signedDateTime: {
        type: Sequelize.DATE
      },
      timezone: {
        type: Sequelize.STRING
      },
      signedDateInGMT: {
        type: Sequelize.DATE
      },
      signedUserId: {
        type: Sequelize.INTEGER
      },
      signedByType: {
        type: Sequelize.STRING
      },
      isActive: Sequelize.BOOLEAN,
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('SignatureTracks');
  }
};