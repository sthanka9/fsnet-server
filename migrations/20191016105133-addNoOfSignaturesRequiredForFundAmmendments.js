'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'FundAmmendments',
      'noOfSignaturesRequiredForFundAmmendments',
      {
        type: Sequelize.INTEGER,
        defaultValue: 1
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('FundAmmendments', 'noOfSignaturesRequiredForFundAmmendments')
  }
};
