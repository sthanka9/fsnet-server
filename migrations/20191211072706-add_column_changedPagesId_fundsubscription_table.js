'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'FundSubscriptions',
      'changedPagesId',
      {
        type: Sequelize.INTEGER,
        defaultValue: null
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('FundSubscriptions', 'changedPagesId')
  }
};

