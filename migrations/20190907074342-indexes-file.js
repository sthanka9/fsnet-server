'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
   return queryInterface.sequelize.transaction((t) => {
    return Promise.all([
        //FundSubscription
        queryInterface.addIndex(
          'FundSubscriptions',
          ['fundId'],{ transaction:t }
        ),
        //GpDelegates
        queryInterface.addIndex(
          'GpDelegates',
          ['fundId'],{ transaction:t }
        ),
        //GpSignatories
        queryInterface.addIndex(
          'GpSignatories',
          ['fundId'],{ transaction:t }
        ),
        //LpDelegate
        queryInterface.addIndex(
          'LpDelegates',
          ['fundId'],{ transaction:t }
        ),
        //LpSignatories
        queryInterface.addIndex(
          'LpSignatories',
          ['fundId'],{ transaction:t }
        ),
        //User
        queryInterface.addIndex(
          'Users',
          ['email'],{ transaction:t }
        ),
        queryInterface.addIndex(
          'Users',
          ['accountId'],{ transaction:t }
        ),
        //VcFrimDelegate
        queryInterface.addIndex(
          'VcFrimDelegates',
          ['delegateId'],{ transaction:t }
        ),
        //VcFirmSignatories
        queryInterface.addIndex(
          'VcFirmSignatories',
          ['signatoryId'],{ transaction:t }
        ),
        //DocumentsForSignature
        queryInterface.addIndex(
          'DocumentsForSignatures',
          ['subscriptionId'],{ transaction:t }
        ),
        queryInterface.addIndex(
          'DocumentsForSignatures',
          ['fundId'],{ transaction:t }
        ),
        //Fund
        queryInterface.addIndex(
          'Funds',
          ['vcfirmId'],{ transaction:t }
        )
      ])
    })
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
      
    */
   return queryInterface.sequelize.transaction((t) => {
    return Promise.all([

        queryInterface.removeIndex(
          'FundSubscriptions',
          ['fundId'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'GpDelegates',
          ['fundId'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'GpSignatories',
          ['fundId'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'LpDelegates',
          ['fundId'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'LpSignatories',
          ['fundId'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'Users',
          ['email'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'Users',
          ['accountId'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'VcFrimDelegates',
          ['delegateId'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'VcFirmSignatories',
          ['signatoryId'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'DocumentsForSignatures',
          ['subscriptionId'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'DocumentsForSignatures',
          ['fundId'],{ transaction:t }
        ),
        queryInterface.removeIndex(
          'Funds',
          ['vcfirmId'],{ transaction:t }
        )
      ])
    })
  }

};
