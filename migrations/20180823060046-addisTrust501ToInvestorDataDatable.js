'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn(
       'FundSubscriptions',
       'isTrust501c3',
       {
         type: Sequelize.BOOLEAN,
       }
     );
 
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('FundSubscriptions', 'isTrust501c3');
  }
};
