'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Funds', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      vcfirmId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      gpId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      legalEntity: {
        type: Sequelize.STRING
      },
      fundHardCap: {
        type: Sequelize.DECIMAL(19,2),
        allowNull: true,
        defaultValue: null
      },
      fundManagerCommonName: Sequelize.STRING,
      hardCapApplicationRule: Sequelize.STRING,
      fundManagerTitle: Sequelize.STRING,
      fundCommonName: Sequelize.STRING,
      fundManagerLegalEntityName: {
        type: Sequelize.STRING
      },
      fundTargetCommitment: {
        type: Sequelize.DECIMAL(19,2),
        allowNull: true,
        defaultValue: null
      },
      capitalCommitmentByFundManager: {
        type: Sequelize.DECIMAL(19,2),
        allowNull: true,
        defaultValue: null
      },
      partnershipDocument: {
        type: Sequelize.TEXT
      },
      fundImage: {
        type: Sequelize.TEXT
      },
      percentageOfLPCommitment: {
        type: Sequelize.STRING
      },
      percentageOfLPAndGPAggregateCommitment: {
        type: Sequelize.STRING
      },
      deactivateReason: {
        type: Sequelize.STRING,
        allowNull: true
      },
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      subscriptionHtmlLastUpdated: Sequelize.DATE,
      subscriptionHtml: Sequelize.BLOB,
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Funds');
  }
};