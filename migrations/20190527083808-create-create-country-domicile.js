'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('FundSubscriptions','countryDomicileId', {
      type: Sequelize.INTEGER     
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('FundSubscriptions', 'countryDomicileId');
  }
};