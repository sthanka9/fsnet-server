'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'FundSubscriptions',
      'trustTitle',
      {
        type: Sequelize.STRING,
        defaultValue: null
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('FundSubscriptions', 'trustTitle')
  }
};
