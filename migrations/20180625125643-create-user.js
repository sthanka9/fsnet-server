'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      password: {
        type: Sequelize.STRING
      },
      cellNumber: {
        type: Sequelize.STRING
      },
      profilePic: {
        type: Sequelize.TEXT
      },
      isEmailConfirmed: {
        type: Sequelize.BOOLEAN,
        allowNull: false, 
        defaultValue: false  
      },
      emailConfirmCode: {
        type: Sequelize.STRING
      },
      lastCodeGeneratedAt: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.DATE
      },
      city :  {
        type: Sequelize.STRING,
        defaultValue: null
      },
      isImpersonatingAllowed :
      {
        type: Sequelize.BOOLEAN,
        allowNull: false, 
        defaultValue: false  
      },
      accountType: {
        type:   Sequelize.ENUM,
        values: ['FSNETAdministrator', 'GP', 'GPDelegate', 'LP', 'LPDelegate', 'SecondaryGP', 'SecondaryLP'],
        allowNull: false
      },
      loginFailAttempts: {
        allowNull: true,
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      loginFailAttemptsAt: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.DATE
      },  
      lastLoginAt: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.DATE
      },
      organizationName: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      streetAddress1: Sequelize.STRING,
      streetAddress2: Sequelize.STRING,
      zipcode:  Sequelize.STRING,
      primaryContact: Sequelize.TEXT
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};