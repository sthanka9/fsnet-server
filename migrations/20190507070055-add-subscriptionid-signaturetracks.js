'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('SignatureTracks','subscriptionId', {
      type: Sequelize.INTEGER      
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('SignatureTracks', 'subscriptionId');
  }
};