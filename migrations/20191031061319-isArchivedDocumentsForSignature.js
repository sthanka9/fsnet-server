'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'DocumentsForSignatures',
      'isArchived',
      {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('DocumentsForSignatures', 'isArchived')
  }
};

