'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DocumentsForSignatures', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fundId: {
        type: Sequelize.INTEGER
      },
      subscriptionId: {
        type: Sequelize.INTEGER
      },
      changeCommitmentId: {
        type: Sequelize.INTEGER
      },
      filePath: {
        type: Sequelize.STRING
      },
      docType: {
        type: Sequelize.STRING
      },
      gpSignCount: {
        type: Sequelize.INTEGER,
        default: 0
      },
      lpSignCount: {
        type: Sequelize.INTEGER,
        default: 0
      },
      isPrimaryGpSigned: {
        type: Sequelize.BOOLEAN,
        default: false
      },
      isPrimaryLpSigned: {
        type: Sequelize.BOOLEAN,
        default: false
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },    
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      },
      isActive: Sequelize.BOOLEAN
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DocumentsForSignatures');
  }
};