'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Funds',
      'timezone',
      {
        type: Sequelize.BOOLEAN,
        defaultValue: null
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('Funds', 'timezone')
  }
};
