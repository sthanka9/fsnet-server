'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('VcFrimLps','isOffline', {
      type: Sequelize.BOOLEAN,
      defaultValue:false      
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('VcFrimLps', 'isOffline');
  }
};