'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'Funds',
        'subscriptionAgreementPath',
        {
          type: Sequelize.BLOB,
        }
      )
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Funds', 'subscriptionAgreementPath')
  }
};