'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Funds',
      'isFundLocked',
      {
        type: Sequelize.BOOLEAN
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('Funds', 'isFundLocked')
  }
};
