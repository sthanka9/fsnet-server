'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('SubscriptionCrons', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fundId: {
        type: Sequelize.INTEGER
      },
      subscriptionId: {
        type: Sequelize.INTEGER
      },
      partnershipDocumentPageCount: {
        type: Sequelize.INTEGER
      },
      uploadedFilePath: {
        type: Sequelize.TEXT
       
      },
      originalPartnershipDocument:{
        type: Sequelize.TEXT
      },
      partnershipDocumentToSwap:{
        type: Sequelize.STRING
      },
      swappedPartnershipAgreement:{
        type: Sequelize.STRING
      },
      partnershipDocumentToReplace:{
        type: Sequelize.STRING
      },
      docType:{
        type: Sequelize.STRING
      },
      userId: {
        type: Sequelize.INTEGER
      },
      amendmentId: {
        type: Sequelize.INTEGER
      },
      fundCronId:{
        type: Sequelize.INTEGER
      },
      status:{
        type: Sequelize.INTEGER,
        defaultValue:null   
      },
      count:{
        type: Sequelize.INTEGER,
        defaultValue:null   
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('SubscriptionCrons');
  }
};