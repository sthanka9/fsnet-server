'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Accounts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING
      },     
      lastName: {
        type: Sequelize.STRING
      },     
      middleName: {
        type: Sequelize.STRING
      },                  
      email: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      organizationName: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      state: {
        type: Sequelize.STRING
      },
      country: {
        type: Sequelize.STRING
      },   
      cellNumber: {
        type: Sequelize.STRING
      },         
      profilePic: {
        type: Sequelize.TEXT
      },     
      isImpersonatingAllowed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },  
      defaultUserId:{
        type: Sequelize.INTEGER
      },
      isEmailConfirmed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      emailConfirmCode: {
        type: Sequelize.STRING
      },
      lastCodeGeneratedAt: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.DATE
      },
      loginFailAttempts: {
        allowNull: true,
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      loginFailAttemptsAt: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.DATE
      },
      lastLoginAt: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.DATE
      },
      streetAddress1: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      streetAddress2: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      zipcode: {
        defaultValue: null,
        allowNull: true,
        type: Sequelize.STRING
      },
      signaturePic: {
        type: Sequelize.TEXT
      },
      primaryContact: {
        type: Sequelize.TEXT
      },
      isEmailNotification: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      deletedAt: Sequelize.DATE,   
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },                 
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Accounts');
  }
};