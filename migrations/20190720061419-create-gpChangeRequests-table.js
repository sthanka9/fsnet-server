'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('gpChangeRequests', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },      
      fundId: {
        type: Sequelize.INTEGER
      },      
      currentGpId: {
        type: Sequelize.INTEGER
      },
      newGpId: {
        type: Sequelize.INTEGER
      },
      vcfirmIdOld:{
        type: Sequelize.INTEGER,
      },
      randomToken: {
        type: Sequelize.STRING
      },
      convertCurrentGpToSignatory : {
        type: Sequelize.BOOLEAN
      },
      deletedAt:Sequelize.DATE,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('gpChangeRequests');
  }
};
