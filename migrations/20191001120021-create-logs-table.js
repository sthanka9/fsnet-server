'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('LogsTables', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fundId: {
        type: Sequelize.INTEGER
      },
      subscriptionId: {
        type: Sequelize.INTEGER
      },
      status:{
        type: Sequelize.ENUM,
        values: [0,1,2],
        allowNull:false
  
      },
      docType:{
        type: Sequelize.STRING,
      },
      createdBy:{
        type: Sequelize.INTEGER
      },
      startTime:{
        type: Sequelize.DATE
      },
      endTime:{
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('LogsTables');
  }
};