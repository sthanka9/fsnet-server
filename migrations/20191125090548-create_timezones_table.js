'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('timezones', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      displayName: {
        type: Sequelize.TEXT
      },
      code: {
        type: Sequelize.STRING
      },
      isDefault: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      status:{
        type: Sequelize.INTEGER,
        defaultValue:1   
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }      
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('timezones');
  }
}
