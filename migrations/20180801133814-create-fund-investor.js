'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FundSubscriptions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fundId: {
        type: Sequelize.INTEGER
      },
      lpId: {
        type: Sequelize.INTEGER
      },
      investorType: {
        type: Sequelize.ENUM,
        values: ['Individual', 'LLC', 'Trust'],
        allowNull: false
      },
      whatSTATEIsTheEntityRegistered: {
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      areYouSubscribingAsJointIndividual: {
        type: Sequelize.BOOLEAN
      },
      areYouAccreditedInvestor: {
        type: Sequelize.BOOLEAN
      },
      indirectBeneficialOwnersSubjectFOIAStatutes: {
        type: Sequelize.STRING
      },
      areYouQualifiedPurchaser: {
        type: Sequelize.BOOLEAN
      },
      areYouQualifiedClient: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      typeOfLegalOwnership: {
        type: Sequelize.STRING,
       // values: ['jointTenants', 'jointTenantsRights', 'tenantsInCommon', 'tenantsEntirety', 'communityProperty'],
        allowNull: true
      },
      mailingAddressStreet: {
        type: Sequelize.STRING
      },
      mailingAddressCity: {
        type: Sequelize.STRING
      },
      mailingAddressState: {
        type: Sequelize.STRING
      },
      mailingAddressZip: {
        type: Sequelize.STRING
      },
      mailingAddressPhoneNumber: {
        type: Sequelize.STRING
      },

      otherInvestorAttributes: {
        type: Sequelize.TEXT('long')
      },

      //.... LLC
      investorSubType: {
        type: Sequelize.INTEGER
      },
      otherInvestorSubType: {
        type: Sequelize.STRING
      },
      entityName: {
        type: Sequelize.STRING
      },
      jurisdictionEntityLegallyRegistered: {
        type: Sequelize.STRING
      },
      isEntityTaxExemptForUSFederalIncomeTax: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      isEntityUS501c3: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      istheEntityFundOfFundsOrSimilarTypeVehicle: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      releaseInvestmentEntityRequired: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      // 3
      companiesAct: {
        type: Sequelize.STRING,
      },
      numberOfDirectEquityOwners: {
        type:Sequelize.INTEGER
      },
      existingOrProspectiveInvestorsOfTheFund: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      numberOfexistingOrProspectives: {
        type:Sequelize.STRING
      },
      //Entity proposing
      entityProposingAcquiringInvestment: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
       anyOtherInvestorInTheFund: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      entityHasMadeInvestmentsPriorToThedate: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      partnershipWillNotConstituteMoreThanFortyPercent: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      beneficialInvestmentMadeByTheEntity: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      //ERISA
      employeeBenefitPlan: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      planAsDefinedInSection4975e1: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      benefitPlanInvestor: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      totalValueOfEquityInterests: {
        type: Sequelize.DECIMAL(19,2),
        allowNull: true,
        defaultValue: null
      },
      fiduciaryEntityIvestment: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      entityDecisionToInvestInFund: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      aggrement1: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      aggrement2: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      // ...
      status:
      {
        type:Sequelize.INTEGER
      },
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },

      numberOfGrantorsOfTheTrust: Sequelize.INTEGER,
      trustLegallyDomiciled: Sequelize.INTEGER,
      selectState: Sequelize.INTEGER,
      trustName: Sequelize.STRING,
      acceptedByGp: Sequelize.DECIMAL(19, 2),
      acceptedByGp: Sequelize.DECIMAL(19, 2),
      lpCapitalCommitment: Sequelize.DECIMAL(19,2),
      isSubjectToDisqualifyingEvent: Sequelize.BOOLEAN,
      fundManagerInfo: Sequelize.TEXT('long'),
      entityTitle: Sequelize.STRING,
      subscriptionHtmlLastUpdated: Sequelize.DATE,
      subscriptionHtml: Sequelize.BLOB,
      deletedAt: Sequelize.DATE,
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FundSubscriptions');
  }
};