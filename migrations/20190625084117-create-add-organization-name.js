'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('FundSubscriptions','organizationName', {
      type: Sequelize.STRING      
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('FundSubscriptions', 'organizationName');
  }
};