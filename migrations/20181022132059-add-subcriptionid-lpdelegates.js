'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn(
       'LpDelegates',
       'subscriptionId',
       {
         type: Sequelize.INTEGER,
       }
     );
 
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('LpDelegates', 'subscriptionId');
  }
};
