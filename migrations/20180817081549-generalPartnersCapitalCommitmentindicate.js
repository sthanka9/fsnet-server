'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn(
       'Funds',
       'generalPartnersCapitalCommitmentindicated',
       {
         type: Sequelize.INTEGER,
       }
     );
 
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('Funds', 'generalPartnersCapitalCommitmentindicated');
  }
};