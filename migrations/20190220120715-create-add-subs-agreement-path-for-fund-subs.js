'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'FundSubscriptions',
        'subscriptionAgreementPath',
        {
          type: Sequelize.BLOB,
        }
      )
  },
  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('FundSubscriptions', 'subscriptionAgreementPath')
  }
};