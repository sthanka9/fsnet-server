'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'FundSubscriptions',
      'isLpSignatoriesNotified',
      {
        type: Sequelize.BOOLEAN,
        defaultValue:0
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('FundSubscriptions', 'isLpSignatoriesNotified')
  }
};