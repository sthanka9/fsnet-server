'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Events', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      eventType: Sequelize.STRING,
      userId: Sequelize.INTEGER,
      fundId: Sequelize.INTEGER,
      subscriptionId: Sequelize.INTEGER,
      eventTimestamp: Sequelize.DATE,
      ipAddress: Sequelize.STRING,
      createdBy: Sequelize.INTEGER,
      updatedBy: Sequelize.INTEGER,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Events');
  }
};
