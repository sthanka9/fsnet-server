'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn(
       'FundSubscriptions',
       'mailingAddressCountry',
       {
         type: Sequelize.INTEGER,
       }
     );
 
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('FundSubscriptions', 'mailingAddressCountry');
  }
};