'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'GpSignatoryApprovals',
      'amendmentId',
      {
        type: Sequelize.INTEGER,
        defaultValue: null
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('GpSignatoryApprovals', 'amendmentId')
  }
};
