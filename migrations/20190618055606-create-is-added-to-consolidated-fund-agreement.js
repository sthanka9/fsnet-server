'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('SignatureTracks','isAddedToConsolidatedFundAgreement', {
      type: Sequelize.BOOLEAN,
      default:false     
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('SignatureTracks','isAddedToConsolidatedFundAgreement');
  }
};