'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FundCrons', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fundId: {
        type: Sequelize.INTEGER
      },
      uploadedFilePath: {
        type: Sequelize.TEXT
      },
      uploadType:{
        type: Sequelize.STRING
      },
      status:{
        type: Sequelize.INTEGER,
        defaultValue:null   
      },
      userId: {
        type: Sequelize.INTEGER
      },
      amendmentId: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FundCrons');
  }
};