'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Funds',
      'deletedAt',
      {
        type: Sequelize.DATE
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Funds', 'deletedAt')
  }
};
