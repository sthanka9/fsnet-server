'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('VCFirms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firmName: {
        type: Sequelize.STRING
      },
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      cellNumber: {
        type: Sequelize.STRING
      },
      subscriptonType: {
        type: Sequelize.STRING
      },
      isImporsonatingAllowed: {
        type: Sequelize.BOOLEAN,
        allowNull: false, 
        defaultValue: false  
      },
      allowGPdelegatesToSign: {
        type: Sequelize.BOOLEAN,
        allowNull: false, 
        defaultValue: false  
      },
      gpId: {
        type: Sequelize.INTEGER
      },
      VCFirmLogo: {
        type: Sequelize.TEXT
      },
      isPublished: {
        type: Sequelize.BOOLEAN
      },
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('VCFirms');
  }
};

