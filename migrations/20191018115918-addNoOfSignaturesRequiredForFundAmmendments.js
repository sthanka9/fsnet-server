'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Funds',
      'noOfSignaturesRequiredForFundAmmendments',
      {
        type: Sequelize.INTEGER,
        defaultValue: 1
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('Funds', 'noOfSignaturesRequiredForFundAmmendments')
  }
};
