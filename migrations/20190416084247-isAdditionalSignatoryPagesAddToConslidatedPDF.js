'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Funds',
      'isAdditionalSignatoryPagesAddedToConslidatedPDF',
      {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Funds', 'isAdditionalSignatoryPagesAddedToConslidatedPDF')
  }
};



