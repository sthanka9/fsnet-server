'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'FundSubscriptions',
      'isAdditionalQuestionAdded',
      {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('FundSubscriptions', 'isAdditionalQuestionAdded')
  }
};
