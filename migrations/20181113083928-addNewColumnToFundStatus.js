'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn(
        'FundStatuses',
        'lpSideName',
        {
          type: Sequelize.STRING,
        }
      )
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.removeColumn('FundStatuses', 'lpSideName')
  }
};
