'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'Funds',
        'statusId',
        {
          type: Sequelize.INTEGER,
        }
      )
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.removeColumn('Funds', 'statusId')
  }
};
