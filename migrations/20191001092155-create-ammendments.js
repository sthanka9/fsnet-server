'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FundAmmendments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fundId: {
        type: Sequelize.INTEGER
      },
      document: {
        type: Sequelize.TEXT
      },
      targetPercentage:{
        type: Sequelize.FLOAT
      },
      approvedPercentage:{
        type: Sequelize.FLOAT
      },
      isAmmendment:{
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      isAffectuated:{
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      affectuatedDate:{
        type: Sequelize.DATE
      },
      isTerminated:{
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      isArchived: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }, 
      isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      }, 
      considerOnlyLpOfferedAmount: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      }, 
      deletedAt: {
        type: Sequelize.DATE
      },
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FundAmmendments');
  }
};