'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'FundSubscriptions',
      'isInvestorInvited',
      {
        type: Sequelize.INTEGER,
        defaultValue:0
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('FundSubscriptions', 'isInvestorInvited')
  }
};
