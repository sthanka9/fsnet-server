'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'FundAmmendments',
      'changedPagesId',
      {
        type: Sequelize.INTEGER,
        defaultValue: null
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('FundAmmendments', 'changedPagesId')
  }
};

