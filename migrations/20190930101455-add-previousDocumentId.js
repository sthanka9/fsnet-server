'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'DocumentsForSignatures',
      'previousDocumentId',
      {
        type: Sequelize.INTEGER
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('DocumentsForSignatures', 'previousDocumentId')
  }
};
