'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'FundSubscriptions',
        'invitedDate',
        {
          type: Sequelize.DATE,
        }
      )
  },
  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('FundSubscriptions', 'invitedDate')
  }
};