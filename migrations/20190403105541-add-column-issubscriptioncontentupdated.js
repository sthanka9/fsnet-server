'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'FundSubscriptions',
      'isSubscriptionContentUpdated',
      {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    )
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.removeColumn('FundSubscriptions', 'isSubscriptionContentUpdated')
  }
};
