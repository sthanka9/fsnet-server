'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FundClosings', {
      
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      subscriptionId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
  
      fundId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
    
      lpCapitalCommitment: {
        type: Sequelize.DECIMAL(19, 2)
      },

      gpConsumed: {
        type: Sequelize.DECIMAL(19, 2)
      },

      createdBy: {
        type: Sequelize.INTEGER
      },

      updatedBy: {
        type: Sequelize.INTEGER
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: Sequelize.DATE,
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FundClosings');
  }
};