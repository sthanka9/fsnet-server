'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'DocumentsForSignatures',
      'amendmentId',
      {
        type: Sequelize.INTEGER,
        defaultValue: null
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('DocumentsForSignatures', 'amendmentId')
  }
};
