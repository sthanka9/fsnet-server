'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('GpDelegates', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fundId: Sequelize.INTEGER,
      delegateId: Sequelize.INTEGER,
      gpId: Sequelize.INTEGER,
      gPDelegateRequiredDocuSignBehalfGP: Sequelize.BOOLEAN,
      gPDelegateRequiredConsentHoldClosing: Sequelize.BOOLEAN,
      createdBy: Sequelize.INTEGER,
      updatedBy: Sequelize.INTEGER,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('GpDelegates');
  }
};