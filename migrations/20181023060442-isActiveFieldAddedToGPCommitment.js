'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn(
       'FundClosings',
       'isActive',
       {
         type: Sequelize.BOOLEAN,
         default: true
       }
     );
 
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('FundClosings', 'isActive');
  }
};