'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('OfflineUsers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      middleName: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      email: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      fundId:{
        type: Sequelize.INTEGER
      },
      organizationName: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      address:{
        type: Sequelize.TEXT,
        defaultValue: null
      },
      cellNumber:{
        type: Sequelize.STRING,
        defaultValue: null
      },
      accountType: {
        type: Sequelize.ENUM,
        values: ['OfflineLP'],
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      },
      deletedAt: Sequelize.DATE
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('OfflineUsers');
  }
};