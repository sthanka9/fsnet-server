'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.addColumn(
      'Funds',
      'noOfSignaturesRequiredForClosing',
      {
        type: Sequelize.INTEGER
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Funds', 'noOfSignaturesRequiredForClosing');
  }
};
