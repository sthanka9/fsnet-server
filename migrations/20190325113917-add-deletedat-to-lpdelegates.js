'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'LpDelegates',
      'deletedAt',
      {
        type: Sequelize.DATE
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('LpDelegates', 'deletedAt')
  }
};
