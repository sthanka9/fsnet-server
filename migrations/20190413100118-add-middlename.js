'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'Users',
        'middleName',
        {
          type: Sequelize.STRING,
          defaultValue: null
        }
      )
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Users', 'middleName')
  }
};