'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'LpDelegates',
        'isInvestorInvited',
        {
          type: Sequelize.INTEGER,
        }
      )
  },
  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('LpDelegates', 'isInvestorInvited')
  }
};