'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('FundSubscriptions','isPrimaryInvestment', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false  
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('FundSubscriptions', 'isPrimaryInvestment');
  }
}