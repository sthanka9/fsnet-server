'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Funds',
      'isNotificationsEnabled',
      {
        type: Sequelize.INTEGER,
        defaultValue: 0
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('Funds', 'isNotificationsEnabled')
  }
};

