'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'DocumentsForSignatures',
      'isDocumentEffectuated',
      {
        type: Sequelize.BOOLEAN,
        defaultValue: 0
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return  queryInterface.removeColumn('DocumentsForSignatures', 'isDocumentEffectuated')
  }
};
