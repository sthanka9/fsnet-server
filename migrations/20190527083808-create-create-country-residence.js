'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('FundSubscriptions','countryResidenceId', {
      type: Sequelize.INTEGER     
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('FundSubscriptions', 'countryResidenceId');
  }
};