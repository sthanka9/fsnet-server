'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('SignatureTracks','documentType', {
      type: Sequelize.STRING      
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('SignatureTracks', 'documentType');
  }
};