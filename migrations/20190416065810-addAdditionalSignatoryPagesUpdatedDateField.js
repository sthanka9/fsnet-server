'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Funds',
      'additionalSignatoryPagesUpdatedDate',
      {
        type: Sequelize.DATE,
        defaultValue: null
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Funds', 'additionalSignatoryPagesUpdatedDate')
  }
};
