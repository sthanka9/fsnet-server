'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'GpDelegates',
      'deletedAt',
      {
        type: Sequelize.DATE
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('GpDelegates', 'deletedAt')
  }
};
